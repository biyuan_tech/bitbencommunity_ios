//
//  AppDelegate.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/5/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppDelegate.h"
#import "BYTabbarViewController.h"
#import "AppDelegate+Config.h"
#import "ZYTabBar.h"
#import "PDWelcomeViewController.h"
#import <Bugtags/Bugtags.h>
#import "APNSTool.h"
#import "LaunchImageViewController.h"
#import <RPSDK/RPSDK.h>

@interface AppDelegate ()<UITabBarControllerDelegate,ZYTabBarDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    if (@available(iOS 11.0, *)){
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    }
    [BYServiceManager registerSettingDefultValue];
    // 1. 创建视图
    [self mainSetup];
    [self createStartAnimation];
    // 2. 绑定三方授权
    [ShareSDKManager registeShareSDK];
    // 6. 添加埋点
    [MTAManager mtainitWithApp];
    // 7. bug 管理
    //    [self registBugTagsWithOptions:launchOptions];
    // 8. 注册 推送
    __weak typeof(self)weakSelf = self;
    [[APNSTool shareInstance] registWithPUSHWithApplication:application launchOptions:launchOptions deviceIdBlock:^{
        if (!weakSelf){
            return ;
        }
    }];
    // 9.识别
    [RPSDK initialize:RPSDKEnvOnline];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    
}


#pragma maek - 推送
#pragma mark ---------------------------------------------------
// 注册deviceToken
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //        [[EMClient sharedClient] bindDeviceToken:deviceToken];
    });
    [[APNSTool shareInstance] registerWithDeviceId:deviceToken];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

// 通知统计回调
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo {
    [[APNSTool shareInstance] registerWithReceiveRemoteNotification:userInfo];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
}


#pragma mark - 推送end
#pragma mark ----------------------------------------------

#ifdef IS_IOS9_LATER
-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options{
    if ([url.description hasPrefix:@"bitben://"]) {
        return [self openURLManagerWithURL:url];
    }
    return YES;
}
#else
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    if ([Growing handleUrl:url]){
        return YES;
    } else {
        return [self openURLManagerWithURL:url];
    }
    return NO;
    
}
#endif

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    if ([viewController.tabBarItem.title isEqualToString:@"消息"]){
        // 埋点
        [MTAManager event:MTATypeTabMsg params:nil];
        // clean
        [[BYTabbarViewController sharedController] cleanMessageBirdge];
        
        if (![[AccountModel sharedAccountModel]hasLoggedIn]){
            [[AccountModel sharedAccountModel] authorizeWithCompletionHandler:^(BOOL successed) {
                if (successed){
                    [[BYTabbarViewController sharedController] showMessage];
                }
            }];
            return NO;
        } else {
            return YES;
        }
    } else if ([viewController.tabBarItem.title isEqualToString:@"我的"]){
        // 埋点
        [MTAManager event:MTATypeTabCenter params:nil];
        
        //        if (![[AccountModel sharedAccountModel]hasLoggedIn]){
        //            [[AccountModel sharedAccountModel] authorizeWithCompletionHandler:^(BOOL successed) {
        //                if (successed){
        //                    [[BYTabbarViewController sharedController] showCenter];
        //                }
        //            }];
        //            return NO;
        //        } else {
        //            return YES;
        //        }
    } else if ([viewController.tabBarItem.title isEqualToString:@"直播"]){
        // 埋点
        [MTAManager event:MTATypeTabLive params:nil];
    } else if ([viewController.tabBarItem.title isEqualToString:@"观点"]){
        // 埋点
        [MTAManager event:MTATypeTabGuandian params:nil];
        
    }
    if (tabBarController.selectedViewController == viewController) {
#pragma clang diagnostic ignored "-Wundeclared-selector"
        if ([CURRENT_VC respondsToSelector:@selector(autoReload)]) {
            [CURRENT_VC performSelector:@selector(autoReload)];
        }
    }
    return YES;
}

#pragma mark - ZYTabBarDelegate
// 发布按钮跳转
- (void)centerItemBtnClick{
    [[BYTabbarViewController sharedController] centerItemBtnClick];
}

#pragma mark - 是否是第一次使用
-(void)createStartAnimation{
    // 1. 判断是否第一次使用此版本
    NSString *versionKey = @"CFBundleShortVersionString";
    NSString *lastVersionCode = [[NSUserDefaults standardUserDefaults] objectForKey:versionKey];
    NSString *currentVersionCode = [[NSBundle mainBundle].infoDictionary objectForKey:versionKey];
    if ([lastVersionCode isEqualToString:currentVersionCode]) {                 // 非第一次使用软件
        [UIApplication sharedApplication].statusBarHidden = NO ;
        [self viewInfoManager];
    } else {                                                                    // 第一次使用软件
        [[NSUserDefaults standardUserDefaults] setObject:currentVersionCode forKey:versionKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // 跳转开场动画
        
        PDWelcomeViewController *welcomeViewController = [[PDWelcomeViewController alloc] init];
        __weak typeof(self)weakSelf = self;
        welcomeViewController.startBlock = ^(){
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf viewInfoManager];
        };
        
        self.window.rootViewController = welcomeViewController;
    }
}

#pragma mark - createMainView
-(void)mainSetup{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = RGB(240, 240, 240, 1);
    [self.window makeKeyAndVisible];
}


-(void)viewInfoManager{
    BYTabbarViewController *tabBarController = [BYTabbarViewController sharedController];
    tabBarController.delegate = self;
    ((ZYTabBar *)tabBarController.tabBar).tabbarDelegate = self;
    self.window.rootViewController = tabBarController;
    
    LaunchImageViewController *launchVC = [[LaunchImageViewController alloc] init];
    [self.window addSubview:launchVC.view];
}


-(void)registBugTagsWithOptions:(NSDictionary *)launchOptions {
    BugtagsOptions *options = [[BugtagsOptions alloc] init];
    options.remoteConfigDataMode = BTGDataModeProduction;
    
    options.trackingCrashes = YES;        // 是否收集闪退，联机 Debug 状态下默认 NO，其它情况默认 YES
    options.trackingUserSteps = YES;      // 是否跟踪用户操作步骤，默认 YES
    options.trackingConsoleLog = YES;     // 是否收集控制台日志，默认 YES
    options.trackingUserLocation = YES;   // 是否获取位置，默认 YES
    options.crashWithScreenshot = YES;    // 收集闪退是否附带截图，默认 YES
    options.ignorePIPESignalCrash = YES;  // 是否忽略 PIPE Signal (SIGPIPE) 闪退，默认 NO
    options.version = [Tool appVersion];          // 自定义版本名称，默认自动获取应用的版本号
    
    [Bugtags startWithAppKey:@"96c78f295d563fdbee3b5521df8898d2" invocationEvent:BTGInvocationEventBubble options:options];
}

#pragma mark - 创建推送apns
-(void)createAPNSManager{
    
}



#pragma mark --- 配置文件
-(void)makeConfiguration
{
    
}

@end
