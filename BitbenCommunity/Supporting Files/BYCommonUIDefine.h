
//
//  BYCommonUIDefine.h
//  BY
//
//  Created by 黄亮 on 2018/7/30.
//  Copyright © 2018年 Belief. All rights reserved.
//

#ifndef BYCommonUIDefine_h
#define BYCommonUIDefine_h

#pragma mark - 色值
// ** 色值选取
#define kColorRGB(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define kColorRGBValue(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

// ** Color

#define kRedColor kNaTitleColor_237
#define kNaTitleColor_53 kColorRGBValue(0x353535)
#define kNaTitleColor_237 kColorRGBValue(0xed4a45)

#define kTextColor_102  kColorRGBValue(0x666666)
#define kTextColor_117  kColorRGBValue(0xb1b1b1)
#define kTextColor_168  kColorRGBValue(0xa8a8a8)
#define kTextColor_238  kColorRGBValue(0xee4944)
#define kTextColor_39   kColorRGBValue(0x272727)
#define kTextColor_154  kColorRGBValue(0x9a9a9a)
#define kTextColor_77   kColorRGBValue(0x4d4d4d)
#define kTextColor_237  kNaTitleColor_237
#define kTextColor_60   kColorRGBValue(0x3c3c3c)
#define kTextColor_144  kColorRGBValue(0x909090)
#define kTextColor_48   kColorRGBValue(0x303030)
#define kTextColor_135  kColorRGBValue(0x878787)
#define kTextColor_111  kColorRGBValue(0x6f6f6f)
#define kTextColor_53   kNaTitleColor_53
#define kTextColor_107  kColorRGBValue(0x6b6b6b)
#define kTextColor_86   kColorRGBValue(0x565656)
#define kTextColor_68   kColorRGBValue(0x4484ee)
#define kTextColor_71   kColorRGBValue(0x717171)
#define kTextColor_229  kColorRGBValue(0xe5e5e5)
#define kTextColor_139  kColorRGBValue(0x8b8b8b)
#define kTextColor_70   kColorRGBValue(0x464545)
#define kTextColor_180  kBgColor_180

#define kBgColor_248    kColorRGBValue(0xf8f8f8)
#define kBgColor_237    kColorRGBValue(0xededed) // white
#define kBgColor_238    kNaTitleColor_237 // red
#define kBgColor_180    kColorRGBValue(0xb4b4b4)

#pragma mark - 布局样式

#define kCellLeftSpace       15
#define kCellRightSpace      15
#define kCellRightTitleSapce 13

#pragma mark - 系统属性
// ** 获取window
#define kCommonWindow [UIApplication sharedApplication].keyWindow


#pragma mark - 适配值
// ** iphonex设配判断

// ** 获取statusBar高度
#define kStatusBarHeight (IS_PhoneXAll ? 44.f : 20.f)

#define kNavigationHeight (IS_PhoneXAll ? 88.f : 64.f)

#define kiPhoneX_Mas_top(x) (IS_PhoneXAll ? (44.f + x) : x)
#define kiPhoneX_Mas_buttom(x) (IS_PhoneXAll ? (-34.f - x) : -x)
#define kSafeAreaInsetsBottom (IS_PhoneXAll ? 34 : 0)
#define kSafeAreaInsetsTop (IS_PhoneXAll ? 44 : 0)

#define kSafe_Mas_Bottom(x) (IS_PhoneXAll ? (34.f + x) : x)
#define kSafe_Mas_Top(x) (IS_PhoneXAll ? (44.f + x) : x)

#pragma mark - 时长
#define kToastDuration 2.0


#define kShowToastView(title,view) showToastView(title,view)

#endif /* BYCommonUIDefine_h */

