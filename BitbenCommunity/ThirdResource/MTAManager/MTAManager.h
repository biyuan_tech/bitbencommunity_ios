//
//  MTAManager.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/8.
//  Copyright © 2018 币本. All rights reserved.
//


#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,MTAType) {
    // 登录
    MTATypeLoginPageLogin,              /**< 登录页面-登录按钮*/
    MTATypeLoginPageRegister,           /**< 登录页面-注册按钮*/
    MTATypeLoginPageFindPwd,            /**< 登录页面-找回密码*/
    MTATypeLoginPageWechatLogin,        /**< 登录页面-微信登录*/
    MTATypeRegisterPageGotoLogin,       /**< 注册页面-立即登录*/
    MTATypeFindPwdPageNext,             /**< 找回密码页面-下一步*/
    MTATypeFindPwdPageSetting,          /**< 找回密码页面-设置密码*/

    // tab
    MTATypeTabLive,             /**< tab_直播*/
    MTATypeTabGuandian,         /**< tab_观点*/
    MTATypeTabAdd,              /**< tab_添加*/
    MTATypeTabAddLongArticle,   /**< tab_添加_长文*/
    MTATypeTabAddShortArticle,  /**< tab_添加_短图文*/
    MTATypeTabAddLive,          /**< tab_添加_直播*/
    MTATypeTabMsg,              /**< tab_消息*/
    MTATypeTabCenter,           /**< tab_个人中心*/
    
    
    // 热门推荐
    MTATypeHotBanner,           /**< hot_banner*/
    MTATypeHotLiveYuyue,        /**< 直播预约*/
    MTATypeHotLiveDetail,       /**< 直播详情*/
    MTATypeHotLiveBack,         /**< 直播返回*/
    MTATypeHotLiveShare,        /**< 直播分享*/
    MTATypeHotLiveJianjie,      /**< 直播简介*/
    MTATypeHotLiveLive,         /**< 直播直播*/
    MTATypeHotLiveDianping,     /**< 直播点评*/
    
    MTATypeHotLiveHomeHot,          /**< 热门推荐*/
    MTATypeHotLiveHomeJianjie,      /**< 直播简介*/
    MTATypeHotLiveHomeLive,         /**< 直播直播*/
    MTATypeHotLiveHomeDianping,     /**< 直播点评*/

    MTATypeVoiceLiveDetail,     /**< 音频直播详情*/
    MTATypePlayerLiveDetail,    /**< 视频直播详情*/
    MTATypeMyLinkDetail,        /**< 我关注的详情*/
    MTATypeLiveShang,           /**< 直播赏*/
    MTATypeLiveDing,            /**< 直播顶*/
    MTATypeLiveCai,             /**< 直播踩*/
    MTATypeLiveComment,         /**< 直播评论*/
    MTATypeLiveSend,            /**< 直播发送*/
    
    // 邀请
    MTATypeInvitation,          /**< 邀请*/
    
    // 段图文
    MTATypeShortChooseHuati,    /**< 选择话题*/
    MTATypeShortDelHuati,       /**< 删除话题*/
    MTATypeShortChooseImg,      /**< 选择图片*/
    MTATypeShortChooseJinghao,  /**< 选择井号按钮*/
    MTATypeShortRelease,        /**< 发布*/
    MTATypeShortShare,          /**< 分享*/
    
    // 直播
    MTATypeLiveRelease,         /**< 直播-提交*/
    MTATypeLiveEdit,            /**< 编辑介绍页*/
    MTATypeLiveGoin,            /**< 进入直播*/
    MTATypeLiveGoinDetail,      /**< 直播进入简介*/
    MTATypeLiveGoinDianping,    /**< 直播简介*/
    
};

@interface MTAManager : FetchModel

+(void)mtainitWithApp;

+(void)event:(MTAType)type params:( NSDictionary * __nullable)params;


+ (void)profileSignInWithId:(NSString *)user_id;
+ (void)profileSignOut;

@end

NS_ASSUME_NONNULL_END
