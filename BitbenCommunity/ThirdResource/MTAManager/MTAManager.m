//
//  MTAManager.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/8.
//  Copyright © 2018 币本. All rights reserved.
//

#import "MTAManager.h"
#import <UMCommon/UMCommon.h>
#import <UMAnalytics/MobClick.h>
@implementation MTAManager

#pragma mark - MTA初始化
+(void)mtainitWithApp{
    NSString *chanel = [NSString stringWithFormat:@"%@%@",@"appstore",[Tool appVersion]];
    [UMConfigure initWithAppkey:@"5be5246ff1f5569146000588" channel:chanel];
}

+(void)event:(MTAType)type params:( NSDictionary *__nullable)params{
    // 登录模块
    if (type == MTATypeLoginPageLogin){
        [MTAManager eventDic:@"登录页面-登录按钮"info:params];
    } else if (type == MTATypeLoginPageRegister){
        [MTAManager eventDic:@"登录页面-注册按钮"info:params];
    } else if (type == MTATypeLoginPageFindPwd){
        [MTAManager eventDic:@"登录页面-找回密码"info:params];
    } else if (type == MTATypeLoginPageWechatLogin){
        [MTAManager eventDic:@"登录页面-微信登录"info:params];
    } else if (type == MTATypeRegisterPageGotoLogin){
        [MTAManager eventDic:@"注册页面-立即登录"info:params];
    } else if (type == MTATypeFindPwdPageNext){
        [MTAManager eventDic:@"找回密码页面-下一步"info:params];
    } else if (type == MTATypeFindPwdPageSetting){
        [MTAManager eventDic:@"找回密码页面-设置密码"info:params];
    }
    
    // TAB
    
    
    // tab
    if (type == MTATypeTabLive){
        [MTAManager eventDic:@"Tab_直播"info:params];
    } else if (type == MTATypeTabGuandian){
        [MTAManager eventDic:@"Tab_观点"info:params];
    } else if (type == MTATypeTabAdd){
        [MTAManager eventDic:@"Tab_添加"info:params];
    } else if (type == MTATypeTabAddLongArticle){
        [MTAManager eventDic:@"Tab_添加_长文"info:params];
    } else if (type == MTATypeTabAddShortArticle){
        [MTAManager eventDic:@"Tab_添加_短文" info:params];
    } else if (type == MTATypeTabAddLive){
        [MTAManager eventDic:@"Tab_添加_直播" info:params];
    } else if (type == MTATypeTabMsg){
        [MTAManager eventDic:@"Tab_消息" info:params];
    } else if (type == MTATypeTabCenter){
        [MTAManager eventDic:@"Tab_个人中心" info:params];
    }
    
    // Hot
    
    if (type == MTATypeHotBanner){
        [MTAManager eventDic:@"首页banner" info:params];
    } else if (type == MTATypeHotLiveYuyue){
        [MTAManager eventDic:@"直播预约" info:params];
    } else if (type == MTATypeHotLiveDetail){
        [MTAManager eventDic:@"直播详情" info:params];
    } else if (type == MTATypeHotLiveBack){
        [MTAManager eventDic:@"直播返回" info:params];
    } else if (type == MTATypeHotLiveShare){
        [MTAManager eventDic:@"直播分享" info:params];
    } else if (type == MTATypeHotLiveJianjie){
        [MTAManager eventDic:@"直播简介" info:params];
    } else if (type == MTATypeHotLiveLive){
        [MTAManager eventDic:@"直播直播" info:params];
    } else if (type == MTATypeHotLiveDianping){
        [MTAManager eventDic:@"直播点评" info:params];
    } else if (type == MTATypeHotLiveHomeHot){
        [MTAManager eventDic:@"热门推荐" info:params];
    } else if (type == MTATypeVoiceLiveDetail){
        [MTAManager eventDic:@"音频直播详情" info:params];
    } else if (type == MTATypePlayerLiveDetail){
        [MTAManager eventDic:@"视频直播详情" info:params];
    } else if (type == MTATypeMyLinkDetail){
        [MTAManager eventDic:@"我关注的详情" info:params];
    } else if (type == MTATypeLiveShang){
        [MTAManager eventDic:@"直播赏" info:params];
    } else if (type == MTATypeLiveDing){
        [MTAManager eventDic:@"直播顶" info:params];
    } else if (type == MTATypeLiveCai){
        [MTAManager eventDic:@"直播踩" info:params];
    } else if (type == MTATypeLiveComment){
        [MTAManager eventDic:@"直播评论" info:params];
    } else if (type == MTATypeLiveSend){
        [MTAManager eventDic:@"直播发送" info:params];
    } else if (type == MTATypeInvitation){
        [MTAManager eventDic:@"邀请" info:params];
    } else if (type == MTATypeShortChooseHuati){
        [MTAManager eventDic:@"选择话题" info:params];
    } else if (type == MTATypeShortDelHuati){
        [MTAManager eventDic:@"删除话题" info:params];
    } else if (type == MTATypeShortChooseImg){
        [MTAManager eventDic:@"选择图片" info:params];
    } else if (type == MTATypeShortChooseJinghao){
        [MTAManager eventDic:@"发布短图文井号" info:params];
    } else if (type == MTATypeShortRelease){
        [MTAManager eventDic:@"短图文 - 发布" info:params];
    } else if (type == MTATypeLiveRelease){
        [MTAManager eventDic:@"直播 - 提交" info:params];
    } else if (type == MTATypeLiveEdit){
        [MTAManager eventDic:@"直播 - 编辑介绍页" info:params];
    } else if (type == MTATypeLiveGoin){
        [MTAManager eventDic:@"直播 - 进入直播" info:params];
    } else if (type == MTATypeLiveGoinDetail){
        [MTAManager eventDic:@"直播 - 进入直播简介" info:params];
    } else if (type == MTATypeLiveGoinDianping){
        [MTAManager eventDic:@"直播 - 进入直播点评" info:params];
    }
}

+(void)eventDic:(NSString *)event info:(NSDictionary *)params{
    [MobClick event:event attributes:params];
}

+ (void)profileSignInWithId:(NSString *)user_id{
    [MobClick profileSignInWithPUID:user_id];
}
+ (void)profileSignOut{
    [MobClick profileSignOff];
}

@end
