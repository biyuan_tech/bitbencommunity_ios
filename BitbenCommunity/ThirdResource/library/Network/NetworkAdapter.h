//
//  NetworkAdapter.h
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//
// 网络适配器
#import <Foundation/Foundation.h>
#import "WebSocketReceiveMainModel.h"
#import "WebSocketConnection.h"

#define PDBizErrorDomain @"PDBizErrorDomain"

@protocol NetworkAdapterDelegate <NSObject>

@optional

-(void)notLoginNeedLoginManager;                           // 没有登录，需要登录
-(void)btnEnableStatusManager:(UIButton *)currentButton;    // 按钮仅仅点击1次。不能多次点击
@end

@protocol NetworkAdapterSocketDelegate <NSObject>

@optional
-(void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data;
-(void)webSocketDidConnectedErrorTimes:(NSInteger)times connectType:(SocketConnectType)connectType errorString:(NSString *)err;
-(void)webSocketDidConnectedWithSocket:(WebSocketConnection *)webSocketConnection connectedStatus:(SocketConnectType)connectedStatus;


@end

@interface NetworkAdapter : NSObject<WebSocketConnectionDelegate>

+(instancetype)sharedAdapter;                                                               /**< 单例*/

@property (nonatomic,weak)id<NetworkAdapterDelegate> delegate;                        /**< */
@property (nonatomic,weak)id<NetworkAdapterSocketDelegate> socketDelegate;                        /**< */


#pragma mark - 短链接
-(void)fetchWithPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(void(^)(BOOL isSucceeded,id responseObject, NSError *error))block;
+ (NSString*)dictionaryToJson:(NSDictionary *)dic;
-(void)actionGetServerInterfaceBlock:(void(^)(BOOL isSuccessed))block;
// tableView 上啦加载 下拉刷新
@property (nonatomic,strong)NSMutableArray *scrollTableViewMutableArr;                  /**< 上拉 */


#pragma mark - webSocket
@property (nonatomic,strong)WebSocketConnection *mainRootSocketConnection;                   /**< 绑定的长连接*/
@property (nonatomic,strong)NSMutableArray *socketConnectMutableArr;

@property (nonatomic,strong)WebSocketConnection *testConnect1;
@property (nonatomic,strong)WebSocketConnection *testConnect2;
@property (nonatomic,strong)WebSocketConnection *testConnect3;
@property (nonatomic,strong)WebSocketConnection *testConnect4;
@property (nonatomic,strong)WebSocketConnection *testConnect5;
@property (nonatomic,strong)WebSocketConnection *testConnect6;
@property (nonatomic,strong)WebSocketConnection *testConnect7;
@property (nonatomic,strong)WebSocketConnection *testConnect8;
@property (nonatomic,strong)WebSocketConnection *testConnect9;
@property (nonatomic,strong)WebSocketConnection *testConnect10;



-(WebSocketConnection *)webSocketConnection:(WebSocketConnection *)webSocketConnection host:(NSString *)host port:(NSInteger)port;
-(void)webSocketCloseConnectionWithSoceket:(WebSocketConnection *)webSocketConnection;
-(void)webSocketFetchModelWithRequestParams:(NSDictionary *)requestParams socket:(WebSocketConnection *)webSocketConnection;

#pragma mark - 临时调用。用来使按钮无法多次点击效果
@property (nonatomic,strong)NSMutableArray *tempButtonMutableArr;
@property (nonatomic,strong)UIButton *currentTempButton;
-(void)btnTempDelegateManager:(UIButton *)button;
@end

