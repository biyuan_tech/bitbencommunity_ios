//
//  RequestSerializer.m
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//
// 设置请求头
#import "RequestSerializer.h"
#import "APNSTool.h"

@implementation RequestSerializer

-(NSMutableURLRequest *)requestWithMethod:(NSString *)method URLString:(NSString *)URLString parameters:(id)parameters error:(NSError *__autoreleasing *)error{
    NSMutableURLRequest *request = [super requestWithMethod:method URLString:URLString parameters:parameters error:error];
    if ([AccountModel sharedAccountModel].token.length){
        [request setValue:[AccountModel sharedAccountModel].token forHTTPHeaderField:@"token"];
    }
    [request setValue:@"ios" forHTTPHeaderField:@"type"];
    
    if ([APNSTool shareInstance].deviceToken.length){
        [request setValue:[APNSTool shareInstance].deviceToken forHTTPHeaderField:@"sn"];
    }
    return request;
}


@end
