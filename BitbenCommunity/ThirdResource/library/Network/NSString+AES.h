//
//  NSString+AES.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (AES)
/**< 加密方法 */
- (NSString*)aci_encryptWithAES;

/**< 解密方法 */
- (NSString*)aci_decryptWithAES;
@end

NS_ASSUME_NONNULL_END
