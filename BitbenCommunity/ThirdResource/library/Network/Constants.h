
//
//  Constants.h
//  BBFinance
//
//  Created by 裴烨烽 on 2018/6/12.
//  Copyright © 2018年 BasicPod. All rights reserved.
//

#ifndef Constants_h
#define Constants_h


// 【USER Detault】
#define USERDEFAULTS     [NSUserDefaults standardUserDefaults]
#define NOTIFICENTER     [NSNotificationCenter defaultCenter]

#define ACCOUNT_ID      [AccountModel sharedAccountModel].account_id
#define LOGIN_MODEL     [AccountModel sharedAccountModel].loginServerModel
#define CURRENT_VC      [BYTabbarViewController sharedController].currentController


// 【System】
#define IOS_Version [UIDevice currentDevice].systemVersion.floatValue
#define IS_IOS7_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 6.99)
#define IS_IOS8_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 7.99)
#define IS_IOS9_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 8.99)
#define IS_IOS10_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 9.99)
#define IS_IOS11_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 10.99)




//#define IS_iPhoneX (@available(iOS 11.0, *) ? [[[UIApplication sharedApplication] delegate] window].safeAreaInsets.bottom > 0.0 : NO )

#define IS_iPhoneX ([UIScreen mainScreen].bounds.size.height == 812 || [UIScreen mainScreen].bounds.size.height == 896)



#define IS_IPHONE_X ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)

#define IS_IPHONE_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)

#define IS_IPHONE_Xs ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)

#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)









#define IS_iPhone_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size)  : NO)
#define isPad ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#define IS_IPHONE_Xr1 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
#define IS_iPhone_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) : NO)

//#define IS_PhoneXAll (IS_iPhoneX || IS_iPhone_Xr || IS_iPhone_Xs_Max)
#define IS_PhoneXAll (kScreenBounds.size.height >= 812 || kScreenBounds.size.width >= 812)

#define iPhone5  ([UIScreen mainScreen].bounds.size.height == 568)
#define iPhone6  ([UIScreen mainScreen].bounds.size.height == 667)
#define iPhone6Plus  ([UIScreen mainScreen].bounds.size.height == 736)
#define ipadMini2  ([UIScreen mainScreen].bounds.size.height == 1024)
#define isMoreThanIphone5 ([UIScreen mainScreen].bounds.size.height > 568)

// 【屏幕尺寸】
#define kScreenBounds               [[UIScreen mainScreen] bounds]

// 【Style】
#define RGB(r, g, b,a)    [UIColor colorWithRed:(r)/255. green:(g)/255. blue:(b)/255. alpha:a]
#define BACKGROUND_VIEW_COLOR        RGB(239, 239, 244,1)               // 主题色
#define NAVBAR_COLOR RGB(255, 255, 255,1)                               // navBar

// 【nav】
#define BAR_BUTTON_FONT       [UIFont systemFontOfSize:14.]
#define BAR_MAIN_TITLE_FONT   [UIFont boldSystemFontOfSize:16.]
#define BAR_SUB_TITLE_FONT    [UIFont systemFontOfSize:12.]
#define BAR_TITLE_PADDING_TOP -3.
#define BAR_TITLE_MAX_WIDTH   LCFloat(230)

// 【log】
// 【Log 方式】
#ifdef DEBUG
#   define PDLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define PDLog(...)
#endif

// 【高德地图】
#define mapIdentify @"995c2d5470ac90fd6b71a216e83aa48d"
#define beeMapIdentify @"f1725b00611c5667e42a56b49fb2a51b"

// 【百川聊天】
#define AliChattingIdentify @"25047349"                                     // 百川chattingAPI
#define AliPush @"3448665"
#define AliIMKey @"25047349"
#define AliIMSecret @"b46fffc1600f79d7846461c6275989a7"

// 【阿里推送】
#define kAPNS_KEY    @"25426209" // 23330943
#define kAPNS_SECRET @"0fe4b6b6d2504973848b5ec7d21e663e" // 1a87d709667be873ead5f738df4e1c7c

#define AliPushKey @"25047349"
#define AliPushAppSecret @"b46fffc1600f79d7846461c6275989a7"

// 【是否layer】
#define View_Layer @"View_Layer"                    // 是否边框

// 【debug状态下的地址】
#define testNet_address @"testNet_address"          // 端口号
#define testNet_port @"testNet_port"                //port

// 【用户自定义配置】
#define mainStyleColor @"mainStyleColor"
#define UserDefaultCustomerType    @"CustomerType"                             // 游客类型
#define UserDefaultUserToken @"UserDefaultUserToken"
#define UserDefaultCustomerMemberId @"CustomerMemberId"
#define UserDetailtCustomeSocketAddress @"UserDetailtCustomeSocketAddress"      // socket的地址
#define UserDetailtCustomeSocketPort @"UserDetailtCustomeSocketPort"            // socket的端口

// 【三方分享】
#define WeChatAppID         @"wx9c66ca64ff5ca106"
#define WeChatAppSecret     @"6729310ffbdf0178bd39e3a368afc85b"

#define QQAppID             @"1105501406"
#define QQAppAppSecret      @"a0zBvC0I5boEIliE"

#define WeiBoAPPID          @"2715771338"
#define WeiBoAppSecret      @"d51c6c4209158fdfc312aca3408ff1a0"
#define WeiBoRedirectUri    @"http://www.pandaol.com"

// 【阿里OSS】
#define ossAccessKey    @"LTAIcQxwreHhXLrt"
#define ossSecretKey @"CMY8mTxvbgFR4SLyJwxQsFHUZtfD65"
#define ossEndpoint   @"http://oss-cn-hangzhou.aliyuncs.com"
#define ossBucketName @"bee-tv"
#define aliyunOSS_BaseURL @"http://bee-tv.oss-cn-hangzhou.aliyuncs.com/"


// 【分页每页数量】
static NSInteger cutPageNum = 10;

// 腾讯云
#define QQLiveID @"1400049832"
#define QQAccountType @"19334"

// 腾讯云实时音视频
#define kTRTCSDKAppId 1400219696
#define kTRTCAppId 1257261118
#define kTRTCBizid 29266

// 【渠道】
#define Channel @"AppStore"

// 【scheme跳转】
#define schemeAction_Live @"bitben://live"                  // 直播
#define schemeAction_Article @"bitben://article"            // 文章


static NSString *testURL = @"http://d.ifengimg.com/mw600/img1.ugc.ifeng.com/newugc/20170410/9/wemedia_h5/e12537bd4e740e208ccfc048754325229ac3e465_size1235_w3000_h2002.jpeg";


#define LoginType @"loginType"              // 登录类型
#define LoginAccount @"LoginAccount"                // 登录key
#define LoginPassword @"LoginPassword"                // 登录密码
#define LoginAvatar @"loginAvatar"                  // 登录头像


static NSString *const LoginNotificationWithLoginSuccessed = @"LoginNotificationWithLoginSuccessed";  /**< 登录信息通知*/


#endif /* Constants_h */
