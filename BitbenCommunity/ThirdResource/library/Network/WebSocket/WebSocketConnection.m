//
//  WebSocketConnection.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "WebSocketConnection.h"


typedef NS_ENUM(NSInteger,ConnectTimerType) {
    ConnectTimerTypePing,                   /**< 表示发送了ping*/
    ConnectTimerTypePong,                   /**< 表示收到了pong*/
};

@interface WebSocketConnection()<SRWebSocketDelegate>
@property (nonatomic,strong) NSTimer *connectTimer;               /**< 计时器*/
@property (nonatomic,assign)NSInteger connectTimes;
@property (nonatomic,assign)NSInteger connectTimeDistance;        /**< 心跳包计时器*/
@property (nonatomic,assign)ConnectTimerType connectTimerType;    /**< 心跳包*/

@property (nonatomic,copy)NSString *hostName;
@property (nonatomic,assign)NSInteger port;

@end

@implementation WebSocketConnection

-(instancetype)init{
    self = [super init];
    if (self){
        self.webSocket.delegate = nil;
        [self.webSocket close];
        self.connectTimeDistance = 20;
        self.socketConnectType = SocketConnectTypeNotConnected;
    }
    return self;
}

#pragma mark - 链接到服务器
- (void)webSocketConnectWithHost:(NSString *)hostName port:(NSInteger)port{
    if (hostName.length){
        self.hostName = hostName;
        self.port = port;
    }
    
    NSString *requestURL = [NSString stringWithFormat:@"ws://%@:%li/chat",hostName,(long)port];
    self.webSocket = [[SRWebSocket alloc]initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:requestURL]]];
    self.webSocket.delegate = self;

    [self.webSocket open];
}

#pragma mark 断开服务器
- (void)webSocketDisconnect{
    self.webSocket.delegate = nil;
    [self.webSocket close];
    self.webSocket = nil;
    
    // 关闭计时器
    if (self.connectTimer){
        [self.connectTimer invalidate];                          // 计时器断开
    }
    // 客户端主动断开
    self.socketConnectType = SocketConnectTypeClosedByCustomer;
    PDSocketLog(@"【webSocket 用户主动断开】");
}

#pragma mark SRWebSocketDelegate
-(void)webSocketDidOpen:(SRWebSocket *)webSocket{
    PDSocketLog(@"【webSocket已链接上】");
    self.socketConnectType = SocketConnectTypeOpen;
    
    // 发送pong 心跳
    self.connectTimerType = ConnectTimerTypePong;

    
    // 心跳包
    self.connectTimes = 0;
    if (!self.connectTimer){
        self.connectTimer = [NSTimer scheduledTimerWithTimeInterval:self.connectTimeDistance target:self selector:@selector(longConnectToSocket) userInfo:nil repeats:YES];
        [self.connectTimer fire];
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(webSocketDidConnectedWithSocket:)]) {
        [_delegate webSocketDidConnectedWithSocket:webSocket];
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error{
    PDSocketLog(@":( Websocket Failed With Error %@", error);
    
    self.socketConnectType = SocketConnectTypeClosedByServer;
    self.webSocket = nil;
}


#pragma mark - 返回数据
- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    if ([message isKindOfClass:[NSString class]]){
        NSString *backInfo = (NSString *)message;
        if ([backInfo isEqualToString:@"pong"]){
            PDSocketLog(@"心跳pong");
            self.connectTimerType = ConnectTimerTypePong;
            return;
        }
    }
    
    
    if (_delegate && [_delegate respondsToSelector:@selector(webSocket:didReceiveData:)]) {
        [_delegate webSocket:webSocket didReceiveData:message];
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean; {
    PDSocketLog(@"WebSocket closed【关闭】");
    self.socketConnectType = SocketConnectTypeClosedByServer;
//    [self webSocketDisconnect];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload; {
    PDSocketLog(@"Websocket received pong");
}


#pragma mark - 发送信息
- (void)webSocketWriteData:(NSString *)info{
    PDSocketLog(@"发送");
    [self.webSocket send:info];
}


#pragma mark - 心跳包
-(void)longConnectToSocket{
    PDSocketLog(@"心跳ping");
    NSString *longConnect = @"ping";
    [self webSocketWriteData:longConnect];
    if (self.connectTimerType == ConnectTimerTypePong){         // 表示收到的pong
        self.connectTimerType = ConnectTimerTypePing;
    } else {                                                    // 表示没有收到pong
        // 报错+1
        self.connectTimes += 1;
    }
    
    // 如果超过3次，短线重连
    if (self.connectTimes >= 3){
        // 向外抛出错误
        self.socketConnectType = SocketConnectTypeReconnection;
        if (self.delegate && [self.delegate respondsToSelector:@selector(webSocketDidConnectedErrorTimes:connectType:errorString:)]) {
            NSString *errStr = [NSString stringWithFormat:@"与服务器断开连接,正在第%li次重新连接",(long)self.connectTimes - 2];
            [self.delegate webSocketDidConnectedErrorTimes:self.connectTimes - 2 connectType:SocketConnectTypeReconnection errorString:errStr];
            PDSocketLog(@"与服务器断开连接");
        }
        
        [self webSocketConnectWithHost:self.hostName port:self.port];

    }
    
    if (self.connectTimes > 10){
        //
        if (self.delegate && [self.delegate respondsToSelector:@selector(webSocketDidConnectedErrorTimes:connectType:errorString:)]) {
            NSString *errStr = @"与服务器已经断开连接";
            PDSocketLog(@"与服务器已经断开连接");
            [self.delegate webSocketDidConnectedErrorTimes:self.connectTimes - 2 connectType:SocketConnectTypeClosedByServer errorString:errStr];
        }
        
        [self webSocketDisconnect];
    }
}


// 心跳包的时间
-(NSInteger)connectTimeDistance{
    return 10;
}
@end
