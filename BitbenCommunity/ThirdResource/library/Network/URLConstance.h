//
//  URLConstance.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#ifndef URLConstance_h
#define URLConstance_h


// 【接口环境】
#ifdef DEBUG        // 测试
//#define JAVA_Host @"47.97.211.170"
//#define JAVA_Port @"443"
#define JAVA_Host kServiceHost
//#define JAVA_Host @"192.168.31.61"
#define JAVA_Port kSercicePort
#define JAVA_HTTP @"http"

#else               // 线上
#define JAVA_Host kServiceHost
#define JAVA_Port kSercicePort
#define JAVA_HTTP @"http"
#endif

#define Article_Guide @"507942021026021376"

// 【配置】
static NSString *const ErrorDomain = @"ErrorDomain";
static BOOL NetLogAlert = YES;                                                  /**< 网络输出*/
static NSInteger TimeoutInterval = 15;                                          /**< 短链接失效时间*/

// 【长链接】
static BOOL isHeartbeatPacket = NO;                                            /**< 是否心跳包*/
static BOOL isSocketLogAlert = NO;                                             /**< 是否socket*/

#ifdef DEBUG
#define PDSocketDebug
#endif

#ifdef PDSocketDebug
#define PDSocketLog(format, ...) NSLog(format, ## __VA_ARGS__)
#else
#define PDSocketLog(format, ...)
#endif

// 【server】
static NSString *get_server_list = @"gate/get_server_list";                             // 网关
static NSString *get_base_data = @"gate/get_base_data";                                 // 网关

// 【login】
static NSString *login_register = @"account/register";                                 // 1.注册
static NSString *login_login = @"account/login";                                       // 1.登录
static NSString *login_other_login = @"account/other_login";                           // 三方登录
static NSString *login_getAccount = @"account/get_account_by_id";                      // 根据账户ID获取账户信息
static NSString *login_getAccountByPhone = @"account/get_account_by_phone";            // 根据账户手机获取账户信息
static NSString *login_updatePwd = @"account/update_password_by_phone";                // 修改密码
static NSString *login_updatePhone = @"account/update_phone";                          // 修改手机号
static NSString *login_SMS = @"account/send_register_code";                            // 注册 发送验证码
static NSString *login_send_password_code = @"account/send_password_code";
static NSString *login_validateSMS = @"account/validate_sms_code";                     // 验证验证码
static NSString *login_updateAvatar = @"account/update_head_img";                      // 修改头像
static NSString *center_attendance = @"account/attendance";                            // 签到领BBT
static NSString *send_sms_code = @"account/send_sms_code";                             // 发送验证码
static NSString *login_logout = @"account/logout";                                     // 退出登录


// 【account】
static NSString *get_user_and_account = @"user/get_user_and_account";                          // 获取当前账户信息
static NSString *update_user_and_account = @"user/update_user_and_account";                 // 修改信息
static NSString *get_user_count = @"user/get_user_count";                                // 获取用户个人中心数据统计等相关信息
static NSString *create_attention = @"user/create_attention";                  // 关注
static NSString *delete_attention = @"user/delete_attention";            // 取消关注
static NSString *page_attention = @"user/page_attention";                      // 获取我关注的用户
static NSString *page_fans = @"user/page_fans";                                // 获取我的粉丝
static NSString *get_my_invitation = @"user/get_my_invitation";                       // 获取我的邀请
static NSString *page_invitation_history = @"user/page_invitation_history";         // 我的邀请历史
static NSString *get_invitation_url = @"user/get_invitation_url";             // 分享
static NSString *create_comment = @"user/create_comment";                   // 添加评论
static NSString *page_comment = @"user/page_comment";
static NSString *page_hot_comment = @"user/page_hot_comment";
static NSString *create_comment_operate = @"user/create_comment_operate";          // 顶踩赞;
static NSString *create_interaction = @"user/create_interaction";
static NSString *get_topic_list = @"user/get_topic_list";
static NSString *user_certification_confirm = @"user/user_certification_confirm";
static NSString *get_certification_confirm = @"user/get_certification_confirm";
static NSString *create_report = @"user/create_report";
static NSString *personal_task_list = @"user/personal_task_list";
static NSString *get_personal_fund = @"user/get_personal_fund";
static NSString *receive_task = @"user/receive_task";
static NSString *receive_fund = @"user/receive_fund";
static NSString *share_theme = @"user/share_theme";
static NSString *page_user_history = @"user/page_user_history";
static NSString *get_energy = @"user/get_energy";
static NSString *get_certification_status = @"user/get_certification_status";
static NSString *user_member_center = @"user/user_member_center";
static NSString *order_member_product = @"user/order_member_product";
static NSString *quick_auth = @"user/quick_auth";
static NSString *get_quick_auth_status = @"user/get_quick_auth_status";
static NSString *page_invitation_rank = @"user/page_invitation_rank";
static NSString *page_invitation_member_history = @"user/page_invitation_member_history";
static NSString *band_member_invitation_code = @"user/band_member_invitation_code";
static NSString *get_ali_face_verify_token = @"user/get_ali_face_verify_token";
static NSString *insert_person_cert = @"user/insert_person_cert";
static NSString *page_columnist = @"user/page_columnist";
static NSString *page_information = @"user/page_information";
static NSString *get_information_date = @"user/get_information_date";
static NSString *get_upload_token = @"user/get_upload_token";
static NSString *page_bulletin= @"user/page_bulletin";
static NSString *create_bulletin_operate = @"user/create_bulletin_operate";
static NSString *get_bulletin = @"user/get_bulletin";
static NSString *create_bulletin = @"user/create_bulletin";
static NSString *get_user_cert_type = @"user/get_user_cert_type";

// 【Article】
static NSString *article_create_article = @"article/create_article";                    // 添加文章
static NSString *article_get_article = @"article/get_article";                                  // 获取文章
static NSString *article_page_article = @"article/page_article";
static NSString *page_topic_article = @"article/page_topic_article";
static NSString *page_author_article = @"article/page_author_article";
static NSString *publish_article = @"article/publish_article";                          // 立即发布
static NSString *page_attention_article = @"article/page_attention_article";                          // 立即发布
static NSString *cancel_timing_article = @"article/cancel_timing_article";              // 取消定时发布
static NSString *get_article_rank = @"article/get_article_rank";                          // 文章排行榜
static NSString *get_mine_rank = @"article/get_mine_rank";                          // 排行榜

// 【Live】
static NSString *live_ppt_main_info = @"live/get_live_room";                         // 获取直播间信息
static NSString *live_get_live_record = @"live/get_live_record";          // 顶踩赞;
static NSString *page_appointment = @"live/page_appointment";          // 顶踩赞;
static NSString *get_appointment = @"live/get_appointment";          // 顶踩赞;

// 【message】
static NSString *center_page_message = @"message/page_message";                          // 我的消息
static NSString *center_cleanMessage = @"message/delete_all_message";                    // 清空我的消息
static NSString *message_get_message = @"message/get_message";                    // 清空我的消息
static NSString *message_message_read = @"message/update_message_read";                           // 我的消息已读
static NSString *message_readmessage = @"message/read_message";                           // 我的消息已读
static NSString *message_get_message_center = @"message/get_message_center";                           // 我的消息已读


// 【finance】
static NSString *create_account = @"finance/create_account";                            /**< 创建区块链钱包*/
static NSString *get_wallet_status = @"finance/get_wallet_status";
static NSString *get_balance = @"finance/get_balance";                                  /**< 获取余额*/
static NSString *get_transaction = @"finance/get_transaction";                          /**< 交易详情*/
static NSString *get_recharge_list = @"finance/get_recharge_list";                        /**< 交易记录*/
static NSString *get_service_charge = @"finance/get_service_charge";
static NSString *offline_transaction = @"finance/offline_transaction";                  /**< 发起交易离线签名 */
static NSString *check_address_legal = @"finance/check_address_legal";
static NSString *valid_block_password = @"finance/valid_block_password";
static NSString *count_block_password = @"finance/count_block_password";
static NSString *update_block_password = @"finance/update_block_password";
static NSString *retrieve_block_password = @"finance/retrieve_block_password";
static NSString *password_certification_confirm = @"finance/password_certification_confirm";
static NSString *insert_address_book = @"finance/insert_address_book";
static NSString *delete_address_book = @"finance/delete_address_book";
static NSString *address_book_list = @"finance/address_book_list";
static NSString *pay_member_product = @"finance/pay_member_product";
static NSString *finance_get_finance = @"finance/get_finance";                            // 获取注册奖励
static NSString *center_page_finance_history = @"finance/page_finance_history";          // 获取我的资金流水历史
static NSString *get_my_finance = @"finance/get_my_finance";
static NSString *get_my_finance_history = @"finance/get_my_finance_history";
static NSString *convert_finance = @"finance/convert_finance";
static NSString *get_cash_info = @"finance/get_cash_info";
static NSString *send_cash = @"finance/send_cash";
static NSString *get_my_cash_history = @"finance/get_my_cash_history";
static NSString *unlock_fund = @"finance/unlock_fund";
static NSString *get_unlock_fund_history = @"finance/get_unlock_fund_history";








// 【block】



//static NSString *send_transaction = @"send_transaction";                        /**< 转账*/



static NSString *register_delegate = @"http://m.bitben.com/statement";
static NSString *about_delegate = @"http://m.bitben.com/aboutbbt";
static NSString *wallet_user_delegate = @"http://m.bitben.com/wallet/pact";
static NSString *wallet_about = @"http://m.bitben.com/wallet/about";
static NSString *wallet_help = @"http://m.bitben.com/wallet/help";
static NSString *help = @"http://m.bitben.com/help";
static NSString *page1 = @"http://m.bitben.com/others/page1";            /**< 邀请好友*/
static NSString *page2 = @"http://m.bitben.com/others/page2";            /**< 会员权益*/
static NSString *page3 = @"http://m.bitben.com/others/page3";            /**< 我的资产*/

// 【assets】



// 【情报】




#endif /* URLConstance_h */

