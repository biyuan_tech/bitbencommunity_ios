//
//  NetworkAdapter.m
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

// 网络适配器
#import "NetworkAdapter.h"
#import "NetworkEngine.h"
#import "WebSocketReceiveMainModel.h"
#import "NSString+AES.h"

@implementation NetworkAdapter

+(instancetype)sharedAdapter{
    static NetworkAdapter *_sharedAdapter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAdapter = [[NetworkAdapter alloc] init];
    });
    return _sharedAdapter;
}

-(NSMutableArray *)scrollTableViewMutableArr{
    if (!_scrollTableViewMutableArr){
        _scrollTableViewMutableArr = [NSMutableArray array];
    }
    return _scrollTableViewMutableArr;
}

#pragma mark - 短链接
-(void)fetchWithPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(void(^)(BOOL isSucceeded,id responseObject, NSError *error))block{
    __weak typeof(self)weakSelf = self;
    
    [self actionGetServerInterfaceBlock:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
             [strongSelf subFetchWithPath:path requestParams:requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:block];
        }
    }];
    
    
    
    
    //
//    if (![path isEqualToString:@"get_server_list"]){
//        [self subFetchWithPath:path requestParams:requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:block];
//        return;
//    }
//    if (![AccountModel sharedAccountModel].serverModel && [AccountModel sharedAccountModel].getServerListTimes == 0){
//        [AccountModel sharedAccountModel].getServerListTimes += 1;
//        __weak typeof(self)weakSelf = self;
//        [self actionGetServerInterfaceBlock:^(BOOL isSuccessed) {
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            if (isSuccessed){
//                [strongSelf subFetchWithPath:path requestParams:requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:block];
//            }
//        }];
//    } else {
//        [self subFetchWithPath:path requestParams:requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:block];
//    }
}

-(void)subFetchWithPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(void(^)(BOOL isSucceeded,id responseObject, NSError *error))block{
    __weak typeof(self)weakSelf = self;
    
    if ([path isEqualToString:get_server_list]){                    // 表示已经调用过getserverlist了
        [AccountModel sharedAccountModel].getServerListTimes += 1;
    }
    
    NetworkEngine *networkEngine;
//    if (![AccountModel sharedAccountModel].serverModel){            // 如果没有的话。就调用服务器端口
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@://%@:%@",JAVA_HTTP,JAVA_Host,JAVA_Port]]];
//    } else {                                                        // 就调用分发的端口
//        NSString *nPath = [NSString stringWithFormat:@"/%@",path];
//        NSString *nIp = @"";
//        NSString *nPort = @"";
//        if ([[AccountModel sharedAccountModel].serverModel.server.MESSAGE_SERVER.protocol_list containsObject:nPath]){
//            nIp = [AccountModel sharedAccountModel].serverModel.server.MESSAGE_SERVER.ip;
//            nPort = [AccountModel sharedAccountModel].serverModel.server.MESSAGE_SERVER.port;
//
//        } else if ([[AccountModel sharedAccountModel].serverModel.server.USER_SERVER.protocol_list containsObject:nPath]){
//            nIp = [AccountModel sharedAccountModel].serverModel.server.USER_SERVER.ip;
//            nPort = [AccountModel sharedAccountModel].serverModel.server.USER_SERVER.port;
//
//        } else if ([[AccountModel sharedAccountModel].serverModel.server.ARTICLE_SERVER.protocol_list containsObject:nPath]){
//            nIp = [AccountModel sharedAccountModel].serverModel.server.ARTICLE_SERVER.ip;
//            nPort = [AccountModel sharedAccountModel].serverModel.server.ARTICLE_SERVER.port;
//
//        } else if ([[AccountModel sharedAccountModel].serverModel.server.ACCOUNT_SERVER.protocol_list containsObject:nPath]){
//            nIp = [AccountModel sharedAccountModel].serverModel.server.ACCOUNT_SERVER.ip;
//            nPort = [AccountModel sharedAccountModel].serverModel.server.ACCOUNT_SERVER.port;
//
//        } else if ([[AccountModel sharedAccountModel].serverModel.server.GATE_SERVER.protocol_list containsObject:nPath]){
//            nIp = [AccountModel sharedAccountModel].serverModel.server.GATE_SERVER.ip;
//            nPort = [AccountModel sharedAccountModel].serverModel.server.GATE_SERVER.port;
//
//        } else if ([[AccountModel sharedAccountModel].serverModel.server.VIDEO_SERVER.protocol_list containsObject:nPath]){
//            nIp = [AccountModel sharedAccountModel].serverModel.server.VIDEO_SERVER.ip;
//            nPort = [AccountModel sharedAccountModel].serverModel.server.VIDEO_SERVER.port;
//
//        } else if ([[AccountModel sharedAccountModel].serverModel.server.LIVE_SERVER.protocol_list containsObject:nPath]){
//            nIp = [AccountModel sharedAccountModel].serverModel.server.LIVE_SERVER.ip;
//            nPort = [AccountModel sharedAccountModel].serverModel.server.LIVE_SERVER.port;
//        } else if ([[AccountModel sharedAccountModel].serverModel.server.FINANCE_SERVER.protocol_list containsObject:nPath]){
//            nIp = [AccountModel sharedAccountModel].serverModel.server.FINANCE_SERVER.ip;
//            nPort = [AccountModel sharedAccountModel].serverModel.server.FINANCE_SERVER.port;
//        }
//
//        if (nIp.length && nPort.length){
//            networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@://%@:%@",JAVA_HTTP,nIp,nPort]]];
//        } else {
//            networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@://%@:%@",JAVA_HTTP,JAVA_Host,JAVA_Port]]];
//        }
//    }
    
    // 将按钮设置为不可点击&& 设置按钮的网络接口
    if (self.currentTempButton){
        dispatch_async(dispatch_get_main_queue(), ^{
            self.currentTempButton.pathBinding = path;
            self.currentTempButton.enabled = NO;
            self.currentTempButton.userInteractionEnabled = NO;
        });
        
        if (![self.tempButtonMutableArr containsObject:self.currentTempButton]){
            [self.tempButtonMutableArr addObject:self.currentTempButton];
        }
    }
    
    [networkEngine fetchWithPath:path requestParams:requestParams responseObjectClass:responseObjectClass succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ((!([path isEqualToString:get_upload_token] || [path isEqualToString:login_logout]))){
            dispatch_async(dispatch_get_main_queue(), ^{
                strongSelf.currentTempButton.enabled = YES;
                strongSelf.currentTempButton.userInteractionEnabled = YES;
            });
        }
        
        // 将按钮置灰
        for (UIButton *button in strongSelf.tempButtonMutableArr){
            if ([path isEqualToString:button.pathBinding]){
//                dispatch_async(dispatch_get_main_queue(), ^{
                    button.enabled = YES;
                    button.userInteractionEnabled = NO;
//                });
                [strongSelf.tempButtonMutableArr removeObject:button];
                break;
            }
        }
        
        if (isSucceeded){
            
            if ([responseObject isKindOfClass:[NSDictionary class]]){           // 字典类别
                // 判断是否请求成功
                NSNumber *errorNumber = [responseObject objectForKey:@"result"];
                if (errorNumber.integerValue == 0){           // 请求成功
                    if (responseObjectClass == nil){
                        NSDictionary *tempResponseDic = (NSDictionary *)responseObject;
                        
                        if([tempResponseDic.allKeys containsObject:@"smart"]){          // 表示有加密
                            NSString *smart = [tempResponseDic objectForKey:@"smart"];
                            NSString *dicStr = [smart aci_decryptWithAES];
                            
                            
                            id response = [NetworkEngine dictionaryWithJsonString:dicStr];
                            
                            block(YES,response,nil);
                            return;
                        } else {
                            NSDictionary *dic = [responseObject objectForKey:@"data"];
                            block(YES,dic,nil);
                            return;
                        }
                    }
                    NSDictionary *respnseDic = (NSDictionary *)responseObject;
                    NSMutableDictionary *infoDic = [NSMutableDictionary dictionaryWithDictionary:respnseDic];
                    if ([respnseDic.allKeys containsObject:@"obj"]){
                        [infoDic setObject:[respnseDic objectForKey:@"obj"] forKey:@"data"];
                    } else if ([respnseDic.allKeys containsObject:@"data1"] && [respnseDic.allKeys containsObject:@"data2"]){
                        NSMutableDictionary *tempDataDic = [NSMutableDictionary dictionary];
                        [tempDataDic setObject:[respnseDic objectForKey:@"data1"] forKey:@"data1"];
                        [tempDataDic setObject:[respnseDic objectForKey:@"data2"] forKey:@"data2"];
                        
                        [infoDic setObject:tempDataDic forKey:@"data"];
                    }
                    
                    if ([[infoDic objectForKey:@"data"] isKindOfClass:[NSArray class]]){
                        NSDictionary *infoTempListDic = @{@"infoList":[infoDic objectForKey:@"data"]};
                        [infoDic setObject:infoTempListDic forKey:@"data"];
                    }
                    
                    //
                    if([infoDic.allKeys containsObject:@"smart"]){          // 表示有加密
                        NSString *smart = [infoDic objectForKey:@"smart"];
                        NSString *dicStr = [smart aci_decryptWithAES];
                      
                        NSDictionary *response = [NetworkEngine dictionaryWithJsonString:dicStr];
                        
                        FetchModel *responseModelObject = (FetchModel *)[[responseObjectClass alloc] initWithJSONDict:response];
                        block(YES,responseModelObject,nil);
                    } else {
                        FetchModel *responseModelObject = (FetchModel *)[[responseObjectClass alloc] initWithJSONDict:[respnseDic objectForKey:@"data"]];
                        block(YES,responseModelObject,nil);
                    }
                } else if (errorNumber.integerValue == 1005 ){            // 重新登录
                    if ([NetworkAdapter sharedAdapter].delegate && [[NetworkAdapter sharedAdapter].delegate respondsToSelector:@selector(notLoginNeedLoginManager)]){
                        // 清空数据
                        [[AccountModel sharedAccountModel] logoutManagerBlock:^{
                            [[CenterRootViewController sharedController] logoutStatusManager];
                        }];
                        [[NetworkAdapter sharedAdapter].delegate notLoginNeedLoginManager];
                    }
                } else if (errorNumber.integerValue == -2014){              // 测试
                    if ([NetworkAdapter sharedAdapter].delegate && [[NetworkAdapter sharedAdapter].delegate respondsToSelector:@selector(notLoginNeedLoginManager)]){
                        // 清空数据
                        [[AccountModel sharedAccountModel] logoutManagerBlock:^{
                            [[CenterRootViewController sharedController] logoutStatusManager];
                        }];
                        [[NetworkAdapter sharedAdapter].delegate notLoginNeedLoginManager];
                    }
                } else if (errorNumber.integerValue == 2018 || errorNumber.integerValue == 2019){
                    // 2018 直播不存在 2019 文章不存在
                    NSString *errorMsg = @"";
                    if (errorNumber.integerValue == 2018){
                        errorMsg = @"直播已被删除";
                    } else if (errorNumber.integerValue == 2019){
                        errorMsg = @"文章已被删除";
                    }
                    [[UIAlertView alertViewWithTitle:@"提示" message:errorMsg buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                        // 1. 判断
                        if ([BYTabbarViewController sharedController].currentController.navigationController.viewControllers.count > 1){
                            [[BYTabbarViewController sharedController].currentController.navigationController popViewControllerAnimated:YES];
                        } else {
                            [[BYTabbarViewController sharedController] showHome];
                        }
                    }]show];
                }
                else {                                        // 业务错误
//
                    
//                    [StatusBarManager statusBarHidenWithText:@"服务器好像出了点问题"];
                    id errInfo = [responseObject objectForKey:@"resultMsg"];
                    if ([errInfo isKindOfClass:[NSString class]]){
                        NSString *errorInfo = [responseObject objectForKey:@"resultMsg"];
                        if (!errorInfo.length){
                            errorInfo = @"系统错误，请联系管理员";
                        }
                        NSDictionary *dict = @{NSLocalizedDescriptionKey: errorInfo};
                        NSError *bizError = [NSError errorWithDomain:PDBizErrorDomain code:errorNumber.integerValue userInfo:dict];
                        block(NO,responseObject,bizError);

                        if (errorInfo.length){
                            [[UIAlertView alertViewWithTitle:@"提示" message:errorInfo buttonTitles:@[@"确定"] callBlock:NULL]show];
                            return;
                        } else {
                             [[UIAlertView alertViewWithTitle:@"提示" message:@"业务出现错误" buttonTitles:@[@"确定"] callBlock:NULL]show];
                        }
                        return;
                    } else {
                        return;
                    }
                }
            }
        } else {
            NSInteger errorCode = error.code;
            if (errorCode == -1004){
                [StatusBarManager statusBarHidenWithText:@"您可能与服务器断开连接"];
            }
            
            block(NO,responseObject,error);
        }
    }];
}



- (NSString*)dictionaryToJson:(NSDictionary *)dic{
    NSError *parseError = nil;
    
    SBJSON *jsonSDK = [[SBJSON alloc] init];
    NSString *jsonStr = [jsonSDK stringWithObject:dic error:&parseError];
    return jsonStr;
}


+ (NSString*)dictionaryToJson:(NSDictionary *)dic{
    NSError *parseError = nil;
    
    SBJSON *jsonSDK = [[SBJSON alloc] init];
    NSString *jsonStr = [jsonSDK stringWithObject:dic error:&parseError];
    return jsonStr;
}



-(void)actionGetServerInterfaceBlock:(void(^)(BOOL isSuccessed))block{
    NSDictionary *params = @{@"secret":@"ZxB6T_D3A*U3G9TC",
                             @"channel":Channel,
                             @"version_number":getAppVersion,
                             };
    __weak typeof(self)weakSelf = self;
    
    if ([AccountModel sharedAccountModel].getServerListTimes != 0){
        if (block){
            block(YES);
        }
        return;
    }
    
    [AccountModel sharedAccountModel].getServerListTimes += 1;
    
    [self fetchWithPath:get_server_list requestParams:params responseObjectClass:[ServerRootMainModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ServerRootMainModel *serverModel = (ServerRootMainModel *)responseObject;
            [AccountModel sharedAccountModel].serverModel = serverModel;
            
            [[BYTabbarViewController sharedController] shenheManager];
            [[BYTabbarViewController sharedController] versionUpdate];
            if (block){
                block(YES);
            }
        }
    }];
}


#pragma mark - 按钮点击方法
-(void)btnTempDelegateManager:(UIButton *)button{
    if(self.delegate && [self.delegate respondsToSelector:@selector(btnEnableStatusManager:)]){
        [self.delegate btnEnableStatusManager:button];
    }
}


#pragma mark - Websocket
-(void)webSocket:(SRWebSocket *)webSocket didReceiveData:(id)message{
    [self adjustReceiveManagerData:message websocket:webSocket];
}

#pragma mark - 分包黏包
-(void)adjustReceiveManagerData:(NSString *)data websocket:(SRWebSocket *)webSocket{
    // 第一步，寻找到是哪条connection
    WebSocketConnection *socketConnection;
    for (int i = 0 ; i < self.socketConnectMutableArr.count;i++){
        WebSocketConnection *singleConnection = [self.socketConnectMutableArr objectAtIndex:i];
        if (singleConnection.webSocket == webSocket){
            socketConnection = singleConnection;
        }
    }
    
    // 第二步，获取当前的内容数组
    NSArray *infoArr = [data componentsSeparatedByString:@"&"];
    
    // 第三步进行计算
    NSMutableString *mutableData = [NSMutableString string];
    
    if (socketConnection.socketWillTempData.length){
        [mutableData appendString:socketConnection.socketWillTempData];
        socketConnection.socketWillTempData = @"";
    }
    
    if (data.length){
        [mutableData appendString:data];
    }
    
    // 第四部 置空
    if (socketConnection.socketTempData.length == 0){
        socketConnection.socketTempData = @"";
    }
    if (socketConnection.socketWillTempData.length == 0){
        socketConnection.socketWillTempData = @"";
    }
    
    if (socketConnection.socketTempLength == 0){                // 第一次
        // 1. 获取长度
        NSString *numberStr = [infoArr firstObject];
        NSInteger backCountLength = [numberStr integerValue];
        NSInteger numberLength = numberStr.length;
        NSInteger backLength = backCountLength - numberLength;
        PDLog(@"%li",(long)backLength);
        // 1. 过滤4字节
        socketConnection.socketTempLength = backCountLength;
        socketConnection.socketTempWillLength = backCountLength;                   // 需要接收的长度
    }
    // 如果需要接收的长度大于当前接收到的长度就接收
    if (socketConnection.socketTempWillLength >= mutableData.length){
        if (socketConnection.socketTempData.length < socketConnection.socketTempLength){                    // 需要粘包
            socketConnection.socketTempData = [socketConnection.socketTempData stringByAppendingString:mutableData];
            socketConnection.socketTempWillLength = socketConnection.socketTempWillLength - mutableData.length; // 需要接收的长度
        }
    } else {
        NSRange range = NSMakeRange(0, socketConnection.socketTempWillLength);
        NSString *jiequhouData = [mutableData substringWithRange:range];
        socketConnection.socketTempData = [socketConnection.socketTempData stringByAppendingString:jiequhouData];
        socketConnection.socketTempWillLength = socketConnection.socketTempWillLength - jiequhouData.length;    // 需要接收的长度
        
        // 存储需要保存下来的data
        NSRange willSaveRange = NSMakeRange(jiequhouData.length, (mutableData.length - jiequhouData.length));
        socketConnection.socketWillTempData = [mutableData substringWithRange: willSaveRange];
    }
    
    if (socketConnection.socketTempData.length == socketConnection.socketTempLength && socketConnection.socketTempData.length != 0){
        
        // 表示拿到了完整的数据
        NSString *result = socketConnection.socketTempData;
        // 进行执行算法抛出
        [self webSocketCallBack:socketConnection didReceiveData:result];
        
        socketConnection.socketTempLength = 0;
        socketConnection.socketTempData = @"";
//        if (socketConnection.socketWillTempData.length){
//            [self adjustReceiveManagerData:nil websocket:webSocket];
//        }
    }
}

-(void)webSocketCallBack:(WebSocketConnection *)webSocketConnection didReceiveData:(id)message{
    // 1. 解析Data
    if ([message isKindOfClass:[NSString class]]){
        NSString *infoMsg = (NSString *)message;
        
        // 协议层
        NSArray *infoMsgArr = [infoMsg componentsSeparatedByString:@"☼❅"];
        // 1. 文字长度
        NSString *delegateNumberStr = [infoMsgArr objectAtIndex:0];
        NSInteger delegateNumber = -1;
        if (delegateNumberStr.length){
            delegateNumber = [delegateNumberStr integerValue];;
        }
        
        // 1.1 内容长度判断
        if (delegateNumber == infoMsg.length && [infoMsg hasSuffix:endCode]){
            // 2. 获取类型
            NSInteger socketTypeInteger = [[infoMsgArr objectAtIndex:1] integerValue];
            WebSocketType socketType = socketTypeInteger;
            
            // 3. 序列号
            NSInteger serverSerialNum = [[infoMsgArr objectAtIndex:2] integerValue];
            
            // 4.业务码
            NSInteger businessCode = [[infoMsgArr objectAtIndex:3]integerValue];
            
            // 5. 传输内容
            NSString *infoData = [infoMsgArr objectAtIndex:4];
            
            // 6. 结束符
            NSString *endNum = [infoMsgArr objectAtIndex:5];

            PDLog(@"%li",(long)serverSerialNum);
            PDLog(@"%li",(long)businessCode);
            PDLog(@"%@",endNum);
            
            
            
            // 7.判断序列号
//            if (socketConnection.serialAutoNumber > serverSerialNum){
//
//            }
            
            NSDictionary *responseDic = [NetworkEngine dictionaryWithJsonString:infoData];
            if (responseDic){
                PDLog(@"RESPONSE Socket :%@", infoData);
                WebSocketReceiveMainModel *responseModelObject = (WebSocketReceiveMainModel *)[[WebSocketReceiveMainModel alloc] initWithJSONDict:responseDic];
                // json
                responseModelObject.receiveDataJson = infoData;
                responseModelObject.socketType = socketType;
                // 进行解析
                [WebSocketReceiveMainModel socketReceiceMainEcode:responseModelObject];
                
                if (self.socketDelegate && [self.socketDelegate respondsToSelector:@selector(webSocketReceiveData:data:)]){
                    [self.socketDelegate webSocketReceiveData:socketType data:responseModelObject];
                }
            }
        } else {
            PDLog(@"获取到错误数据");
            [[UIAlertView alertViewWithTitle:@"标题" message:@"获取到错误数据，无法解析，等待后续操作" buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }
}


#pragma mark - Socket连接成功
-(void)webSocketDidConnectedWithSocket:(SRWebSocket *)webSocket{
    // 1. 判断当前的websocket与连接池的
    WebSocketConnection *socketConnection;
    for (int i = 0 ; i < self.socketConnectMutableArr.count;i++){
        WebSocketConnection *singleConnection = [self.socketConnectMutableArr objectAtIndex:i];
        if (singleConnection.webSocket == webSocket){
            socketConnection = singleConnection;
        }
    }
    
    // 进行代理回调
    if(self.socketDelegate && [self.socketDelegate respondsToSelector:@selector(webSocketDidConnectedWithSocket:connectedStatus:)]){
        [self.socketDelegate webSocketDidConnectedWithSocket:socketConnection connectedStatus:SocketConnectTypeOpen];
    }
    // 写出swebsocket
    PDLog(@"%@",webSocket);
}

#pragma mark - WebSocket
// 【1.webSocket 连接】
-(WebSocketConnection *)webSocketConnection:(WebSocketConnection *)webSocketConnection host:(NSString *)host port:(NSInteger)port{
    if (webSocketConnection){
        [self webSocketCloseConnectionWithSoceket:webSocketConnection];
    }
    webSocketConnection = [[WebSocketConnection alloc]init];
    webSocketConnection.delegate = self;
    [webSocketConnection webSocketConnectWithHost:host port:port];
    
    
    // 加入到连接池
    if (!self.socketConnectMutableArr){
        self.socketConnectMutableArr = [NSMutableArray array];
    }
    [self.socketConnectMutableArr addObject:webSocketConnection];
    
    return webSocketConnection;
}

// 【2.断开长链接【WebSocket】】
-(void)webSocketCloseConnectionWithSoceket:(WebSocketConnection *)webSocketConnection{
    if (webSocketConnection){
        webSocketConnection.delegate = nil;
        [webSocketConnection webSocketDisconnect];
        webSocketConnection = nil;
    }
}

// 【3.填写信息WebSocket】
-(void)webSocketFetchModelWithRequestParams:(NSDictionary *)requestParams socket:(WebSocketConnection *)webSocketConnection{
    if (!webSocketConnection){
        [[UIAlertView alertViewWithTitle:@"no socket connected" message:@"" buttonTitles:@[@"ok"] callBlock:NULL]show];
        return;
    }
    
    
    
    NSString *jsonParams = [NetworkAdapter dictionaryToJson:requestParams];
    NSString *appendingParams = [NSString stringWithFormat:@"\r\n"];
    NSString *dataStr = [NSString stringWithFormat:@"%@%@",jsonParams,appendingParams];
    PDLog(@"dataStr%@",dataStr);
    // 0. 拼接
    NSString *mainStr = @"";
    // 1.长度
    NSInteger infoLength = 0;
    // 2.类型
    WebSocketType type = WebSocketTypeIM;
    NSInteger typeLen = (NSInteger)type;
    // 3. 序列号
    NSInteger serialNum = 0;
    // 4. 业务吗
    NSInteger bussnessCode = 413;
    // 5. 传输内容
    NSString *transferStr = jsonParams;
    // 6. 结束符
    NSString *transferEndCode = endCode;
    // 7.拼接
    NSString *tempMainString = [NSString stringWithFormat:@"☼❅%li☼❅%li☼❅%li☼❅%@☼❅%@",(long)typeLen,(long)serialNum,(long)bussnessCode,transferStr,transferEndCode];
    // 8.计算长度
    infoLength = tempMainString.length;
    NSString *lenStr = [NSString stringWithFormat:@"%li",(long)infoLength];
    NSInteger lenStrLen = lenStr.length;
    infoLength += lenStrLen;
    mainStr = [NSString stringWithFormat:@"%li%@",(long)infoLength,tempMainString];
    
    [webSocketConnection webSocketWriteData:mainStr];

}

-(void)webSocketDidConnectedErrorTimes:(NSInteger)times connectType:(SocketConnectType)connectType errorString:(NSString *)err{
    if (self.socketDelegate && [self.socketDelegate respondsToSelector:@selector(webSocketDidConnectedErrorTimes:connectType:errorString:)]) {
        [self.socketDelegate webSocketDidConnectedErrorTimes:times connectType:connectType errorString:err];
    }
}

-(NSMutableArray *)tempButtonMutableArr{
    if (!_tempButtonMutableArr){
        _tempButtonMutableArr = [NSMutableArray array];
    }
    return _tempButtonMutableArr;
}

-(void)notLoginNeedLoginManager{
    NSLog(@"123");
}

@end

