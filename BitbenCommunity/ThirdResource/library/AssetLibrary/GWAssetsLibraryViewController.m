//
//  GWAssetsLibraryViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/10.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWAssetsLibraryViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "GWAssetsCollectionViewCell.h"
#import "GWVideoCollectionViewCell.h"
#import <objc/runtime.h>
#import "GWAssetsAblumsViewController.h"            // 相册
#import "PhotoTweaksViewController.h"               // 裁剪

#define k_assets_margin LCFloat(10)
#define kOriginalScrollView     @"assetOriginalScrollView"
#define kOriginalImageView      @"assetOriginalImageView"
#define kOriginalMaskImageView  @"assetOriginalMaskImageView"
#define kOriginalNaviBar        @"assetOriginalNaviBar"
#define kSelectionLabKey        @"assetSelectionLabKey"
#define kSelectionImageKey      @"assetSelectionOImageKey"

static char originalFrameKey;
static char selectedImageArrManagerKey;        // 选择照片方法

@interface GWAssetsLibraryViewController()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate,PhotoTweaksViewControllerDelegate>
@property (nonatomic,strong)NSMutableArray *ablumsMutableArr;                          /**< 相册数量*/
@property (nonatomic,strong)NSMutableArray *assetsMutableArr;                          /**< 照片数量*/
@property (nonatomic,strong)NSMutableArray *selectedAssetsMutableArr;                  /**< 选中的照片内容*/
@property (nonatomic,strong)ALAssetsLibrary *assetsLibrary;                            /**< 所有的相册内容*/
@property (nonatomic , strong) PHAssetCollection *assetCollection;

@property (nonatomic,strong)UICollectionView *assetsCollectionView;                          /**< 相册collectionView*/
@property (nonatomic,strong)UITableView *ablumTableView;
@property (nonatomic,strong)UIImage *blurImage;

// pageSetting
@property (nonatomic,strong)UIButton *navRightButton;
@property (nonatomic,assign)NSInteger maxSelected;
@property (nonatomic,strong)GWVideoCollectionViewCell *videoCollectionCell;               /**< 照相机cell*/
@property (nonatomic,strong)UIButton *actionButton;

@end

@implementation GWAssetsLibraryViewController
-(void)selectImageArrayFromImagePickerWithMaxSelected:(NSInteger)maxSelected andBlock:(void(^)(NSArray *selectedImgArr))block{
    self.maxSelected = maxSelected;
    objc_setAssociatedObject(self, &selectedImageArrManagerKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)dealloc{
    NSLog(@"释放了");
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];                                 // 页面初始胡
    [self arrayWithInit];                               // 数据初始化
    [self judgmentAuthorityWithAssetLibrary];           // 权限访问
    [self createBottomBtn];
    [self createCollectionView];                        // 创建一个collectionView
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"相册";
    __weak typeof(self)weakSelf = self;
    self.navRightButton = [weakSelf rightBarButtonWithTitle:@"确定" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf assetsChangeImageManager];
    }];
    self.navRightButton.hidden = YES;
    [self createNavigationBar];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.ablumsMutableArr = [NSMutableArray array];
    self.selectedAssetsMutableArr = [NSMutableArray array];
    self.assetsLibrary = [[ALAssetsLibrary alloc]init];
    self.assetCollection = [[PHAssetCollection alloc] init];
}

#pragma mark - 【1】获取当前相册权限
-(void)judgmentAuthorityWithAssetLibrary{
    if (!IS_IOS8_LATER) {
        if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusDenied) {
           [[UIAlertView alertViewWithTitle:@"提示" message:@"请在iPhone的\"设置－隐私－照片\"选项中，允许访问你的手机相册" buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
        else
        {
            [self searchAllAlassetsLibrary];
        }
    }
    else
    {
        if ([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusDenied){       // 用户已经明确否认了这一照片数据的应用程序访问.
               [[UIAlertView alertViewWithTitle:@"提示" message:@"请在iPhone的\"设置－隐私－照片\"选项中，允许访问你的手机相册" buttonTitles:@[@"确定"] callBlock:NULL]show];
           }  else {
               [self searchAllAlassetsLibrary];
           }
    }
}

#pragma mark - 【2】搜索并获取所有相册内容
-(void)searchAllAlassetsLibrary{
    __weak typeof(self)weakSelf = self;
    
    if (!IS_IOS8_LATER) {
         [weakSelf searchAssetCollectionWithType:PHAssetCollectionTypeSmartAlbum cb:^(PHFetchResult *result) {
             if(!weakSelf){
                 return ;
             }
             __strong typeof(weakSelf)strongSelf = weakSelf;
             if (!result.count) return;
             [strongSelf.ablumsMutableArr addObject:result];
             // 如果当前的相册是有相片的，就加入数组
             if (strongSelf.ablumsMutableArr.count == 1){
                 [strongSelf searchAllAssetCollectionWithResult:result];
             }
         }];
        
        return;
    }
    
    // 搜索保存的照片
    [weakSelf searchAlassetLibraryWithType:ALAssetsGroupSavedPhotos withSuccessBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 过滤相片
        if (group){
            [group setAssetsFilter:[ALAssetsFilter allPhotos]];
            // 相册增加
            if (group.numberOfAssets > 0){
                [strongSelf.ablumsMutableArr addObject:group];
            }
            // 如果当前的相册是有相片的，就加入数组
            if (strongSelf.ablumsMutableArr.count == 1){
                [strongSelf searchAllAssetsWithGroup:group];
            }
        }
    }];

    [weakSelf searchAlassetLibraryWithType:ALAssetsGroupAlbum withSuccessBlock:^(ALAssetsGroup *group, BOOL *stop) {
           if(!weakSelf){
               return ;
           }
           __strong typeof(weakSelf)strongSelf = weakSelf;
           if (group) {
               [group setAssetsFilter:[ALAssetsFilter allPhotos]];
           }
           if (group.numberOfAssets > 0) {
               [strongSelf.ablumsMutableArr addObject:group];
           }
    }];

}
#pragma mark - 【搜索相册方法】
-(void)searchAlassetLibraryWithType:(ALAssetsGroupType)type withSuccessBlock:(void(^)(ALAssetsGroup *group, BOOL *stop))searchBlock{
    __weak typeof(self)weakSelf = self;

    [weakSelf.assetsLibrary enumerateGroupsWithTypes:type usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
           if (!weakSelf){
               return ;
           }
           return searchBlock(group, stop);
       } failureBlock:^(NSError *error) {
           [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
       }];
}

- (void)searchAssetCollectionWithType:(PHAssetCollectionType)type cb:(void(^)(PHFetchResult *result))cb{
    PHFetchResult *fetchResult = [PHAssetCollection fetchAssetCollectionsWithType:type subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    if (cb) cb(fetchResult);
}

#pragma mark - 【3】搜索当前group内的照片数量
-(void)searchAllAssetsWithGroup:(ALAssetsGroup *)group{
    if (!self.assetsMutableArr) {
        self.assetsMutableArr = [NSMutableArray array];
    } else {
        [self.assetsMutableArr removeAllObjects];
    }
    __weak typeof(self)weakSelf = self;
    // 遍历相册内容
    [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (result){
            [strongSelf.assetsMutableArr addObject:result];
        }
    }];
    self.barMainTitle = [group valueForProperty:ALAssetsGroupPropertyName];
    [self.assetsCollectionView reloadData];
    [self.assetsCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:[self.assetsCollectionView numberOfItemsInSection:0] - 1 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
}

- (void)searchAllAssetCollectionWithResult:(PHFetchResult *)result{
    if (!self.assetsMutableArr) {
        self.assetsMutableArr = [NSMutableArray array];
    } else {
        [self.assetsMutableArr removeAllObjects];
    }
    __weak typeof(self)weakSelf = self;
    [result enumerateObjectsUsingBlock:^(PHAssetCollection *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (obj) {
            [strongSelf.assetsMutableArr addObject:result];
        }
    }];
    self.barMainTitle = @"相册";
    [self.assetsCollectionView reloadData];
    [self.assetsCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:[self.assetsCollectionView numberOfItemsInSection:0] - 1 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];

}

#pragma mark - 【UICollectionView】
-(void)createCollectionView{
    UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
    self.assetsCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, self.view.size_height - [BYTabbarViewController sharedController].tabBarHeight - [BYTabbarViewController sharedController].navBarHeight) collectionViewLayout:flowLayout];
    self.assetsCollectionView.backgroundColor = [UIColor clearColor];
    self.assetsCollectionView.showsVerticalScrollIndicator = NO;
    [self.assetsCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
    self.assetsCollectionView.delegate = self;
    self.assetsCollectionView.dataSource = self;
    self.assetsCollectionView.showsHorizontalScrollIndicator = NO;
    self.assetsCollectionView.scrollsToTop = YES;
//    self.assetsCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.assetsCollectionView];
    
    [self.assetsCollectionView registerClass:[GWAssetsCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
    [self.assetsCollectionView registerClass:[GWVideoCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify2"];
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.isNotShowCamera ? self.assetsMutableArr.count :self.assetsMutableArr.count + 1;
}

#pragma mark - UICollectionViewDelegate
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == self.assetsMutableArr.count){
        self.videoCollectionCell = (GWVideoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectionIdentify2"  forIndexPath:indexPath];
        return self.videoCollectionCell;
    } else {            // 相册
        GWAssetsCollectionViewCell *cell = (GWAssetsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectionIdentify"  forIndexPath:indexPath];
        cell.transferSingleAsset = [self.assetsMutableArr objectAtIndex:indexPath.row];
        cell.transferSelectedAssets = self.selectedAssetsMutableArr;
        cell.fixedTransferMaxSelected = self.maxSelected;
        __weak typeof(self)weakSelf = self;
        cell.assetSelectedBlock = ^(){
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf selectedAssetsManager];
        };
        return cell;
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((kScreenBounds.size.width - 4 * k_assets_margin) / 3., (kScreenBounds.size.width - 4 * k_assets_margin) / 3.);
}

#pragma mark - UICollectionViewDelegate
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0,0,0);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    CGSize size = {kScreenBounds.size.width,LCFloat(20)};
    return size;
}

#pragma mark - PhotoTweaksViewControllerDelegate 图片裁剪

- (void)photoTweaksController:(PhotoTweaksViewController *)controller didFinishWithCroppedImage:(UIImage *)croppedImage{
    NSMutableArray *selectedImageMutableArr = [NSMutableArray array];
    [selectedImageMutableArr addObject:croppedImage];
    void(^selectedImgArrBlock)(NSArray *selectedImgArr) = objc_getAssociatedObject(self, &selectedImageArrManagerKey);
    if (selectedImgArrBlock){
        selectedImgArrBlock(selectedImageMutableArr);
        if (self.navigationController.viewControllers.count == 1){
            [controller dismissViewControllerAnimated:YES completion:NULL];
        } else {
            NSInteger count = controller.navigationController.viewControllers.count;
            UIViewController *viewController = controller.navigationController.viewControllers[count - 3];
            [controller.navigationController popToViewController:viewController animated:YES];
        }
    }
}

- (void)photoTweaksControllerDidCancel:(PhotoTweaksViewController *)controller{
    [controller.navigationController popViewControllerAnimated:YES];
}

#pragma mark cell的点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != self.assetsMutableArr.count){              // 点开相册
        ALAsset *selectedAsset = [self.assetsMutableArr objectAtIndex:indexPath.row];
        GWAssetsCollectionViewCell *assetCell = (GWAssetsCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [self showBigImageManagerWithSelectedAsset:selectedAsset cell:assetCell];
    } else {  // 点开照相机
        [self takeCameraToMakePhoto];
    }
}

#pragma mark - createNavigationBar
-(void)createNavigationBar{
    self.barMainTitle = @"添加图片";
    self.barSubTitle = @"相机胶卷";
    
    // 创建button
    UIButton *showAblumsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    showAblumsButton.frame = self.navigationItem.titleView.bounds;
    [showAblumsButton setImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateNormal];
    [showAblumsButton setImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateHighlighted];
    CGFloat verticalInset = (CGRectGetHeight(showAblumsButton.frame)-8)/2.;
    CGFloat horizontalInset = CGRectGetWidth(showAblumsButton.frame)-12;
    showAblumsButton.imageEdgeInsets = UIEdgeInsetsMake(verticalInset, horizontalInset-48, verticalInset, 48);
    [self.navigationItem.titleView addSubview:showAblumsButton];
    __weak typeof(self)weakSelf = self;
    [showAblumsButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        GWAssetsAblumsViewController *assetsAblumsViewController = [[GWAssetsAblumsViewController alloc]init];
        assetsAblumsViewController.ablumsArr = strongSelf.ablumsMutableArr;
        assetsAblumsViewController.ablumSelectrdBlock = ^(ALAssetsGroup *ablum){
            [strongSelf searchAllAssetsWithGroup:ablum];
            [strongSelf actionShowAblums];
            [strongSelf.assetsCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:[strongSelf.assetsCollectionView numberOfItemsInSection:0] - 1 inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:NO];
        };
        [strongSelf.navigationController pushViewController:assetsAblumsViewController animated:YES];
    }];
}

#pragma mark -actionShowAblums
- (void)actionShowAblums {
    if (self.ablumsMutableArr.count == 0) {
        if (self.ablumsMutableArr.count == 0) {
            [[UIAlertView alertViewWithTitle:@"相册为空" message:@"相册为空" buttonTitles:@[@"确定"] callBlock:NULL] show];
            return ;
        }
    }
    BOOL hadShowAblumTableView = (CGRectGetHeight(self.ablumTableView.frame) == CGRectGetHeight(self.view.bounds)-64);
    UIImageView *blurImageView = (UIImageView *)[self.navigationController.view viewWithStringTag:@"blurImageView"];
    if (!blurImageView) {
        blurImageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        blurImageView.userInteractionEnabled = YES;
        blurImageView.stringTag = @"blurImageView";
        blurImageView.image =  [[UIImage screenShoot:self.navigationController.view] applyExtraWithAlAssetLibrary];
        
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDismissAblums)];
        [blurImageView addGestureRecognizer:gesture];
    }
    if (!hadShowAblumTableView) {
        [self.navigationController.view insertSubview:blurImageView belowSubview:self.ablumTableView];
    }
    
    if (hadShowAblumTableView) {
        self.ablumTableView.frame = CGRectMake(10, 64, CGRectGetWidth(self.view.bounds)-20, 0);
        [blurImageView removeFromSuperview];
    } else {
        [self.ablumTableView reloadData];
        self.ablumTableView.frame = CGRectMake(10, 64, CGRectGetWidth(self.view.bounds)-20, CGRectGetHeight(self.view.bounds)-64);
    }
}

#pragma mark -actionDismissAblums
- (void)actionDismissAblums{
//    [self actionShowAblums];
    
    GWAssetsAblumsViewController *assetsAblumsViewController = [[GWAssetsAblumsViewController alloc]init];
    assetsAblumsViewController.ablumsArr = self.ablumsMutableArr;
    [self.navigationController pushViewController:assetsAblumsViewController animated:YES];
}


#pragma mark - OtherManager
-(void)showBigImageManagerWithSelectedAsset:(ALAsset *)asset cell:(GWAssetsCollectionViewCell *)cell{
    UIScrollView *originalScrollView = (UIScrollView *)[self.view.window viewWithStringTag:kOriginalScrollView];
    if (originalScrollView){
        return;
    }
    CGRect convertItemFrame = [cell convertRect:cell.assetsImageView.frame toView:self.view.window];
    originalScrollView = [[UIScrollView alloc] initWithFrame:convertItemFrame];
    originalScrollView.stringTag = kOriginalScrollView;
    originalScrollView.backgroundColor = [UIColor blackColor];

    
    UITapGestureRecognizer * tapForHideKeyBoard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDismissOriginalPhoto)];
    [originalScrollView addGestureRecognizer:tapForHideKeyBoard];
    objc_setAssociatedObject(originalScrollView, &originalFrameKey, [NSValue valueWithCGRect:convertItemFrame], OBJC_ASSOCIATION_RETAIN);
    
    UIImageView *originalImageView = [[UIImageView alloc] initWithFrame:originalScrollView.bounds];
    originalImageView.stringTag = kOriginalImageView;
//    UIImage *originalImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullResolutionImage]];
    
    UIImage *originalImage = [UIImage imageWithCGImage:asset.defaultRepresentation.fullResolutionImage
          scale:asset.defaultRepresentation.scale
    orientation:(UIImageOrientation)asset.defaultRepresentation.orientation];
    
    originalImage = [UIImage scaleDown:originalImage withSize:CGSizeMake(kScreenBounds.size.width, kScreenBounds.size.width*originalImage.size.height/originalImage.size.width)];
    originalImageView.image = originalImage;
    originalImageView.contentMode = UIViewContentModeScaleAspectFit;
    [originalScrollView addSubview:originalImageView];
    [self.view.window addSubview:originalScrollView];
    
    
    // 设置比例
    CGFloat imageRadio = originalImage.size.width/originalImage.size.height;
    CGFloat screenRadio = CGRectGetWidth(self.view.window.frame)/CGRectGetHeight(self.view.window.frame);
    if (imageRadio >= screenRadio) {
        CGFloat currentImageHeight = CGRectGetWidth(self.view.window.frame)/imageRadio;
        originalScrollView.maximumZoomScale = CGRectGetHeight(self.view.window.frame)/currentImageHeight;
    } else {
        CGFloat currentImageWidth = CGRectGetWidth(self.view.window.frame)*imageRadio;
        originalScrollView.maximumZoomScale = CGRectGetWidth(self.view.window.frame)/currentImageWidth;
    }
    originalScrollView.minimumZoomScale = 1.;
    // 开启动画
    [UIView animateWithDuration:.5f delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        originalScrollView.frame = self.navigationController.view.bounds;
        originalImageView.frame = self.navigationController.view.bounds;
    } completion:^(BOOL finished) {
        
    }];
}


-(void)actionDismissOriginalPhoto{
    UIScrollView *originalScrollView = (UIScrollView *)[self.view.window viewWithStringTag:kOriginalScrollView];
    UIImageView *originalImageView = (UIImageView *)[originalScrollView viewWithStringTag:kOriginalImageView];
    CGRect originalFrame = (CGRect)[objc_getAssociatedObject(originalScrollView, &originalFrameKey)CGRectValue];
    
    [UIView animateWithDuration:.5 animations:^{
        originalScrollView.frame = originalFrame;
        originalScrollView.layer.cornerRadius = 5;
        originalScrollView.alpha = 0.;
        originalImageView.frame = CGRectMake(0, 0, CGRectGetWidth(originalFrame), CGRectGetHeight(originalFrame));
    } completion:^(BOOL finished) {
        [originalScrollView removeFromSuperview];
    }];
}

// 缩放
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    UIScrollView *originalScrollView = (UIScrollView *)[self.view.window viewWithStringTag:kOriginalScrollView];
    UIImageView *originalImageView = (UIImageView *)[originalScrollView viewWithStringTag:kOriginalImageView];
    return originalImageView;
}

#pragma mark 判断nav右侧按钮是否可以点击
-(void)selectedAssetsManager{
    [self.actionButton setTitle:[NSString stringWithFormat:@"确定（%li/%li）",self.selectedAssetsMutableArr.count,self.maxSelected] forState:UIControlStateNormal];
    if (self.selectedAssetsMutableArr.count){
        self.actionButton.enabled = YES;
        [self.actionButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    } else {
        self.actionButton.enabled = NO;
        [self.actionButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
}

#pragma mark 选中的asset进行转换成img
-(void)assetsChangeImageManager{
    NSMutableArray *selectedImageMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < self.selectedAssetsMutableArr.count;i++){
        ALAsset *singleAsset = [self.selectedAssetsMutableArr objectAtIndex:i];
        UIImage *singleImage = [UIImage imageWithCGImage:singleAsset.defaultRepresentation.fullResolutionImage
              scale:singleAsset.defaultRepresentation.scale
        orientation:(UIImageOrientation)singleAsset.defaultRepresentation.orientation];
        [selectedImageMutableArr addObject:singleImage];
    }
    void(^selectedImgArrBlock)(NSArray *selectedImgArr) = objc_getAssociatedObject(self, &selectedImageArrManagerKey);
    if (self.isCanCut && self.maxSelected == 1) {
        CGRect cropFrame = CGRectZero;
        if (CGRectEqualToRect(self.cropFrame, CGRectZero)) {
            cropFrame = CGRectMake(0, (kCommonScreenHeight - kCommonScreenWidth)/2, kCommonScreenWidth, kCommonScreenWidth);
        }else{
            cropFrame = self.cropFrame;
        }
        PhotoTweaksViewController *imageCropperController = [[PhotoTweaksViewController alloc] initWithImage:selectedImageMutableArr[0]];
        imageCropperController.autoSaveToLibray = NO;
        imageCropperController.delegate = self;
        [self.navigationController pushViewController:imageCropperController animated:YES];
    }else{
        if (selectedImgArrBlock){
            selectedImgArrBlock(selectedImageMutableArr);
            if (self.navigationController.viewControllers.count == 1){
                [self dismissViewControllerAnimated:YES completion:NULL];
            } else {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}

#pragma mark 点开相机
-(void)takeCameraToMakePhoto{
    if (![UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera]){
        [[UIAlertView alertViewWithTitle:@"提示" message:@"当前设备不可用" buttonTitles:@[@"确认"] callBlock:nil]show];
        return ;
    }
    
    if ([self.videoCollectionCell.captureSession isRunning]){                       // 如果摄像头在运行，就把他关闭
        [self.videoCollectionCell.captureSession stopRunning];
        [self performSelector:@selector(actionOpenImagePickerController) withObject:nil afterDelay:0];
    } else {
        return;
    }
    
}

- (void) actionOpenImagePickerController {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark –
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    [self performSelector:@selector(saveImage:) withObject:image afterDelay:0.5];
}

// 保存图片
- (void)saveImage:(UIImage *)image {
    __weak typeof(self)weakSelf = self;
    [weakSelf.assetsLibrary writeImageToSavedPhotosAlbum:image.CGImage orientation:(ALAssetOrientation)image.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error) {          // 返回了一个url
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.assetsLibrary assetForURL:assetURL resultBlock:^(ALAsset *asset) {
            [strongSelf.assetsMutableArr addObject:asset];                  // 1. 所有的asset增加
            if (strongSelf.selectedAssetsMutableArr.count < strongSelf.maxSelected){        // 选中
                [strongSelf.selectedAssetsMutableArr addObject:asset];
            }
            [strongSelf selectedAssetsManager];
            [strongSelf.assetsCollectionView reloadData];
            
        } failureBlock:^(NSError *error) {
            
        }];
    }];
    
    // 开启照相机
    [self.videoCollectionCell actionRunningCamera];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if (![self.videoCollectionCell.captureSession isRunning]){
        [self.videoCollectionCell.captureSession startRunning];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - createBottomBtn
-(void)createBottomBtn{
    if (!self.actionButton){
        self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.actionButton.backgroundColor = [UIColor colorWithRed:246/255.0 green:246/255.0 blue:249/255.0 alpha:1.0];
    
        __weak typeof(self)weakSelf = self;
        [self.actionButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf assetsChangeImageManager];
        }];
        [self.actionButton setTitleColor:[UIColor hexChangeFloat:@"EA6441"] forState:UIControlStateNormal];
        self.actionButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
        self.actionButton.frame = CGRectMake(0, self.view.bounds.size.height - [BYTabbarViewController sharedController].navBarHeight - [BYTabbarViewController sharedController].tabBarHeight, kScreenBounds.size.width, [BYTabbarViewController sharedController].tabBarHeight);
        [self.actionButton setTitle:[NSString stringWithFormat:@"确定（%li/%li）",self.selectedAssetsMutableArr.count,self.maxSelected] forState:UIControlStateNormal];
        [self.view addSubview:self.actionButton];
    }
}
@end
