//
//  GWAssetsImgSelectedViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/13.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "ArticleImageSelectedTableViewCell.h"               // 文章里面使用到
#import "ArticleRootTableViewCell.h"
#import "ArticleDetailInfoTableViewCell.h"
#import "MineRealChoosePhotoTableViewCell.h"
#import "BYPersonHomeHeaderView.h"
#import "BYHomeArticleImgCell.h"
#import "BYHomeNewsletterRootCell.h"

@interface GWAssetsImgSelectedViewController : AbstractViewController

@property (nonatomic,assign)CGRect webViewSelectedRect;

/** 是否可保存 */
@property (nonatomic ,assign) BOOL canSave;


-(void)showInView:(UIViewController *)viewController imgArr:(NSArray *)imgArr currentIndex:(NSInteger)currentIndex cell:(id )cell;

- (void)dismissFromView:(UIViewController *)viewController;

@end
