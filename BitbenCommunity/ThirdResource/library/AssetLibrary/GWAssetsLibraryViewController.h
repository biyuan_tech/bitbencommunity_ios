//
//  GWAssetsLibraryViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/10.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "AbstractViewController.h"

@interface GWAssetsLibraryViewController : AbstractViewController
@property (nonatomic,assign)BOOL isNotShowCamera;
/** 是否可裁剪编辑(单张图片选择可用) */
@property (nonatomic ,assign) BOOL isCanCut;
/** 裁剪区域 */
@property (nonatomic ,assign) CGRect cropFrame;


// 选择照片方法
-(void)selectImageArrayFromImagePickerWithMaxSelected:(NSInteger)maxSelected andBlock:(void(^)(NSArray *selectedImgArr))block;
@end
