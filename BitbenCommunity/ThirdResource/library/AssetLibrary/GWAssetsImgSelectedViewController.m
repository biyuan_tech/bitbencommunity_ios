//
//  GWAssetsImgSelectedViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/13.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "GWAssetsImgSelectedViewController.h"
#import <Photos/Photos.h>

static NSInteger kBaseTag = 843;
@interface GWAssetsImgSelectedViewController ()<UIScrollViewDelegate>
{
    NSInteger _lastIndex;
}
@property (nonatomic,weak)UIViewController *showViewController;                 /**< 显示的控制器*/
@property (nonatomic,strong)UIView *backView;                                   /**< 背景View*/
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)PDImageView *userAnimationImgView;;
@property (nonatomic,strong)NSArray *tempImgArr;
@property (nonatomic,assign)NSInteger tempCurrentIndex;
@property (nonatomic,strong)UIButton *downButton;
@property (nonatomic,strong)PDImageView *tempConvertView;
@property (nonatomic,strong)id tempCell;
@property (nonatomic,strong)UIPageControl *pageControl;
@end

@implementation GWAssetsImgSelectedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    [self pageSetting];
}

-(void)dealloc{
    NSLog(@"123");
}

- (void)setCanSave:(BOOL)canSave{
    _canSave = canSave;
    _downButton.hidden = !canSave;
}

-(void)pageSetting{
    if (!self.backView){
        self.backView = [[UIView alloc]init];
        self.backView.backgroundColor = [UIColor blackColor];
        self.backView.frame = self.view.bounds;
        self.backView.alpha = 1;
        self.backView.userInteractionEnabled = YES;
        [self.view addSubview:self.backView];
        // 2. 创建手势
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.backView addGestureRecognizer:tapGestureRecognizer];
    }
    
    // 2. 创建scrollview
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]init];
        self.mainScrollView.backgroundColor = [UIColor blackColor];
        self.mainScrollView.showsHorizontalScrollIndicator = NO;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.layer.zPosition = MAXFLOAT;
        self.mainScrollView.delegate = self;
        self.mainScrollView.userInteractionEnabled = YES;
        self.mainScrollView.maximumZoomScale=2.0;
        self.mainScrollView.minimumZoomScale=0.5;
        self.mainScrollView.bounces = YES;
        self.mainScrollView.pagingEnabled = YES;
        self.mainScrollView.bouncesZoom = YES;
        self.mainScrollView.frame = kScreenBounds;
        [self.backView addSubview:self.mainScrollView];
    }
    // 2. 创建pagecontrol
    if (!self.pageControl){
        self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, kScreenBounds.size.height - LCFloat(11) * 3,kScreenBounds.size.width,LCFloat(11))];
        self.pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        self.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
        [self.view addSubview:self.pageControl];
    }
    
    [self.view addSubview:self.downButton];
    [self.downButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.width.height.mas_equalTo(40);
        make.bottom.mas_equalTo(-kSafe_Mas_Bottom(15));
    }];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak GWAssetsImgSelectedViewController *weakVC = self;
    
    self.backView.alpha = 0;
    self.mainScrollView.alpha = 0;
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
    // 1. 获取当前的index
    if (self.pageControl.currentPage + 1 > 2){
        [UIView animateWithDuration:.3f animations:^{
            
        } completion:^(BOOL finished) {
            if (!weakVC){
                return ;
            }
            __strong typeof(weakVC)strongSelf = weakVC;
            [strongSelf.userAnimationImgView removeFromSuperview];
            strongSelf.userAnimationImgView = nil;
            [strongSelf willMoveToParentViewController:nil];
            [strongSelf.view removeFromSuperview];
            [strongSelf removeFromParentViewController];
        }];
    } else {
        // 1. 找到当前的view

        CGRect dismissTargetRect = CGRectMake(0, 0, 100, 100);
        if ([self.tempCell isKindOfClass:[ArticleImageSelectedTableViewCell class]]){
            ArticleImageSelectedTableViewCell *tempCell = (ArticleImageSelectedTableViewCell *)self.tempCell;
            self.userAnimationImgView.image = tempCell.selectedImgView.image;
            
            dismissTargetRect = [tempCell convertRect:tempCell.selectedImgView.superview.frame toView:self.view.window];
        } else if ([self.tempCell isKindOfClass:[ArticleRootTableViewCell class]]){
            ArticleRootTableViewCell *tempCell = (ArticleRootTableViewCell *)self.tempCell;
            self.userAnimationImgView.image = tempCell.selectedImgView.image;
            
            dismissTargetRect = [tempCell.imgBgView convertRect:tempCell.selectedImgView.frame toView:self.view.window];
        } else if ([self.tempCell isKindOfClass:[ArticleDetailInfoTableViewCell class]]){
            ArticleDetailInfoTableViewCell *tempCell = (ArticleDetailInfoTableViewCell *)self.tempCell;
            
             if (tempCell.transferArticleModel.article_type == article_typeNormal){
                 self.userAnimationImgView.image = tempCell.selectedImgView.image;
                 dismissTargetRect = [tempCell.imgBgView convertRect:tempCell.selectedImgView.frame toView:self.view.window];
             } else {
                 dismissTargetRect = [tempCell.webView convertRect:self.webViewSelectedRect toView:self.view.window];
             }
        } else if ([self.tempCell isKindOfClass:[MineRealChoosePhotoTableViewCell class]]){
            MineRealChoosePhotoTableViewCell *tempCell = (MineRealChoosePhotoTableViewCell *)self.tempCell;
            self.userAnimationImgView.image = tempCell.transferImg;
            dismissTargetRect = [tempCell.convertView convertRect:tempCell.imageView.frame toView:self.view.window];
        } else if ([self.tempCell isKindOfClass:[BYPersonHomeHeaderView class]]){
            BYPersonHomeHeaderView *tempCell = (BYPersonHomeHeaderView *)self.tempCell;
            self.userAnimationImgView.image = tempCell.headerImgView.image;
            dismissTargetRect = [tempCell convertRect:tempCell.headerImgView.frame toView:self.view.window];
        } else if ([self.tempCell isKindOfClass:[BYHomeArticleImgCell class]]){
            BYHomeArticleImgCell *tempCell = (BYHomeArticleImgCell *)self.tempCell;
            self.userAnimationImgView.image = tempCell.selectImgView.image;
            dismissTargetRect = [tempCell convertRect:tempCell.selectImgView.frame toView:self.view.window];
        } else if ([self.tempCell isKindOfClass:[BYHomeNewsletterRootCell class]]){
            BYHomeNewsletterRootCell *cell = (BYHomeNewsletterRootCell *)self.tempCell;
            self.userAnimationImgView.image = cell.transferModel.ablum;
            dismissTargetRect = [cell convertRect:cell.ablumImgView.frame toView:self.view.window];
        }
        
        CGSize fromImgSize = [self convertToCalculationImg:self.userAnimationImgView.image];
        self.userAnimationImgView.frame = CGRectMake((kScreenBounds.size.width - fromImgSize.width) / 2., (kScreenBounds.size.height - fromImgSize.height) / 2., fromImgSize.width, fromImgSize.height);
        self.userAnimationImgView.alpha = 1;
        
        // 2. 寻找target frame
        __weak typeof(self)weakSelf = self;
        [UIView animateWithDuration:.3f animations:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.userAnimationImgView.frame = dismissTargetRect;
        } completion:^(BOOL finished) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.userAnimationImgView removeFromSuperview];
            strongSelf.userAnimationImgView = nil;
            [strongSelf willMoveToParentViewController:nil];
            [strongSelf.view removeFromSuperview];
            [strongSelf removeFromParentViewController];
        }];
    }
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    // 1. 计算将要变成的frame
    CGSize maxSize = [self convertToCalculationImg:self.userAnimationImgView.image];
    CGRect animaRect = CGRectMake((kScreenBounds.size.width - maxSize.width) / 2., (kScreenBounds.size.height - maxSize.height ) / 2., maxSize.width, maxSize.height);
    if (!self.userAnimationImgView.image){
        [self sheetViewDismiss];
        return;
    }
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:.3f animations:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.backView.alpha = 1;
        strongSelf.userAnimationImgView.frame = animaRect;
        strongSelf.downButton.alpha = 1.0;
    } completion:^(BOOL finished) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf addOtherImgView];
    }];
}

#pragma mark - action
- (void)downBtnAction{
    NSInteger index = self.mainScrollView.contentOffset.x/kCommonScreenWidth;
    UIScrollView *subScrollView = (UIScrollView *)[self.mainScrollView viewWithTag:kBaseTag + index];
    UIImageView *imageView = [subScrollView viewWithTag:743];
    if (!imageView.image) return;
    UIImage *image = imageView.image;
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        [PHAssetChangeRequest creationRequestForAssetFromImage:image];
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                showToastView(@"保存失败", kCommonWindow);
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                showToastView(@"保存成功", kCommonWindow);
            });
        }
    }];
}

#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}

//- (void)swipeGestureAction:(UIGestureRecognizer *)gesture{
//    
//}

-(void)showInView:(UIViewController *)viewController imgArr:(NSArray *)imgArr currentIndex:(NSInteger)currentIndex cell:(id)idCell{
    // 1. 进行赋值
    CGRect convertImgFrame = CGRectMake(0, 0, 100, 100);
    UIImage *selectedImg;
    if ([idCell isKindOfClass:[ArticleImageSelectedTableViewCell class]]){
        ArticleImageSelectedTableViewCell *cell = (ArticleImageSelectedTableViewCell *)idCell;
        convertImgFrame = [cell.bgView convertRect:cell.selectedImgView.superview.frame toView:self.view.window];
        selectedImg = cell.selectedImgView.image;
    } else if ([idCell isKindOfClass:[ArticleRootTableViewCell class]]){
        ArticleRootTableViewCell *cell = (ArticleRootTableViewCell *)idCell;
        convertImgFrame = [cell.imgBgView convertRect:cell.selectedImgView.frame toView:self.view.window];
        selectedImg = cell.selectedImgView.image;
    } else if ([idCell isKindOfClass:[ArticleDetailInfoTableViewCell class]]){
        ArticleDetailInfoTableViewCell *cell = (ArticleDetailInfoTableViewCell *)idCell;
        if (cell.transferArticleModel.article_type == article_typeNormal){
            convertImgFrame = [cell.imgBgView convertRect:cell.selectedImgView.frame toView:self.view.window];
            selectedImg = cell.selectedImgView.image;
        } else {            // 打开网站的
            convertImgFrame = [cell.webView convertRect:self.webViewSelectedRect toView:self.view.window];
            if (cell.selectedImgView.image){
                selectedImg = cell.selectedImgView.image;
            } else {
                selectedImg = [UIImage imageNamed:@"AppIcon"];
            }
        }
    } else if ([idCell isKindOfClass:[MineRealChoosePhotoTableViewCell class]]){
        MineRealChoosePhotoTableViewCell *cell = (MineRealChoosePhotoTableViewCell *)idCell;
        convertImgFrame = [cell.convertView convertRect:cell.imageView.frame toView:self.view.window];
        selectedImg = cell.transferImg;
    }  else if ([idCell isKindOfClass:[BYPersonHomeHeaderView class]]){
        BYPersonHomeHeaderView *view = (BYPersonHomeHeaderView *)idCell;
        convertImgFrame = [view.headerImgView convertRect:view.headerImgView.frame toView:self.view.window];
        selectedImg = view.headerImgView.image;
    } else if ([idCell isKindOfClass:[BYHomeArticleImgCell class]]) {
        BYHomeArticleImgCell *cell = (BYHomeArticleImgCell *)idCell;
        convertImgFrame = [cell.selectImgView convertRect:cell.selectImgView.frame toView:self.view.window];
        selectedImg = cell.selectImgView.image;
    } else if ([idCell isKindOfClass:[BYHomeNewsletterRootCell class]]) {
        BYHomeNewsletterRootCell *cell = (BYHomeNewsletterRootCell *)idCell;
        convertImgFrame =  [cell convertRect:cell.ablumImgView.frame toView:self.view.window];
        selectedImg = cell.transferModel.ablum;
    }
    
    // 1. 创建view
    [self pageSetting];
    // 2. img
    
    self.tempImgArr = imgArr;
    _lastIndex = currentIndex;
    
    self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * self.tempImgArr.count, kScreenBounds.size.height);
    [self.mainScrollView setContentOffset:CGPointMake(currentIndex * kScreenBounds.size.width, 0)];
    // 3. cell
    self.tempCell = idCell;
    
    if (!self.userAnimationImgView){
        self.userAnimationImgView = [[PDImageView alloc]init];
        self.userAnimationImgView.backgroundColor = [UIColor clearColor];
        self.userAnimationImgView.frame = convertImgFrame;
        self.userAnimationImgView.userInteractionEnabled = NO;
        [self.view addSubview:self.userAnimationImgView];
        self.userAnimationImgView.image = selectedImg;
    }
    
    [self showInView:viewController];
}


-(CGSize)convertToCalculationImg:(UIImage *)image{
    if (!image){
        return CGSizeMake(0, 0);
    }
    UIImage *originalImage = [UIImage scaleDown:image withSize:CGSizeMake(self.view.size_width, self.view.size_width * image.size.height/image.size.width)];
    
    // 设置比例
    CGFloat imageRadio = originalImage.size.width/originalImage.size.height;
    CGFloat screenRadio = CGRectGetWidth(self.view.window.frame)/CGRectGetHeight(self.view.window.frame);
    if (imageRadio >= screenRadio) {
        CGFloat currentImageHeight = CGRectGetWidth(self.view.window.frame)/imageRadio;
        self.mainScrollView.maximumZoomScale = CGRectGetHeight(self.view.window.frame)/currentImageHeight;
        return CGSizeMake(kScreenBounds.size.width, currentImageHeight);
    } else {
        CGFloat currentImageWidth = CGRectGetWidth(self.view.window.frame)*imageRadio;
        self.mainScrollView.maximumZoomScale = CGRectGetWidth(self.view.window.frame)/currentImageWidth;

        if (currentImageWidth){
            NSLog(@"123");
        }
        return CGSizeMake(currentImageWidth, kScreenBounds.size.height);
        
        
        
    }
}

-(void)addOtherImgView{                 //    // 1. 创建多张图片
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i < self.tempImgArr.count;i++){
        
        UIScrollView *scrollView = [[UIScrollView alloc] init];
        scrollView.minimumZoomScale = 0.5;
        scrollView.maximumZoomScale = 8.0f;
        scrollView.delegate = self;
        scrollView.tag = kBaseTag + i;
        scrollView.frame = CGRectMake(kCommonScreenWidth*i, 0,
                                      CGRectGetWidth(self.mainScrollView.frame),
                                      CGRectGetHeight(self.mainScrollView.frame));
        [self.mainScrollView addSubview:scrollView];
        
        
        PDImageView *originalImageView = [[PDImageView alloc] init];
        originalImageView.contentMode = UIViewContentModeScaleAspectFit;
        originalImageView.tag = 743;
        [scrollView addSubview:originalImageView];

        id imgOriginal = [self.tempImgArr objectAtIndex:i];
        if ([imgOriginal isKindOfClass:[UIImage class]]){               // 如果是图片的话
            UIImage *image = (UIImage *)imgOriginal;
            UIImage *originalImage = [UIImage scaleDown:image withSize:CGSizeMake(self.view.size_width, self.view.size_width * image.size.height/image.size.width)];
            originalImageView.image = originalImage;
            
            // 设置比例
            CGFloat imageRadio = originalImage.size.width/originalImage.size.height;
            CGFloat screenRadio = CGRectGetWidth(self.view.window.frame)/CGRectGetHeight(self.view.window.frame);
            if (imageRadio >= screenRadio) {
                CGFloat currentImageHeight = CGRectGetWidth(self.view.window.frame)/imageRadio;
                self.mainScrollView.maximumZoomScale = CGRectGetHeight(self.view.window.frame)/currentImageHeight;
            } else {
                CGFloat currentImageWidth = CGRectGetWidth(self.view.window.frame)*imageRadio;
                self.mainScrollView.maximumZoomScale = CGRectGetWidth(self.view.window.frame)/currentImageWidth;
            }
            
            if (i == self.tempCurrentIndex){
                self.userAnimationImgView.alpha = 0;
            }
        } else if ([imgOriginal isKindOfClass:[NSString class]]){        // 如果是网站url的话
            NSString *imgUrl = (NSString *)imgOriginal;
            
            
            [originalImageView uploadMainImageWithURL:imgUrl placeholder:nil imgType:PDImgTypeOriginalWithLoading callback:^(UIImage *image) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                UIImage *originalImage = [UIImage scaleDown:image withSize:CGSizeMake(strongSelf.view.size_width, strongSelf.view.size_width * image.size.height/image.size.width)];
                originalImageView.image = originalImage;
                
                // 设置比例
                CGFloat imageRadio = originalImage.size.width/originalImage.size.height;
                CGFloat screenRadio = CGRectGetWidth(strongSelf.view.window.frame)/CGRectGetHeight(strongSelf.view.window.frame);
                if (imageRadio >= screenRadio) {
                    CGFloat currentImageHeight = CGRectGetWidth(strongSelf.view.window.frame)/imageRadio;
                    strongSelf.mainScrollView.maximumZoomScale = CGRectGetHeight(strongSelf.view.window.frame)/currentImageHeight;
                } else {
                    CGFloat currentImageWidth = CGRectGetWidth(strongSelf.view.window.frame)*imageRadio;
                    strongSelf.mainScrollView.maximumZoomScale = CGRectGetWidth(strongSelf.view.window.frame)/currentImageWidth;
                }
                
                if (i == strongSelf.tempCurrentIndex){
                    strongSelf.userAnimationImgView.alpha = 0;
                }
            }];
        }
        
        originalImageView.frame =   kScreenBounds;
//        originalImageView.orgin_x = kScreenBounds.size.width * i;
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.mainScrollView){
        NSInteger index = self.mainScrollView.contentOffset.x / kScreenBounds.size.width;
        self.pageControl.currentPage = index;
        if (_lastIndex == index) return;
        
        UIScrollView *subScrollView = [scrollView viewWithTag:kBaseTag + _lastIndex];
        UIView *imageView = [subScrollView viewWithTag:743];
        if (!imageView) return;
        [subScrollView setZoomScale:1.0];
        [subScrollView setContentSize:CGSizeZero];
        _lastIndex = index;
    }
    
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    if (scrollView == self.mainScrollView) return nil;
    UIView *imageView = [scrollView viewWithTag:743];
    if (imageView) {
        return imageView;
    }
    return nil;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
    if (scrollView == self.mainScrollView) return ;
    UIView *imageView = [scrollView viewWithTag:743];
    if (!imageView) return;
    CGRect frame = imageView.frame;
    
    frame.origin.y = (scrollView.frame.size.height - imageView.frame.size.height) > 0 ? (scrollView.frame.size.height - imageView.frame.size.height) * 0.5 : 0;
    frame.origin.x = (scrollView.frame.size.width - imageView.frame.size.width) > 0 ? (scrollView.frame.size.width - imageView.frame.size.width) * 0.5 : 0;
    imageView.frame = frame;
    
    scrollView.contentSize = CGSizeMake(imageView.frame.size.width + 30, imageView.frame.size.height + 30);
}

- (UIButton *)downButton{
    if (!_downButton) {
        _downButton = [UIButton by_buttonWithCustomType];
        [_downButton setBy_imageName:@"common_imagebrowse_save" forState:UIControlStateNormal];
        [_downButton addTarget:self action:@selector(downBtnAction) forControlEvents:UIControlEventTouchUpInside];
        _downButton.alpha = 0.0f;
        _downButton.hidden = !_canSave;
    }
    return _downButton;
}


@end
