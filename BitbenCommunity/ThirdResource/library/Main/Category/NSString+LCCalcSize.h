//
//  NSString+LCCalcSize.h
//  LCZC
//
//  Created by SmartMin on 15/10/3.
//  Copyright © 2015年 SmartMin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NSString (LCCalcSize)

- (CGSize)sizeWithCalcFont:(UIFont *)font;

- (CGSize)sizeWithCalcFont:(UIFont *)font constrainedToSize:(CGSize)size;

+ (CGFloat)contentofHeightWithFont:(UIFont *)font;

+(CGSize)makeSizeWithLabel:(UILabel *)label;
- (NSString *)md5String;
@end
