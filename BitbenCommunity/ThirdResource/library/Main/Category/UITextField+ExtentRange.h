//
//  UITextField+ExtentRange.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (ExtentRange)

- (NSRange) selectedRange;
- (void) setSelectedRange:(NSRange) range;

@end
