//
//  UITableViewCell+MMHSeparatorLine.m
//  MamHao
//
//  Created by SmartMin on 15/4/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "UITableViewCell+MMHSeparatorLine.h"

#define kTopTag      @"topSeparatorTag"
#define kBottomTag   @"bottomSeparatorTag"

#define hltColor     @"e3e0de"

@implementation UITableViewCell (MMHSeparatorLine)

- (void)addSeparatorLineWithType:(SeparatorType)separatorType
{
    UIImageView *topImageView = (UIImageView *)[self viewWithStringTag:kTopTag];
    UIImageView *bottomImageView = (UIImageView *)[self viewWithStringTag:kBottomTag];
    
    CGFloat separatorInset   = (separatorType == SeparatorTypeBottom || separatorType == SeparatorTypeSingle)?0:15;
    BOOL    showTopSeparator = (separatorType == SeparatorTypeHead   || separatorType == SeparatorTypeSingle)?YES:NO;
    
    if (showTopSeparator)
    {
        if (!topImageView)
        {
            topImageView = [[UIImageView alloc] init];
            [topImageView setStringTag:kTopTag];
            [topImageView setImage:[UIImage imageWithRenderColor:[UIColor clearColor] renderSize:CGSizeMake(10., 10.)]];
            [topImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor clearColor] renderSize:CGSizeMake(10., 10.)]];
        }
        [topImageView setFrame:CGRectMake(0, 0,CGRectGetWidth([self frame]), 0.5)];
        [self addSubview:topImageView];
        
    }
    else
    {
        if (topImageView)
            [topImageView removeFromSuperview];
    }
    
    if (!bottomImageView)
    {
        bottomImageView = [[UIImageView alloc] init];
        [bottomImageView setStringTag:kBottomTag];
        [bottomImageView setImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
        [bottomImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
    }
    [bottomImageView setFrame:CGRectMake( separatorInset, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]), 0.5)];
    [self addSubview:bottomImageView];
}




#pragma mark -ares
- (void)addSeparatorLineWithTypeWithAres:(SeparatorType)separatorType andUseing:(NSString *)usingVC {
    UIImageView *topImageView = (UIImageView *)[self viewWithStringTag:kTopTag];
    UIImageView *bottomImageView = (UIImageView *)[self viewWithStringTag:kBottomTag];
    
//    CGFloat separatorInset   = (separatorType == SeparatorTypeBottom || separatorType == SeparatorTypeSingle)?0:LCFloat(56);
    BOOL    showTopSeparator = (separatorType == SeparatorTypeHead   || separatorType == SeparatorTypeSingle)?YES:NO;
    
    if (showTopSeparator) {
        if (!topImageView) {
            topImageView = [[UIImageView alloc] init];
            [topImageView setStringTag:kTopTag];
            [topImageView setImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
            [topImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
        }
        [topImageView setFrame:CGRectMake(0, 0,CGRectGetWidth([self frame]), 0.5)];
        [self addSubview:topImageView];
    } else {
        if (topImageView)
            [topImageView removeFromSuperview];
    }
    
    if (!bottomImageView) {
        bottomImageView = [[UIImageView alloc] init];
        [bottomImageView setStringTag:kBottomTag];
        [bottomImageView setImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
        [bottomImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
    }
    if([usingVC isEqualToString:@"ranking"]){
        CGFloat origin_x = LCFloat(11);
        [bottomImageView setFrame:CGRectMake(origin_x, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]) - origin_x, 0.5)];
    } else if ([usingVC isEqualToString:@"message"]){
        CGFloat origin_x = LCFloat(11) ;
        [bottomImageView setFrame:CGRectMake(origin_x, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]) - origin_x * 2, 0.5)];
    } else if ([usingVC isEqualToString:@"center"]){
        CGFloat origin_x = LCFloat(33) ;
        [bottomImageView setFrame:CGRectMake(origin_x, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]) - origin_x * 2, 0.5)];
    }
    [self addSubview:bottomImageView];
}

@end
