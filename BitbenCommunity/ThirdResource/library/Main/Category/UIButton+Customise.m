//
//  UIButton+Customise.m
//  LaiCai
//
//  Created by SmartMin on 15-8-10.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import "UIButton+Customise.h"
#import <objc/runtime.h>
#import "NetworkAdapter.h"
static char *buttonCallBackBlockKey;
static char *setBadgeLabelKey;
static char *setPathBindingKey;

@implementation UIButton (Customise)

- (void) buttonWithBlock:(void(^)(UIButton *button))buttonClickBlock{
    
    [self addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    if (buttonClickBlock){
        objc_setAssociatedObject(self, &buttonCallBackBlockKey, buttonClickBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
}


-(void)buttonClick:(UIButton *)sender{
    void(^buttonClickBlock)(UIButton *button) = objc_getAssociatedObject(sender, &buttonCallBackBlockKey);
    if (buttonClickBlock){
        [[NetworkAdapter sharedAdapter] btnTempDelegateManager:self];
        buttonClickBlock(sender);
    }
}

- (void)layoutButtonWithEdgeInsetsStyle:(ButtonEdgeInsetsStyle)style
                        imageTitleSpace:(CGFloat)space
{
    // 1. 得到imageView和titleLabel的宽、高
    CGFloat imageWith = self.imageView.frame.size.width;
    CGFloat imageHeight = self.imageView.frame.size.height;
    
    CGFloat labelWidth = 0.0;
    CGFloat labelHeight = 0.0;
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        // 由于iOS8中titleLabel的size为0，用下面的这种设置
        labelWidth = self.titleLabel.intrinsicContentSize.width;
        labelHeight = self.titleLabel.intrinsicContentSize.height;
    } else {
        labelWidth = self.titleLabel.frame.size.width;
        labelHeight = self.titleLabel.frame.size.height;
    }
    
    // 2. 声明全局的imageEdgeInsets和labelEdgeInsets
    UIEdgeInsets imageEdgeInsets = UIEdgeInsetsZero;
    UIEdgeInsets labelEdgeInsets = UIEdgeInsetsZero;
    
    // 3. 根据style和space得到imageEdgeInsets和labelEdgeInsets的值
    switch (style) {
        case ButtonEdgeInsetsStyleTop:
        {
            imageEdgeInsets = UIEdgeInsetsMake(-labelHeight-space/2.0, 0, 0, -labelWidth);
            labelEdgeInsets = UIEdgeInsetsMake(0, -imageWith, -imageHeight-space/2.0, 0);
        }
            break;
        case ButtonEdgeInsetsStyleLeft:
        {
            imageEdgeInsets = UIEdgeInsetsMake(0, -space/2.0, 0, space/2.0);
            labelEdgeInsets = UIEdgeInsetsMake(0, space/2.0, 0, -space/2.0);
        }
            break;
        case ButtonEdgeInsetsStyleBottom:
        {
            imageEdgeInsets = UIEdgeInsetsMake(0, 0, -labelHeight-space/2.0, -labelWidth);
            labelEdgeInsets = UIEdgeInsetsMake(-imageHeight-space/2.0, -imageWith, 0, 0);
        }
            break;
        case ButtonEdgeInsetsStyleRight:
        {
            imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth+space/2.0, 0, -labelWidth-space/2.0);
            labelEdgeInsets = UIEdgeInsetsMake(0, -imageWith-space/2.0, 0, imageWith+space/2.0);
        }
            break;
        default:
            break;
    }
    // 4. 赋值
    self.titleEdgeInsets = labelEdgeInsets;
    self.imageEdgeInsets = imageEdgeInsets;
}


- (void)setBadge:(NSString *)number font:(NSString *)font{
    CGFloat width = self.size_width;
    
    if (!self.badgeLabel){
        self.badgeLabel = [GWViewTool createLabelFont:font textColor:@"白"];
        self.badgeLabel.textAlignment = NSTextAlignmentCenter;
        self.badgeLabel.backgroundColor = [UIColor hexChangeFloat:@"EA6441"];
        [self addSubview:self.badgeLabel];
    }
    
    self.badgeLabel.text = number;
    if ([number isEqualToString:@"0"]){
        self.badgeLabel.hidden = YES;
    } else {
        self.badgeLabel.hidden = NO;
    }
    
    CGSize badgeSize = [number sizeWithCalcFont:self.badgeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.badgeLabel.font])];
    CGFloat maxWidth = badgeSize.width + 2 * LCFloat(3);
    self.badgeLabel.frame = CGRectMake(width - maxWidth, 0, maxWidth, badgeSize.height);
    self.badgeLabel.layer.cornerRadius = MIN(self.badgeLabel.size_width, self.badgeLabel.size_height) / 2.;
    self.badgeLabel.clipsToBounds = YES;
}

- (void)setBadge:(NSString *)number {
    [self setBadge:number font:@"8"];
}


-(void)setBadgeLabel:(UILabel *)badgeLabel{
    objc_setAssociatedObject(self, &setBadgeLabelKey, badgeLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(UILabel *)badgeLabel{
    UILabel *badgeLabel = objc_getAssociatedObject(self, &setBadgeLabelKey);
    return badgeLabel;

}

-(void)setPathBinding:(NSString *)pathBinding{
    objc_setAssociatedObject(self, &setPathBindingKey, pathBinding, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(NSString *)pathBinding{
    NSString *pathBindings = objc_getAssociatedObject(self, &setPathBindingKey);
    return pathBindings;
}

/**获得字符串的大小*/

//-(CGSize)getSizeOfStringWihtFont:(int)fontSize addMaxSize:(CGSize)maxSize{
//    CGSize size=[self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]} context:nil].size;
//    return size;
    
//}
@end
