//
//  UILabel+VerticalAlign.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (VerticalAlign)
- (void)alignTop;
- (void)alignBottom;

@end

NS_ASSUME_NONNULL_END
