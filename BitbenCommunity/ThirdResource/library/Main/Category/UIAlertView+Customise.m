//
//  UIAlertView+Customise.m
//  LaiCai
//
//  Created by SmartMin on 15-8-10.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import "UIAlertView+Customise.h"
#import <objc/runtime.h>

static char *alertViewCallBaclBlockKey;

@implementation UIAlertView (Customise)


+(instancetype)alertViewWithTitle:(NSString *)title message:(NSString *)msg buttonTitles:(NSArray *)titleArr callBlock:(void (^)(UIAlertView *, NSInteger))callBaclBlock{
    NSInteger count = titleArr.count;
    NSString *cancelTitle = nil;
    NSString *otherTitle1 = nil;
    NSString *otherTitle2 = nil;
    switch (count) {
        case 1:{
            cancelTitle = [titleArr objectAtIndex:0];
            break;
        }
        case 2:{
            cancelTitle = [titleArr objectAtIndex:0];
            otherTitle1 = [titleArr objectAtIndex:1];
            break;
        }
        case 3:{
            cancelTitle = [titleArr objectAtIndex:0];
            otherTitle1 = [titleArr objectAtIndex:1];
            otherTitle2 = [titleArr objectAtIndex:2];
            break;
        }
    }
    
    if (kCommonGetSystemVersion() >= 18) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
        if (callBaclBlock){
            objc_setAssociatedObject(alertController, &alertViewCallBaclBlockKey, callBaclBlock, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        }
        for (int i = 0; i < count; i ++) {
            UIAlertActionStyle style = i == 0 ? UIAlertActionStyleCancel : UIAlertActionStyleDefault;
            UIAlertAction *action = [UIAlertAction actionWithTitle:titleArr[i] style:style handler:^(UIAlertAction * _Nonnull action) {
                void (^callbackBlock) (UIAlertView *alertView, NSInteger buttonIndex) = objc_getAssociatedObject(alertController, &alertViewCallBaclBlockKey);
                if (callbackBlock) {
                    callbackBlock(nil,i);
                }
            }];
            [alertController addAction:action];
        }
        
        UITabBarController *tabBarController = [BYTabbarViewController sharedController];
        UINavigationController *navigationController = [tabBarController selectedViewController];
        UIViewController *viewController = [navigationController.viewControllers lastObject];
        UIViewController *currentController;
        if ([viewController isKindOfClass:[RTContainerController class]]) {
            currentController = ((RTContainerController *)viewController).contentViewController;
        }
        else{
            currentController = viewController;
        }
        [currentController presentViewController:alertController animated:YES completion:nil];
        
        return nil;
    }
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:msg delegate:self cancelButtonTitle:cancelTitle otherButtonTitles:otherTitle1, otherTitle2, nil];
    if (callBaclBlock){
        objc_setAssociatedObject(alertView, &alertViewCallBaclBlockKey, callBaclBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
    alertView.delegate = alertView;
    return alertView;
}

-(UITextField *)nameTextField{
    return [self textFieldAtIndex:0];
}

-(UITextField *)passwordTextField{
    return [self textFieldAtIndex:1];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    alertView.delegate = nil;
    void (^callbackBlock) (UIAlertView *alertView, NSInteger buttonIndex) = objc_getAssociatedObject(alertView, &alertViewCallBaclBlockKey);
    
    if (callbackBlock) {
        callbackBlock(alertView, buttonIndex);
    }
    
    // 使用我alert后重置keywindow
    UITabBarController *tabBarController = [BYTabbarViewController sharedController];
    [tabBarController.view.window makeKeyAndVisible];
}



@end
