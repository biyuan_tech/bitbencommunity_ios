//
//  UITextView+Customise.h
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/29.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^textViewDidChangeBlock)(NSInteger currentCount);
typedef void (^textViewDidSendBlock)(void);

@interface UITextView (Customise)

@property (nonatomic,copy)NSString *placeholder;            /**< placeholder*/
@property (nonatomic,assign)NSInteger limitMax;             /**< 最大文字数量*/
@property (nonatomic,strong)UILabel *placeholderLabel;


-(void)textViewDidChangeWithBlock:(textViewDidChangeBlock)block;
-(void)textViewDidPasteChangeWithBlock:(void(^)())block;
- (void)textViewSendActionBlock:(textViewDidSendBlock)block;
@end
