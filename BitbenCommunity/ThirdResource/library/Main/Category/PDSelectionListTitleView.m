//
//  PDSelectionListTitleView.m
//  SpiritCommunity
//
//  Created by 裴烨烽 on 2018/2/23.
//  Copyright © 2018年 Liaoba. All rights reserved.
//

#import "PDSelectionListTitleView.h"

@implementation PDSelectionListTitleView

- (CGSize)intrinsicContentSize {
    return CGSizeMake(LCFloat(200), 40);
}

@end
