//
//  UILabel+Copy.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/17.
//  Copyright © 2018 币本. All rights reserved.
//

#import "UILabel+Copy.h"

@implementation UILabel (Copy)

- (void)attachTapHandler {
    self.userInteractionEnabled = YES;
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(actionS:)];
    [self addGestureRecognizer:longPress];
    
}

- (void)actionS:(UILongPressGestureRecognizer *)gesture{
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        
//        [self becomeFirstResponder];
        UIMenuItem *menuItem1 = [[UIMenuItem alloc] initWithTitle:@"拷贝哦" action:@selector(copyS:)];
        UIMenuItem *menuItem2 = [[UIMenuItem alloc] initWithTitle:@"粘贴哦" action:@selector(pasteS:)];
        UIMenuItem *menuItem3 = [[UIMenuItem alloc] initWithTitle:@"剪切哦" action:@selector(cutS:)];
        UIMenuController *menuC = [UIMenuController sharedMenuController];
        
        menuC.menuItems = @[menuItem1, menuItem2, menuItem3];
        menuC.arrowDirection = UIMenuControllerArrowUp;
        
        if (menuC.menuVisible) {
            //        NSLog(@"menuC.menuVisible    判断 --  %d", menuC.menuVisible);
            return ;
        }
        
        [menuC setTargetRect:self.frame inView:self.superview];
        [menuC setMenuVisible:YES animated:YES];
        
    }
}

- (BOOL)canBecomeFirstResponder{
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    
    if ( action == @selector(copyS:)  || (action == @selector(pasteS:) && [UIPasteboard generalPasteboard].string) || action == @selector(cutS:) ) {
        //        NSLog(@"粘贴板   --  %@", [UIPasteboard generalPasteboard].string);
        return YES;
    }else{
        return NO;
    }
}
//剪切事件（暂时不用）
- (void)cutS:(id)sender{
    
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = self.text;
    //剪切 功能
    self.text = nil;
}

//拷贝
- (void)copyS:(id)sender{
    
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = self.text;
    
}
//粘贴
- (void)pasteS:(id)sender{
    
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    self.text = pboard.string;
}


- (void)setIsCopyable:(BOOL)number {
    objc_setAssociatedObject(self, @selector(isCopyable), [NSNumber numberWithBool:number], OBJC_ASSOCIATION_ASSIGN);
    [self attachTapHandler];
}

- (BOOL)isCopyable {
    return [objc_getAssociatedObject(self, @selector(isCopyable)) boolValue];
}

@end
