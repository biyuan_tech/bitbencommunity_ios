//
//  UIView+PDPrompt.m
//  PandaChallenge
//
//  Created by 盼达 on 16/3/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "UIView+PDPrompt.h"
#import <objc/runtime.h>
#import "UIView+Animation.h"

#define PADDING 5.

static char promptKey;
static char tapBlockKey;
@implementation UIView (PDPrompt)

- (void)layoutSubviews{
    UIView *containerView = objc_getAssociatedObject(self, &promptKey);
    containerView = [[UIView alloc] initWithFrame:self.bounds];
}

- (void)showPrompt:(NSString *)promptString withImage:(UIImage *)promptImage andImagePosition:(PDPromptImagePosition)position tapBlock:(void(^)())tapBlock{
    objc_setAssociatedObject(self, &tapBlockKey, tapBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    UIView *containerView = objc_getAssociatedObject(self, &promptKey);
    if (containerView) {
        [containerView removeFromSuperview];
    }
    containerView = [[UIView alloc] initWithFrame:self.bounds];
    containerView.userInteractionEnabled = NO;
    containerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    
    objc_setAssociatedObject(self, &promptKey, containerView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    UIImageView *promptImageView = nil;
    if (!promptImageView) {
        promptImageView = [[UIImageView alloc] initWithImage:promptImage];
        [containerView addSubview:promptImageView];
    }
    
    LOTAnimationView *animation = [LOTAnimationView animationNamed:@"whale"];
    animation.stringTag = @"LOTAnimationView";
    animation.frame = CGRectMake(LCFloat(80), 0, kScreenBounds.size.width - LCFloat(80) * 2, kScreenBounds.size.width - LCFloat(80) * 2);
    [containerView addSubview:animation];
    animation.loopAnimation = YES;


    UILabel *promptLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    promptLabel.backgroundColor = [UIColor clearColor];
    promptLabel.textColor = [UIColor lightGrayColor];
    promptLabel.text = promptString;
    promptLabel.numberOfLines = 0;
    promptLabel.font = [UIFont systemFontOfCustomeSize:14.];
    promptLabel.textAlignment = NSTextAlignmentCenter;
    [promptLabel sizeToFit];
    [containerView addSubview:promptLabel];
    
    CGSize imageSize = promptImageView.bounds.size;
    CGSize labelSize = promptLabel.bounds.size;
    
    if (!promptImage) {
        
        promptLabel.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .5f);
        if ([self isKindOfClass:[UITableView class]]) {
            promptLabel.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .5f - ((UITableView *) self).contentInset.bottom / 2.);
        }
        imageSize = CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(80), kScreenBounds.size.width - 2 * LCFloat(80));
        promptImageView.hidden = YES;
        animation.hidden = NO;
        if (position == PDPromptImagePositionLeft) {
            CGFloat comboWidth = imageSize.width + labelSize.width + PADDING;
            CGFloat minX = ceilf(self.bounds.size.width *.5f - comboWidth * .5f);
            animation.center = CGPointMake(minX + imageSize.width * .5f, self.center.y);
            promptLabel.center = CGPointMake(minX + comboWidth - labelSize.width * .5f, self.center.y);
        } else {
            animation.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .3f);
            promptLabel.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .35f + imageSize.height * .5f + labelSize.height * .5f + PADDING);
        }
        if (animation){
            animation.backgroundColor = [UIColor clearColor];
            [animation playWithCompletion:^(BOOL animationFinished) {

            }];
        }
    } else {
        animation.hidden = YES;
        if (position == PDPromptImagePositionLeft) {
            CGFloat comboWidth = imageSize.width + labelSize.width + PADDING;
            CGFloat minX = ceilf(self.bounds.size.width *.5f - comboWidth * .5f);
            promptImageView.center = CGPointMake(minX + imageSize.width * .5f, self.center.y);
            promptLabel.center = CGPointMake(minX + comboWidth - labelSize.width * .5f, self.center.y);
        } else {
            promptImageView.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .3f);
            promptLabel.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .35f + imageSize.height * .5f + labelSize.height * .5f + PADDING);
        }
    }

    [self addSubview:containerView];
    containerView.userInteractionEnabled = YES;
    
    // 添加按钮
    UIButton *tapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    tapButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(100)) / 2., CGRectGetMaxY(promptLabel.frame) + LCFloat(50), LCFloat(100), 40);
    __weak typeof(self)weakSelf = self;
    [tapButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        if (tapBlock){
            tapBlock();
        }
    }];
    tapButton.layer.cornerRadius = LCFloat(4);
    [tapButton setTitle:@"点击操作" forState:UIControlStateNormal];
    tapButton.layer.borderWidth = 1.f;
    tapButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"13"];
    [tapButton setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
    tapButton.layer.borderColor = [UIColor colorWithCustomerName:@"红"].CGColor;
    if(tapBlock){
        tapButton.hidden = NO;
    } else {
        tapButton.hidden = YES;
    }
    [containerView addSubview:tapButton];
    
//    // 添加手势
//    if (tapBlock){
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
//        [containerView addGestureRecognizer:tap];
//    }
}

 // 【手势按钮】
-(void)tapManager{
    void(^tapBlock)() = objc_getAssociatedObject(self, &tapBlockKey);
    if (tapBlock){
        tapBlock();
    }
}

- (void)dismissPrompt {
    
    UIView *containerView = objc_getAssociatedObject(self, &promptKey);
    if (containerView) {

        LOTAnimationView *animation = (LOTAnimationView *)[containerView viewWithStringTag:@"LOTAnimationView"];
        [animation pause];

        [UIView animateWithDuration:.3f animations:^{
            containerView.alpha = 0;
        } completion:^(BOOL finished) {
            [containerView removeFromSuperview];
        }];
    }
}






@end
