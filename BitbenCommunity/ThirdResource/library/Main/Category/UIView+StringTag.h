//
//  UIView+StringTag.h
//  LaiCai
//
//  Created by SmartMin on 15-8-10.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (StringTag)

@property (nonatomic,strong)NSString *stringTag;

-(UIView *)viewWithStringTag:(NSString *)tag;

-(UIView *)findFirstResponder;

@end
