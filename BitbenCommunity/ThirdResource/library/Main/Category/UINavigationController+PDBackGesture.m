//
//  UINavigationController+PDBackGesture.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "UINavigationController+PDBackGesture.h"
#import <objc/runtime.h>

static const char *associatedKeyPanGesture = "associatedKeyPanGestureKey";
static const char *associatedKeyEnableGesture = "associatedKeyEnableGestureKey";

@interface UINavigationController()<UIGestureRecognizerDelegate>

@property(nonatomic,strong)UIPanGestureRecognizer *panGestureRecognizer;

@end


@implementation UINavigationController (PDBackGesture)

-(BOOL)enableBackGesture{
    NSNumber *enableGesture = objc_getAssociatedObject(self, associatedKeyEnableGesture);
    if (enableGesture) {
        return [enableGesture boolValue];
    }
    return NO;
}

-(void)setEnableBackGesture:(BOOL)enableBackGesture{
    NSNumber *enableGestureNum = @(enableBackGesture);
    objc_setAssociatedObject(self, associatedKeyEnableGesture, enableGestureNum, OBJC_ASSOCIATION_RETAIN);
    if (enableBackGesture) {
        [self.view addGestureRecognizer:[self panGestureRecognizer]];
    } else {
        [self.view removeGestureRecognizer:[self panGestureRecognizer]];
    }
}

//-(UIPanGestureRecognizer *)panGestureRecognizer{
//    UIPanGestureRecognizer *panGestureRecognizer = objc_getAssociatedObject(self, associatedKeyPanGesture);
//    if (!panGestureRecognizer) {
//        id target = self.interactivePopGestureRecognizer.delegate;
//        panGestureRecognizer=[[UIPanGestureRecognizer alloc]initWithTarget:target action:@selector(handleNavigationTransition:)];
//        [panGestureRecognizer setDelegate:self];
//        self.interactivePopGestureRecognizer.enabled = NO;
//        objc_setAssociatedObject(self, associatedKeyPanGesture, panGestureRecognizer, OBJC_ASSOCIATION_RETAIN);
//    }
//    return panGestureRecognizer;
//}
//
//-(void)handleNavigationTransition:(UIPanGestureRecognizer *)gestureRecognizer{
//
//}
//
//- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
//    if (self.childViewControllers.count <= 1) {
//        return NO;
//    }
//    return YES;
//}
//
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
//    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]){
//        CGPoint point = [touch locationInView:gestureRecognizer.view];
//        if (point.x < 50.0 || point.x > self.view.frame.size.width - 50.0) {
//            return YES;
//        } else {
//            return NO;
//        }
//    }
//
//    return YES;
//}


@end
