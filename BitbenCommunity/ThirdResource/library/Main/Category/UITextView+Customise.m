//
//  UITextView+Customise.m
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/29.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import "UITextView+Customise.h"
#import <objc/runtime.h>

static char textViewLimitTag;
static char textViewDidChangeBlockTag;
static char textViewPlaceholderTag;
static char setPlaceholderLabelKey;
static char textViewSendActionKey;
static char textViewDidPasteChangeWithBlockKey;
@interface UITextView()<UITextViewDelegate>
@end

@implementation UITextView (Customise)

-(void)textViewDidChangeWithBlock:(textViewDidChangeBlock)block{
    objc_setAssociatedObject(self, &textViewDidChangeBlockTag, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)textViewSendActionBlock:(textViewDidSendBlock)block{
    objc_setAssociatedObject(self, &textViewSendActionKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(NSString *)placeholder{
    return objc_getAssociatedObject(self, &textViewPlaceholderTag);
}

-(NSInteger)limitMax{
    return [objc_getAssociatedObject(self, &textViewLimitTag) integerValue];
}

-(void)setLimitMax:(NSInteger)limitMax{
    self.delegate = self;
    NSNumber *limitMaxNum = [NSNumber numberWithInteger:limitMax];
    objc_setAssociatedObject(self, &textViewLimitTag, limitMaxNum, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self createLimitLabelWithMax:limitMax];
}

-(void)setPlaceholder:(NSString *)placeholder{
    self.delegate = self;
    if (!self.placeholderLabel){
        self.placeholderLabel = [self createPlaceholderLabelWithplaceholder:placeholder];
    }
    
    if (!self.text.length){
        self.placeholderLabel.text = placeholder;
    } else {
        self.placeholderLabel.text = @"";
    }
    [self addSubview:self.placeholderLabel];
    objc_setAssociatedObject(self, &textViewPlaceholderTag, placeholder, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - createLimitLabel
-(UILabel *)createLimitLabelWithMax:(NSInteger)maxLimit{
    UILabel *limitLabel = [[UILabel alloc]init];
    limitLabel.backgroundColor = [UIColor clearColor];
    limitLabel.text = [NSString stringWithFormat:@"%li/%li",(long)0,(long)maxLimit];
    limitLabel.textAlignment = NSTextAlignmentRight;
    limitLabel.textColor = [UIColor blackColor];
    limitLabel.font= [UIFont systemFontOfSize:LCFloat(13.)];
    limitLabel.textColor = [UIColor lightGrayColor];
    CGSize limitSize = [limitLabel.text sizeWithCalcFont:limitLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:limitLabel.font])];
    limitLabel.frame = CGRectMake(self.bounds.size.width - limitSize.width - LCFloat(5),self.bounds.size.height + LCFloat(5),limitSize.width,[NSString contentofHeightWithFont:limitLabel.font]);
    return limitLabel;
}

-(void)setTransferPlaceFont:(UIFont *)transferPlaceFont{
    self.placeholderLabel.font = transferPlaceFont;
}

#pragma mark - createPlaceholder
-(UILabel *)createPlaceholderLabelWithplaceholder:(NSString *)placeholder{
    UILabel *placeholderLabel = [[UILabel alloc]init];
    placeholderLabel.numberOfLines = 0;
    placeholderLabel.backgroundColor = [UIColor clearColor];
    placeholderLabel.font = self.font;
    placeholderLabel.text = placeholder;
    placeholderLabel.textColor = [UIColor colorWithRed:200/256.0 green:200/256.0 blue:200/256.0 alpha:1];
    CGSize placeholderSize = [placeholder sizeWithCalcFont:placeholderLabel.font constrainedToSize:CGSizeMake(self.size_width, CGFLOAT_MAX)];
    placeholderLabel.size_height = placeholderSize.height;
    placeholderLabel.frame = CGRectMake(self.orgin_x + LCFloat(5), LCFloat(7), self.size_width - 2 * LCFloat(7), placeholderSize.height);
    return placeholderLabel;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (text.length){
        self.placeholderLabel.hidden = YES;
    } 
    NSNumber *limitMaxWithNumber = objc_getAssociatedObject(self, &textViewLimitTag);
    NSInteger limitMax = [limitMaxWithNumber integerValue];
    if([text isEqualToString:@"\n"]) {
        if (textView.returnKeyType == UIReturnKeySend) {
            textViewDidSendBlock sendBlock = objc_getAssociatedObject(self, &textViewSendActionKey);
            if (sendBlock) {
                sendBlock();
            }
            return NO;
        }
        return YES;
    }
    NSString *toBeString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if([toBeString length] > limitMax) {
        textView.text = [toBeString substringToIndex:limitMax];
        return NO;
    }
    
    void(^block)() = objc_getAssociatedObject(self, &textViewDidPasteChangeWithBlockKey);
    if (block){
        block();
    }
    
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView {
    NSString *placeholder = objc_getAssociatedObject(self, &textViewPlaceholderTag);
    void(^textViewDidChangeBlock)(NSInteger currentCount) = objc_getAssociatedObject(self, &textViewDidChangeBlockTag);
    if (textView.text.length){
        self.placeholderLabel.text = @"";
    } else {
        self.placeholderLabel.text = placeholder;
    }
    if (textViewDidChangeBlock){
        textViewDidChangeBlock(textView.text.length);
    }
}

-(void)textViewDidPasteChangeWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &textViewDidPasteChangeWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


#pragma mark - GETSET
-(void)setPlaceholderLabel:(UILabel *)placeholderLabel{
    objc_setAssociatedObject(self, &setPlaceholderLabelKey, placeholderLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(UILabel *)placeholderLabel{
    UILabel *placeholderLabel =  objc_getAssociatedObject(self, &setPlaceholderLabelKey);
    return placeholderLabel;
}


@end
