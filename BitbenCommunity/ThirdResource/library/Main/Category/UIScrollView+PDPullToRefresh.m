//
//  UIScrollView+PDPullToRefresh.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "UIScrollView+PDPullToRefresh.h"
#import <MJRefresh.h>
#import "BYRefreshNormalHeader.h"
#import "BYRefreshNormalFooter.h"
#import <objc/runtime.h>

static char pageKey;
static char isXiaLaKey;              //
static char setHasLoadKey;
@implementation UIScrollView (PDPullToRefresh)


#pragma mark - 下拉刷新
-(void)appendingPullToRefreshHandler:(void (^)())block{
    __weak typeof(self)weakSelf = self;
    self.mj_header = [BYRefreshNormalHeader headerWithRefreshingBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 1.当前页面设置0
        strongSelf.currentPage = 0;
        strongSelf.isXiaLa = YES;
        
        if (block){
            block();
        }
    }];
}

#pragma mark - 上啦加载
-(void)appendingFiniteScrollingPullToRefreshHandler:(void (^)())block{              // 上啦加载
    __weak typeof(self)weakSelf = self;
    self.mj_footer = [BYRefreshNormalFooter footerWithRefreshingBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 上拉加载
        if (strongSelf.currentPage == 0){
            strongSelf.currentPage = 1;
        }
        
        strongSelf.isXiaLa = NO;
        
        // 2. 网络层加入当前tableView，以解决网络返回page + 1
        if(![[NetworkAdapter sharedAdapter].scrollTableViewMutableArr containsObject:strongSelf]){
            [[NetworkAdapter sharedAdapter].scrollTableViewMutableArr addObject:strongSelf];
        }
        
        if (block){
            block();
        }
    }];
}



#pragma mark 停止下拉刷新
-(void)stopPullToRefresh{
    __weak typeof(self)weakSelf = self;
    [self.mj_header endRefreshingWithCompletionBlock:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(strongSelf.isXiaLa){
            strongSelf.currentPage = 1;
        }
    }];
}

#pragma mark 停止上啦加载
-(void)stopFinishScrollingRefresh{
    __weak typeof(self)weakSelf = self;
    [self.mj_footer endRefreshingWithCompletionBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 2. 网络层加入当前tableView，以解决网络返回page + 1
        if([[NetworkAdapter sharedAdapter].scrollTableViewMutableArr containsObject:strongSelf]){
            [[NetworkAdapter sharedAdapter].scrollTableViewMutableArr removeObject:strongSelf];
            strongSelf.currentPage += 1;
        }
    }];
}



#pragma mark - Properties
-(void)setCurrentPage:(NSInteger)currentPage{
    objc_setAssociatedObject(self, &pageKey, @(currentPage), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSInteger)currentPage{
    NSInteger currrentPage = [objc_getAssociatedObject(self, &pageKey) integerValue];
    return currrentPage;
}

-(void)setIsXiaLa:(BOOL)isXiaLa{
    objc_setAssociatedObject(self, &isXiaLaKey, @(isXiaLa), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)isXiaLa{
    BOOL xiala = [objc_getAssociatedObject(self, &isXiaLaKey) integerValue];
    return xiala;
}

-(void)setHasLoad:(BOOL)hasLoad{
    objc_setAssociatedObject(self, &setHasLoadKey, @(hasLoad), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)hasLoad{
    BOOL hasLoad = [objc_getAssociatedObject(self, &setHasLoadKey) integerValue];
    return hasLoad;
}

@end
