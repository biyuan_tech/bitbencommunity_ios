//
//  UIScrollView+PDPullToRefresh.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (PDPullToRefresh)

@property (nonatomic,assign)NSInteger currentPage;                  // 当前的页码
@property (nonatomic,assign)BOOL isXiaLa;                           // 判断是否上啦加载

@property (nonatomic,assign)BOOL hasLoad;

-(void)appendingPullToRefreshHandler:(void(^)())block;              /**< 下啦刷新*/
-(void)appendingFiniteScrollingPullToRefreshHandler:(void(^)())block;/**< 上啦加载*/
-(void)stopFinishScrollingRefresh;                                  /**< 停止上啦加载*/
-(void)stopPullToRefresh;                                           /**< 停止下啦刷新*/


@end
