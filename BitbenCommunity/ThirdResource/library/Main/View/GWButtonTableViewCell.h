//
//  GWButtonTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBaseTableViewCell.h"

#define img_btn_normal @"bg_center_button_nor"
#define img_btn_hlt @"bg_center_button"
#define img_btn_notSelected @"bg_center_button"

typedef NS_ENUM(NSInteger,GWButtonTableViewCellType) {
    GWButtonTableViewCellTypeNormal,
    GWButtonTableViewCellTypeLayer,
};

@interface GWButtonTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;

@property (nonatomic,strong)UIButton *button;
@property (nonatomic,assign)GWButtonTableViewCellType transferType;


-(void)setButtonStatus:(BOOL)enable;

-(void)buttonClickManager:(void(^)())block;

+(CGFloat)calculationCellHeight;

@end
