//
//  GWAvatarTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWAvatarTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)PDImageView *avatar;
@property (nonatomic,strong)UIImage *transferAvatar;
@property (nonatomic,copy)NSString *transferAvatarUrl;
@property (nonatomic,strong)UILabel *fixedLabel;

+(CGFloat)calculationCellHeight;

@end
