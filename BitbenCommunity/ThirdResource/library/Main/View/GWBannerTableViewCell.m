//
//  GWBannerTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWBannerTableViewCell.h"
#import "PDScrollView.h"
static char bannerActionClickBlockKey;
@interface GWBannerTableViewCell()
@property (nonatomic,strong)PDImageView *infoView;


@end

@implementation GWBannerTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    if (!self.headerScrollView){
        self.headerScrollView = [[PDScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [GWBannerTableViewCell calculationCellHeight])];
        self.headerScrollView.backgroundColor = BACKGROUND_VIEW_COLOR;

        [self addSubview:self.headerScrollView];
        __weak typeof(self)weakSelf = self;
        [self.headerScrollView bannerImgTapManagerWithInfoblock:^(GWScrollViewSingleModel *model) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(GWScrollViewSingleModel *singleModel) = objc_getAssociatedObject(strongSelf, &bannerActionClickBlockKey);
            if(block){
                block(model);
            }
        }];
    }
    self.infoView = [[PDImageView alloc]init];
    self.infoView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [GWBannerTableViewCell calculationCellHeight]);
    self.infoView.hidden = YES;
    [self addSubview:self.infoView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight {
    _transferCellHeight = transferCellHeight;
}

-(void)setHasOneView:(BOOL)hasOneView{
    _hasOneView = hasOneView;
    self.infoView.hidden = !hasOneView;
    self.headerScrollView.hidden = hasOneView;
}

-(void)setTransferImgArr:(NSArray *)transferImgArr{
    _transferImgArr = transferImgArr;
    
    if (!self.hasOneView){
        if (!self.headerScrollView.transferImArr.count){
            NSMutableArray *tempScrollMutableArr = [NSMutableArray array];
            for (int i = 0 ; i < self.transferImgArr.count;i++){
                id info = [self.transferImgArr objectAtIndex:i];
                GWScrollViewSingleModel *scrollViewSingleModel = [[GWScrollViewSingleModel alloc]init];
                if([info isKindOfClass:[TaskHeaderInfoModelBanner class]]){
                    TaskHeaderInfoModelBanner *smartModel = (TaskHeaderInfoModelBanner *)info;
                    scrollViewSingleModel.img = smartModel.content;
                    scrollViewSingleModel.bannerModel = smartModel;
                } else if ([info isKindOfClass:[NSString class]]){
                    scrollViewSingleModel.img = info;
                }
                
                [tempScrollMutableArr addObject:scrollViewSingleModel];
            }
            self.headerScrollView.transferImArr = tempScrollMutableArr;
        }
    } else {
        NSString *infoStr = [self.transferImgArr firstObject];
        [self.infoView uploadHDImageWithURL:infoStr callback:NULL];
    }
}

-(void)bannerActionClickBlock:(void(^)(GWScrollViewSingleModel *singleModel))block{
    objc_setAssociatedObject(self, &bannerActionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)remove{
    [self.headerScrollView removeFromSuperview];
    self.headerScrollView = nil;
}

+(CGFloat) calculationCellHeight{
    return LCFloat(375) / 2.;
}
@end
