//
//  PDImageView.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIImageView+WebCache.h>
#import "ArticleDetailAvatarTableViewCell.h"
@class ArticleDetailAvatarTableViewCell;
typedef NS_ENUM(NSInteger ,PDImgType) {
    PDImgTypeNormal,                        /**<方框裁切*/
    PDImgTypeOriginal,                      /**<原图*/
    PDImgTypeRound,                         /**<圆形裁切 */
    PDImgTypeDrawRound,                     /** 圆角绘制 */
    PDImgTypeOriginalWithLoading,           /**< 圆图进行加载*/
    PDImgTypeHD,                            /** 获取高清尺寸图片 */
    PDImgTypeTask,                          /**< 任务*/
    PDImgTypeAvatar,                        /**< 头像*/
};

typedef NS_ENUM(NSInteger ,PDImgStyle) {
    PDImgStyleNormal = -1,                       // 普通样式
    PDImgStyleAvatar_Vip_Person,            // 个人
    PDImgStyleAvatar_Vip_Industry,          // 产业vip样式
    PDImgStyleAvatar_Vip_Organ,             // 机构vip样式
    PDImgStyleAvatar_Vip_Official,          // 官方vip样式
};

#define imageBaseUrl @"http://img.bitben.net/"
#define imageBaseURL_SLL @"https://img-ssl.bitben.net/"

@interface PDImageView : UIImageView

@property (nonatomic,copy)NSString *imgUrl;
/** 样式 */
@property (nonatomic ,assign) PDImgStyle style;
/** vip图标大小（默认16） */
@property (nonatomic ,assign) NSInteger vipImg_WH;


// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;
// 更新头像方法
-(void)uploadImageWithRoundURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;
// 更新图片主方法
-(void)uploadMainImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder imgType:(PDImgType)type callback:(void(^)(UIImage *image))callbackBlock;
// 更新高清图片
- (void)uploadHDImageWithURL:(NSString *)urlString callback:(void(^)(UIImage *image))callbackBlock;

// 清除图片缓存
+(void)cleanImgCacheWithBlock:(void(^)())block;
// 当前图片数量
+(NSString *)diskCount;

+(NSString *)appendingImgUrl:(NSString *)url;

+ (NSString *)getUploadBucket:(NSString *)url;
@end
