//
//  PDBaseTableViewCell.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/11/3.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDBaseTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeight;

@end
