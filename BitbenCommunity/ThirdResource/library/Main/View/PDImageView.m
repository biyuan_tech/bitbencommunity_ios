//
//  PDImageView.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "PDImageView.h"
#import "PDImageView+DefultBg.h"
#import "PDHUD.h"
#import "CenterShareImgModel.h"

static NSInteger vipWidth = 16;

@interface PDImageView ()

/** Vip图标 */
@property (nonatomic ,strong) PDImageView *vipImgView;

@end

@implementation PDImageView

- (void)layoutSubviews{
    CGFloat r = CGRectGetWidth(self.frame)/2.0;
    CGFloat detalX = sqrtf(r*r/2.0);
    CGFloat width = _vipImg_WH > 0 ? _vipImg_WH : vipWidth;
    _vipImgView.frame = CGRectMake(0, 0, width, width);
    _vipImgView.center = CGPointMake(self.center_x + detalX, self.center_y + detalX);
}

// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
     [self uploadMainImageWithURL:urlString placeholder:placeholder imgType:PDImgTypeNormal callback:callbackBlock];
}

-(void)uploadImageOriginalWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    [self uploadMainImageWithURL:urlString placeholder:placeholder imgType:PDImgTypeOriginal callback:callbackBlock];
}

// 更新用户头像图片
-(void)uploadImageWithRoundURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    [self uploadMainImageWithURL:urlString placeholder:placeholder imgType:PDImgTypeRound callback:callbackBlock];
}

- (void)uploadHDImageWithURL:(NSString *)urlString callback:(void(^)(UIImage *image))callbackBlock{
    [self uploadMainImageWithURL:urlString placeholder:nil imgType:PDImgTypeHD callback:callbackBlock];
}

#pragma mark - 更新图片 主方法
-(void)uploadMainImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder imgType:(PDImgType)type callback:(void(^)(UIImage *image))callbackBlock{
    self.imgUrl = urlString;
    if ((![urlString isKindOfClass:[NSString class]])){
        return;
    }

    // 判断是否为本地默认图
    if ([self verifyIsLocalDefultBg:urlString]) {
        self.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",urlString]];
        if (callbackBlock){
            callbackBlock(self.image);
        }
        return;
    }
    // 1. 判断placeholder
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"placehorder_bg.png"];
//        [CenterShareImgModel createImgManagerWithPlacehorderWithRect:self.frame.size];
    }
    
    // 2. 判断url
    NSString *mainImgUrl = @"";
    NSURL *imgUrl;
//    if ([urlString hasPrefix:@"http://img.bitben.net/web-"]){
//        mainImgUrl = [NSString stringWithFormat:@"%@?imageView2/1/w/%li/h/%li/q/75|imageslim",urlString,(long)self.size_width * 1,(long)self.size_height * 1];
//        imgUrl = [Tool transformationUrl:mainImgUrl];
//    } else
    
    if ([urlString hasPrefix:imageBaseUrl]){
        mainImgUrl = urlString;
        imgUrl =  [NSURL URLWithString:mainImgUrl];
    } else if ([urlString hasPrefix:@"http://"] || [urlString hasPrefix:@"https://"]){
        mainImgUrl = urlString;
        imgUrl =  [NSURL URLWithString:mainImgUrl];
    }else if (!urlString.length){
        mainImgUrl = @"";
        imgUrl = nil;
    }else {
        NSString *baseURL = [PDImageView getUploadBucket:urlString];
        if (type == PDImgTypeOriginal || type == PDImgTypeOriginalWithLoading){
            mainImgUrl = [NSString stringWithFormat:@"%@%@",baseURL,urlString];
        }
        else if (type == PDImgTypeHD){
            mainImgUrl = [NSString stringWithFormat:@"%@%@?imageView2/2/w/%li/q/75|imageslim",baseURL,urlString,(long)(self.size_width*[UIScreen mainScreen].scale)];
        } else if(type == PDImgTypeTask){
            mainImgUrl = [NSString stringWithFormat:@"%@%@?imageView2/1/w/%li/h/%li/format/png/q/100|imageslim",baseURL,urlString,(long)self.size_width * 2,(long)self.size_height * 2];
        } else if (type == PDImgTypeAvatar){
            mainImgUrl = [NSString stringWithFormat:@"%@%@?imageView2/5/w/%li/h/%li/q/75|imageslim",baseURL,urlString,(long)self.size_width * 3,(long)self.size_height * 3];
        } else {
            mainImgUrl = [NSString stringWithFormat:@"%@%@?imageView2/1/w/%li/h/%li/q/75|imageslim",baseURL,urlString,(long)self.size_width * 2,(long)self.size_height * 2];
        }
        imgUrl = [Tool transformationUrl:mainImgUrl];
    }
    
    // 如果是原图需要进行loading
    if (type == PDImgTypeOriginalWithLoading){              // 如果是进行loading的话
        placeholder = nil;
//        [PDHUD showHUDProgress:@"正在加载中…" diary:0];
    }
//    if (_style == PDImgStyleNormal ||
//        _style == PDImgStyleAvatar_Vip_Person) {
//        _vipImgView.hidden = YES;
//    }
    // 3. 进行加载图片
    @weakify(self);
    [self sd_setImageWithURL:imgUrl placeholderImage:placeholder completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        [PDHUD dismissManager];
        @strongify(self);
                
        if (image){
            if (type == PDImgTypeNormal){
                self.image = image;
            } else if (type == PDImgTypeRound){
                self.image = [Tool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round.png"]];
            }else if (type == PDImgTypeDrawRound){
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.image = [image addCornerRadius:image.size.width/2 size:image.size];
                });
            }
        }
        
//        if (self.style != PDImgStyleNormal &&
//            self.style != PDImgStyleAvatar_Vip_Person) {
//            ServerBadgeModel *model = [BYCommonTool getVipImageName:stringFormatInteger(self.style)];
//            [self.vipImgView uploadHDImageWithURL:model.picture callback:nil];
//            self.vipImgView.hidden = NO;
//        }
        
        if (callbackBlock){
            if (self.image){
                callbackBlock(self.image);
            } else if (image){
                callbackBlock(image);
            } else {
                callbackBlock(self.image);
            }
        }
    }];
}

- (void)setImage:(UIImage *)image{
    [super setImage:image];
    if (_style == PDImgStyleNormal ||
        _style == PDImgStyleAvatar_Vip_Person) {
        _vipImgView.hidden = YES;
    }else{
        ServerBadgeModel *model = [BYCommonTool getVipImageName:stringFormatInteger(self.style)];
        [self.vipImgView uploadHDImageWithURL:model.picture callback:nil];
        if (!self.hidden) {
            self.vipImgView.hidden = NO;
        }
    }
}

- (void)setHidden:(BOOL)hidden{
    [super setHidden:hidden];
    _vipImgView.hidden = hidden;
}

#pragma mark - 清除缓存
+(void)cleanImgCacheWithBlock:(void(^)())block{
    [[SDWebImageManager sharedManager].imageCache clearMemory];
    __weak typeof(self)weakSelf = self;
    [[SDWebImageManager sharedManager].imageCache clearDiskOnCompletion:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
}

+(NSString *)diskCount{
    float tmpSize = [[SDImageCache sharedImageCache] getSize];
    CGFloat tempSizeWithM = tmpSize / 1024. / 1024.;
    
    NSString *clearCacheName = tempSizeWithM >= 1 ? [NSString stringWithFormat:@"%.2fM",tempSizeWithM] : [NSString stringWithFormat:@"%.2fK",tempSizeWithM * 1024];
    return clearCacheName;
}

+(CGFloat)diskCountFloat{
    float tmpSize = [[SDImageCache sharedImageCache] getSize];
    CGFloat tempSizeWithM = tmpSize / 1024. / 1024.;
    
    return tempSizeWithM;
}

+(NSString *)appendingImgUrl:(NSString *)url{
   NSString *mainImgUrl = [NSString stringWithFormat:@"%@%@",imageBaseUrl,url];
    return mainImgUrl;
}

+ (NSString *)getUploadBucket:(NSString *)url{
    if ([url hasPrefix:@"avatar"]) { // 头像上传至https
        return imageBaseURL_SLL;
    }else if ([url hasPrefix:@"http://"] ||
              [url hasPrefix:@"https://"]){
        return @"";
    }
    return imageBaseUrl;
}

- (NSString *)getVipImageName:(PDImgStyle)style{
    NSArray *vipDatas = [AccountModel sharedAccountModel].serverModel.badge.cert_badge_list;
    for (ServerBadgeModel *model in vipDatas) {
        if ([model.badge_id integerValue] == style) {
            return model.picture;
        }
    }
    return @"";
}

- (PDImageView *)vipImgView{
    if (!_vipImgView) {
        _vipImgView = [[PDImageView alloc] init];
        _vipImgView.hidden = YES;
        [self.superview addSubview:self.vipImgView];
//        CGFloat r = CGRectGetWidth(self.frame)/2.0;
//        CGFloat detalX = sqrtf(r*r/2.0);
//        [_vipImgView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.width.height.mas_equalTo(16);
//            make.centerX.mas_equalTo(self.mas_centerX).mas_offset(detalX);
//            make.centerY.mas_equalTo(self.mas_centerY).mas_offset(detalX);
//        }];
    }
    return _vipImgView;
}

@end
