//
//  PDBaseTableViewCell.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/11/3.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "PDBaseTableViewCell.h"

#if defined(__IPHONE_10_0) && (__IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0)
@interface PDBaseTableViewCell () <CAAnimationDelegate>
#else
@interface PDBaseTableViewCell ()
#endif


@end

@implementation PDBaseTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

//#pragma mark - createView
//-(void)createView{
//
//}

+(CGFloat)calculationCellHeight{
    return LCFloat(44);
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}



@end
