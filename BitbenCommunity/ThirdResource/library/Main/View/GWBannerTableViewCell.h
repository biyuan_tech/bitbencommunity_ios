//
//  GWBannerTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDScrollView.h"
#import "GWScrollViewSingleModel.h"

@interface GWBannerTableViewCell : UITableViewCell

@property (nonatomic,strong)NSArray *transferImgArr;
@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDScrollView *headerScrollView;
@property (nonatomic,assign)BOOL hasOneView;


+(CGFloat) calculationCellHeight;

-(void)bannerActionClickBlock:(void(^)(GWScrollViewSingleModel *singleModel))block;

-(void)remove;
@end
