//
//  GWButtonTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWButtonTableViewCell.h"


static char buttonKey;
@interface GWButtonTableViewCell()

@end

@implementation GWButtonTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    [self.button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.button buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &buttonKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.button];
    [self setButtonStatus:YES];
    
}

-(void)setTransferTitle:(NSString *)transferTitle {
    _transferTitle = transferTitle;
    self.button.frame = CGRectMake(LCFloat(25), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(25), self.transferCellHeight - 2 * LCFloat(11));
    [self.button setTitle:transferTitle forState:UIControlStateNormal];
}

-(void)setTransferType:(GWButtonTableViewCellType)transferType{
    _transferType = transferType;
}

-(void)setButtonStatus:(BOOL)enable{
    if (self.transferType == GWButtonTableViewCellTypeNormal){
        if(enable){         // 可以进行点击
            self.button.userInteractionEnabled = YES;
            [self.button setBackgroundImage:[Tool stretchImageWithName:@"bg_center_button"] forState:UIControlStateNormal];
            [self.button setBackgroundImage:[Tool stretchImageWithName:@"bg_center_button"] forState:UIControlStateHighlighted];
        } else {
            self.button.userInteractionEnabled = NO;
            [self.button setBackgroundImage:[Tool stretchImageWithName:@"bg_center_button_nor"] forState:UIControlStateNormal];
        }
    } else if (self.transferType == GWButtonTableViewCellTypeLayer){
        self.button.layer.borderColor = [UIColor hexChangeFloat:@"EA6441"].CGColor;
        self.button.layer.borderWidth = 1;
        self.button.clipsToBounds = YES;
        self.button.layer.cornerRadius = LCFloat(4);
        [self.button setTitleColor:[UIColor hexChangeFloat:@"EA6441"] forState:UIControlStateNormal];
    }
}

-(void)buttonClickManager:(void(^)())block{
    objc_setAssociatedObject(self, &buttonKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


+(CGFloat)calculationCellHeight{
    return LCFloat(80);
}

@end
