//
//  AbstractViewController.h
//  BitbenLibrary
//
//  Created by 裴烨烽 on 2018/8/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AbstractViewController : UIViewController

#pragma mark - 属性
NS_ASSUME_NONNULL_BEGIN
@property (nonatomic,copy)NSString *barMainTitle;                       /**< 标题*/
@property (nonatomic,strong)UILabel *barMainTitleLabel;                          /**< 标题*/
@property (nonatomic,copy)NSString *barSubTitle;                        /**< 副标题*/
@property (nonatomic,assign)BOOL hasCancelPanDismiss;                            /**< 该页面是否左滑动删除*/
@property (nonatomic ,assign) BOOL hasCancelSocket;
/** statusBarHidden */
@property (nonatomic ,assign) BOOL statusBarHidden;


NS_ASSUME_NONNULL_END
- (nonnull UIButton *)leftBarButtonWithTitle:(nullable NSString *)title barNorImage:(nullable UIImage *)norImage barHltImage:(nullable UIImage *)hltImage action:(nullable void(^)(void))actionBlock ;

- (nonnull UIButton *)rightBarButtonWithTitle:(nullable NSString *)title barNorImage:(nullable UIImage *)norImage barHltImage:(nullable UIImage *)hltImage action:(nullable void(^)(void))actionBlock ;

- (void)hidesTabBarWhenPushed;

// LoginManager
- (void)authorizeWithCompletionHandler:(nullable void(^)(BOOL successed))handler;
// 登录成功后的通知
-(void)loginSuccessedNotif:(nullable void(^)(BOOL isSuccessed))block;
-(void)actionAutoLoginBlock:(void(^)())block;

// cell Manager
-(NSInteger)cellIndexPathSectionWithcellData:(nullable NSString *)string sourceArr:(nullable NSArray *)array;
-(NSInteger )cellIndexPathRowWithcellData:(nullable NSString *)string sourceArr:(nullable NSArray *)array;

- (void)backAction;

@end
