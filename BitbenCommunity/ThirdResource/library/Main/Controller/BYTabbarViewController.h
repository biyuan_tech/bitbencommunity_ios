//
//  BYTabbarViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYTabBar.h"

typedef NS_ENUM(NSInteger, TabbarType) {
    TabbarTypeHome = 0,
    TabbarTypeClass = 1,
    TabbarTypeRelease = 10,
    TabbarTypeMessage = 2,
    TabbarTypeCenter = 3,
};


@interface BYTabbarViewController : UITabBarController

+(instancetype)sharedController;

@property (nonatomic,assign)CGFloat tabBarHeight;
@property (nonatomic,assign)CGFloat navBarHeight;
@property (nonatomic,assign)CGFloat statusHeight;

@property (nonatomic,strong)UINavigationController *transferNavController;      /**< 传递过来的nav*/
@property (nonatomic,strong)ZYTabBar *customerTabBar;
@property (nonatomic,strong)UIViewController *currentController;

-(void)centerItemBtnClick;

-(void)showMessage;                 // 跳转到我的消息
-(void)showCenter;                  // 跳转到个人中心
-(void)showHome;

-(void)showTabbar;
-(void)hiddenTabbar;

-(void)shenheManager;
-(void)versionUpdate; // 版本更新

-(void)messageAddBirdge;
-(void)cleanMessageBirdge;

- (UIViewController *)getCurrentController;
@end
