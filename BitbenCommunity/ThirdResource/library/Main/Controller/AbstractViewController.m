//
//  AbstractViewController.m
//  BitbenLibrary
//
//  Created by 裴烨烽 on 2018/8/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "NetworkAdapter.h"

static char buttonActionBlockKey;
static char loginSuccessNotiManagerKey;
@interface AbstractViewController ()<NetworkAdapterDelegate,NetworkAdapterSocketDelegate>{

}
@property (nonatomic,strong)UIView *barTitleView;                   /**< navbar上面显示的View*/
@property (nonatomic,strong)UILabel *barSubTitleLabel;              /**< 副标题*/
@property (nonatomic,strong)UIButton *absLeftButton;

@end

@implementation AbstractViewController

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LoginNotificationWithLoginSuccessed object:nil];
    PDLog(@"释放了");
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.hasCancelSocket = YES;
        self.statusBarHidden = NO;
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [BYTabbarViewController sharedController].currentController = self;
    // 1.网络层异常代理
    if ([NetworkAdapter sharedAdapter]){
        [NetworkAdapter sharedAdapter].delegate = self;
    }
    
    // 修改 By belief 存在viewdidload方法执行完后还未执行下列代码
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!self.hasCancelSocket){
        if ([NetworkAdapter sharedAdapter]){
            [NetworkAdapter sharedAdapter].socketDelegate = self;
        }
    }
}

-(void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data{
    NSLog(@"%li%@",(long)type,data);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createBarTitleView];                      // 1. 创建navBar
    [self setupNavBarStatus];
    if (!self.hasCancelPanDismiss){
//        [self.navigationController setEnableBackGesture:YES];
//        self.navigationController.popGestureRecognizer = YES;
    } else {
//        [self.navigationController setEnableBackGesture:NO];
//        self.navigationController.popGestureRecognizer = NO;
    }
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loginStatusNotification:) name:LoginNotificationWithLoginSuccessed object:nil];
}

#pragma mark - 1. 创建navbar
- (void)createBarTitleView {
    _barTitleView = [[UIView alloc] initWithFrame:CGRectMake((kScreenBounds.size.width - BAR_TITLE_MAX_WIDTH) / 2., 0, BAR_TITLE_MAX_WIDTH, 44)];
    _barTitleView.backgroundColor = [UIColor clearColor];
    _barTitleView.clipsToBounds = YES;
    
    _barMainTitleLabel = [[UILabel alloc] initWithFrame:_barTitleView.bounds];
    _barMainTitleLabel.backgroundColor = [UIColor clearColor];
    _barMainTitleLabel.font = BAR_MAIN_TITLE_FONT;
//    _barMainTitleLabel.adjustsFontSizeToFitWidth = YES;
    _barMainTitleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    _barMainTitleLabel.text = @"";
    [_barTitleView addSubview:_barMainTitleLabel];
    
    _barSubTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _barSubTitleLabel.backgroundColor = [UIColor clearColor];
    _barSubTitleLabel.font = BAR_SUB_TITLE_FONT;
    _barSubTitleLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    [_barTitleView addSubview:_barSubTitleLabel];
    
    self.navigationItem.titleView = _barTitleView;
}

- (void)setBarMainTitle:(NSString *)barMainTitle {
    _barMainTitle = [barMainTitle copy];
    _barMainTitleLabel.text = _barMainTitle;
    
    [_barMainTitleLabel sizeToFit];
    CGRect rect = _barMainTitleLabel.bounds;
    rect.size.width = MIN(rect.size.width, BAR_TITLE_MAX_WIDTH);
    _barMainTitleLabel.bounds = rect;
    
    [self resetBarTitleView];
}

- (void)resetBarTitleView {
    CGSize mainTitleSize = _barMainTitleLabel.bounds.size;
    CGSize subTitleSize = _barSubTitleLabel.bounds.size;
    
    CGFloat titleViewHeight = mainTitleSize.height + subTitleSize.height;
    if (_barMainTitle.length && _barSubTitle.length) {
        titleViewHeight += BAR_TITLE_PADDING_TOP;
    }
    
    _barTitleView.bounds = CGRectMake(0., 0., BAR_TITLE_MAX_WIDTH, titleViewHeight);
    if ([Tool isEmpty:_barSubTitle]) {
        if (titleViewHeight < 44) {
            titleViewHeight = 44;
        }
        _barTitleView.bounds = CGRectMake(0., 0., BAR_TITLE_MAX_WIDTH, titleViewHeight);
        _barMainTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH*0.5, 22);
        
        // 移除
        for (UIView *view in self.navigationController.navigationBar.subviews){
            if ([view isKindOfClass:[HTHorizontalSelectionList class]]){
                [view removeFromSuperview];
            }
        }
    } else {
        _barMainTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH*0.5, _barMainTitleLabel.bounds.size.height*0.5);
        _barSubTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH*0.5, titleViewHeight - _barSubTitleLabel.bounds.size.height*0.5);
    }
}

#pragma mark - Button
- (void)hidesBackButton {
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
}

- (UIButton *)leftBarButtonWithTitle:(NSString *)title barNorImage:(UIImage *)norImage barHltImage:(UIImage *)hltImage action:(void(^)(void))actionBlock {
    UIButton *button = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    objc_setAssociatedObject(button, &buttonActionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    
    return button;
}

- (UIButton *)rightBarButtonWithTitle:(NSString *)title barNorImage:(UIImage *)norImage barHltImage:(UIImage *)hltImage action:(void(^)(void))actionBlock {
    UIButton *button = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    
    objc_setAssociatedObject(button, &buttonActionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButtonItem;
    
    return button;
}

- (UIButton *)buttonWithTitle:(NSString *)title buttonNorImage:(UIImage *)norImage buttonHltImage:(UIImage *)hltImage {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setImage:norImage forState:UIControlStateNormal];
    [button setImage:hltImage forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithCustomerName:@"黑"]forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateDisabled];
    [button buttonWithBlock:^(UIButton *button) {
        void (^actionBlock) (void) = objc_getAssociatedObject(button, &buttonActionBlockKey);
        if (actionBlock) actionBlock();
        
    }];
    button.backgroundColor = [UIColor clearColor];
    button.titleLabel.font = BAR_BUTTON_FONT;
    [button sizeToFit];
    
    button.bounds = CGRectMake(0, 0, 44, 44);
    
    return button;
}

#pragma mark - TabBar
- (void)hidesTabBarWhenPushed {
    [self setHidesBottomBarWhenPushed:YES];
}

- (void)hidesTabBar:(BOOL)hidden animated:(BOOL)animated {
    UITabBarController *tabBarController = self.tabBarController;
    UITabBar *tabBar = tabBarController.tabBar;
    if (!tabBarController || (tabBar.hidden == hidden)) {
        return;
    }
    
    CGFloat tabBarHeight = tabBar.bounds.size.height;
    CGFloat adjustY = hidden ? tabBarHeight : -tabBarHeight;
    
    if (!hidden) {
        tabBar.hidden = NO;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:animated ? 0.3 : 0. animations:^{
        CGRect rect = tabBar.frame;
        rect.origin.y += adjustY;
        tabBar.frame = rect;
        
        for (UIView *view in tabBarController.view.subviews) {
            if ([NSStringFromClass([view class]) hasSuffix:@"TransitionView"]) {
                if (!IS_IOS7_LATER) {
                    CGRect rect = view.frame;
                    rect.size.height += adjustY;
                    view.frame = rect;
                }
                view.backgroundColor = weakSelf.view.backgroundColor;
            }
        }
    } completion:^(BOOL finished) {
        tabBar.hidden = hidden;
        CGRect rect = weakSelf.view.frame;
        rect.size.height += adjustY;
        weakSelf.view.frame = rect;
    }];
}

- (void)hideTabBar {
    if (self.tabBarController.tabBar.hidden == YES) {
        return;
    }
    UIView *contentView;
    if ( [[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] )
        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
    else
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
    contentView.frame = CGRectMake(contentView.bounds.origin.x,  contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height + self.tabBarController.tabBar.frame.size.height);
    self.tabBarController.tabBar.hidden = YES;
}

- (void)showTabBar {
    if (self.tabBarController.tabBar.hidden == NO) {
        return;
    }
    UIView *contentView;
    if ([[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]]){
        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
    } else {
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
    }
    contentView.frame = CGRectMake(contentView.bounds.origin.x, contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height - self.tabBarController.tabBar.frame.size.height);
    self.tabBarController.tabBar.hidden = NO;
}

    
    
    
#pragma mark -
#pragma mark  NavigationBar
-(void)setupNavBarStatus{
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.modalPresentationCapturesStatusBarAppearance = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithRenderColor:[UIColor whiteColor] renderSize:CGSizeMake(kScreenBounds.size.width, 64)] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    
    self.view.autoresizesSubviews = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.view.backgroundColor = [UIColor whiteColor];
    //去掉navigationBar底部线
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc]init]];
    
    // navbar 左侧的按钮
    [self navBarLeftButtonStatus];
}

-(void)navBarLeftButtonStatus{
    __weak typeof(self)weakSelf = self;
    if (self != [self.navigationController.viewControllers firstObject]) {
        self.absLeftButton =  [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_main_back"] barHltImage:[UIImage imageNamed:@"icon_main_back"] action:^{
            [weakSelf backAction];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
        self.absLeftButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 30);

    } else {
        if (!([self isKindOfClass:[BYLiveHomeControllerV1 class]] || [self isKindOfClass:[ArticleRootViewController class]] || [self isKindOfClass:[CenterMessageRootViewController class]] || [self isKindOfClass:[CenterRootViewController class]] || [self isKindOfClass:[CenterMessageMainViewController class]] || [self isKindOfClass:[MessageNewRootViewController class]] || [self isKindOfClass:[CenterNotifyRootViewController class]])){
            self.absLeftButton =  [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_main_cancel"] barHltImage:[UIImage imageNamed:@"icon_main_cancel"] action:^{
                [weakSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
            }];
        }
    }
    
 
}

#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string sourceArr:(NSArray *)array{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < array.count ; i++){
        id arrInfo = [array objectAtIndex:i];
        if ([arrInfo isKindOfClass:[NSArray class]]){
            NSArray *dataTempArr = (NSArray *)arrInfo;
            for (int j = 0 ; j <dataTempArr.count; j++){
                id item = [dataTempArr objectAtIndex:j];
                if ([item isKindOfClass:[NSString class]]){
                    NSString *itemString = (NSString *)item;
                    if ([itemString isEqualToString:string]){
                        cellIndexPathOfSection = i;
                        break;
                    }
                }
            }
        } 
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string sourceArr:(NSArray *)array{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < array.count ; i++){
        id arrInfo = [array objectAtIndex:i];
        if ([arrInfo isKindOfClass:[NSArray class]]){
            NSArray *dataTempArr = [array objectAtIndex:i];
            for (int j = 0 ; j <dataTempArr.count; j++){
                id item = [dataTempArr objectAtIndex:j];
                if ([item isKindOfClass:[NSString class]]){
                    NSString *itemString = (NSString *)item;
                    if ([itemString isEqualToString:string]){
                        cellRow = j;
                        break;
                    }
                }
            }
        }
    }
    return cellRow;;
}

#pragma mark - 自动登录
- (void)authorizeWithCompletionHandler:(void(^)(BOOL successed))handler{
    if ([self isKindOfClass:[LoginRootViewController class]] ){
        [AccountModel sharedAccountModel].hasShowLogin = YES;
        return;
    }
    
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){
        if (handler){
            handler(YES);
        }
    } else {
        if ([Tool userDefaultGetWithKey:LoginPassword].length && [Tool userDefaultGetWithKey:LoginAccount].length && [Tool userDefaultGetWithKey:LoginType].length){
            [[AccountModel sharedAccountModel] autoLoginWithBlock:^(BOOL isSuccessed) {
                if (handler){
                    handler(isSuccessed);
                }
            }];
        } else {
            if (handler){
                handler(NO);
            }
            [StatusBarManager statusBarHidenWithText:@"请先进行登录"];
            [AccountModel sharedAccountModel].hasShowLogin = NO;
            LoginRootViewController *loginMainViewController = [[LoginRootViewController alloc]init];
            __weak typeof(self)weakSelf = self;
            [loginMainViewController loginSuccess:^(BOOL success) {
                if (!weakSelf){
                    return ;
                }
                if (success){
                    if (handler){
                        handler(YES);
                    }
                } else {
                    if (handler){
                        handler(NO);
                    }
                    [StatusBarManager statusBarHidenWithText:@"取消登录&登录失败"];
                }
            }];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginMainViewController];
            [[BYTabbarViewController sharedController] presentViewController:nav animated:YES completion:NULL];
        }
    }
}

-(void)actionAutoLoginBlock:(void(^)())block{
    if ([Tool userDefaultGetWithKey:LoginPassword].length && [Tool userDefaultGetWithKey:LoginAccount].length && [Tool userDefaultGetWithKey:LoginType].length){
        [[AccountModel sharedAccountModel] autoLoginWithBlock:^(BOOL isSuccessed) {
            if (block){
                block();
            }
        }];
    }
}
    
// 登录成功后的通知
-(void)loginSuccessedNotif:(nullable void(^)(BOOL isSuccessed))block{
    objc_setAssociatedObject(self,&loginSuccessNotiManagerKey , block, OBJC_ASSOCIATION_COPY);
}

-(void)loginStatusNotification:(NSNotification *)note{
    void(^block)() = objc_getAssociatedObject(self, &loginSuccessNotiManagerKey);
    
    if (block){
        if ([note.userInfo.allKeys containsObject:@"loginStatus"]){
            NSString *status = [note.userInfo objectForKey:@"loginStatus"];
            BOOL hasLogin = NO;
            if ([status isEqualToString:@"1"]){
                hasLogin = YES;
            } else {
                hasLogin = NO;
            }
            block(hasLogin);
        }
    }
}


#pragma mark - NetworkDelegate
-(void)notLoginNeedLoginManager{
    if ([AccountModel sharedAccountModel].hasShowLogin == YES){
        return;
    }
    
    [self authorizeWithCompletionHandler:NULL];
}

- (void)backAction{
    
}

-(void)btnEnableStatusManager:(UIButton *)currentButton{
    [NetworkAdapter sharedAdapter].currentTempButton = currentButton;
}

- (BOOL)prefersStatusBarHidden{
    return self.statusBarHidden;
}
@end
