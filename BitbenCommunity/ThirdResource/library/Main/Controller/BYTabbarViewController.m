//
//  BYTabbarViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYTabbarViewController.h"
#import "BYNavigationController.h"
#import "PDGradientNavBar.h"
#import "PopMenu.h"
#import "ArticleReleaseRootViewController.h"
#import "ArticleDetailRootViewController.h"
#import "BYUpdateView.h"
#import "CenterMessageMainViewController.h"
#import "ArticleNewReleaseViewController.h"
#import "MessageNewRootViewController.h"
#import "BYVideoHomeController.h"
#import "BYArticleRootViewController.h"
#import "ArticleQuicklyInfoReleaseViewController.h"
#import "NetworkAdapter+Article.h"
#import "BYHomeNewsletterController.h"
#import "BYCreateActivityController.h"
#import "ArticleInfoListViewController.h"

@interface BYTabbarViewController ()<ZYTabBarDelegate>
@property (nonatomic, strong) PopMenu *popMenu;

@end

@implementation BYTabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomerTabBar];
    [self pageSetting];
    
    [self.customerTabBar addCenterBtn];
    self.customerTabBar.plusBtn.frame = CGRectMake((kScreenBounds.size.width - self.customerTabBar.plusBtn.size_width) / 2., - LCFloat(10), self.customerTabBar.plusBtn.size_width, self.customerTabBar.plusBtn.size_height);
    
    [[UITabBar appearance]  addSubview:self.customerTabBar.plusBtn];
}

+(instancetype)sharedController{
    static BYTabbarViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[BYTabbarViewController alloc] init];
    });
    return _sharedAccountModel;
}


#pragma mark page
-(void)pageSetting{
    BYNavigationController *navigationController = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [[navigationController navigationBar] setTranslucent:NO];
    
    UIColor *color1 = [UIColor colorWithCustomerName:@"白"];
    UIColor *color6 = [UIColor colorWithCustomerName:@"白"];
    NSArray *navBarArr = [NSArray arrayWithObjects:(id)color6.CGColor,(id)color1.CGColor,nil];
    
    
    UIImage *unselectedImage = [UIImage imageNamed:@"icon_tabbar_home_nor"];
    UIImage *selectedImage = [UIImage imageNamed:@"icon_tabbar_home_hlt"];
    
    // 1. 【首页】
//    BYLiveHomeController *homeViewController = [BYLiveHomeController sharedController];
    BYLiveHomeControllerV1 *homeViewController = [BYLiveHomeControllerV1 shareManager];
    BYNavigationController *pandaNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [pandaNav setViewControllers:@[homeViewController]];
    pandaNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    pandaNav.navigationBar.layer.shadowOpacity = 2.0;
    pandaNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    homeViewController.tabBarItem.tag = 1;
    homeViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"首页" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // 2. 【分类】
//    ArticleRootViewController *productViewController = [ArticleRootViewController sharedController];
    BYArticleRootViewController *productViewController = [[BYArticleRootViewController alloc] init];
//    productViewController.transferType = ArticleRootViewControllerTypeNormal;
    BYNavigationController *productNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [productNav setViewControllers:@[productViewController]];
    
    productNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    productNav.navigationBar.layer.shadowOpacity = 2.0;
    productNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    productViewController.tabBarItem.tag = 2;
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_ community_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_ community_hlt"];
    productViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"社区" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        
    // 3.【消息】
    MessageNewRootViewController *findViewController = [[MessageNewRootViewController alloc]init];;
    BYNavigationController *findNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [findNav setViewControllers:@[findViewController]];
    findNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    findNav.navigationBar.layer.shadowOpacity = 2.0;
    findNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    findViewController.tabBarItem.tag = 4;
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_class_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_class_hlt"];
    findViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"消息" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // 视频
    BYVideoHomeController *videoHomeController = [[BYVideoHomeController alloc] init];
    BYNavigationController *videoHomeNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [videoHomeNav setViewControllers:@[videoHomeController]];
    videoHomeNav.popGestureRecognizer = YES;
    videoHomeNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    videoHomeNav.navigationBar.layer.shadowOpacity = 2.0;
    videoHomeNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    videoHomeController.tabBarItem.tag = 4;
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_live_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_live_hlt"];
    videoHomeController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"直播" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // 4.【个人中心】
    CenterRootViewController *centerViewController = [CenterRootViewController sharedController];
    BYNavigationController *centerNav = [[BYNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [centerNav setViewControllers:@[centerViewController]];
    
    centerNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    centerNav.navigationBar.layer.shadowOpacity = 2.0;
    centerNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    centerViewController.tabBarItem.tag = 5;
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_center_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_center_hlt"];
    centerViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"我的" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    
//    if ([AccountModel sharedAccountModel].isShenhe){
//        self.viewControllers = @[productNav,centerNav];
//    } else {
        self.viewControllers = @[pandaNav,videoHomeNav,productNav,centerNav];
//    }
    
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor hexChangeFloat:@"EE4C47"],NSFontAttributeName : [UIFont fontWithCustomerSizeName:@"12"]} forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor hexChangeFloat:@"353535"],NSFontAttributeName : [UIFont fontWithCustomerSizeName:@"12"]} forState:UIControlStateNormal];
    
    UITabBar *tabBar = self.tabBar;
    
    centerNav.popGestureRecognizer = YES;
    pandaNav.popGestureRecognizer = YES;
    videoHomeNav.popGestureRecognizer = YES;
    productNav.popGestureRecognizer = YES;
    findNav.popGestureRecognizer = YES;
    
    
    [[PDGradientNavBar appearance] setBarTintGradientColors:navBarArr];
    // 设置背景颜色
    
    if ([tabBar respondsToSelector:@selector(setBarTintColor:)]){
        [tabBar setBarTintColor:[UIColor whiteColor]];
    }else{
        for (UIView *view in tabBar.subviews) {
            if ([NSStringFromClass([view class]) hasSuffix:@"TabBarBackgroundView"]) {
                [view removeFromSuperview];
                break;
            }
        }
    }
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width,self.tabBarHeight)];
    backView.backgroundColor = [UIColor whiteColor];;
    [self.tabBar insertSubview:backView atIndex:0];
    self.tabBar.opaque = NO;
}

-(void)createCustomerTabBar{
    self.customerTabBar = [[ZYTabBar alloc]init];;
    self.customerTabBar.tabbarDelegate = self;
    self.customerTabBar.backgroundColor = [UIColor whiteColor];
    self.customerTabBar.tintColor = [UIColor whiteColor];
    self.customerTabBar.pathButtonArray = nil;
    self.customerTabBar.basicDuration = 0.5;
    self.customerTabBar.bloomRadius = 100;
    self.customerTabBar.allowCenterButtonRotation = NO;
    self.customerTabBar.bloomAngel = 100;
    self.customerTabBar.allowSubItemRotation = NO;
    [self setValue:self.customerTabBar forKeyPath:@"tabBar"];
}


-(CGFloat)tabBarHeight{
    if (IS_iPhoneX){
        return  49 + 34;
    } else {
        return  49 ;
    }
}
-(CGFloat)navBarHeight{
    if (IS_iPhoneX){
        return  44 + 44;
    } else {
        return  20 + 44;
    }
}

-(CGFloat)statusHeight{
    if (IS_iPhoneX){
        return  44 ;
    } else {
        return  20 ;
    }
}

-(void)showMessage{
    self.selectedIndex = TabbarTypeMessage;
}

-(void)showCenter{
    self.selectedIndex = TabbarTypeCenter;
}

-(void)showHome{
    self.selectedIndex = TabbarTypeHome;
}

-(void)showTabbar{
    NSArray *views = self.tabBarController.view.subviews;
    UIView *contentView = [views objectAtIndex:0];
    contentView.size_height -= self.tabBarHeight;
    self.tabBarController.tabBar.hidden = NO;
}

-(void)hiddenTabbar{
    //    NSArray *views = [BYTabbarViewController sharedController].tabBar.view.subviews;
    UIView *contentView = [BYTabbarViewController sharedController].customerTabBar;
    contentView.size_height += self.tabBarHeight;
    self.tabBarController.tabBar.hidden = YES;
}

-(void)directToController:(NSInteger)controllerIndex{
    self.selectedIndex = controllerIndex;
}

-(void)centerItemBtnClick{
    // 埋点
    [MTAManager event:MTATypeTabAdd params:nil];
    [self showMenu];
}

#pragma mark - 显示菜单
- (void)showMenu {
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    
    NSMutableArray *items = [[NSMutableArray alloc] initWithCapacity:3];
    
    MenuItem *menuItem =  [MenuItem itemWithTitle:@"长文" iconName:@"icon_tabbar_menu_1" glowColor:RGB(232, 198, 131, .4f)];
    
    [items addObject:menuItem];
    
    
    MenuItem * menuItem1 = [MenuItem itemWithTitle:@"微文" iconName:@"icon_tabbar_menu_2" glowColor: RGB(232, 198, 131, .4f)];
    
    [items addObject:menuItem1];
    
    
    if (![AccountModel sharedAccountModel].isShenhe){
        MenuItem *menuItem2 = [MenuItem itemWithTitle:@"直播" iconName:@"icon_tabbar_menu_3" glowColor: RGB(232, 198, 131, .4f)];
        [items addObject:menuItem2];
    }
    
    MenuItem *menuItem3 =  [MenuItem itemWithTitle:@"快讯" iconName:@"icon_tabbar_menu_4" glowColor:RGB(232, 198, 131, .4f)];
    
    [items addObject:menuItem3];

    MenuItem *menuItem4 =  [MenuItem itemWithTitle:@"会议" iconName:@"icon_tabbar_menu_5" glowColor:RGB(232, 198, 131, .4f)];
    
    [items addObject:menuItem4];
    
    MenuItem *menuItem5 =  [MenuItem itemWithTitle:@"测试" iconName:@"icon_tabbar_menu_5" glowColor:RGB(232, 198, 131, .4f)];
    
//    [items addObject:menuItem5];
    
//    MenuItem *menuItem3 = [MenuItem itemWithTitle:@"发布测试" iconName:@"icon_tabbar_menu_3" glowColor: RGB(232, 198, 131, .4f)];
//    [items addObject:menuItem3];
    
    if (!self.popMenu) {
        self.popMenu = [[PopMenu alloc] initWithFrame:window.bounds items:items];
        self.popMenu.menuAnimationType = kPopMenuAnimationTypeNetEase;
        self.popMenu.perRowItemCount = 3;;
    }
    if (self.popMenu.isShowed) {
        return;
    }
    __weak typeof(self)weakSelf = self;
    weakSelf.popMenu.didSelectedItemCompletion = ^(MenuItem *selectedItem) {
        if (!weakSelf){
            return ;
        }
        if (selectedItem == nil){
            return;
        }
        if([selectedItem.title isEqualToString:@"长文"]){                 // 跳转长文
            // 埋点
            [MTAManager event:MTATypeTabAddLongArticle params:nil];
            AbstractViewController *absVC = [[AbstractViewController alloc]init];
              __weak typeof(self)weakSelf = self;
             [absVC authorizeWithCompletionHandler:^(BOOL successed) {
                 if (!weakSelf){
                     return ;
                 }
                 
                 if (successed){
                     ArticleNewReleaseViewController *articleReleaseVC = [[ArticleNewReleaseViewController alloc]init];
                      UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:articleReleaseVC];
                      
                      [[BYTabbarViewController sharedController] presentViewController:nav animated:YES completion:NULL];
                 }
             }];
             return;
         

            
//            ArticleDetailRootViewController *articleVC = [[ArticleDetailRootViewController alloc]init];
//            articleVC.transferPageType = ArticleDetailRootViewControllerTypeArticle;
//            self.selectedIndex = 1;
//            [[ArticleRootViewController sharedController].navigationController pushViewController:articleVC animated:YES];
        } else if ([selectedItem.title isEqualToString:@"微文"]){        // 跳转短图文
            // 埋点
            [MTAManager event:MTATypeTabAddShortArticle params:nil];
            
            [self shortImgTextInfoManager];
        } else if ([selectedItem.title isEqualToString:@"直播"]){         // 跳转直播
            // 埋点
            [MTAManager event:MTATypeTabAddLive params:nil];
            
            [BYCommonViewController centerItemBtnClickAction];
        } else if ([selectedItem.title isEqualToString:@"测试"]){
            
            ArticleInfoListViewController *articleReleaseVC = [[ArticleInfoListViewController alloc]init];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:articleReleaseVC];

            [[BYTabbarViewController sharedController] presentViewController:nav animated:YES completion:NULL];
        } else if ([selectedItem.title isEqualToString:@"快讯"]){
            AbstractViewController *absVC = [[AbstractViewController alloc]init];
             __weak typeof(self)weakSelf = self;
            [absVC authorizeWithCompletionHandler:^(BOOL successed) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (successed){
                    [strongSelf releaseQuicklyInfoManager];
                }
            }];
            return;
         } else if ([selectedItem.title isEqualToString:@"会议"]){
            AbstractViewController *absVC = [[AbstractViewController alloc]init];
            __weak typeof(self)weakSelf = self;
            [absVC authorizeWithCompletionHandler:^(BOOL successed) {
                if (!weakSelf){
                    return ;
                }
                if (successed){
                    BYCreateActivityController *createActivityVC = [[BYCreateActivityController alloc] init];
                    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:createActivityVC];
                    [createActivityVC authorizeWithCompletionHandler:^(BOOL successed) {
                        [[BYTabbarViewController sharedController] presentViewController:nav animated:YES completion:NULL];
                    }];
                              
                }
            }];
             return;
         }
    };
    
    CGPoint startPoint = CGPointMake(kScreenBounds.size.width / 2., self.customerTabBar.orgin_y);
    [_popMenu showMenuAtView:window startPoint:startPoint endPoint:startPoint];
    
}


#pragma mark - 发布短图文
-(void)shortImgTextInfoManager{
    AbstractViewController *absVC = [[AbstractViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [absVC authorizeWithCompletionHandler:^(BOOL successed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (successed){
            ArticleReleaseRootViewController *articleReleaseVC = [[ArticleReleaseRootViewController alloc]init];
            [articleReleaseVC actionWithReleaseSuccessedBlock:^{            // 跳转到观点
                strongSelf.selectedIndex = 1;
            }];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:articleReleaseVC];
            [[BYTabbarViewController sharedController] presentViewController:nav animated:YES completion:NULL];
        }
    }];
}

#pragma mark - 发布快讯
-(void)releaseQuicklyInfoManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] autoGetUserTypeHasReleaseQuicklyInfoManagerblock:^(BOOL hasRelease, NSString *articleId) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (hasRelease){                // 表示可以发布
            ArticleQuicklyInfoReleaseViewController *articleQuicklyInfoVC = [[ArticleQuicklyInfoReleaseViewController alloc]init];
            [articleQuicklyInfoVC actionWithReleaseSuccessedBlock:^{
                // 1. tabbar 调到首页
                [[BYTabbarViewController sharedController] showHome];
                // 1. segment跳到2
               [[BYLiveHomeControllerV1 shareManager] selectSegmentAtIndex:2];
                // 3.快讯页面刷新
                BYHomeNewsletterController *kuaixunVC = (BYHomeNewsletterController *)[[BYLiveHomeControllerV1 shareManager] getSegmentSubControllerAtIndex:2];
                [kuaixunVC autoReload];
            }];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:articleQuicklyInfoVC];
            [[BYTabbarViewController sharedController] presentViewController:nav animated:YES completion:NULL];
        } else {                        //
            ArticleDetailRootViewController *articleVC = [[ArticleDetailRootViewController alloc]init];
            articleVC.transferArticleId = articleId;
            [strongSelf.currentController.navigationController pushViewController:articleVC animated:YES];
        }
    }];
}

-(void)shenheManager{



}

- (void)versionUpdate{
    ServerRootConfigModel *config = [AccountModel sharedAccountModel].serverModel.config;
    if ([BYUpdateView verifyIsUpdate:config]) {
        BYUpdateView *updateView = [BYUpdateView shareManager];
        updateView.model = config;
        [updateView showAnimation];
    }
}

-(void)messageAddBirdge{
    [CenterMessageRootViewController sharedController].tabBarItem.badgeValue = [NSString stringWithFormat:@"%li",(long)[[CenterMessageRootViewController sharedController].tabBarItem.badgeValue integerValue] + 1];
}

-(void)cleanMessageBirdge{
    [CenterMessageRootViewController sharedController].tabBarItem.badgeValue = nil;
}

- (UIViewController *)getCurrentController{
    return self.currentController;
}

- (UIViewController *)currentController{
    UITabBarController *tabBarController = [BYTabbarViewController sharedController];
    UINavigationController *navigationController = [tabBarController selectedViewController];
    UIViewController *viewController = [navigationController.viewControllers lastObject];
    UIViewController *currentController;
    if ([viewController isKindOfClass:[RTContainerController class]]) {
        currentController = ((RTContainerController *)viewController).contentViewController;
    } else if ([viewController isKindOfClass: [BYLiveHomeControllerV1 class]]){
        currentController = [BYLiveHomeControllerV1 shareManager];
    }
    else{
        currentController = viewController;
    }
    return currentController;
}


@end
