//
//  GWScrollViewSingleModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/14.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "TaskHeaderInfoModel.h"

@interface GWScrollViewSingleModel : FetchModel

@property (nonatomic,copy)NSString *img;
@property (nonatomic,copy)NSString *content;

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *info;
@property (nonatomic,assign)NSInteger type;

@property (nonatomic,strong)TaskHeaderInfoModelBanner *bannerModel;

@end
