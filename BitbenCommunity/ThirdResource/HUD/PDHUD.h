//
//  PDHUD.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <WSProgressHUD/WSProgressHUD.h>

@interface PDHUD : WSProgressHUD

#pragma mark - Text
+(void)showText:(NSString *)text;                               /**< 显示文字*/
-(void)showText:(NSString *)text;
+(void)showLongText:(NSString *)text;
+(void)showConnectionErr:(NSString *)text;

#pragma mark -Progress
+(void)showHUDProgress:(NSString *)text diary:(NSInteger)diary; /**< */

#pragma mark -Success
+(void)showHUDError:(NSString *)text;                           /**< 显示失败的HUD*/
+(void)showHUDSuccess:(NSString *)text;                         /**< 显示成功的HUD*/

#pragma mark - dismiss
+(void)dismissManager;                                              /**< 这是消失方法*/

+(void)showHUDBindingProgress:(NSString *)text diary:(NSInteger)diary; /**< */
+(void)showHUDBindingRoleDetailProgress:(NSString *)text diary:(NSInteger)diary;

@end
