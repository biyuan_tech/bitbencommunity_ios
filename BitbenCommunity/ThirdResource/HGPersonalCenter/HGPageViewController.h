//
//  HGPageViewController.h
//  HGPersonalCenterExtend
//
//  Created by Arch on 2017/6/16.
//  Copyright © 2017年 mint_bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HGPageViewControllerDelegate <NSObject>
- (void)pageViewControllerLeaveTop;
@end

@interface HGPageViewController : UIViewController
@property (nonatomic, weak) id<HGPageViewControllerDelegate> delegate;
@property (nonatomic) NSInteger pageIndex;
/** 能下拉刷新滑动 */
@property (nonatomic ,assign) BOOL canDownUpdate;


- (void)makePageViewControllerScroll:(BOOL)canScroll;
- (void)makePageViewControllerScrollToTop;


- (void)scrollViewDidScroll:(UIScrollView *)scrollView;

- (void)autoReload;
@end
