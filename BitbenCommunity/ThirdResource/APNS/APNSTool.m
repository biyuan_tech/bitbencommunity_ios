//
//  APNSTool.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "APNSTool.h"
#import "PushNotifManager.h"
#import "NetworkEngine.h"


#define kAPNS_KEY    @"25426209" // 23330943
#define kAPNS_SECRET @"0fe4b6b6d2504973848b5ec7d21e663e" // 1a87d709667be873ead5f738df4e1c7c

@implementation APNSTool

+ (instancetype)shareInstance{
    static APNSTool *pushManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        pushManager = [[APNSTool alloc] init];
    });
    return pushManager;
}

- (void)registWithPUSHWithApplication:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions deviceIdBlock:(void(^)())block{
    // 1.注册苹果推送
    [self registerAPNSWithApplication:application launchOptions:launchOptions];
    // 2.初始化SDK
    [self initSdkWithBlock:block];
    // 3.监听推送通道打开动作
    [self listenerOnChannelOpened];
    // 4.监听推送消息到达
    [self registerMessageReceive];
    // 点击通知将App从关闭状态启动时，将通知打开回执上报
    [CloudPushSDK sendNotificationAck:launchOptions];
}


// push sdk 初始化
- (void)initSdkWithBlock:(void(^)())block{                   //sdk初始化
    [CloudPushSDK asyncInit:kAPNS_KEY appSecret:kAPNS_SECRET callback:^(CloudPushCallbackResult *res) {
        if (res.success) {
            NSString *deviceId = [CloudPushSDK getDeviceId];
            [APNSTool shareInstance].deviceToken = deviceId;
            NSString *result = [CloudPushSDK getApnsDeviceToken];
            NSLog(@"device-======>%@",result);
            if (block){
                block();
            }
        } else {
            
        }
    }];
}

/**
 *	注册推送通道打开监听
 */
- (void) listenerOnChannelOpened {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onChannelOpened:) name:@"CCPDidChannelConnectedSuccess" object:nil];
}

/**
 *    注册推送消息到来监听
 */
- (void)registerMessageReceive {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMessageReceived:) name:@"CCPDidReceiveMessageNotification" object:nil];
}

#pragma mark 注册苹果的推送
- (void)registerAPNSWithApplication:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions{
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
//        // iOS 8 Notifications
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored"-Wdeprecated-declarations"
//        [application registerUserNotificationSettings:
//         [UIUserNotificationSettings settingsForTypes:
//          (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge)
//                                           categories:nil]];
//        [application registerForRemoteNotifications];
//#pragma clang diagnostic pop
//    }
//    else {
//        // iOS < 8 Notifications
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored"-Wdeprecated-declarations"
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
//         (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
//#pragma clang diagnostic pop
//    }
    
    /**
     注册APNS离线推送  iOS8 注册APNS
     */
    
//    if ([application respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [application registerForRemoteNotifications];
        UIUserNotificationType notificationTypes = UIUserNotificationTypeBadge |
        UIUserNotificationTypeSound |
        UIUserNotificationTypeAlert;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:notificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
//    }
//    else{
//        UIRemoteNotificationType notificationTypes = UIRemoteNotificationTypeBadge |
//        UIRemoteNotificationTypeSound |
//        UIRemoteNotificationTypeAlert;
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:notificationTypes];
//    }
}

/*
 *  苹果推送注册成功回调，将苹果返回的deviceToken上传到CloudPush服务器
 */
-(void)registerWithDeviceId:(NSData *)deviceToken{
    [CloudPushSDK registerDevice:deviceToken withCallback:nil];
    
}

/*
 *  App处于启动状态时，通知打开回调
 */
-(void)registerWithReceiveRemoteNotification:(NSDictionary*)userInfo {
    [CloudPushSDK sendNotificationAck:userInfo];
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        return;
    } else {
        // 进行跳转
        NSString *smart = [userInfo objectForKey:@"smart"];
        NSString *dicStr = [smart aci_decryptWithAES];
        NSDictionary *response = [NetworkEngine dictionaryWithJsonString:dicStr];
        DirectModel *directModel = (DirectModel *)[[DirectModel alloc] initWithJSONDict:response];
        // 进行跳转
        [DirectManager directMaxWithModel:directModel block:^{
            NSLog(@"123");
        }];
    }
}

/**
 *    处理到来推送消息
 *
 */
- (void)onMessageReceived:(NSNotification *)notification {
    CCPSysMessage *message = [notification object];
    NSString *title = [[NSString alloc] initWithData:message.title encoding:NSUTF8StringEncoding];
    NSString *body = [[NSString alloc] initWithData:message.body encoding:NSUTF8StringEncoding];

    NetworkEngine *engine = [[NetworkEngine alloc]init];
    NSDictionary *bodyDic = [engine dictionaryWithJsonString:body];
    NSString *desc = [bodyDic objectForKey:@"content"];
    NSInteger is_remindStr = (NSInteger)[[bodyDic objectForKey:@"is_remind"] integerValue];
    NSInteger type = (NSInteger)[[bodyDic objectForKey:@"type_code"] integerValue];
    NSString *theme_id = [bodyDic objectForKey:@"theme_id"];
    NSString *user_message_id = [bodyDic objectForKey:@"user_message_id"];
    NSInteger live_type = (NSInteger)[[bodyDic objectForKey:@"live_type"] integerValue];
    NSInteger theme_type = (NSInteger)[[bodyDic objectForKey:@"theme_type"] integerValue];

    // 消息未读数量+1；
    [AccountModel sharedAccountModel].unReadMessage += 1;

    
    if (is_remindStr == 1){
        
    } else {
        [PushNotifManager showMessageWithTitle:title desc:desc backBlock:^{
            DirectModel *directModel = [[DirectModel alloc]init];
            directModel.type_code = type;
            directModel.theme_id = theme_id;
            directModel.user_message_id = user_message_id;
            directModel.live_type = live_type;
            directModel.theme_type = theme_type;
            
            [DirectManager directMaxWithModel:directModel block:NULL];
        }];
    }

    
    [[BYTabbarViewController sharedController] messageAddBirdge];
    
    NSLog(@"Receive message title: %@, content: %@.", title, body);
}

- (void)onChannelOpened:(NSNotification *)notification {
    CCPSysMessage *message = [notification object];
    NSString *title = [[NSString alloc] initWithData:message.title encoding:NSUTF8StringEncoding];
    NSString *body = [[NSString alloc] initWithData:message.body encoding:NSUTF8StringEncoding];
    NSLog(@"Receive message title: %@, content: %@.", title, body);
}


+(void)directManagerControlerWithType:(PushType)type{
    if (type == PushTypeUserBeConcerned){               // 被关注
        CenterMessageFansViewController *fansVC = [[CenterMessageFansViewController alloc]init];
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:fansVC animated:YES];
    } else if (type == PushTypeUserAuth || type == PushTypeSystemAuthSuccess || type ==  PushTypeSystemAuthFail){              // 用户 = 认证
        MineRealNameViewController *realVC = [[MineRealNameViewController alloc]init];
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:realVC animated:YES];
    } else if (type == PushTypeUserIncome){             // 每日收益
        CenterBBTDetailedViewController *bbtVC = [[CenterBBTDetailedViewController alloc]init];
        bbtVC.transferFinaniceType = BBTMyFinaniceTypeBpSuoding;
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:bbtVC animated:YES];
    } else if (type == PushTypeArticleSupport || type == PushTypeLiveMyBeZan || type == PushTypeLiveMyCommentBeZan){         // 点赞
        CenterMessageZanViewController *messageZanViewController = [[CenterMessageZanViewController alloc]init];
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:messageZanViewController animated:YES];
    } else if (type == PushTypeArticleComment){         // 文章被评论
        CenterMessageCommentViewController *centerMessageVC = [[CenterMessageCommentViewController alloc]init];
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:centerMessageVC animated:YES];
    } else if (type == PushTypeSystemAttendance){       // 签到成功
        CenterBBTDetailedViewController *bbtVC = [[CenterBBTDetailedViewController alloc]init];
        bbtVC.transferFinaniceType = BBTMyFinaniceTypeBpSuoding;
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:bbtVC animated:YES];
    } else if (type == PushTypeSystemInvitation){       // 邀请好友
        VersionInvitationLeijiJiangliViewController *leijijiangliVC = [[VersionInvitationLeijiJiangliViewController alloc]init];
        leijijiangliVC.transferType = VersionInvitationLeijiJiangliViewControllerTypeYaoqingjilu;
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:leijijiangliVC animated:YES];
    } else if (type == PushTypeSystemInviteMember){
        VersionInvitationLeijiJiangliViewController *leijijiangliVC = [[VersionInvitationLeijiJiangliViewController alloc]init];
        leijijiangliVC.transferType = VersionInvitationLeijiJiangliViewControllerTypeJiasujilu;
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:leijijiangliVC animated:YES];
    } else if (type == PushTypeSystemInviteRegister){
        VersionInvitationLeijiJiangliViewController *leijijiangliVC = [[VersionInvitationLeijiJiangliViewController alloc]init];
        leijijiangliVC.transferType = VersionInvitationLeijiJiangliViewControllerTypeYaoqingjilu;
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:leijijiangliVC animated:YES];
    }
}

@end
