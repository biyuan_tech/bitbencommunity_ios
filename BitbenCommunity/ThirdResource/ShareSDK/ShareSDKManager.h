//
//  ShareSDKManager.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/4/25.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import <ShareSDKExtension/SSEThirdPartyLoginHelper.h>

typedef NS_ENUM(NSInteger,thirdLoginType) {
    thirdLoginTypeNative = 0,                   /**< 本地登录*/
    thirdLoginTypeWechat = 1,                   /**< 微信登录*/
    thirdLoginTypeWechatFirend = 2,             /**< 微信朋友圈*/
    thirdLoginTypeQQ = 3,                       /**< QQ登录*/
    thirdLoginTypeWeibo = 4,                    /**< 微博登录*/
};

typedef NS_ENUM(NSInteger,shareType) {
    shareTypeArticle = 0,
    shareTypeLive = 1,
    shareTypeInvitation = 2,
};

@interface ShareSDKManager : FetchModel

+(void)registeShareSDK;                             // 注册ShareSDK

+(void)thirdLoginWithType:(thirdLoginType)thirdLoginType successBlock:(void(^)(SSDKUser *user))successHandle failBlock:(void(^)())failHandle;


+(void)shareManagerWithType:(thirdLoginType)type title:(NSString *)title desc:(NSString *)desc img:(id)img url:(NSString *)url callBack:(void(^)(BOOL success))callback;

+(void)shareSuccessBack:(shareType)type block:(void(^)())block;

+(BOOL)hasSetupWechat;

@end
