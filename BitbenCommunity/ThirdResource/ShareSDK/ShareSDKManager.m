//
//  ShareSDKManager.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/4/25.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "ShareSDKManager.h"
#import <ShareSDK/ShareSDK.h>

#import "WXApi.h"
#import "NetworkAdapter+Task.h"

@implementation ShareSDKManager

+(void)registeShareSDK{
    [ShareSDK registPlatforms:^(SSDKRegister *platformsRegister) {
        //更新到4.3.3或者以上版本，微信初始化需要使用以下初始化
        [platformsRegister setupWeChatWithAppId:WeChatAppID appSecret:WeChatAppSecret universalLink:@"https://jfgvl.share2dlink.com/"];
               //新浪
        [platformsRegister setupSinaWeiboWithAppkey:WeiBoAPPID appSecret:WeiBoAppSecret redirectUrl:@"http://www.sharesdk.cn"];
    }];
}

+(void)thirdLoginWithType:(thirdLoginType)thirdLoginType successBlock:(void(^)(SSDKUser *user))successHandle failBlock:(void(^)())failHandle{
    [SSEThirdPartyLoginHelper loginByPlatform:SSDKPlatformTypeWechat onUserSync:^(SSDKUser *user, SSEUserAssociateHandler associateHandler) {
        if (successHandle){
            successHandle(user);
        }
    } onLoginResult:^(SSDKResponseState state, SSEBaseUser *user, NSError *error) {
        if (state == SSDKResponseStateSuccess) {
            if (successHandle){
                successHandle(nil);
            }
        } else if (state == SSDKResponseStateFail){
            if (failHandle){
                failHandle();
            }
        }
    }];
}


+(void)shareManagerWithType:(thirdLoginType)type title:(NSString *)title desc:(NSString *)desc img:(id)img url:(NSString *)url callBack:(void(^)(BOOL success))callback{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSURL *mainUrl = [NSURL URLWithString:url];
    
//    if (!img){
//        img = [UIImage imageNamed:@"icon_main_logo"];
//    }
    
    if (!img){
        img = [UIImage imageNamed:@"icon_share_logo"];
    } else {
        
    }
    
    [params SSDKSetupShareParamsByText:desc images:img url:mainUrl title:title type:SSDKContentTypeAuto];
    
    SSDKPlatformType mainType;
    if (type == thirdLoginTypeWechat){
        mainType = SSDKPlatformTypeWechat;
    } else if (type == thirdLoginTypeQQ){
        mainType = SSDKPlatformTypeQQ;
    } else if (type == thirdLoginTypeWeibo){
        mainType = SSDKPlatformTypeSinaWeibo;
        [params SSDKSetupShareParamsByText:[NSString stringWithFormat:@"%@\r%@\r%@",title,mainUrl,desc] images:img url:mainUrl title:title type:SSDKContentTypeAuto];

    } else if (type == thirdLoginTypeWechatFirend){
        mainType = SSDKPlatformSubTypeWechatTimeline;
    } else {
        mainType = SSDKPlatformTypeWechat;
    }
    
    
    
    [ShareSDK share:mainType parameters:params onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
        if (state == SSDKResponseStateSuccess){
            [StatusBarManager statusBarHidenWithText:@"分享成功"];
        } else if (state == SSDKResponseStateFail){
            [StatusBarManager statusBarHidenWithText:@"分享失败"];
        }
    }];
}

#pragma mark - 分享成功提交后台
+(void)shareSuccessBack:(shareType)type block:(void(^)())block{
    [[NetworkAdapter sharedAdapter] taskShareManager:type block:block];
}

#pragma makr - 判断是否有微信
+(BOOL)hasSetupWechat{
    BOOL has = [WXApi isWXAppInstalled];
    NSString *info = [WXApi getApiVersion];
    BOOL mainhas = has;
    if (info.length > 0){
        mainhas = YES;
    }
    return mainhas;
}

@end

