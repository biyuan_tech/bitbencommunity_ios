//
//  SuperPlayerViewConfig.h
//  SuperPlayer
//
//  Created by annidyfeng on 2018/10/18.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger , PLAYER_TYPE) {
    PLAYER_TYPE_DEFULT = 0,  // 默认
    PLAYER_TYPE_REPORT, // 带举报样式
    PLAYER_TYPE_LIVE,  // 直播样式
    PLAYER_TYPE_COURSE, // 课程样式
    PLAYER_TYPE_PF_DEFULT, // 推流回放默认样式
    PLAYER_TYPE_ILIVE_END, // 互动直播回放
};

@interface SuperPlayerViewConfig : NSObject
NS_ASSUME_NONNULL_BEGIN
/// 是否镜像，默认NO
@property BOOL mirror;
/// 是否硬件加速，默认YES
@property BOOL hwAcceleration;
@property BOOL hwAccelerationChanged;
/// 播放速度，默认1.0
@property CGFloat playRate;
/// 是否静音，默认NO
@property BOOL mute;
/// 填充模式，默认铺满。 参见 TXLiveSDKTypeDef.h
@property NSInteger renderMode;
/// http头，跟进情况自行设置
@property NSDictionary *headers;
/// 播放器最大缓存个数
@property (nonatomic) NSInteger maxCacheItem;
/// 时移域名，默认为playtimeshift.live.myqcloud.com
@property NSString *playShiftDomain;
//修改By Belief
/** 是否包含举报按钮 */
//@property (nonatomic ,assign) BOOL hasReport;
///** 是否为直播 */
//@property (nonatomic ,assign) BOOL isLive;
/** 播放器样式 */
@property (nonatomic ,assign) PLAYER_TYPE type;
/** 是否隐藏顶部 */
@property (nonatomic ,assign) BOOL isHiddenTop;
/** 是否包含更多按钮 */
@property (nonatomic ,assign) BOOL hasMoreBtn;



NS_ASSUME_NONNULL_END

@end
