//
//  SPILiveEndControlView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "SuperPlayerControlView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SPILiveEndControlView : SuperPlayerControlView


/** 开始播放按钮 */
@property (nonatomic, strong) UIButton                *startBtn;
/** 当前播放时长label */
@property (nonatomic, strong) UILabel                 *currentTimeLabel;
/** 视频总时长label */
@property (nonatomic, strong) UILabel                 *totalTimeLabel;
/** bottomView*/
@property (nonatomic, strong) UIImageView             *bottomImageView;
/** 播放按钮 */
@property (nonatomic, strong) UIButton                *playeBtn;
/** 加载失败按钮 */
@property (nonatomic, strong) UIButton                *middleBtn;
/// 画面比例
@property CGFloat videoRatio;

/** 滑杆 */
@property (nonatomic, strong) PlayerSlider   *videoSlider;

/** 重播按钮 */
@property (nonatomic, strong) UIButton       *repeatBtn;

@end

NS_ASSUME_NONNULL_END
