//
//  SPILiveEndControlView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "SPILiveEndControlView.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

#import "MoreContentView.h"
#import "DataReport.h"
#import "SuperPlayerFastView.h"
#import "PlayerSlider.h"
#import "UIView+MMLayout.h"
#import "SuperPlayerView+Private.h"
#import "StrUtils.h"
#import "UIView+Fade.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"

#define MODEL_TAG_BEGIN 20

@interface SPILiveEndControlView ()<UIGestureRecognizerDelegate, PlayerSliderDelegate>

@end

@implementation SPILiveEndControlView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
//        [self addSubview:self.bottomImageView];
//        [self.bottomImageView addSubview:self.startBtn];
//        [self.bottomImageView addSubview:self.currentTimeLabel];
//        [self.bottomImageView addSubview:self.videoSlider];
//        [self.bottomImageView addSubview:self.totalTimeLabel];
        
//        [self addSubview:self.playeBtn];
        
        // 添加子控件的约束
//        [self makeSubViewsConstraints];
        
        // 初始化时重置controlView
        [self playerResetControlView];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
}

- (void)makeSubViewsConstraints {


    [self.bottomImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.mas_equalTo(0);
        //        make.height.mas_equalTo(50);
        make.height.mas_equalTo(40);
    }];
    
 
//
//    [self.currentTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.leading.equalTo(self.startBtn.mas_trailing);
//        make.centerY.equalTo(self.startBtn.mas_centerY);
//        make.width.mas_equalTo(60);
//    }];
//
    
//
//    [self.videoSlider mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.leading.equalTo(self.currentTimeLabel.mas_trailing);
//        make.trailing.equalTo(self.totalTimeLabel.mas_leading);
//        make.centerY.equalTo(self.currentTimeLabel.mas_centerY).offset(-1);
//    }];
    

//    [self.playeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.height.mas_equalTo(50);
//        make.center.equalTo(self);
//    }];
    
}

#pragma mark - Action

- (void)playBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        [self.delegate controlViewPlay:self];
    } else {
        [self.delegate controlViewPause:self];
    }
    [self cancelFadeOut];
}

- (void)panGestureRecognizer:(UIPanGestureRecognizer *)pan{
    if ([self.delegate respondsToSelector:@selector(onControlView:panGesture:)]) {
        [self.delegate onControlView:self panGesture:pan];
    }
}

- (void)progressSliderTouchBegan:(UISlider *)sender {
    self.isDragging = YES;
    [self cancelFadeOut];
}

- (void)progressSliderValueChanged:(UISlider *)sender {
    [self.delegate controlViewPreview:self where:sender.value];
}

- (void)progressSliderTouchEnded:(UISlider *)sender {
    [self.delegate controlViewSeek:self where:sender.value];
    self.isDragging = NO;
    [self fadeOut:5];
}
/**
 *  屏幕方向发生变化会调用这里
 */
- (void)setOrientationLandscapeConstraint {
}
/**
 *  设置竖屏的约束
 */
- (void)setOrientationPortraitConstraint {
}

#pragma mark - Private Method

#pragma mark - setter

- (UIImageView *)bottomImageView {
    if (!_bottomImageView) {
        _bottomImageView                        = [[UIImageView alloc] init];
        _bottomImageView.userInteractionEnabled = YES;
        //        _bottomImageView.image                  = SuperPlayerImage(@"bottom_shadow");
        [_bottomImageView setImageColor:kColorRGB(103, 103, 103, 0.47)];
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognizer:)];
        [_bottomImageView addGestureRecognizer:pan];
    }
    return _bottomImageView;
}

- (UIButton *)startBtn {
    if (!_startBtn) {
        _startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //        [_startBtn setImage:SuperPlayerImage(@"play") forState:UIControlStateNormal];
        //        [_startBtn setImage:SuperPlayerImage(@"pause") forState:UIControlStateSelected];
        [_startBtn setImage:[UIImage imageNamed:@"videoPlay_play"] forState:UIControlStateNormal];
        [_startBtn setImage:SuperPlayerImage(@"pause") forState:UIControlStateSelected];
        [_startBtn addTarget:self action:@selector(playBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _startBtn;
}

- (UILabel *)currentTimeLabel {
    if (!_currentTimeLabel) {
        _currentTimeLabel               = [[UILabel alloc] init];
        _currentTimeLabel.textColor     = [UIColor whiteColor];
        _currentTimeLabel.font          = [UIFont systemFontOfSize:12.0f];
        _currentTimeLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _currentTimeLabel;
}

- (PlayerSlider *)videoSlider {
    if (!_videoSlider) {
        _videoSlider                       = [[PlayerSlider alloc] init];
        //        [_videoSlider setThumbImage:SuperPlayerImage(@"slider_thumb") forState:UIControlStateNormal];
        [_videoSlider setThumbImage:[UIImage imageNamed:@"videoPlay_progress"] forState:UIControlStateNormal];
        _videoSlider.minimumTrackTintColor = TintColor;
        // slider开始滑动事件
        [_videoSlider addTarget:self action:@selector(progressSliderTouchBegan:) forControlEvents:UIControlEventTouchDown];
        // slider滑动中事件
        [_videoSlider addTarget:self action:@selector(progressSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        // slider结束滑动事件
        [_videoSlider addTarget:self action:@selector(progressSliderTouchEnded:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
        _videoSlider.delegate = self;
    }
    return _videoSlider;
}

- (UILabel *)totalTimeLabel {
    if (!_totalTimeLabel) {
        _totalTimeLabel               = [[UILabel alloc] init];
        _totalTimeLabel.textColor     = [UIColor whiteColor];
        _totalTimeLabel.font          = [UIFont systemFontOfSize:12.0f];
        _totalTimeLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _totalTimeLabel;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    
    if ([touch.view isKindOfClass:[UISlider class]]) { // 如果在滑块上点击就不响应pan手势
        return NO;
    }
    return YES;
}

#pragma mark - Public method

- (void)setHidden:(BOOL)hidden
{
    [super setHidden:hidden];
}

/** 重置ControlView */
- (void)playerResetControlView {
    self.videoSlider.value           = 0;
    self.videoSlider.progressView.progress = 0;
    self.currentTimeLabel.text       = @"00:00";
    self.totalTimeLabel.text         = @"00:00";
    self.playeBtn.hidden             = YES;
    self.backgroundColor             = [UIColor clearColor];
}

- (void)setPointArray:(NSArray *)pointArray
{
    [super setPointArray:pointArray];
    
    for (PlayerPoint *holder in self.videoSlider.pointArray) {
        [holder.holder removeFromSuperview];
    }
    [self.videoSlider.pointArray removeAllObjects];
    
    for (SuperPlayerVideoPoint *p in pointArray) {
        PlayerPoint *point = [self.videoSlider addPoint:p.where];
        point.content = p.text;
        point.timeOffset = p.time;
    }
}



- (void)onPlayerPointSelected:(PlayerPoint *)point {
    [DataReport report:@"player_point" param:nil];
}

- (void)setProgressTime:(NSInteger)currentTime
              totalTime:(NSInteger)totalTime
          progressValue:(CGFloat)progress
          playableValue:(CGFloat)playable {
    if (!self.isDragging) {
        // 更新slider
        self.videoSlider.value           = progress;
    }
    // 更新当前播放时间
    self.currentTimeLabel.text = [StrUtils timeFormat:currentTime];
    // 更新总时间
    self.totalTimeLabel.text = [StrUtils timeFormat:totalTime];
    [self.videoSlider.progressView setProgress:playable animated:NO];
    
    NSDictionary *param = @{@"currentTime":[StrUtils timeFormat:currentTime],
                            @"totalTime":[StrUtils timeFormat:totalTime],
                            @"progress":@(progress),
                            @"playable":@(playable)
                            };
    [[NSNotificationCenter defaultCenter] postNotificationName:KNSNotification_ProgressTime object:param];
}

- (void)playerBegin:(SuperPlayerModel *)model
             isLive:(BOOL)isLive
     isTimeShifting:(BOOL)isTimeShifting
         isAutoPlay:(BOOL)isAutoPlay
{
    [self setPlayState:isAutoPlay];
}

/** 播放按钮状态 */
- (void)setPlayState:(BOOL)state {
    self.startBtn.selected = state;
}

#pragma clang diagnostic pop
@end
