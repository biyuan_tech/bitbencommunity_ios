//
//  CDChatMacro.h
//  Pods
//
//  Created by chdo on 2017/10/26.
//


#import "CDChatListProtocols.h"
#import "ChatConfiguration.h"

#ifndef CDChatMacro_h
#define CDChatMacro_h

// 0 调试 1 生产
#define Environment ChatHelpr.share.config.environment
#define isChatListDebug [ChatHelpr.share.config isDebug]


#define stringGetWidth(string,fontSize) ceil([string sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}].width)
#define stringGetHeight(string,fontSize) ceil([string sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}].height)

// ** 格式化字符串
#define nullToEmpty(string)  [NSString stringWithFormat:@"%@",string?string:@""]

#define kScreenWidth [UIScreen mainScreen].bounds.size.width

#endif /* CDChatMacro_h */

