//
//  CDImageTableViewCell.m
//  CDChatList
//
//  Created by chdo on 2017/11/6.
//

#import "CDImageTableViewCell.h"
#import "ChatHelpr.h"
#import "UITool.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIView+WebCache.h"
#import "ChatListInfo.h"
#import "CellCaculator.h"
#import <SDWebImage/FLAnimatedImageView+WebCache.h>
@interface CDImageTableViewCell()

/**
 图片_左侧
 */
@property(nonatomic, strong) FLAnimatedImageView *imageContent_left;

/**
 图片_右侧侧
 */
@property(nonatomic, strong) FLAnimatedImageView *imageContent_right;

@end;

@implementation CDImageTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    [self initLeftImageContent];
    [self initRightImageContent];
    
    return self;
}

-(void)initLeftImageContent{
    // 将气泡图换位透明图
    NSLog(@"ChatHelpr.share.imageDic----%@",ChatHelpr.share.imageDic);
    UIImage *left_box = ChatHelpr.share.imageDic[@"bg_mask_left"];
//    UIImage *left_box = [UIImage imageNamed:@"talk_pop_l_nor"];
    self.bubbleImage_left.image = left_box;
    self.bubbleImage_left.hidden = YES;
    // 在气泡图下面添加信息图片
    self.imageContent_left = [[FLAnimatedImageView alloc] initWithFrame:self.bubbleImage_left.frame];
    self.imageContent_left.contentMode = UIViewContentModeScaleAspectFill;
    self.imageContent_left.userInteractionEnabled = YES;
    self.imageContent_left.layer.cornerRadius = 4.0f;
    self.imageContent_left.clipsToBounds = YES;
    self.imageContent_left.backgroundColor = [UIColor lightGrayColor];
    [self.msgContent_left insertSubview:self.imageContent_left
                           belowSubview:self.bubbleImage_left];
    [self.msgContent_left sd_setShowActivityIndicatorView:YES];
    [self.msgContent_left sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    // 点击手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapContent:)];
    [self.bubbleImage_left addGestureRecognizer:tap];
    [self.imageContent_left addGestureRecognizer:tap];
}

-(void)initRightImageContent{
    // 将气泡图换位透明图
    UIImage *right_box = ChatHelpr.share.imageDic[@"bg_mask_right"];
//    UIImage *right_box = [UIImage imageNamed:@"talk_pop_l_nor"];
    self.bubbleImage_right.image = right_box;
    self.bubbleImage_right.hidden = YES;
    // 在气泡图下面添加信息图片
    self.imageContent_right = [[FLAnimatedImageView alloc] initWithFrame:self.bubbleImage_right.frame];
    self.imageContent_right.contentMode = UIViewContentModeScaleAspectFill;
    self.imageContent_right.userInteractionEnabled = YES;
    self.imageContent_right.layer.cornerRadius = 4.0f;
    self.imageContent_right.clipsToBounds = YES;
    self.imageContent_right.backgroundColor = CDHexColor(0x808080);
    [self.msgContent_right insertSubview:self.imageContent_right
                            belowSubview:self.bubbleImage_right];
    [self.msgContent_right sd_setShowActivityIndicatorView:YES];
    [self.msgContent_right sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    // 点击手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapContent:)];
    [self.bubbleImage_right addGestureRecognizer:tap];
    [self.imageContent_right addGestureRecognizer:tap];
}

-(void)configCellByData:(CDChatMessage)data table:(CDChatListView *)table{
    [super configCellByData:data table:table];
    
    if (data.isLeft) {
        // 左侧
        // 设置消息内容的总高度
        [self configImage_Left:data];
    } else {
        // 右侧
        // 设置消息内容的总高度
        [self configImage_Right:data];
    }

}

-(void)configImage_Left:(CDChatMessage)data {
    CGRect bubbleRec = self.bubbleImage_left.frame;
    self.imageContent_left.frame = bubbleRec;

    UIImage *image;
    
//    UIImage *image = [[SDImageCache sharedImageCache] imageFromCacheForKey:data.msg];
//
//    if (data.msg_image) {
//        image = data.msg_image;
//    }
//
//    if (!image) { // 如果是自己发的图片,则先通过data.messageId缓存，
//        image = [[SDImageCache sharedImageCache] imageFromCacheForKey:data.messageId];
//    }

    if (image) {
        self.imageContent_left.image = image;
    } else {
        NSString *url = [NSString stringWithFormat:@"%@?imageView2/2/w/%li/q/75|imageslim",data.msg,(long)(self.imageContent_left.size_width*[UIScreen mainScreen].scale)];
        NSURL *imageUrl = [NSURL URLWithString:[[NSString stringWithFormat:@"%@",url] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//        @weakify(self);
        [self.imageContent_left sd_setImageWithURL:imageUrl placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
//            @strongify(self);
//            NSData *aniData = UIImagePNGRepresentation(image);
//            FLAnimatedImage *aniImage = [FLAnimatedImage animatedImageWithGIFData:aniData];
//            self.imageContent_left.animatedImage = aniImage;
//            if (!error) {
//                if (data.msgState == CDMessageStateNormal) {
//                    return ;
//                }
//                CGSize size = caculateImageSize140By140(image,data);
//                CGFloat height = size.height;
//                data.cellHeight = height + (data.willDisplayTime ? data.chatConfig.msgTimeH : 0);
//                data.bubbleSize = CGSizeMake(size.width, ceil(height - data.chatConfig.messageMargin - data.chatConfig.nickNameHeight) - data.chatConfig.bubbleSharpAngleTopInset);
//                data.bubbleWidth = size.width;
//                data.msgState = CDMessageStateNormal;
//                [self.tableView updateMessage:data];
//            }else{
//                data.msgState = CDMessageStateDownloadFaild;
//                [self.tableView updateMessage:data];
//            }
//            [[SDImageCache sharedImageCache] storeImage:image forKey:data.msg completion:nil];
        }];
    }
}

-(void)configImage_Right:(CDChatMessage)data {
    
    CGRect bubbleRec = self.bubbleImage_right.frame;
    
    self.imageContent_right.frame = bubbleRec;
    UIImage *image;
//    UIImage *image = [[SDImageCache sharedImageCache] imageFromCacheForKey:data.msg];
//
//    if (!image) { // 如果是自己发的图片,则先通过data.messageId缓存，
//        image = [[SDImageCache sharedImageCache] imageFromCacheForKey:data.messageId];
//    }

    if (image) {
        self.imageContent_right.image = image;
    } else {
        NSString *url = [NSString stringWithFormat:@"%@?imageView2/2/w/%li/q/75|imageslim",data.msg,(long)(self.imageContent_right.size_width*[UIScreen mainScreen].scale)];
        NSURL *imageUrl = [NSURL URLWithString:[[NSString stringWithFormat:@"%@",url] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//        @weakify(self);
        [self.imageContent_right sd_setImageWithURL:imageUrl placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
//            @strongify(self);
//            if (!error) {
//                if (data.msgState == CDMessageStateNormal) {
//                    return ;
//                }
//                CGSize size = caculateImageSize140By140(image,data);
//                data.bubbleWidth = size.width;
//                CGFloat height = size.height;
//                data.cellHeight = height + (data.willDisplayTime ? data.chatConfig.msgTimeH : 0);
//                data.bubbleSize = CGSizeMake(size.width, ceil(height - data.chatConfig.messageMargin - data.chatConfig.nickNameHeight) - data.chatConfig.bubbleSharpAngleTopInset);
//                data.msgState = CDMessageStateNormal;
//                [self.tableView updateMessage:data];
////                [[SDImageCache sharedImageCache] storeImage:image forKey:data.msg completion:nil];
//            }else{
//                data.msgState = CDMessageStateDownloadFaild;
//                [self.tableView updateMessage:data];
//            }
        }];
    }
}

-(void)tapContent:(UITapGestureRecognizer *)tap {
    //
    if (self.msgModal.msgState == CDMessageStateDownloading ||
        self.msgModal.msgState == CDMessageStateDownloadFaild) {
        return;
    }
    
    ChatListInfo *info = [ChatListInfo new];
    info.eventType = ChatClickEventTypeIMAGE;
    if (self.msgModal.isLeft) {
        info.containerView = self.bubbleImage_left;
        info.image = self.imageContent_left.image;
    } else {
        info.containerView = self.bubbleImage_right;
        info.image = self.imageContent_right.image;
    }
    
    info.msgText = self.msgModal.msg;
    info.msgModel = self.msgModal;
    if ([self.tableView.msgDelegate respondsToSelector:@selector(chatlistClickMsgEvent:)]) {
        [self.tableView.msgDelegate chatlistClickMsgEvent:info];
    } else {
#ifdef DEBUG
      NSLog(@"[CDChatList] chatlistClickMsgEvent未实现，不能响应点击事件");
#endif
    }
}

//CGSize caculateImageSize140By140(UIImage *image, CDChatMessage msgData) {
//
//    // 图片将被限制在140*140的区域内，按比例显示
//    CGFloat width = image.size.width;
//    CGFloat height = image.size.height;
//
//    CGFloat maxSide = MAX(width, height);
//    CGFloat miniSide = MIN(width, height);
//
//    // 按比例缩小后的小边边长
//    CGFloat actuallMiniSide = 140 * miniSide / maxSide;
//
//    // 防止长图，宽图，限制最小边 下限
//    if (actuallMiniSide < 80) {
//        actuallMiniSide = 80;
//    }
//
//    // 返回的高度是图片高度，需加上消息内边距变成消息体高度
//    if (maxSide == width) {
//        return CGSizeMake(140, actuallMiniSide + msgData.chatConfig.messageMargin * 2);
//    } else {
//        return CGSizeMake(actuallMiniSide, 140 + msgData.chatConfig.messageMargin * 2);
//    }
//}

@end
