//
//  CDBaseMsgCell.m
//  CDChatList
//
//  Created by chdo on 2017/11/2.
//

#import "CDBaseMsgCell.h"
#import "ChatHelpr.h"
#import "UITool.h"
#import "ChatListInfo.h"

@interface CDBaseMsgCell()

@end

@implementation CDBaseMsgCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // 1 消息时间初始化
    _timeLabel = [[UILabel alloc] init];
    [_timeLabel setFrame:CGRectMake(0, 0, 100, 0)];
    _timeLabel.center = CGPointMake(cd_ScreenW() / 2, 0 / 2);
    _timeLabel.text = @"星期一 下午 2:38";
    _timeLabel.textColor = [UIColor whiteColor];
    _timeLabel.backgroundColor = CDHexColor(0xCECECE);
    _timeLabel.layer.cornerRadius = 5;
    _timeLabel.clipsToBounds = YES;
    _timeLabel.font = [UIFont systemFontOfSize:12];
    _timeLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_timeLabel];
    
    // 2 左边 消息内容初始化  头像  气泡
    [self initMessageContent_Left];
    
    // 3 右边 消息内容初始化  头像  气泡
    [self initMessageContent_Right];
    
    return self;
}
#pragma mark 初始化左侧消息UI
-(void)initMessageContent_Left {
    
    // 视图容器
    _msgContent_left = [[UIView alloc] initWithFrame:CGRectZero];
    _msgContent_left.clipsToBounds = YES;
    [self addSubview:_msgContent_left];
    
    // 头像
    
    UIImage *left_head = ChatHelpr.share.imageDic[ChatHelpr.share.config.icon_head];
    _headImage_left = [[UIImageView alloc] initWithImage:left_head];
    _headImage_left.frame = CGRectZero;
    _headImage_left.contentMode = UIViewContentModeScaleAspectFill;
    _headImage_left.userInteractionEnabled = YES;
    _headImage_left.clipsToBounds = YES;
    _headImage_left.layer.cornerRadius = 20.0f;
    [_msgContent_left addSubview:_headImage_left];
    @weakify(self);
    [_headImage_left addTapGestureRecognizer:^{
        @strongify(self);
        [self headImgTapAction];
    }];
    
    // vip图标
    self.vipImgView_left = [[PDImageView alloc] init];
    self.vipImgView_left.hidden = YES;
    [_msgContent_left addSubview:self.vipImgView_left];
    
    // 头像背景遮罩
    _headImageMask_left = [[UIImageView alloc] init];
    _headImageMask_left.frame = CGRectZero;
    _headImageMask_left.contentMode = UIViewContentModeScaleAspectFill;
//    _headImageMask_left.layer.shadowColor = CDHexColor(0x4e4e4e).CGColor;
//    _headImageMask_left.layer.shadowOpacity = 0.45;
//    _headImageMask_left.layer.shadowRadius = 5;
//    _headImageMask_left.layer.shadowOffset = CGSizeMake(0, 1);
//    [_msgContent_left addSubview:_headImageMask_left];
//    [_msgContent_left insertSubview:_headImageMask_left belowSubview:_headImage_left];
    
    _rewardBtn_left = [UIButton buttonWithType:UIButtonTypeCustom];
    [_rewardBtn_left addTarget:self action:@selector(rewardBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_msgContent_left addSubview:_rewardBtn_left];
    
    // 昵称
    _userName_left = [[UILabel alloc] initWithFrame:CGRectZero];
    _userName_left.textAlignment = NSTextAlignmentLeft;
    _userName_left.textColor = CDHexColor(0x8f8f8f);
    _userName_left.font = [UIFont systemFontOfSize:12];
    [_msgContent_left addSubview:_userName_left];
    
    // 身份
    _identity_left = [[UILabel alloc] initWithFrame:CGRectZero];
    _identity_left.textAlignment = NSTextAlignmentLeft;
    _identity_left.textColor = CDHexColor(0xedb845);
    _identity_left.font = [UIFont systemFontOfSize:11];
    [_msgContent_left addSubview:_identity_left];
    
    // 气泡
    UIImage *left_box = ChatHelpr.share.imageDic[ChatHelpr.share.config.left_box];
    _bubbleImage_left = [[UIImageView alloc] initWithImage:left_box];
    _bubbleImage_left.userInteractionEnabled = YES;
    _bubbleImage_left.frame = CGRectZero;
    [_msgContent_left addSubview:_bubbleImage_left];
    
    //消息失败icon
    _failLabel_left = [[UILabel alloc] init];
    [_msgContent_left addSubview:_failLabel_left];
    
    if (@available(iOS 8.2, *)) {
        _failLabel_left.font = [UIFont systemFontOfSize:16 weight:UIFontWeightHeavy];
    } else {
        _failLabel_left.font = [UIFont systemFontOfSize:16];
    }
    
    _failLabel_left.text = @"!";
    _failLabel_left.textAlignment = NSTextAlignmentCenter;
    _failLabel_left.textColor = [UIColor whiteColor];
    _failLabel_left.backgroundColor = [UIColor redColor];
    _failLabel_left.clipsToBounds = YES;
    _failLabel_left.layer.cornerRadius = 10;
    _failLabel_left.frame = CGRectMake(0, 0, 20, 20);
    _failLabel_left.center = CGPointMake(_bubbleImage_left.frame.origin.x + _bubbleImage_left.frame.size.width + 20,
                                         _bubbleImage_left.frame.origin.y + _bubbleImage_left.frame.size.height * 0.5);
    
    //发送中的菊花loading
    _indicator_left = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [_msgContent_left addSubview:_indicator_left];
    [_indicator_left startAnimating];
    _indicator_left.frame = _failLabel_left.frame;
    _indicator_left.center = _failLabel_left.center;
    
    // 禁言
    _bannedBtn_left = [UIButton buttonWithType:UIButtonTypeCustom];
    [_bannedBtn_left addTarget:self action:@selector(bannedBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_msgContent_left addSubview:_bannedBtn_left];
    
}
#pragma mark 初始化右侧消息UI
-(void)initMessageContent_Right{
    
    // 视图容器
    _msgContent_right = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:_msgContent_right];
    
    // 头像
    UIImage *right_head = ChatHelpr.share.imageDic[ChatHelpr.share.config.icon_head];
    
    _headImage_right = [[UIImageView alloc] initWithImage:right_head];
    _headImage_right.frame = CGRectZero;
    _headImage_right.contentMode = UIViewContentModeScaleAspectFill;
    _headImage_right.userInteractionEnabled = YES;
    _headImage_right.layer.cornerRadius = 20.0f;
    _headImage_right.clipsToBounds = YES;
    [_msgContent_right addSubview:_headImage_right];
    @weakify(self);
    [_headImage_right addTapGestureRecognizer:^{
        @strongify(self);
        [self headImgTapAction];
    }];
    
    // vip图标
    self.vipImgView_right = [[PDImageView alloc] init];
    self.vipImgView_right.hidden = YES;
    [_msgContent_right addSubview:self.vipImgView_right];
    
    // 头像背景遮罩
    _headImageMask_right = [[UIImageView alloc] init];
    _headImageMask_right.frame = CGRectZero;
    _headImageMask_right.contentMode = UIViewContentModeScaleAspectFill;
    _headImageMask_right.clipsToBounds = YES;
//    [_msgContent_right addSubview:_headImageMask_right];
//    [_msgContent_right insertSubview:_headImageMask_right belowSubview:_headImage_right];
    
    // 昵称
    _userName_right = [[UILabel alloc] initWithFrame:CGRectZero];
    _userName_right.textAlignment = NSTextAlignmentRight;
    _userName_right.textColor = CDHexColor(0x8f8f8f);
    _userName_right.font = [UIFont systemFontOfSize:12];
    [_msgContent_right addSubview:_userName_right];
    
    // 身份
    _identity_right = [[UILabel alloc] initWithFrame:CGRectZero];
    _identity_right.textAlignment = NSTextAlignmentRight;
    _identity_right.textColor = CDHexColor(0xedb845);
    _identity_right.font = [UIFont systemFontOfSize:11];
    [_msgContent_right addSubview:_identity_right];
    
    // 气泡
    UIImage *right_box = ChatHelpr.share.imageDic[ChatHelpr.share.config.right_box];
    _bubbleImage_right = [[UIImageView alloc] initWithImage:right_box];
    _bubbleImage_right.userInteractionEnabled = YES;
    _bubbleImage_right.frame = CGRectZero;
    [_msgContent_right addSubview:_bubbleImage_right];
    
    //消息失败icon
    _failLabel_right = [[UILabel alloc] init];
    [_msgContent_right addSubview:_failLabel_right];
    if (@available(iOS 8.2, *)) {
        _failLabel_right.font = [UIFont systemFontOfSize:16 weight:UIFontWeightHeavy];
    } else {
        _failLabel_right.font = [UIFont systemFontOfSize:16];
    }
    
    _failLabel_right.text = @"!";
    _failLabel_right.textAlignment = NSTextAlignmentCenter;
    _failLabel_right.textColor = [UIColor whiteColor];
    _failLabel_right.backgroundColor = [UIColor redColor];
    _failLabel_right.clipsToBounds = YES;
    _failLabel_right.layer.cornerRadius = 10;
    _failLabel_right.frame = CGRectMake(0, 0, 20, 20);
    _failLabel_right.center = CGPointMake(_bubbleImage_right.frame.origin.x - 40,
                                          _bubbleImage_right.frame.size.height * 0.5);
    
    //发送中的菊花loading
    _indicator_right = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [_msgContent_right addSubview:_indicator_right];
    [_indicator_right startAnimating];
    
    _indicator_right.frame = _failLabel_right.frame;
    _indicator_right.center = _failLabel_right.center;
}

#pragma mark 根据消息中的cellHeight  bubbleWidth 更新左侧UI
/**
 根据消息中的cellHeight  bubbleWidth 更新UI
 
 @param data 消息体
 */
-(void)updateMsgContentFrame_left:(CDChatMessage) data{
    
    // 头像
    if (data.userThumImage || data.userThumImageURL){
        _headImage_left.frame = CGRectMake(data.chatConfig.messageMargin, data.chatConfig.messageMargin, data.chatConfig.headSideLength, data.chatConfig.headSideLength);
        
//        if (!_headImageMask_left.image) {
//            UIImage * image = [self imageWithColor:[UIColor whiteColor] Size:CGSizeMake(data.chatConfig.headSideLength + 4, data.chatConfig.headSideLength + 4)];
//            _headImageMask_left.image = [self addCornerRadius:(data.chatConfig.headSideLength + 4)/2 size:CGSizeMake(data.chatConfig.headSideLength + 4, data.chatConfig.headSideLength + 4) img:image];
//        }
//        _headImageMask_left.frame = CGRectMake(0, 0, data.chatConfig.headSideLength + 4, data.chatConfig.headSideLength + 4);
//        _headImageMask_left.center = _headImage_left.center;
        if ([ChatHelpr.share.imageDic.allKeys indexOfObject:@"reward_left"] != NSNotFound) {
            [_rewardBtn_left setImage:ChatHelpr.share.imageDic[@"reward_left"] forState:UIControlStateNormal];
            _rewardBtn_left.frame = CGRectMake(0, CGRectGetMaxY(_headImage_left.frame) + 16, 24, 24);
            _rewardBtn_left.center = CGPointMake(_headImage_left.center.x, _rewardBtn_left.center.y);
        }
        _rewardBtn_left.hidden = !data.hasReward;
        
    } else {
        _headImage_left.frame = CGRectZero;
        _rewardBtn_left.frame = CGRectZero;
    }
    
    CGFloat r = CGRectGetWidth(_headImage_left.frame)/2.0;
    CGFloat detalX = sqrtf(r*r/2.0);
    CGFloat width = 16;
    _vipImgView_left.frame = CGRectMake(0, 0, width, width);
    _vipImgView_left.center = CGPointMake(_headImage_left.center_x + detalX, _headImage_left.center_y + detalX);
    if (data.cert_bagde == PDImgStyleNormal ||
        data.cert_bagde == PDImgStyleAvatar_Vip_Person) {
        _vipImgView_left.hidden = YES;
    }else{
        ServerBadgeModel *model = [BYCommonTool getVipImageName:stringFormatInteger(data.cert_bagde)];
        [self.vipImgView_left uploadHDImageWithURL:model.picture callback:nil];
        self.vipImgView_left.hidden = NO;
    }
    
    
    // 昵称
    _userName_left.frame = CGRectMake(2*data.chatConfig.messageMargin + _headImage_left.cd_width + data.chatConfig.bubbleShareAngleWidth, 14, data.chatConfig.bubbleMaxWidth, data.chatConfig.nickNameHeight);
    _userName_left.textColor = data.chatConfig.nickNameColor;
    
    // 身份
    CGFloat originX = CGRectGetMinX(_userName_left.frame) + stringGetWidth(_userName_left.text, _userName_left.font.pointSize) + 5;
    _identity_left.frame = CGRectMake(originX, 0, stringGetWidth(_identity_left.text, _identity_left.font.pointSize), stringGetHeight(_identity_left.text, _identity_left.font.pointSize));
    _identity_left.center = CGPointMake(_identity_left.center.x, _userName_left.center.y);
    
    // 左侧
    // 设置消息内容的总高度
    CGRect msgRect = self.msgContent_left.frame;
    CGFloat msgContentHeight = data.cellHeight;
    
    // 根据是否显示时间，调整msgContent_left位置
    if (data.willDisplayTime) {
        msgRect.origin = CGPointMake(0, data.chatConfig.msgTimeH);
        msgContentHeight = msgContentHeight - data.chatConfig.msgTimeH; //
    } else {
        msgRect.origin = CGPointZero;
    }
    msgRect.size.width = cd_ScreenW();
    msgRect.size.height = msgContentHeight;
    self.msgContent_left.frame = msgRect;
    
    // 更新消息气泡的高度和宽度
    CGRect bubbleRec = self.bubbleImage_left.frame;
    bubbleRec.origin.x = data.chatConfig.messageMargin * 2 + _headImage_left.cd_width - data.chatConfig.bubbleShareAngleWidth;
//    bubbleRec.origin.y = data.userName.length == 0 ? data.chatConfig.messageMargin : data.chatConfig.nickNameHeight;
    // 修改 by belief
    bubbleRec.origin.y = data.userName.length == 0 ? data.chatConfig.messageMargin : CGRectGetMinX(_headImage_left.frame) + data.chatConfig.bubbleSharpAngleTopInset;
    bubbleRec.size.width = data.bubbleSize.width;
    bubbleRec.size.height = data.bubbleSize.height;
//    bubbleRec.size.width = data.bubbleWidth;
//    if (data.userName.length == 0) {
//        bubbleRec.size.height = msgContentHeight - data.chatConfig.messageMargin * 2;
//    } else {
//        bubbleRec.size.height = msgContentHeight - data.chatConfig.messageMargin - data.chatConfig.nickNameHeight;
//    }
    
    self.bubbleImage_left.frame = bubbleRec;
    
    // 禁言
    if ([ChatHelpr.share.imageDic.allKeys indexOfObject:@"banned_left"] != NSNotFound) {
        [_bannedBtn_left setImage:ChatHelpr.share.imageDic[@"banned_left"] forState:UIControlStateNormal];
        CGFloat originX = kScreenWidth - 82 - 19;
        _bannedBtn_left.frame = CGRectMake(originX, bubbleRec.origin.y + data.bubbleSize.height + 10, 19, 17);
    }
    _bannedBtn_left.hidden = !data.hasBanned;
    
    // 更新loading位置
    _indicator_left.frame = CGRectMake(0, 0, 20, 20);
    _indicator_left.center = CGPointMake(_bubbleImage_left.frame.origin.x + _bubbleImage_left.frame.size.width + 20,
                                         _bubbleImage_left.frame.origin.y + _bubbleImage_left.frame.size.height * 0.5);
    
    // 更新faillabel位置
    _failLabel_left.frame = _indicator_left.frame;
    _failLabel_left.center = _indicator_left.center;
    
    // 更新动画状态
    // 更新动画状态
    if (data.msgState == CDMessageStateNormal) {
        [_indicator_left stopAnimating];
        [_failLabel_left setHidden: YES];
    } else if (data.msgState == CDMessageStateSending) {
        [_indicator_left startAnimating];
        [_failLabel_left setHidden: YES];
    } else if (data.msgState == CDMessageStateSendFaild ||
               data.msgState == CDMessageStateDownloadFaild) {
        [_indicator_left stopAnimating];
        [_failLabel_left setHidden: NO];
    } else if (data.msgState == CDMessageStateDownloading) {
        [_indicator_left startAnimating];
        [_failLabel_left setHidden: YES];
    }
}

#pragma mark 根据消息中的cellHeight  bubbleWidth 更新右侧UI
/**
 根据消息中的cellHeight  bubbleWidth 更新UI
 
 @param data 消息体
 */
-(void)updateMsgContentFrame_right:(CDChatMessage) data{
    
    // 头像
    if (data.userThumImage || data.userThumImageURL){
        _headImage_right.frame = CGRectMake(cd_ScreenW() - (data.chatConfig.headSideLength + data.chatConfig.messageMargin), data.chatConfig.messageMargin, data.chatConfig.headSideLength, data.chatConfig.headSideLength);
        
        _headImageMask_right.frame = CGRectMake(0, 0, data.chatConfig.headSideLength + 4, data.chatConfig.headSideLength + 4);
        _headImageMask_right.center = _headImage_right.center;
        _headImageMask_right.layer.cornerRadius = (data.chatConfig.headSideLength + 4)/2;

    } else {
        _headImage_right.frame = CGRectMake(cd_ScreenW(), data.chatConfig.messageMargin, 0, data.chatConfig.headSideLength);
    }
    
    CGFloat r = CGRectGetWidth(_headImage_right.frame)/2.0;
    CGFloat detalX = sqrtf(r*r/2.0);
    CGFloat width = 16;
    _vipImgView_right.frame = CGRectMake(0, 0, width, width);
    _vipImgView_right.center = CGPointMake(_headImage_right.center_x + detalX, _headImage_right.center_y + detalX);
    if (data.cert_bagde == PDImgStyleNormal ||
        data.cert_bagde == PDImgStyleAvatar_Vip_Person) {
        _vipImgView_right.hidden = YES;
    }else{
        ServerBadgeModel *model = [BYCommonTool getVipImageName:stringFormatInteger(data.cert_bagde)];
        [self.vipImgView_right uploadHDImageWithURL:model.picture callback:nil];
        self.vipImgView_right.hidden = NO;
    }
    
    // 昵称
    _userName_right.frame = CGRectMake(_headImage_right.cd_left - data.chatConfig.messageMargin - data.chatConfig.bubbleMaxWidth, 14, data.chatConfig.bubbleMaxWidth, data.chatConfig.nickNameHeight);
    _userName_right.textColor = data.chatConfig.nickNameColor;
    
    // 身份
    CGFloat originX = CGRectGetMaxX(_userName_right.frame) - stringGetWidth(_userName_right.text, _userName_right.font.pointSize) - 5 - stringGetWidth(_identity_right.text, _identity_right.font.pointSize);
    _identity_right.frame = CGRectMake(originX, 0, stringGetWidth(_identity_right.text, _identity_right.font.pointSize), stringGetHeight(_identity_right.text, _identity_right.font.pointSize));
    _identity_right.center = CGPointMake(_identity_right.center.x, _userName_right.center.y);
    
    // 右侧
    // 设置消息内容的总高度
    CGRect msgRect = self.msgContent_right.frame;
    CGFloat msgContentHeight = data.cellHeight;
    if (data.willDisplayTime) {
        msgRect.origin = CGPointMake(0, data.chatConfig.msgTimeH);
        msgContentHeight = msgContentHeight - data.chatConfig.msgTimeH; //
    } else {
        msgRect.origin = CGPointZero;
    }
    
    msgRect.size.width = cd_ScreenW();
    msgRect.size.height = msgContentHeight;
    self.msgContent_right.frame = msgRect;
    
    // 更新气泡的高度和宽度
    CGRect bubbleRec = self.bubbleImage_right.frame;
    bubbleRec.origin.x = cd_ScreenW() - (data.bubbleWidth + _headImage_right.cd_width) - data.chatConfig.messageMargin * 2 + data.chatConfig.bubbleShareAngleWidth;
//    bubbleRec.origin.y = data.userName.length == 0 ? data.chatConfig.messageMargin : data.chatConfig.nickNameHeight;
    // 修改by belief
    bubbleRec.origin.y = data.userName.length == 0 ? data.chatConfig.messageMargin : CGRectGetMinY(_headImage_right.frame) + data.chatConfig.bubbleSharpAngleTopInset;
//    bubbleRec.size.width = data.bubbleWidth;
    bubbleRec.size.width = data.bubbleSize.width;
    bubbleRec.size.height = data.bubbleSize.height;
//    if (data.userName.length == 0) {
//        bubbleRec.size.height = msgContentHeight - data.chatConfig.messageMargin * 2;
//    } else {
//        bubbleRec.size.height = msgContentHeight - data.chatConfig.messageMargin - data.chatConfig.nickNameHeight;
//    }
    self.bubbleImage_right.frame = bubbleRec;
    
    // 设置loading位置
    _indicator_right.frame = CGRectMake(0, 0, 20, 20);
    _indicator_right.center = CGPointMake(_bubbleImage_right.frame.origin.x - 20,
                                          _bubbleImage_right.frame.origin.y + _bubbleImage_right.frame.size.height * 0.5);
    // 更新faillabel位置
    _failLabel_right.frame = _indicator_right.frame;
    _failLabel_right.center = _indicator_right.center;
    
    // 更新动画状态
    if (data.msgState == CDMessageStateNormal) {
        [_indicator_right stopAnimating];
        [_failLabel_right setHidden: YES];
    } else if (data.msgState == CDMessageStateSending) {
        [_indicator_right startAnimating];
        [_failLabel_right setHidden: YES];
    } else if (data.msgState == CDMessageStateSendFaild ||
               data.msgState == CDMessageStateDownloadFaild) {
        [_indicator_right stopAnimating];
        [_failLabel_right setHidden: NO];
    } else if (data.msgState == CDMessageStateDownloading) {
        [_indicator_right startAnimating];
        [_failLabel_right setHidden: YES];
    }
}

#pragma mark 设置消息data
-(void)configCellByData:(CDChatMessage)data table:(CDChatListView *)table{
    
    self.backgroundColor = data.chatConfig.msgBackGroundColor;
    
    self.msgModal = data;
    self.tableView = table;
    
    // 设置显示或隐藏  左右气泡
    [self.msgContent_left setHidden:!data.isLeft];
    self.msgContent_left.backgroundColor = data.chatConfig.msgContentBackGroundColor;
    [self.msgContent_right setHidden:data.isLeft];
    self.msgContent_right.backgroundColor = data.chatConfig.msgContentBackGroundColor;
    
    // 设置头像
//    _headImage_right.backgroundColor = data.chatConfig.headBackGroundColor;
//    _headImage_left.backgroundColor = data.chatConfig.headBackGroundColor;
    
    if (data.userThumImage || data.userThumImageURL){
        if (data.isLeft) {
            CGSize size = CGSizeMake(data.chatConfig.headSideLength, data.chatConfig.headSideLength);
            if (data.userThumImage) {
                _headImage_left.image = data.userThumImage;
            } else if (data.userThumImageURL) {
                [_headImage_left sd_setImageWithURL:[NSURL URLWithString:data.userThumImageURL] placeholderImage:ChatHelpr.share.imageDic[ChatHelpr.share.config.icon_head]];
            } else {
                [_headImage_left setImage:ChatHelpr.share.imageDic[ChatHelpr.share.config.icon_head]];
            }
        } else {
            if (data.userThumImage) {
                _headImage_right.image = data.userThumImage;
            } else if (data.userThumImageURL) {
                [_headImage_right sd_setImageWithURL:[NSURL URLWithString:data.userThumImageURL] placeholderImage:ChatHelpr.share.imageDic[ChatHelpr.share.config.icon_head]];
            } else {
                [_headImage_right setImage:ChatHelpr.share.imageDic[ChatHelpr.share.config.icon_head]];
            }
        }
    }
    
    // 设置昵称
    if (data.isLeft) {
        _userName_left.text = data.userName;
        _identity_left.text = nullToEmpty(data.identity);
        _identity_left.textColor = data.identity_color ? data.identity_color : kColorRGBValue(0xea6438);
    } else {
        _userName_right.text = data.userName;
        _identity_right.text = nullToEmpty(data.identity);
        _identity_right.textColor = data.identity_color ? data.identity_color : kColorRGBValue(0xea6438);
    }
    
    // 设置顶部时间Label
    NSDate *date;
    if (data.createTime.length == 10) {
        date = [NSDate dateWithTimeIntervalSince1970:[data.createTime doubleValue]];
    } else {
        date = [NSDate dateWithTimeIntervalSince1970:[data.createTime doubleValue] * 0.001];
    }
    self.timeLabel.text = [self checkDateDisplay:date msg:data];
    CGSize textSize = [self.timeLabel.text boundingRectWithSize:CGSizeMake(cd_ScreenW(), data.chatConfig.msgTimeH) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: self.timeLabel.font} context:nil].size;
    if (textSize.height < data.chatConfig.msgTimeH) {
        textSize.height = data.chatConfig.msgTimeH;
    }
    [_timeLabel setFrame:CGRectMake(0, 0, textSize.width + data.chatConfig.sysInfoPadding * 2, textSize.height)];
    _timeLabel.center = CGPointMake(cd_ScreenW() / 2, data.chatConfig.msgTimeH / 2);
    
    if (data.isLeft) {
        [self updateMsgContentFrame_left:data];
    } else {
        [self updateMsgContentFrame_right:data];
    }
}

#pragma mark 根据消息时间，计算需要显示的消息时间格式
/**
 根据消息时间，计算需要显示的消息时间格式
 
 @param thisDate 消息时间
 @return 显示在label上的
 */
- (NSString*)checkDateDisplay:(NSDate *)thisDate msg:(CDChatMessage)data{
    
    if (data.ctDataconfig.matchLink) {
        NSDateFormatter *formate = [[NSDateFormatter alloc] init];
        formate.dateFormat = @"MM-dd HH:mm";
        return [formate stringFromDate:thisDate];
    }
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierChinese];
    NSDate *nowDate =  [NSDate date];
    
    // IOS8 最低支持；
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear;
    
    NSDateComponents *nowComps = [[NSDateComponents alloc] init];
    nowComps = [calendar components:unitFlags fromDate:nowDate];
    NSInteger nowDay = nowComps.day;
    
    // 时间戳  转 是日期
    NSDateComponents *thisComps = [[NSDateComponents alloc] init];
    thisComps = [calendar components:unitFlags fromDate:thisDate];
    NSInteger thisDay = thisComps.day;
    
    // 当前时间差；
    NSDateComponents *dayDiffComps = [[NSDateComponents alloc] init] ;
    dayDiffComps = [calendar components:NSCalendarUnitDay fromDate:thisDate toDate:nowDate options:NSCalendarWrapComponents];
    NSInteger compareDay = dayDiffComps.day;
    
    NSString *timeString;
    
    // 是否 是当天 的 时间；
    if (compareDay == 0 && nowDay == thisDay) {
        NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"HH:mm"];
        timeString  = [dateFormat stringFromDate:thisDate];
        return timeString;
    }
    
    // 是否  昨天时间；
    if (compareDay == 1 || (compareDay == 0 && nowDay != thisDay) ){
        timeString = @"昨天";
        
        NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"HH:mm"];
        NSString* time = [dateFormat stringFromDate:thisDate];
        timeString = [NSString stringWithFormat:@"%@ %@" , timeString , time ];
        
        return timeString;
    }
    
    // 是否  前天时间；
    if (compareDay == 2 || (compareDay == 0 && nowDay != thisDay) ){
        timeString = @"前天";
        
        NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"HH:mm"];
        NSString* time = [dateFormat stringFromDate:thisDate];
        timeString = [NSString stringWithFormat:@"%@ %@" , timeString , time ];
        
        return timeString;
    }
    
    // 非近 一周时间 ；
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd"];
    timeString  = [dateFormat stringFromDate:thisDate];
    
    [dateFormat setDateFormat:@"yy/MM/dd"];
    timeString  = [dateFormat stringFromDate:thisDate];
    
    return timeString;
}

- (UIImage *)addCornerRadius:(CGFloat)radius size:(CGSize)size img:(UIImage *)img{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(radius, radius)];
    CGContextAddPath(contextRef,path.CGPath);
    CGContextClip(contextRef);
    [img drawInRect:rect];
    CGContextDrawPath(contextRef, kCGPathFillStroke);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage *)imageWithColor:(UIColor *)color Size:(CGSize)size
{
    CGRect rect = CGRectMake(0.0, 0.0, size.width, size.height);
    UIGraphicsBeginImageContext(size);
    CGContextRef ref = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(ref, [color CGColor]);
    CGContextFillRect(ref, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark - action
- (void)rewardBtnAction:(id)sender{
    ChatListInfo *info = [ChatListInfo new];
    info.eventType = ChatClickEventTypeREWARD;
    info.msgModel = self.msgModal;
    if ([self.tableView.msgDelegate respondsToSelector:@selector(chatlistClickMsgEvent:)]) {
        [self.tableView.msgDelegate chatlistClickMsgEvent:info];
    }else{
#ifdef DEBUG
        NSLog(@"[CDChatList] chatlistClickMsgEvent未实现，不能响应点击事件");
#endif
    }
}

- (void)bannedBtnAction:(id)sender{
    ChatListInfo *info = [ChatListInfo new];
    info.eventType = ChatClickEventTypeBanned;
    info.msgModel = self.msgModal;
    if ([self.tableView.msgDelegate respondsToSelector:@selector(chatlistClickMsgEvent:)]) {
        [self.tableView.msgDelegate chatlistClickMsgEvent:info];
    }else{
#ifdef DEBUG
        NSLog(@"[CDChatList] chatlistClickMsgEvent未实现，不能响应点击事件");
#endif
    }
}

- (void)headImgTapAction{
    ChatListInfo *info = [ChatListInfo new];
    info.eventType = ChatClickEventTypeHeadImg;
    info.msgModel = self.msgModal;
    if ([self.tableView.msgDelegate respondsToSelector:@selector(chatlistClickMsgEvent:)]) {
        [self.tableView.msgDelegate chatlistClickMsgEvent:info];
    }else{
#ifdef DEBUG
        NSLog(@"[CDChatList] chatlistClickMsgEvent未实现，不能响应点击事件");
#endif
    }
}
@end
