//
//  BYIMCommon.h
//  BY
//
//  Created by 黄亮 on 2018/8/24.
//  Copyright © 2018年 BY. All rights reserved.
//

#ifndef BYIMCommon_h
#define BYIMCommon_h

#import <Foundation/Foundation.h>

#define LIVEGUEST_MEMBERS @[LIVE_GUEST_ONE] //LIVE_GUEST_TWO,LIVE_GUEST_THERE

#define RENDERVIEW_FRAMES \
@[@(CGRectMake(kCommonScreenWidth - 111, kCommonScreenHeight - 165 - kSafe_Mas_Bottom(110), 105, 165)),\
@(CGRectMake(kCommonScreenWidth - 105, kCommonScreenHeight - 287 - kSafe_Mas_Bottom(64), 90, 137)),\
@(CGRectMake(15, 130, 90, 137))]

typedef NS_ENUM(NSInteger ,BYIMGroupMemberRole) {
    BYTIM_GROUP_MEMBER_ROLE_MEMBER  = 200, // 普通成员
    BYTIM_GROUP_MEMBER_ROLE_ADMIN   = 300 // 管理员
};

typedef NS_ENUM(NSInteger ,BYIMConversationType) {
    BYIM_CVT_TYPE_C2C, // 单聊
    BYIM_CVT_TYPE_GROUP // 群聊
};

// ** 获取聊天室登录sig
//static NSString * RPC_get_chat_sig = @"live/get_chat_sig";

/** 下麦通知 */
static NSString *const NSNotification_downVideo = @"NSNotification_downVideo";
/** 上麦成员发生变动 */
static NSString *const NSNotification_upToVideoMembersValueDidChangle = @"NSNotification_upToVideoMembersValueDidChangle";
/** 停止推流消息 */
static NSString *const NSNotification_stopPushStream = @"NSNotification_stopPushStream";
#define LIVE_HOST @"LiveMaster" // 默认主播配置
#define LIVE_GUEST @"Guest" // 默认观众配置
#define LIVE_GUEST_ONE @"LiveGuest" // 连麦观众配置
#define LIVE_GUEST_TWO @"LiveGuest1"
#define LIVE_GUEST_THERE @"LiveGuest2"

#define MAX_UPVIDEO_NUM 1

#define connFailed @"网络连接失败"
#define connlost   @"网络连接断开"
#define connecting @"网络连接中"

#define kJoinMeg    @"进入直播间"
#define kGroupType  @"AVChatRoom"

#define kError_Room(t,c,m) [NSString stringWithFormat:@"TIM%@失败\n errorCode:%i errMsg:%@",t,c,m]

#endif /* BYIMCommon_h */
