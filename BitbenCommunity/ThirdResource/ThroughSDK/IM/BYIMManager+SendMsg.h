//
//  BYIMManager+SendMsg.h
//  BY
//
//  Created by 黄亮 on 2018/8/24.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYIMManager.h"

@interface BYIMManager (SendMsg)


/**
 设置消息回调监听
 */
- (void)setMessageListener:(void(^)(NSArray *msgs))msgs;

/**
 发送文本消息

 @param text 消息内容
 @param succ succ description
 @param fail fail description
 */
- (void)sendTextMessage:(NSString *)text succ:(sucCallback)succ fail:(failCallback)fail;

/**
 发送图片消息

 @param imagePath 本地iamge路径
 @param succ succ description
 @param fail fail description
 */
- (void)sendImageMessage:(NSString *)imagePath succ:(sucCallback)succ fail:(failCallback)fail;

/** 打赏消息 */
- (void)sendRewardMessage:(CGFloat)amount receicer_user_Name:(NSString *)receicer_user_Name succ:(sucCallback)succ fail:(failCallback)fail;
/** text为自定义显示问题，不传则默认 */
- (void)sendRewardMessage:(CGFloat)amount receicer_user_Name:(NSString *)receicer_user_Name text:(NSString *)text succ:(sucCallback)succ fail:(failCallback)fail;
- (void)sendRewardMessage:(CGFloat)amount receicer_user_Name:(NSString *)receicer_user_Name receicer_user_Id:(NSString *)receicer_user_Id text:(NSString *)text succ:(sucCallback)succ fail:(failCallback)fail;



/** 发送开播通知 */
- (void)sendOpenLiveMessage:(sucCallback)succ fail:(failCallback)fail;

/** 发送上麦请求 */
- (void)sendUpVideoMessage:(sucCallback)succ fail:(failCallback)fail;
/** 发送同意上麦请求 */
- (void)sendAgreeUpVideoMessage:(NSString *)userId stream_id:(NSString *)stream_id succ:(sucCallback)succ fail:(failCallback)fail;
/** 发起下麦请求 */
- (void)sendDownVideoMessage:(NSString *)userId role:(NSString *)role succ:(sucCallback)succ fail:(failCallback)fail;

/** 发送结束直播消息 */
- (void)sendStopPushStreamMessage:(sucCallback)succ fail:(failCallback)fail;
- (void)sendStopPushStreamMessage:(NSDictionary *)data succ:(sucCallback)succ fail:(failCallback)fail;

@end
