//
//  BYIMManager.m
//  BY
//
//  Created by 黄亮 on 2018/8/22.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYIMManager.h"
#import "BYToastView.h"
#import "BYRequestManager.h"
//#import <sqlite3.h>
#import "BYIMManager+SendMsg.h"
#import "BYLiveRPCDefine.h"
//#import <ILiveSDK/ILiveLoginManager.h>
#import <TXLiteAVSDK_Professional/TXLiteAVSDK.h>

static char const _upToVideoMebers;
@interface BYIMManager()<NSCopying>
{
    sucCallback _callBack;
}
@property (nonatomic ,strong) BYToastView *connectingToast;
@property (nonatomic ,copy) void (^iLiveLoginHandle)(BOOL isSuccess);

@end

@implementation BYIMManager

static BYIMManager *_manager;
+ (BYIMManager *)sharedInstance{
    return [[self alloc] init];
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!_manager) {
            _manager = [super allocWithZone:zone];
        }
    });
    return _manager;
}

- (id)copyWithZone:(NSZone *)zone{
    return _manager;
}


- (void)loginILiveSuccessNotif:(void (^)(BOOL))cb{
    self.iLiveLoginHandle = cb;
}

// 获取登录sig
//- (void)getSigRequest:(void(^)(NSString *sig))suc{
//    [BYRequestManager ansyRequestWithURLString:RPC_get_user_sig
//                                    parameters:nil
//                                 operationType:BYOperationTypePost
//                                  successBlock:^(id result) {
//                                      NSString *sig = result[@"userSig"];
//                                      NSAssert(sig, @"sig为空");
//                                      if (suc) suc(sig);
//                                  } failureBlock:^(NSError *error) {
//                                      NSLog(@"获取聊天室sig失败");
//                                  }];
//}

- (void)receiveUpToVideoRequest:(BYUpVideoCellModel *)model{
    if (![self verifyIsInUpVideoList:model]) {
        NSMutableArray *mubArray = [NSMutableArray arrayWithArray:self.upToVideoMembers];
        [mubArray addObject:model];
        self.upToVideoMembers = mubArray;
    }
}

- (void)receiveUpToVideoRequestWithAudienceData:(NSDictionary *)data{
    BYUpVideoCellModel *model = [[BYUpVideoCellModel alloc] init];
    model.userlogoUrl = nullToEmpty(data[@"head_img"]);
    model.userName = nullToEmpty(data[@"nickname"]);
    model.liveRole = LIVE_GUEST;
    model.userId = data[@"user_id"];
    model.cellString = @"BYUpVideoCell";
    model.cellHeight = 57.0f;
    [self receiveUpToVideoRequest:model];
}


- (void)downToVideoRole:(NSString *)role succ:(sucCallback)succ fail:(failCallback)fail{
    [[TRTCCloud sharedInstance] switchRole:TRTCRoleAudience];
    [[TRTCCloud sharedInstance] stopLocalPreview];
}

- (NSArray *)getInUpVideoMembers{
    NSMutableArray *tmpArr = [NSMutableArray array];
    if (!self.upToVideoMembers.count) {
        return tmpArr;
    }
    for (BYUpVideoCellModel *model in self.upToVideoMembers) {
        if (model.isUpVideo) {
            [tmpArr addObject:model];
        }
    }
    return tmpArr;
}

// 打开相机与mic
//- (void)openCameraAndMic:(sucCallback)succ fail:(failCallback)fail{
//    ILiveRoomManager *roomManager = [ILiveRoomManager getInstance];
//    [roomManager enableCamera:CameraPosFront enable:YES succ:^{
//        [roomManager enableMic:YES succ:^{
//            if (succ) succ();
//        } failed:^(NSString *module, int errId, NSString *errMsg) {
//            if (fail) fail();
//            showDebugToastView(@"上麦打开麦克失败！", kCommonWindow);
//        }];
//    } failed:^(NSString *module, int errId, NSString *errMsg) {
//        if (fail) fail();
//        showDebugToastView(@"上麦打开相机失败！", kCommonWindow);
//    }];
//}

//- (void)setUpVideoMemberStatus:(NSString *)userId isUp:(BOOL)isUp{
//    for (BYUpVideoCellModel *model in self.upToVideoMembers) {
//        if ([model.userId isEqualToString:userId]) {
//            model.isUpVideo = isUp;
//        }
//    }
//}

// 分配连麦观众标识(nil 表示当前上麦达到上限，无可分配的标识)
- (NSString *)allotLiveGuestIdentifier{
    NSArray *identifiers = [self getCurrentAllUpVideoMembers];
    if (!identifiers || !identifiers.count) {
        return LIVE_GUEST_ONE;
    }
    else{
        if (identifiers.count >= MAX_UPVIDEO_NUM) {
            return nil;
        }
        else{
            NSMutableArray *tmpArr = [NSMutableArray array];
            [tmpArr addObjectsFromArray:LIVEGUEST_MEMBERS];
            [tmpArr addObjectsFromArray:identifiers];
            NSSet *set = [NSSet setWithArray:tmpArr];
            return set.allObjects[0];
        }
    }
}

//- (void)setNickName:(NSString *)nick{
//    if (!nick.length) return;
//    int success = [[TIMFriendshipManager sharedInstance] SetNickname:nick succ:nil fail:nil];
//    NSLog(@"%d",success);
//}
//
//- (void)setHeadImg:(NSString *)headImg{
//    if (!headImg.length) return;
//    int success = [[TIMFriendshipManager sharedInstance] SetFaceURL:headImg succ:nil fail:nil];
//    NSLog(@"%d",success);
//}

#pragma mark - TIMCallback(TIM登录回调)
- (void)onSucc{
    if (_callBack) _callBack();
    showToastView(@"TIM登录成功", kCommonWindow);
}

- (void)onErr:(int)errCode errMsg:(NSString *)errMsg{
    showToastView(kError_Room(@"登录", errCode, errMsg), kCommonWindow);
}

#pragma mark - TIMConnListener(网络监听)

- (void)onConnSucc{
    [_connectingToast dissmissToastView];
}

- (void)onConnFailed:(int)code err:(NSString *)err{
    [BYToastView toastViewPresentInWindowTitle:connFailed duration:2.0 toastViewType:kToastViewTypeNormal complete:nil];
}

- (void)onDisconnect:(int)code err:(NSString *)err{
    [BYToastView toastViewPresentInWindowTitle:connlost duration:2.0 toastViewType:kToastViewTypeNormal complete:nil];
}

- (void)onConnecting{
    _connectingToast = [BYToastView tostViewPresentInView:kCommonWindow title:connecting duration:MAXFLOAT complete:nil];
}

#pragma mark - init
- (void)setUpToVideoMembers:(NSArray<BYUpVideoCellModel *> *)upToVideoMembers{
    objc_setAssociatedObject(self, &_upToVideoMebers, upToVideoMembers, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//    self.upToVideoMembersValueDidChangle();
    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotification_upToVideoMembersValueDidChangle object:upToVideoMembers];
}

- (NSArray<BYUpVideoCellModel *> *)upToVideoMembers{
    NSArray *array = objc_getAssociatedObject(self, &_upToVideoMebers);
    if (!array) {
        array = [NSArray array];
        objc_setAssociatedObject(self, &_upToVideoMebers, array, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return array;
}

// 获取当前上麦user_id
- (NSArray *)getCurrentAllUpVideoMembers{
//    TILLiveManager *manager = [TILLiveManager getInstance];
//    NSArray *renderViews = [manager getAllAVRenderViews];
//    NSMutableArray *members = [NSMutableArray array];
//    if (!renderViews.count) {
//        return nil;
//    }
//    for (ILiveRenderView *view in renderViews) {
//        if (view.identifier.length) {
//            [members addObject:view.identifier];
//        }
//    }
//    return members;
    NSArray *members = [[BYIMManager sharedInstance] getInUpVideoMembers];
    NSMutableArray *userIds = [NSMutableArray array];
    for (BYUpVideoCellModel *model in members) {
        if (model.isUpVideo) {
            [userIds addObject:model.liveRole];
        }
    }
    return userIds;
}

// 上麦列表去重
- (BOOL)verifyIsInUpVideoList:(BYUpVideoCellModel *)model{
    if (!self.upToVideoMembers.count) {
        return NO;
    }
    for (BYUpVideoCellModel *tmpModel in self.upToVideoMembers) {
        if ([model.userId isEqualToString:tmpModel.userId]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - 移除所有上麦者
- (void)removeAllUpToVideoMember{
    NSArray *members = [[BYIMManager sharedInstance] getInUpVideoMembers];
    if (members.count) {
        for (BYUpVideoCellModel *model in members) {
            if (model.isUpVideo) {
                [[BYIMManager sharedInstance] sendDownVideoMessage:model.userId role:model.liveRole succ:nil fail:nil];
            }
        }
    }
    // 清空上麦成员
    [BYIMManager sharedInstance].upToVideoMembers = @[];
}

- (void)removeUpToVideoMember:(NSString *)userId{
    NSArray *members = [[BYIMManager sharedInstance] getInUpVideoMembers];
    NSMutableArray *tmpArr = members.mutableCopy;
    if (members.count) {
        for (BYUpVideoCellModel *model in members) {
            if ([model.userId isEqualToString:userId]) {
                if (model.isUpVideo) {
                    [[BYIMManager sharedInstance] sendDownVideoMessage:model.userId role:model.liveRole succ:nil fail:nil];
                }
                [tmpArr removeObject:model];
                [BYIMManager sharedInstance].upToVideoMembers = tmpArr.copy;
                return;
            }
        }
    }
}
@end
