//
//  BYIMManager.h
//  BY
//
//  Created by 黄亮 on 2018/8/22.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <ImSDK/TIMCallback.h>
//#import <ImSDK/ImSDK.h>
//#import <TILLiveSDK/TILLiveSDK.h>
#import "BYIMCommon.h"
#import "BYUpVideoCellModel.h"

typedef void (^sucCallback) (void);
typedef void (^failCallback) (void);
typedef void (^failCodeCallback) (int code);

typedef void (^sucMsgsCallback) (NSArray *msgs);

@interface BYIMManager : NSObject


/** 上麦成员 */
@property (nonatomic ,strong) NSArray <BYUpVideoCellModel *>*upToVideoMembers;

/** 当前屏幕上的画面个数 */
@property (nonatomic ,strong) NSArray <NSString *> *memberIds;

/** 记录主播的渲染层 */
//@property (nonatomic ,strong) ILiveRenderView *renderView;

/** upToVideoMembers数值变动监听 */
//@property (nonatomic ,copy) void(^upToVideoMembersValueDidChangle) (void);

/**
 获取管理器实例

 @return BYIMManager
 */
+ (BYIMManager *)sharedInstance;


/**
 初始化TIM SDK
 */
//- (void)initSdk;

/** 互动直播登录 */
//- (void)loginILive:(sucCallback)suc  fail:(failCallback)fail;
/** 手动传入账号,sig登录互动直播  */
//- (void)loginILive:(NSString *)userId sig:(NSString *)sig suc:(sucCallback)suc  fail:(failCallback)fail;
/** 用于监听互动直播登录成功 */
//- (void)loginILiveSuccessNotif:(void(^)(BOOL isSuccess))cb;

/** 互动直播登出 */
//- (void)loginOutILive:(sucCallback)suc;



/**
 TIM登录调用

 @param cb  回调
 */
//- (void)login:(sucCallback)cb;

/**
 TIM登出调用

 @param cb 回调
 */
//- (void)loginOut:(sucCallback)cb;

/**
 创建聊天室

 @param groupId 聊天室id
 @param succ 成功回调
 @param fail 失败回调
 */
//- (void)createAVChatRoomGroupId:(NSString *)groupId succ:(sucCallback)succ fail:(failCallback)fail;

/**
 加入聊天室

 @param groupId 聊天室id
 @param succ succ description
 @param fail fail description
 */
//- (void)joinAVChatRoomGroupId:(NSString *)groupId succ:(sucCallback)succ fail:(failCallback)fail;

/**
 修改聊天室成员的角色

 @param userId 用户id
 @param role 角色
 @param succ succ description
 @param fail fail description
 */
//- (void)modifyGroupMemberInfoSetRole:(NSString *)userId role:(BYIMGroupMemberRole)role succ:(sucCallback)succ fail:(failCallback)fail;

/** 接收上麦请求 */
- (void)receiveUpToVideoRequest:(BYUpVideoCellModel *)model;
- (void)receiveUpToVideoRequestWithAudienceData:(NSDictionary *)data;

/** 上麦 （观众调用）*/
- (void)upToVideoRole:(NSString *)role succ:(sucCallback)succ fail:(failCallback)fail;

/** 下麦 */
- (void)downToVideoRole:(NSString *)role succ:(sucCallback)succ fail:(failCallback)fail;

/** 获取在麦成员 */
- (NSArray *)getInUpVideoMembers;

/** 开启相机与mic */
//- (void)openCameraAndMic:(sucCallback)succ fail:(failCallback)fail;
/** 设置上麦状态 */
//- (void)setUpVideoMemberStatus:(NSString *)userId isUp:(BOOL)isUp;

/** 移除所有上麦者 */
- (void)removeAllUpToVideoMember;

/** 移除上麦者 */
- (void)removeUpToVideoMember:(NSString *)userId;

//- (void)setNickName:(NSString *)nick;
//- (void)setHeadImg:(NSString *)headImg;

@end
