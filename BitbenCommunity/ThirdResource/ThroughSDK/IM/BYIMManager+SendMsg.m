//
//  BYIMManager+SendMsg.m
//  BY
//
//  Created by 黄亮 on 2018/8/24.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYIMManager+SendMsg.h"
//#import <TILLiveSDK/TILLiveSDK.h>

@interface BYIMManager()


// ** 会话类型
@property (nonatomic ,assign) BYIMConversationType conversationType;

@property (nonatomic ,copy) void (^messageHandle)(NSArray *msgs);

@end

static const char conType;
static const char msgHandle;
@implementation BYIMManager (SendMsg)

#pragma mark - initMethod


- (BYIMConversationType)conversationType{
    return [objc_getAssociatedObject(self, &conType) integerValue];
}

- (void)setConversationType:(BYIMConversationType)conversationType{
    objc_setAssociatedObject(self, &conType, @(conversationType), OBJC_ASSOCIATION_ASSIGN);
}

- (void)setMessageHandle:(void (^)(NSArray *))messageHandle{
    objc_setAssociatedObject(self, &msgHandle, messageHandle, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(NSArray *))messageHandle{
    return objc_getAssociatedObject(self, &msgHandle);
}

#pragma mark - Listener

// 设置消息监听
- (void)setMessageListener:(void (^)(NSArray *))msgs{
    self.messageHandle = msgs;
}

// 接收消息回调
- (void)onNewMessage:(NSArray *)msgs{
    if (self.messageHandle) {
        self.messageHandle(msgs);
    }
//    for (TIMMessage *msg in msgs) {
//        if ([[msg getElem:0] isMemberOfClass:[TIMGroupSystemElem class]]) break;
//        if ([[msg getElem:0] isMemberOfClass:[TIMProfileSystemElem class]]) break;
//        if ([[msg getElem:0] isMemberOfClass:[TIMCustomElem class]]) {
//            TIMCustomElem *elem = (TIMCustomElem *)[msg getElem:0];
//            NSString *receiveStr = [[NSString alloc]initWithData:elem.data encoding:NSUTF8StringEncoding];
//            NSData * data = [receiveStr dataUsingEncoding:NSUTF8StringEncoding];
//            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//            // 判断当前是否为开播消息
//            if ([dic[@"opera_type"] integerValue] == BY_GUEST_OPERA_TYPE_RECEIVE_STREAM) {
//                break;
//            }
//        }
//        [msg convertToImportedMsg];
//        TIMTextElem *elem = (TIMTextElem *)[msg getElem:0];
//        dispatch_semaphore_t semaphore = dispatch_semaphore_create(1);
//        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [BYToastView tostViewPresentInView:kCommonWindow title:elem.text duration:1.0 complete:^{
//                dispatch_semaphore_signal(semaphore);
//            }];
//        });
//    }
}

- (void) onRecvMessageReceipts:(NSArray*)receipts{
    NSLog(@"%s",__func__);
    NSLog(@"%@",receipts);
}

#pragma mark - customMethod

- (void)sendTextMessage:(NSString *)text succ:(sucCallback)succ fail:(failCallback)fail{
    NSString *string = [text removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
    if (!string.length) return;
    BYSIMMessage *message = [[BYSIMMessage alloc] init];
    message.msg_type = BY_SIM_MSG_TYPE_TEXT;
    message.msg_sender = [AccountModel sharedAccountModel].account_id;
    message.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
    message.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
    message.time = [NSDate getCurrentTimeStr];
    BYSIMTextElem *elem = [[BYSIMTextElem alloc] init];
    elem.text = string;
    message.elem = elem;
    [[BYSIMManager shareManager] sendMessage:message suc:succ fail:fail];
}

- (void)sendImageMessage:(NSString *)imagePath succ:(sucCallback)succ fail:(failCallback)fail{
    
}

//- (void)sendRewardMessage:(CGFloat)amount receicer_user_Name:(NSString *)receicer_user_Name text:(NSString *)text succ:(sucCallback)succ fail:(failCallback)fail{
//
//}

- (void)sendRewardMessage:(CGFloat)amount receicer_user_Name:(NSString *)receicer_user_Name receicer_user_Id:(NSString *)receicer_user_Id text:(NSString *)text succ:(sucCallback)succ fail:(failCallback)fail{
    if (amount <= 0) return;
    NSString *string = text.length ? text : [NSString stringWithFormat:@"%@ 打赏了 %@ %.2fBP",[AccountModel sharedAccountModel].loginServerModel.user.nickname,receicer_user_Name,amount];
    //    NSString *text = [NSString stringWithFormat:@"%@ 打赏了 %@ %.2fBP",[AccountModel sharedAccountModel].loginServerModel.user.nickname,receicer_user_Name,amount];
    NSDictionary *dic = @{@"amount":@(amount),
                          @"receicer_user_Name":nullToEmpty(receicer_user_Name),
                          @"receicer_user_Id":nullToEmpty(receicer_user_Id),
                          @"text":string,
                          @"opera_type":@(2)
                          };
    [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_REWARD data:dic suc:^{
        if (succ) succ();
        showDebugToastView(@"打赏消息发送成功", kCommonWindow);
    } fail:^{
        if (fail) fail();
        showDebugToastView(@"打赏消息发送失败", kCommonWindow);
    }];
}


- (void)sendRewardMessage:(CGFloat)amount receicer_user_Name:(NSString *)receicer_user_Name succ:(sucCallback)succ fail:(failCallback)fail{
    [self sendRewardMessage:amount receicer_user_Name:receicer_user_Name text:@"" succ:succ fail:fail];
}

- (void)sendOpenLiveMessage:(sucCallback)succ fail:(failCallback)fail{
    [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_RECEIVE_STREAM data:nil suc:^{
        if (succ) succ();
        showDebugToastView(@"开播消息发送成功", kCommonWindow);
    } fail:^{
        if (fail) fail();
        showDebugToastView(@"开播消息发送失败", kCommonWindow);
    }];
}

- (void)sendUpVideoMessage:(sucCallback)succ fail:(failCallback)fail{
    NSDictionary *dic = @{@"opera_type":@(4)};
    [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_UP_VIDEO data:dic suc:^{
        if (succ) succ();
        showToastView(@"连麦申请已发送", CURRENT_VC.view);
    } fail:^{
        if (fail) fail();
        showToastView(@"上麦消息发送失败", CURRENT_VC.view);
    }];
}

- (void)sendAgreeUpVideoMessage:(NSString *)userId stream_id:(NSString *)stream_id succ:(sucCallback)succ fail:(failCallback)fail{
    NSString *role = [self verifyIsMaxUpVideo];
    if (!role.length) {
        showToastView([NSString stringWithFormat:@"当前最多只可连麦%i位观众",MAX_UPVIDEO_NUM], kCommonWindow);
        return;
    }
    NSDictionary *dic = @{@"opera_type":@(5),
                          @"user_id":userId,
                          };
    [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_AGREE_UP_VIDEO data:dic suc:^{
        if (succ) succ();
        showDebugToastView(@"同意上麦消息发送成功", kCommonWindow);
    } fail:^{
        if (fail) fail();
        showDebugToastView(@"同意上麦消息发送失败", kCommonWindow);
    }];
}

- (void)sendDownVideoMessage:(NSString *)userId role:(NSString *)role succ:(sucCallback)succ fail:(failCallback)fail{
    NSDictionary *dic = @{@"opera_type":@(6),
                          @"user_id":userId,
                          };
    [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_DOWN_VIDEO data:dic suc:^{
        if (succ) succ();
        showDebugToastView(@"下麦消息发送成功", kCommonWindow);
    } fail:^{
        if (fail) fail();
        showDebugToastView(@"下麦消息发送失败", kCommonWindow);
    }];
}

- (void)sendStopPushStreamMessage:(sucCallback)succ fail:(failCallback)fail{
    [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM data:nil suc:^{
        if (succ) succ();
        showDebugToastView(@"结束直播消息发送成功", kCommonWindow);
    } fail:^{
        if (fail) fail();
        showDebugToastView(@"结束直播消息发送失败", kCommonWindow);
    }];
}

- (void)sendStopPushStreamMessage:(NSDictionary *)data succ:(sucCallback)succ fail:(failCallback)fail{
    [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM data:data suc:^{
        if (succ) succ();
        showDebugToastView(@"结束直播消息发送成功", kCommonWindow);
    } fail:^{
        if (fail) fail();
        showDebugToastView(@"结束直播消息发送失败", kCommonWindow);
    }];
}

// 校验最大上麦数
- (NSString *)verifyIsMaxUpVideo{
    NSMutableArray *tmpArr = [NSMutableArray array];
    NSMutableArray *guestMembers = [NSMutableArray array];
    for (BYUpVideoCellModel *model in self.upToVideoMembers) {
        if (model.isUpVideo) {
            [tmpArr addObject:model];
            [guestMembers addObject:model.liveRole];
        }
    }
    if (!guestMembers.count) {
        return LIVEGUEST_MEMBERS[0];
    }
    
    if (guestMembers.count >= MAX_UPVIDEO_NUM) {
        return @"";
    }
    
    NSString *role = @"";
    for (NSString *tmpRole in LIVEGUEST_MEMBERS) {
        if ([guestMembers indexOfObject:tmpRole] == NSNotFound) {
            role = tmpRole;
            return role;
            break;
        }
    }
    return role;
}


@end
