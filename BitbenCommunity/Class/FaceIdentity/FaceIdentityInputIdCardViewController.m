//
//  FaceIdentityInputIdCardViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FaceIdentityInputIdCardViewController.h"

@interface FaceIdentityInputIdCardViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *identityTableView;
@property (nonatomic,strong)NSArray *identityArr;
@end

@implementation FaceIdentityInputIdCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"输入证件信息";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.identityArr = @[@[@"标题"],@[@"姓名",@"身份证"],@[@"下一步"]];
}

#pragma mark - arrayWithInit
-(void)createTableView{
    if (!self.identityTableView){
        self.identityTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.identityTableView.dataSource = self;
        self.identityTableView.delegate = self;
        [self.view addSubview:self.identityTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.identityArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.identityArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.identityArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"姓名" sourceArr:self.identityArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWInputTextFieldTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        return cellWithRowTwo;
    } else {
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        return cellWithRowThr;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}




@end
