//
//  FaceWaringIconTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FaceWaringIconTableViewCell.h"

@interface FaceWaringIconTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *infoLabel;
@end

@implementation FaceWaringIconTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImgView];
    
    self.infoLabel = [GWViewTool createLabelFont:@"19" textColor:@"454545"];
    self.infoLabel.font = [self.infoLabel.font boldFont];
    self.infoLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.infoLabel];
    
    [self infoManager];
}

#pragma mark - infoManager
-(void)infoManager{
    self.iconImgView.image = [UIImage imageNamed:@"icon_face_shengming"];
    self.iconImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(51)) / 2., 51, LCFloat(51), LCFloat(61));
    
    self.infoLabel.text = @"授权声明";
    
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(61);
    cellHeight += LCFloat(39);
    cellHeight += [NSString contentofHeightWithFont:[[UIFont fontWithCustomerSizeName:@"19"] boldFont]];
    return cellHeight;
}

@end
