//
//  FaceWaringViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FaceWaringViewController.h"
#import "FaceWaringTextTableViewCell.h"
#import "FaceWaringIconTableViewCell.h"

static NSString *info = @"本APP运营方为确保用户身份真实性，向您提供更好的安全保障，您可以通过提交身份证等身份信息或面部特征等生物识别信息（均属于个人敏感信息）来完成具体产品服务所需或必要的实人认证。上述信息将仅用于验证用户身份的真实性。\r\r我们会采用行业领先的技术来保护您提供的个人信息，并使用加密、限权等方式避免其被用于其他用途。\r\r点击“同意”则表示本人同意我们根据以上方式和目的收集、使用及存储您提供的本人身份材料、面部特征等信息用于实人认证。";
@interface FaceWaringViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *faceWaringTableView;
@property (nonatomic,strong)NSArray *faceArr;
@end

@implementation FaceWaringViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"授权声明";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.faceArr = @[@[@"图标",@"声明"],@[@"内容"],@[@"同意"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.faceWaringTableView){
        self.faceWaringTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.faceWaringTableView.dataSource = self;
        self.faceWaringTableView.delegate = self;
        [self.view addSubview:self.faceWaringTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.faceArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.faceArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"图标" sourceArr:self.faceArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        FaceWaringIconTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[FaceWaringIconTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"内容" sourceArr:self.faceArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        FaceWaringTextTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[FaceWaringTextTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferInfo = info;
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"同意" sourceArr:self.faceArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSLog(@"123");
        }];
        return cellWithRowThr;
    }
    
    return  nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [FaceWaringTextTableViewCell calculationCellHeightWithInfo:info];
}



@end
