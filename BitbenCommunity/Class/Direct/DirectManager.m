//
//  DirectManager.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/19.
//  Copyright © 2019 币本. All rights reserved.
//

#import "DirectManager.h"
#import "BYHomeBannerModel.h"
#import "ArticleDetailRootViewController.h"
#import "BYVideoRoomController.h"
#import "BYVODPlayController.h"
#import "BYPPTRoomController.h"
#import "BYLiveConfig.h"
#import "BYILiveRoomController.h"
#import "BYVideoLiveController.h"
#import "NetworkAdapter+Center.h"
#import "BYLiveController.h"
#import "AppointmentModel.h"
#import "BYMicroLiveController.h"
#import "BYLiveDetailController.h"
#import "BYPushFlowController.h"
#import "BYPersonHomeController.h"
#import "BYILiveController.h"
#import "BYHomeNewsletterController.h"

@implementation DirectModel

@end

@implementation DirectManager

#pragma mark - 终极direct
+(void)directMaxWithModel:(id)directInModel block:(void(^)())block{
    // 1. 创建directModel
    DirectModel *directModel;
    if ([directInModel isKindOfClass:[CenterMessageRootSingleModel class]]){
         CenterMessageRootSingleModel *directSingleModel = (CenterMessageRootSingleModel *)directInModel;
        directModel = [[DirectModel alloc]init];
        directModel.type_code = directSingleModel.type_code;
        directModel.theme_id = directSingleModel.theme_id;
        directModel.live_type = directSingleModel.live_type;
        directModel.user_message_id = directSingleModel.user_message_id;
        directModel.theme_type = directSingleModel.theme_type;
    } else if ([directInModel isKindOfClass:[DirectModel class]]){
        directModel = directInModel;
    } else if ([directInModel isKindOfClass:[AppointmentModel class]]){
        AppointmentModel *directSingleModel = (AppointmentModel *)directInModel;
        directModel = [[DirectModel alloc]init];
        directModel.type_code = PushTypeSystemMail;
        directModel.theme_id = directSingleModel.live_record_id;
        directModel.live_type = directSingleModel.live_type;
//        directModel.user_message_id = directSingleModel.user_message_id;
        directModel.theme_type = BY_THEME_TYPE_LIVE;
    } else if ([directInModel isKindOfClass:[IntelligenceSubSingleModel class]]){
        IntelligenceSubSingleModel *directSingleModel = (IntelligenceSubSingleModel *)directInModel;
        
        directModel = [[DirectModel alloc]init];
        directModel.type_code = PushTypeSystemMail;
        directModel.theme_id = directSingleModel.theme_id;
        directModel.live_type = directSingleModel.live_type;
//        directModel.user_message_id = directSingleModel.mess;
        directModel.theme_type = directSingleModel.theme_type;
    }
    
    [DirectManager directMaxSubWithModel:directModel];
    
    if (block){
        block();
    }
}

+(void)directMaxSubWithModel:(DirectModel *)directModel {
    if (directModel.type_code == PushTypeUserBeConcerned){              // 被关注
        CenterMessageFansViewController *fansVC = [[CenterMessageFansViewController alloc]init];
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:fansVC animated:YES];
    } else if (directModel.type_code == PushTypeUserAuth || directModel.type_code == PushTypeSystemAuthSuccess || directModel.type_code ==  PushTypeSystemAuthFail){             // 用户 = 认证
        MineRealNameViewController *realVC = [[MineRealNameViewController alloc]init];
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:realVC animated:YES];
    } else if (directModel.type_code == PushTypeUserIncome){           // 每日收益
        CenterBBTDetailedViewController *bbtVC = [[CenterBBTDetailedViewController alloc]init];
        bbtVC.transferFinaniceType = BBTMyFinaniceTypeBpSuoding;
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:bbtVC animated:YES];
    } else if (directModel.type_code == PushTypeArticleSupport || directModel.type_code == PushTypeLiveMyBeZan || directModel.type_code == PushTypeLiveMyCommentBeZan || directModel.type_code == PushTypeArticleSubSupport){           // 点赞
        CenterMessageZanViewController *messageZanViewController = [[CenterMessageZanViewController alloc]init];
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:messageZanViewController animated:YES];
    } else if (directModel.type_code == PushTypeArticleComment ||directModel.type_code == PushTypeLiveSuport || directModel.type_code == PushTypeLiveSubSuport || directModel.type_code == PushTypeArticleSubComment){        // 文章评论
        CenterMessageCommentViewController *centerMessageVC = [[CenterMessageCommentViewController alloc]init];
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:centerMessageVC animated:YES];
    } else if (directModel.type_code == PushTypeSystemAttendance){      // 签到成功
        CenterBBTDetailedViewController *bbtVC = [[CenterBBTDetailedViewController alloc]init];
        bbtVC.transferFinaniceType = BBTMyFinaniceTypeBpSuoding;
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:bbtVC animated:YES];
    } else if (directModel.type_code == PushTypeSystemInvitation){      // 邀请好友
        VersionInvitationLeijiJiangliViewController *leijijiangliVC = [[VersionInvitationLeijiJiangliViewController alloc]init];
        leijijiangliVC.transferType = VersionInvitationLeijiJiangliViewControllerTypeYaoqingjilu;
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:leijijiangliVC animated:YES];
    } else if (directModel.type_code == PushTypeSystemInviteMember){    // 邀请成功
        VersionInvitationLeijiJiangliViewController *leijijiangliVC = [[VersionInvitationLeijiJiangliViewController alloc]init];
        leijijiangliVC.transferType = VersionInvitationLeijiJiangliViewControllerTypeJiasujilu;
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:leijijiangliVC animated:YES];
    } else if (directModel.type_code == PushTypeSystemInviteRegister){  // 好友注册邀请
        VersionInvitationLeijiJiangliViewController *leijijiangliVC = [[VersionInvitationLeijiJiangliViewController alloc]init];
        leijijiangliVC.transferType = VersionInvitationLeijiJiangliViewControllerTypeYaoqingjilu;
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:leijijiangliVC animated:YES];
    } else if (directModel.type_code == PushTypeSystemMail){
        if (directModel.theme_type == 0){                       // 跳转直播
            [DirectManager commonPushControllerWithThemeType:directModel.theme_type liveType:directModel.live_type themeId:directModel.theme_id];
        } else if (directModel.theme_type == 1){                // 1.跳转文章
            ArticleDetailRootViewController *articleVC = [[ArticleDetailRootViewController alloc]init];
            articleVC.transferPageType = ArticleDetailRootViewControllerTypeBanner;
            articleVC.transferArticleId = directModel.theme_id;
            [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:articleVC animated:YES];
        } else if (directModel.theme_type == 2){
            PDWebViewController *webViewController = [[PDWebViewController alloc]init];
            [webViewController webDirectedWebUrl:directModel.theme_id];
            [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:webViewController animated:YES];
        }
    } else if (directModel.type_code == PushTypeSystemKuaixun){         // 快讯
        [[BYLiveHomeControllerV1 shareManager] selectSegmentAtIndex:BY_HOME_SEGMENT_SUB_KUAIXUN];
        BYHomeNewsletterController *kuaixunVC = (BYHomeNewsletterController *)[[BYLiveHomeControllerV1 shareManager] getSegmentSubControllerAtIndex:BY_HOME_SEGMENT_SUB_KUAIXUN];
        [kuaixunVC notificationDirectWithShowManager:directModel.theme_id];
    
    } else if (directModel.type_code == PushTypeSystemMsg){
        // 现在不跳
        return;
    } else if (directModel.type_code == PushTypeLiveMyLink || directModel.type_code == PushTypeLiveMyAppointment){
        [DirectManager commonPushControllerWithThemeType:directModel.theme_type liveType:directModel.live_type themeId:directModel.theme_id];
    } else if (directModel.type_code == PushTypeLiveMyCreateBefore || directModel.type_code == PushTypeLiveMyCreateAfter){     // 直播
        [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_APP_LIVE_HOME liveType:directModel.live_type themeId:directModel.theme_id];
    } else if (directModel.type_code == PushTypeWalletSendSuccess ||
          directModel.type_code == PushTypeWalletSendFail ||
          directModel.type_code == PushTypeWalletGetSuccess ||
          directModel.type_code == PushTypeWalletGetFail ||
          directModel.type_code == PushTypeWalletMemberFail ||
          directModel.type_code == PushTypeWalletMemberSuccess ||
          directModel.type_code == PushTypeWalletTransferFail ||
          directModel.type_code == PushTypeWalletCashSuccess ||
          directModel.type_code == PushTypeWalletCashFail ||
          directModel.type_code == PushTypeWalletCashValidFail ||
          directModel.type_code == PushTypeWalletMemberMoreSuccess ||
          directModel.type_code == PushTypeWalletMemberMoreFail ||
          directModel.type_code == PushTypeWalletMemberUpSuccess ||
          directModel.type_code == PushTypeWalletMemberUpFail){
        if (directModel.theme_id.length){
            MessageNewWalletTransferDetailViewController *walletTransferVC = [[MessageNewWalletTransferDetailViewController alloc]init];
            walletTransferVC.transferMessageId = directModel.user_message_id;
            [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:walletTransferVC animated:YES];
        }
    }
}

















+(void)directToControllerWithBannerModel:(BYHomeBannerModel *)bannerModel childController:(AbstractViewController *)controller{
    AbstractViewController *commonVC;
    commonVC = (AbstractViewController *)controller;
    
    if (bannerModel.theme_type == BY_THEME_TYPE_ARTICLE) { // 跳文章
        ArticleDetailRootViewController *articleController = [[ArticleDetailRootViewController alloc] init];
        articleController.transferArticleId = bannerModel.theme_id;
        articleController.transferPageType = ArticleDetailRootViewControllerTypeBanner;
        
        [controller.navigationController pushViewController:articleController animated:YES];
    }
    else if (bannerModel.theme_type == BY_THEME_TYPE_LIVE) { // 跳转直播
        switch (bannerModel.live_type) {
            case BY_NEWLIVE_TYPE_ILIVE:
            {
//                BYLiveConfig *config = [[BYLiveConfig alloc] init];
//                config.isHost = NO;
//                BYILiveRoomController *ilivewRoomController = [[BYILiveRoomController alloc] initWithConfig:config];
//                ilivewRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
//                [controller.navigationController pushViewController:ilivewRoomController animated:YES];
                BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
                liveDetailController.live_record_id = nullToEmpty(bannerModel.theme_id);
                liveDetailController.isHost = NO;
                liveDetailController.navigationTitle = @"个人互动直播";
                [controller.navigationController pushViewController:liveDetailController animated:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_UPLOAD_VIDEO:
            {
                BYVideoRoomController *videoRoomController = [[BYVideoRoomController alloc] init];
                videoRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
                [controller.navigationController pushViewController:videoRoomController animated:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_AUDIO_PPT:
            case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
            {
//                BYPPTRoomController *pptRoomController = [[BYPPTRoomController alloc] init];
//                pptRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
//                pptRoomController.isHost = NO;
//                [controller.navigationController pushViewController:pptRoomController animated:YES];
                BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
                liveDetailController.live_record_id = nullToEmpty(bannerModel.theme_id);
                liveDetailController.isHost = NO;
                liveDetailController.navigationTitle = @"微直播";
                [controller.navigationController pushViewController:liveDetailController animated:YES];

            }
                break;
            case BY_NEWLIVE_TYPE_VOD_AUDIO:
            case BY_NEWLIVE_TYPE_VOD_VIDEO:
            {
                BYVODPlayController *vodPlayController = [[BYVODPlayController alloc] init];
                vodPlayController.live_record_id = nullToEmpty(bannerModel.theme_id);
                [controller.navigationController pushViewController:vodPlayController animated:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_VIDEO:
            {
//                BYVideoLiveController *videoLiveController = [[BYVideoLiveController alloc] init];
//                videoLiveController.live_record_id = nullToEmpty(bannerModel.theme_id);
//                videoLiveController.isHost = NO;
//                [controller.navigationController pushViewController:videoLiveController animated:YES];
                BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
                liveDetailController.live_record_id = nullToEmpty(bannerModel.theme_id);
                liveDetailController.isHost = NO;
                liveDetailController.navigationTitle = @"推流直播";
                [controller.navigationController pushViewController:liveDetailController animated:YES];
            }
                break;
            default:
                break;
        }
    }
    else if (bannerModel.theme_type == BY_THEME_TYPE_APP_H5) { // 跳转app内部h5
        PDWebViewController *webViewController = [[PDWebViewController alloc] init];
        [webViewController webDirectedWebUrl:bannerModel.theme_id];
        [controller.navigationController pushViewController:webViewController animated:YES];

    }
    
}

+ (void)commonPushControllerWithThemeType:(BY_THEME_TYPE)themeType
                                 liveType:(BY_NEWLIVE_TYPE)liveType
                                  themeId:(NSString *)themeId{
    [DirectManager destoryLiveController];
    AbstractViewController *currentController = (AbstractViewController *)[[BYTabbarViewController sharedController] getCurrentController];
//    [currentController hidesTabBarWhenPushed];
    BYCommonViewController *controller;
    if ([currentController isKindOfClass:[AbstractViewController class]]) {
        controller = (BYCommonViewController *)currentController;
    }
     if (themeType == BY_THEME_TYPE_APP_LIVE_HOME){
        
         [DirectManager liveManagerWithThemeId:themeId];
         
        return;
    } else if (themeType == BY_THEME_TYPE_ARTICLE) { // 跳文章
        ArticleDetailRootViewController *articleController = [[ArticleDetailRootViewController alloc] init];
        articleController.transferArticleId = themeId;
        articleController.transferPageType = ArticleDetailRootViewControllerTypeBanner;
        [currentController.navigationController pushViewController:articleController animated:YES];
    }
    else if (themeType == BY_THEME_TYPE_LIVE) { // 跳转直播
        switch (liveType) {
            case BY_NEWLIVE_TYPE_ILIVE:
            {
                BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
                liveDetailController.live_record_id = nullToEmpty(themeId);
                liveDetailController.isHost = NO;
                liveDetailController.navigationTitle = @"个人互动直播";
                [currentController.navigationController pushViewController:liveDetailController animated:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_UPLOAD_VIDEO:
            {
                BYVideoRoomController *videoRoomController = [[BYVideoRoomController alloc] init];
                videoRoomController.live_record_id = nullToEmpty(themeId);
                if ([[AccountModel sharedAccountModel] hasLoggedIn]){
                    [currentController.navigationController pushViewController:videoRoomController animated:YES];
                } else {
                    if (controller) {
                        [controller authorizePush:videoRoomController animation:YES];
                        return;
                    }
                    [currentController.navigationController pushViewController:videoRoomController animated:YES];
                }
            }
                break;
            case BY_NEWLIVE_TYPE_AUDIO_PPT:
            case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
            case BY_NEWLIVE_TYPE_VIDEO:
            {
                BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
                liveDetailController.live_record_id = nullToEmpty(themeId);
                liveDetailController.isHost = NO;
                liveDetailController.navigationTitle = liveType == BY_NEWLIVE_TYPE_VIDEO ? @"推流直播" : @"微直播";
                [currentController.navigationController pushViewController:liveDetailController animated:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_VOD_AUDIO:
            case BY_NEWLIVE_TYPE_VOD_VIDEO:
            {
                BYVODPlayController *vodPlayController = [[BYVODPlayController alloc] init];
                vodPlayController.live_record_id = nullToEmpty(themeId);
                [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:vodPlayController animated:YES];
            }
                break;
            default:
                break;
        }
    }
    else if (themeType == BY_THEME_TYPE_APP_H5) { // 跳转app内部h5
        PDWebViewController *webViewController = [[PDWebViewController alloc] init];
        [webViewController webDirectedWebUrl:themeId];
        [currentController.navigationController pushViewController:webViewController animated:YES];
    }
}

+ (void)destoryLiveController{
    NSMutableArray *array = [NSMutableArray arrayWithArray:CURRENT_VC.navigationController.viewControllers];
    [array enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[BYILiveController class]] ||
            [obj isKindOfClass:[BYVODPlayController class]] ||
            [obj isKindOfClass:[BYVideoRoomController class]]) {
            [CURRENT_VC.rt_navigationController removeViewController:obj];
        }else if ([obj isKindOfClass:[BYMicroLiveController class]] ||
                  [obj isKindOfClass:[BYPushFlowController class]]) {
            [CURRENT_VC.rt_navigationController removeViewController:obj];
            UIViewController *controller = CURRENT_VC.navigationController.viewControllers[idx - 1];
            [CURRENT_VC.rt_navigationController removeViewController:controller];
        }
    }];
}


#pragma mark -
+(void)liveManagerWithThemeId:(NSString *)themeId{
    [[NetworkAdapter sharedAdapter] sendRequestToGetLiveModel:themeId block:^(BYCommonLiveModel *liveModel) {
        BYLiveController *livecontroller = [[BYLiveController alloc] init];
        livecontroller.model = liveModel;
        [[BYTabbarViewController sharedController].currentController.navigationController pushViewController:livecontroller animated:YES];

    }];
}

@end
