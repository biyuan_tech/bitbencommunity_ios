//
//  DirectManager.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/19.
//  Copyright © 2019 币本. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BYHomeBannerModel.h"
#import "CenterMessageRootSingleModel.h"
#import "MessageNewWalletTransferDetailViewController.h"

NS_ASSUME_NONNULL_BEGIN


@interface DirectModel : FetchModel

@property (nonatomic,assign)PushType type_code;
@property (nonatomic,copy)NSString *theme_id;
@property (nonatomic,assign)BY_NEWLIVE_TYPE live_type;         /**< 如果是直播的话，返回直播类型*/
@property (nonatomic,copy)NSString *user_message_id;           /**< 用来转账进行跳转*/
@property (nonatomic,assign)BY_THEME_TYPE theme_type;          /**< 判断直播还是文章还是外链*/

@end



@interface DirectManager : NSObject

#pragma mark - 终极direct
+(void)directMaxWithModel:(id)directModel block:(void(^)())block;


+(void)directToControllerWithBannerModel:(BYHomeBannerModel *)bannerModel childController:(BYCommonViewController *)controller;

+ (void)commonPushControllerWithThemeType:(BY_THEME_TYPE)themeType liveType:(BY_NEWLIVE_TYPE)liveType themeId:(NSString *)themeId;

+(void)liveManagerWithThemeId:(NSString *)themeId;
@end

NS_ASSUME_NONNULL_END
