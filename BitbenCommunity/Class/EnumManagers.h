//
//  EnumManagers.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, MessageType) {
    MessageTypeRoot = 0,                /**< 消息中心*/
    MessageTypeBlock = 1,               /**< 钱包消息*/
    MessageTypeComment = 2,             /**< 评论消息*/
    MessageTypeZan = 3,                 /**< 点赞消息*/
    MessageTypeLink = 4,                /**< 关注消息*/
    MessageTypeLive = 5,                /**< 开播消息*/
    MessageTypeShouyiEveryDay = 6,      /**< 每日收益*/
};


typedef NS_ENUM(NSInteger,PushType) {
    PushTypeLive = 100,                             /**< 直播*/
    PushTypeLiveAppointment = 101,                  /**< 直播预约关注*/
    PushTypeLiveChat = 102,                         /**< 直播私聊*/
    PushTypeLiveProfit = 103,                       /**< 直播昨日收益*/
    PushTypeLiveDelete = 104,                       /**< 某直播删除下线*/
    PushTypeLiveMyLink = 105,                       /**< 我关注的将要开播*/
    PushTypeLiveMyAppointment = 106,                /**< 我预约的将要开播*/
    PushTypeLiveSuport = 107,                       /**< 直播评论*/
    PushTypeLiveSubSuport = 108,                    /**< 直播评论的评论*/
    PushTypeLiveMyLinkBefore = 109,                 /**< 关注的用户即将开播*/
    PushTypeLiveMyLinkAfter = 110,                  /**< 关注的用户正在热播中*/
    PushTypeLiveMyAppointmentBefore = 111,          /**< 预约的直播即将开播*/
    PushTypeLiveMyAppointmentAfter = 112,           /**< 预约的直播正在热播中*/
    PushTypeLiveMyCreateBefore = 113,               /**< 创建的直播即将开播*/
    PushTypeLiveMyCreateAfter = 114,                /**<"创建的直播已超时"*/
    PushTypeLiveMyBeZan = 115,                      /**<"创建的直播已被赞"*/
    PushTypeLiveMyCommentBeZan = 116,               /**<"点赞-直播评论被赞"*/
    // 关注
    PushTypeUser = 200,                             /**< 用户*/
    PushTypeUserBeConcerned = 201,                  /**< 被关注*/
    PushTypeUserBeRelease = 202,                    /**< 关注的 人发布了内容*/
    PushTypeUserAuth = 203,                         /**< 认证*/
    PushTypeUserIncome = 204,                       /**< "收益消息", "每日收益"*/
   
    // 文章
    PushTypeArticle = 300,                          /**< 文章*/
    PushTypeArticleDelete = 301,                    /**< 文章被删除*/
    PushTypeArticleInteraction = 302,               /**< 互动*/
    PushTypeArticleProfit = 303,                    /**< 文章昨日收益*/
    PushTypeArticleSupport = 304,                   /**< 文章赞*/
    PushTypeArticleSubSupport = 305,                /**< 文章评论赞*/
    PushTypeArticleComment = 306,                   /**< 文章评论*/
    PushTypeArticleSubComment = 307,                /**< 文章评论的评论*/
    
    // 系统
    PushTypeSystem = 400,                           /**< 系统*/
    PushTypeSystemMail = 401,                       /**< 站内信*/
    PushTypeSystemMsg = 402,                        /**< 系统消息*/
    PushTypeSystemOther = 403,                      /**< APP 推送*/
    PushTypeSystemReport = 404,                     /**< app 举报*/
    PushTypeSystemBeReport = 405,                   /**< app 被举报*/
    PushTypeSystemRecharge = 406,                   /**< app 充值*/
    PushTypeSystemTransaction = 407,                /**< app 交易*/
    PushTypeSystemAttendance = 408,                 /**<"签到成功", "系统通知", "您已签到成功，获取{0}BP，可在钱包中查看。"),*/
    PushTypeSystemInvitation = 409,                 /**<"邀请好友成功", "系统通知", "您手机号为{0}的好友已通过您的邀请注册成功。"),*/
    PushTypeSystemPassword = 410,                   /**<"修改密码成功", "系统通知", "您已修改密码成功。"),*/
    PushTypeSystemInviteRegister = 411,             /**<"系统通知-邀请好友成功", "好友注册提醒", "恭喜您邀请的好友已完成注册，奖励已发放至您的资产中"),*/
    PushTypeSystemInviteMember = 412,               /**<"系统通知-邀请会员成功", "会员绑定", "您的好友已完成会员绑定，您已获得会员每日加速回馈收益"),*/
    PushTypeSystemAuthSuccess = 413,                /**<"系统通知-认证成功", "认证提醒", "恭喜您已完成{0}用户身份认证"),*/
    PushTypeSystemAuthFail = 414,                   /**<"系统通知-认证失败", "认证提醒"*/
    PushTypeSystemKuaixun = 415,                    /**< 快讯*/
    // 网页
    PushTypeWeb = 500,
    
    // 钱包
    PushTypeWallet = 1000,                          /**<"钱包", "", ""),*/
    PushTypeWalletSendSuccess = 1001,               /**<"转账成功", "交易提醒", "您的转账已成功。-{0}{1}"),*/
    PushTypeWalletSendFail = 1002,                  /**<"交易异常", "交易提醒", "转账失败，我们已将原额返还至您的钱包中。+{0}{1}"),*/
    PushTypeWalletGetSuccess = 1003,                /**<"收款成功", "交易提醒", "收款成功。+{0}{1}"),*/
    PushTypeWalletGetFail = 1004 ,                  /**<"收款失败", "交易提醒", "收款失败"),*/
    PushTypeWalletValidSuccess = 1005,              /**<"身份验证成功", "钱包消息", "身份验证成功，点击重置安全密码"),*/
    PushTypeWalletValidFail = 1006,                 /**<"身份验证失败", "钱包消息", "身份验证失败，请提交有效证件"),*/
    PushTypeWalletUpdatePassword = 1007,            /**<"修改密码成功", "钱包消息", "您已修改密码成功"),*/
    PushTypeWalletRetrievePassword = 1008,          /**<"找回密码成功", "钱包消息", "您已找回密码成功，请妥善保管"),*/
    PushTypeWalletTransferSuccess = 1009,            /**<  钱包转账通过*/
    PushTypeWalletTransferFail = 1010,               /**<  钱包转账驳回*/
    PushTypeWalletMemberSuccess = 1011,              /**< "会员充值消息", "会员充值成功"*/
    PushTypeWalletMemberFail = 1012,                 /**< "会员充值消息", "会员充值失败"*/
    PushTypeWalletCashSuccess = 1013,                /**< "提现消息", "提现成功"*/
    PushTypeWalletCashFail = 1014,                   /**< "提现消息", "提现失败"*/
    PushTypeWalletCashValidSuccess = 1015,              /**< "提现审核通过", "交易提醒"*/
    PushTypeWalletCashValidFail = 1016,                 /**<"提现审核驳回", "交易提醒", "您的提现未通过审核"),*/
    PushTypeWalletMemberMoreSuccess = 1017,             /**<"会员续费成功", "交易提醒", "您的会员续费已成功！有效截止日期为：{0}"),*/
    PushTypeWalletMemberMoreFail = 1018,                /**<"会员续费失败", "交易提醒", "会员续费失败，已原额返还至您的钱包中"),*/
    PushTypeWalletMemberUpSuccess = 1019,               /**<"会员升级成功", "交易提醒", "您的会员升级已成功！恭喜您成为{0}；{1}生效"),*/
    PushTypeWalletMemberUpFail = 1020,                  /**<"会员升级失败", "交易提醒", "会员升级失败，已原额返还至您的钱包中");*/
};



@interface EnumManagers : FetchModel

@end

NS_ASSUME_NONNULL_END
