//
//  AppointmentDongtaiRootViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentDongtaiRootViewController.h"
#import "AppointmentDongtaiRootListTableViewCell.h"
#import "AppointmentModel.h"


@interface AppointmentDongtaiRootViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *dongtaiTableView;
@property (nonatomic,strong)NSMutableArray *dongtaiMutableArr;
@end

@implementation AppointmentDongtaiRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetDongtaiManamer];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"最新动态";
    self.view.backgroundColor = RGB(242, 244, 245, 1);
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.dongtaiMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.dongtaiTableView){
        self.dongtaiTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.dongtaiTableView.dataSource = self;
        self.dongtaiTableView.delegate = self;
        [self.view addSubview:self.dongtaiTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.dongtaiTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetDongtaiManamer];
    }];
    [self.dongtaiTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetDongtaiManamer];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dongtaiMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    AppointmentDongtaiRootListTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[AppointmentDongtaiRootListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferModel = [self.dongtaiMutableArr objectAtIndex:indexPath.row];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [AppointmentDongtaiRootListTableViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AppointmentModel *transferModel = [self.dongtaiMutableArr objectAtIndex:indexPath.row];
    [DirectManager directMaxWithModel:transferModel block:NULL];
}

-(void)sendRequestToGetDongtaiManamer{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page_number":@(self.dongtaiTableView.currentPage),@"page_size":@"10"};
    [[NetworkAdapter sharedAdapter]fetchWithPath:page_appointment requestParams:params responseObjectClass:[AppointmentListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            AppointmentListModel *listModel = (AppointmentListModel *)responseObject;
            if (strongSelf.dongtaiTableView.isXiaLa){
                [strongSelf.dongtaiMutableArr removeAllObjects];
            }
            
            [strongSelf.dongtaiMutableArr addObjectsFromArray:listModel.content];
            [strongSelf.dongtaiTableView reloadData];
            
            if (strongSelf.dongtaiMutableArr.count){
                [strongSelf.dongtaiTableView dismissPrompt];
            } else {
                [strongSelf.dongtaiTableView showPrompt:@"当前没有最新动态" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }
            if (strongSelf.dongtaiTableView.isXiaLa){
                [strongSelf.dongtaiTableView stopPullToRefresh];
            } else {
                [strongSelf.dongtaiTableView stopFinishScrollingRefresh];
            }
        }
    }];
}

@end
