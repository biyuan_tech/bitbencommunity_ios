//
//  AppointmentFengcaiTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentFengcaiTableViewCell.h"

@interface AppointmentFengcaiTableViewCollectionViewCell()
@property (nonatomic,strong)PDImageView *bgView1;
@property (nonatomic,strong)PDImageView *bgView2;
@property (nonatomic,strong)PDImageView *bgView3;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *desLabel;
@property (nonatomic,strong)PDImageView *dotImgView;

@end

@implementation AppointmentFengcaiTableViewCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView1 = [[PDImageView alloc]init];
    self.bgView1.backgroundColor = [UIColor clearColor];
    self.bgView1.image = [Tool stretchImageWithName:@"bg_chuanglianren_top"];
    self.bgView1.frame = CGRectMake(0, 0, LCFloat(250), LCFloat(160));
    [self addSubview:self.bgView1];
    
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame = CGRectMake(LCFloat(24), LCFloat(24), LCFloat(48), LCFloat(48));
    [self addSubview:self.avatarImgView];
    
    self.nameLabel = [GWViewTool createLabelFont:@"18" textColor:@"323232"];
    self.nameLabel.font = [self.nameLabel.font boldFont];
    [self addSubview:self.nameLabel];
    
    self.titleLabel = [GWViewTool createLabelFont:@"13" textColor:@"828282"];
    self.titleLabel.numberOfLines = 0;
    [self addSubview:self.titleLabel];
    
    self.bgView2 = [[PDImageView alloc]init];
    self.bgView2.backgroundColor = [UIColor clearColor];
    UIImage *infoImg = [UIImage imageNamed:@"bg_chuanglianren_nor"];
    self.bgView2.frame = CGRectMake(0, CGRectGetMaxY(self.bgView1.frame), self.bgView1.size_width, LCFloat(50));
    self.bgView2.image = [infoImg stretchableImageWithLeftCapWidth:infoImg.size.width * 0.73 topCapHeight:infoImg.size.height * 0.5];
    [self addSubview:self.bgView2];
    
    self.bgView3 = [[PDImageView alloc]init];
    self.bgView3.image = [Tool stretchImageWithName:@"bg_chuanglianren_bottom"];
    self.bgView3.frame = CGRectMake(0, CGRectGetMaxY(self.bgView2.frame), self.bgView2.size_width, LCFloat(90));
    [self addSubview:self.bgView3];
    
    self.dotImgView = [[PDImageView alloc]init];
    self.dotImgView.backgroundColor = [UIColor clearColor];
    self.dotImgView.image = [UIImage imageNamed:@"icon_bibenyouyue_dot"];
    self.dotImgView.frame = CGRectMake(self.size_width - LCFloat(24) - LCFloat(18), LCFloat(24), LCFloat(18), LCFloat(17));
    [self addSubview:self.dotImgView];
    
    self.desLabel = [GWViewTool createLabelFont:@"15" textColor:@"323232"];
    self.desLabel.numberOfLines = 0;
    [self addSubview:self.desLabel];
}

-(void)setTransferAppointmentModel:(AppointmentModel *)transferAppointmentModel{
    _transferAppointmentModel = transferAppointmentModel;
    
    __weak typeof(self)weakSelf = self;
    [self.avatarImgView uploadHDImageWithURL:transferAppointmentModel.speaker_head_img callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.avatarImgView.image = [Tool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round.png"]];
    }];
    
    // 2. nick
    self.nameLabel.text = transferAppointmentModel.speaker;
    CGSize nickNameSize = [Tool makeSizeWithLabel:self.nameLabel];
    self.nameLabel.frame = CGRectMake(LCFloat(24), CGRectGetMaxY(self.avatarImgView.frame) + LCFloat(16), nickNameSize.width, nickNameSize.height);
    
    // 3. title
    self.titleLabel.text = transferAppointmentModel.speaker_intro;
    CGFloat width = self.size_width - LCFloat(24) - LCFloat(57);
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (titleSize.height >= 2 * [NSString contentofHeightWithFont:self.titleLabel.font]){
        self.titleLabel.frame = CGRectMake(LCFloat(24), CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), width, 2 * [NSString contentofHeightWithFont:self.titleLabel.font]);
    } else {
        self.titleLabel.frame = CGRectMake(LCFloat(24), CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    }
    
    // des
    self.desLabel.text = transferAppointmentModel.speaker_quotation;
    CGFloat main_width = self.size_width - 2 * LCFloat(24);
    CGSize desSize = [self.desLabel.text sizeWithCalcFont:self.desLabel.font constrainedToSize:CGSizeMake(main_width, CGFLOAT_MAX)];
    self.desLabel.frame = CGRectMake(LCFloat(24), CGRectGetMaxY(self.bgView2.frame), main_width, desSize.height);
}

@end






@interface AppointmentFengcaiTableViewCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)UICollectionView *fengcaiCollectionView;
@property (nonatomic,strong)NSMutableArray *fengcaiMutableArr;

@end

@implementation AppointmentFengcaiTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.fengcaiMutableArr = [NSMutableArray array];
        [self createView];
    }
    return self;
}

-(void)setTransferFengcaiArr:(NSArray *)transferFengcaiArr{
    _transferFengcaiArr = transferFengcaiArr;
    [self.fengcaiMutableArr removeAllObjects];
    
    [self.fengcaiMutableArr addObjectsFromArray:transferFengcaiArr];

    [self.fengcaiCollectionView reloadData];
}


#pragma mark - createView
-(void)createView{
    if (!self.fengcaiCollectionView){
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = CGSizeMake(LCFloat(250), [AppointmentFengcaiTableViewCell calculationCellHeight]);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 2;
        
        
        self.fengcaiCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width,[AppointmentFengcaiTableViewCell calculationCellHeight]) collectionViewLayout:layout];
        self.fengcaiCollectionView.backgroundColor = [UIColor clearColor];
        self.fengcaiCollectionView.showsVerticalScrollIndicator = NO;
        [self.fengcaiCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
        self.fengcaiCollectionView.delegate = self;
        self.fengcaiCollectionView.dataSource = self;
        self.fengcaiCollectionView.showsHorizontalScrollIndicator = NO;
        self.fengcaiCollectionView.scrollsToTop = YES;
        [self addSubview:self.fengcaiCollectionView];
        
        [self.fengcaiCollectionView registerClass:[AppointmentFengcaiTableViewCollectionViewCell class] forCellWithReuseIdentifier:@"GWfengcaiCollectionViewCell"];
    }
}

#pragma mark - CollectionDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.fengcaiMutableArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    AppointmentFengcaiTableViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GWfengcaiCollectionViewCell" forIndexPath:indexPath];
    AppointmentModel *model = [self.fengcaiMutableArr objectAtIndex:indexPath.row];
    cell.transferAppointmentModel = model;
    return cell;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return LCFloat(12);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 0;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(0, LCFloat(15), 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(LCFloat(250) , [AppointmentFengcaiTableViewCell calculationCellHeight]);
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    return nil;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    return CGSizeZero;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    
    return CGSizeZero;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"123");
}

+(CGFloat)calculationCellHeight{
    return LCFloat(300);
}

@end
