//
//  AppointmentDongtaiTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "AppointmentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppointmentDongtaiTableViewCellCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)AppointmentModel *transferAppointmentModel;

@end

@interface AppointmentDongtaiTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)NSArray *transferDongtaiArr;

-(void)actionClickWithItemsBlock:(void(^)(AppointmentModel *transferAppointmentModel))block;

@end

NS_ASSUME_NONNULL_END
