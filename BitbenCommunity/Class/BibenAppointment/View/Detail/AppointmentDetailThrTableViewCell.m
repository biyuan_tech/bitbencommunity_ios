//
//  AppointmentDetailThrTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentDetailThrTableViewCell.h"

@interface AppointmentDetailThrTableViewCell()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UILabel *infoLabel;

@end

@implementation AppointmentDetailThrTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.bgImgView.image = [Tool stretchImageWithName:@"icon_appointmentDetailOne_bottom"];
    self.backgroundView = self.bgImgView;
    
    self.infoLabel = [GWViewTool createLabelFont:@"15" textColor:@"323232"];
    [self addSubview:self.infoLabel];
    self.infoLabel.numberOfLines = 0;
    self.infoLabel.font = [self.infoLabel.font boldFont];
    
}

-(void)setTransferAppointmentModel:(AppointmentModel *)transferAppointmentModel{
    _transferAppointmentModel = transferAppointmentModel;
    
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(35);
    self.infoLabel.text = transferAppointmentModel.speaker_quotation.length ? transferAppointmentModel.speaker_quotation :@"这家伙很懒，什么都没有留下";
    CGSize infoSize = [transferAppointmentModel.speaker_quotation sizeWithCalcFont:self.infoLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.infoLabel.frame = CGRectMake(LCFloat(39), 0, width, infoSize.height);

}


+(CGFloat)calculationCellHeightWithModel:(AppointmentModel *)transferAppointmentModel{
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(35);
    CGSize infoSize = [transferAppointmentModel.speaker_quotation sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"15"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    return infoSize.height + 2 * LCFloat(11);
}

@end
