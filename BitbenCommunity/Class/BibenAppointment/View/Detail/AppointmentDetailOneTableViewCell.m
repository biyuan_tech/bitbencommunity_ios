//
//  AppointmentDetailOneTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentDetailOneTableViewCell.h"
#import "CenterShareImgModel.h"

@interface AppointmentDetailOneTableViewCell()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *infoLabel;
@property (nonatomic,strong)PDImageView *dotImgView;
@end

@implementation AppointmentDetailOneTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.bgImgView.image = [Tool stretchImageWithName:@"icon_appointmentDetailOne_header"];
    self.backgroundView = self.bgImgView;
    
    // ava
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"18" textColor:@"323232"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
    
    self.infoLabel = [GWViewTool createLabelFont:@"13" textColor:@"828282"];
    self.infoLabel.numberOfLines = 0;
    [self addSubview:self.infoLabel];
    
    self.dotImgView = [[PDImageView alloc]init];
    self.dotImgView.backgroundColor = [UIColor clearColor];
    self.dotImgView.image = [UIImage imageNamed:@"icon_bibenyouyue_dot"];
    [self addSubview:self.dotImgView];
}

#pragma mark -
-(void)setTransferAppointmentModel:(AppointmentModel *)transferAppointmentModel{
    _transferAppointmentModel = transferAppointmentModel;

    __weak typeof(self)weakSelf = self;

    [self.avatarImgView uploadHDImageWithURL:transferAppointmentModel.speaker_head_img callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (image){
            strongSelf.avatarImgView.image = [Tool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round.png"]];
        } else {
            strongSelf.avatarImgView.image = [Tool imageByComposingImage:[CenterShareImgModel createImgManagerWithPlacehorderWithRect:strongSelf.avatarImgView.frame.size] withMaskImage:[UIImage imageNamed:@"round.png"]];
        }
        
    }];
    self.avatarImgView.frame = CGRectMake(LCFloat(39), LCFloat(24), LCFloat(48), LCFloat(48));
    
    self.titleLabel.text = transferAppointmentModel.speaker;
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(39);
    self.titleLabel.frame = CGRectMake(LCFloat(39), CGRectGetMaxY(self.avatarImgView.frame) + LCFloat(16), width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    self.infoLabel.text = transferAppointmentModel.speaker_intro;
    CGSize infoSize = [self.infoLabel.text sizeWithCalcFont:self.infoLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.infoLabel.frame = CGRectMake(LCFloat(39), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), width, infoSize.height);

    self.dotImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(40) - LCFloat(18), LCFloat(24), LCFloat(18), LCFloat(17));    
}

+(CGFloat)calculationCellHeight:(AppointmentModel *)transferAppointmentModel{
    CGFloat cellHeight = 0 ;
    cellHeight += LCFloat(24);
    cellHeight += LCFloat(48);
    cellHeight += LCFloat(16);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"18"]];
    cellHeight += LCFloat(11);
    NSString *info = transferAppointmentModel.speaker_intro;
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(39);
    CGSize infoSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"13"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += infoSize.height;
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end
