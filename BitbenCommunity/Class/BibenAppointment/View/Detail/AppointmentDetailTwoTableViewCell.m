//
//  AppointmentDetailTwoTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentDetailTwoTableViewCell.h"

@interface AppointmentDetailTwoTableViewCell()
@property (nonatomic,strong)PDImageView *bgImgView;
@end

@implementation AppointmentDetailTwoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    UIImage *infoImg = [UIImage imageNamed:@"icon_appointmentDetailOne_normal"];
    self.bgImgView.image = [infoImg stretchableImageWithLeftCapWidth:infoImg.size.width * 0.73 topCapHeight:infoImg.size.height * 0.5];
    self.backgroundView = self.bgImgView;
}


@end
