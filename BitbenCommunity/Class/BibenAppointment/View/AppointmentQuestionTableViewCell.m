//
//  AppointmentQuestionTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentQuestionTableViewCell.h"

@interface AppointmentQuestionTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *arrowImgView;
@property (nonatomic,strong)UILabel *desLabel;
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UIView *lineView;

@end

@implementation AppointmentQuestionTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.bgImgView.image = [Tool stretchImageWithName:@"bg_bibenyouyue_question"];
    self.bgImgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(50));
    [self addSubview:self.bgImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"323232"];
    self.titleLabel.numberOfLines = 0;
    [self addSubview:self.titleLabel];
    
    self.arrowImgView = [[PDImageView alloc]init];
    self.arrowImgView.image = [UIImage imageNamed:@"icon_center_bbt_detail_downarrow"];
    self.arrowImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(35) - LCFloat(13), (LCFloat(50) - LCFloat(7)) / 2.,  LCFloat(13), LCFloat(7));
    [self addSubview:self.arrowImgView];
    
    self.desLabel = [GWViewTool createLabelFont:@"15" textColor:@"828181"];
    self.desLabel.numberOfLines = 0;
    [self addSubview:self.desLabel];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor hexChangeFloat:@"E7E7EA"];
    [self addSubview:self.lineView];
}

-(void)setTransferModel:(AppointmentMainQAModel *)transferModel{
    _transferModel = transferModel;
    
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(36) - LCFloat(13) - LCFloat(11);
    
    self.titleLabel.text = transferModel.question;
    CGSize titleSize = [transferModel.question sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(LCFloat(36), LCFloat(18), width, titleSize.height);
    
    self.lineView.frame = CGRectMake(LCFloat(36), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(18), kScreenBounds.size.width - 2 * LCFloat(36), 1.f);
    
    self.desLabel.text = transferModel.answer;
    CGSize desSize = [self.desLabel.text sizeWithCalcFont:self.desLabel.font constrainedToSize:CGSizeMake(self.lineView.size_width, CGFLOAT_MAX)];
    self.desLabel.frame = CGRectMake(self.lineView.orgin_x, CGRectGetMaxY(self.lineView.frame) + LCFloat(15), self.lineView.size_width, desSize.height);
    
    if (transferModel.hasShow){
        self.lineView.hidden = NO;
        self.desLabel.hidden = NO;
        self.bgImgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), [AppointmentQuestionTableViewCell calculationCellHeightModel:transferModel] - LCFloat(10));
    } else {
        self.lineView.hidden = YES;
        self.desLabel.hidden = YES;
        self.bgImgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), [AppointmentQuestionTableViewCell calculationCellHeightModel:transferModel] - LCFloat(10));
    }
}

+(CGFloat)calculationCellHeightModel:(AppointmentMainQAModel *)model{
    CGFloat width = kScreenBounds.size.width - LCFloat(36) * 2 - LCFloat(13) - LCFloat(11);
    CGSize titleSize = [model.question sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"15"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    CGSize descSize = [model.answer sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"15"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(18);
    cellHeight += titleSize.height;
    if (model.hasShow){
        cellHeight += LCFloat(33);
        cellHeight += descSize.height;
        cellHeight += LCFloat(20);
    } else {
        cellHeight += LCFloat(18);
    }
    cellHeight += LCFloat(10);
    return cellHeight;
}

@end
