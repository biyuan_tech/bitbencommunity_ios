//
//  AppointmentItemsTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentItemsTableViewCell.h"

static char actionClickWithItemsBlockKey;
@interface AppointmentItemsTableViewCell()

@end

@implementation AppointmentItemsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


-(void)createView{
    CGFloat margin = LCFloat(3);
    CGFloat single_width = (kScreenBounds.size.width - 3 * margin - 2 * LCFloat(20)) / 4.;
    for (int i = 0 ; i < 4; i++){
        CGFloat origin_x = LCFloat(20) + (single_width + margin) * i;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(origin_x, 0, single_width, [AppointmentItemsTableViewCell calculationCellHeight]);
        [button setTitleColor:[UIColor hexChangeFloat:@"323232"] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
        [self addSubview:button];
        
        [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"icon_bibenyouyue_items_%li",(long)i + 1]] forState:UIControlStateNormal];
        if (i == 0){
            [button setTitle:@"最新动态" forState:UIControlStateNormal];
        } else if (i == 1){
            [button setTitle:@"创链人招募" forState:UIControlStateNormal];
        } else if (i == 2){
            [button setTitle:@"创链人风采" forState:UIControlStateNormal];
        } else if (i == 3){
            [button setTitle:@"常见问题" forState:UIControlStateNormal];
        }
        
        [button layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleTop imageTitleSpace:LCFloat(16)];
        __weak typeof(self)weakSelf = self;
        [button buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(NSInteger index) = objc_getAssociatedObject(strongSelf, &actionClickWithItemsBlockKey);
            if (block){
                block(i);
            }
        }];
    }
}

-(void)actionClickWithItemsBlock:(void(^)(NSInteger index))block{
    
    objc_setAssociatedObject(self, &actionClickWithItemsBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


+(CGFloat)calculationCellHeight{
    return LCFloat(117);
}
@end
