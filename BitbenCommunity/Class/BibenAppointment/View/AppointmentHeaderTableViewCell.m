//
//  AppointmentHeaderTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentHeaderTableViewCell.h"

static char actionClickWithMoreBlockKey;
@interface AppointmentHeaderTableViewCell()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *desLabel;
@property (nonatomic,strong)UILabel *tagLabel;
@property (nonatomic,strong)UILabel *subTagLabel;
@property (nonatomic,strong)UIButton *detailButton;
@end

@implementation AppointmentHeaderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.bgImgView.image = [UIImage imageNamed:@"bg_bibenyouyue_header_bg"];
    self.bgImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [AppointmentHeaderTableViewCell calculationCellHeight]);
    [self addSubview:self.bgImgView];
    
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.tagLabel = [GWViewTool createLabelFont:@"30" textColor:@"FFFFFF"];
    self.tagLabel.font = [self.tagLabel.font boldFont];
    self.tagLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.tagLabel];
    
    self.subTagLabel = [GWViewTool createLabelFont:@"16" textColor:@"FFFFFF"];
    self.subTagLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.subTagLabel];
    
    self.desLabel = [GWViewTool createLabelFont:@"15" textColor:@"FFFFFF"];
    self.desLabel.numberOfLines = 0;
    [self addSubview:self.desLabel];
    
    self.detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.detailButton.backgroundColor = [UIColor clearColor];
    [self.detailButton setTitle:@"了解详情" forState:UIControlStateNormal];
    [self.detailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.detailButton.layer.cornerRadius = LCFloat(5);
    self.detailButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.detailButton.layer.borderWidth = 1.f;
    self.detailButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"16"];
    __weak typeof(self)weakSelf = self;
    [self.detailButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithMoreBlockKey);
        if (block){
            block();
        }
    }];
    
    [self addSubview:self.detailButton];
}

-(void)setTransferMainModel:(AppointmentMainModel *)transferMainModel{
    _transferMainModel = transferMainModel;
    
    self.avatarImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(60)) / 2., LCFloat(77), LCFloat(60), LCFloat(60));
    __weak typeof(self)weakSelf = self;
    [self.avatarImgView uploadHDImageWithURL:transferMainModel.head_img callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.avatarImgView.image = [Tool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round.png"]];
    }];
    
    self.tagLabel.text = transferMainModel.title;
    self.tagLabel.frame = CGRectMake(0, CGRectGetMaxY(self.avatarImgView.frame) + LCFloat(32), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.tagLabel.font]);
    
    self.subTagLabel.text = transferMainModel.subtitle;
    self.subTagLabel.frame = CGRectMake(0, CGRectGetMaxY(self.tagLabel.frame) + LCFloat(20), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.subTagLabel.font]);
    
    self.desLabel.text = transferMainModel.intro;
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(15);
    CGSize desSize = [self.desLabel.text sizeWithCalcFont:self.desLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.desLabel.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.subTagLabel.frame) + LCFloat(20), width, desSize.height);
    
    self.detailButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(120)) / 2., CGRectGetMaxY(self.desLabel.frame) + LCFloat(29), LCFloat(120), LCFloat(40));
}


+(CGFloat)calculationCellHeight{
    return LCFloat(445);
}


-(void)actionClickWithMoreBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithMoreBlockKey, block, OBJC_ASSOCIATION_COPY);
}
@end
