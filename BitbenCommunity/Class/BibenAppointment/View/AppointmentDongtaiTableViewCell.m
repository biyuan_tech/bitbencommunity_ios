//
//  AppointmentDongtaiTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentDongtaiTableViewCell.h"
#import "AppointmentModel.h"



static char actionClickWithItemsBlockKey;
@interface AppointmentDongtaiTableViewCellCollectionViewCell()
@property (nonatomic,strong)PDImageView *imgView;
@property (nonatomic,strong)UIView *titleBgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *convertImgView;


@end


@implementation AppointmentDongtaiTableViewCellCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.imgView = [[PDImageView alloc]init];
    self.imgView.backgroundColor = [UIColor clearColor];
    self.imgView.frame = CGRectMake(0, 0, self.size_width, LCFloat(123));
    [self addSubview:self.imgView];
    
    self.titleBgView = [[UIView alloc]init];
    self.titleBgView.backgroundColor = [UIColor whiteColor];
    self.titleBgView.frame = CGRectMake(0, CGRectGetMaxY(self.imgView.frame), self.imgView.size_width, self.size_height - self.imgView.size_height);
    [self addSubview:self.titleBgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"323232"];
    self.titleLabel.numberOfLines = 0;
    [self.titleBgView addSubview:self.titleLabel];
    
    self.convertImgView = [[PDImageView alloc]init];
    self.convertImgView.image = [Tool stretchImageWithName:@"icon_message_live_img_convert"];
    self.convertImgView.frame = self.bounds;
    [self addSubview:self.convertImgView];
}

-(void)setTransferAppointmentModel:(AppointmentModel *)transferAppointmentModel{
    _transferAppointmentModel = transferAppointmentModel;
    [self.imgView uploadHDImageWithURL:transferAppointmentModel.live_cover_url callback:NULL];
    
    // title
    self.titleLabel.text = transferAppointmentModel.live_title;
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(self.size_width - 2 * LCFloat(13), CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(LCFloat(13), LCFloat(12), self.size_width - 2 * LCFloat(13), MIN(titleSize.height, 2 * [NSString contentofHeightWithFont:self.titleLabel.font]));
    
    
}

@end







@interface AppointmentDongtaiTableViewCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)UICollectionView *dongtaiCollectionView;
@property (nonatomic,strong)NSMutableArray *dongtaiMutableArr;
@end

@implementation AppointmentDongtaiTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.dongtaiMutableArr = [NSMutableArray array];
        [self createView];
    }
    return self;
}

-(void)setTransferDongtaiArr:(NSArray *)transferDongtaiArr{
    _transferDongtaiArr = transferDongtaiArr;
    [self.dongtaiMutableArr removeAllObjects];
    
    [self.dongtaiMutableArr addObjectsFromArray:transferDongtaiArr];

    [self.dongtaiCollectionView reloadData];

}


#pragma mark - createView
-(void)createView{
    if (!self.dongtaiCollectionView){
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = CGSizeMake(LCFloat(250), LCFloat(180));
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 2;
        
        
        self.dongtaiCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(180)) collectionViewLayout:layout];
        self.dongtaiCollectionView.backgroundColor = [UIColor clearColor];
        self.dongtaiCollectionView.showsVerticalScrollIndicator = NO;
        [self.dongtaiCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
        self.dongtaiCollectionView.delegate = self;
        self.dongtaiCollectionView.dataSource = self;
        self.dongtaiCollectionView.showsHorizontalScrollIndicator = NO;
        self.dongtaiCollectionView.scrollsToTop = YES;
        [self addSubview:self.dongtaiCollectionView];
        
        [self.dongtaiCollectionView registerClass:[AppointmentDongtaiTableViewCellCollectionViewCell class] forCellWithReuseIdentifier:@"GWdongtaiCollectionViewCell"];
    }
}

#pragma mark - CollectionDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dongtaiMutableArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    AppointmentDongtaiTableViewCellCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GWdongtaiCollectionViewCell" forIndexPath:indexPath];
    cell.transferAppointmentModel = [self.dongtaiMutableArr objectAtIndex:indexPath.row];
    return cell;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 0;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(0, LCFloat(15), 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(LCFloat(250) , LCFloat(180));
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    return nil;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    return CGSizeZero;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    
    return CGSizeZero;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    AppointmentModel *appointmentModel = [self.dongtaiMutableArr objectAtIndex:indexPath.row];
    void(^block)(AppointmentModel *transferAppointmentModel) = objc_getAssociatedObject(self, &actionClickWithItemsBlockKey);
    if (block){
        block(appointmentModel);
    }
}

-(void)actionClickWithItemsBlock:(void(^)(AppointmentModel *transferAppointmentModel))block{
    objc_setAssociatedObject(self, &actionClickWithItemsBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}

+(CGFloat)calculationCellHeight{
    return LCFloat(180);
}
@end
