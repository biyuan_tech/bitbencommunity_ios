//
//  AppointmentZhaomuTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentZhaomuTableViewCell.h"

@interface AppointmentZhaomuTableViewCell()
@property (nonatomic,strong)PDImageView *imgView;
@end

@implementation AppointmentZhaomuTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.imgView = [[PDImageView alloc]init];
    self.imgView.backgroundColor = [UIColor clearColor];
    self.imgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(115));
    self.imgView.image = [UIImage imageNamed:@"bg_bibenyouyue_chuanglianren"];
    [self addSubview:self.imgView];
}

+(CGFloat)calculationCellHeight{
    return LCFloat(115);
}

@end
