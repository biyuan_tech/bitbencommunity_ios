//
//  AppointmentWechatTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/21.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "AppointmentMainModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppointmentWechatTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)AppointmentMainModel *transferAppointmentModel;

@end

NS_ASSUME_NONNULL_END
