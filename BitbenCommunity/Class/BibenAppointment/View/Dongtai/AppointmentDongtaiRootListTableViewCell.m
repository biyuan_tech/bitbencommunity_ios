//
//  AppointmentDongtaiTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentDongtaiRootListTableViewCell.h"

@interface AppointmentDongtaiRootListTableViewCell()
@property (nonatomic,strong)PDImageView *imgView;
@property (nonatomic,strong)UIView *titleBgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *convertImgView;
@property (nonatomic,strong)PDImageView *playerImgView;


@end

@implementation AppointmentDongtaiRootListTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.imgView = [[PDImageView alloc]init];
    self.imgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.imgView];
    
    self.titleBgView = [[UIView alloc]init];
    self.titleBgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.titleBgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"323232"];
    self.titleLabel.numberOfLines = 0;
    [self.titleBgView addSubview:self.titleLabel];
    
    self.convertImgView = [[PDImageView alloc]init];
    [self addSubview:self.convertImgView];
    
    self.playerImgView = [[PDImageView alloc]init];
    self.playerImgView.image = [UIImage imageNamed:@"icon_bibenyouyue_player"];
    [self addSubview:self.playerImgView];

}

-(void)setTransferModel:(AppointmentModel *)transferModel{
    _transferModel = transferModel;

    self.imgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(160));
    [self.imgView uploadHDImageWithURL:transferModel.live_cover_url callback:NULL];
    
    self.titleBgView.frame = CGRectMake(self.imgView.orgin_x, CGRectGetMaxY(self.imgView.frame), self.imgView.size_width, LCFloat(73));
    
    self.titleLabel.text = transferModel.speaker_intro;
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(30);
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(LCFloat(15), LCFloat(15), width, MIN(titleSize.height, 2 * [NSString contentofHeightWithFont:self.titleLabel.font]));
    
    self.convertImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [AppointmentDongtaiRootListTableViewCell calculationCellHeight] + 5);
    self.convertImgView.image = [Tool stretchImageWithName:@"bg_message_detailList_convert"];
    
    self.playerImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(50)) / 2., (self.imgView.size_height - LCFloat(50)) / 2., LCFloat(50), LCFloat(50));
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(233);
    return cellHeight;
}

@end
