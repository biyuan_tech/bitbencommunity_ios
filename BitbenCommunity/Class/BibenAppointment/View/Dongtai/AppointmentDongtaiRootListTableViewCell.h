//
//  AppointmentDongtaiTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "AppointmentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppointmentDongtaiRootListTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)AppointmentModel *transferModel;

@end

NS_ASSUME_NONNULL_END
