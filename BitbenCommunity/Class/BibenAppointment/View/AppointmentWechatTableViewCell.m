//
//  AppointmentWechatTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/21.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentWechatTableViewCell.h"

@interface AppointmentWechatTableViewCell()
@property(nonatomic,strong)PDImageView *bgImgView;

@end

@implementation AppointmentWechatTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.bgImgView.image = [Tool stretchImageWithName:@"bg_bibenyouyue_question"];
    self.bgImgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), [AppointmentWechatTableViewCell calculationCellHeight]);
    self.bgImgView.userInteractionEnabled = YES;
    [self addSubview:self.bgImgView];
    
  
}

-(void)setTransferAppointmentModel:(AppointmentMainModel *)transferAppointmentModel{
    _transferAppointmentModel = transferAppointmentModel;
    
    if (!self.bgImgView.subviews.count){
        for (int i = 0 ; i < 2 ; i++){
            CGFloat origin_y = LCFloat(25) + (LCFloat(17) + LCFloat(30)) * i;
            
            PDImageView *iconImgView = [[PDImageView alloc]init];
            iconImgView.backgroundColor = [UIColor clearColor];
            iconImgView.frame = CGRectMake(LCFloat(23), origin_y, LCFloat(17), LCFloat(17));
            [self.bgImgView addSubview:iconImgView];
            
            UILabel *titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"323232"];
            titleLabel.frame = CGRectMake(CGRectGetMaxX(iconImgView.frame) + LCFloat(21), 0, LCFloat(150), [NSString contentofHeightWithFont:titleLabel.font]);
            titleLabel.center_y = iconImgView.center_y;
            [self.bgImgView addSubview:titleLabel];
            
            UIButton *copyButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [copyButton setTitle:@"复制" forState:UIControlStateNormal];
            copyButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"12"];
            copyButton.layer.cornerRadius = LCFloat(3);
            copyButton.layer.borderColor = [UIColor hexChangeFloat:@"EA6438"].CGColor;
            copyButton.layer.borderWidth = 1.f;
            [copyButton setTitleColor:[UIColor hexChangeFloat:@"EA6438"] forState:UIControlStateNormal];
            copyButton.frame = CGRectMake(self.bgImgView.size_width - LCFloat(20) - LCFloat(32), 0, LCFloat(32), LCFloat(18));
            copyButton.center_y = iconImgView.center_y;
            [self.bgImgView addSubview:copyButton];
            
            if (i == 0){
                iconImgView.image = [UIImage imageNamed:@"icon_bibenyouyue_phone"];
                titleLabel.text = transferAppointmentModel.phone;
            } else if (i == 1){
                iconImgView.image = [UIImage imageNamed:@"icon_bibenyouyue_wechat"];
                titleLabel.text = transferAppointmentModel.wechat;
            }
            
            __weak typeof(self)weakSelf = self;
            [copyButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                [Tool copyWithString:titleLabel.text callback:NULL];
                [StatusBarManager statusBarHidenWithText:@"复制成功"];
            }];
        }
    }
    
}

+(CGFloat)calculationCellHeight{
    return LCFloat(112);
}
@end
