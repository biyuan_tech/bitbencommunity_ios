//
//  AppointmentItemsTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppointmentItemsTableViewCell : PDBaseTableViewCell

-(void)actionClickWithItemsBlock:(void(^)(NSInteger index))block;

@end

NS_ASSUME_NONNULL_END
