//
//  AppointmentHeaderTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "AppointmentMainModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppointmentHeaderTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)AppointmentMainModel *transferMainModel;

-(void)actionClickWithMoreBlock:(void(^)())block;

@end

NS_ASSUME_NONNULL_END
