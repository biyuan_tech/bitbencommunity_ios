//
//  AppointmentViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentViewController.h"
#import "AppointmentHeaderTableViewCell.h"
#import "AppointmentItemsTableViewCell.h"
#import "AppointmentChuanglianrenListViewController.h"
#import "AppointmentDongtaiRootViewController.h"
#import "AppointmentDongtaiTableViewCell.h"
#import "AppointmentModel.h"
#import "AppointmentZhaomuTableViewCell.h"
#import "AppointmentFengcaiTableViewCell.h"
#import "AppointmentMainModel.h"
#import "AppointmentQuestionTableViewCell.h"
#import "AppointmentDetailViewController.h"
#import "AppointmentWechatTableViewCell.h"
#import <MJRefresh.h>

@interface AppointmentViewController ()<UITableViewDelegate,UITableViewDataSource>{
    AppointmentMainModel *rootModel;
}
@property (nonatomic,strong)UITableView *appointmentTableView;
@property (nonatomic,strong)NSMutableArray *appointmentMutableArr;
@property (nonatomic,strong)UIView *navView;
@property (nonatomic,strong)UIButton *navBackButton;
@property (nonatomic,strong)UILabel *navTitleLabel;
@property (nonatomic,strong)NSMutableArray *infoMutableArr;
@property (nonatomic,strong)NSMutableArray *questionMutableArr;

@end

@implementation AppointmentViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableViw];
    [self createNavView];
    __weak typeof(self)weakSelf = self;
    [self sendRequestToGetBibenyouyueBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoManager];
    }];
}

- (void)autoReload{
    [self.appointmentTableView.mj_header beginRefreshing];
    [self reloadData];
}

- (void)reloadData{
    __weak typeof(self)weakSelf = self;
    [self sendRequestToGetBibenyouyueBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoManager];
    }];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"币本有约";
    self.view.backgroundColor = RGB(242, 244, 245, 1);
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.questionMutableArr = [NSMutableArray array];
    self.infoMutableArr = [NSMutableArray array];
    self.appointmentMutableArr = [NSMutableArray array];
    [self.appointmentMutableArr addObject:@[@"头"]];
    [self.appointmentMutableArr addObject:@[@"标签"]];
    [self.appointmentMutableArr addObject:@[@"最新动态标签",@"最新动态"]];
    [self.appointmentMutableArr addObject:@[@"创链人招募",@"创链人详情"]];
    [self.appointmentMutableArr addObject:@[@"创链人风采",@"创链人风采详情"]];
    [self.questionMutableArr addObject:@"常见问题"];
    [self.appointmentMutableArr addObject:self.questionMutableArr];
 
   [self.appointmentMutableArr addObject:@[@"微信"]];
    [self.appointmentMutableArr addObject:@[@"按钮"]];
}

#pragma mark - createView
-(void)createTableViw{
    if (!self.appointmentTableView){
        self.appointmentTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.appointmentTableView.dataSource = self;
        self.appointmentTableView.delegate = self;
        [self.view addSubview:self.appointmentTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.appointmentMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.appointmentMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"头" sourceArr:self.appointmentMutableArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        AppointmentHeaderTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if(!cellWithRowOne){
            cellWithRowOne = [[AppointmentHeaderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferMainModel = rootModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickWithMoreBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            AppointmentDetailViewController *detailVC = [[AppointmentDetailViewController alloc]init];
            detailVC.transferModel = rootModel;
            [strongSelf.navigationController pushViewController:detailVC animated:YES];
        }];
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标签" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标签" sourceArr:self.appointmentMutableArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        AppointmentItemsTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if(!cellWithRowOne){
            cellWithRowOne = [[AppointmentItemsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickWithItemsBlock:^(NSInteger index) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (index == 0){
                NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"最新动态标签" sourceArr:strongSelf.appointmentMutableArr];
                NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"最新动态标签" sourceArr:strongSelf.appointmentMutableArr];
                NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
                [tableView scrollToRowAtIndexPath:newIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            } else if (index == 1){
                NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"创链人招募" sourceArr:strongSelf.appointmentMutableArr];
                NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"创链人招募" sourceArr:strongSelf.appointmentMutableArr];
                NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
                [tableView scrollToRowAtIndexPath:newIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            } else if (index == 2){
                NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"创链人风采" sourceArr:strongSelf.appointmentMutableArr];
                NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"创链人风采" sourceArr:strongSelf.appointmentMutableArr];
                NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
                [tableView scrollToRowAtIndexPath:newIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            } else if (index == 3){
                NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"常见问题" sourceArr:strongSelf.appointmentMutableArr];
                NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"常见问题" sourceArr:strongSelf.appointmentMutableArr];
                NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
                [tableView scrollToRowAtIndexPath:newIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            }
        }];
        
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最新动态标签" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"最新动态标签" sourceArr:self.appointmentMutableArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if(!cellWithRowOne){
            cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowOne.backgroundColor = RGB(242, 244, 245, 1);
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferTitle = @"最新动态";
        cellWithRowOne.transferHasArrow = YES;
        cellWithRowOne.transferDesc = @"查看全部";
        cellWithRowOne.titleLabel.font = [cellWithRowOne.titleLabel.font boldFont];
        cellWithRowOne.titleLabel.orgin_x = LCFloat(16);
        
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最新动态" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"最新动态" sourceArr:self.appointmentMutableArr]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        AppointmentDongtaiTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if(!cellWithRowOne){
            cellWithRowOne = [[AppointmentDongtaiTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
        }
        cellWithRowOne.backgroundColor = RGB(242, 244, 245, 1);
        cellWithRowOne.transferDongtaiArr = self.infoMutableArr;
//        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickWithItemsBlock:^(AppointmentModel * _Nonnull transferAppointmentModel) {
            [DirectManager directMaxWithModel:transferAppointmentModel block:NULL];
        }];
        
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"创链人招募" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"创链人招募" sourceArr:self.appointmentMutableArr]){
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        GWNormalTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if(!cellWithRowFiv){
            cellWithRowFiv = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
        }
        cellWithRowFiv.backgroundColor = RGB(242, 244, 245, 1);
        cellWithRowFiv.transferCellHeight = cellHeight;
        cellWithRowFiv.transferTitle = @"创链人招募";
        cellWithRowFiv.titleLabel.font = [cellWithRowFiv.titleLabel.font boldFont];
        cellWithRowFiv.titleLabel.orgin_x = LCFloat(16);
        return cellWithRowFiv;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"创链人详情" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"创链人详情" sourceArr:self.appointmentMutableArr]){
        static NSString *cellIdentifyWithRowSix = @"cellIdentifyWithRowSix";
        AppointmentZhaomuTableViewCell *cellWithRowSix = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSix];
        if(!cellWithRowSix){
            cellWithRowSix = [[AppointmentZhaomuTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSix];
            cellWithRowSix.backgroundColor = RGB(242, 244, 245, 1);
        }
        return cellWithRowSix;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"创链人风采" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"创链人风采" sourceArr:self.appointmentMutableArr]){
        static NSString *cellIdentifyWithRowSev = @"cellIdentifyWithRowSev";
        GWNormalTableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev];
        if(!cellWithRowSev){
            cellWithRowSev = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev];
            cellWithRowSev.backgroundColor =  RGB(242, 244, 245, 1);
        }
        cellWithRowSev.transferCellHeight = cellHeight;
        cellWithRowSev.transferTitle = @"创链人风采";
        cellWithRowSev.transferDesc = @"查看全部";
        cellWithRowSev.transferHasArrow = YES;
        cellWithRowSev.titleLabel.font = [cellWithRowSev.titleLabel.font boldFont];
        cellWithRowSev.titleLabel.orgin_x = LCFloat(16);
        
        return cellWithRowSev;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"创链人风采详情" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"创链人风采详情" sourceArr:self.appointmentMutableArr]){
        static NSString *cellIdentifyWithRowEig = @"cellIdentifyWithRowEig";
        AppointmentFengcaiTableViewCell *cellWithRowEig = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowEig];
        if(!cellWithRowEig){
            cellWithRowEig = [[AppointmentFengcaiTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowEig];
            cellWithRowEig.backgroundColor = RGB(242, 244, 245, 1);
        }
        cellWithRowEig.transferCellHeight = cellHeight;
        cellWithRowEig.transferFengcaiArr = self.infoMutableArr;
        return cellWithRowEig;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"常见问题" sourceArr:self.appointmentMutableArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"常见问题" sourceArr:self.appointmentMutableArr]){
            static NSString *cellIdentifyWithRowNig = @"cellIdentifyWithRowNig";
            GWNormalTableViewCell *cellWithRowNig = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowNig];
            if(!cellWithRowNig){
                cellWithRowNig = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowNig];
                cellWithRowNig.backgroundColor = RGB(242, 244, 245, 1);

            }
            cellWithRowNig.transferCellHeight = cellHeight;
            cellWithRowNig.transferTitle = @"常见问题";
            
            cellWithRowNig.titleLabel.font = [cellWithRowNig.titleLabel.font boldFont];
            cellWithRowNig.titleLabel.orgin_x = LCFloat(16);
            return cellWithRowNig;
        } else {
            static NSString *cellIdentifyWithRowTen = @"cellIdentifyWithRowTen";
            AppointmentQuestionTableViewCell *cellWithRowTen = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTen];
            if(!cellWithRowTen){
                cellWithRowTen = [[AppointmentQuestionTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTen];
            }
            AppointmentMainQAModel *qaModel = [self.questionMutableArr objectAtIndex:indexPath.row];
            cellWithRowTen.transferModel = qaModel;
            return cellWithRowTen;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"按钮" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"按钮" sourceArr:self.appointmentMutableArr]){
        static NSString *cellIdentifyWithRowEle = @"cellIdentifyWithRowEle";
        UITableViewCell *cellWithRowEle = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowEle];
        if(!cellWithRowEle){
            cellWithRowEle = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowEle];
            cellWithRowEle.backgroundColor = [UIColor clearColor];
            
            UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
            actionButton.stringTag = @"actionButton";
            actionButton.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(44));
            actionButton.backgroundColor = [UIColor hexChangeFloat:@"EA6438"];
            [actionButton setTitle:@"立即报名" forState:UIControlStateNormal];
            [actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            actionButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
            actionButton.layer.cornerRadius = LCFloat(22);
            [cellWithRowEle addSubview:actionButton];
        }
        UIButton *actionButton = (UIButton *)[cellWithRowEle viewWithStringTag:@"actionButton"];
        
        __weak typeof(self)weakSelf = self;
        [actionButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDWebViewController *webViewController = [[PDWebViewController alloc]init];
            [webViewController webDirectedWebUrl:@"http://bibenshequ.mikecrm.com/x2r4FRz"];
            [strongSelf.navigationController pushViewController:webViewController animated:YES];
        }];

        return cellWithRowEle;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"微信" sourceArr:self.appointmentMutableArr]){
        static NSString *cellIdentifyWithRowTww = @"cellIdentifyWithRowTww";
        AppointmentWechatTableViewCell *cellWithRowTww = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTww];
        if(!cellWithRowTww){
            cellWithRowTww = [[AppointmentWechatTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTww];
        }
        cellWithRowTww.transferCellHeight = cellHeight;
        cellWithRowTww.transferAppointmentModel = rootModel;
        return cellWithRowTww;
    }
    

    
    else {
        static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
        GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
        if(!cellWithRowOne){
            cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
        }
        cellWithRowOne.transferTitle = @"123";
        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"头" sourceArr:self.appointmentMutableArr]){
        return [AppointmentHeaderTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标签" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标签" sourceArr:self.appointmentMutableArr]){
        return [AppointmentItemsTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最新动态标签" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"最新动态标签" sourceArr:self.appointmentMutableArr]){
        return [GWNormalTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最新动态" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"最新动态" sourceArr:self.appointmentMutableArr]){
        return [AppointmentDongtaiTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"创链人招募" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"创链人招募" sourceArr:self.appointmentMutableArr]){
        return [GWNormalTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"创链人详情" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"创链人详情" sourceArr:self.appointmentMutableArr]){
        return [AppointmentZhaomuTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"创链人风采" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"创链人风采" sourceArr:self.appointmentMutableArr]){
        return [GWNormalTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"创链人风采详情" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"创链人风采详情" sourceArr:self.appointmentMutableArr]){
        return [AppointmentFengcaiTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"常见问题" sourceArr:self.appointmentMutableArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"常见问题" sourceArr:self.appointmentMutableArr]){
            return [GWNormalTableViewCell calculationCellHeight];
        } else {
            AppointmentMainQAModel *qaModel = [self.questionMutableArr objectAtIndex:indexPath.row];
            return [AppointmentQuestionTableViewCell calculationCellHeightModel:qaModel];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"按钮" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"按钮" sourceArr:self.appointmentMutableArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"微信" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"微信" sourceArr:self.appointmentMutableArr]){
        return [AppointmentWechatTableViewCell calculationCellHeight];
    } else {
        return [GWNormalTableViewCell calculationCellHeight];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最新动态标签" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"最新动态标签" sourceArr:self.appointmentMutableArr]){
        AppointmentDongtaiRootViewController *appointmentDongtaiVC = [[AppointmentDongtaiRootViewController alloc]init];
        [self.navigationController pushViewController:appointmentDongtaiVC animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"常见问题" sourceArr:self.appointmentMutableArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"常见问题" sourceArr:self.appointmentMutableArr]){
        
        } else {
            AppointmentMainQAModel *qaModel = [self.questionMutableArr objectAtIndex:indexPath.row];
            qaModel.hasShow = !qaModel.hasShow;
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"创链人风采" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"创链人风采" sourceArr:self.appointmentMutableArr]){
        AppointmentChuanglianrenListViewController *appointmentChuanglianrenListVC = [[AppointmentChuanglianrenListViewController alloc]init];
        [self.navigationController pushViewController:appointmentChuanglianrenListVC animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"创链人招募" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"创链人招募" sourceArr:self.appointmentMutableArr]){
        PDWebViewController *webViewController = [[PDWebViewController alloc]init];
        [webViewController webDirectedWebUrl:@"http://bibenshequ.mikecrm.com/x2r4FRz"];
        [self.navigationController pushViewController:webViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"创链人详情" sourceArr:self.appointmentMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"创链人详情" sourceArr:self.appointmentMutableArr]){
        PDWebViewController *webViewController = [[PDWebViewController alloc]init];
        [webViewController webDirectedWebUrl:@"http://m.bitben.com/others/bbnAppointmentRecruit"];
        [self.navigationController pushViewController:webViewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"最新动态标签" sourceArr:self.appointmentMutableArr] ){
        return LCFloat(8);
    } else if (section == [self cellIndexPathSectionWithcellData:@"微信" sourceArr:self.appointmentMutableArr]){
        return LCFloat(12);
    } else if (section == [self cellIndexPathSectionWithcellData:@"按钮" sourceArr:self.appointmentMutableArr]){
        return LCFloat(31);
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    if (section == [self cellIndexPathSectionWithcellData:@"最新动态标签" sourceArr:self.appointmentMutableArr] ){
        headerView.backgroundColor = [UIColor hexChangeFloat:@"E6E6E6"];
    } else if (section == [self cellIndexPathSectionWithcellData:@"微信" sourceArr:self.appointmentMutableArr]){
        headerView.backgroundColor = [UIColor clearColor];
    } else {
        headerView.backgroundColor = [UIColor clearColor];
    }
    
    return headerView;
}

#pragma mark - createNavView
-(void)createNavView{
    self.navView = [[UIView alloc]init];
    self.navView.backgroundColor = [UIColor whiteColor];
    self.navView.alpha = 0;
    self.navView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [BYTabbarViewController sharedController].navBarHeight);
    [self.view addSubview:self.navView];
    
    self.navTitleLabel = [GWViewTool createLabelFont:@"16" textColor:@"FFFFFF"];
    self.navTitleLabel.frame = CGRectMake(0, [BYTabbarViewController sharedController].statusHeight, kScreenBounds.size.width, 40);
    self.navTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.navTitleLabel.text = @"币本有约";
    [self.view addSubview:self.navTitleLabel];
    
    self.navBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.navBackButton.frame = CGRectMake(LCFloat(11), [BYTabbarViewController sharedController].statusHeight, 44, 44);
    [self.view addSubview:self.navBackButton];
    __weak typeof(self)weakSelf = self;
    [self.navBackButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.navBackButton setImage:[UIImage imageNamed:@"icon_center_bbt_back"] forState:UIControlStateNormal];
}




#pragma mark - Interface
-(void)sendRequestToGetInfoManager{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page_number":@"0",@"page_size":@"10"};
    [[NetworkAdapter sharedAdapter]fetchWithPath:page_appointment requestParams:params responseObjectClass:[AppointmentListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            AppointmentListModel *listModel = (AppointmentListModel *)responseObject;
            [strongSelf.infoMutableArr removeAllObjects];
            [strongSelf.infoMutableArr addObjectsFromArray:listModel.content];
            [strongSelf.appointmentTableView reloadData];
        }
    }];
}


-(void)sendRequestToGetBibenyouyueBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"show_type":@"1"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_appointment requestParams:params responseObjectClass:[AppointmentMainModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            AppointmentMainModel *model = (AppointmentMainModel *)responseObject;
            strongSelf->rootModel = model;
            [strongSelf.questionMutableArr removeAllObjects];
            [strongSelf.questionMutableArr addObject:@"常见问题"];
            [strongSelf.questionMutableArr addObjectsFromArray:model.qa_list];
            if (block){
                block();
            }
        }
    }];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat alpha = scrollView.contentOffset.y / [BYTabbarViewController sharedController].navBarHeight;
    self.navView.alpha = alpha;
    if(alpha > 0.6){
        [self.navBackButton setImage:[UIImage imageNamed:@"icon_main_back"] forState:UIControlStateNormal];
        self.navTitleLabel.textColor = [UIColor blackColor];
        self.navTitleLabel.alpha = alpha;
        self.navBackButton.alpha = alpha;
    } else {
        [self.navBackButton setImage:[UIImage imageNamed:@"icon_center_bbt_back"] forState:UIControlStateNormal];
        self.navTitleLabel.textColor = [UIColor whiteColor];
        
        self.navTitleLabel.alpha = 1 - alpha;
        self.navBackButton.alpha = 1 - alpha;
    }
}


@end
