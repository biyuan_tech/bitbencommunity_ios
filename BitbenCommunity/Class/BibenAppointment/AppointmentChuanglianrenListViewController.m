//
//  AppointmentChuanglianrenListViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentChuanglianrenListViewController.h"
#import "AppointmentDetailOneTableViewCell.h"
#import "AppointmentDetailTwoTableViewCell.h"
#import "AppointmentDetailThrTableViewCell.h"
#import "AppointmentModel.h"

@interface AppointmentChuanglianrenListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *appointmentListTableView;
@property (nonatomic,strong)NSMutableArray *appointmentMutableArr;
@end

@implementation AppointmentChuanglianrenListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetDongtaiManamer];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"币本有约创链人";
    self.view.backgroundColor = RGB(242, 244, 245, 1);
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.appointmentMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.appointmentListTableView){
        self.appointmentListTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.appointmentListTableView.dataSource = self;
        self.appointmentListTableView.delegate = self;
        [self.view addSubview:self.appointmentListTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.appointmentListTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetDongtaiManamer];
    }];
    [self.appointmentListTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetDongtaiManamer];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.appointmentMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        AppointmentDetailOneTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[AppointmentDetailOneTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferAppointmentModel = [self.appointmentMutableArr objectAtIndex:indexPath.section];
        return cellWithRowOne;
    } else if (indexPath.row == 1){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowTwo";
        AppointmentDetailTwoTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[AppointmentDetailTwoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowThr";
        AppointmentDetailThrTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[AppointmentDetailThrTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferAppointmentModel = [self.appointmentMutableArr objectAtIndex:indexPath.section];

        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    AppointmentModel *appointmentModel =  [self.appointmentMutableArr objectAtIndex:indexPath.section];
    if (indexPath.row == 0){
        return [AppointmentDetailOneTableViewCell calculationCellHeight:appointmentModel];
    } else if (indexPath.row == 1){
        return [AppointmentDetailTwoTableViewCell calculationCellHeight];
    } else {
        return [AppointmentDetailThrTableViewCell calculationCellHeightWithModel:appointmentModel];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(10);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}




-(void)sendRequestToGetDongtaiManamer{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page_number":@(self.appointmentListTableView.currentPage),@"page_size":@"10"};
    [[NetworkAdapter sharedAdapter]fetchWithPath:page_appointment requestParams:params responseObjectClass:[AppointmentListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            AppointmentListModel *listModel = (AppointmentListModel *)responseObject;
            if (strongSelf.appointmentListTableView.isXiaLa){
                [strongSelf.appointmentMutableArr removeAllObjects];
            }
            
            [strongSelf.appointmentMutableArr addObjectsFromArray:listModel.content];
            [strongSelf.appointmentListTableView reloadData];
            
            if (strongSelf.appointmentMutableArr.count){
                [strongSelf.appointmentListTableView dismissPrompt];
            } else {
                [strongSelf.appointmentListTableView showPrompt:@"当前没有最新动态" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }
            if (strongSelf.appointmentListTableView.isXiaLa){
                [strongSelf.appointmentListTableView stopPullToRefresh];
            } else {
                [strongSelf.appointmentListTableView stopFinishScrollingRefresh];
            }
        }
    }];
}

@end
