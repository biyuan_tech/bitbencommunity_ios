//
//  AppointmentModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN


@protocol AppointmentModel <NSObject>

@end

@interface AppointmentModel : FetchModel

@property (nonatomic,copy)NSString *_id;
@property (nonatomic,copy)NSString *ad_img;
@property (nonatomic,assign)NSInteger attention;
@property (nonatomic,assign)NSInteger attention_count;
@property (nonatomic,copy)NSString *author_id;
@property (nonatomic,copy)NSString *begin_time;
@property (nonatomic,assign)NSInteger comment_count;
@property (nonatomic,assign)NSInteger count_support;
@property (nonatomic,assign)NSInteger count_tread;
@property (nonatomic,assign)NSInteger count_uv;
@property (nonatomic,copy)NSString *datetime;
@property (nonatomic,copy)NSString *head_img;
@property (nonatomic,assign)NSInteger isSupport;
@property (nonatomic,copy)NSString *live_cover_url;
@property (nonatomic,copy)NSString *live_record_id;
@property (nonatomic,copy)NSString *live_title;
@property (nonatomic,assign)NSInteger live_type;
@property (nonatomic,copy)NSString *nickname;
@property (nonatomic,assign)NSInteger organ_type;
@property (nonatomic,copy)NSString *play_url;
@property (nonatomic,assign)CGFloat reward;
@property (nonatomic,assign)CGFloat reward_double;
@property (nonatomic,copy)NSString *room_id;
@property (nonatomic,copy)NSString *speaker;
@property (nonatomic,copy)NSString *speaker_head_img;
@property (nonatomic,copy)NSString *speaker_intro;
@property (nonatomic,copy)NSString *speaker_quotation;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,copy)NSString *user_id;
@property (nonatomic,copy)NSString *watch_times;
@property (nonatomic ,assign) NSInteger cert_badge;
/** 成就组 */
@property (nonatomic ,strong) NSArray <NSString *>*achievement_badge_list;

//shareMap =                 {
//    "share_img" = "web-201971154126208-in1n.png";
//    "share_intro" = "\U6700\U65b0\U6700\U70ed\U533a\U5757\U94fe\U5185\U5bb9\Uff0c\U5c3d\U5728\U5e01\U672cAPP!";
//    "share_param" = "live_record_id=613015628927729664room_id=683146032";
//    "share_title" = "\U5e01\U672c\U6709\U7ea6001";
//    "share_url" = "http://m.bitben.com/liveshare?live_record_id=613015628927729664";
//};

@end


@interface AppointmentListModel : FetchModel

@property (nonatomic,strong)NSArray<AppointmentModel> *content;

@end


NS_ASSUME_NONNULL_END
