//
//  AppointmentMainModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AppointmentMainQAModel <NSObject>


@end

@interface AppointmentMainQAModel:FetchModel

@property(nonatomic,copy)NSString *answer;
@property(nonatomic,copy)NSString *question;
@property (nonatomic,assign)BOOL hasShow;
@end


@interface AppointmentMainModel : FetchModel

@property(nonatomic,copy)NSString *aim;
@property(nonatomic,copy)NSString *head_img;
@property(nonatomic,copy)NSString *intro;
@property(nonatomic,copy)NSString *phone;
@property(nonatomic,copy)NSString *subtitle;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *wechat;
@property (nonatomic,strong)NSArray<AppointmentMainQAModel> *qa_list;

@end

NS_ASSUME_NONNULL_END
