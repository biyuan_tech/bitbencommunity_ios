//
//  AppointmentDetailViewController.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/21.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AbstractViewController.h"
#import "AppointmentMainModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppointmentDetailViewController : AbstractViewController

@property (nonatomic,strong)AppointmentMainModel *transferModel;

@end

NS_ASSUME_NONNULL_END
