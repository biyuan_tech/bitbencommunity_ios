//
//  AppointmentDetailViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/21.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AppointmentDetailViewController.h"

@interface AppointmentDetailViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *detailTableView;
@property (nonatomic,strong)NSArray *detailArr;
@end

@implementation AppointmentDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UURandomColor;
    
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"币本有约";
    
    PDImageView *bgImgView = [[PDImageView alloc]init];
    bgImgView.image = [UIImage imageNamed:@"bg_bibenyouyue"];
    bgImgView.frame = kScreenBounds;
    [self.view addSubview:bgImgView];
}

-(void)arrayWithInit{
    self.detailArr = @[@[@"题目"],@[@"介绍"],@[@"宗旨"]];
}

-(void)createTableView{
    if (!self.detailTableView){
        self.detailTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.detailTableView.dataSource = self;
        self.detailTableView.delegate = self;
        [self.view addSubview:self.detailTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.detailArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr  = [self.detailArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.backgroundColor = [UIColor clearColor];
            
            UILabel *titleLabel = [GWViewTool createLabelFont:@"49" textColor:@"FFFFFF"];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.font = [[UIFont fontWithCustomerSizeName:@"49"] boldFont];
            titleLabel.stringTag = @"titleLabel";
            [cellWithRowOne addSubview:titleLabel];
            
            UIView *alphaView = [[UIView alloc]init];
            alphaView.backgroundColor = [UIColor whiteColor];
            alphaView.stringTag = @"alphaView";
            alphaView.alpha = .5f;
            [cellWithRowOne addSubview:alphaView];
            
            UILabel *itemsLabel = [GWViewTool createLabelFont:@"20" textColor:@"FFFFFF"];
            itemsLabel.font = [itemsLabel.font boldFont];
            itemsLabel.stringTag = @"itemsLabel";
            [cellWithRowOne addSubview:itemsLabel];
        }
        UILabel *titleLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"titleLabel"];
        titleLabel.frame = CGRectMake(LCFloat(31), LCFloat(55), kScreenBounds.size.width - 2 * LCFloat(31), LCFloat(45));
        titleLabel.text = @"币本有约";

        // alpha
        UIView *alphaView = (UIView *)[cellWithRowOne viewWithStringTag:@"alphaView"];
        
        UILabel *itemsLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"itemsLabel"];
        itemsLabel.text = @"创业+创新+区块链";
        CGSize itemSize=  [Tool makeSizeWithLabel:itemsLabel];
        alphaView.frame = CGRectMake(titleLabel.orgin_x, LCFloat(115), itemSize.width + 2 * LCFloat(15), itemSize.height + 2 * LCFloat(11));
        itemsLabel.frame = CGRectMake(alphaView.orgin_x + LCFloat(15), alphaView.orgin_y + LCFloat(11), itemSize.width, itemSize.height);
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowOne.backgroundColor = [UIColor clearColor];
            // View
            UIView *lineView = [[UIView alloc]init];
            lineView.backgroundColor = [UIColor whiteColor];
            lineView.stringTag = @"lineView";
            [cellWithRowOne addSubview:lineView];
            
            UILabel *titleLabel = [GWViewTool createLabelFont:@"20" textColor:@"FFFFFF"];
            titleLabel.font = [titleLabel.font boldFont];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.stringTag = @"titleLabel";
            [cellWithRowOne addSubview:titleLabel];
            
            UILabel *itemsLabel = [GWViewTool createLabelFont:@"15" textColor:@"FFFFFF"];
            itemsLabel.stringTag = @"itemsLabel";
            itemsLabel.numberOfLines = 0;
            [cellWithRowOne addSubview:itemsLabel];
        }
        UILabel *titleLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"titleLabel"];
        
        UIView *lineView = (UIView *)[cellWithRowOne viewWithStringTag:@"lineView"];
        lineView.frame = CGRectMake(LCFloat(31), 0, LCFloat(3), LCFloat(18));
        
        UILabel *itemsLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"itemsLabel"];
        
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"介绍" sourceArr:self.detailArr]){
            titleLabel.text = @"币本有约";
            itemsLabel.text = self.transferModel.aim;
        } else {
            titleLabel.text = @"宗旨";
            itemsLabel.text = self.transferModel.aim;
        }
        CGSize titleSize = [Tool makeSizeWithLabel:titleLabel];
        titleLabel.frame = CGRectMake(CGRectGetMaxX(lineView.frame) + LCFloat(12), lineView.orgin_y, titleSize.width, titleSize.height);
        
        CGFloat width = kScreenBounds.size.width - 2 * LCFloat(31);
        CGSize descSize = [itemsLabel.text sizeWithCalcFont:itemsLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
        itemsLabel.frame = CGRectMake(LCFloat(31), CGRectGetMaxY(titleLabel.frame) + LCFloat(19), width, descSize.height);
        
        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return LCFloat(200);
    } else {
        NSString *info = @"";
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"介绍" sourceArr:self.detailArr]){
            info = self.transferModel.aim;
        } else {
            info = self.transferModel.aim;
        }
        CGFloat width = kScreenBounds.size.width - 2 * LCFloat(31);
        CGSize descSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"15"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
        return descSize.height + LCFloat(19) + [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"20"]];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"宗旨" sourceArr:self.detailArr]){
        return LCFloat(19);
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}
@end
