//
//  AppointmentViewController.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppointmentViewController : AbstractViewController

- (void)autoReload;

@end

NS_ASSUME_NONNULL_END
