//
//  AuthorAblumSingleTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "SpecialColumnModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AuthorAblumSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)SpecialColumnModel *transferSpecialModel;

-(void)actionClickWithDingyueManagerBlock:(void(^)())block;

-(void)actionLinkManager:(BOOL)hasLink;
-(void)avatarSelectedWithBlock:(void(^)())block;
@end

NS_ASSUME_NONNULL_END
