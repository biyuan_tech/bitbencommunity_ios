//
//  SpecialColumnModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SpecialColumnModel <NSObject>

@end

@interface SpecialColumnModel : FetchModel

@property (nonatomic,copy)NSString *account_id;
@property (nonatomic,assign)NSInteger article;
@property (nonatomic,assign)NSInteger comment_count;
@property (nonatomic,assign)NSInteger fans;
@property (nonatomic,assign)BOOL is_attention;
@property (nonatomic,copy)NSString *head_img;
@property (nonatomic,assign)NSInteger live;
@property (nonatomic,copy)NSString *nickname;
@property (nonatomic,assign)NSInteger support_count;
@property (nonatomic,copy)NSString *user_id;
@property (nonatomic,copy)NSString *self_introduction;
@property (nonatomic ,assign) NSInteger cert_badge;
/** 成就组 */
@property (nonatomic ,strong) NSArray <NSString *>*achievement_badge_list;

@end

@interface SpecialColumnListModel : FetchModel

@property (nonatomic,strong)NSArray<SpecialColumnModel> *content;

@end


NS_ASSUME_NONNULL_END
