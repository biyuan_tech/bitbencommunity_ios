//
//  AuthorHeaderTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AuthorHeaderTableViewCell.h"

static char actionClickBlockKey;
#define author_info @"来自区块链行业优秀作者，为您提供区块链行业资讯、观点、报道以及区块链项目分析，区块链行业最 有思想的 KOL 聚集地。"
@interface AuthorHeaderTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *desLabel;
@property (nonatomic,strong)UIButton *actionButton;

@end

@implementation AuthorHeaderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"30" textColor:@"黑"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
    
    self.desLabel = [GWViewTool createLabelFont:@"15" textColor:@"333333"];
    self.desLabel.numberOfLines = 0;
    [self addSubview:self.desLabel];
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.actionButton];
    
    [self actionFrame];
}

-(void)actionFrame{
    self.titleLabel.text = @"专栏作者";
    self.titleLabel.frame = CGRectMake(0, LCFloat(11), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    self.desLabel.text = author_info;
    CGSize desSize = [self.desLabel.text sizeWithCalcFont:self.desLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(15), CGFLOAT_MAX)];
    [self.desLabel setText:author_info lineSpacing:LCFloat(5)];
    NSInteger number = desSize.height / [NSString contentofHeightWithFont:self.desLabel.font];
    self.desLabel.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(18), kScreenBounds.size.width - 2 * LCFloat(15), desSize.height + (number - 1) * LCFloat(5));
    
    self.actionButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(120)) / 2., CGRectGetMaxY(self.desLabel.frame) + LCFloat(28), LCFloat(120), LCFloat(40));
    [self.actionButton setTitleColor:[UIColor hexChangeFloat:@"EA6438"] forState:UIControlStateNormal];
    [self.actionButton setTitle:@"申请入驻专栏" forState:UIControlStateNormal];
    self.actionButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"16"];
    self.actionButton.layer.cornerRadius = LCFloat(5);
    self.actionButton.clipsToBounds = YES;
    self.actionButton.layer.borderColor = [UIColor hexChangeFloat:@"EA6438"].CGColor;
    self.actionButton.layer.borderWidth = 1.f;
    __weak typeof(self)weakSelf  = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickBlockKey);
        if (block){
            block();
        }
    }];
}

-(void)actionClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellheight = 0;
    cellheight += LCFloat(11);
    cellheight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"34"]];
    cellheight += LCFloat(18);
    CGSize infoSize = [author_info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"15"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(15), CGFLOAT_MAX)];
    cellheight += infoSize.height;
    
    NSInteger number = infoSize.height / [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"15"]];;
    cellheight += (number - 1) * LCFloat(5);
    
    cellheight += LCFloat(28);
    cellheight += LCFloat(40);
    cellheight += LCFloat(24);
    return cellheight;
}

@end
