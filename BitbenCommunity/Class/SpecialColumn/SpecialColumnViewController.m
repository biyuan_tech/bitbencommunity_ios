//
//  SpecialColumnViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "SpecialColumnViewController.h"
#import "AuthorHeaderTableViewCell.h"
#import "AuthorAblumSingleTableViewCell.h"
#import "NetworkAdapter+Center.h"
#import "BYPersonHomeController.h"

@interface SpecialColumnViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *specialTableView;
@property (nonatomic,strong)NSMutableArray *infoMutableArr;
@property (nonatomic,strong)NSMutableArray *rootMutableArr;
@end

@implementation SpecialColumnViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self InterfaceManager];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"专栏";
    self.view.backgroundColor = [UIColor hexChangeFloat:@"F2F4F5"];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.infoMutableArr = [NSMutableArray array];
    self.rootMutableArr = [NSMutableArray array];
    [self.rootMutableArr addObject:@[@"头部"]];
    [self.rootMutableArr addObject:self.infoMutableArr];
    
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.specialTableView){
        self.specialTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.specialTableView.dataSource = self;
        self.specialTableView.delegate = self;
        [self.view addSubview:self.specialTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.specialTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf InterfaceManager];
    }];
    [self.specialTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf InterfaceManager];
    }];
}

#pragma mark - arrayWithInit
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.rootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr  =[self.rootMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        AuthorHeaderTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if(!cellWithRowOne){
            cellWithRowOne = [[AuthorHeaderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDWebViewController *webViewController = [[PDWebViewController alloc]init];
            [webViewController webDirectedWebUrl:@"http://bibenshequ.mikecrm.com/05aJjk9"];
            [strongSelf.navigationController pushViewController:webViewController animated:YES];
        }];

        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        AuthorAblumSingleTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if(!cellWithRowTwo){
            cellWithRowTwo = [[AuthorAblumSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        SpecialColumnModel *speModel = [self.infoMutableArr objectAtIndex:indexPath.row];
        cellWithRowTwo.transferSpecialModel = speModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo actionClickWithDingyueManagerBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf linkWIthCell:cellWithRowTwo hasLink:!speModel.is_attention];
        }];
        [cellWithRowTwo avatarSelectedWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = cellWithRowTwo.transferSpecialModel.account_id;
            [strongSelf.navigationController pushViewController:personHomeController animated:YES];
        }];
        
        
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0 && indexPath.section == 0){
        return [AuthorHeaderTableViewCell calculationCellHeight];
    } else {
        return [AuthorAblumSingleTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else {
        return LCFloat(8);
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.specialTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.rootMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.rootMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"ranking"];
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - Interface
-(void)InterfaceManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] getZhuanlanManagerWithPage:self.specialTableView.currentPage block:^(SpecialColumnListModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.specialTableView.isXiaLa){
            [strongSelf.infoMutableArr removeAllObjects];
        }
        [strongSelf.infoMutableArr addObjectsFromArray:listModel.content];
        [strongSelf.specialTableView reloadData];
        
        if (strongSelf.specialTableView.isXiaLa){
            [strongSelf.specialTableView stopPullToRefresh];
        } else {
            [strongSelf.specialTableView stopFinishScrollingRefresh];
        }
    }];
}


#pragma mark - 关注
-(void)linkWIthCell:(AuthorAblumSingleTableViewCell *)cell hasLink:(BOOL)link{
    NSString *userId = cell.transferSpecialModel.account_id;
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]centerSendRequestToLinkManagerWithUserId:userId hasLink:link block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        if (isSuccessed){
            if (link){
                [StatusBarManager statusBarHidenWithText:@"已关注"];
                [AccountModel sharedAccountModel].loginServerModel.infoCount.attention += 1;
            } else {
                [AccountModel sharedAccountModel].loginServerModel.infoCount.attention -= 1;
            }
            [cell actionLinkManager:link];
        }
    }];
}

@end
