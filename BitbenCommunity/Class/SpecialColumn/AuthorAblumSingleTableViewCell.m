//
//  AuthorAblumSingleTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AuthorAblumSingleTableViewCell.h"

static char avatarTapKey;
static char actionClickWithDingyueManagerBlockKey;
@interface AuthorAblumSingleTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)PDImageView *convertImgView;
@property (nonatomic,strong)UIButton *avatarButton;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *dingyueLabel;
@property (nonatomic,strong)UILabel *desLabel;
@property (nonatomic,strong)UIButton *dingyueButton;
@property (nonatomic,strong)UILabel *wenzhangLabel;
@property (nonatomic,strong)UILabel *zhiboLabel;
@property (nonatomic,strong)UILabel *pinglunLabel;
@property (nonatomic,strong)UILabel *zanLabel;
/** 承载成就图标 */
@property (nonatomic ,strong) UIView *achievementView;

@end

@implementation AuthorAblumSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.convertImgView = [[PDImageView alloc]init];
    self.convertImgView.backgroundColor = [UIColor clearColor];
    self.convertImgView.image = [UIImage imageNamed:@"bg_avatar_convert"];
    [self addSubview:self.convertImgView];
    
    self.avatarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.avatarButton];
    
    self.nickNameLabel = [GWViewTool createLabelFont:@"15" textColor:@"323232"];
    [self addSubview:self.nickNameLabel];
    
    // 承载成就view
    self.achievementView = [[UIView alloc] init];
    [self.contentView addSubview:self.achievementView];
    
    self.dingyueLabel = [GWViewTool createLabelFont:@"11" textColor:@"EA6438"];
    [self addSubview:self.dingyueLabel];
    
    self.dingyueButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.dingyueButton];
    self.dingyueButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"10"];
    self.dingyueButton.layer.cornerRadius = LCFloat(3);
    self.dingyueButton.layer.borderWidth = 1.f;
    
    self.desLabel = [GWViewTool createLabelFont:@"13" textColor:@"828282"];
    self.desLabel.numberOfLines = 2;
    [self addSubview:self.desLabel];

    self.wenzhangLabel = [GWViewTool createLabelFont:@"10" textColor:@"828282"];
    [self addSubview:self.wenzhangLabel];
    
    self.zhiboLabel = [GWViewTool createLabelFont:@"10" textColor:@"828282"];
    [self addSubview:self.zhiboLabel];
    
    self.pinglunLabel = [GWViewTool createLabelFont:@"10" textColor:@"828282"];
    [self addSubview:self.pinglunLabel];
    
    self.zanLabel = [GWViewTool createLabelFont:@"10" textColor:@"828282"];
    [self addSubview:self.zanLabel];
}

-(void)avatarSelectedWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &avatarTapKey,block , OBJC_ASSOCIATION_COPY_NONATOMIC);
}


-(void)avatarTap{
    void(^block)() = objc_getAssociatedObject(self, &avatarTapKey);
    if (block){
        block();
    }
}
-(void)setTransferSpecialModel:(SpecialColumnModel *)transferSpecialModel{
    _transferSpecialModel = transferSpecialModel;
    self.avatarImgView.style = transferSpecialModel.cert_badge;
    // avatar
    self.avatarImgView.frame = CGRectMake(LCFloat(15), LCFloat(17), LCFloat(44), LCFloat(44));
    [self.avatarImgView uploadHDImageWithURL:transferSpecialModel.head_img callback:NULL];
    
    self.convertImgView.frame = CGRectMake(0, 0, LCFloat(44), LCFloat(44));
    self.convertImgView.center = self.avatarImgView.center;
    
    self.avatarButton.frame = self.convertImgView.frame;
    __weak typeof(self)weakSelf = self;
    [self.avatarButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf avatarTap];
    }];
    
    // nick
    self.nickNameLabel.text = transferSpecialModel.nickname;
    CGSize nickSize = [Tool makeSizeWithLabel:self.nickNameLabel];
    CGFloat originX = CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11);
    CGFloat maxWidth = kCommonScreenWidth - originX - 68 - LCFloat(15) - LCFloat(44);
    CGFloat nickWidth = nickSize.width > maxWidth ? maxWidth : nickSize.width;
    self.nickNameLabel.frame = CGRectMake(originX, self.avatarImgView.orgin_y + LCFloat(7), nickWidth, nickSize.height);
    
    // 添加成就
    self.achievementView.frame = CGRectMake(CGRectGetMaxX(_nickNameLabel.frame) + 5,
                                            0, 58, 16);
    self.achievementView.center = CGPointMake(self.achievementView.center_x, self.nickNameLabel.center_y);
    
    for (UIView *subView in self.achievementView.subviews) {
        [subView removeFromSuperview];
    }
    
    NSArray *achievementData = transferSpecialModel.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [self.achievementView addSubview:imageView];
    }
    
    // 订阅
    self.dingyueLabel.text = [NSString stringWithFormat:@"%li人订阅",transferSpecialModel.fans];
    CGSize dingyueSize = [Tool makeSizeWithLabel:self.dingyueLabel];
    self.dingyueLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - dingyueSize.width, LCFloat(25), dingyueSize.width, dingyueSize.height);
    
    // des
    self.desLabel.text = transferSpecialModel.self_introduction.length?transferSpecialModel.self_introduction:@"这个人很懒,什么都没有留下。";
    
    CGFloat width = kScreenBounds.size.width - LCFloat(69) - LCFloat(84);
    CGSize contentOfSize = [self.desLabel.text sizeWithCalcFont:self.desLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.desLabel.frame = CGRectMake(LCFloat(69), CGRectGetMaxY(self.nickNameLabel.frame) + LCFloat(11), width, contentOfSize.height);
    
    //
    CGFloat main_width = kScreenBounds.size.width - LCFloat(69) - LCFloat(15);
    CGFloat margin = LCFloat(11);
    CGFloat singleWidth = (main_width - 3 * margin) / 4.;
    self.wenzhangLabel.text = [NSString stringWithFormat:@"%li文章量",transferSpecialModel.article];
    self.zhiboLabel.text = [NSString stringWithFormat:@"%li直播",transferSpecialModel.live];
    self.pinglunLabel.text = [NSString stringWithFormat:@"%li评论",transferSpecialModel.comment_count];
    self.zanLabel.text = [NSString stringWithFormat:@"%li点赞",transferSpecialModel.support_count];
    
    self.wenzhangLabel.textAlignment = NSTextAlignmentLeft;
    self.zanLabel.textAlignment = NSTextAlignmentCenter;
    self.wenzhangLabel.frame = CGRectMake(self.nickNameLabel.orgin_x + singleWidth * 0, CGRectGetMaxY(self.nickNameLabel.frame) + LCFloat(76), singleWidth, [NSString contentofHeightWithFont:self.wenzhangLabel.font]);
    self.zhiboLabel.frame = CGRectMake(self.nickNameLabel.orgin_x + singleWidth * 1, CGRectGetMaxY(self.nickNameLabel.frame) + LCFloat(76), singleWidth, [NSString contentofHeightWithFont:self.wenzhangLabel.font]);
    self.pinglunLabel.frame = CGRectMake(self.nickNameLabel.orgin_x + singleWidth * 2, CGRectGetMaxY(self.nickNameLabel.frame) + LCFloat(76), singleWidth, [NSString contentofHeightWithFont:self.wenzhangLabel.font]);
    self.zanLabel.frame = CGRectMake(self.nickNameLabel.orgin_x + singleWidth * 3, CGRectGetMaxY(self.nickNameLabel.frame) + LCFloat(76), singleWidth, [NSString contentofHeightWithFont:self.wenzhangLabel.font]);
    
    
    if (transferSpecialModel.is_attention == 0){
        [self.dingyueButton setTitle:@"订阅" forState:UIControlStateNormal];
        [self.dingyueButton setTitleColor:[UIColor hexChangeFloat:@"EA6438"] forState:UIControlStateNormal];
        self.dingyueButton.layer.borderColor = [UIColor hexChangeFloat:@"EA6438"].CGColor;
    } else {
        [self.dingyueButton setTitle:@"已订阅" forState:UIControlStateNormal];
        [self.dingyueButton setTitleColor:[UIColor hexChangeFloat:@"B2B2B2"] forState:UIControlStateNormal];
        self.dingyueButton.layer.borderColor = [UIColor hexChangeFloat:@"B2B2B2"].CGColor;
    }
    self.dingyueButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(44), LCFloat(48), LCFloat(44), LCFloat(24));
    [self.dingyueButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithDingyueManagerBlockKey);
        if (block){
            block();
        }
    }];
}

-(void)actionClickWithDingyueManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithDingyueManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionLinkManager:(BOOL)hasLink{
    self.transferSpecialModel.is_attention = !self.transferSpecialModel.is_attention;
    if (hasLink){
        [self.dingyueButton setTitle:@"已订阅" forState:UIControlStateNormal];
        [self.dingyueButton setTitleColor:[UIColor hexChangeFloat:@"B2B2B2"] forState:UIControlStateNormal];
        self.dingyueButton.layer.borderColor = [UIColor hexChangeFloat:@"B2B2B2"].CGColor;
        
        self.transferSpecialModel.fans += 1;
        self.dingyueLabel.text = [NSString stringWithFormat:@"%li人订阅",(long)self.transferSpecialModel.fans];
    } else {
        [self.dingyueButton setTitle:@"订阅" forState:UIControlStateNormal];
        [self.dingyueButton setTitleColor:[UIColor hexChangeFloat:@"EA6438"] forState:UIControlStateNormal];
        self.dingyueButton.layer.borderColor = [UIColor hexChangeFloat:@"EA6438"].CGColor;
        
        self.transferSpecialModel.fans -= 1;
        self.dingyueLabel.text = [NSString stringWithFormat:@"%li人订阅",(long)self.transferSpecialModel.fans];
    }
    CGSize dingyueSize = [Tool makeSizeWithLabel:self.dingyueLabel];
    self.dingyueLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - dingyueSize.width, LCFloat(25), dingyueSize.width, dingyueSize.height);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(140);
}


@end
