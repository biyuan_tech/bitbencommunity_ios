//
//  NetworkAdapter+Login.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/23.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "NetworkAdapter.h"
#import "ShareSDKManager.h"

typedef NS_ENUM(NSInteger,SMSCodeType) {
    SMSCodeType0 = 0,                           /**< 手机号码存在的时候发送验证码*/
    SMSCodeType1 = 1,                           /**< 手机号码不存在的时候发送验证码*/
    SMSCodeType2 = 2,                           /**< 手机号码为被占用的时候发送验证码*/
};


@interface NetworkAdapter (Login)

#pragma mark - 注册
-(void)loginRegisterWithPhoneNumber:(NSString *)phoneNumber password:(NSString *)password smsCode:(NSString *)smsCode yaoqing:(NSString *)yaoqing block:(void(^)(BOOL isSuccessed))block;
#pragma mark - 1.0 注册
-(void)loginRegisterWithPhoneNumber:(NSString *)phoneNumber password:(NSString *)password smsCode:(NSString *)smsCode channel:(thirdLoginType)loginType authToken:(NSString *)authToken yaoqing:(NSString *)yaoqing block:(void(^)(BOOL isSuccessed))block;
#pragma mark - 登录主方法
-(void)loginThirdType:(thirdLoginType)loginType account:(NSString *)account pwd:(NSString *)pwd block:(void(^)(BOOL isSuccessed,LoginServerModelAccount *serverAccountModel))block;
#pragma mark - 发送验证码
-(void)loginSendSMSCode:(NSString *)phoneNumber block:(void(^)(BOOL isSuccessed))block;
#pragma mark - 找回密码发送验证码
-(void)loginSendSMSCodeWithFindPwd:(NSString *)phoneNumber block:(void(^)(BOOL isSuccessed))block;
#pragma mark - 校验验证码
-(void)loginValidateSMSCodeWithPhone:(NSString *)phoneNumber smsCode:(NSString *)smsCode block:(void(^)(BOOL isSuccessed))block;
#pragma mark - 根据账户ID获取账户信息
-(void)loginGetAccountInfoWithId:(NSString *)accountId block:(void(^)())block;
#pragma mark - 修改资料
-(void)loginUpdateAccountInfoWithUserDic:(NSDictionary *)params block:(void(^)())block;
#pragma mark - 修改头像
-(void)loginWithUploadAvatar:(NSString *)avatarUrl block:(void(^)(BOOL isSuccessed ,NSString *avararUrl))block;
#pragma mark - 忘记密码
-(void)loginResetPwdManagerWithPhone:(NSString *)phone smsCode:(NSString *)smsCode pwd:(NSString *)pwd block:(void(^)(BOOL isSuccessed))block;
#pragma mark - 发送验证码
-(void)sendRequestToSendSMSCode:(NSString *)phoneNumber  statuas:(SMSCodeType)status block:(void(^)(BOOL isSuccessed))block;
@end
