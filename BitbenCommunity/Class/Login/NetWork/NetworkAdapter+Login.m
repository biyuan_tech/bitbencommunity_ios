//
//  NetworkAdapter+Login.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/23.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "NetworkAdapter+Login.h"
#import "LoginServerModel.h"
#import "ThirdLoginModel.h"
#import "BYIMManager.h"
#import "NetworkAdapter+Center.h"
#import <Bugtags/Bugtags.h>
#import "EveryManager.h"

@implementation NetworkAdapter (Login)

-(void)loginRegisterWithPhoneNumber:(NSString *)phoneNumber password:(NSString *)password smsCode:(NSString *)smsCode yaoqing:(NSString *)yaoqing block:(void(^)(BOOL isSuccessed))block{
    [self loginRegisterWithPhoneNumber:phoneNumber password:password smsCode:smsCode channel:thirdLoginTypeNative authToken:nil yaoqing:yaoqing block:^(BOOL isSuccessed) {
        if (block){
            block(isSuccessed);
        }
    }];
}

#pragma mark - 1.0 注册
-(void)loginRegisterWithPhoneNumber:(NSString *)phoneNumber password:(NSString *)password smsCode:(NSString *)smsCode channel:(thirdLoginType)loginType authToken:(NSString *)authToken yaoqing:(NSString *)yaoqing block:(void(^)(BOOL isSuccessed))block{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (phoneNumber.length){
        [params setObject:phoneNumber forKey:@"phone"];
    }
    if (password.length){
        [params setObject:password forKey:@"password"];
    }
    if (smsCode.length){
        [params setObject:smsCode forKey:@"sms_code"];
    }

    [params setObject:@(loginType) forKey:@"channel"];
    if (authToken.length){
        [params setObject:authToken forKey:@"oauth_id"];
    }
    if (yaoqing.length){
        [params setObject:yaoqing forKey:@"invite_code"];
    }
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_register requestParams:params responseObjectClass:[LoginServerModelAccount class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        
        if (isSucceeded){
            if (![AccountModel sharedAccountModel].loginServerModel){
                LoginServerModel *loginServerModel = [[LoginServerModel alloc]init];
                [AccountModel sharedAccountModel].loginServerModel = loginServerModel;
            }
            LoginServerModelAccount *singleModel = (LoginServerModelAccount *)responseObject;
            [AccountModel sharedAccountModel].loginServerModel.account = singleModel;
            
            if (block){
                block(YES);
            }
        } else {
            if (block){
                block(NO);
            }
        }
    }];
}

#pragma mark - 登录主方法
-(void)loginThirdType:(thirdLoginType)loginType account:(NSString *)account pwd:(NSString *)pwd block:(void(^)(BOOL isSuccessed,LoginServerModelAccount *serverAccountModel))block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *path = @"";
    if (loginType == thirdLoginTypeNative){         // 本地登录
        [params setObject:account forKey:@"phone"];
        [params setObject:pwd forKey:@"password"];
        path = login_login;
    } else if (loginType == thirdLoginTypeWechat){
        [params setObject:account forKey:@"oauth_id"];
        [params setObject:@(loginType) forKey:@"channel"];
        path = login_other_login;
    }
    [[NetworkAdapter sharedAdapter] fetchWithPath:path requestParams:params responseObjectClass:[LoginServerModelAccount class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        
        if (isSucceeded){
            LoginServerModelAccount *singleModel = (LoginServerModelAccount *)responseObject;
            if (singleModel.userSig.length){
                [AccountModel sharedAccountModel].userSig = singleModel.userSig;
            }
            
            if (!(loginType == thirdLoginTypeWechat && singleModel.isFirst)){
                // 账号统计
                [MTAManager profileSignInWithId:singleModel._id];
                
                if (![AccountModel sharedAccountModel].loginServerModel){
                    [AccountModel sharedAccountModel].loginServerModel = [[LoginServerModel alloc]init];
                }
                [AccountModel sharedAccountModel].loginServerModel.account = singleModel;
                [AccountModel sharedAccountModel].unReadMessage = [AccountModel sharedAccountModel].loginServerModel.account.unread_message;
                
                [AccountModel sharedAccountModel].account_id = singleModel._id;
                
                // 保存UserDefault
                [Tool userDefaulteWithKey:LoginAccount Obj:account];
                [Tool userDefaulteWithKey:LoginType Obj:[NSString stringWithFormat:@"%li",(long)loginType]];
                // 保存头像
                [Tool userDefaulteWithKey:LoginAvatar Obj:singleModel.head_img];
                if (loginType == thirdLoginTypeWechat){
                    [Tool userDefaulteWithKey:LoginPassword Obj:@"pwd"];
                } else {
                    [Tool userDefaulteWithKey:LoginPassword Obj:pwd];
                }
                
                // 全局通知
                NSDictionary *resultDic = @{@"loginStatus":@"1"};
                [[NSNotificationCenter defaultCenter]postNotificationName:LoginNotificationWithLoginSuccessed object:nil userInfo:resultDic];

                [Bugtags setUserData:singleModel._id forKey:@"userId"];
                [Bugtags setUserData:singleModel.name forKey:@"nickName"];
                [Bugtags setUserData:singleModel.phone forKey:@"phoneNumber"];
                
                // 获取当前的用户资料
                [[NetworkAdapter sharedAdapter] centerGetUserCountInfoManagerBlock:^(BOOL isSuccessed) {
                    if (block){
                        block(YES,singleModel);
                    }
                }];
                
                // 获取当前的bbt
                if (singleModel.today_coin.is_first == YES){
                    EveryManager *everyManager = [[EveryManager alloc]init];
                    [everyManager showEveryManagerWithTitle:[NSString stringWithFormat:@"+ %@ BBT",singleModel.today_coin.bbt]];
                }
                
            } else {
                if (block){
                    block(YES,singleModel);
                }
            }
        } else {
            if (block){
                block(NO,nil);
            }
        }
    }];
}

#pragma mark - 根据账户ID获取账户信息
-(void)loginGetAccountInfoWithId:(NSString *)accountId block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"accountId":accountId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_getAccount requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        } else {
            if (block){
                block();
            }
        }
    }];
}

#pragma mark - 修改密码
-(void)loginChangePwdWithAccountId:(NSString *)accountId password:(NSString *)password newPassword:(NSString *)newPassword block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"accountId":accountId,@"password":password,@"newPassword":newPassword};
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_getAccountByPhone requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            
        } else {
            
        }
    }];
}

#pragma mark - 修改手机号
-(void)loginUpdatePhoneNumber:(NSString *)phoneNumber accountId:(NSString *)accountId block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"phone":phoneNumber,@"accountId":accountId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_updatePhone requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        } else {
            if (block){
                block();
            }
        }
    }];
}

#pragma mark - 发送验证码
-(void)loginSendSMSCode:(NSString *)phoneNumber block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"phone":phoneNumber};
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_SMS requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block(YES);
            }
        } else {
            if (block){
                block(NO);
            }
        }
    }];
}

#pragma mark - 找回密码发送验证码
-(void)loginSendSMSCodeWithFindPwd:(NSString *)phoneNumber block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"phone":phoneNumber};
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_send_password_code requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block(YES);
            }
        } else {
            if (block){
                block(NO);
            }
        }
    }];
}

#pragma mark - 校验验证码
-(void)loginValidateSMSCodeWithPhone:(NSString *)phoneNumber smsCode:(NSString *)smsCode block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"phone":phoneNumber,@"sms_code":smsCode};
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_validateSMS requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block(YES);
            }
        } else {
            if (block){
                block(NO);
            }
        }
    }];
}

#pragma mark - 修改头像
-(void)loginChangeAvatarWithAccountId:(NSString *)accountId headImg:(NSString *)headImg block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"accountId":accountId,@"headImg":headImg};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_updateAvatar requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        } else {
            if (block){
                block();
            }
        }
    }];
}

#pragma mark - 获取用户信息和账户信息
-(void)loginGetUserAccountInfoWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"userId":@""};
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_user_and_account requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        } else {
            if (block){
                block();
            }
        }
    }];
}

#pragma mark - 修改资料
-(void)loginUpdateAccountInfoWithUserDic:(NSDictionary *)params block:(void(^)())block{
    __weak typeof(self)weaKSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:update_user_and_account requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weaKSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        } else {
            if(block){
                block();
            }
        }
    }];
}

#pragma mark - 修改头像
-(void)loginWithUploadAvatar:(NSString *)avatarUrl block:(void(^)(BOOL isSuccessed ,NSString *avararUrl))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"head_img":avatarUrl};
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_updateAvatar requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block(YES,avatarUrl);
            }
        } else {
            if (block){
                block(NO,nil);
            }
        }
    }];
}

#pragma mark - 忘记密码
-(void)loginResetPwdManagerWithPhone:(NSString *)phone smsCode:(NSString *)smsCode pwd:(NSString *)pwd block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"phone":phone,@"sms_code":smsCode,@"password":pwd};
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_updatePwd requestParams:params responseObjectClass:[LoginServerModelAccount class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (![AccountModel sharedAccountModel].loginServerModel){
                LoginServerModel *loginServerModel = [[LoginServerModel alloc]init];
                [AccountModel sharedAccountModel].loginServerModel = loginServerModel;
            }
            LoginServerModelAccount *singleModel = (LoginServerModelAccount *)responseObject;
            [AccountModel sharedAccountModel].loginServerModel.account = singleModel;
            
            if (block){
                block(YES);
            }
        }
    }];
}

#pragma mark - 发送验证码
-(void)sendRequestToSendSMSCode:(NSString *)phoneNumber  statuas:(SMSCodeType)status block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSString *error = @"";
    if (!phoneNumber.length){
        error = @"请输入手机号";
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }
    NSDictionary *params = @{@"phone":phoneNumber,@"status":@(status)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:send_sms_code requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block(YES);
            }
        }
    }];
}

@end

