//
//  AccountModel.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"
#import "LoginServerModel.h"
#import "ShareSDKManager.h"
#import "ServerRootModel.h"

@interface AccountModel : FetchModel

+(instancetype)sharedAccountModel;                                                               /**< 单例*/

@property (nonatomic,strong)LoginServerModel *loginServerModel;                                  /**<本地登录的Model*/
@property (nonatomic,strong)NSMutableArray *loginServerMutableArr;
@property (nonatomic,strong)NSString *block_Address;
@property (nonatomic,strong)NSString *token;
@property (nonatomic,copy)NSString *account_id;
@property (nonatomic,assign)thirdLoginType loginType;                                           /**< 登录类型*/
@property (nonatomic,strong)ServerRootMainModel *serverModel;                                   /**< 服务器分发*/
@property (nonatomic,assign)BOOL isShenhe;
@property (nonatomic,strong)UIImage *faceImg;
@property (nonatomic,assign)NSInteger getServerListTimes;
@property (nonatomic,strong)UIImage *userAvatarImg;
@property (nonatomic,copy)NSString *userSig;
@property (nonatomic,assign)NSInteger unReadMessage;


@property (nonatomic,assign)BOOL hasShowLogin;

// 判断是否登录
- (BOOL)hasLoggedIn;

#pragma mark - 自动登录
-(void)autoLoginWithBlock:(void(^)(BOOL isSuccessed))block;
#pragma mark - 自动登录
- (void)authorizeWithCompletionHandler:(void(^)(BOOL successed))handler;
#pragma mark - 无UI效果的登录
-(void)autoLoginWithNoneWindowBlock:(void(^)(BOOL isLogin))handler;


#pragma mark - logout
-(void)logoutManagerBlock:(void(^)())block;









@end
