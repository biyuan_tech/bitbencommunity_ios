//
//  ServerRootModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/1.
//  Copyright © 2018 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger , BY_VERSION_STATUS) { // 版本状态
    BY_VERSION_STATUS_FORCED = 0, // 不支持版本，需强更新
    BY_VERSION_STATUS_UPDATE, // 存在新版本更新
    BY_VERSION_STATUS_LAST_VERSION, // 最新版本
    BY_VERSION_STATUS_IN_REVIEW // 审核中版本
};

@interface ServerRootModel : FetchModel

@property (nonatomic,copy)NSString *port;
@property (nonatomic,copy)NSString *ip;
@property (nonatomic,strong)NSArray *protocol_list;

@end

@interface ServerRootListModel : FetchModel

@property (nonatomic,strong)ServerRootModel *MESSAGE_SERVER;            /**< 消息模块*/
@property (nonatomic,strong)ServerRootModel *USER_SERVER;               /**< 用户模块*/
@property (nonatomic,strong)ServerRootModel *ARTICLE_SERVER;            /**< 文章模块*/
@property (nonatomic,strong)ServerRootModel *ACCOUNT_SERVER;            /**< 账户模块*/
@property (nonatomic,strong)ServerRootModel *GATE_SERVER;               /**< 消息模块*/
@property (nonatomic,strong)ServerRootModel *VIDEO_SERVER;              /**< 消息模块*/
@property (nonatomic,strong)ServerRootModel *LIVE_SERVER;               /**< 消息模块*/
@property (nonatomic,strong)ServerRootModel *FINANCE_SERVER;            /**< 资金模块*/


@end


@interface ServerRootConfigModel : FetchModel
@property (nonatomic,copy)NSString *guide_publish_long_article;         /**< 文章ID*/
@property (nonatomic,copy)NSString *guide_publish_live;         /**< 直播ID*/
@property (nonatomic,copy)NSString *launch_image;

/** 隐藏状态 是否隐藏[0:审核成功，不隐藏；1:审核中，隐藏] */
@property (nonatomic ,assign) BOOL is_live_module_hidden;
/** 版本状态 */
@property (nonatomic ,assign) BY_VERSION_STATUS version_status;
/** 最新版本号 */
@property (nonatomic ,copy) NSString *last_version_number;
/** 更新内容 */
@property (nonatomic ,copy) NSString *last_version_content;
/** 更新地址 */
@property (nonatomic ,copy) NSString *last_version_download_address;


@end

@protocol ServerBadgeModel <NSObject>

@end

@interface ServerBadgeModel : FetchModel

/** id */
@property (nonatomic ,copy) NSString *badge_id;
/** 名称 */
@property (nonatomic ,copy) NSString *name;
/** 图标 */
@property (nonatomic ,copy) NSString *picture;

@end

@interface ServerRootBadgeModel : FetchModel

/** vip配置 */
@property (nonatomic ,strong) NSArray <ServerBadgeModel> *cert_badge_list;
/** 成就配置 */
@property (nonatomic ,strong) NSArray <ServerBadgeModel> *achievement_badge_list;


@end

@interface ServerRootMainModel : FetchModel

@property (nonatomic,strong)ServerRootListModel *server;
@property (nonatomic,strong)ServerRootConfigModel *config;
/** vip,成就配置 */
@property (nonatomic ,strong) ServerRootBadgeModel *badge;

@end

NS_ASSUME_NONNULL_END
