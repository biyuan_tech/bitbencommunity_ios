//
//  LoginServerModel.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/29.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol LoginServerModelAccountTopic<NSObject>

@end

@interface LoginServerModelAccountTopic :FetchModel

@property (nonatomic,copy)NSString *_id;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,assign)BOOL isLike;

@end


@interface LoginServerModelToDayCoinAccount : FetchModel

@property (nonatomic,copy)NSString *bbt;
@property (nonatomic,assign)BOOL is_first;

@end



@interface LoginServerModelAccount : FetchModel

@property (nonatomic,copy)NSString *_id;
@property (nonatomic,copy)NSString *last_login;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *password;
@property (nonatomic,copy)NSString *phone;
@property (nonatomic,copy)NSString *salt;
@property (nonatomic,copy)NSString *device_sn;          // 序列号
@property (nonatomic,copy)NSString *channel;            // 渠道
@property (nonatomic,copy)NSString *oauth_id;           // 三方登录的ID
@property (nonatomic,copy)NSString *oauth_sdk;          // 三方登录的平台
@property (nonatomic,copy)NSString *head_img;           // 头像
@property (nonatomic,copy)NSString *full_name;          // 姓名
@property (nonatomic,copy)NSString *sex;                // 性别
@property (nonatomic,copy)NSString *province;                // 省
@property (nonatomic,copy)NSString *city;                // 城市
@property (nonatomic,copy)NSString *birthday;                // 生日
@property (nonatomic,copy)NSString *profession;                // 职业
@property (nonatomic,copy)NSString *bank_card_id;                // 银行卡号
@property (nonatomic,copy)NSString *wechat_pay_id;                // wx支付
@property (nonatomic,copy)NSString *ali_pay_id;                // zfb支付
@property (nonatomic,copy)NSString *last_login_ip;                // 最后登录的IP
@property (nonatomic,copy)NSString *block_address;                // 区块链地址
@property (nonatomic,copy)NSString *my_invitation_code;                // 邀请码
@property (nonatomic,assign)NSInteger unread_message;               // 未读消息数量



@property (nonatomic,strong)NSArray<LoginServerModelAccountTopic> *topic_list;       /**< 我的关注*/

@property (nonatomic,assign)BOOL isFirst;           /**< 是否第一次三方登录*/
@property (nonatomic,copy)NSString *userSig;        /**< 腾讯直播sig*/
@property (nonatomic,strong)LoginServerModelToDayCoinAccount *today_coin;

@end


@interface LoginServerModelCount:FetchModel

@property (nonatomic,copy)NSString *_id;              // 用户Id
@property (nonatomic,assign)NSInteger article;        // 文章数
@property (nonatomic,assign)NSInteger attention;      // 我关注的人数
@property (nonatomic,assign)NSInteger fans;           // 粉丝数
@property (nonatomic,assign)NSInteger live;           // 直播数

@end

@interface LoginServerModelFinance:FetchModel
@property (nonatomic,copy)NSString *_id;
@property (nonatomic,copy)NSString *account_id;
@property (nonatomic,assign)NSInteger coin;
@property (nonatomic,assign)NSInteger diamond;

@end

@interface LoginServerModelUser:FetchModel
@property (nonatomic,copy)NSString *_id;
@property (nonatomic,copy)NSString *account_id;
@property (nonatomic,assign)CGFloat exp;
@property (nonatomic,assign)NSInteger level;// 用户的社区等级
@property (nonatomic,assign)NSInteger vip;
@property (nonatomic,copy)NSString *name;// 账号名
@property (nonatomic,copy)NSString *nickname;           // 昵称
@property (nonatomic,copy)NSString *self_introduction;  // 自我介绍
@property (nonatomic,copy)NSString *mail;
@property (nonatomic,copy)NSString *friends;
@property (nonatomic,assign)NSInteger energy;
/** vip (-1: 普通 0: 个人 1:产业 2:机构 3:官方 ) */
@property (nonatomic ,assign) NSInteger cert_badge;
/** 成就组 */
@property (nonatomic ,strong) NSArray <NSString *>*achievement_badge_list;
@end


#pragma mark - Main
@interface LoginServerModel : FetchModel

@property (nonatomic,strong)LoginServerModelAccount *account;
@property (nonatomic,strong)LoginServerModelCount *infoCount;
@property (nonatomic,strong)LoginServerModelFinance *finance;
@property (nonatomic,strong)LoginServerModelUser *user;

@end

NS_ASSUME_NONNULL_END


