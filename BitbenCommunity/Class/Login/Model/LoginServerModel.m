//
//  LoginServerModel.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/29.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LoginServerModel.h"
#import "BYIMManager.h"

@implementation LoginServerModelAccountTopic

@end

@implementation LoginServerModelAccount

- (void)setHead_img:(NSString *)head_img{
    _head_img = head_img;
}

@end

@implementation LoginServerModelCount


@end

@implementation LoginServerModelToDayCoinAccount

@end



@implementation LoginServerModelFinance

@end

@implementation LoginServerModelUser

- (NSString *)nickname{
    LoginServerModelAccount *account = LOGIN_MODEL.account;
    if (!_nickname.length) {
        return account.name.length ? account.name : @"昵称未获取到";
    }
    return _nickname;
}

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"friends":@"friend"};
}


@end

@implementation LoginServerModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"infoCount":@"count"};
}

@end





