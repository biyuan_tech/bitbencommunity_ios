//
//  AccountModel.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AccountModel.h"
#import "NetworkAdapter+Login.h"

@implementation AccountModel

+(instancetype)sharedAccountModel{
    static AccountModel *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[AccountModel alloc] init];
    });
    return _sharedAccountModel;
}

#pragma mark - 1.判断是否登录
- (BOOL)hasLoggedIn{
    return ([AccountModel sharedAccountModel].account_id.length ? YES : NO);
}

#pragma mark - 自动登录
-(void)autoLoginWithBlock:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSString *loginUserType = [Tool userDefaultGetWithKey:LoginType];
    NSString *loginAccount = [Tool userDefaultGetWithKey:LoginAccount];
    NSString *loginPwd = [Tool userDefaultGetWithKey:LoginPassword];

    if (loginUserType.length && loginAccount.length && loginPwd.length){
        thirdLoginType loginType = (thirdLoginType)[loginUserType integerValue];
        [AccountModel sharedAccountModel].loginType = loginType;
        [[NetworkAdapter sharedAdapter] loginThirdType:loginType account:loginAccount pwd:loginPwd block:^(BOOL isSuccessed, LoginServerModelAccount *serverAccountModel) {
            if (!weakSelf){
                return ;
            }
            if (isSuccessed){
                if (block){
                    block(YES);
                }
            }
        }];
    }
}

#pragma mark - 自动登录
- (void)authorizeWithCompletionHandler:(void(^)(BOOL successed))handler{
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){
        handler(YES);
    } else {
        NSString *pwd = [Tool userDefaultGetWithKey:LoginPassword];
        NSString *account = [Tool userDefaultGetWithKey:LoginAccount];
        NSString *loginType = [Tool userDefaultGetWithKey:LoginType];
        if (pwd.length && account.length && loginType.length){
            [[AccountModel sharedAccountModel] autoLoginWithBlock:^(BOOL isSuccessed) {
                handler(isSuccessed);
            }];
        } else {
            [StatusBarManager statusBarHidenWithText:@"请先进行登录"];
            LoginRootViewController *loginMainViewController = [[LoginRootViewController alloc]init];
            [loginMainViewController loginSuccess:^(BOOL success) {
                if (success){
                    [loginMainViewController dismissViewControllerAnimated:YES completion:NULL];
                    handler(YES);
                } else {
                    handler(NO);
                    [StatusBarManager statusBarHidenWithText:@"取消登录&登录失败"];
                }
            }];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginMainViewController];
            [[BYTabbarViewController sharedController] presentViewController:nav animated:YES completion:NULL];
        }
    }
}

#pragma mark - 无UI效果的登录
-(void)autoLoginWithNoneWindowBlock:(void(^)(BOOL isLogin))handler{
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){
        handler(YES);
    } else {
        NSString *pwd = [Tool userDefaultGetWithKey:LoginPassword];
        NSString *account = [Tool userDefaultGetWithKey:LoginAccount];
        NSString *loginType = [Tool userDefaultGetWithKey:LoginType];
        if (pwd.length && account.length && loginType.length){
            [[AccountModel sharedAccountModel] autoLoginWithBlock:^(BOOL isSuccessed) {
                handler(isSuccessed);
            }];
        } else {
            handler(NO);
        }
    }
}

#pragma mark - 退出登录
-(void)logoutManagerBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_logout requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }

        if (isSucceeded){
            [MTAManager profileSignOut];
            
            // 单例置空
            [AccountModel sharedAccountModel].loginServerModel = nil;
            [AccountModel sharedAccountModel].token = @"";
            [AccountModel sharedAccountModel].account_id = @"";
            
            // userdefault 清空
            [Tool userDefaultDelegtaeWithKey:LoginType];
            [Tool userDefaultDelegtaeWithKey:LoginAccount];
            [Tool userDefaultDelegtaeWithKey:LoginPassword];
            
            // 登录退出后发出通知
            NSDictionary *resultDic = @{@"loginStatus":@"0"};
            [[NSNotificationCenter defaultCenter]postNotificationName:LoginNotificationWithLoginSuccessed object:nil userInfo:resultDic];
            
            if(block){
                block();
            }
        }
    }];
}

-(BOOL)isShenhe{
//    return YES;
    return [AccountModel sharedAccountModel].serverModel.config.is_live_module_hidden;
}

-(NSMutableArray *)loginServerMutableArr{
    if(!_loginServerMutableArr){
        _loginServerMutableArr = [NSMutableArray array];
    }
    return _loginServerMutableArr;
}

@end
