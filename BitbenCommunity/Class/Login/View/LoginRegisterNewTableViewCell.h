//
//  LoginRegisterNewTableViewCell.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

@interface LoginRegisterNewTableViewCell : PDBaseTableViewCell

-(void)actionClickWithLeftButtonBlock:(void(^)())block;
-(void)actionClickWithRightButtonBlock:(void(^)())block;

@end
