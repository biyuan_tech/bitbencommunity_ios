//
//  LoginThirdLoginTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/23.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LoginThirdLoginTableViewCell.h"

static char actionWeChatButtonClickBlockKey;
@interface LoginThirdLoginTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *weChatButton;
@end

@implementation LoginThirdLoginTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"AFAFAF"];
    self.titleLabel.text = @"其他方式登录";
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    self.weChatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.weChatButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.weChatButton setImage:[UIImage imageNamed:@"icon_login_wechat"] forState:UIControlStateNormal];
    [self.weChatButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionWeChatButtonClickBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.weChatButton];
    
    [self frameSetting];
}

-(void)frameSetting{
    self.titleLabel.frame = CGRectMake(0, LCFloat(36), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    self.weChatButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(40)) / 2., CGRectGetMaxY(self.titleLabel.frame) + LCFloat(20), LCFloat(40), LCFloat(40));
}


-(void)actionWeChatButtonClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionWeChatButtonClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(36);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]];
    cellHeight += LCFloat(20);
    cellHeight += LCFloat(40);
    return cellHeight;
}

@end
