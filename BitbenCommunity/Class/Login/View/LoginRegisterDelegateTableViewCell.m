//
//  LoginRegisterDelegateTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/23.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LoginRegisterDelegateTableViewCell.h"

@interface LoginRegisterDelegateTableViewCell()
@property (nonatomic,strong)UIButton *checkButton;
@property (nonatomic,strong)UILabel *delegateLabel1;
@property (nonatomic,strong)UILabel *delegateLabel2;
@property (nonatomic,strong)UILabel *delegateLabel3;
@property (nonatomic,strong)UIButton *delegateButton;

@end

static char actionClickWithCheckButtonBlockKey;
static char actionClickWithDelegateButtonBlockKey;
@implementation LoginRegisterDelegateTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    __weak typeof(self)weakSelf = self;
    
    self.checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.checkButton.backgroundColor = [UIColor clearColor];
    self.checkButton.frame = CGRectMake(LCFloat(8), LCFloat(11), LCFloat(33), LCFloat(33));
    [self.checkButton setImage:[UIImage imageNamed:@"icon_center_check_nor"] forState:UIControlStateNormal];
    [self.checkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithCheckButtonBlockKey);
        if (block){
            block();
        }
    }];
    
    [self addSubview:self.checkButton];
    
    self.delegateLabel1 = [GWViewTool createLabelFont:@"12" textColor:@"AFAFAF"];
    self.delegateLabel1.text = @"同意《";
    CGSize delegateSize1 = [Tool makeSizeWithLabel:self.delegateLabel1];
    self.delegateLabel1.frame = CGRectMake(CGRectGetMaxX(self.checkButton.frame), LCFloat(17), delegateSize1.width, delegateSize1.height);
    [self addSubview:self.delegateLabel1];
    
    self.delegateLabel2 = [GWViewTool createLabelFont:@"12" textColor:@"ED5745"];
    [self addSubview:self.delegateLabel2];
    self.delegateLabel2.text = @"用户服务协议";
    CGSize delegateLabelSize2 = [Tool makeSizeWithLabel:self.delegateLabel2];
    self.delegateLabel2.frame = CGRectMake(CGRectGetMaxX(self.delegateLabel1.frame), self.delegateLabel1.orgin_y, delegateLabelSize2.width, delegateLabelSize2.height);
    
    self.delegateLabel3 = [GWViewTool createLabelFont:@"12" textColor:@"AFAFAF"];
    self.delegateLabel3.text = @"》";
    CGSize delegateSize3 = [Tool makeSizeWithLabel:self.delegateLabel3];
    self.delegateLabel3.frame = CGRectMake(CGRectGetMaxX(self.delegateLabel2.frame), self.delegateLabel2.orgin_y, delegateSize3.width, delegateSize3.height);
    [self addSubview:self.delegateLabel3];
    
    self.delegateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.delegateButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.delegateButton];
    [self.delegateButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithDelegateButtonBlockKey);
        if (block){
            block();
        }
    }];
    self.delegateButton.frame = CGRectMake(self.delegateLabel1.orgin_x, 0, CGRectGetMaxX(self.delegateLabel3.frame), [LoginRegisterDelegateTableViewCell calculationCellHeight]);
}

-(void)freameInfo{
    
}

-(void)setHasSelected:(BOOL)hasSelected{
    _hasSelected = hasSelected;
    if (hasSelected){
        [self.checkButton setImage:[UIImage imageNamed:@"icon_center_check_hlt"] forState:UIControlStateNormal];
    } else {
        [self.checkButton setImage:[UIImage imageNamed:@"icon_center_check_nor"] forState:UIControlStateNormal];
    }
}

-(void)checkButtonStatus{
    __weak typeof(self)weakSelf = self;
    [Tool clickZanWithView:self.checkButton block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.hasSelected){
            [strongSelf.checkButton setImage:[UIImage imageNamed:@"icon_center_check_hlt"] forState:UIControlStateNormal];
        } else {
            [strongSelf.checkButton setImage:[UIImage imageNamed:@"icon_center_check_nor"] forState:UIControlStateNormal];
        }
    }];
}

-(void)actionClickWithCheckButtonBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithCheckButtonBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithDelegateButtonBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithDelegateButtonBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
+(CGFloat)calculationCellHeight{
    return LCFloat(11) + LCFloat(33) + LCFloat(11);
}

-(void)delegateBtnClickBlock:(void(^)())block{
    
}
@end
