//
//  LoginIconTableViewCell.m
//  BBFinance
//
//  Created by 裴烨烽 on 2018/7/18.
//  Copyright © 2018年 川普投资. All rights reserved.
//

#import "LoginIconTableViewCell.h"

@interface LoginIconTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)PDImageView *iconSubImgView;

@end

@implementation LoginIconTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

#pragma mar k- createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(118)) / 2., LCFloat(8), LCFloat(118), LCFloat(118));
    self.iconImgView.center_x = kScreenBounds.size.width / 2.;
    self.iconImgView.image = [UIImage imageNamed:@"icon_login_avatar_bg"];
    [self addSubview:self.iconImgView];
    
    self.iconSubImgView = [[PDImageView alloc]init];
    self.iconSubImgView.backgroundColor = [UIColor redColor];
    self.iconSubImgView.frame = CGRectMake(0, 0, LCFloat(80), LCFloat(80));
    self.iconSubImgView.center = self.iconImgView.center;
    self.iconSubImgView.layer.cornerRadius = self.iconSubImgView.size_width / 2.;
    self.iconSubImgView.clipsToBounds = YES;
    [self addSubview:self.iconSubImgView];
}

-(void)setTransferAvatarImg:(NSString *)transferAvatarImg{
    _transferAvatarImg = transferAvatarImg;
    
    
    [self.iconSubImgView uploadMainImageWithURL:transferAvatarImg placeholder:nil imgType:PDImgTypeNormal callback:NULL];
}

+(CGFloat)calculationCellHeight{
    return LCFloat(118) + 2 * LCFloat(8);
}

@end
