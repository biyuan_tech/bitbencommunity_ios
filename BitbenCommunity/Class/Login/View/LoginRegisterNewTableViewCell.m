//
//  LoginRegisterNewTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LoginRegisterNewTableViewCell.h"

static char actionClickWithLeftButtonBlockKey;
static char actionClickWithRightButtonBlockKey;
@interface LoginRegisterNewTableViewCell()
@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)UIButton *rightButton;
@end

@implementation LoginRegisterNewTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithLeftButtonBlockKey);
        if (block){
            block();
        }
    }];
    [self.leftButton setTitle:@"注册新用户" forState:UIControlStateNormal];
    [self.leftButton setTitleColor:[UIColor colorWithCustomerName:@"ED5745"] forState:UIControlStateNormal];
    [self addSubview:self.leftButton];
     self.leftButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    
    CGSize leftSize = [@"注册新用户" sizeWithCalcFont:self.leftButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 40)];
    self.leftButton.frame = CGRectMake(LCFloat(11), 0, leftSize.width + 2 * LCFloat(11), LCFloat(40));
    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.backgroundColor = [UIColor clearColor];
    [self.rightButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithRightButtonBlockKey);
        if (block){
            block();
        }
    }];
    [self.rightButton setTitle:@"找回密码" forState:UIControlStateNormal];
    self.rightButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    [self.rightButton setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
    [self addSubview:self.rightButton];
    self.rightButton.frame = CGRectMake(kScreenBounds.size.width - self.leftButton.size_width - LCFloat(11), 0, self.leftButton.size_width, LCFloat(40));
    
}


-(void)actionClickWithLeftButtonBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithLeftButtonBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithRightButtonBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithRightButtonBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]];
    cellHeight += 2 * LCFloat(7);
    return cellHeight;
}
@end
