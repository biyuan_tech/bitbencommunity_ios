//
//  LoginRegisterLogoTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/23.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LoginRegisterLogoTableViewCell.h"

@interface LoginRegisterLogoTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@end

@implementation LoginRegisterLogoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.image = [UIImage imageNamed:@"icon_main_logo"];
    self.iconImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(74)) / 2., LCFloat(40), LCFloat(74), LCFloat(74));
    [self addSubview:self.iconImgView];
}

+(CGFloat)calculationCellHeight{
    return LCFloat(40) + LCFloat(74) + LCFloat(23);
}

@end
