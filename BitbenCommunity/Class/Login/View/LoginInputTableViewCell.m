//
//  LoginInputTableViewCell.m
//  BBFinance
//
//  Created by 裴烨烽 on 2018/7/10.
//  Copyright © 2018年 川普投资. All rights reserved.
//

#import "LoginInputTableViewCell.h"

static char actionClickWithSMSBtnKey;
@interface LoginInputTableViewCell()<UITextFieldDelegate>
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UIButton *actionShowPwdButton;          /**< 是否显示密码*/
@property (nonatomic,strong)UIView *lineView;                       /**< 下划线*/
@property (nonatomic,strong)UIButton *eyeBtn;                       /**< 眼睛按钮*/
@property (nonatomic,strong)PDCountDownButton *daojishiBtn;         /**< 倒计时按钮*/

@end

@implementation LoginInputTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImgView];
    
    // 2. 创建输入框
    self.inputTextField = [[UITextField alloc]init];
    self.inputTextField.backgroundColor = [UIColor clearColor];
    self.inputTextField.userInteractionEnabled = YES;
    self.inputTextField.delegate = self;
    self.inputTextField.font = [UIFont systemFontOfCustomeSize:14.];
    [self.inputTextField addTarget:self action:@selector(textFieldWillChange) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.inputTextField];
    
    __weak typeof(self)weakSelf = self;
    [self.inputTextField showBarCallback:^(UITextField *textField, NSInteger buttonIndex, UIButton *button) {
        if (!weakSelf){
            return ;
        }
    
        if ([textField isFirstResponder]){
            [textField resignFirstResponder];
        }
    }];
    
    // 3. 倒计时
    if (!self.daojishiBtn){
        self.daojishiBtn = [[PDCountDownButton alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(20) - LCFloat(90), 0, LCFloat(90), LCFloat(34)) daojishi:60 withBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            
            void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithSMSBtnKey);
            if(block){
                block();
            }
        }];
    }
    self.daojishiBtn.hidden = YES;
    [self addSubview:self.daojishiBtn];
    
    // 4. line
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor hexChangeFloat:@"DCDCDC"];
    [self addSubview:self.lineView];
    
    // 5. 添加眼睛
    self.eyeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.eyeBtn.backgroundColor = [UIColor clearColor];
    [self.eyeBtn setImage:[UIImage imageNamed:@"icon_login_pwd_eye_close"] forState:UIControlStateNormal];
    self.eyeBtn.selected = NO;
    [self.eyeBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        self.eyeBtn.selected = !self.eyeBtn.selected;
        if (weakSelf.eyeBtn.selected){
            [weakSelf.eyeBtn setImage:[UIImage imageNamed:@"icon_login_pwd_eye_open"] forState:UIControlStateNormal];
            self.inputTextField.secureTextEntry = NO;
        } else {
            [weakSelf.eyeBtn setImage:[UIImage imageNamed:@"icon_login_pwd_eye_close"] forState:UIControlStateNormal];
            self.inputTextField.secureTextEntry = YES;
        }
    }];
    [self addSubview:self.eyeBtn];
}

-(void)setTransferType:(LoginInputType)transferType{
    _transferType = transferType;
    // title
    if (transferType == LoginInputTypePhone){
        self.iconImgView.image = [UIImage imageNamed:@"icon_login_phone"];
        self.iconImgView.frame = CGRectMake(LCFloat(28), ([LoginInputTableViewCell calculationCellHeight] - LCFloat(21)) / 2., LCFloat(14), LCFloat(21));
        self.inputTextField.placeholder = @"请输入手机号码";
        self.daojishiBtn.hidden = YES;
        self.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
    } else if (transferType == LoginInputTypeSMS){
        self.iconImgView.image = [UIImage imageNamed:@"icon_login_sms"];
        self.iconImgView.frame = CGRectMake(LCFloat(25), LCFloat(32), LCFloat(17), LCFloat(21));
        
        self.inputTextField.placeholder = @"请输入手机验证码";
        self.daojishiBtn.hidden = NO;
        self.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
        
    } else if (transferType == LoginInputTypePwd || transferType == LoginInputTypePwdAgain){
        
        self.iconImgView.image = [UIImage imageNamed:@"icon_login_pwd"];
        self.iconImgView.frame = CGRectMake(LCFloat(27), ([LoginInputTableViewCell calculationCellHeight] - LCFloat(21)) / 2., LCFloat(16), LCFloat(21));
        self.inputTextField.placeholder = @"请输入密码";
        self.daojishiBtn.hidden = YES;
        self.inputTextField.clearButtonMode = UITextFieldViewModeAlways;
        self.inputTextField.keyboardType = UIKeyboardTypeEmailAddress;
        self.inputTextField.secureTextEntry = YES;
        if(transferType == LoginInputTypePwdAgain){
            self.inputTextField.placeholder = @"请再次输入密码";
        }
    } else if (transferType == LoginInputTypeNick){
        self.inputTextField.placeholder = @"请输入昵称";
        self.daojishiBtn.hidden = YES;
        self.inputTextField.keyboardType = UIKeyboardTypeDefault;
        self.inputTextField.secureTextEntry = NO;
    } else if (transferType == LoginInputTypeYaoqing){
        self.iconImgView.image = [UIImage imageNamed:@"icon_login_sms"];
        self.iconImgView.frame = CGRectMake(LCFloat(25), LCFloat(32), LCFloat(17), LCFloat(21));
        
        
        self.inputTextField.placeholder = @"请输入邀请码(选填)";
        self.daojishiBtn.hidden = YES;
        self.inputTextField.keyboardType = UIKeyboardTypeDefault;
        self.inputTextField.secureTextEntry = NO;
    } else {
        self.daojishiBtn.hidden = YES;
        
    }
    
    // 2. input
    self.inputTextField.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(20),  LCFloat(2.5), kScreenBounds.size.width - 2 * LCFloat(20) - CGRectGetMaxX(self.iconImgView.frame), LCFloat(40));
    self.inputTextField.center_y = self.iconImgView.center_y;
    
    // 3.line
    self.lineView.frame = CGRectMake(LCFloat(15), [LoginInputTableViewCell calculationCellHeight] - 1, kScreenBounds.size.width - 2 * LCFloat(15), .5f);
    
    // 4. 倒计时
    self.daojishiBtn.frame = CGRectMake(kScreenBounds.size.width - LCFloat(42) - self.daojishiBtn.size_width, self.transferCellHeight - LCFloat(10) - self.daojishiBtn.size_height, self.daojishiBtn.size_width, self.daojishiBtn.size_height);
    self.daojishiBtn.center_y = self.inputTextField.center_y;
    
    if (transferType == LoginInputTypePwd){
        self.inputTextField.size_width -= (LCFloat(44) + LCFloat(11));
        self.eyeBtn.frame = CGRectMake(CGRectGetMaxX(self.inputTextField.frame) + LCFloat(11), LCFloat(11), LCFloat(44), LCFloat(44));
        self.eyeBtn.center_y = self.inputTextField.center_y;
        self.eyeBtn.hidden = NO;
    } else {
        self.eyeBtn.hidden = YES;
    }
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]){
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if (self.transferType == LoginInputTypePwd){                // 用户
        if (toBeString.length > 18){
            textField.text = [toBeString substringToIndex:18];
            return NO;
        }
        //        if (toBeString.length >= 5 && toBeString.length <= 20){
        //            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
        //                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:YES];
        //            }
        //        } else {
        //            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
        //                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:NO];
        //            }
        //        }
    } else if (self.transferType == LoginInputTypePhone){           // 手机号
        if (toBeString.length > 11){
            textField.text = [toBeString substringToIndex:11];
            return NO;
        }
        //        if (toBeString.length == 11 && [TSTool validateMobile:toBeString]){
        //            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
        //                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:YES];
        //            }
        //        } else {
        //            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
        //                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:NO];
        //            }
        //        }
    } else if (self.transferType == LoginInputTypeSMS){  // 验证码
        if (toBeString.length > 6){
            textField.text = [toBeString substringToIndex:6];
            return NO;
        }
        //        else if (toBeString.length == 4){
        //            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
        //                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:YES];
        //            }
        //        }
    } else if (self.transferType == LoginInputTypeNick){
        if (toBeString.length > 15){
            textField.text = [toBeString substringToIndex:15];
            return NO;
        }
    }
    return YES;
}

-(void)textFieldWillChange{
    BOOL success = NO;
    if (self.transferType == LoginInputTypePhone){           // 【手机号】
        if ((self.inputTextField.text.length == 11 && ([Tool validateMobile:self.inputTextField.text])) || [self.inputTextField.text isEqualToString:@"10000000000"]){
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.transferType == LoginInputTypePwd){              // 【密码】
        if (self.inputTextField.text.length >= 6 && self.inputTextField.text.length <= 18){
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.transferType == LoginInputTypeSMS){          // 【验证码】
        if (self.inputTextField.text.length == 4){
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.transferType == LoginInputTypeNick){        // 【昵称】
        if (self.inputTextField.text.length >= 2){
            success = YES;
        } else {
            success = NO;
        }
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
        [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:success];
    }
}


-(void)startTimer{
    [self.daojishiBtn startTimer];
}

-(void)actionClickWithSMSBtn:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithSMSBtnKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(77);
    return cellHeight;
}


@end
