//
//  LoginRegisterViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LoginRegisterViewController.h"
#import "LoginRegisterLogoTableViewCell.h"
#import "LoginRegisterDelegateTableViewCell.h"
#import "LoginInputTableViewCell.h"
#import "NetworkAdapter+Login.h"

@interface LoginRegisterViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UITextField *phoneTextField;
    UITextField *smsTextField;
    UITextField *pwdTextField;
    UITextField *yaoqingTextField;
    LoginInputTableViewCell *smsCell;
    BOOL hasCheck;
}
@property (nonatomic,strong)NSArray *registerArr;
@property (nonatomic,strong)UITableView *registerTableView;
@end

@implementation LoginRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createNotifi];                // 键盘代理
}

#pragma mark - pageSetting
-(void)pageSetting{
    if (self.transferType == LoginRegisterTypeBinding){
        self.barMainTitle = @"完善绑定信息";
    } else if (self.transferType == LoginRegisterTypeRegister){
        self.barMainTitle = @"新用户注册";
    }
}

-(void)arrayWithInit{
    if (self.transferType == LoginRegisterTypeBinding){
        self.registerArr = @[@[@"logo"],@[@"手机号",@"短信号",@"密码"],@[@"协议"],@[@"注册"],@[@"绑定账户"]];
    } else if (self.transferType == LoginRegisterTypeRegister){
        self.registerArr = @[@[@"logo"],@[@"手机号",@"短信号",@"密码"],@[@"协议"],@[@"注册"],@[@"已有账户"]];
    }
    hasCheck = YES;
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.registerTableView){
        self.registerTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.registerTableView.dataSource = self;
        self.registerTableView.delegate = self;
        [self.view addSubview:self.registerTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.registerArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.registerArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"logo" sourceArr:self.registerArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        LoginRegisterLogoTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[LoginRegisterLogoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"手机号" sourceArr:self.registerArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        LoginInputTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[LoginInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        if (indexPath.row == 0){
            cellWithRowTwo.transferType = LoginInputTypePhone;
            phoneTextField = cellWithRowTwo.inputTextField;
        } else if (indexPath.row == 1){
            cellWithRowTwo.transferType = LoginInputTypeSMS;
            smsTextField = cellWithRowTwo.inputTextField;
            smsCell = cellWithRowTwo;
            __weak typeof(self)weakSelf = self;
            [cellWithRowTwo actionClickWithSMSBtn:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (strongSelf.transferType == LoginRegisterTypeBinding){
                    [strongSelf sendRequestToGetSMSCodeWithBinding];
                } else if (strongSelf.transferType == LoginRegisterTypeRegister){
                    [strongSelf sendRequestToGetSmsCode];
                }
            }];
            
        } else if (indexPath.row == 2){
            cellWithRowTwo.transferType = LoginInputTypePwd;
            pwdTextField = cellWithRowTwo.inputTextField;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"邀请码" sourceArr:self.registerArr]){
            cellWithRowTwo.transferType = LoginInputTypeYaoqing;
            yaoqingTextField = cellWithRowTwo.inputTextField;
        }
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"协议" sourceArr:self.registerArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        LoginRegisterDelegateTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[LoginRegisterDelegateTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.hasSelected = hasCheck;
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr actionClickWithCheckButtonBlock:^{
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->hasCheck = !strongSelf->hasCheck;
            cellWithRowThr.hasSelected = hasCheck;
            [cellWithRowThr checkButtonStatus];
        }];
        
        [cellWithRowThr actionClickWithDelegateButtonBlock:^{
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDWebViewController *webViewController = [[PDWebViewController alloc]init];
            [webViewController webDirectedWebUrl:register_delegate];
            [strongSelf.navigationController pushViewController:webViewController animated:YES];
        }];
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"注册" sourceArr:self.registerArr]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        GWButtonTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        if (self.transferType == LoginRegisterTypeBinding){
            cellWithRowFour.transferTitle = @"前往绑定";
        } else if (self.transferType == LoginRegisterTypeRegister){
            cellWithRowFour.transferTitle = @"注册";
        }
        
        __weak typeof(self)weakSlef = self;
        [cellWithRowFour buttonClickManager:^{
            if (!weakSlef){
                return ;
            }
            __strong typeof(weakSlef)strongSelf = weakSlef;
            if (strongSelf.transferType == LoginRegisterTypeBinding){
                [strongSelf sendRequestToBindingWechatManager];
            } else if (strongSelf.transferType == LoginRegisterTypeRegister){
                [strongSelf sendRequestToRegister];
            }
        }];
        return cellWithRowFour;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"已有账户" sourceArr:self.registerArr]){
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        GWNormalTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFiv.transferCellHeight = cellHeight;
        cellWithRowFiv.transferTitle = @"已有账户，立即登录";
        cellWithRowFiv.titleLabel.font = [UIFont fontWithCustomerSizeName:@"12"];
        cellWithRowFiv.titleLabel.textColor = [UIColor hexChangeFloat:@"ED5745"];
        cellWithRowFiv.textAlignmentCenter = YES;
        return cellWithRowFiv;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"绑定账户" sourceArr:self.registerArr]){
        static NSString *cellIdentifyWithRowSix = @"cellIdentifyWithRowSix";
        UITableViewCell *cellWithRowSix = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSix];
        if (!cellWithRowSix){
            cellWithRowSix = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSix];
            cellWithRowSix.selectionStyle = UITableViewCellStyleDefault;
        }
        return cellWithRowSix;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"logo" sourceArr:self.registerArr]){
        return [LoginRegisterLogoTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"手机号" sourceArr:self.registerArr]){
        return [LoginInputTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"协议" sourceArr:self.registerArr]){
        return [LoginRegisterDelegateTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"注册" sourceArr:self.registerArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"已有账户" sourceArr:self.registerArr]){
        return [GWNormalTableViewCell calculationCellHeight];
    }
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"已有账户" sourceArr:self.registerArr]){
        for (UIViewController *controller in self.navigationController.childViewControllers){
            if ([controller isKindOfClass:[LoginRootViewController class]]){
                // 埋点
                [MTAManager event:MTATypeRegisterPageGotoLogin params:nil];
                
                [self.navigationController popToViewController:controller animated:YES];
                return;
            }
        }
    }
}

#pragma mark - Interface
#pragma mark 注册
-(void)sendRequestToRegister{
    NSString *error = @"";
    if (!phoneTextField.text.length){
        error = @"请输入手机号码";
    } else if (!smsTextField.text.length){
        error = @"请输入手机获取到的验证码";
    } else if (!pwdTextField.text.length){
        error = @"请输入密码";
    } else if (!hasCheck){
        error = @"请先同意注册协议";
    }
    if (error.length){
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }

    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginRegisterWithPhoneNumber:phoneTextField.text password:pwdTextField.text smsCode:smsTextField.text yaoqing:yaoqingTextField.text block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [StatusBarManager statusBarHidenWithText:@"注册成功"];
            //注册好后进行登录
            [strongSelf loginManagerWithType:thirdLoginTypeNative Account:phoneTextField.text pwd:pwdTextField.text];
        }
    }];
}

#pragma mark - dismiss
-(void)dismissWithLoginSuccessed{
    for (UIViewController *controller in self.navigationController.childViewControllers){
        if ([controller isKindOfClass:[LoginRootViewController class]]){
            LoginRootViewController *loginViewController = (LoginRootViewController *)controller;
            [loginViewController actionNavLoginSuccessDismiss];
        }
    }
}

#pragma mark 发送验证码
-(void)sendRequestToGetSmsCode{
    NSString *error = @"";
    if (!phoneTextField.text.length){
        error = @"请输入手机号码";
    }
    if (error.length){
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginSendSMSCode:phoneTextField.text block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
             [StatusBarManager statusBarHidenWithText:@"验证码发送成功"];
            [strongSelf->smsCell startTimer];
        } else {
            [StatusBarManager statusBarHidenWithText:@"验证码发送失败，请稍后再试"];
        }
    }];
}

#pragma mark 发送验证码 【微信绑定时使用】
-(void)sendRequestToGetSMSCodeWithBinding{
    NSString *error = @"";
    if (!phoneTextField.text.length){
        error = @"请输入手机号码";
    }
    if (error.length){
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] sendRequestToSendSMSCode:phoneTextField.text statuas:SMSCodeType2 block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [StatusBarManager statusBarHidenWithText:@"验证码发送成功"];
            [strongSelf->smsCell startTimer];
        } else {
            [StatusBarManager statusBarHidenWithText:@"验证码发送失败，请稍后再试"];
        }
    }];
}

#pragma mark - 注册好后直接登录
-(void)loginManagerWithType:(thirdLoginType)type Account:(NSString *)account pwd:(NSString *)pwd{
    __weak typeof(self)weakSelf = self;
    [StatusBarManager statusBarHidenWithText:@"正在登录中……"];
    [[NetworkAdapter sharedAdapter] loginThirdType:type account:account pwd:pwd block:^(BOOL isSuccessed, LoginServerModelAccount *serverAccountModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [StatusBarManager statusBarHidenWithText:@"登录成功"];
            [strongSelf dismissWithLoginSuccessed];
        }
    }];
}

#pragma mark - 进行绑定
-(void)sendRequestToBindingWechatManager{
    NSString *error = @"";
    if (!phoneTextField.text.length){
        error = @"请输入手机号码";
    } else if (!smsTextField.text.length){
        error = @"请输入手机获取到的验证码";
    } else if (!pwdTextField.text.length){
        error = @"请输入密码";
    } else if (!hasCheck){
        error = @"请先同意注册协议";
    }
    if (error.length){
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginRegisterWithPhoneNumber:phoneTextField.text password:pwdTextField.text smsCode:smsTextField.text channel:thirdLoginTypeWechat authToken:self.transferAuthToken yaoqing:yaoqingTextField.text block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [StatusBarManager statusBarHidenWithText:@"绑定成功"];
            // 绑定好后进行登录
            [strongSelf loginManagerWithType:thirdLoginTypeWechat Account:self.transferAuthToken pwd:@""];
        }
    }];
}


#pragma mark - 键盘代理
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.registerTableView.frame = newTextViewFrame;
    [UIView commitAnimations];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.registerTableView.frame = self.view.bounds;
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (phoneTextField && phoneTextField.isFirstResponder){
        [phoneTextField resignFirstResponder];
    } else if (smsTextField && smsTextField.isFirstResponder){
        [smsTextField resignFirstResponder];
    } else if (pwdTextField && pwdTextField.isFirstResponder){
        [pwdTextField resignFirstResponder];
    }
}

@end
