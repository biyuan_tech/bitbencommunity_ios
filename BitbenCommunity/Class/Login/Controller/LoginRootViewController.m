//
//  LoginRootViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LoginRootViewController.h"
#import "LoginIconTableViewCell.h"              // 头像Cell
#import "LoginInputTableViewCell.h"             // 输入框
#import "LoginRegisterNewTableViewCell.h"       // 注册新用户
#import "LoginThirdLoginTableViewCell.h"        // 微信登录
#import "LoginRegisterViewController.h"         // 注册
#import "LoginFindPwdViewController.h"          // 找回密码
#import "NetworkAdapter+Login.h"
#import "ShareSDKManager.h"

#import "OSSManager.h"

static char loginSuccessKey;
@interface LoginRootViewController ()<UITableViewDelegate,UITableViewDataSource,LoginInputTableViewCellDelegate>{
    UITextField *phoneTextField;
    UITextField *pwdTextField;
    GWButtonTableViewCell *loginBtn;
    LoginInputTableViewCell *phoneCell;
    LoginInputTableViewCell *pwdCell;
}
@property (nonatomic,strong)NSArray *loginArr;
@property (nonatomic,strong)UITableView *loginTableView;
@property (nonatomic,strong)UILabel *delegateLabel;

@end

@implementation LoginRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapManager)];
//    [self.view addGestureRecognizer:tap];
    [self createDelegateLabel];
}

#pragma mark - 创建底部label
-(void)createDelegateLabel{
    if (!self.delegateLabel){
        self.delegateLabel = [GWViewTool createLabelFont:@"12" textColor:@"AFAFAF"];
        self.delegateLabel.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(24) - [BYTabbarViewController sharedController].navBarHeight - (IS_iPhoneX?LCFloat(34):0) - 50, kScreenBounds.size.width, 50);
        self.delegateLabel.text = @"登录注册即视为同意币本《用户服务协议》";
        self.delegateLabel.textAlignment = NSTextAlignmentCenter;
        self.delegateLabel.attributedText = [Tool rangeLabelWithContent:@"登录注册即视为同意币本《用户服务协议》" hltContentArr:@[@"《用户服务协议》"] hltColor:[UIColor hexChangeFloat:@"ED4A45"] normolColor:[UIColor hexChangeFloat:@"AFAFAF"]];
        __weak typeof(self)weakSelf = self;
  
        [self.view addSubview:self.delegateLabel];
        
        UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        actionButton.frame = self.delegateLabel.frame;
        [self.view addSubview:actionButton];
        
        [actionButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                         return ;
                     }
                     __strong typeof(weakSelf)strongSelf = weakSelf;
                     PDWebViewController *webViewController = [[PDWebViewController alloc]init];
                     [webViewController webDirectedWebUrl:@"http://m.bitben.com/statement"];
                     [strongSelf.navigationController pushViewController:webViewController animated:YES];
        }];
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"用户登录";
}

-(void)arrayWithInit{
    if ([ShareSDKManager hasSetupWechat]){
        if ([AccountModel sharedAccountModel].isShenhe) {
            self.loginArr = @[@[@"头像"],@[@"账号",@"密码"],@[@"找回密码"],@[@"登录"]];
            return;
        }
        self.loginArr = @[@[@"头像"],@[@"账号",@"密码"],@[@"找回密码"],@[@"登录"],@[@"微信登录"]];
    } else {
        self.loginArr = @[@[@"头像"],@[@"账号",@"密码"],@[@"找回密码"],@[@"登录"]];
    }
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.loginTableView){
        self.loginTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.loginTableView.dataSource = self;
        self.loginTableView.delegate = self;
        [self.view addSubview:self.loginTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.loginArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.loginArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.loginArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        LoginIconTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[LoginIconTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        
        NSString *loginAvatar = [Tool userDefaultGetWithKey:LoginAvatar];
        cellWithRowOne.transferAvatarImg =  [NSString stringWithFormat:@"%@%@",imageBaseUrl,loginAvatar];
        
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"账号" sourceArr:self.loginArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        LoginInputTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[LoginInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.delegate = self;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        if (indexPath.row == 0){
            cellWithRowTwo.transferType = LoginInputTypePhone;
            phoneTextField = cellWithRowTwo.inputTextField;
            phoneCell = cellWithRowTwo;
        } else {
            cellWithRowTwo.transferType = LoginInputTypePwd;
            pwdTextField = cellWithRowTwo.inputTextField;
            pwdCell = cellWithRowTwo;
        }
        
        
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"登录" sourceArr:self.loginArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"登录";
        loginBtn = cellWithRowThr;
        [self btnStatus];
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToLogin];
        }];
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"找回密码" sourceArr:self.loginArr]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        LoginRegisterNewTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[LoginRegisterNewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        __weak typeof(self)weakSelf = self;
        [cellWithRowFour actionClickWithLeftButtonBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            // 埋点
            [MTAManager event:MTATypeLoginPageRegister params:nil];
            
            LoginRegisterViewController *loginRegisterVC = [[LoginRegisterViewController alloc]init];
            [strongSelf.navigationController pushViewController:loginRegisterVC animated:YES];
        }];
        
        [cellWithRowFour actionClickWithRightButtonBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            // 埋点
            [MTAManager event:MTATypeLoginPageFindPwd params:nil];
            
            LoginFindPwdViewController *loginFindPwdViewController = [[LoginFindPwdViewController alloc]init];
            [strongSelf.navigationController pushViewController:loginFindPwdViewController animated:YES];
        }];
        return cellWithRowFour;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"微信登录" sourceArr:self.loginArr]){
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        LoginThirdLoginTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[LoginThirdLoginTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
        }
        cellWithRowFiv.transferCellHeight = cellHeight;
        __weak typeof(self)weakSelf = self;
        [cellWithRowFiv actionWeChatButtonClickBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf shareSDKThirdLoginManager];
        }];
        return cellWithRowFiv;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.loginArr]){
        return [LoginIconTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"账号" sourceArr:self.loginArr]){
        return [LoginInputTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"登录" sourceArr:self.loginArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"找回密码" sourceArr:self.loginArr]){
        return [LoginRegisterNewTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"微信登录" sourceArr:self.loginArr]){
        return [LoginThirdLoginTableViewCell calculationCellHeight];
    }
    return LCFloat(44);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"登录" sourceArr:self.loginArr]){
        return LCFloat(20);
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - Interface
-(void)sendRequestToLogin{
    NSString *error = @"";
    if (!phoneTextField.text.length){
        error = @"请输入手机号";
    } else if (!pwdTextField.text.length){
        error = @"请输入密码";
    }
    if (error.length){
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }
    
    __weak typeof(self)weakSelf = self;
    
    // 进行埋点
    [MTAManager event:MTATypeLoginPageLogin params:nil];
    
    [[NetworkAdapter sharedAdapter] loginThirdType:thirdLoginTypeNative account:phoneTextField.text pwd:pwdTextField.text block:^(BOOL isSuccessed, LoginServerModelAccount *serverAccountModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
//            void(^block)(BOOL isSuccessed) = objc_getAssociatedObject(strongSelf, &loginSuccessKey);
//            if (block){
//                block(isSuccessed);
//            }
            [strongSelf actionNavLoginSuccessDismiss];
            [StatusBarManager statusBarHidenWithText:@"登录成功"];
        }
    }];
}

#pragma mark - 三方登录
-(void)shareSDKThirdLoginManager{
    __weak typeof(self)weakSelf = self;
    [ShareSDKManager thirdLoginWithType:thirdLoginTypeWechat successBlock:^(SSDKUser *user) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([user.rawData.allKeys containsObject:@"unionid"]){
            NSString *unionId = [user.rawData objectForKey:@"unionid"];
            [strongSelf sendRequestToThirdLogin:unionId];
        }
    } failBlock:^{
        [StatusBarManager statusBarHidenWithText:@"登录失败，请稍后再试"];
    }];
}

#pragma mark 三方登录
-(void)sendRequestToThirdLogin:(NSString *)authToken{
    __weak typeof(self)weakSelf = self;

    [[NetworkAdapter sharedAdapter] loginThirdType:thirdLoginTypeWechat account:authToken pwd:@"" block:^(BOOL isSuccessed, LoginServerModelAccount *serverAccountModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (serverAccountModel.isFirst){            // 第一次三方登录跳转绑定
                [StatusBarManager statusBarHidenWithText:@"正在跳转绑定账户"];
                LoginRegisterViewController *loginRegisterVC = [[LoginRegisterViewController alloc]init];
                loginRegisterVC.transferType = LoginRegisterTypeBinding;
                loginRegisterVC.transferAuthToken = authToken;
                [strongSelf.navigationController pushViewController:loginRegisterVC animated:YES];
            } else {
                [StatusBarManager statusBarHidenWithText:@"您已绑定微信，正在跳转登录"];
                [strongSelf actionNavLoginSuccessDismiss];
            }
        }
    }];
}

-(void)loginSuccess:(void(^)(BOOL success))block{
    objc_setAssociatedObject(self, &loginSuccessKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionNavLoginSuccessDismiss{
    void(^block)(BOOL isSuccessed) = objc_getAssociatedObject(self, &loginSuccessKey);
    if (block){
        block(YES);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - 键盘代理
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.loginTableView.frame = newTextViewFrame;
    [UIView commitAnimations];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.loginTableView.frame = self.view.bounds;
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)tapManager{
    if (phoneTextField && phoneTextField.isFirstResponder){
        [phoneTextField resignFirstResponder];
    } else if (pwdTextField && pwdTextField.isFirstResponder){
        [pwdTextField resignFirstResponder];
    }
}

-(void)btnStatus{
    if (phoneCell.inputTextField.text.length && pwdCell.inputTextField.text.length){
        [loginBtn setButtonStatus:YES];
    } else {
        [loginBtn setButtonStatus:NO];
    }
    
}

-(void)inputTextConfirm:(LoginInputType)type textField:(UITextField *)textField status:(BOOL)status{
    
    [self btnStatus];
}


@end
