//
//  LoginFindPwdViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LoginFindPwdViewController.h"
#import "LoginInputTableViewCell.h"
#import "NetworkAdapter+Login.h"

@interface LoginFindPwdViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UITextField *inputPhoneNumberTextField;
    LoginInputTableViewCell *smsCodeCell;
    UITextField *pwdTextField;
    UITextField *pwdNewTextField;
}
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)UIScrollView *findPwdScrollView;
@property (nonatomic,strong)UITableView *yanzhengPwdTableView;
@property (nonatomic,strong)UITableView *settingPwdTableView;
@property (nonatomic,strong)NSArray *yanzhengArr;
@property (nonatomic,strong)NSArray *settingArr;
@end

@implementation LoginFindPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createSegment];
    [self createScrollView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapManager)];
    [self.view addGestureRecognizer:tap];

}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"找回密码";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.yanzhengArr = @[@[@"密码",@"密码2"],@[@"提交"]];
    self.settingArr = @[@[@"密码",@"密码2"],@[@"提交"]];
}

#pragma mark - createSegmentList
-(void)createSegment{
    __weak typeof(self)weakSelf = self;
    self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"验证手机号码",@"设置新密码"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.findPwdScrollView setContentOffset:CGPointMake(index * kScreenBounds.size.width, 0) animated:YES];
    }];
    self.segmentList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleNone;
    self.segmentList.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(100));
    self.segmentList.userInteractionEnabled = NO;
    [self.segmentList setTitleFont:[UIFont fontWithCustomerSizeName:@"14"] forState:UIControlStateNormal];
    [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"353535"] forState:UIControlStateSelected];
    [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"979797"] forState:UIControlStateNormal];
    [self.segmentList setTitleFont:[[UIFont fontWithCustomerSizeName:@"23"] boldFont] forState:UIControlStateSelected];
    [self.view addSubview:self.segmentList];
}

#pragma mark - crateMainScrollView
-(void)createScrollView{
    __weak typeof(self)weakSelf = self;
    self.findPwdScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger page = scrollView.contentOffset.x / kScreenBounds.size.width;
        [strongSelf.segmentList setSelectedButtonIndex:page];
    }];
    self.findPwdScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, self.view.size_height - CGRectGetMaxY(self.segmentList.frame));
    self.findPwdScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 2, self.findPwdScrollView.size_height);
    [self.view addSubview:self.findPwdScrollView];
    self.findPwdScrollView.scrollEnabled = NO;
    
    [self createTableView];
}

-(void)createTableView{
    if (!self.yanzhengPwdTableView){
        self.yanzhengPwdTableView = [GWViewTool gwCreateTableViewRect:self.findPwdScrollView.bounds];
        self.yanzhengPwdTableView.dataSource = self;
        self.yanzhengPwdTableView.delegate = self;
        [self.findPwdScrollView addSubview:self.yanzhengPwdTableView];
    }
    if (!self.settingPwdTableView){
        self.settingPwdTableView = [GWViewTool gwCreateTableViewRect:self.findPwdScrollView.bounds];
        self.settingPwdTableView.orgin_x = kScreenBounds.size.width;
        self.settingPwdTableView.dataSource = self;
        self.settingPwdTableView.delegate = self;
        [self.findPwdScrollView addSubview:self.settingPwdTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.yanzhengPwdTableView){
        return self.yanzhengArr.count;
    } else if (tableView == self.settingPwdTableView){
        return self.settingArr.count;
    }
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *infoArr;
    if (tableView == self.yanzhengPwdTableView){
        infoArr = self.yanzhengArr;
    } else if (tableView == self.settingPwdTableView){
        infoArr = self.settingArr;
    }
    
    NSArray *sectionOfArr = [infoArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (tableView == self.yanzhengPwdTableView){
        if (indexPath.section == 0){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            LoginInputTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[LoginInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            if (indexPath.row == 0){
                cellWithRowOne.transferType = LoginInputTypePhone;
                inputPhoneNumberTextField = cellWithRowOne.inputTextField;
            } else if (indexPath.row == 1){
                cellWithRowOne.transferType = LoginInputTypeSMS;
                smsCodeCell = cellWithRowOne;
                __weak typeof(self)weakSelf = self;
                [cellWithRowOne actionClickWithSMSBtn:^{
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [strongSelf sendRequestToFindPwdSms];
                }];
            }
            return cellWithRowOne;
        } else if (indexPath.section == 1){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            cellWithRowTwo.transferTitle = @"下一步";
            
            __weak typeof(self)weakSelf = self;
            [cellWithRowTwo buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                // 埋点
                [MTAManager event:MTATypeFindPwdPageNext params:nil];
                
                [strongSelf sendRequestToValidateSMSCode];
            }];
            return cellWithRowTwo;
        }
    } else if (tableView == self.settingPwdTableView){
        if (indexPath.section == 0){
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            LoginInputTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[LoginInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            }
            cellWithRowThr.transferCellHeight = cellHeight;
           
            if (indexPath.section == 0){
                if (indexPath.row == 0){
                    cellWithRowThr.transferType = LoginInputTypePwd;
                    pwdTextField = cellWithRowThr.inputTextField;
                } else {
                    cellWithRowThr.transferType = LoginInputTypePwdAgain;
                    pwdNewTextField = cellWithRowThr.inputTextField;
                }
            }
            return cellWithRowThr;

        } else {
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            GWButtonTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowFour){
                cellWithRowFour = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            }
            cellWithRowFour.transferCellHeight = cellHeight;
            cellWithRowFour.transferTitle = @"提交";
            __weak typeof(self)weakSelf = self;
            [cellWithRowFour buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                // 埋点
                [MTAManager event:MTATypeFindPwdPageSetting params:nil];
                
                [strongSelf sendRequestToResetPwd];
            }];
            return cellWithRowFour;

        }
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.yanzhengPwdTableView){
        if (indexPath.section == 0){
            return [LoginInputTableViewCell calculationCellHeight];
        } else if (indexPath.section == 1){
            return [GWButtonTableViewCell calculationCellHeight];
        }
    } else if (tableView == self.settingPwdTableView){
        if (indexPath.section == 0){
            return [LoginInputTableViewCell calculationCellHeight];
        } else {
            return [GWButtonTableViewCell calculationCellHeight];
        }
    }
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.yanzhengPwdTableView){
        if (section == 1){
            return LCFloat(61);
        } else {
            return 0;
        }
    } else {
        if (section == 1){
            return LCFloat(61);
        } else {
            return 0;
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - 接口
#pragma mark 1.找回密码验证手机号
-(void)sendRequestToFindPwdSms{
    NSString *error = @"";
    if (!inputPhoneNumberTextField.text.length){
        error = @"请输入手机号";
    }
    if (error.length){
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]loginSendSMSCodeWithFindPwd:inputPhoneNumberTextField.text block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [strongSelf->smsCodeCell startTimer];
        }
    }];
}

#pragma mark 2.校验验证码
-(void)sendRequestToValidateSMSCode{
    NSString *error = @"";
    if (!inputPhoneNumberTextField.text){
        error = @"请输入手机号";
    } else if (!smsCodeCell.inputTextField.text.length){
        error = @"请输入验证码";
    }
    if (error.length){
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginValidateSMSCodeWithPhone:inputPhoneNumberTextField.text smsCode:smsCodeCell.inputTextField.text block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [strongSelf.findPwdScrollView setContentOffset:CGPointMake(1 * kScreenBounds.size.width, 0) animated:YES];
            [strongSelf.segmentList setSelectedButtonIndex:1 animated:YES];
        }
    }];
}

#pragma mark 3.忘记密码
-(void)sendRequestToResetPwd{
    NSString *error = @"";
    if (!pwdTextField.text.length){
        error = @"请输入密码";
    } else if (!pwdNewTextField.text.length){
        error = @"请再次输入密码";
    } else if (![pwdNewTextField.text isEqualToString:pwdTextField.text]){
        error = @"两次输入密码不一致，请核对后重新输入";
    }
    if (error.length){
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginResetPwdManagerWithPhone:inputPhoneNumberTextField.text smsCode:smsCodeCell.inputTextField.text pwd:pwdTextField.text block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [StatusBarManager statusBarHidenWithText:@"密码修改成功"];
            [strongSelf dismissManager];
        }
    }];
}

#pragma mark - 其他方法
#pragma mark 键盘代理
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    
    NSInteger page = self.findPwdScrollView.contentOffset.x / kScreenBounds.size.width;
    if (page == 0){
        self.yanzhengPwdTableView.frame = newTextViewFrame;
    } else if (page == 1){
        self.settingPwdTableView.frame = newTextViewFrame;
    }
    
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    NSInteger page = self.findPwdScrollView.contentOffset.x / kScreenBounds.size.width;
    if (page == 0){
        self.yanzhengPwdTableView.frame = self.view.bounds;;
    } else if (page == 1){
        self.settingPwdTableView.frame = self.view.bounds;;
    }
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark dismiss
-(void)dismissManager{
    for (UIViewController *controller in self.navigationController.childViewControllers){
        if ([controller isKindOfClass:[LoginRootViewController class]]){
            LoginRootViewController *loginViewController = (LoginRootViewController *)controller;
            [loginViewController actionNavLoginSuccessDismiss];
        }
    }
}

-(void)tapManager{
    if (inputPhoneNumberTextField.isFirstResponder){
        [inputPhoneNumberTextField resignFirstResponder];
    } else if (pwdTextField.isFirstResponder){
        [pwdTextField resignFirstResponder];
    } else if (pwdNewTextField.isFirstResponder){
        [pwdNewTextField resignFirstResponder];
    }
}

@end
