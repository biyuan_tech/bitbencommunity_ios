//
//  WebSocketTestViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/4.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WebSocketTestViewController.h"
#import "NetworkAdapter.h"
#import "NetworkAdapter+MemberVIP.h"
@interface WebSocketTestViewController ()<NetworkAdapterDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSArray *testArr;
@end

@implementation WebSocketTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self rightBarButtonWithTitle:@"连接" barNorImage:nil barHltImage:nil action:^{
//        [self socketManagerLink];
//    }];
//    [self smart];
//    [self smart1];
    
    [self arrayWithInit];
    [self createTableView];
    [NetworkAdapter sharedAdapter].delegate = self;

}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.testArr = @[@[@"1",@"2"],@[@"1",@"2"],@[@"1",@"2"],@[@"1",@"2"],@[@"1",@"2"],@[@"1",@"2"],@[@"1",@"2"],@[@"1",@"2"],@[@"1",@"2"],@[@"1",@"2"]];
//    [NetworkAdapter sharedAdapter].testConnect1 = [[WebSocketConnection alloc]init];
//    [NetworkAdapter sharedAdapter].testConnect2 = [[WebSocketConnection alloc]init];
//    [NetworkAdapter sharedAdapter].testConnect3 = [[WebSocketConnection alloc]init];
//    [NetworkAdapter sharedAdapter].testConnect4 = [[WebSocketConnection alloc]init];
//    [NetworkAdapter sharedAdapter].testConnect5 = [[WebSocketConnection alloc]init];
//    [NetworkAdapter sharedAdapter].testConnect6 = [[WebSocketConnection alloc]init];
//    [NetworkAdapter sharedAdapter].testConnect7 = [[WebSocketConnection alloc]init];
//    [NetworkAdapter sharedAdapter].testConnect8 = [[WebSocketConnection alloc]init];
//    [NetworkAdapter sharedAdapter].testConnect9 = [[WebSocketConnection alloc]init];
//    [NetworkAdapter sharedAdapter].testConnect10 = [[WebSocketConnection alloc]init];
//}
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.tableView){
        self.tableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        [self.view addSubview:self.tableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.testArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferTitle = @"连接";
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne1";
        GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferTitle = @"发送消息";

        return cellWithRowOne;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        if (indexPath.section == 0){
           [NetworkAdapter sharedAdapter].testConnect1 = [[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].testConnect1 host:@"192.168.31.62" port:8082];
        } else if (indexPath.section == 1){
           [NetworkAdapter sharedAdapter].testConnect2 = [[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].testConnect2 host:@"192.168.31.62" port:8082];
        } else if (indexPath.section == 2){
            [NetworkAdapter sharedAdapter].testConnect3 =[[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].testConnect3 host:@"192.168.31.62" port:8082];
        } else if (indexPath.section == 3){
            [NetworkAdapter sharedAdapter].testConnect4 =[[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].testConnect4 host:@"192.168.31.62" port:8082];
        } else if (indexPath.section == 4){
            [NetworkAdapter sharedAdapter].testConnect5 =[[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].testConnect5 host:@"192.168.31.62" port:8082];
        } else if (indexPath.section == 5){
            [NetworkAdapter sharedAdapter].testConnect6 =[[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].testConnect6 host:@"192.168.31.62" port:8082];
        } else if (indexPath.section == 6){
            [NetworkAdapter sharedAdapter].testConnect7 =[[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].testConnect7 host:@"192.168.31.62" port:8082];
        } else if (indexPath.section == 7){
            [NetworkAdapter sharedAdapter].testConnect8 =[[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].testConnect8 host:@"192.168.31.62" port:8082];
        } else if (indexPath.section == 8){
            [NetworkAdapter sharedAdapter].testConnect9 =[[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].testConnect9 host:@"192.168.31.62" port:8082];
        } else if (indexPath.section == 9){
            [NetworkAdapter sharedAdapter].testConnect10 =[[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].testConnect10 host:@"192.168.31.62" port:8082];
        }
    } else {
        if (indexPath.section == 0){
            [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:@{@"n":@(1)} socket:[NetworkAdapter sharedAdapter].testConnect1];
        } else if (indexPath.section == 1){
            [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:@{@"number":@(15),@"smart":@"333"} socket:[NetworkAdapter sharedAdapter].testConnect2];
        } else if (indexPath.section == 2){
            [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:@{@"number":@(15),@"smart":@"333"} socket:[NetworkAdapter sharedAdapter].testConnect3];
        } else if (indexPath.section == 3){
            [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:@{@"number":@(15),@"smart":@"333"} socket:[NetworkAdapter sharedAdapter].testConnect4];
        } else if (indexPath.section == 4){
            [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:@{@"number":@(15),@"smart":@"333"} socket:[NetworkAdapter sharedAdapter].testConnect5];
        } else if (indexPath.section == 5){
            [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:@{@"number":@(15),@"smart":@"333"} socket:[NetworkAdapter sharedAdapter].testConnect6];
        } else if (indexPath.section == 6){
            [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:@{@"number":@(15),@"smart":@"333"} socket:[NetworkAdapter sharedAdapter].testConnect7];
        } else if (indexPath.section == 7){
            [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:@{@"number":@(15),@"smart":@"333"} socket:[NetworkAdapter sharedAdapter].testConnect8];
        } else if (indexPath.section == 8){
            [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:@{@"number":@(15),@"smart":@"333"} socket:[NetworkAdapter sharedAdapter].testConnect9];
        } else if (indexPath.section == 9){
            [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:@{@"number":@(15),@"smart":@"333"} socket:[NetworkAdapter sharedAdapter].testConnect10];
        }
        
            }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = UURandomColor;;
    return headerView;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}


-(void)socketManagerLink{
    [[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].mainRootSocketConnection host:@"192.168.31.62" port:8082];
}

-(void)smart{
    UIButton *smartButton = [UIButton buttonWithType:UIButtonTypeCustom];
    smartButton.backgroundColor = [UIColor redColor];
    smartButton.frame = CGRectMake(100, 100, 100, 100);
    [self.view addSubview:smartButton];
    [smartButton setTitle:@"发送消息" forState:UIControlStateNormal];
    [smartButton buttonWithBlock:^(UIButton *button) {
        [self sma];
    }];
}


-(void)smart1{
    UIButton *smartButton = [UIButton buttonWithType:UIButtonTypeCustom];
    smartButton.backgroundColor = [UIColor redColor];
    [smartButton setTitle:@"断开" forState:UIControlStateNormal];
    smartButton.frame = CGRectMake(100, 200, 100, 100);
    [self.view addSubview:smartButton];
    [smartButton buttonWithBlock:^(UIButton *button) {
        [self smar2];
    }];
}


-(void)sma{
    [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:@{@"number":@(15),@"smart":@"333"} socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
}

-(void)smar2{
    [[NetworkAdapter sharedAdapter] webSocketCloseConnectionWithSoceket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
}

-(void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data{
    [[UIAlertView alertViewWithTitle:@"123" message:data.receiveDataJson buttonTitles:@[@"确定"] callBlock:NULL]show];
}

@end
