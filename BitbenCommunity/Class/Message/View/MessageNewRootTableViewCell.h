//
//  MessageNewRootTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/1.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "MessageNewRootModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface MessageNewRootTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)MessageNewRootListSingleModel *transferSingleModel;

@end

NS_ASSUME_NONNULL_END
