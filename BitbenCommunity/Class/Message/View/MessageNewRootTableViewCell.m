//
//  MessageNewRootTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/1.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "MessageNewRootTableViewCell.h"
#import "NetworkAdapter+Center.h"

@interface MessageNewRootTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *desLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UIView *dotView;
@end

@implementation MessageNewRootTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // icon
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.frame = CGRectMake(LCFloat(15), ([MessageNewRootTableViewCell calculationCellHeight] - LCFloat(44)) / 2., LCFloat(44), LCFloat(44));
    [self addSubview:self.iconImgView];
    
    // title
    self.titleLabel = [GWViewTool createLabelFont:@"16" textColor:@"323232"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.text = @"1231123";
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(12), self.iconImgView.orgin_y, LCFloat(100), [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    // des
    self.desLabel = [GWViewTool createLabelFont:@"14" textColor:@"8F8F8F"];
    self.desLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(8), kScreenBounds.size.width - LCFloat(38) - CGRectGetMaxX(self.iconImgView.frame) - LCFloat(12), [NSString contentofHeightWithFont:self.desLabel.font]);
    [self addSubview:self.desLabel];
    
    // timeLabel
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"8F8F8F"];
    [self addSubview:self.timeLabel];
    
    // dot
    self.dotView = [[PDImageView alloc] init];
    self.dotView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(5), 0, LCFloat(5), LCFloat(5));
    self.dotView.backgroundColor = [UIColor redColor];
    self.dotView.clipsToBounds = YES;
    self.dotView.layer.cornerRadius = self.dotView.size_width / 2.;
    [self addSubview:self.dotView];
}

-(void)setTransferSingleModel:(MessageNewRootListSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    if (transferSingleModel.type == MessageTypeRoot){           // 系统消息
        self.iconImgView.image = [UIImage imageNamed:@"icon_message_new_xitong"];
    } else if(transferSingleModel.type == MessageTypeBlock){    // 钱包消息
        self.iconImgView.image = [UIImage imageNamed:@"icon_message_new_jiaoyi"];
    } else if (transferSingleModel.type == MessageTypeComment){ // 评论消息
        self.iconImgView.image = [UIImage imageNamed:@"icon_message_new_kaibo"];
    } else if (transferSingleModel.type == MessageTypeZan){     // 点赞消息
        
    } else if (transferSingleModel.type == MessageTypeLink){    // 关注消息
        
    } else if (transferSingleModel.type == MessageTypeLive){    // 开播消息
        self.iconImgView.image = [UIImage imageNamed:@"icon_message_new_kaibo"];
    } else if (transferSingleModel.type == MessageTypeShouyiEveryDay){  // 每日收益
        self.iconImgView.image = [UIImage imageNamed:@"icon_message_new_meirishouyi"];
    }
    
    self.titleLabel.text = transferSingleModel.name;
    self.desLabel.text = [NSString stringWithFormat:@"%@%@",transferSingleModel.content,transferSingleModel.content];
    self.dotView.center_y = self.desLabel.center_y;
    
    self.timeLabel.text = [NSDate getTimeGap:transferSingleModel.create_time / 1000.];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - timeSize.width, LCFloat(18), timeSize.width, timeSize.height);
    
    if (transferSingleModel.count == 0){
        self.dotView.hidden = YES;
    } else {
        self.dotView.hidden = NO;
    }
}

#pragma mark -
+(CGFloat)calculationCellHeight{
    return LCFloat(68);
}


@end
