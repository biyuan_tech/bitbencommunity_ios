//
//  MessageNewListDetailSingleTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/1.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "MessageNewListDetailSingleTableViewCell.h"

@interface MessageNewListDetailSingleTableViewCell()
@property (nonatomic,strong)PDImageView *imgView;
@property (nonatomic,strong)UIView *bottomView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *desLabel;
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UILabel *detailLabel;
@property (nonatomic,strong)PDImageView *dotView;
@property (nonatomic,strong)PDImageView *arrowImgView;
@property (nonatomic,strong)PDImageView *convertImgView;
@end

@implementation MessageNewListDetailSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.imgView = [[PDImageView alloc]init];
    self.imgView.backgroundColor = UURandomColor;
    [self addSubview:self.imgView];
    
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bottomView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"16" textColor:@"323232"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self.bottomView addSubview:self.titleLabel];
    
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"8F8F8F"];
    [self.bottomView addSubview:self.timeLabel];
    
    self.desLabel = [GWViewTool createLabelFont:@"15" textColor:@"8F8F8F"];
    self.desLabel.numberOfLines = 0;
    [self.bottomView addSubview:self.desLabel];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor hexChangeFloat:@"E7E7EA"];
    [self.bottomView addSubview:self.lineView];
    
    self.detailLabel = [GWViewTool createLabelFont:@"14" textColor:@"323232"];
    [self.bottomView addSubview:self.detailLabel];
    
    self.dotView = [[PDImageView alloc]init];
    self.dotView.backgroundColor = [UIColor redColor];
    self.dotView.layer.cornerRadius = LCFloat(2);
    self.dotView.clipsToBounds = YES;
    [self.bottomView addSubview:self.dotView];
    
    self.arrowImgView = [[PDImageView alloc]init];
    self.arrowImgView.backgroundColor = [UIColor clearColor];
    self.arrowImgView.image = [UIImage imageNamed:@"icon_base_arrow"];
    [self.bottomView addSubview:self.arrowImgView];
    
    // convert
    self.convertImgView = [[PDImageView alloc]init];
    self.convertImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.convertImgView];
    
}

-(void)setTransferDetailModel:(CenterMessageRootSingleModel *)transferDetailModel{
    _transferDetailModel = transferDetailModel;

    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(15) * 2;
    
    // img
    self.imgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(160));
    [self.imgView uploadHDImageWithURL:transferDetailModel.picture callback:NULL];
    
    if (transferDetailModel.picture.length){
        self.imgView.hidden = NO;
        self.bottomView.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.imgView.frame), width,[MessageNewListDetailSingleTableViewCell calculationCellHeightWithModel:transferDetailModel] - self.imgView.size_height);
    } else {
        self.imgView.hidden = YES;
        self.bottomView.frame = CGRectMake(LCFloat(15), 0, width,[MessageNewListDetailSingleTableViewCell calculationCellHeightWithModel:transferDetailModel]);
    }
    
    // title
    self.titleLabel.text = transferDetailModel.title;
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake(LCFloat(15), LCFloat(15), titleSize.width, titleSize.height);
    
    // time
    self.timeLabel.text = [NSDate getTimeGap:transferDetailModel.create_time / 1000.];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(self.bottomView.size_width - timeSize.width, 0, timeSize.width, timeSize.height);
    self.timeLabel.center_y = self.titleLabel.center_y;
    
    // desc
    self.desLabel.text = transferDetailModel.content;
    CGSize desSize = [self.desLabel.text sizeWithCalcFont:self.desLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.desLabel.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(15), width, desSize.height);
    
    // line
    self.lineView.frame=  CGRectMake(LCFloat(15), CGRectGetMaxY(self.desLabel.frame) + LCFloat(15), width, .5f);
    
    // detail
    self.detailLabel.text = @"查看详情";
    CGSize detailSize = [Tool makeSizeWithLabel:self.detailLabel];
    self.detailLabel.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.lineView.frame), detailSize.width, LCFloat(44));

    self.arrowImgView.frame = CGRectMake(width - LCFloat(5), 5, LCFloat(5), LCFloat(9));
    self.arrowImgView.center_y = self.detailLabel.center_y;
    // dot
    self.dotView.frame = CGRectMake(self.arrowImgView.orgin_x - LCFloat(12) - LCFloat(4), 0, LCFloat(4), LCFloat(4));
    self.dotView.center_y = self.arrowImgView.center_y;
    
    // convertView
    self.convertImgView.image = [Tool stretchImageWithName:@"bg_message_detailList_convert"];
    self.convertImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [MessageNewListDetailSingleTableViewCell calculationCellHeightWithModel:transferDetailModel]);
    
    if (self.transferDetailModel.is_read){
        self.dotView.hidden = YES;
    } else {
        self.dotView.hidden = NO;
    }

}

+(CGFloat)calculationCellHeightWithModel:(CenterMessageRootSingleModel *)model{
    CGFloat cellHeight = 0;
    
    if (model.picture.length){
        cellHeight += LCFloat(160);
    }
    
    // title
    cellHeight += LCFloat(16);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"16"]];
    cellHeight += LCFloat(16);
    
    // des
    CGFloat width = kScreenBounds.size.width - 4 * LCFloat(15);
    CGSize desSize = [model.content sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"15"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += desSize.height;
    
    // line
    cellHeight += LCFloat(15);
    cellHeight += LCFloat(44);
    
    return cellHeight;
}
@end
