//
//  MessageNewItemsTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/1.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "MessageNewItemsTableViewCell.h"

static char actionClickWithCellItemTypeKey;
@interface MessageNewItemsTableViewCell()
@property (nonatomic,strong)UIButton *pinglunButton;
@property (nonatomic,strong)UIButton *zanButton;
@property (nonatomic,strong)UIButton *fansButton;
@end

@implementation MessageNewItemsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i < 3;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, LCFloat(13), LCFloat(60), LCFloat(80));
        button.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [self addSubview:button];
        if (i == 0){            // 评论
            self.pinglunButton = button;
            [button setTitle:@"评论" forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"icon_message_pinglun"] forState:UIControlStateNormal];
            button.orgin_x = LCFloat(40);
        } else if (i == 1){     // 点赞
            self.zanButton = button;
            [button setTitle:@"点赞" forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"icon_message_zan"] forState:UIControlStateNormal];
            button.center_x = kScreenBounds.size.width / 2.;
        } else if (i == 2){     // 新粉丝
            self.fansButton = button;
            [button setTitle:@"新粉丝" forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"icon_message_dianzan"] forState:UIControlStateNormal];
            button.orgin_x = kScreenBounds.size.width - 40 - button.size_width;
        }
        [button layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleTop imageTitleSpace:LCFloat(12)];
        [button buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(MessageNewItemsTableViewCellType type) = objc_getAssociatedObject(strongSelf, &actionClickWithCellItemTypeKey);
            if (block){
                block(i);
            }
        }];
    }
}

-(void)setTransferMessageModel:(MessageNewRootModel *)transferMessageModel{
    _transferMessageModel = transferMessageModel;
    [self.pinglunButton setBadge:transferMessageModel.comment_count font:@"12"];
    [self.zanButton setBadge:transferMessageModel.support_count font:@"12"];
    [self.fansButton setBadge:transferMessageModel.attention_count font:@"12"];
}

-(void)actionClickWithCellItemType:(void(^)(MessageNewItemsTableViewCellType type))block{
    objc_setAssociatedObject(self, &actionClickWithCellItemTypeKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)clearItemBirdge:(MessageNewItemsTableViewCellType)type{
    
}

+(CGFloat)calculationCellHeight{
    return LCFloat(105);
}

@end
