//
//  MessageNewListDetailSingleTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/1.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "MessageNewListDetailModel.h"
#import "CenterMessageSingleModel.h"
#import "CenterMessageRootSingleModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MessageNewListDetailSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)CenterMessageRootSingleModel *transferDetailModel;

+(CGFloat)calculationCellHeightWithModel:(CenterMessageRootSingleModel *)model;

@end

NS_ASSUME_NONNULL_END
