//
//  MessageNewItemsTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/1.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "MessageNewRootModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,MessageNewItemsTableViewCellType) {
    MessageNewItemsTableViewCellTypePinglun = 0,
    MessageNewItemsTableViewCellTypeDianzan = 1,
    MessageNewItemsTableViewCellTypeFans = 2,
};

@interface MessageNewItemsTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)MessageNewRootModel *transferMessageModel;

-(void)actionClickWithCellItemType:(void(^)(MessageNewItemsTableViewCellType type))block;

-(void)clearItemBirdge:(MessageNewItemsTableViewCellType)type;

@end

NS_ASSUME_NONNULL_END
