//
//  MessageNewRootViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/1.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "MessageNewRootViewController.h"

// view
#import "MessageNewRootTableViewCell.h"
#import "MessageNewItemsTableViewCell.h"

// controller
#import "CenterMessageCommentViewController.h"              // 评论
#import "CenterMessageZanViewController.h"                  // 点赞
#import "CenterMessageFansViewController.h"                 // 新粉丝
#import "MessageNewListDetailViewController.h"

// test
#import "TWMessageBarManager.h"
#import "NetworkAdapter+Center.h"


@interface MessageNewRootViewController ()<UITableViewDelegate,UITableViewDataSource>{
    MessageNewRootModel *listModel;
}
@property (nonatomic,strong)UITableView *mainTableView;
@property (nonatomic,strong)NSMutableArray *mainMessageMutableArr;
@property (nonatomic,strong)NSMutableArray *mainMessageListMutableArr;
@end

@implementation MessageNewRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self getMessageList];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"消息";
//    [self rightBarButtonWithTitle:@"123" barNorImage:nil barHltImage:nil action:^{
//        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"123" description:@"333" type:TWMessageBarMessageTypeSuccess callback:^{
//            NSLog(@"123");
//        }];
//    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.mainMessageMutableArr = [NSMutableArray array];
    self.mainMessageListMutableArr = [NSMutableArray array];
    [self.mainMessageMutableArr addObject:@[@"头"]];
    [self.mainMessageMutableArr addObject:self.mainMessageListMutableArr];
}

#pragma mark - tableView
-(void)createTableView{
    if (!self.mainTableView){
        self.mainTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.mainTableView.dataSource = self;
        self.mainTableView.delegate = self;
        [self.view addSubview:self.mainTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.mainMessageMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.mainMessageMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        MessageNewItemsTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[MessageNewItemsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
        }
        cellWithRowZero.transferMessageModel = listModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowZero actionClickWithCellItemType:^(MessageNewItemsTableViewCellType type) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (type == MessageNewItemsTableViewCellTypePinglun){
                [strongSelf settingHasAllReadWithType:MessageTypeComment block:^{
                    CenterMessageCommentViewController *centerMessageCommentVC = [[CenterMessageCommentViewController alloc]init];
                    [strongSelf.navigationController pushViewController:centerMessageCommentVC animated:YES];
                }];
            } else if (type == MessageNewItemsTableViewCellTypeDianzan){
                [strongSelf settingHasAllReadWithType:MessageTypeZan block:^{
                    CenterMessageZanViewController *zanVC = [[CenterMessageZanViewController alloc]init];
                    [strongSelf.navigationController pushViewController:zanVC animated:YES];
                }];
            } else if (type == MessageNewItemsTableViewCellTypeFans){
                [strongSelf settingHasAllReadWithType:MessageTypeLink block:^{
                    CenterMessageFansViewController *fansVC = [[CenterMessageFansViewController alloc]init];
                    [strongSelf.navigationController pushViewController:fansVC animated:YES];
                }];
            }
        }];
        
        return cellWithRowZero;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        MessageNewRootTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[MessageNewRootTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferSingleModel = [self.mainMessageListMutableArr objectAtIndex:indexPath.row];
        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [MessageNewItemsTableViewCell calculationCellHeight];
    } else {
        return [MessageNewRootTableViewCell calculationCellHeight];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section > 0){
        MessageNewListDetailViewController *messageNewListVC = [[MessageNewListDetailViewController alloc]init];
        MessageNewRootListSingleModel *singleModel = [self.mainMessageListMutableArr objectAtIndex:indexPath.row];
        messageNewListVC.transferSingleModel = singleModel;
        [self.navigationController pushViewController:messageNewListVC animated:YES];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0){
        return NO;
    } else {
        return YES;
    }
}

// 定义编辑样式
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

// 进入编辑模式，按下出现的编辑按钮后,进行删除操作
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        MessageNewRootListSingleModel *singleModel = [self.mainMessageListMutableArr objectAtIndex:indexPath.row];
        [self cleanMyMessageWithModel:singleModel];
    }
}

// 修改编辑按钮文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}


#pragma mark - getMessageCount
-(void)getMessageList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] getNoReadMessageListWithBlock:^(MessageNewRootModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->listModel = listModel;
        
        [strongSelf.mainMessageListMutableArr removeAllObjects];
        [strongSelf.mainMessageListMutableArr addObjectsFromArray:listModel.list];
        [strongSelf.mainTableView reloadData];
   
    }];
}

#pragma mark - 清空我的消息
-(void)cleanMyMessageWithModel:(MessageNewRootListSingleModel *)singleModel{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerCleanMyMessagesWithType:singleModel.type block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger index = [strongSelf.mainMessageListMutableArr indexOfObject:singleModel];
        [strongSelf.mainMessageListMutableArr removeObject:singleModel];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:1];
        [strongSelf.mainTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }];
}

#pragma mark - 设置全部已读
-(void)settingHasAllReadWithType:(MessageType)type block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] sendRequestToGetAllReadWithType:type block:^{
        if (!weakSelf){
            return ;
        }
        
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (type == MessageTypeLink){
           strongSelf-> listModel.attention_count = @"0";
        } else if (type == MessageTypeZan){
           strongSelf-> listModel.support_count = @"0";
        } else if (type == MessageTypeComment){
           strongSelf-> listModel.comment_count = @"0";
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [strongSelf.mainTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        if (block){
            block();
        }
    }];
}
@end
