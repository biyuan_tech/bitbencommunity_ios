//
//  MessageNewListDetailViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/1.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "MessageNewListDetailViewController.h"
#import "MessageNewListDetailSingleTableViewCell.h"
#import "NetworkAdapter+Center.h"
#import "APNSTool.h"
#import "MessageNewWalletTransferDetailViewController.h"


@interface MessageNewListDetailViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *detailListTableView;
@property (nonatomic,strong)NSMutableArray *detailListMutableArr;
@end

@implementation MessageNewListDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    if (self.transferSingleModel.type == MessageTypeRoot){
        self.barMainTitle = @"系统通知";
    } else if (self.transferSingleModel.type == MessageTypeBlock){
        self.barMainTitle = @"交易提醒";
    } else if (self.transferSingleModel.type == MessageTypeComment){
        self.barMainTitle = @"评论消息";
    } else if (self.transferSingleModel.type == MessageTypeZan){
        self.barMainTitle = @"点赞消息";
    } else if (self.transferSingleModel.type == MessageTypeLink){
        self.barMainTitle = @"关注消息";
    } else if (self.transferSingleModel.type == MessageTypeLive){
        self.barMainTitle = @"开播提醒";
    } else if (self.transferSingleModel.type == MessageTypeShouyiEveryDay){
        self.barMainTitle = @"每日收益";
    }
        
    self.view.backgroundColor = RGB(242, 244, 245, 1);
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.detailListMutableArr = [NSMutableArray array];
  
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.detailListTableView){
        self.detailListTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.detailListTableView.dataSource = self;
        self.detailListTableView.delegate = self;
        [self.view addSubview:self.detailListTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.detailListTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfo];
    }];
    
    [self.detailListTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfo];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.detailListMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    MessageNewListDetailSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[MessageNewListDetailSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferDetailModel = [self.detailListMutableArr objectAtIndex:indexPath.section];
    return cellWithRowOne;
}

#pragma mark - 
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [MessageNewListDetailSingleTableViewCell calculationCellHeightWithModel:[self.detailListMutableArr objectAtIndex:indexPath.section]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CenterMessageRootSingleModel *singleModel = [self.detailListMutableArr objectAtIndex:indexPath.section];
    __weak typeof(self)weakSelf = self;
    [self messageReadWithId:singleModel block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(singleModel.type == MessageTypeRoot){
            if (singleModel.type_code == PushTypeSystemMail){        // 站内信
                if (singleModel.theme_type == 0){                       // 跳转直播
                    [strongSelf directManagerWithModel:singleModel];
                } else if (singleModel.theme_type == 1){                // 1.跳转文章
                    ArticleDetailRootViewController *articleVC = [[ArticleDetailRootViewController alloc]init];
                    articleVC.transferPageType = ArticleDetailRootViewControllerTypeBanner;
                    articleVC.transferArticleId = singleModel.theme_id;
                    [self.navigationController pushViewController:articleVC animated:YES];
                } else if (singleModel.theme_type == 2){
                    PDWebViewController *webViewController = [[PDWebViewController alloc]init];
                    [webViewController webDirectedWebUrl:singleModel.theme_id];
                    [self.navigationController pushViewController:webViewController animated:YES];
                }
            } else if (singleModel.type_code == PushTypeSystemMsg){
                // 现在不跳
                return;
            } else if (singleModel.type_code == PushTypeSystemInviteRegister){
                [APNSTool directManagerControlerWithType:PushTypeSystemInviteRegister];
            } else if (singleModel.type_code == PushTypeSystemInviteMember){        // 会员绑定
                [APNSTool directManagerControlerWithType:PushTypeSystemInviteMember];
            } else if (singleModel.type_code == PushTypeUserAuth || singleModel.type_code == PushTypeSystemAuthSuccess || singleModel.type_code ==  PushTypeSystemAuthFail){
                [APNSTool directManagerControlerWithType:singleModel.type_code];
            }
        } else if (singleModel.type == MessageTypeBlock){       /**< 钱包消息*/
            if (singleModel.type_code == PushTypeWalletSendSuccess ||
                singleModel.type_code == PushTypeWalletSendFail ||
                singleModel.type_code == PushTypeWalletGetSuccess ||
                singleModel.type_code == PushTypeWalletGetFail ||
                singleModel.type_code == PushTypeWalletMemberFail ||
                singleModel.type_code == PushTypeWalletMemberSuccess ||
                singleModel.type_code == PushTypeWalletTransferFail ||
                singleModel.type_code == PushTypeWalletCashSuccess ||
                singleModel.type_code == PushTypeWalletCashFail ||
                singleModel.type_code == PushTypeWalletCashValidFail ||
                singleModel.type_code == PushTypeWalletMemberMoreSuccess ||
                singleModel.type_code == PushTypeWalletMemberMoreFail ||
                singleModel.type_code == PushTypeWalletMemberUpSuccess ||
                singleModel.type_code == PushTypeWalletMemberUpFail){
                if (singleModel.theme_id.length){
                    MessageNewWalletTransferDetailViewController *walletTransferVC = [[MessageNewWalletTransferDetailViewController alloc]init];
                    walletTransferVC.transferMessageId = singleModel.user_message_id;
                    [self.navigationController pushViewController:walletTransferVC animated:YES];
                }
            } else if (singleModel.type_code == PushTypeWalletValidSuccess ||
                       singleModel.type_code == PushTypeWalletValidFail){
                if (singleModel.link_status == 0){           // 不能跳转
                    [StatusBarManager statusBarHidenWithText:@"当前不可跳转，消息已过期。"];
                } else if (singleModel.link_status == 1){    // 跳转到修改密码
                    [[UIAlertView alertViewWithTitle:@"忘记密码" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
                }
            }
        } else if (singleModel.type == MessageTypeComment){     /**< 评论消息*/
            if (singleModel.type_code == PushTypeArticleComment){               // 文章评论
                [APNSTool directManagerControlerWithType:singleModel.type_code];
            } else if (singleModel.type_code == PushTypeArticleSubComment){     // 文章评论的评论
                return;
            } else if (singleModel.type_code == PushTypeLiveSuport){
                return;
            } else if (singleModel.type_code == PushTypeLiveSubSuport){
                return;
            }
        } else if (singleModel.type == MessageTypeZan){         /**< 点赞消息*/
            if (singleModel.type_code == PushTypeArticleSupport){
                [APNSTool directManagerControlerWithType:singleModel.type_code];
            } else if (singleModel.type_code == PushTypeArticleSubSupport){
                return;
            }

        } else if (singleModel.type == MessageTypeLink){        /**< 关注消息*/
            if (singleModel.type_code == PushTypeUserBeConcerned){
                [APNSTool directManagerControlerWithType:singleModel.type_code];
            } else if (singleModel.type_code == PushTypeUserIncome){
                [APNSTool directManagerControlerWithType:singleModel.type_code];
            } else if (singleModel.type_code == PushTypeUserAuth || singleModel.type_code == PushTypeSystemAuthSuccess || singleModel.type_code ==  PushTypeSystemAuthFail){
                [APNSTool directManagerControlerWithType:singleModel.type_code];
            }
        } else if (singleModel.type == MessageTypeLive){        /**< 开播消息*/
            if (singleModel.type_code == PushTypeLiveMyCreateBefore || singleModel.type_code == PushTypeLiveMyCreateAfter){     // 直播
                [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_APP_LIVE_HOME liveType:singleModel.live_type themeId:singleModel.theme_id];
            } else {
                if (singleModel){
                    [strongSelf directManagerWithModel:singleModel];
                }
            }
        } else if (singleModel.type == MessageTypeShouyiEveryDay){/**< 每日收益*/
            if (singleModel.type_code == PushTypeUserIncome){           //每日收益
                [APNSTool directManagerControlerWithType:PushTypeUserIncome];
            }
        }
            
    
//
//
////
//        if (singleModel.type_code == PushTypeUserIncome){           //每日收益
//            [APNSTool directManagerControlerWithType:PushTypeUserIncome];
//        } else if (singleModel.type_code == PushTypeSystemMail){        // 站内信
//            if (singleModel.type  == MessageTypeRoot){              // main
//
//            }
//        } else if (singleModel.type_code == PushTypeWalletValidSuccess || singleModel.type_code == PushTypeWalletValidFail){
//            if (singleModel.type == MessageTypeBlock){
//                if (singleModel.link_status == 0){           // 不能跳转
//                    [StatusBarManager statusBarHidenWithText:@"当前不可跳转，消息已过期。"];
//                } else if (singleModel.link_status == 1){    // 跳转到修改密码
//                    [[UIAlertView alertViewWithTitle:@"忘记密码" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
//                }
//            }
//        } else if (singleModel.type_code == PushTypeSystemAttendance){          //  签到成功
//            [APNSTool directManagerControlerWithType:PushTypeSystemAttendance];
//        } else if (singleModel.type_code == PushTypeSystemInvitation){          // 邀请好友
//            [APNSTool directManagerControlerWithType:PushTypeSystemInvitation];
//        } else if (singleModel.type_code == PushTypeSystemInviteMember){        // 会员绑定
//            [APNSTool directManagerControlerWithType:PushTypeSystemInviteMember];
//        } else if (singleModel.type_code == PushTypeSystemInviteRegister){
//            [APNSTool directManagerControlerWithType:PushTypeSystemInviteRegister];
//        } else if (singleModel.type_code == PushTypeUserBeConcerned){
//            [APNSTool directManagerControlerWithType:singleModel.type_code];
//        } else if (singleModel.type_code == PushTypeUserAuth || singleModel.type_code == PushTypeSystemAuthSuccess || singleModel.type_code ==  PushTypeSystemAuthFail){
//            [APNSTool directManagerControlerWithType:singleModel.type_code];
//        } else if (singleModel.type_code == PushTypeUserIncome){
//            [APNSTool directManagerControlerWithType:singleModel.type_code];
//        } else if (singleModel.type_code == PushTypeArticleSupport){
//            [APNSTool directManagerControlerWithType:singleModel.type_code];
//        } else if (singleModel.type_code == PushTypeArticleComment){
//            [APNSTool directManagerControlerWithType:singleModel.type_code];
//        } else if (singleModel.type_code == PushTypeWalletSendSuccess){
//            MessageNewWalletTransferDetailViewController *walletTransferVC = [[MessageNewWalletTransferDetailViewController alloc]init];
//            walletTransferVC.transferMessageId = singleModel.user_message_id;
//            [self.navigationController pushViewController:walletTransferVC animated:YES];
//        } else if (singleModel.type_code == PushTypeLiveMyCreateBefore || singleModel.type_code == PushTypeLiveMyCreateAfter){
//            [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_APP_LIVE_HOME liveType:singleModel.live_type themeId:singleModel.live_remind.theme_id];
//        }
    }];
}

-(void)directManagerWithModel:(CenterMessageRootSingleModel *)model{
    if (model.type_code == PushTypeSystemMail){
        [DirectManager commonPushControllerWithThemeType:model.theme_type
                                                liveType:model.live_type
                                                 themeId:model.theme_id];
    } else {
        [DirectManager commonPushControllerWithThemeType:model.theme_type
                                                liveType:model.live_type
                                                 themeId:model.theme_id];
    }
}


#pragma mark - Interface
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerMessageGetListWithPage:self.detailListTableView.currentPage type:self.transferSingleModel.type block:^(CenterMessageRootListModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.detailListTableView.isXiaLa){
            [strongSelf.detailListMutableArr removeAllObjects];
        }
        [strongSelf.detailListMutableArr addObjectsFromArray:listModel.content];
        [strongSelf.detailListTableView reloadData];
        
        if (strongSelf.detailListTableView.isXiaLa){
            [strongSelf.detailListTableView stopPullToRefresh];
        } else {
            [strongSelf.detailListTableView stopFinishScrollingRefresh];
        }
        
        if (strongSelf.detailListMutableArr.count){
            [strongSelf.detailListTableView dismissPrompt];
        } else {
            [strongSelf.detailListTableView showPrompt:@"当前没有内容" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
        }
    }];
}

// 设为已读
-(void)messageReadWithId:(CenterMessageRootSingleModel *)msgModel block:(void(^)())block{
    if (msgModel.is_read == YES){
        if (block){
            block();
        }
        return;
    }
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerMessageReadList:@[msgModel.user_message_id] block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            // 减一条已读消息
            [AccountModel sharedAccountModel].unReadMessage -= 1;
            
            NSInteger index = [strongSelf.detailListMutableArr indexOfObject:msgModel];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:index];
            msgModel.is_read = YES;
            [strongSelf.detailListTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        if (block){
            block();
        }
    }];
}

@end
