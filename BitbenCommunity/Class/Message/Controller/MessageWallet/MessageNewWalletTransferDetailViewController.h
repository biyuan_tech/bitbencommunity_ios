//
//  MessageNewWalletTransferDetailViewController.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MessageNewWalletTransferDetailViewController : AbstractViewController
@property (nonatomic,copy)NSString *transferMessageId;
@end

NS_ASSUME_NONNULL_END
