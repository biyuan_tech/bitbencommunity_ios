//
//  MessageNewWalletTransferDetailViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "MessageNewWalletTransferDetailViewController.h"
#import "WalletTransferDetailSingleTableViewCell.h"
#import "WalletTransferDetailHeaderTableViewCell.h"
#import "NetworkAdapter+Wallet.h"

@interface MessageNewWalletTransferDetailViewController ()<UITableViewDelegate,UITableViewDataSource>{
    WalletTransferDetailModel *detailModel;
}
@property (nonatomic,strong)UITableView *transferDetailTableView;
@property (nonatomic,strong)NSMutableArray *transferDetailMutableArr;

@end

@implementation MessageNewWalletTransferDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self getMessageInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"转账消息";
}

-(void)arrayWithInit{
    self.transferDetailMutableArr = [NSMutableArray array];
    [self.transferDetailMutableArr addObjectsFromArray:@[@[@"标题"],@[@"发送地址",@"01309482304923"],@[@"接收地址",@"sdfadsfaosdfasdf"],@[@"手续费",@"sdfsdjflksjd"],@[@"区块",@"sdlkfjaldsjasdf"],@[@"交易时间",@"klfjsd"],@[@"交易ID",@"sdalkfjdsfdsafadsfosd"]]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.transferDetailTableView){
        self.transferDetailTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.transferDetailTableView.dataSource = self;
        self.transferDetailTableView.delegate = self;
        [self.view addSubview:self.transferDetailTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.transferDetailMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.transferDetailMutableArr]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        WalletTransferDetailHeaderTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[WalletTransferDetailHeaderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
        }
        detailModel.type = detailModel.type;

        cellWithRowZero.transferDetailModel = detailModel;
        return cellWithRowZero;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        WalletTransferDetailSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[WalletTransferDetailSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        NSString *title = [[self.transferDetailMutableArr objectAtIndex:indexPath.section] objectAtIndex:0];
        cellWithRowOne.transferTitle = title;
        if ([title isEqualToString:@"发送地址"]){
            cellWithRowOne.transferDymic = detailModel.from;
        } else if ([title isEqualToString:@"接收地址"]){
            cellWithRowOne.transferDymic = detailModel.to;
        } else if ([title isEqualToString:@"手续费"]){
            NSString *shouxufei = @"";
            if ([ detailModel.status isEqualToString:@"3"]){
                shouxufei = detailModel.service_charge;
            }  else if ([detailModel.status isEqualToString:@"10"]){
                shouxufei = [NSString stringWithFormat:@"0 %@",detailModel.name];
            } else if ([detailModel.status isEqualToString:@"11"]){
                shouxufei = [NSString stringWithFormat:@"0 %@",detailModel.name];
            } else if ([detailModel.status isEqualToString:@"12"]){
                shouxufei = [NSString stringWithFormat:@"0 %@",detailModel.name];
            } else if ([detailModel.status isEqualToString:@"13"]){
                shouxufei = [NSString stringWithFormat:@"0 %@",detailModel.name];
            } else {
                shouxufei = detailModel.service_charge;
            }

            cellWithRowOne.transferDymic = shouxufei;
        } else if ([title isEqualToString:@"区块"]){
            NSString *qukuai = @"";
            if ([ detailModel.status isEqualToString:@"3"]){
                qukuai = detailModel.block_number;
            }  else if ([detailModel.status isEqualToString:@"10"]){
                qukuai = @"无";
            } else if ([detailModel.status isEqualToString:@"11"]){
                qukuai = @"无";
            } else if ([detailModel.status isEqualToString:@"12"]){
                qukuai = @"无";
            } else if ([detailModel.status isEqualToString:@"13"]){
                qukuai = @"无";
            } else {
                qukuai = detailModel.block_number;
            }
            cellWithRowOne.transferDymic = qukuai;
        } else if ([title isEqualToString:@"交易时间"]){
            cellWithRowOne.transferDymic = [NSDate getTimeWithDuobaoString:detailModel.create_time  / 1000].length? [NSDate getTimeWithDuobaoString:detailModel.create_time / 1000]:@"没有交易时间";
        } else if ([title isEqualToString:@"交易ID"]){
            cellWithRowOne.transferDymic = detailModel.hashs.length?detailModel.hashs:@"无";
        }

        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [WalletTransferDetailHeaderTableViewCell calculationCellHeight];
    } else {
        NSString *title = [[self.transferDetailMutableArr objectAtIndex:indexPath.section] objectAtIndex:0];
        NSString *info = @"";
        if ([title isEqualToString:@"发送地址"]){
            info = detailModel.from;
        } else if ([title isEqualToString:@"接收地址"]){
            info = detailModel.to;
        } else if ([title isEqualToString:@"手续费"]){
            info = detailModel.service_charge;
        } else if ([title isEqualToString:@"区块"]){
            info = detailModel.block_number;
        } else if ([title isEqualToString:@"交易时间"]){
            info =  [NSDate getTimeWithDuobaoString:detailModel.create_time].length? [NSDate getTimeWithDuobaoString:detailModel.create_time]:@"没有交易时间";
        } else if ([title isEqualToString:@"交易ID"]){
            info = detailModel.hashs;
        }

        return [WalletTransferDetailSingleTableViewCell calculationCellHeightWithDymic:info];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.transferDetailTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.transferDetailMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.transferDetailMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}







-(void)getMessageInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] messageGetMessageInfoManagerWithId:self.transferMessageId block:^(WalletTransferDetailModel * _Nonnull model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->detailModel = model;
        [strongSelf.transferDetailTableView reloadData];
        
        if (model.type == 0) {
            strongSelf.barMainTitle = @"收款";
        }else{
            strongSelf.barMainTitle = @"转账";
        }
    }];
}












@end
