//
//  MessageNewListDetailModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/2.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MessageNewListDetailModel : FetchModel

@property (nonatomic,copy)NSString *banner;             /**< banner*/
@property (nonatomic,copy)NSString *title;              /**< 标题*/
@property (nonatomic,copy)NSString *time;
@property (nonatomic,copy)NSString *des;                /**< 详情*/

@end

NS_ASSUME_NONNULL_END
