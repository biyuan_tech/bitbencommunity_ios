//
//  MessageNewRootModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol MessageNewRootListSingleModel <NSObject>

@end

@interface MessageNewRootListSingleModel : FetchModel
@property (nonatomic,copy)NSString *content;
@property (nonatomic,assign)NSInteger count;
@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,assign)NSInteger type;

@end

@interface MessageNewRootModel : FetchModel

@property (nonatomic,copy)NSString *attention_count;
@property (nonatomic,copy)NSString *comment_count;
@property (nonatomic,copy)NSString *support_count;
@property (nonatomic,strong)NSArray<MessageNewRootListSingleModel> *list;

@end



NS_ASSUME_NONNULL_END
