//
//  BYEditLiveController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYEditLiveController.h"
#import "BYEditLiveViewModel.h"

@interface BYEditLiveController ()<NetworkAdapterSocketDelegate>

@end

@implementation BYEditLiveController

- (Class)getViewModelClass{
    return [BYEditLiveViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (BYCommonLiveModel *)model{
    if (!_model) {
        _model = [[BYCommonLiveModel alloc] init];
        _model.user_id = [AccountModel sharedAccountModel].account_id;
        if (!_isEdit) {
            _model.scene_type = _scene_type;
            _model.live_type = self.liveType;
        }
    }
    return _model;
}


@end
