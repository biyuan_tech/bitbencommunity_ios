//
//  BYEditLiveIntroController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/21.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYEditLiveIntroController.h"
#import "BYEditLiveIntroViewModel.h"

@interface BYEditLiveIntroController ()<NetworkAdapterSocketDelegate>

@end

@implementation BYEditLiveIntroController

- (Class)getViewModelClass{
    return [BYEditLiveIntroViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    if (!self.isEdit) {
        self.hasCancelSocket = NO;
    }
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.isActivity) {
        self.navigationTitle = @"会议简介";
        return;
    }
    switch (_model.live_type) {
        case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
        case BY_NEWLIVE_TYPE_AUDIO_PPT:
            self.navigationTitle = @"微直播";
            break;
        case BY_NEWLIVE_TYPE_ILIVE:
            self.navigationTitle = @"互动直播";
            break;
        case BY_NEWLIVE_TYPE_VIDEO:
            self.navigationTitle = @"推流直播";
            break;
        default:
            break;
    }
}

-(void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data{
    [[BYSIMManager shareManager] webSocketReceiveData:type data:data];
}

-(void)webSocketDidConnectedWithSocket:(WebSocketConnection *)webSocketConnection connectedStatus:(SocketConnectType)connectedStatus{
    if (webSocketConnection == [NetworkAdapter sharedAdapter].mainRootSocketConnection) {
        if (connectedStatus == SocketConnectTypeOpen) {
            [[BYSIMManager shareManager] webSocketDidConnectedWithSocket:webSocketConnection];
        }
    }
}


@end
