//
//  BYEditLiveIntroViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/21.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYEditLiveIntroViewModel.h"
#import <YYTextView.h>
#import "BYEditLiveIntroController.h"
#import "BYEditLiveWxController.h"
#import "BYEditLiveModel.h"
#import "BYNewLiveControllerV1.h"
#import "BYEditLiveController.h"
#import "BYCreateActivityBindWxController.h"

#define K_VC ((BYEditLiveIntroController *)S_VC)

@interface BYEditLiveIntroViewModel ()<BYCommonTableViewDelegate>

///** textView */
//@property (nonatomic ,strong) YYTextView *textView;
/** toastView */
@property (nonatomic ,strong) BYToastView *toastView;
/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** wxid */
@property (nonatomic ,copy) NSString *wxId;


@end

@implementation BYEditLiveIntroViewModel

#pragma mark - action
- (void)confirmBtnAction{
    if (K_VC.isActivity) {
        BYCreateActivityBindWxController *bindWxController = [[BYCreateActivityBindWxController alloc] init];
        bindWxController.isEdit = K_VC.isEdit;
        bindWxController.tableData = K_VC.tableData;
        bindWxController.activityModel = K_VC.activityModel;
        [S_V_NC pushViewController:bindWxController animated:YES];
        return;
    }
    self.toastView = [BYToastView toastViewPresentLoading];
    @weakify(self);
    [self uploadImage:^{
        @strongify(self);
        if (K_VC.isEdit) {
            [self updateLiveInfo];
            return ;
        }
        [self loadRequestNewLive];
    }];
}

- (void)pushLiveController{
    [self destoryController];
    BYEditLiveWxController *wxController = [[BYEditLiveWxController alloc] init];
    wxController.live_record_id = K_VC.model.live_record_id;
    wxController.wxId = self.wxId;
    wxController.navigationTitle = S_VC.navigationTitle;
    [S_V_NC pushViewController:wxController animated:YES];
}

- (void)destoryController{
    NSMutableArray *array = [NSMutableArray arrayWithArray:S_V_NC.viewControllers];
    [array enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[BYNewLiveControllerV1 class]] ||
            [obj isKindOfClass:[BYEditLiveController class]]) {
            [S_VC.rt_navigationController removeViewController:obj];
        }
    }];
}

- (NSArray *)getHostList{
    if (K_VC.model.scene_type == BY_NEWLIVE_MICRO_TYPE_APP) {
        return @[];
    }
    BYEditGuestInfoModel *tmpModel = K_VC.tableData[3];
    NSDictionary *dic = [tmpModel mj_keyValuesWithIgnoredKeys:@[@"headImg"]];
    BYCommonLiveGuestModel *hostModel = [BYCommonLiveGuestModel mj_objectWithKeyValues:dic];
    NSDictionary *data = [hostModel mj_keyValues];
    return @[data];
}

- (NSArray *)getGuestList{
    NSInteger beginIndex = K_VC.model.scene_type == BY_NEWLIVE_MICRO_TYPE_APP ? 3 : 4;
    NSMutableArray *array = [NSMutableArray array];
    for (int i = beginIndex; i < K_VC.tableData.count; i ++) {
        if ([K_VC.tableData[i] isKindOfClass:[BYEditGuestInfoModel class]]) {
            BYEditGuestInfoModel *tmpModel = K_VC.tableData[i];
            NSDictionary *dic = [tmpModel mj_keyValuesWithIgnoredKeys:@[@"headImg"]];
            BYCommonLiveGuestModel *guestModel = [BYCommonLiveGuestModel mj_objectWithKeyValues:dic];
            NSDictionary *data = [guestModel mj_keyValues];
            [array addObject:data];
        }
    }
    return [array copy];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"intro_change"]) {
        if (K_VC.isActivity) {
            K_VC.activityModel.activity_intro = nullToEmpty(param[@"text"]);
            return;
        }
        K_VC.model.live_intro = nullToEmpty(param[@"text"]);
    }
}
- (void)by_tableViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.dragging) {
        [S_V_VIEW endEditing:YES];
    }
}

#pragma mark - request
- (void)uploadImage:(void(^)(void))cb{
    NSMutableDictionary *modelData = [NSMutableDictionary dictionary];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [K_VC.tableData enumerateObjectsUsingBlock:^(BYCommonModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx == 0) { // 直播封面
            BYEditLiveModel *model = (BYEditLiveModel *)obj;
            if (model.coverImg && !model.live_cover_url.length) {
                [modelData setObject:obj forKey:@"live_cover_url"];
                [dic setObject:model.coverImg forKey:@"live_cover_url"];
            }
        }else if (idx == 2) {
            BYEditLiveModel *model = (BYEditLiveModel *)obj;
            if (model.adImg && !model.ad_img.length) {
                [modelData setObject:obj forKey:@"ad_img"];
                [dic setObject:model.adImg forKey:@"ad_img"];
            }
        }else {
            if (K_VC.model.scene_type == BY_NEWLIVE_MICRO_TYPE_WX) {
                if (![obj isKindOfClass:[BYEditGuestInfoModel class]]) {
                    return ;
                }
                if (idx == 3) {
                    BYEditGuestInfoModel *model = (BYEditGuestInfoModel *)obj;
                    if (model.headImg && !model.head_img.length) {
                        [modelData setObject:obj forKey:@"avatar_host"];
                        [dic setObject:model.headImg forKey:@"avatar_host"];
                    }
                }else {
                    BYEditGuestInfoModel *model = (BYEditGuestInfoModel *)obj;
                    if (model.headImg && !model.head_img.length) {
                        NSString *key = [NSString stringWithFormat:@"avatar_guest_%i",(int)idx];
                        [modelData setObject:obj forKey:key];
                        [dic setObject:model.headImg forKey:key];
                    }
                }
            }
        }
    }];
    if (!dic.allValues.count) {
        cb();
        return;
    }
    dispatch_group_t group = dispatch_group_create();
    // 控制最大并发数
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(dic.allValues.count);
    for (int i = 0; i < dic.allValues.count; i++) {
        OSSFileModel *fileModel = [[OSSFileModel alloc] init];
        NSString *str = dic.allKeys[i];
        fileModel.objcImage = dic.allValues[i];
        if (![str hasPrefix:@"avatar"]) {
            fileModel.objcName = [NSString stringWithFormat:@"image-%@-%i-%@",[NSDate by_stringFromDate:[NSDate by_date] dateformatter:@"yyyy-MM-dd-HH:mm:ss"],i,ACCOUNT_ID];
        }else {
            fileModel.objcName = [NSString stringWithFormat:@"avatar-%@-%i-%@",[NSDate by_stringFromDate:[NSDate by_date] dateformatter:@"yyyy-MM-dd-HH:mm:ss"],i,ACCOUNT_ID];
        }
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        // 用于监控请求线程在请求完成后回调
        // 多线程上传图片
        dispatch_group_async(group, dispatch_get_global_queue(0, 0), ^{
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            [[OSSManager sharedUploadManager] uploadImageManagerWithImageList:[@[fileModel] copy] withUrlBlock:^(NSArray *imgUrlArr) {
                BYCommonModel *obj = modelData[str];
                if ([str isEqualToString:@"live_cover_url"]) {
                    K_VC.model.live_cover_url = imgUrlArr[0];
                    ((BYEditLiveModel *)obj).live_cover_url = imgUrlArr[0];
                }else if ([str isEqualToString:@"ad_img"]){
                    K_VC.model.ad_img = imgUrlArr[0];
                    ((BYEditLiveModel *)obj).ad_img = imgUrlArr[0];
                }else {
                    ((BYEditGuestInfoModel *)obj).head_img = imgUrlArr[0];
                }
                dispatch_semaphore_signal(semaphore);
                dispatch_semaphore_signal(sema);
            }];
            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        });
    }
    // 上传七牛全部回调
    dispatch_group_notify(group, dispatch_get_global_queue(0, 0), ^{
        if (cb) {
            cb();
        }
    });
}

- (void)loadRequestNewLive{
    NSString *topic = @"";
    if (K_VC.model.topic_content.count) {
        for (int i = 0; i < K_VC.model.topic_content.count; i ++) {
            NSString *str = K_VC.model.topic_content[i];
            topic = [topic stringByAppendingString:[NSString stringWithFormat:@"%@%@",str,i == K_VC.model.topic_content.count - 1 ? @"" : @","]];
        }
    }
    NSArray *hostlist = [self getHostList];
    NSArray *guestlist = [self getGuestList];
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestNewLive:K_VC.model.live_title live_record_id:K_VC.model.live_record_id beginTime:K_VC.model.begin_time live_cover_url:K_VC.model.live_cover_url ad_img:K_VC.model.ad_img topics:topic intro:K_VC.model.live_intro liveType:K_VC.model.live_type scene_type:K_VC.model.scene_type host_list:hostlist guest_list:guestlist successBlock:^(id object) {
        @strongify(self);
        [self.toastView dissmissToastView];
        K_VC.model.live_record_id = object[@"live_record_id"];
        self.wxId = nullToEmpty(object[@"assistant_wechat"]);
        [self pushLiveController];

    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.toastView dissmissToastView];
    }];
}

- (void)updateLiveInfo{
    
    NSString *topic = @"";
    if (K_VC.model.topic_content.count) {
        for (int i = 0; i < K_VC.model.topic_content.count; i ++) {
            NSString *str = K_VC.model.topic_content[i];
            topic = [topic stringByAppendingString:[NSString stringWithFormat:@"%@%@",str,i == K_VC.model.topic_content.count - 1 ? @"" : @","]];
        }
    }
    NSArray *hostlist = [self getHostList];
    NSArray *guestlist = [self getGuestList];
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestNewLive:K_VC.model.live_title live_record_id:K_VC.model.live_record_id beginTime:K_VC.model.begin_time live_cover_url:K_VC.model.live_cover_url ad_img:K_VC.model.ad_img topics:topic intro:K_VC.model.live_intro liveType:K_VC.model.live_type scene_type:K_VC.model.scene_type host_list:hostlist guest_list:guestlist successBlock:^(id object) {
        @strongify(self);
        [self.toastView dissmissToastView];
        showToastView(@"修改成功", S_V_VIEW);
        UIViewController *controller = S_V_NC.viewControllers[S_V_NC.viewControllers.count - 3];
        [S_V_NC popToViewController:controller animated:YES];
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"修改失败", S_V_VIEW);
        [self.toastView dissmissToastView];
    }];
}

//- (void)createSIMGroup:(NSString *)record_id cb:(void(^)(BOOL isSuccess))cb{
//    @weakify(self);
//    [[BYSIMManager shareManager] createRoom:^(NSString * _Nonnull roomId) {
//        @strongify(self);
//        if (roomId.length) {
//            PDLog(@"视频音频直播群聊id创建成功");
//            [self updateGroupId:roomId recordId:record_id cb:^{
//                if (cb) cb(YES);
//            }];
//        }
//    } fail:^{
//        if (cb) cb(NO);
//    }];
//}

//- (void)updateGroupId:(NSString *)group_id recordId:(NSString *)recordId cb:(void(^)())cb{
//    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveSet:recordId param:@{@"group_id":group_id} successBlock:^(id object) {
//        PDLog(@"同步群聊id成功");
//        if (cb) cb();
//    } faileBlock:nil];
//}

- (void)setContentView{
    
    [self addTableView];
    
    [self addConfirmBtn];
}

- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.backgroundColor = kColorRGBValue(0xf2f4f5);
    self.tableView.group_delegate = self;
    [S_V_VIEW addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kSafe_Mas_Bottom(49), 0));
    }];
    
    NSString *intro = K_VC.isActivity ? K_VC.activityModel.activity_intro : K_VC.model.live_intro;
    self.tableView.tableData = [BYEditLiveModel getEditLiveIntroTableData:intro isActivity:K_VC.isActivity];
}


- (void)addConfirmBtn{
    UIButton *confirmBtn = [UIButton by_buttonWithCustomType];
    [confirmBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    [confirmBtn setBy_attributedTitle:@{@"title":@"下一步",
                                        NSFontAttributeName:[UIFont systemFontOfSize:15],
                                        NSForegroundColorAttributeName:kColorRGBValue(0xfefefe)
                                        } forState:UIControlStateNormal];
    confirmBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, kSafeAreaInsetsBottom, 0);
    [confirmBtn addTarget:self action:@selector(confirmBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [S_V_VIEW addSubview:confirmBtn];
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(49));
    }];
}

@end
