//
//  BYEditLiveWxController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/22.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYEditLiveWxController : BYCommonViewController

/** id */
@property (nonatomic ,copy) NSString *live_record_id;
/** wx */
@property (nonatomic ,copy) NSString *wxId;
/** 活动id */
@property (nonatomic ,copy) NSString *activity_id;


@end

NS_ASSUME_NONNULL_END
