//
//  BYLiveTypeDesController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/28.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveTypeDesController.h"

@interface BYLiveTypeDesController ()<UIScrollViewDelegate>

/** scrollView */
@property (nonatomic ,strong) UIScrollView *scrollView;
/** imageView */
@property (nonatomic ,strong) PDImageView *imageView;
/** navigationView */
@property (nonatomic ,strong) UIView *navigationView;
/** navigationTitle */
@property (nonatomic ,strong) UILabel *navigationTitleLab;
/** backBtn */
@property (nonatomic ,strong) UIButton *backBtn;


@end

@implementation BYLiveTypeDesController

- (void)viewDidLoad {
    [super viewDidLoad];
  
//    self.navigationTitle = @"我要合作";
    [self setContentView];
}

- (void)showNavigationViewBg:(BOOL)isShow{
    UIColor *color = isShow ? [UIColor whiteColor] : [UIColor clearColor];
    NSString *imageName = isShow ? @"icon_main_back" : @"icon_center_bbt_back";
    UIColor *titleColor = isShow ? [UIColor blackColor] : [UIColor whiteColor];
    [UIView animateWithDuration:0.1 animations:^{
        self.navigationView.backgroundColor = color;
        self.navigationTitleLab.textColor = titleColor;
        [self.backBtn setBy_imageName:imageName forState:UIControlStateNormal];
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    self.navigationController.navigationBar.tintColor = [UIColor clearColor];
}

- (void)backBtnActon{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat contentOffsetY = scrollView.contentOffset.y;
    [self showNavigationViewBg:contentOffsetY >= 70 ? YES : NO];
}


- (void)setContentView{
    
    NSString *title;
    NSString *imageName;
    switch (_live_type) {
        case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
            if (_micro_type == BY_NEWLIVE_MICRO_TYPE_APP) {
                title = @"APP微直播说明页";
                imageName = @"newlive_micro_app";
            }
            else{
                title = @"微信群直播说明页";
                imageName = @"newlive_micro_wx";
            }
            break;
        case BY_NEWLIVE_TYPE_ILIVE:
            title = @"个人手机直播说明页";
            imageName = @"newlive_ilive";
            break;
        case BY_NEWLIVE_TYPE_VIDEO:
            title = @"推流直播说明页";
            imageName = @"newlive_pushflow";
            break;
        default:
            break;
    }
    
    
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.delegate = self;
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    self.imageView = [[PDImageView alloc] init];
    [self.imageView by_setImageName:imageName];
    [self.scrollView addSubview:self.imageView];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.height.mas_greaterThanOrEqualTo(kCommonScreenWidth*self.imageView.image.size.height/self.imageView.image.size.width);
        make.right.mas_equalTo(self.view);
    }];
    
    [self.scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.imageView.mas_bottom).offset(0).priorityLow();
        make.bottom.mas_greaterThanOrEqualTo(self.view);
    }];
    
    [self addNavigationView:title];
}

- (void)addNavigationView:(NSString *)title{
    self.navigationView = [UIView by_init];
    [self.navigationView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.navigationView];
    [self.navigationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(kNavigationHeight);
    }];
    
    self.navigationTitleLab = [UILabel by_init];
    self.navigationTitleLab.font = [UIFont boldSystemFontOfSize:16];
    self.navigationTitleLab.textColor = [UIColor whiteColor];
    self.navigationTitleLab.text = title;
    self.navigationTitleLab.textAlignment = NSTextAlignmentCenter;
    [self.navigationView addSubview:self.navigationTitleLab];
    [self.navigationTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(150);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(44);
        make.top.mas_equalTo(kStatusBarHeight);
    }];
    
    UIButton *backBtn = [UIButton by_buttonWithCustomType];
    [backBtn setBy_imageName:@"icon_center_bbt_back" forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnActon) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationView addSubview:backBtn];
    self.backBtn = backBtn;
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.centerY.mas_equalTo(self.navigationTitleLab);
        make.width.height.mas_equalTo(44);
    }];
    
}

@end
