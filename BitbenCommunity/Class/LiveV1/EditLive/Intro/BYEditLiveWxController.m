//
//  BYEditLiveWxController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/22.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYEditLiveWxController.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>
#import "BYLiveDetailController.h"
#import "BYNewLiveControllerV1.h"
#import "BYEditLiveController.h"
#import "BYEditLiveIntroController.h"

#import "BYCreateActivityController.h"
#import "BYCreateActivityBindWxController.h"
#import "BYActivityDetailController.h"

@interface BYEditLiveWxController ()

@end

@implementation BYEditLiveWxController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self setContentView];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.popGestureRecognizerEnale = NO;
    [self leftBarButtonWithTitle:@"" barNorImage:nil barHltImage:nil action:nil];
}

- (void)destoryController{
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    if (_live_record_id.length) {
        [array enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[BYNewLiveControllerV1 class]] ||
                [obj isKindOfClass:[BYEditLiveController class]] ||
                [obj isKindOfClass:[BYEditLiveIntroController class]]) {
                [self.rt_navigationController removeViewController:obj];
            }
        }];
        return;
    }
    [array enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[BYCreateActivityController class]] ||
            [obj isKindOfClass:[BYEditLiveIntroController class]] ||
            [obj isKindOfClass:[BYCreateActivityBindWxController class]]) {
            [self.rt_navigationController removeViewController:obj];
        }
    }];
   
}

- (void)copyBtnAction{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setString:self.wxId];
    showToastView(@"已复制微信号", self.view);
}

- (void)confirmBtnAction{
//    BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
//    liveDetailController.live_record_id = self.live_record_id;
//    [self.navigationController pushViewController:liveDetailController animated:YES];
    
    if (_activity_id.length) {
        [self pushActivity];
        return;
    }
    // push 出来的界面
    if (self.tabBarController) {
        [self destoryController];
        BYLiveDetailController *liveController = [[BYLiveDetailController alloc] init];
        liveController.live_record_id = self.live_record_id;
        liveController.isHost = YES;
        liveController.navigationTitle = self.navigationTitle;
        [self.navigationController pushViewController:liveController animated:YES];
        
        return;
    }
    // present 出来的界面
    [self dismissViewControllerAnimated:NO completion:nil];
    
    [self destoryController];
    BYLiveDetailController *liveController = [[BYLiveDetailController alloc] init];
    liveController.live_record_id = self.live_record_id;
    liveController.isHost = YES;
    liveController.navigationTitle = self.navigationTitle;
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    [currentController.navigationController pushViewController:liveController animated:YES];
}

- (void)pushActivity{
    // push 出来的界面
    if (self.tabBarController) {
        [self destoryController];
        BYActivityDetailController *activityController = [[BYActivityDetailController alloc] init];
        activityController.activiry_id = self.activity_id;
        [self.navigationController pushViewController:activityController animated:YES];
        
        return;
    }
    // present 出来的界面
    [self dismissViewControllerAnimated:NO completion:nil];
    
    [self destoryController];
    BYActivityDetailController *activityController = [[BYActivityDetailController alloc] init];
    activityController.activiry_id = self.activity_id;
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    [currentController.navigationController pushViewController:activityController animated:YES];
}

- (void)setContentView{
    YYLabel *titleLab = [[YYLabel alloc] init];
    titleLab.font = [UIFont systemFontOfSize:14];
    titleLab.textColor = kColorRGBValue(0xa1a1a1);
    titleLab.numberOfLines = 0;
    
    NSString *text = @"请添加“币本直播小助手”为微信好友，经过审核确认直播不违规我们将会开启该直播，届时该直播将会对所有用户可见。";
    NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(text)];
    messageAttributed.yy_lineSpacing = 10.0f;
    messageAttributed.yy_font = [UIFont systemFontOfSize:14];
    messageAttributed.yy_color = kColorRGBValue(0xa1a1a1);
    YYTextContainer *textContainer = [YYTextContainer new];
    textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
    YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
    titleLab.attributedText = messageAttributed;
    [self.view addSubview:titleLab];
    
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(ceil(layout.textBoundingSize.height));
    }];
    
    UIView *bgView = [UIView by_init];
    [bgView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(49);
        make.top.mas_equalTo(titleLab.mas_bottom).offset(18);
    }];
    
    UILabel *leftTitleLab = [UILabel by_init];
    leftTitleLab.font = [UIFont boldSystemFontOfSize:14];
    leftTitleLab.textColor = kColorRGBValue(0x323232);
    leftTitleLab.text = @"币本直播小助手";
    [bgView addSubview:leftTitleLab];
    CGSize size = [leftTitleLab.text getStringSizeWithFont:leftTitleLab.font maxWidth:MAXFLOAT];
    [leftTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.height.mas_equalTo(leftTitleLab.font.pointSize);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(ceil(size.width));
    }];
    
    UILabel *wxLab = [UILabel by_init];
    wxLab.font = [UIFont systemFontOfSize:14];
    wxLab.textColor = kColorRGBValue(0xc9c9c9);
    wxLab.text = self.wxId;
    [bgView addSubview:wxLab];
    [wxLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftTitleLab.mas_right).offset(15);
        make.width.mas_equalTo(stringGetWidth(wxLab.text, 14));
        make.height.mas_equalTo(wxLab.font.pointSize);
        make.centerY.mas_equalTo(0);
    }];
    
    UIButton *copyBtn = [UIButton by_buttonWithCustomType];
    [copyBtn setBy_attributedTitle:@{@"title":@"复制",
                                     NSFontAttributeName:[UIFont systemFontOfSize:15],
                                     NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                     } forState:UIControlStateNormal];
    [copyBtn addTarget:self action:@selector(copyBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:copyBtn];
    [bgView sendSubviewToBack:copyBtn];
    [copyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.mas_equalTo(0);
        make.width.mas_equalTo(60);
    }];
    
    UIButton *confirmBtn = [UIButton by_buttonWithCustomType];
    [confirmBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    [confirmBtn setBy_attributedTitle:@{@"title":@"完成",
                                        NSFontAttributeName:[UIFont systemFontOfSize:15],
                                        NSForegroundColorAttributeName:kColorRGBValue(0xfefefe)
                                        } forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(confirmBtnAction) forControlEvents:UIControlEventTouchUpInside];
    confirmBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, kSafeAreaInsetsBottom, 0);
    [self.view addSubview:confirmBtn];
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(49));
    }];
}

@end
