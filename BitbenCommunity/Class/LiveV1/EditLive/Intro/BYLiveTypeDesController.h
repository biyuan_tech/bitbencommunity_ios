//
//  BYLiveTypeDesController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/28.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYLiveTypeDesController : BYCommonViewController

/** 直播类型 */
@property (nonatomic ,assign) BY_NEWLIVE_TYPE live_type;
/** 微直播类型 */
@property (nonatomic ,assign) BY_NEWLIVE_MICRO_TYPE micro_type;


@end

NS_ASSUME_NONNULL_END
