//
//  BYEditLiveModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYEditLiveModel.h"

@implementation BYEditGuestInfoModel

@end

@implementation BYEditLiveModel

+ (NSArray *)getNewLiveTableData:(BY_NEWLIVE_MICRO_TYPE)scene_type{
    if (scene_type == BY_NEWLIVE_MICRO_TYPE_APP) {
        return [BYEditLiveModel getEditDefultTableData_APP];
    }
    return [BYEditLiveModel getEditDefultTableData_WX];
}

+ (NSArray *)getEditDefultTableData_APP{
    NSMutableArray *tableData = [NSMutableArray array];
    for (int i = 0; i < 4; i ++) {
        BYEditLiveModel *model = [[BYEditLiveModel alloc] init];
        switch (i) {
            case 0:
                model.cellString = @"BYEditLiveTitleCell";
                model.cellHeight = 130;
                break;
            case 1:
                model.cellString = @"BYEditLiveTopicsCell";
                model.cellHeight = 59;
                break;
            case 2:
                model.cellString = @"BYEditLiveAdImgCell";
                model.cellHeight = 130;
                break;
            default:
                model.cellString = @"BYEditLiveAddCell";
                model.cellHeight = 59;
                break;
        }
        [tableData addObject:model];
    }
    return tableData;
}

+ (NSArray *)getEditDefultTableData_WX{
    NSMutableArray *tableData = [NSMutableArray array];
    for (int i = 0; i < 5; i ++) {
        BYEditLiveModel *model = [[BYEditLiveModel alloc] init];
        switch (i) {
            case 0:
                model.cellString = @"BYEditLiveTitleCell";
                model.cellHeight = 130;
                [tableData addObject:model];
                break;
            case 1:
                model.cellString = @"BYEditLiveTopicsCell";
                model.cellHeight = 59;
                [tableData addObject:model];
                break;
            case 2:
                model.cellString = @"BYEditLiveAdImgCell";
                model.cellHeight = 130;
                [tableData addObject:model];
                break;
            case 3:
            {
                BYEditGuestInfoModel *infoModel = [[BYEditGuestInfoModel alloc] init];
                infoModel.cellString = @"BYEditLiveHostWxCell";
                infoModel.cellHeight = 130;
                [tableData addObject:infoModel];
            }
                break;
            default:
                model.cellString = @"BYEditLiveAddCell";
                model.cellHeight = 59;
                [tableData addObject:model];
                break;
        }
    }
    return tableData;
}

+ (BYEditGuestInfoModel *)getSingleGuestCellData:(BY_NEWLIVE_MICRO_TYPE)scene_type{
    BYEditGuestInfoModel *model = [[BYEditGuestInfoModel alloc] init];
    if (scene_type == BY_NEWLIVE_MICRO_TYPE_APP) {
        model.cellString = @"BYEditLiveHostAppCell";
        model.cellHeight = 116;
        model.isExist = -1;
    }else{
        model.cellString = @"BYEditLiveGuestWxCell";
        model.cellHeight = 166;
        model.isExist = 1;
    }
    return model;
}

+ (BYEditGuestInfoModel *)getSingleGuestCreateActivityCellData{
    BYEditGuestInfoModel *model = [[BYEditGuestInfoModel alloc] init];
    model.cellString = @"BYCreateActivityGuestCell";
    model.cellHeight = 130;
    model.isExist = 1;
    return model;
}

+ (NSArray *)getEditViewTableData:(BYCommonLiveModel *)liveModel{
    NSMutableArray *tableData = [NSMutableArray array];
    
    for (int i = 0; i < 4; i ++) {
        BYEditLiveModel *model = [[BYEditLiveModel alloc] init];
        switch (i) {
            case 0:
                model.live_cover_url = liveModel.live_cover_url;
                model.live_title = liveModel.live_title;
                model.begin_time = liveModel.begin_time;
                model.cellString = @"BYEditLiveTitleCell";
                model.cellHeight = 130;
                break;
            case 1:
                model.topic_content = liveModel.topic_content;
                model.cellString = @"BYEditLiveTopicsCell";
                model.cellHeight = 59;
                break;
            case 2:
                model.ad_img = liveModel.ad_img;
                model.cellString = @"BYEditLiveAdImgCell";
                model.cellHeight = 130;
                break;
            default:
                model.cellString = @"BYEditLiveAddCell";
                model.cellHeight = 59;
                break;
        }
        [tableData addObject:model];
    }
    
    if (liveModel.scene_type == BY_NEWLIVE_MICRO_TYPE_WX && liveModel.host_list.count) {
        [liveModel.host_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dic = [obj mj_keyValues];
            BYEditGuestInfoModel *model = [BYEditGuestInfoModel mj_objectWithKeyValues:dic];
            model.isExist = 1;
            model.cellString = @"BYEditLiveHostWxCell";
            model.cellHeight = 130;
            [tableData insertObject:model atIndex:tableData.count - 1];
        }];
    }
    
    if (liveModel.interviewee_list.count) {
        [liveModel.interviewee_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dic = [obj mj_keyValues];
            BYEditGuestInfoModel *model = [BYEditGuestInfoModel mj_objectWithKeyValues:dic];
            if (liveModel.scene_type == BY_NEWLIVE_MICRO_TYPE_APP) {
                model.cellString = @"BYEditLiveHostAppCell";
                model.cellHeight = 116;
                model.isExist = 1;
            }else{
                model.cellString = @"BYEditLiveGuestWxCell";
                model.cellHeight = 166;
                model.isExist = 1;
            }
            [tableData insertObject:model atIndex:tableData.count - 1];
        }];
    }
    
    return tableData;
}

+ (NSArray *)getEditLiveIntroTableData:(NSString *)intro isActivity:(BOOL)isActivity{
    BYEditLiveModel *model = [[BYEditLiveModel alloc] init];
    model.cellString = @"BYEditLiveIIntroCell";
    model.live_intro = nullToEmpty(intro);
    model.cellHeight = kCommonScreenHeight - kNavigationHeight - kSafe_Mas_Bottom(49);
    model.title = !isActivity ? @"请点击填写直播简介（选填）" : @"请点击填写会议简介（选填）";
    return @[model];
}

@end
