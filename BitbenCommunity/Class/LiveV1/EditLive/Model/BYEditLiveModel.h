//
//  BYEditLiveModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYEditGuestInfoModel : BYCommonModel

/** 嘉宾id */
@property (nonatomic ,copy) NSString *user_id;
/** 嘉宾头像 */
@property (nonatomic ,copy) NSString *head_img;
/** 嘉宾本地选择头像 */
@property (nonatomic ,strong) UIImage *headImg;
/** 嘉宾简介 */
@property (nonatomic ,copy) NSString *intro;
/** 嘉宾昵称 */
@property (nonatomic ,copy) NSString *nickname;
/** 用户类型 */
@property (nonatomic ,assign) BY_NEWLIVE_MICRO_TYPE user_type;
/** 身份类型 */
@property (nonatomic ,assign) BY_IDENTITY_TYPE identity_type;
/** id是否不存在(-1 未校验，0 不存在 1 存在) */
@property (nonatomic ,assign) NSInteger isExist;

@end

@interface BYEditLiveModel : BYCommonModel

/** 直播封面图 */
@property (nonatomic ,copy) NSString *live_cover_url;
/** 封面图 */
@property (nonatomic ,strong) UIImage *coverImg;
/** 直播标题 */
@property (nonatomic ,copy) NSString *live_title;
/** 直播时间 */
@property (nonatomic ,copy) NSString *begin_time;
/** 话题 */
@property (nonatomic ,strong) NSArray *topic_content;
/** 直播宣传图 */
@property (nonatomic ,copy) NSString *ad_img;
@property (nonatomic ,strong) UIImage *adImg;

/** 直播简介 */
@property (nonatomic ,copy) NSString *live_intro;


+ (NSArray *)getNewLiveTableData:(BY_NEWLIVE_MICRO_TYPE)scene_type;

+ (NSArray *)getEditLiveIntroTableData:(NSString *)intro isActivity:(BOOL)isActivity;

+ (BYEditGuestInfoModel *)getSingleGuestCellData:(BY_NEWLIVE_MICRO_TYPE)scene_type;

+ (NSArray *)getEditViewTableData:(BYCommonLiveModel *)liveModel;

+ (BYEditGuestInfoModel *)getSingleGuestCreateActivityCellData;

//- (BYEditLiveModel *)

@end


NS_ASSUME_NONNULL_END
