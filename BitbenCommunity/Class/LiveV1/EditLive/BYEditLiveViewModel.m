//
//  BYEditLiveViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYEditLiveViewModel.h"
#import "BYEditLiveController.h"
#import "GWAssetsLibraryViewController.h"
#import "ArticleDetailHuatiListViewController.h"
#import "GWAssetsImgSelectedViewController.h"
#import "BYLiveDetailController.h"
#import "BYLiveRoomHomeController.h"
#import "BYLiveController.h"
#import "BYNewLiveControllerV1.h"
#import "BYEditLiveIntroController.h"

#import "BYSelectTimePickerView.h"
#import <YYTextView.h>

#import "BYEditLiveModel.h"

#define K_VC ((BYEditLiveController *)S_VC)
@interface BYEditLiveViewModel ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** 时间选择器 */
@property (nonatomic ,strong) BYSelectTimePickerView *timePickerView;
///** 封面图 */
//@property (nonatomic ,strong) UIImage *coverImg;
///** 海报 */
//@property (nonatomic ,strong) UIImage *adImg;


@end

@implementation BYEditLiveViewModel



- (void)setContentView{
    [self addTableView];
    [self addConfirmBtn];
}

- (void)pushLiveController{
    
    NSString *title = @"";
    switch (K_VC.model.live_type) {
        case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
        case BY_NEWLIVE_TYPE_AUDIO_PPT:
            title = @"微直播";
            break;
        case BY_NEWLIVE_TYPE_VIDEO:
            title = @"推流直播";
            break;
        default:
            title = @"个人互动直播";
            break;
    }
    
    // push 出来的界面
    if (S_VC.tabBarController) {
        [self destoryController];
        BYLiveDetailController *liveController = [[BYLiveDetailController alloc] init];
        liveController.live_record_id = K_VC.model.live_record_id;
        liveController.isHost = YES;
        liveController.navigationTitle = title;
        [S_V_NC pushViewController:liveController animated:YES];

        return;
    }
    // present 出来的界面
    [S_VC dismissViewControllerAnimated:NO completion:nil];

    [self destoryController];
    BYLiveDetailController *liveController = [[BYLiveDetailController alloc] init];
    liveController.live_record_id = K_VC.model.live_record_id;
    liveController.isHost = YES;
    liveController.navigationTitle = title;
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    [currentController.navigationController pushViewController:liveController animated:YES];

}

- (void)destoryController{
    NSMutableArray *array = [NSMutableArray arrayWithArray:S_V_NC.viewControllers];
    [array enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[BYNewLiveControllerV1 class]]) {
            [S_VC.rt_navigationController removeViewController:obj];
        }
    }];
}

// 校验添加的嘉宾是否通过检测
- (BOOL)verifyGuest{
    __block BOOL result = YES;
    [self.tableView.tableData enumerateObjectsUsingBlock:^(BYCommonModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([model isKindOfClass:[BYEditGuestInfoModel class]]) {
            BYEditGuestInfoModel *tmpModel = (BYEditGuestInfoModel *)model;
            if (K_VC.model.scene_type == BY_NEWLIVE_MICRO_TYPE_APP) {
                if (tmpModel.isExist == -1 || tmpModel.isExist == 0) {
                    result = NO;
                    [S_VC showToastView:@"存在未通过的嘉宾信息"];
                    *stop = YES;
                }
                if (!tmpModel.user_id.length || !tmpModel.intro.length) {
                    [S_VC showToastView:@"请完善嘉宾信息"];
                    result = NO;
                    *stop = YES;
                }
            }else{
                if (idx == 3) {
                    if (!tmpModel.user_id.length || !tmpModel.nickname.length) {
                        [S_VC showToastView:@"请完善主持人信息"];
                        result = NO;
                        *stop = YES;
                    }
                }else {
                    if (!tmpModel.user_id.length || !tmpModel.intro.length || !tmpModel.nickname.length) {
                        [S_VC showToastView:@"请完善嘉宾信息"];
                        result = NO;
                        *stop = YES;
                    }
                }
            }
        }
    }];
    return result;
}

- (BOOL)verifyImageData{
    __block BOOL result = YES;
    [self.tableView.tableData enumerateObjectsUsingBlock:^(BYCommonModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([model isKindOfClass:[BYEditLiveModel class]]) {
            BYEditLiveModel *tmpModel = (BYEditLiveModel *)model;
            if (!tmpModel.coverImg &&
                !tmpModel.live_cover_url.length &&
                idx == 0) {
                result = NO;
                [S_VC showToastView:@"请上传封面图"];
                *stop = YES;
            }
        }
        else if ([model isKindOfClass:[BYEditGuestInfoModel class]]) {
            BYEditGuestInfoModel *tmpModel = (BYEditGuestInfoModel *)model;
            if (K_VC.model.scene_type == BY_NEWLIVE_MICRO_TYPE_WX) {
                if (!tmpModel.headImg && !tmpModel.head_img.length) {
                    result = NO;
                    if (idx == 3) {
                        [S_VC showToastView:@"请上传主持人头像"];
                    }else{
                        [S_VC showToastView:@"请上传嘉宾头像"];
                    }
                    *stop = YES;
                }
            }
        }
    }];
    return result;
}
// 将cell中嘉宾信息转换到model中
//- (void)transfromGuestData{
//    NSMutableArray *array = [NSMutableArray array];
//    NSMutableArray *hostData = [NSMutableArray array];
//    [self.tableView.tableData enumerateObjectsUsingBlock:^(BYCommonModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
//        if (K_VC.model.scene_type == BY_NEWLIVE_MICRO_TYPE_APP) {
//            if ([model isKindOfClass:[BYEditGuestInfoModel class]]) {
//                BYEditGuestInfoModel *tmpModel = (BYEditGuestInfoModel *)model;
//                NSDictionary *dic = [tmpModel mj_keyValues];
//                BYCommonLiveGuestModel *guestModel = [BYCommonLiveGuestModel mj_objectWithKeyValues:dic];
//                [array addObject:guestModel];
//            }
//        }else {
//
//        }
//
//    }];
//    K_VC.model.interviewee_list = [array copy];
//}

#pragma mark - action
- (void)confirmBtnAction{
    if (!K_VC.model.live_title.length) {
        [S_VC showToastView:@"请填写直播名称"];
        return;
    }
    if (!K_VC.model.begin_time.length) {
        [S_VC showToastView:@"请选择开播时间"];
        return;
    }

    if (![self verifyImageData]) {
        return;
    }
    
    if (!K_VC.model.topic_content.count) {
        [S_VC showToastView:@"请选择话题"];
        return;
    }
    
    if (![self verifyGuest]) {
        return;
    }
    
//    [self transfromGuestData];
    
    if (!K_VC.isEdit) {
        K_VC.model.scene_type = K_VC.scene_type;
        K_VC.model.live_type = K_VC.liveType;
    }
    
    BYEditLiveIntroController *editLiveIntroController = [[BYEditLiveIntroController alloc] init];
    editLiveIntroController.isEdit = K_VC.isEdit;
    editLiveIntroController.model = K_VC.model;
    editLiveIntroController.tableData = self.tableView.tableData;
    [S_V_NC pushViewController:editLiveIntroController animated:YES];
    
}

- (void)selectImage:(NSIndexPath *)indexPath{
    BYCommonModel *model = self.tableView.tableData[indexPath.row];
    UIImage *image;
    BOOL canCropper = NO;
    if (indexPath.row == 0) {
        canCropper = YES;
        if (((BYEditLiveModel *)model).coverImg) {
            image = ((BYEditLiveModel *)model).coverImg;
        }
    }else if (indexPath.row == 2) {
        if (((BYEditLiveModel *)model).adImg) {
            image = ((BYEditLiveModel *)model).adImg;
        }
    }else {
        if (((BYEditGuestInfoModel *)model).headImg) {
            image = ((BYEditGuestInfoModel *)model).headImg;
        }
    }
    if (image) {
        UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *albumAction = [UIAlertAction actionWithTitle:@"从手机相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @weakify(self);
            [self assetsLibrarySelImgCanCropper:canCropper cb:^(UIImage *image) {
                @strongify(self);
                [self setImageAtIndexPath:indexPath image:image];
            }];
        }];
        
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [self setImageAtIndexPath:indexPath image:nil];
        }];
        
        [sheetController addAction:albumAction];
        [sheetController addAction:confirmAction];
        [sheetController addAction:cancelAction];
        [S_V_NC presentViewController:sheetController animated:YES completion:nil];
    }else{
        @weakify(self);
        [self assetsLibrarySelImgCanCropper:canCropper cb:^(UIImage *image) {
            @strongify(self);
            [self setImageAtIndexPath:indexPath image:image];
        }];
    }
}

- (void)setImageAtIndexPath:(NSIndexPath *)indexPath image:(UIImage *)image{
    BYCommonModel *model = self.tableView.tableData[indexPath.row];
    if (indexPath.row == 0) {
        ((BYEditLiveModel *)model).coverImg = image;
        ((BYEditLiveModel *)model).live_cover_url = @"";
    }else if (indexPath.row == 2) {
        ((BYEditLiveModel *)model).adImg = image;
        ((BYEditLiveModel *)model).ad_img = @"";
    }else {
        ((BYEditGuestInfoModel *)model).headImg = image;
        ((BYEditGuestInfoModel *)model).head_img = @"";
    }
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)assetsLibrarySelImgCanCropper:(BOOL)canCropper cb:(void(^)(UIImage *image))cb{
    CGFloat cropperH = kCommonScreenWidth*180/345;
    CGRect cropperRect = CGRectMake(0, (kCommonScreenHeight - cropperH)/2, kCommonScreenWidth, cropperH);
    GWAssetsLibraryViewController *assetVC = [[GWAssetsLibraryViewController alloc] init];
    assetVC.isCanCut = canCropper;
    assetVC.cropFrame = cropperRect;
    [assetVC selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
        if (cb) {
            cb(selectedImgArr[0]);
        }
    }];
    [S_V_NC pushViewController:assetVC animated:YES];

}

- (void)delGuestData:(NSIndexPath *)indexPath{
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:@"删除提醒" message:@"确定要删除嘉宾吗？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.tableView.notAutoReload = YES;
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
        [array removeObjectAtIndex:indexPath.row];
        self.tableView.tableData = [array copy];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
    
    [sheetController addAction:confirmAction];
    [sheetController addAction:cancelAction];
    [S_V_NC presentViewController:sheetController animated:YES completion:nil];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1) { // 选择话题
        BYEditLiveModel *model = tableView.tableData[indexPath.row];
        ArticleDetailHuatiListViewController *huatiListVC = [[ArticleDetailHuatiListViewController alloc]init];
        huatiListVC.transferSelectedHuatiArr = model.topic_content;
        huatiListVC.maxCount = 3;
        @weakify(self);
        [huatiListVC actionClickWithSelectedBlock:^(NSArray *selectedArr) {
            @strongify(self);
            model.topic_content = selectedArr;
            K_VC.model.topic_content = selectedArr;
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        }];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:huatiListVC];
        [S_V_NC presentViewController:nav animated:YES completion:NULL];
    }else if (indexPath.row == tableView.tableData.count - 1) { // 添加嘉宾
        if (K_VC.scene_type == BY_NEWLIVE_MICRO_TYPE_APP) {
            if (tableView.tableData.count >= 24) {
                showToastView(@"嘉宾数达到上限", S_V_VIEW);
                return;
            }
        }else{
            if (tableView.tableData.count >= 25) {
                showToastView(@"嘉宾数达到上限", S_V_VIEW);
                return;
            }
        }
        [self.tableView beginUpdates];
        tableView.notAutoReload = YES;
        BYEditGuestInfoModel *guestModel = [BYEditLiveModel getSingleGuestCellData:K_VC.model.scene_type];
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
        [array insertObject:guestModel atIndex:array.count - 1];
        self.tableView.tableData = array;
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:array.count - 2 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
        [self.tableView endUpdates];
        
        [self.tableView scrollToBottom:YES];
    }
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    BYEditLiveModel *model = tableView.tableData[indexPath.row];
    if ([actionName isEqualToString:@"selTime"]) { // 选择开播时间
        [self.timePickerView showAnimation];
        [S_V_VIEW endEditing:YES];
    }else if ([actionName isEqualToString:@"selImgAction"]) { // 选择图片
        [self selectImage:indexPath];
        [S_V_VIEW endEditing:YES];
    }else if ([actionName isEqualToString:@"textField_change"]) { // 直播名称编辑
        model.live_title = nullToEmpty(param[@"text"]);
        K_VC.model.live_title = nullToEmpty(param[@"text"]);
    }else if ([actionName isEqualToString:@"intro_change"]) { // 直播简介编辑
        model.live_intro = nullToEmpty(param[@"text"]);
        K_VC.model.live_intro = nullToEmpty(param[@"text"]);
    }else if ([actionName isEqualToString:@"delCellAction"]) { // 删除嘉宾
        [self delGuestData:indexPath];
    }
}

- (void)by_tableViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.dragging) {
        [S_V_VIEW endEditing:YES];
    }
}

#pragma mark - configUI

- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.backgroundColor = kColorRGBValue(0xf2f4f5);
    self.tableView.group_delegate = self;
    [S_V_VIEW addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kSafe_Mas_Bottom(49), 0));
    }];
    
    UIButton *confimBtn = [UIButton by_buttonWithCustomType];
    [confimBtn setBy_attributedTitle:@{@"title":@"完成",
                                       NSFontAttributeName:[UIFont systemFontOfSize:15],
                                       NSForegroundColorAttributeName:[UIColor whiteColor]
                                       } forState:UIControlStateNormal];
    [confimBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    [S_V_VIEW addSubview:confimBtn];
    if (!K_VC.isEdit) {
        self.tableView.tableData = [BYEditLiveModel getNewLiveTableData:K_VC.scene_type];
    }else{
        self.tableView.tableData = [BYEditLiveModel getEditViewTableData:K_VC.model];
    }
}

- (void)addConfirmBtn{
    UIButton *confirmBtn = [UIButton by_buttonWithCustomType];
    [confirmBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    [confirmBtn setBy_attributedTitle:@{@"title":@"下一步",
                                        NSFontAttributeName:[UIFont systemFontOfSize:15],
                                        NSForegroundColorAttributeName:kColorRGBValue(0xfefefe)
                                        } forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(confirmBtnAction) forControlEvents:UIControlEventTouchUpInside];
    confirmBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, kSafeAreaInsetsBottom, 0);
    [S_V_VIEW addSubview:confirmBtn];
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(49));
    }];
}

- (BYSelectTimePickerView *)timePickerView{
    if (!_timePickerView) {
        _timePickerView = [[BYSelectTimePickerView alloc] initWithFathureView:kCommonWindow];
        @weakify(self);
        _timePickerView.didSelectTimeString = ^(NSString *data,NSString *showTimeData) {
            @strongify(self);
            BYEditLiveModel *model = self.tableView.tableData[0];
            model.begin_time = showTimeData;
            K_VC.model.begin_time = data;
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        };
    }
    return _timePickerView;
}



@end
