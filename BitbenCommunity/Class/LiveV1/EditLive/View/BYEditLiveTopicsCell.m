//
//  BYEditLiveTopicsCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYEditLiveTopicsCell.h"
#import "BYEditLiveModel.h"

static NSInteger kBaseTag = 0x685;
@interface BYEditLiveTopicsCell ()

/** model */
@property (nonatomic ,strong) BYEditLiveModel *model;
/** titleLab */
@property (nonatomic ,strong) UILabel *titleLab;


@end

@implementation BYEditLiveTopicsCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYEditLiveModel *model = object[indexPath.row];
    self.model = model;
    [self addTagsView];
}

- (void)addTagsView{
    NSArray *topicArr = self.model.topic_content;
    for (UIView *view in self.contentView.subviews) {
        if (view.tag >= kBaseTag && view.tag < kBaseTag + 20) {
            [view removeFromSuperview];
        }
    }
    
    for (int i = 0; i < topicArr.count; i ++) {
        UIButton *tagView = [UIButton by_buttonWithCustomType];
        NSString *title = topicArr[i];
        [tagView setTitle:title forState:UIControlStateNormal];
        tagView.titleLabel.font = [UIFont systemFontOfSize:11];
        [tagView setTitleColor:kColorRGBValue(0x4f86cb) forState:UIControlStateNormal];
        [tagView setBackgroundColor:kColorRGBValue(0xf7fbff)];
        CGFloat width = stringGetWidth(title, 11) + 12;
        CGFloat height = stringGetHeight(title, 11) + 10;
        tagView.layer.cornerRadius = 2;
        tagView.layer.borderWidth = 0.5;
        tagView.layer.borderColor = kColorRGBValue(0x92b8e8).CGColor;
//        [tagView addTarget:self action:@selector(tagBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        UIButton *lastTagView = [self viewWithTag:kBaseTag + i - 1];
        [self.contentView addSubview:tagView];
        tagView.tag = kBaseTag + i;
        @weakify(self);
        [tagView mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            if (lastTagView) {
                make.left.mas_equalTo(lastTagView.mas_right).mas_offset(5);
            }else{
                make.left.mas_equalTo(105);
            }
            make.centerY.mas_equalTo(self.titleLab);
        }];
    }
}

- (void)setContentView{
    UIView *topView = [UIView by_init];
    [topView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.contentView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(10);
    }];
    
    UIImageView *mustNameImgView = [UIImageView by_init];
    [mustNameImgView by_setImageName:@"newlive_must"];
    [self.contentView addSubview:mustNameImgView];
    [mustNameImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(32);
        make.width.height.mas_equalTo(6);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    titleLab.textColor = kColorRGBValue(0x333333);
    titleLab.text = @"选择话题";
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(mustNameImgView);
        make.left.mas_equalTo(mustNameImgView.mas_right).mas_offset(12);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.width.mas_equalTo(stringGetWidth(titleLab.text, 14));
    }];
    
    UIImageView *rightIcon = [UIImageView by_init];
    [rightIcon by_setImageName:@"livehome_rightIcon"];
    [self.contentView addSubview:rightIcon];
    [rightIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(6);
        make.height.mas_equalTo(12);
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(titleLab);
    }];
}

@end
