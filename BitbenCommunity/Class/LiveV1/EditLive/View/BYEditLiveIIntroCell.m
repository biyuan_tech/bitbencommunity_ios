//
//  BYEditLiveIIntroCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYEditLiveIIntroCell.h"
#import "BYEditLiveModel.h"
#import <YYTextView.h>

@interface BYEditLiveIIntroCell ()<YYTextViewDelegate>

/** textView */
@property (nonatomic ,strong) YYTextView *textView;
/** model */
@property (nonatomic ,strong) BYEditLiveModel *model;


@end

@implementation BYEditLiveIIntroCell

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
        [self addNSNotification];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYEditLiveModel *model = object[indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    self.textView.text = nullToEmpty(model.live_intro);
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:model.title attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0xa3a3a3)}];
    self.textView.placeholderAttributedText = attributedString;
}

#pragma mark - YYTextViewDelegate
- (void)textViewDidChange:(YYTextView *)textView{
    NSInteger maxlength = 500;
    if (textView.text.length > maxlength) {
        UITextRange *markedRange = [textView markedTextRange];
        if (markedRange) {
            return;
        }
        NSRange range = [textView.text rangeOfComposedCharacterSequenceAtIndex:maxlength];
        textView.text = [textView.text substringToIndex:range.location];
    }
    self.model.live_intro = nullToEmpty(textView.text);
    [self sendActionName:@"intro_change" param:@{@"text":nullToEmpty(textView.text)} indexPath:self.indexPath];
}

- (void)textViewDidEndEditing:(YYTextView *)textView{
    self.model.live_intro = nullToEmpty(textView.text);
    [self sendActionName:@"intro_change" param:@{@"text":nullToEmpty(textView.text)} indexPath:self.indexPath];
}

#pragma mark - keyBoardNSNotification

- (void)keyBoardWillShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
//    [self keyBoardShowAnimation:value];
    [self sendActionName:@"keyBoardWillShow" param:@{@"value":value} indexPath:self.indexPath];
}

- (void)keyBoardDidShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self sendActionName:@"keyBoardWillShow" param:@{@"value":value,@"textView":self.textView} indexPath:self.indexPath];
//    [self keyBoardShowAnimation:value];
}

- (void)keyBoardWillHidden:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self sendActionName:@"keyBoardWillHidden" param:@{@"value":value} indexPath:self.indexPath];
//    [UIView animateWithDuration:0.1 animations:^{
//        if (!self.tableView) return ;
//        [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.bottom.mas_equalTo(-49);
//        }];
//        [self.tableView.superview layoutIfNeeded];
//    }];
}
//
//- (void)keyBoardShowAnimation:(NSValue *)value{
////    if ([self.textView isFirstResponder]) {
//    CGFloat detalY = [value CGRectValue].size.height;
//    if (!self.tableView) return ;
//    [UIView animateWithDuration:0.1 animations:^{
//        [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.bottom.mas_equalTo(-detalY);
//        }];
//        [self.tableView.superview layoutIfNeeded];
//    }];
////    }
//}


#pragma mark - regisetNSNotic

- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - configUI
- (void)setContentView{
    UIView *topView = [UIView by_init];
    [topView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.contentView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(10);
    }];
    
    self.textView = [[YYTextView alloc] init];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"请点击填写直播简介（选填）" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0xa3a3a3)}];
    self.textView.placeholderAttributedText = attributedString;
    self.textView.font = [UIFont systemFontOfSize:14];
    self.textView.textColor = kColorRGBValue(0x323232);
    self.textView.delegate = self;
    [self.contentView addSubview:self.textView];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(25, 15, 15, 15));
    }];
}

@end
