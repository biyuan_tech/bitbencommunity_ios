//
//  BYEditLiveTitleCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYEditLiveTitleCell.h"
#import "BYEditLiveModel.h"

@interface BYEditLiveTitleCell ()<UITextFieldDelegate>

/** 封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 直播名称 */
@property (nonatomic ,strong) UITextField *textField;
/** 开播时间 */
@property (nonatomic ,strong) UILabel *timeLab;
/** model */
@property (nonatomic ,strong) BYEditLiveModel *model;



@end

@implementation BYEditLiveTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYEditLiveModel *model = object[indexPath.row];
    self.indexPath = indexPath;
    self.model = model;
    if (model.coverImg) {
        self.coverImgView.image = model.coverImg;
        self.coverImgView.hidden = NO;
    }else if (model.live_cover_url.length){
        @weakify(self);
        [self.coverImgView uploadHDImageWithURL:model.live_cover_url callback:^(UIImage *image) {
            @strongify(self);
            if (!image) return ;
            self.model.coverImg = image;
            self.coverImgView.hidden = NO;
        }];
    }else{
        self.coverImgView.hidden = YES;
    }
    self.textField.text = nullToEmpty(model.live_title);
    if (model.begin_time.length) {
        self.timeLab.text = model.begin_time;
    }
}

#pragma mark - action
- (void)selTimeBtnAction{
    [self sendActionName:@"selTime" param:nil indexPath:self.indexPath];
}

- (void)addImageAction{
    [self sendActionName:@"selImgAction" param:nil indexPath:self.indexPath];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *rangString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (rangString.length > 30) { // 当直播主题输入30字符以上时禁止继续输入
        return NO;
    }
    _model.live_title = rangString;
    [self sendActionName:@"textField_change" param:@{@"text":nullToEmpty(rangString)} indexPath:self.indexPath];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    _model.live_title = textField.text;
    [self sendActionName:@"textField_change" param:@{@"text":nullToEmpty(textField.text)} indexPath:self.indexPath];
}

#pragma mark - configUI

- (void)setContentView{
    UIView *topView = [UIView by_init];
    [topView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.contentView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(10);
    }];
    
    UIImageView *editImgView = [UIImageView by_init];
    [editImgView by_setImageName:@"newlive_addImg"];
    editImgView.userInteractionEnabled = YES;
    [self.contentView addSubview:editImgView];
    [editImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(90);
        make.top.mas_equalTo(25);
        make.left.mas_equalTo(15);
    }];
    @weakify(self);
    [editImgView addTapGestureRecognizer:^{
        @strongify(self);
        [self addImageAction];
    }];
    
    UILabel *editImgLab = [UILabel by_init];
    [editImgLab setBy_font:14];
    editImgLab.textColor = kColorRGBValue(0xa3a3a3);
    editImgLab.text = @"上传封面";
    [editImgView addSubview:editImgLab];
    [editImgLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(stringGetWidth(editImgLab.text, 14));
        make.height.mas_equalTo(editImgLab.font.pointSize);
        make.bottom.mas_equalTo(-18);
    }];
    
    PDImageView *coverImgView = [[PDImageView alloc] init];
    coverImgView.layer.cornerRadius = 5.0;
    coverImgView.userInteractionEnabled = YES;
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.clipsToBounds = YES;
    coverImgView.hidden = YES;
    [self.contentView addSubview:coverImgView];
    self.coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(editImgView);
    }];
    
    [coverImgView addTapGestureRecognizer:^{
        @strongify(self);
        [self addImageAction];
    }];
    
    
    UILabel *coverTitleLab = [UILabel by_init];
    coverTitleLab.backgroundColor = kColorRGB(50, 50, 50, 0.8);
    [coverTitleLab setBy_font:11];
    coverTitleLab.textColor = [UIColor whiteColor];
    coverTitleLab.text = @"封面图";
    coverTitleLab.textAlignment = NSTextAlignmentCenter;
    [coverImgView addSubview:coverTitleLab];
    [coverTitleLab layerCornerRadius:4.0f byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight size:CGSizeMake(90, 20)];
    [coverTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(20);
    }];
    
    
    UIImageView *mustNameImgView = [UIImageView by_init];
    [mustNameImgView by_setImageName:@"newlive_must"];
    [self.contentView addSubview:mustNameImgView];
    [mustNameImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(125);
        make.top.mas_equalTo(40);
        make.width.height.mas_equalTo(6);
    }];
    
    UITextField *textField = [UITextField by_init];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"直播名称" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0x313131)}];
    textField.attributedPlaceholder = attributedString;
    textField.delegate = self;
    [self.contentView addSubview:textField];
    self.textField = textField;
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mustNameImgView.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(mustNameImgView);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(30);
    }];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(125);
        make.centerY.mas_equalTo(editImgView);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    
    UIImageView *mustNameImgView1 = [UIImageView by_init];
    [mustNameImgView1 by_setImageName:@"newlive_must"];
    [self.contentView addSubview:mustNameImgView1];
    [mustNameImgView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(125);
        make.top.mas_equalTo(94);
        make.width.height.mas_equalTo(6);
    }];
    
    UIButton *selTimeBtn = [UIButton by_buttonWithCustomType];
    [selTimeBtn addTarget:self action:@selector(selTimeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:selTimeBtn];
    [selTimeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(140);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(70);
        make.bottom.mas_equalTo(0);
    }];
    
    UILabel *timeLab = [UILabel by_init];
    [timeLab setBy_font:14];
    timeLab.textColor = kColorRGBValue(0x313131);
    timeLab.text = @"开播时间";
    [self.contentView addSubview:timeLab];
    self.timeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mustNameImgView1.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(selTimeBtn);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(timeLab.font.pointSize);
    }];
    
 
    
}

@end
