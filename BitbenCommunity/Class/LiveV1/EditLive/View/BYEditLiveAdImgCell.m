//
//  BYEditLiveAdImgCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYEditLiveAdImgCell.h"
#import "BYEditLiveModel.h"

@interface BYEditLiveAdImgCell ()

/** 封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** model */
@property (nonatomic ,strong) BYEditLiveModel *model;

@end

@implementation BYEditLiveAdImgCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYEditLiveModel *model = object[indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    if (model.adImg) {
        self.coverImgView.image = model.adImg;
        self.coverImgView.hidden = NO;
    }else if (model.ad_img.length){
        @weakify(self);
        [self.coverImgView uploadHDImageWithURL:model.ad_img callback:^(UIImage *image) {
            @strongify(self);
            if (!image) return ;
            self.model.adImg = image;
            self.coverImgView.hidden = NO;
        }];
    }else{
        self.coverImgView.hidden = YES;
    }
}

#pragma mark - action

- (void)addImageAction{
    [self sendActionName:@"selImgAction" param:nil indexPath:self.indexPath];
}

#pragma mark - configUI

- (void)setContentView{
    UIView *topView = [UIView by_init];
    [topView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.contentView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(10);
    }];
    
    UIImageView *editImgView = [UIImageView by_init];
    [editImgView by_setImageName:@"newlive_addImg"];
    editImgView.userInteractionEnabled = YES;
    [self.contentView addSubview:editImgView];
    [editImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(90);
        make.top.mas_equalTo(25);
        make.left.mas_equalTo(15);
    }];
    @weakify(self);
    [editImgView addTapGestureRecognizer:^{
        @strongify(self);
        [self addImageAction];
    }];
    
    UILabel *editImgLab = [UILabel by_init];
    [editImgLab setBy_font:14];
    editImgLab.textColor = kColorRGBValue(0xa3a3a3);
    editImgLab.text = @"上传海报";
    [editImgView addSubview:editImgLab];
    [editImgLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(stringGetWidth(editImgLab.text, 14));
        make.height.mas_equalTo(editImgLab.font.pointSize);
        make.bottom.mas_equalTo(-18);
    }];
    
    PDImageView *coverImgView = [[PDImageView alloc] init];
    coverImgView.layer.cornerRadius = 5.0;
    coverImgView.userInteractionEnabled = YES;
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.clipsToBounds = YES;
    coverImgView.hidden = YES;
    [self.contentView addSubview:coverImgView];
    self.coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(editImgView);
    }];
    [coverImgView addTapGestureRecognizer:^{
        @strongify(self);
        [self addImageAction];
    }];
    
    UILabel *coverTitleLab = [UILabel by_init];
    coverTitleLab.backgroundColor = kColorRGB(50, 50, 50, 0.8);
    [coverTitleLab setBy_font:11];
    coverTitleLab.textColor = [UIColor whiteColor];
    coverTitleLab.text = @"海报";
    coverTitleLab.textAlignment = NSTextAlignmentCenter;
    [coverImgView addSubview:coverTitleLab];
    [coverTitleLab layerCornerRadius:4.0f byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight size:CGSizeMake(90, 20)];
    [coverTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(20);
    }];
}

@end
