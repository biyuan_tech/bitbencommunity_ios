//
//  BYEditLiveAddCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYEditLiveAddCell.h"

@implementation BYEditLiveAddCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    
}

- (void)setContentView{
    UIView *topView = [UIView by_init];
    [topView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.contentView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(10);
    }];
    
    UIImageView *addImgView = [[UIImageView alloc] init];
    [addImgView by_setImageName:@"newlive_add"];
    [self.contentView addSubview:addImgView];
    [addImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(25);
        make.width.height.mas_equalTo(19);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    titleLab.textColor = kColorRGBValue(0x313131);
    titleLab.text = @"添加嘉宾";
    [self.contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(addImgView.mas_right).mas_equalTo(10);
        make.centerY.mas_equalTo(addImgView);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.width.mas_equalTo(stringGetWidth(titleLab.text, 14));
    }];
}

@end
