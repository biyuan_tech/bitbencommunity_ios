//
//  BYEditLiveHostAppCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYEditLiveHostAppCell.h"
#import "BYEditLiveModel.h"

static NSInteger baseTag = 0x754;

@interface BYEditLiveHostAppCell ()<UITextFieldDelegate>

/** 提示 */
@property (nonatomic ,strong) UILabel *promptLab;
/** model */
@property (nonatomic ,strong) BYEditGuestInfoModel *model;


@end

@implementation BYEditLiveHostAppCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    [super setContentWithObject:object indexPath:indexPath];
    BYEditGuestInfoModel *model = object[indexPath.row];
    self.model = model;
    self.promptLab.hidden = model.isExist == 0 ? NO : YES;
    for (int i = 0; i < 2; i ++) {
        UITextField *textField = [self.contentView viewWithTag:baseTag + i];
        textField.text = i == 0 ? nullToEmpty(model.user_id) : nullToEmpty(model.intro);
    }
}

- (void)delBtnAction:(UIButton *)sender{
    [self sendActionName:@"delCellAction" param:nil indexPath:self.indexPath];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *rangString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField.tag == baseTag) {
        if (rangString.length > 9) {
            return NO;
        }
    }else{
//        if (rangString.length > 10) { // 当直播主题输入10字符以上时禁止继续输入
//            return NO;
//        }
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag == baseTag) {
        [self verifyAccountId:textField.text];
        self.model.user_id = textField.text;
    }else{
        self.model.intro = textField.text;
    }
}

- (void)textFieldDidChangeNSNOtific:(id)sender{
    self.model.isExist = -1;
}


#pragma mark - request

- (void)verifyAccountId:(NSString *)accountId{
    @weakify(self);
    [[BYLiveHomeRequest alloc] verifyAccountId:accountId successBlock:^(id object) {
        @strongify(self);
        self.model.isExist = [object[@"is_exist"] integerValue];
        if (![object[@"is_exist"] boolValue]) {
            self.promptLab.hidden = NO;
            showToastView(object[@"is_exist_msg"], kCommonWindow);
        }else{
            self.promptLab.hidden = YES;
        }
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)setContentView{
    UIView *topView = [UIView by_init];
    [topView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.contentView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(10);
    }];
    
    
    NSArray *placeholders = @[@"嘉宾币本ID",@"嘉宾简介"];
    for (int i = 0; i < 2; i ++) {
        UIView *textView = [UIView by_init];
        [self.contentView addSubview:textView];
        [textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(10 + 53*i);
            make.right.mas_equalTo(-50);
            make.height.mas_equalTo(53);
        }];
        [self textViewAddSubView:textView placeholder:placeholders[i] index:i];
    }
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(63);
        make.right.mas_equalTo(-50);
        make.height.mas_equalTo(0.5);
    }];
    
    UIButton *delBtn = [UIButton by_buttonWithCustomType];
    [delBtn setBy_imageName:@"newlive_del" forState:UIControlStateNormal];
    [delBtn addTarget:self action:@selector(delBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:delBtn];
    [delBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(50);
        make.top.mas_equalTo(10);
        make.bottom.mas_equalTo(0);
    }];
    
    self.promptLab = [UILabel by_init];
    [self.promptLab setBy_font:14];
    self.promptLab.textColor = kColorRGBValue(0xfe0000);
    self.promptLab.text = @"ID不存在";
    self.promptLab.hidden = YES;
    [self.contentView addSubview:self.promptLab];
    [self.promptLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-50);
        make.top.mas_equalTo(30);
        make.width.mas_equalTo(stringGetWidth(self.promptLab.text, 14));
        make.height.mas_equalTo(self.promptLab.font.pointSize);
    }];
}

- (void)textViewAddSubView:(UIView *)textView placeholder:(NSString *)placeholder index:(NSInteger)index{
    UIImageView *mustNameImgView = [UIImageView by_init];
    [mustNameImgView by_setImageName:@"newlive_must"];
    [textView addSubview:mustNameImgView];
    [mustNameImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(0);
        make.width.height.mas_equalTo(6);
    }];
    
    UITextField *textField = [UITextField by_init];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0x313131)}];
    textField.attributedPlaceholder = attributedString;
    textField.delegate = self;
    textField.tag = baseTag + index;
    if (index == 0) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
        [textField addTarget:self action:@selector(textFieldDidChangeNSNOtific:) forControlEvents:UIControlEventEditingChanged];
    }
    [textView addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mustNameImgView.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(mustNameImgView);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(textView);
    }];
}

@end
