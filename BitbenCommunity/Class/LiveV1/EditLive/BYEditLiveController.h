//
//  BYEditLiveController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYEditLiveController : BYCommonViewController

/** liveType */
@property (nonatomic ,assign) BY_NEWLIVE_TYPE liveType;
/** 微直播类型 */
@property (nonatomic ,assign) BY_NEWLIVE_MICRO_TYPE scene_type;
/** 当前是编辑流程(分新建直播流程、编辑直播信息流程) */
@property (nonatomic ,assign) BOOL isEdit;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 更新回调 */
@property (nonatomic ,copy) void (^didUpdateInfoHandle)(BYCommonLiveModel *model);


@end

NS_ASSUME_NONNULL_END
