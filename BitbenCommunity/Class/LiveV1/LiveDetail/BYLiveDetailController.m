//
//  BYLiveDetailController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveDetailController.h"
#import "BYLiveDetailViewModel.h"
#import "BYNewLiveControllerV1.h"
#import "BYEditLiveController.h"
#import "BYEditLiveWxController.h"

@interface BYLiveDetailController ()

@end

@implementation BYLiveDetailController

- (void)dealloc
{
    
}

- (Class)getViewModelClass{
    return [BYLiveDetailViewModel class];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   
//    [self.navigationController setViewControllers:array animated:NO];
//    for (UIViewController *viewController in array) {
//        if ([viewController isKindOfClass:[BYNewLiveControllerV1 class]]) {
//            [self.rt_navigationController removeViewController:viewController];
////            [array removeObject:viewController];
//        }else if ([viewController isKindOfClass:[BYEditLiveController class]]) {
//            [self.rt_navigationController removeViewController:viewController];
////            [array removeObject:viewController];
//        }
//    }
//    [self.navigationController setViewControllers:array animated:NO];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:kColorRGBValue(0xe7e7ea)]];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
//    self.navigationTitle = @"微直播";
//    NSMutableArray *array = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
//    [self.navigationController.viewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        if ([obj isKindOfClass:[BYNewLiveControllerV1 class]] ||
//            [obj isKindOfClass:[BYEditLiveController class]]) {
//            [array removeObject:obj];
//        }
//    }];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        self.navigationController.viewControllers = [array copy];
//    });
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [array enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[BYEditLiveWxController class]]) {
            [self.rt_navigationController removeViewController:obj];
        }
    }];
  
}

@end
