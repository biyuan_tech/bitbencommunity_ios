//
//  BYLiveDetailViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveDetailViewModel.h"
#import "BYLiveDetailController.h"
#import "BYEditLiveController.h"
#import "BYMicroLiveController.h"
#import "BYPPTRoomController.h"
#import "ShareRootViewController.h"
#import "BYPushFlowSetController.h"
#import "BYVideoLiveController.h"
#import "BYPushFlowController.h"
#import "MsgPicViewController.h"
#import "BYILiveController.h"

#import "BYReportSheetView.h"

#import "BYLiveDetailModel.h"

#define K_VC ((BYLiveDetailController *)S_VC)

@interface BYLiveDetailViewModel ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** 举报 */
@property (nonatomic ,strong) BYReportSheetView *reportSheetView;
/** 直播推流码 */
@property (nonatomic ,copy) NSString *codeUrl;

@end

@implementation BYLiveDetailViewModel

- (void)dealloc
{
    [_reportSheetView removeFromSuperview];
    [_tableView removeFromSuperview];
    _reportSheetView = nil;
    _tableView = nil;
}

- (void)viewDidAppear{
    if (K_VC.live_record_id.length) {
        [self loadLiveDetailRequest];
    }
    [[BYSIMManager shareManager] resetJoinTime];
}
- (void)setContentView{
    [self addTableView];
    [self addBottomView];
    [self setReportView];
}

- (BY_IDENTITY_TYPE )verifyIdentithy{
    __block BOOL hasResult = NO;
    if (K_VC.model.host_list.count) {
        [K_VC.model.host_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.user_id isEqualToString:ACCOUNT_ID]) {
                *stop = YES;
                hasResult = YES;
            }
        }];
        if (hasResult) {
            return BY_IDENTITY_TYPE_HOST;
        }
    }
    if (K_VC.model.interviewee_list.count) {
        [K_VC.model.interviewee_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.user_id isEqualToString:ACCOUNT_ID]) {
                *stop = YES;
                hasResult = YES;
            }
        }];
        if (hasResult) {
            return BY_IDENTITY_TYPE_GUEST;
        }
    }
    if ([K_VC.model.user_id isEqualToString:ACCOUNT_ID]) {
        return BY_IDENTITY_TYPE_CREATOR;
    }
    return BY_IDENTITY_TYPE_NORMAL;
}

#pragma mark - action
// 分享
- (void)shareAction{
    if (!K_VC.model.shareMap) return;
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.collection_status = K_VC.model.collection_status;
    shareViewController.transferCopyUrl = K_VC.model.shareMap.share_url;
    
    shareViewController.hasJubao = [K_VC.model.user_id isEqualToString:[AccountModel sharedAccountModel].account_id] ? NO : YES;
    @weakify(self);
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        @strongify(self);
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        NSString *mainImgUrl = [PDImageView appendingImgUrl:K_VC.model.shareMap.share_img];
        NSString *imgurl = [mainImgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [ShareSDKManager shareManagerWithType:shareType title:K_VC.model.shareMap.share_title desc:K_VC.model.shareMap.share_intro img:imgurl url:K_VC.model.shareMap.share_url callBack:NULL];
        [ShareSDKManager shareSuccessBack:shareTypeLive block:NULL];
    }];
    [shareViewController actionClickWithJubaoBlock:^{
        @strongify(self);
        if (!K_VC.model) return ;
        self.reportSheetView.theme_id = K_VC.model.live_record_id;
        self.reportSheetView.theme_type = BY_THEME_TYPE_LIVE;
        self.reportSheetView.user_id = K_VC.model.user_id;
        [self.reportSheetView showAnimation];
    }];
    
    [shareViewController actionClickWithCollectionBlock:^{
       @strongify(self);
       if (!K_VC.model) return ;
        K_VC.model.collection_status = !K_VC.model.collection_status;
    }];
    [shareViewController showInView:S_VC];
}

- (void)enterLiveAction{
    if (!K_VC.model) return;
    
    if (K_VC.model.status == BY_LIVE_STATUS_REVIEW_WAITING ||
        K_VC.model.status == BY_LIVE_STATUS_REVIEW_FAIL ||
        K_VC.model.status == BY_LIVE_STATUS_OVERTIME) {
        NSString *title;
        if (K_VC.model.status == BY_LIVE_STATUS_REVIEW_WAITING) {
            title = @"审核中无法进入直播间";
        }else if (K_VC.model.status == BY_LIVE_STATUS_REVIEW_FAIL){
            title = @"审核失败无法进入直播间";
        }else {
            title = @"直播超时无法进入直播间";
        }
        showToastView(title, S_V_VIEW);
        return;
    }
    switch (K_VC.model.live_type) {
        case BY_NEWLIVE_TYPE_AUDIO_LECTURE :
        case BY_NEWLIVE_TYPE_AUDIO_PPT:
        {
            BYMicroLiveController *liveController = [[BYMicroLiveController alloc] init];
            liveController.live_record_id = K_VC.model.live_record_id;
            liveController.isHost = [K_VC.model.user_id isEqualToString:[AccountModel sharedAccountModel].account_id] ? YES : NO;
            liveController.identityType = [self verifyIdentithy];
            [S_V_NC pushViewController:liveController animated:YES];
        }
            break;
        case BY_NEWLIVE_TYPE_VIDEO:
        {
            BYPushFlowController *pushFlowController = [[BYPushFlowController alloc] init];
            pushFlowController.live_record_id = K_VC.model.live_record_id;
            pushFlowController.isHost = [ACCOUNT_ID isEqualToString:K_VC.model.user_id] ? YES : NO;
            [S_V_NC pushViewController:pushFlowController animated:YES];
            break;
        }
        default:
        {
            BYILiveController *iliveController = [[BYILiveController alloc] init];
            iliveController.live_record_id = K_VC.model.live_record_id;
            iliveController.isHost = [ACCOUNT_ID isEqualToString:K_VC.model.user_id] ? YES : NO;
            [S_V_NC pushViewController:iliveController animated:YES];
        }
            break;
    }
}

- (void)infoBtnAction{
    NSString *title = K_VC.model.status == BY_LIVE_STATUS_REVIEW_WAITING ? @"审核提示" : @"审核未通过提示";
    NSString *message;;
    if (K_VC.model.status == BY_LIVE_STATUS_REVIEW_WAITING) {
        message = [NSString stringWithFormat:@"%@“%@”",review_waiting,K_VC.model.assistant_wechat];
    }else{
        message = [NSString stringWithFormat:@"审核失败原因：%@。有疑问请添加微信“%@”",K_VC.model.remark,K_VC.model.assistant_wechat];
    }
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleCancel handler:nil];
    
    [sheetController addAction:cancelAction];
    [CURRENT_VC.navigationController presentViewController:sheetController animated:YES completion:nil];
}

#pragma mark - BYCommonTableViewDelegate
- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForFooterInSection:(NSInteger)section{
    UIView *sectonView = [UIView by_init];
    [sectonView setBackgroundColor:[UIColor clearColor]];
    return sectonView;
}

- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForFooterInSection:(NSInteger)section{
    return 10;
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"delAction"]) { // 删除
        UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:@"确认删除此条直播吗？" message:@"删除后将无法恢复" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self loadRequestDelLive];
        }];
        
        [sheetController addAction:confirmAction];
        [sheetController addAction:cancelAction];
        [S_V_NC presentViewController:sheetController animated:YES completion:nil];
    }else if ([actionName isEqualToString:@"editAction"]) { // 编辑
        BYEditLiveController *editLiveController = [[BYEditLiveController alloc] init];
        editLiveController.isEdit = YES;
        editLiveController.model = K_VC.model;
        editLiveController.navigationTitle = @"直播设置";
        @weakify(self);
        editLiveController.didUpdateInfoHandle = ^(BYCommonLiveModel * _Nonnull model) {
            @strongify(self);
            self.tableView.tableData = [BYLiveDetailModel getTableData:model];
        };
        [S_V_NC pushViewController:editLiveController animated:YES];
    }else if ([actionName isEqualToString:@"appointAction"]) { // 预约
        NSDictionary *dic = self.tableView.tableData[indexPath.section];
        BYLiveDetailModel *model = dic.allValues[0][indexPath.row];
        if (model.attention) {
            [self loadRequestCancelAttentionLive:model tableView:tableView indexPath:indexPath];
        }else{
            [self loadRequestAttentionLive:model tableView:tableView indexPath:indexPath];
        }
    }else if ([actionName isEqualToString:@"tutorialAction"]) { // 推流教程
        BYPushFlowSetController *pushFlowSetController = [[BYPushFlowSetController alloc] init];
        pushFlowSetController.live_record_id = K_VC.live_record_id;
        [S_V_NC pushViewController:pushFlowSetController animated:YES];
    }else if ([actionName isEqualToString:@"copyAction"]) { // 复制直播码
        if (!self.codeUrl.length) {
            showToastView(@"暂未获取到推流码！请重新获取！", S_V_VIEW);
            return;
        }
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        [pasteboard setString:self.codeUrl];
        showToastView(@"已复制推流码", S_V_VIEW);
    }else if ([actionName isEqualToString:@"imageViewAction"]) { // 封面图点击放大
        UIImageView *imageView = param[@"imageView"];
        if (!imageView.image) return;
        CGRect newe =  [imageView.superview convertRect:imageView.frame toView:kCommonWindow];
        [MsgPicViewController addToRootViewController:imageView.image ofMsgId:@"" in:newe from:@[]];
    }else if ([actionName isEqualToString:@"attentionAction"]) {
        if (![[AccountModel sharedAccountModel] hasLoggedIn]){
            [S_VC authorizeWithCompletionHandler:nil];
        }
        else {
            [self attentionBtnAction:indexPath];
        }
    }else if ([actionName isEqualToString:@"infoBtnAction"]) {
        [self infoBtnAction];
    }
}

#pragma mark - request
- (void)loadLiveDetailRequest{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLiveDetail:K_VC.live_record_id successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        if (!object) return ;
        K_VC.model = object;
        self.tableView.tableData = [BYLiveDetailModel getTableData:K_VC.model];
        if (K_VC.model.status > 0) {
            [self setRightNavigationBtn];
        }
        if (K_VC.model.live_type == BY_NEWLIVE_TYPE_VIDEO &&
            [AccountModel sharedAccountModel].hasLoggedIn) {
            [self getPushFlowURLRequest];
        }
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"获取直播详情失败", S_V_VIEW);
    }];
}

- (void)getPushFlowURLRequest{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetPushURL:K_VC.live_record_id successBlock:^(id object) {
        @strongify(self);
        NSString *pushUrl = nullToEmpty(object[@"pushUrl"]);
        NSString *streamCode = nullToEmpty(object[@"streamCode"]);
        NSString *string = [NSString stringWithFormat:@"%@%@",pushUrl,streamCode];
        self.codeUrl = string;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"推流地址获取失败", S_V_VIEW);
    }];
}


- (void)loadRequestDelLive{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestDelTheme_id:K_VC.model.live_record_id theme_type:BY_THEME_TYPE_LIVE successBlock:^(id object) {
        @strongify(self);
        [S_V_NC popViewControllerAnimated:YES];
        showToastView(@"删除成功", kCommonWindow);
    } faileBlock:^(NSError *error) {
        showToastView(@"删除失败", kCommonWindow);
    }];
}

- (void)loadRequestCancelAttentionLive:(BYLiveDetailModel *)model tableView:(BYCommonTableView *)tableView indexPath:(NSIndexPath *)indexPath{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCancelAttentionLive:K_VC.model.live_record_id successBlock:^(id object) {
        @strongify(self);
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYLiveDetailModel *model = dic.allValues[0][indexPath.row];
        model.attention = [object boolValue];
        K_VC.model.attention = [object boolValue];
        [tableView reloadData];
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)loadRequestAttentionLive:(BYLiveDetailModel *)model tableView:(BYCommonTableView *)tableView indexPath:(NSIndexPath *)indexPath{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestAttentionLive:K_VC.model.live_record_id successBlock:^(id object) {
        @strongify(self);
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYLiveDetailModel *model = dic.allValues[0][indexPath.row];
        model.attention = [object boolValue];
        K_VC.model.attention = [object boolValue];
        [tableView reloadData];
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)attentionBtnAction:(NSIndexPath *)indexPath{
    NSDictionary *dic = self.tableView.tableData[indexPath.section];
    BYLiveDetailUserModel *model = dic.allValues[0][indexPath.row];
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:model.user_id
                                             isAttention:!model.isAttention
                                            successBlock:^(id object) {
                                                @strongify(self);
                                                model.isAttention = !model.isAttention;
                                                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
                                            } faileBlock:nil];
}

#pragma mark - configUI

- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    [self.tableView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    self.tableView.group_delegate = self;
    [S_V_VIEW addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kSafe_Mas_Bottom(65), 0));
    }];
    if (K_VC.model) {
        self.tableView.tableData = [BYLiveDetailModel getTableData:K_VC.model];
    }
}

- (void)addBottomView{
    UIView *bottomView = [UIView by_init];
    [bottomView setBackgroundColor:[UIColor whiteColor]];
    [S_V_VIEW addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(65));
    }];
    
    UIButton *enterBtn = [UIButton by_buttonWithCustomType];
    [enterBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    [enterBtn setBy_attributedTitle:@{@"title":@"进入直播",
                                          NSFontAttributeName:[UIFont systemFontOfSize:16],
                                          NSForegroundColorAttributeName:[UIColor whiteColor]
                                          } forState:UIControlStateNormal];
    [enterBtn addTarget:self action:@selector(enterLiveAction)
       forControlEvents:UIControlEventTouchUpInside];
    enterBtn.layer.cornerRadius = 22.0f;
    [bottomView addSubview:enterBtn];
    [enterBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(10);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(44);
    }];
}

- (void)setRightNavigationBtn{
    @weakify(self);
    [S_VC rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_article_more"] barHltImage:[UIImage imageNamed:@"icon_article_more"] action:^{
        @strongify(self);
        [self shareAction];
    }];
}

- (void)setReportView{
    self.reportSheetView = [BYReportSheetView initReportSheetViewShowInView:kCommonWindow];
}

@end
