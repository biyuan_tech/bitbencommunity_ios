//
//  BYLiveDetailController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYLiveDetailController : BYCommonViewController

/** 是否为主播端 */
@property (nonatomic ,assign) BOOL isHost;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** id */
@property (nonatomic ,copy) NSString *live_record_id;


@end

NS_ASSUME_NONNULL_END
