//
//  BYLiveDetailSingleTitleCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveDetailSingleTitleCell.h"

@interface BYLiveDetailSingleTitleCell ()

/** titleLab */
@property (nonatomic ,strong) UILabel *titleLab;

@end

@implementation BYLiveDetailSingleTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYCommonModel *model = dic.allValues[0][indexPath.row];
    self.titleLab.text = model.title;
}

- (void)setContentView{
    UILabel *titleLab = [UILabel by_init];
    titleLab.font = [UIFont boldSystemFontOfSize:16];
    titleLab.textColor = kColorRGBValue(0x313131);
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(80);
    }];
}
@end
