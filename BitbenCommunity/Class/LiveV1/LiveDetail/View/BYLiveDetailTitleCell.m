//
//  BYLiveDetailTitleCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveDetailTitleCell.h"
#import "BYLiveDetailModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>
#import <SDWebImage/FLAnimatedImageView+WebCache.h>


static NSInteger kBaseTag = 584;
@interface BYLiveDetailTitleCell ()

/** 直播封面 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 直播标题 */
@property (nonatomic ,strong) YYLabel *titleLab;
/** 直播开始时间 */
@property (nonatomic ,strong) UILabel *timeLab;
/** model */
@property (nonatomic ,strong) BYLiveDetailModel *model;
/** 直播状态 */
@property (nonatomic ,strong) UILabel *liveStatusLab;
/** 直播状态背景 */
@property (nonatomic ,strong) UIView *liveStatusBg;
/** 直播中动画 */
@property (nonatomic ,strong) FLAnimatedImageView *livIngImgView;
/** 预约按钮 */
@property (nonatomic ,strong) UIButton *appointBtn;
/** 推流码View */
@property (nonatomic ,strong) UIView *pushPlowView;
/** 审核视图 */
@property (nonatomic ,strong) UIView *reviewView;
/** 审核title */
@property (nonatomic ,strong) UILabel *reviewLab;
/** infoBtn */
@property (nonatomic ,strong) UIButton *infoBtn;

@end

@implementation BYLiveDetailTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = object[indexPath.section];
    BYLiveDetailModel *model = dic.allValues[0][indexPath.row];
    
    self.model = model;
    self.indexPath = indexPath;
    
    self.reviewView.hidden = [model.user_id isEqualToString:ACCOUNT_ID] ? NO : YES;
    if (![model.user_id isEqualToString:[AccountModel sharedAccountModel].account_id]) {
        for (int i = 0; i < 2; i ++) {
            UIButton *btn = [self viewWithTag:2*kBaseTag + i];
            btn.hidden = YES;
        }
        self.appointBtn.hidden = model.status == BY_LIVE_STATUS_SOON ? NO : YES;
    }else{
        self.pushPlowView.hidden = model.live_type == BY_NEWLIVE_TYPE_VIDEO ? NO : YES;
        self.appointBtn.hidden = YES;
        UIButton *btn = [self viewWithTag:2*kBaseTag + 1];
        if (model.status == BY_LIVE_STATUS_REVIEW_FAIL ||
            model.status == BY_LIVE_STATUS_REVIEW_WAITING) {
            self.infoBtn.hidden = NO;
            btn.hidden = NO;
        }else{
            self.infoBtn.hidden = YES;
            btn.hidden = YES;
        }
    }
    
    if (model.coverImg) {
        _coverImgView.image = model.coverImg;
    }else{
        [_coverImgView uploadHDImageWithURL:model.live_cover_url callback:nil];
    }
    
    NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.live_title)];
    messageAttributed.yy_lineSpacing = 8.0f;
    messageAttributed.yy_font = [UIFont boldSystemFontOfSize:15];
    messageAttributed.yy_color = kColorRGBValue(0x323232);
    YYTextContainer *textContainer = [YYTextContainer new];
    textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
    YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
    [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(ceil(layout.textBoundingSize.height));
    }];
    self.titleLab.attributedText = messageAttributed;
    
    NSDate *date = [NSDate by_dateFromString:model.begin_time dateformatter:K_D_F];
    NSString *time = [NSDate by_stringFromDate:date dateformatter:@"yyyy-MM-dd HH:mm"];
    self.timeLab.text = time;
    [self.timeLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(time, self.timeLab.font.pointSize));
    }];
    
    [self reloadAppointStatus:model.attention];
    
    [self reloadLiveStatus:model.status liveType:model.live_type];

    [self addTagsView];
    
}

- (void)reloadLiveStatus:(BY_LIVE_STATUS)status liveType:(BY_NEWLIVE_TYPE)liveType{
    _liveStatusBg.hidden = YES;
    _liveStatusLab.hidden = YES;
    _livIngImgView.hidden = YES;
    switch (status) {
        case BY_LIVE_STATUS_LIVING:
            _liveStatusBg.hidden = NO;
            _liveStatusLab.hidden = NO;
            _livIngImgView.hidden = NO;
            _liveStatusBg.backgroundColor = kColorRGBValue(0xea6441);
            _liveStatusLab.text = @"直播中";
            _reviewLab.text = @"审核已通过";
            _reviewLab.textColor = kColorRGBValue(0x6384ff);
            break;
        case BY_LIVE_STATUS_SOON:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xffbe21);
            _liveStatusLab.text = @"直播预告";
            _reviewLab.text = @"审核已通过";
            _reviewLab.textColor = kColorRGBValue(0x6384ff);
            break;
        case BY_LIVE_STATUS_END:
        case BY_LIVE_STATUS_OVERTIME:
            _liveStatusBg.backgroundColor = kColorRGBValue(0x4165ea);
            _liveStatusLab.text = @"直播回放";
            _reviewLab.text = @"审核已通过";
            _reviewLab.textColor = kColorRGBValue(0x6384ff);
            break;
        case BY_LIVE_STATUS_NOTLIVE_ONTIME:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xf28d73);
            _liveStatusLab.text = @"暂未开播";
            _reviewLab.text = @"审核已通过";
            _reviewLab.textColor = kColorRGBValue(0x6384ff);
            break;
        case BY_LIVE_STATUS_REVIEW_WAITING:
            _reviewLab.text = @"审核中";
            _reviewLab.textColor = kColorRGBValue(0xea6438);
            break;
        case BY_LIVE_STATUS_REVIEW_FAIL:
            _reviewLab.text = @"审核失败";
            _reviewLab.textColor = kColorRGBValue(0xea6438);
            break;
        default:
            break;
    }
    
    CGFloat labOriginX = status == BY_LIVE_STATUS_LIVING ? 35 : 25;
    @weakify(self);
    [_liveStatusLab mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(labOriginX);
        make.width.mas_equalTo(stringGetWidth(self.liveStatusLab.text, 11) + 10);
        make.height.mas_equalTo(stringGetHeight(self.liveStatusLab.text, 11) + 4);
    }];
    
    CGFloat bgWidth = status == BY_LIVE_STATUS_LIVING ? stringGetWidth(self.liveStatusLab.text, 11) + 18 : stringGetWidth(self.liveStatusLab.text, 11) + 10;
    [_liveStatusBg mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(bgWidth);
    }];
}

- (void)reloadAppointStatus:(BOOL)isAppoint{
    self.appointBtn.selected = isAppoint;
    self.appointBtn.backgroundColor = isAppoint ? kColorRGBValue(0xe69277) : kColorRGBValue(0xea6438);
}

#pragma mark - action
- (void)buttonAction:(UIButton *)sender{
    NSInteger i = sender.tag - 2*kBaseTag;
    if (i == 0) { // 删除
        [self sendActionName:@"delAction" param:nil indexPath:self.indexPath];
    }else{ // 编辑
        [self sendActionName:@"editAction" param:nil indexPath:self.indexPath];
    }
}

- (void)enterLiveAction{
    [self sendActionName:@"enterLiveAction" param:nil indexPath:self.indexPath];
}

- (void)appointBtnAction:(UIButton *)sender{
    [self sendActionName:@"appointAction" param:@{@"btn":sender} indexPath:self.indexPath];
}

- (void)tutorialBtnAction{
    [self sendActionName:@"tutorialAction" param:nil indexPath:self.indexPath];
}

// 复制直播码
- (void)copyBtnAction{
    [self sendActionName:@"copyAction" param:nil indexPath:self.indexPath];
}

- (void)infoBtnAction{
    [self sendActionName:@"infoBtnAction" param:nil indexPath:self.indexPath];
}

#pragma mark - configUI

- (void)addTagsView{
    NSArray *topicArr = self.model.topic_content;
    for (UIView *view in self.contentView.subviews) {
        if (view.tag >= kBaseTag && view.tag < kBaseTag + 20) {
            [view removeFromSuperview];
        }
    }
    
    for (int i = 0; i < topicArr.count; i ++) {
        UIButton *tagView = [UIButton by_buttonWithCustomType];
        NSString *title = topicArr[i];
        [tagView setTitle:title forState:UIControlStateNormal];
        tagView.titleLabel.font = [UIFont systemFontOfSize:11];
        [tagView setTitleColor:kColorRGBValue(0x4f86cb) forState:UIControlStateNormal];
        [tagView setBackgroundColor:kColorRGBValue(0xf7fbff)];
        CGFloat width = stringGetWidth(title, 11) + 12;
        CGFloat height = stringGetHeight(title, 11) + 10;
        tagView.layer.cornerRadius = 2;
        tagView.layer.borderWidth = 0.5;
        tagView.layer.borderColor = kColorRGBValue(0x92b8e8).CGColor;
        UIButton *lastTagView = [self viewWithTag:kBaseTag + i - 1];
        [self.contentView addSubview:tagView];
        tagView.tag = kBaseTag + i;
        [tagView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            if (lastTagView) {
                make.left.mas_equalTo(lastTagView.mas_right).mas_offset(5);
            }else{
                make.left.mas_equalTo(15);
            }
            make.top.mas_equalTo(self.titleLab.mas_bottom).mas_offset(10);
        }];
    }
}

- (void)setContentView{
    
    // 直播封面图
    self.coverImgView = [[PDImageView alloc] init];
    [self.contentView addSubview:self.coverImgView];
    CGFloat coverH = (kCommonScreenWidth - 30)*180/345;
    [self.coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(coverH);
    }];
    
    // 直播标题
    self.titleLab = [YYLabel by_init];
    self.titleLab.font = [UIFont systemFontOfSize:15];
    self.titleLab.textColor = kColorRGBValue(0x323232);
    self.titleLab.numberOfLines = 2.0f;
    [self.contentView addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.coverImgView.mas_bottom).mas_offset(15);
        make.height.mas_greaterThanOrEqualTo(0);
        make.right.mas_equalTo(-15);
    }];
    
    self.timeLab = [UILabel by_init];
    [self.timeLab setBy_font:13];
    self.timeLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:self.timeLab];
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.titleLab.mas_bottom).mas_offset(49);
        make.height.mas_equalTo(self.timeLab.font.pointSize);
        make.width.mas_equalTo(150);
    }];
    
    self.reviewView = [UIView by_init];
    [self.contentView addSubview:self.reviewView];
    [self.reviewView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.timeLab.mas_right).offset(20);
        make.centerY.mas_equalTo(self.timeLab);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(72);
    }];
    
    self.reviewLab = [UILabel by_init];
    [self.reviewLab setBy_font:13];
    self.reviewLab.textColor = kColorRGBValue(0xea6438);
    [self.reviewView addSubview:self.reviewLab];
    [self.reviewLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.centerY.mas_equalTo(0);
        make.height.mas_equalTo(self.reviewLab.font.pointSize);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    self.infoBtn = [UIButton by_buttonWithCustomType];
    [self.infoBtn setBy_imageName:@"newlive_info" forState:UIControlStateNormal];
    [self.infoBtn addTarget:self action:@selector(infoBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.reviewView addSubview:self.infoBtn];
    [self.infoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.reviewLab.mas_right).offset(5);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(20);
    }];
    
    NSArray *images = @[@"newlive_delete",@"newlive_edit"];
    for (int i = 0; i < 2; i ++) {
        UIButton *button = [UIButton by_buttonWithCustomType];
        [button setBy_imageName:images[i] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = 2*kBaseTag + i;
        [self.contentView addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.timeLab);
            make.right.mas_equalTo(-40*i);
            make.width.height.mas_equalTo(40);
        }];
    }
    
//    UIButton *enterLiveBtn = [UIButton by_buttonWithCustomType];
//    [enterLiveBtn setBackgroundColor:kColorRGBValue(0xea6438)];
//    [enterLiveBtn setBy_attributedTitle:@{@"title":@"进入直播",
//                                          NSFontAttributeName:[UIFont systemFontOfSize:16],
//                                          NSForegroundColorAttributeName:[UIColor whiteColor]
//                                          } forState:UIControlStateNormal];
//    [enterLiveBtn addTarget:self action:@selector(enterLiveAction) forControlEvents:UIControlEventTouchUpInside];
//    enterLiveBtn.layer.cornerRadius = 22.0f;
//    [self.contentView addSubview:enterLiveBtn];
//    [enterLiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(15);
//        make.right.mas_equalTo(-15);
//        make.height.mas_equalTo(44);
////        make.top.mas_equalTo(self.timeLab.mas_bottom).mas_offset(17);
//        make.bottom.mas_equalTo(-15);
//    }];
    
    // 关注按钮
    UIButton *appointBtn = [UIButton by_buttonWithCustomType];
    [appointBtn setBy_attributedTitle:@{@"title":@"预约",
                                        NSForegroundColorAttributeName:[UIColor whiteColor],
                                        NSFontAttributeName:[UIFont systemFontOfSize:11],
                                        } forState:UIControlStateNormal];
    [appointBtn setBy_attributedTitle:@{@"title":@"已预约",
                                        NSForegroundColorAttributeName:[UIColor whiteColor],
                                        NSFontAttributeName:[UIFont systemFontOfSize:11],
                                        } forState:UIControlStateSelected];
    [appointBtn addTarget:self
                   action:@selector(appointBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];
    appointBtn.layer.cornerRadius = 2.0f;
    appointBtn.hidden = YES;
    [self.contentView addSubview:appointBtn];
    self.appointBtn = appointBtn;
    [appointBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(41);
        make.height.mas_equalTo(17);
        make.right.mas_equalTo(self.coverImgView).mas_offset(-8);
        make.bottom.mas_equalTo(self.coverImgView).mas_offset(-8);
    }];
    [self reloadAppointStatus:NO];
    
    [self addLiveStatusView];
    
    [self addPushFlowView];
}

- (void)addLiveStatusView{
    
    UILabel *liveStatusLab = [UILabel by_init];
    liveStatusLab.text = @"直播中";
    [liveStatusLab setBy_font:11];
    [liveStatusLab setTextColor:[UIColor whiteColor]];
    liveStatusLab.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:liveStatusLab];
    _liveStatusLab = liveStatusLab;
    [liveStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
        make.top.mas_equalTo(25);
        make.width.mas_equalTo(stringGetWidth(liveStatusLab.text, liveStatusLab.font.pointSize) + 10);
        make.height.mas_equalTo(stringGetHeight(liveStatusLab.text, liveStatusLab.font.pointSize) + 4);
    }];
    
    UIView *liveStatusBg = [UIView by_init];
    liveStatusBg.layer.cornerRadius = 2.0f;
    [self.contentView addSubview:liveStatusLab];
    [self.contentView insertSubview:liveStatusBg belowSubview:liveStatusLab];
    self.liveStatusBg = liveStatusBg;
    [liveStatusBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
        make.top.mas_equalTo(25);
        make.width.mas_equalTo(0);
        make.height.mas_equalTo(liveStatusLab.mas_height).mas_offset(0);
    }];
    
    NSString *filePath = [[NSBundle bundleWithPath:[[NSBundle mainBundle] bundlePath]] pathForResource:@"video_living_animation.gif" ofType:nil];
    NSData *imageData = [NSData dataWithContentsOfFile:filePath];
    
    FLAnimatedImageView *livIngImgView = [FLAnimatedImageView by_init];
    livIngImgView.animatedImage = [FLAnimatedImage animatedImageWithGIFData:imageData];
    livIngImgView.hidden = YES;
    [self.contentView addSubview:livIngImgView];
    self.livIngImgView = livIngImgView;
    [livIngImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.centerY.mas_equalTo(liveStatusBg);
        make.width.mas_equalTo(8);
        make.height.mas_equalTo(9);
    }];
}

- (void)addPushFlowView{
    UIView *pushPlowView = [UIView by_init];
    pushPlowView.hidden = YES;
    [self.contentView addSubview:pushPlowView];
    self.pushPlowView = pushPlowView;
    [pushPlowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.timeLab.mas_bottom).mas_offset(8);
        make.height.mas_equalTo(30);
    }];
    
    UIButton *copyBtn = [UIButton by_buttonWithCustomType];
    [copyBtn setBy_imageName:@"liveDetail_copy" forState:UIControlStateNormal];
    [copyBtn setBy_attributedTitle:@{@"title":@"推流码复制",
                                     NSFontAttributeName:[UIFont systemFontOfSize:13],
                                     NSForegroundColorAttributeName:kColorRGBValue(0x323232)
                                     } forState:UIControlStateNormal];
    [copyBtn addTarget:self action:@selector(copyBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [pushPlowView addSubview:copyBtn];
    copyBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
    copyBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -3, 0, 0);
    [copyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(90);
    }];

    UIButton *tutorialBtn = [UIButton by_buttonWithCustomType];
    [tutorialBtn setBy_imageName:@"liveDetail_view" forState:UIControlStateNormal];
    [tutorialBtn setBy_attributedTitle:@{@"title":@"查看推流教程",
                                     NSFontAttributeName:[UIFont systemFontOfSize:13],
                                     NSForegroundColorAttributeName:kColorRGBValue(0x323232)
                                     } forState:UIControlStateNormal];
    [tutorialBtn addTarget:self action:@selector(tutorialBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [pushPlowView addSubview:tutorialBtn];
    tutorialBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
    tutorialBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -3, 0, 0);
    [tutorialBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(125);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(100);
    }];
}

@end
