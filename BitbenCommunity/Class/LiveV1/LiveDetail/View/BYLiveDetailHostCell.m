//
//  BYLiveDetailHostCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveDetailHostCell.h"
#import "BYLiveDetailModel.h"
#import "BYPersonHomeController.h"

@interface BYLiveDetailHostCell ()

/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogo;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 简介 */
@property (nonatomic ,strong) UILabel *introLab;
/** 关注按钮 */
@property (nonatomic ,strong) UIButton *attentonBtn;
/** 底部横线 */
@property (nonatomic ,strong) UIView *lineView;
/** model */
@property (nonatomic ,strong) BYLiveDetailUserModel *model;


@end

@implementation BYLiveDetailHostCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYLiveDetailUserModel *model = dic.allValues[0][indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    if (model.user_type == BY_NEWLIVE_MICRO_TYPE_APP) {
        self.attentonBtn.hidden = [model.user_id isEqualToString:ACCOUNT_ID] ? YES : NO;
    }else{
        self.attentonBtn.hidden = YES;
    }
    self.attentonBtn.selected = model.isAttention;
    [self.userlogo uploadHDImageWithURL:model.head_img callback:nil];
    self.userNameLab.text = model.nickname;
    self.introLab.text = nullToEmpty(model.intro);
    self.lineView.hidden = !model.showBottomLine;
    
    
    if (model.isActivity) {
        BYCommonModel *titleModel = dic.allValues[0][indexPath.row - 1];
        if ([titleModel.title isEqualToString:@"创建人"]) {
            self.attentonBtn.hidden = [model.user_id isEqualToString:ACCOUNT_ID] ? YES : NO;
        }else{
            self.attentonBtn.hidden = YES;
        }
    }
    
    [self reloadAttentionStatus];
    CGFloat top = model.intro.length ? 4 : 13;
    [self.userNameLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.userlogo).offset(top);
    }];
}

- (void)reloadAttentionStatus{
    UIColor *bgColor = self.model.isAttention ? kColorRGBValue(0xc3c3c3) : kColorRGBValue(0xea6438);
    [self.attentonBtn setBackgroundColor:bgColor];
}

- (void)attentonBtnAction:(UIButton *)sender{
    [self sendActionName:@"attentionAction" param:nil indexPath:self.indexPath];
}

- (void)userlogoAction{
    if (![self.model.user_id verifyIsNum]) return;
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
    personHomeController.user_id = self.model.user_id;
    [(BYCommonViewController *)currentController authorizePush:personHomeController animation:YES];
}

- (void)setContentView{
    self.userlogo = [[PDImageView alloc] init];
    self.userlogo.contentMode = UIViewContentModeScaleAspectFill;
    self.userlogo.layer.cornerRadius = 19;
    self.userlogo.clipsToBounds = YES;
    @weakify(self);
    [self.userlogo addTapGestureRecognizer:^{
        @strongify(self);
        [self userlogoAction];
    }];
    [self.contentView addSubview:self.userlogo];
    [self.userlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(15);
        make.width.height.mas_equalTo(38);
    }];
    
    self.userNameLab = [UILabel by_init];
    self.userNameLab.font = [UIFont boldSystemFontOfSize:14];
    self.userNameLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:self.userNameLab];
    [self.userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.userlogo.mas_right).offset(12);
        make.top.mas_equalTo(self.userlogo).offset(4);
        make.right.mas_equalTo(-100);
        make.height.mas_equalTo(self.userNameLab.font.pointSize);
    }];
    
    self.introLab = [UILabel by_init];
    self.introLab.font = [UIFont systemFontOfSize:12];
    self.introLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:self.introLab];
    [self.introLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.userlogo.mas_right).offset(12);
        make.top.mas_equalTo(self.userNameLab.mas_bottom).offset(7);
        make.height.mas_equalTo(self.introLab.font.pointSize);
        make.right.mas_equalTo(-100);
    }];
    
    self.attentonBtn = [UIButton by_buttonWithCustomType];
    [self.attentonBtn setBy_attributedTitle:@{@"title":@"关注",
                                        NSForegroundColorAttributeName:[UIColor whiteColor],
                                        NSFontAttributeName:[UIFont systemFontOfSize:12],
                                        } forState:UIControlStateNormal];
    [self.attentonBtn setBy_attributedTitle:@{@"title":@"已关注",
                                        NSForegroundColorAttributeName:[UIColor whiteColor],
                                        NSFontAttributeName:[UIFont systemFontOfSize:12],
                                        } forState:UIControlStateSelected];
    [self.attentonBtn addTarget:self
                   action:@selector(attentonBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];
    self.attentonBtn.layer.cornerRadius = 4.0f;
    self.attentonBtn.hidden = YES;
    [self.contentView addSubview:self.attentonBtn];
    [self.attentonBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(46);
        make.height.mas_equalTo(22);
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(self.userlogo);
    }];
    
    self.lineView = [UIView by_init];
    [self.lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    self.lineView.hidden = YES;
    [self.contentView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(66);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
}

@end
