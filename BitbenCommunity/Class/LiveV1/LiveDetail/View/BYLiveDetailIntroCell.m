//
//  BYLiveDetailIntroCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveDetailIntroCell.h"
#import "BYLiveDetailModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>
#import "BYActivityDetailModel.h"

@interface BYLiveDetailIntroCell ()

/** 简介 */
@property (nonatomic ,strong) YYLabel *introLab;
/** 海报 */
@property (nonatomic ,strong) PDImageView *adImgView;
/** 空imageView */
@property (nonatomic ,strong) UIImageView *emptyImgView;
/** 空文案 */
@property (nonatomic ,strong) UILabel *emptyLabel;
/** titleLab */
@property (nonatomic ,strong) UILabel *titleLab;


@end

@implementation BYLiveDetailIntroCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
        
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    id obj = dic.allValues[0][indexPath.row];
    
    if ([obj isKindOfClass:[BYLiveDetailModel class]]) {
        [self setLiveDetailIntroData:obj];
    }else if ([obj isKindOfClass:[BYActivityDetailModel class]]) {
        [self setActiviryDetailIntroData:obj];
    }
    
}

- (void)setLiveDetailIntroData:(BYLiveDetailModel *)model{
    if (!model.live_intro.length && !model.ad_img.length) {
        [self addEmptyView];
        return;
    }
    
    NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.live_intro)];
    messageAttributed.yy_lineSpacing = 12.0f;
    messageAttributed.yy_font = [UIFont systemFontOfSize:14];
    messageAttributed.yy_color = kColorRGBValue(0x585858);
    YYTextContainer *textContainer = [YYTextContainer new];
    textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
    YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
    [self.introLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(ceil(layout.textBoundingSize.height));
    }];
    self.introLab.attributedText = messageAttributed;
    
    @weakify(self);
    [self.adImgView uploadHDImageWithURL:model.ad_img callback:^(UIImage *image) {
        @strongify(self);
        if (image) {
            CGFloat adImgH = image.size.height*(kCommonScreenWidth - 30)/image.size.width;
            model.cellHeight = 73 + layout.textBoundingSize.height + 20 + adImgH + 10;
            [self.adImgView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(adImgH);
            }];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        }
    }];
}

- (void)setActiviryDetailIntroData:(BYActivityDetailModel *)model{
    if (!model.activity_intro.length && !model.ad_img.length) {
        [self addEmptyView];
        return;
    }
    self.titleLab.text = @"会议详情";
    NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.activity_intro)];
    messageAttributed.yy_lineSpacing = 12.0f;
    messageAttributed.yy_font = [UIFont systemFontOfSize:14];
    messageAttributed.yy_color = kColorRGBValue(0x585858);
    YYTextContainer *textContainer = [YYTextContainer new];
    textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
    YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
    [self.introLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(ceil(layout.textBoundingSize.height));
    }];
    self.introLab.attributedText = messageAttributed;
    
    @weakify(self);
    [self.adImgView uploadHDImageWithURL:model.ad_img callback:^(UIImage *image) {
        @strongify(self);
        if (image) {
            CGFloat adImgH = image.size.height*(kCommonScreenWidth - 30)/image.size.width;
            model.cellHeight = 73 + layout.textBoundingSize.height + 20 + adImgH + 10;
            [self.adImgView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(adImgH);
            }];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        }
    }];
}

- (void)imageViewAction{
    if (!self.adImgView.image) return;
    [self sendActionName:@"imageViewAction" param:@{@"imageView":self.adImgView} indexPath:self.indexPath];
}

- (void)setContentView{
    
    UILabel *titleLab = [UILabel by_init];
    titleLab.font = [UIFont boldSystemFontOfSize:16];
    titleLab.textColor = kColorRGBValue(0x313131);
    titleLab.text = @"直播详情";
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(24);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.width.mas_equalTo(70);
    }];
    
    YYLabel *introLab = [[YYLabel alloc] init];
    introLab.font = [UIFont systemFontOfSize:14];
    introLab.textColor = kColorRGBValue(0x585858);
    introLab.numberOfLines = 0;
    [self.contentView addSubview:introLab];
    self.introLab = introLab;
    [introLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(24);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    PDImageView *adImgView = [[PDImageView alloc] init];
    [self.contentView addSubview:adImgView];
    self.adImgView = adImgView;
    [adImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(introLab.mas_bottom).mas_offset(22);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    @weakify(self);
    [adImgView addTapGestureRecognizer:^{
        @strongify(self);
        [self imageViewAction];
    }];
}

- (void)addEmptyView{
    if (_emptyImgView) return;
    [self.contentView addSubview:self.emptyImgView];
    [self.contentView addSubview:self.emptyLabel];
    [self.emptyImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(70);
        make.width.mas_equalTo(208);
        make.height.mas_equalTo(151);
    }];
    
    [self.emptyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(self.emptyImgView.mas_bottom).offset(24);
        make.height.mas_equalTo(self.emptyLabel.font.pointSize);
        make.width.mas_equalTo(100);
    }];
}

- (UIImageView *)emptyImgView{
    if (!_emptyImgView) {
        _emptyImgView = [[UIImageView alloc] init];
        [_emptyImgView by_setImageName:@"common_livedetail_empty"];
    }
    return _emptyImgView;
}

- (UILabel *)emptyLabel{
    if (!_emptyLabel) {
        _emptyLabel = [[UILabel alloc] init];
        _emptyLabel.text = @"空空如也～";
        _emptyLabel.font = [UIFont systemFontOfSize:14];
        _emptyLabel.textAlignment = NSTextAlignmentCenter;
        _emptyLabel.textColor = kColorRGBValue(0xa9a9a9);
    }
    return _emptyLabel;
}

@end
