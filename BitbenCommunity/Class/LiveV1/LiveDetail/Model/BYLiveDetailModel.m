//
//  BYLiveDetailModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveDetailModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

@implementation BYLiveDetailUserModel

@end

@implementation BYLiveDetailModel

+ (NSArray *)getTableData:(BYCommonLiveModel *)model{
    NSMutableArray *data = [NSMutableArray array];
    for (int i = 0; i < 3; i++) {
        NSDictionary *dic = [model mj_keyValuesWithIgnoredKeys:@[@"shareMap"]];
        if (i == 0) {
            BYLiveDetailModel *detailModel = [BYLiveDetailModel mj_objectWithKeyValues:dic];
            [data addObject:@{@"data":@[detailModel]}];
            detailModel.cellString = @"BYLiveDetailTitleCell";
            // 封面图高度
            CGFloat coverH = (kCommonScreenWidth - 30)*180/345;
            NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.live_title)];
            messageAttributed.yy_lineSpacing = 8.0f;
            messageAttributed.yy_font = [UIFont boldSystemFontOfSize:15];
            messageAttributed.yy_color = kColorRGBValue(0x323232);
            YYTextContainer *textContainer = [YYTextContainer new];
            textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
            YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
            if (model.live_type == BY_NEWLIVE_TYPE_VIDEO) {
                if (![model.user_id isEqualToString:[AccountModel sharedAccountModel].account_id]) {
                    detailModel.cellHeight = coverH + layout.textBoundingSize.height + 167 - 59;
                }else{
                    detailModel.cellHeight = coverH + layout.textBoundingSize.height + 150 + 49 - 59;
                }
            }else{
                detailModel.cellHeight = coverH + layout.textBoundingSize.height + 167 - 59;
            }
        }
        else if (i == 1){
            NSMutableArray *cellData = [NSMutableArray array];
            if (model.host_list.count) {
                BYCommonModel *titleModel = [self getSignleTitleModel:@"主持人"];
                [cellData addObject:titleModel];
                [model.host_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary *tmpDic = [obj mj_keyValues];
                    BYLiveDetailUserModel *userModel = [BYLiveDetailUserModel mj_objectWithKeyValues:tmpDic];
                    userModel.cellString = @"BYLiveDetailHostCell";
                    userModel.cellHeight = 68;
                    userModel.showBottomLine = model.interviewee_list.count ? YES : NO;
                    [cellData addObject:userModel];
                }];
            }
            if (model.interviewee_list.count) {
                BYCommonModel *titleModel = [self getSignleTitleModel:@"嘉宾"];
                [cellData addObject:titleModel];
                [model.interviewee_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary *tmpDic = [obj mj_keyValues];
                    BYLiveDetailUserModel *userModel = [BYLiveDetailUserModel mj_objectWithKeyValues:tmpDic];
                    userModel.cellString = @"BYLiveDetailHostCell";
                    userModel.cellHeight = 68;
                    userModel.showBottomLine = idx != model.host_list.count - 1 ? YES : NO;
                    [cellData addObject:userModel];
                }];
            }
            [data addObject:@{@"data":[cellData copy]}];
        }
        else{
            BYLiveDetailModel *detailModel = [BYLiveDetailModel mj_objectWithKeyValues:dic];
            [data addObject:@{@"data":@[detailModel]}];
            detailModel.cellString = @"BYLiveDetailIntroCell";
            
            NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.live_intro)];
            messageAttributed.yy_lineSpacing = 12.0f;
            messageAttributed.yy_font = [UIFont systemFontOfSize:14];
            messageAttributed.yy_color = kColorRGBValue(0x585858);
            YYTextContainer *textContainer = [YYTextContainer new];
            textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
            YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
        
            detailModel.titleSize = layout.textBoundingSize;
            if (!detailModel.live_intro.length && !detailModel.ad_img.length) {
                detailModel.cellHeight = 300;
            }else {
                detailModel.cellHeight = 73 + layout.textBoundingSize.height + 10;
            }
        }
    }
    return [data copy];
}

+ (BYCommonModel *)getSignleTitleModel:(NSString *)title{
    BYCommonModel *model = [[BYCommonModel alloc] init];
    model.cellString = @"BYLiveDetailSingleTitleCell";
    model.cellHeight = 30;
    model.title = title;
    return model;
}

+ (NSArray *)getPushFlowIntrolTableData:(BYCommonLiveModel *)model{
    NSMutableArray *data = [NSMutableArray array];
    for (int i = 0; i < 3; i++) {
        NSDictionary *dic = [model mj_keyValuesWithIgnoredKeys:@[@"shareMap"]];
        if (i == 0) {
            BYLiveDetailModel *detailModel = [BYLiveDetailModel mj_objectWithKeyValues:dic];
            [data addObject:@{@"data":@[detailModel]}];
            detailModel.cellString = @"BYPushFlowIntroTitleCell";
            // 封面图高度
            NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.live_title)];
            messageAttributed.yy_lineSpacing = 8.0f;
            messageAttributed.yy_font = [UIFont boldSystemFontOfSize:15];
            messageAttributed.yy_color = kColorRGBValue(0x323232);
            YYTextContainer *textContainer = [YYTextContainer new];
            textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
            YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
            detailModel.cellHeight = layout.textBoundingSize.height + 91;
        }
        else if (i == 1){
            NSMutableArray *cellData = [NSMutableArray array];
            if (model.host_list.count) {
                BYCommonModel *titleModel = [self getSignleTitleModel:@"主持人"];
                [cellData addObject:titleModel];
                [model.host_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary *tmpDic = [obj mj_keyValues];
                    BYLiveDetailUserModel *userModel = [BYLiveDetailUserModel mj_objectWithKeyValues:tmpDic];
                    userModel.cellString = @"BYLiveDetailHostCell";
                    userModel.cellHeight = 68;
                    userModel.showBottomLine = model.interviewee_list.count ? YES : NO;
                    [cellData addObject:userModel];
                }];
            }
            if (model.interviewee_list.count) {
                BYCommonModel *titleModel = [self getSignleTitleModel:@"嘉宾"];
                [cellData addObject:titleModel];
                [model.interviewee_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary *tmpDic = [obj mj_keyValues];
                    BYLiveDetailUserModel *userModel = [BYLiveDetailUserModel mj_objectWithKeyValues:tmpDic];
                    userModel.cellString = @"BYLiveDetailHostCell";
                    userModel.cellHeight = 68;
                    userModel.showBottomLine = idx != model.host_list.count - 1 ? YES : NO;
                    [cellData addObject:userModel];
                }];
            }
            [data addObject:@{@"data":[cellData copy]}];
        }
        else{
            BYLiveDetailModel *detailModel = [BYLiveDetailModel mj_objectWithKeyValues:dic];
            [data addObject:@{@"data":@[detailModel]}];
            detailModel.cellString = @"BYLiveDetailIntroCell";
            
            NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.live_intro)];
            messageAttributed.yy_lineSpacing = 12.0f;
            messageAttributed.yy_font = [UIFont systemFontOfSize:14];
            messageAttributed.yy_color = kColorRGBValue(0x585858);
            YYTextContainer *textContainer = [YYTextContainer new];
            textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
            YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
            
            detailModel.titleSize = layout.textBoundingSize;
            if (!detailModel.live_intro.length && !detailModel.ad_img.length) {
                detailModel.cellHeight = 300;
            }else {
                detailModel.cellHeight = 73 + layout.textBoundingSize.height + 10;
            }
        }
    }
    return [data copy];
}

+ (NSArray *)getILiveIntrolTableData:(BYCommonLiveModel *)model{
    NSMutableArray *data = [NSMutableArray array];
    for (int i = 0; i < 3; i++) {
        NSDictionary *dic = [model mj_keyValuesWithIgnoredKeys:@[@"shareMap"]];
        if (i == 0) {
            BYLiveDetailModel *detailModel = [BYLiveDetailModel mj_objectWithKeyValues:dic];
            [data addObject:@{@"data":@[detailModel]}];
            detailModel.cellString = @"BYILiveIntroTitleCell";
            // 封面图高度
            NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.live_title)];
            messageAttributed.yy_lineSpacing = 8.0f;
            messageAttributed.yy_font = [UIFont boldSystemFontOfSize:15];
            messageAttributed.yy_color = kColorRGBValue(0x323232);
            YYTextContainer *textContainer = [YYTextContainer new];
            textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
            YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
            
            CGFloat coverH = (kCommonScreenWidth - 30)*180/345;
            detailModel.cellHeight = layout.textBoundingSize.height + 91 + coverH + 10;
        }
        else if (i == 1){
            NSMutableArray *cellData = [NSMutableArray array];
            if (model.host_list.count) {
                BYCommonModel *titleModel = [self getSignleTitleModel:@"主持人"];
                [cellData addObject:titleModel];
                [model.host_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary *tmpDic = [obj mj_keyValues];
                    BYLiveDetailUserModel *userModel = [BYLiveDetailUserModel mj_objectWithKeyValues:tmpDic];
                    userModel.cellString = @"BYLiveDetailHostCell";
                    userModel.cellHeight = 68;
                    userModel.showBottomLine = model.interviewee_list.count ? YES : NO;
                    [cellData addObject:userModel];
                }];
            }
            if (model.interviewee_list.count) {
                BYCommonModel *titleModel = [self getSignleTitleModel:@"嘉宾"];
                [cellData addObject:titleModel];
                [model.interviewee_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary *tmpDic = [obj mj_keyValues];
                    BYLiveDetailUserModel *userModel = [BYLiveDetailUserModel mj_objectWithKeyValues:tmpDic];
                    userModel.cellString = @"BYLiveDetailHostCell";
                    userModel.cellHeight = 68;
                    userModel.showBottomLine = idx != model.host_list.count - 1 ? YES : NO;
                    [cellData addObject:userModel];
                }];
            }
            [data addObject:@{@"data":[cellData copy]}];
        }
        else{
            BYLiveDetailModel *detailModel = [BYLiveDetailModel mj_objectWithKeyValues:dic];
            [data addObject:@{@"data":@[detailModel]}];
            detailModel.cellString = @"BYLiveDetailIntroCell";
            
            NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.live_intro)];
            messageAttributed.yy_lineSpacing = 12.0f;
            messageAttributed.yy_font = [UIFont systemFontOfSize:14];
            messageAttributed.yy_color = kColorRGBValue(0x585858);
            YYTextContainer *textContainer = [YYTextContainer new];
            textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
            YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
            
            detailModel.titleSize = layout.textBoundingSize;
            if (!detailModel.live_intro.length && !detailModel.ad_img.length) {
                detailModel.cellHeight = 300;
            }else {
                detailModel.cellHeight = 73 + layout.textBoundingSize.height + 10;
            }
        }
    }
    return [data copy];
}

@end
