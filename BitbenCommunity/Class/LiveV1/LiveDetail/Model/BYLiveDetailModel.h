//
//  BYLiveDetailModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYEditLiveModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYLiveDetailUserModel : BYCommonModel

/** 用户id */
@property (nonatomic ,copy) NSString *user_id;
/** 用户头像 */
@property (nonatomic ,copy) NSString *head_img;
/** 用户简介 */
@property (nonatomic ,copy) NSString *intro;
/** 用户昵称 */
@property (nonatomic ,copy) NSString *nickname;
/** 关注状态 */
@property (nonatomic ,assign) BOOL isAttention;
/** 绑定的直播id */
@property (nonatomic ,copy) NSString *live_recoed_id;
/** 用户类型 */
@property (nonatomic ,assign) BY_NEWLIVE_MICRO_TYPE user_type;
/** 身份类型 */
@property (nonatomic ,assign) BY_IDENTITY_TYPE identity_type;
/** 是否显示底部横线 */
@property (nonatomic ,assign) BOOL showBottomLine;
/** 场景是否为活动 */
@property (nonatomic ,assign) BOOL isActivity;

@end

@interface BYLiveDetailModel : BYEditLiveModel

/** size */
@property (nonatomic ,assign) CGSize titleSize;
/** status */
@property (nonatomic ,assign) BY_LIVE_STATUS status;
/** live_type */
@property (nonatomic ,assign) BY_NEWLIVE_TYPE live_type;
/** user_id */
@property (nonatomic ,copy) NSString *user_id;
/** 关注状态(直播的关注) */
@property (nonatomic ,assign) BOOL attention;



+ (NSArray *)getTableData:(BYCommonLiveModel *)model;

+ (NSArray *)getPushFlowIntrolTableData:(BYCommonLiveModel *)model;

+ (NSArray *)getILiveIntrolTableData:(BYCommonLiveModel *)model;
@end

NS_ASSUME_NONNULL_END
