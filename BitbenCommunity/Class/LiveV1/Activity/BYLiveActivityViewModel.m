//
//  BYLiveActivityViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveActivityViewModel.h"
#import "BYActivityDetailController.h"

#import "BYActivityModel.h"

@interface BYLiveActivityViewModel ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;


@end

@implementation BYLiveActivityViewModel

- (void)setContentView{
    self.pageNum = 0;
    [S_V_VIEW addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [self loadRequest];
}

- (void)reloadData{
    self.pageNum = 0;
    [self loadRequest];
}

- (void)loadMoreData{
    [self loadRequest];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BYActivityModel *model = tableView.tableData[indexPath.row];
    BYActivityDetailController *detailController = [[BYActivityDetailController alloc] init];
    detailController.activiry_id = model.activity_id;
    [S_V_NC pushViewController:detailController animated:YES];
}

#pragma mark - request
- (void)loadRequest{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetActivity:self.pageNum successBlock:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        if (self.pageNum == 0) {
            self.tableView.tableData = object;
            if (![object count]) {
                [self.tableView addEmptyView:@"暂无活动"];
                return ;
            }
        }
        else{
            NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [array addObjectsFromArray:object];
            self.tableView.tableData = [array copy];
        }
        self.pageNum ++;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

#pragma mark - configUI

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        [_tableView addHeaderRefreshTarget:self action:@selector(reloadData)];
        [_tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    }
    return _tableView;
}

@end
