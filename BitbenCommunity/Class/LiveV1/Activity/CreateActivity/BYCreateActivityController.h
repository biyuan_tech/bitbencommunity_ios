//
//  BYCreateActivityController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/23.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"
#import "BYActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYCreateActivityController : BYCommonViewController

/** 是否为编辑 */
@property (nonatomic ,assign) BOOL isEdit;
/** model */
@property (nonatomic ,strong) BYActivityModel *model;

@end

NS_ASSUME_NONNULL_END
