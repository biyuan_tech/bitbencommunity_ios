//
//  BYCreateActivityModelCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/23.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCreateActivityModelCell.h"
#import "BYCreateActivityModel.h"

static NSInteger baseTag = 0x231;
@interface BYCreateActivityModelCell ()

/** model */
@property (nonatomic ,strong) BYCreateActivityModel *model;


@end

@implementation BYCreateActivityModelCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYCreateActivityModel *model = object[indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    NSInteger selectIndex = model.isFree ? 0 : 1;
    for (int i = 0; i < 2; i ++) {
        UIButton *button = [self.contentView viewWithTag:baseTag + i];
        button.selected = i == selectIndex ? YES : NO;
    }
    
}

#pragma mark - action

- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - baseTag;
    BOOL isFree = index == 0 ? YES : NO;
    if (self.model.isFree == isFree) return;
    self.model.isFree = isFree;
    for (int i = 0; i < 2; i ++) {
        UIButton *button = [self.contentView viewWithTag:baseTag + i];
        button.selected = i == index ? YES : NO;
    }
    [self sendActionName:@"activityModel" param:@{@"isFree":@(self.model.isFree)} indexPath:self.indexPath];
}



#pragma mark - configUI

- (void)setContentView{
    UIView *topView = [UIView by_init];
    [topView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.contentView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(10);
    }];
    
    UILabel *modelTitleLab = [UILabel by_init];
    [modelTitleLab setBy_font:14];
    modelTitleLab.text = @"会议付费模式：";
    modelTitleLab.textColor = kColorRGBValue(0x313131);
    [self.contentView addSubview:modelTitleLab];
    [modelTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(30);
        make.height.mas_equalTo(modelTitleLab.font.pointSize);
        make.width.mas_equalTo(stringGetWidth(modelTitleLab.text, 14));
    }];
    
    NSArray *titles = @[@"免费",@"付费"];
    for (int i = 0; i < 2; i ++) {
        UIButton *button = [UIButton by_buttonWithCustomType];
        [button setBy_imageName:@"activiry_create_normal" forState:UIControlStateNormal];
        [button setBy_imageName:@"activiry_create_selected" forState:UIControlStateSelected];
        [button setBy_attributedTitle:@{@"title":titles[i],
                                        NSFontAttributeName:[UIFont systemFontOfSize:14],
                                        NSForegroundColorAttributeName:kColorRGBValue(0x313131)
                                        } forState:UIControlStateNormal];
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 4);
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 4, 0, 0);
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = baseTag + i;
        button.selected = i == 0 ? YES : NO;
        [self.contentView addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(modelTitleLab.mas_right).offset(15 + 80*i);
            make.centerY.mas_equalTo(modelTitleLab);
            make.height.mas_equalTo(25);
            make.width.mas_equalTo(50);
        }];
    }
}

@end
