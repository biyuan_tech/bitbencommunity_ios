//
//  BYCreateActivityPeopleCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/23.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCreateActivityPeopleCell.h"
#import "BYCreateActivityModel.h"

@interface BYCreateActivityPeopleCell () <UITextFieldDelegate>

/** textField */
@property (nonatomic ,strong) UITextField *textField;
/** model */
@property (nonatomic ,strong) BYCreateActivityModel *model;

@end

@implementation BYCreateActivityPeopleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYCreateActivityModel *model = object[indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    if (model.maxNum > 0) {
        self.textField.text = stringFormatInteger(model.maxNum);
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@""]) return YES;
    NSString *numberRegex = @"[0-9]";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegex];
    return [numberTest evaluateWithObject:string];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (!textField.text.length) return;
    self.model.maxNum = [textField.text integerValue];
}

#pragma mark - configUI

- (void)setContentView{
    UIView *topView = [UIView by_init];
    [topView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.contentView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(10);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    titleLab.text = @"人数限制：";
    titleLab.textColor = kColorRGBValue(0x313131);
    [self.contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(28);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.width.mas_equalTo(stringGetWidth(titleLab.text, 14));
    }];
    
    self.textField = [[UITextField alloc] init];
    self.textField.delegate = self;
    CGFloat fontSize = iPhone5 ? 11 : 14;
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"请填写人数限制，不填写默认为无人数限制" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSForegroundColorAttributeName:kColorRGBValue(0xc7c7c7)}];
    self.textField.attributedPlaceholder = attributedString;
    self.textField.font = [UIFont systemFontOfSize:14];
    self.textField.textColor = kColorRGBValue(0x313131);
    self.textField.keyboardType = UIKeyboardTypeNumberPad;
    [self.contentView addSubview:self.textField];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(88);
        make.top.mas_equalTo(10);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(49);
    }];
}

@end
