//
//  BYCreateActivityTitleCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/23.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCreateActivityTitleCell.h"
#import "BYCreateActivityModel.h"

static NSInteger baseTag = 0x255;
@interface BYCreateActivityTitleCell ()<UITextFieldDelegate>

/** model */
@property (nonatomic ,strong) BYCreateActivityModel *model;
/** 封面 */
@property (nonatomic ,strong) PDImageView *coverImgView;


@end

@implementation BYCreateActivityTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    [super setContentWithObject:object indexPath:indexPath];
    BYCreateActivityModel *model = object[indexPath.row];
    self.model = model;
    if (model.coverImg) {
        self.coverImgView.image = model.coverImg;
        self.coverImgView.hidden = NO;
    }else if (model.live_cover_url.length){
        @weakify(self);
        [self.coverImgView uploadHDImageWithURL:model.live_cover_url callback:^(UIImage *image) {
            @strongify(self);
            if (!image) return ;
            self.model.coverImg = image;
            self.coverImgView.hidden = NO;
        }];
    }else{
        self.coverImgView.hidden = YES;
    }
    
    for (int i = 0; i < 2; i ++) {
        UITextField *textField = [self.contentView viewWithTag:baseTag + i];
        textField.text = i == 0 ? nullToEmpty(model.activityName) : nullToEmpty(model.activityLocation);
    }
}

#pragma mark - action

- (void)addImageAction{
    [self sendActionName:@"selImgAction" param:nil indexPath:self.indexPath];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *rangString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField.tag == baseTag) {
        //        if (rangString.length > 9) {
        //            return NO;
        //        }
    }else{
        //        if (rangString.length > 10) { // 当直播主题输入10字符以上时禁止继续输入
        //            return NO;
        //        }
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag == baseTag) {
        self.model.activityName = textField.text;
    }else {
        self.model.activityLocation = textField.text;
    }
}

#pragma mark - configUI

- (void)setContentView{
    
    UIView *topView = [UIView by_init];
    [topView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.contentView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(10);
    }];
    
    UIImageView *editImgView = [UIImageView by_init];
    [editImgView by_setImageName:@"newlive_addImg"];
    editImgView.userInteractionEnabled = YES;
    [self.contentView addSubview:editImgView];
    [editImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(90);
        make.top.mas_equalTo(25);
        make.left.mas_equalTo(15);
    }];
    @weakify(self);
    [editImgView addTapGestureRecognizer:^{
        @strongify(self);
        [self addImageAction];
    }];
    
    UILabel *editImgLab = [UILabel by_init];
    [editImgLab setBy_font:14];
    editImgLab.textColor = kColorRGBValue(0xa3a3a3);
    editImgLab.text = @"上传封面";
    [editImgView addSubview:editImgLab];
    [editImgLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(stringGetWidth(editImgLab.text, 14));
        make.height.mas_equalTo(editImgLab.font.pointSize);
        make.bottom.mas_equalTo(-18);
    }];
    
    PDImageView *coverImgView = [[PDImageView alloc] init];
    coverImgView.layer.cornerRadius = 5.0;
    coverImgView.userInteractionEnabled = YES;
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.clipsToBounds = YES;
    coverImgView.hidden = YES;
    [self.contentView addSubview:coverImgView];
    self.coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(editImgView);
    }];
    [coverImgView addTapGestureRecognizer:^{
        @strongify(self);
        [self addImageAction];
    }];
    
    UILabel *coverTitleLab = [UILabel by_init];
    coverTitleLab.backgroundColor = kColorRGB(50, 50, 50, 0.8);
    [coverTitleLab setBy_font:11];
    coverTitleLab.textColor = [UIColor whiteColor];
    coverTitleLab.text = @"封面";
    coverTitleLab.textAlignment = NSTextAlignmentCenter;
    [coverImgView addSubview:coverTitleLab];
    [coverTitleLab layerCornerRadius:4.0f byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight size:CGSizeMake(90, 20)];
    [coverTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(20);
    }];
    
    NSArray *placeholders = @[@"会议名称",@"会议地点"];
    for (int i = 0; i < 2; i ++) {
        UIView *textView = [UIView by_init];
        [self.contentView addSubview:textView];
        [textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(105);
            make.top.mas_equalTo(10 + 60*i);
            make.right.mas_equalTo(-15);
            make.height.mas_equalTo(60);
        }];
        [self textViewAddSubView:textView placeholder:placeholders[i] index:i];
    }
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(105);
        make.top.mas_equalTo(70);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)textViewAddSubView:(UIView *)textView placeholder:(NSString *)placeholder index:(NSInteger)index{
    UIImageView *mustNameImgView = [UIImageView by_init];
    [mustNameImgView by_setImageName:@"newlive_must"];
    [textView addSubview:mustNameImgView];
    [mustNameImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.centerY.mas_equalTo(0);
        make.width.height.mas_equalTo(6);
    }];
    
    UITextField *textField = [UITextField by_init];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0x313131)}];
    textField.attributedPlaceholder = attributedString;
    textField.delegate = self;
    textField.tag = baseTag + index;
    [textView addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mustNameImgView.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(mustNameImgView);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(textView);
    }];
}

@end
