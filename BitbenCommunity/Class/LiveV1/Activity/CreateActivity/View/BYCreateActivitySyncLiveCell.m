//
//  BYCreateActivitySyncLiveCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCreateActivitySyncLiveCell.h"
#import "BYSwitch.h"

#import "BYCreateActivityModel.h"


@interface BYCreateActivitySyncLiveCell ()

/** switch */
@property (nonatomic ,strong) BYSwitch *syncLiveSwitch;
/** model */
@property (nonatomic ,strong) BYCreateActivityModel *model;

@end

@implementation BYCreateActivitySyncLiveCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYCreateActivityModel *model = object[indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    
    [self.syncLiveSwitch setOn:model.isSyncLive animated:YES];
}

- (void)switchAction{
    self.model.isSyncLive = self.syncLiveSwitch.on;
    [self sendActionName:@"switchSyncLiveAction" param:nil indexPath:self.indexPath];
}

- (void)setContentView{
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    titleLab.text = @"同步线上直播：";
    titleLab.textColor = kColorRGBValue(0x313131);
    [self.contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(20);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.width.mas_equalTo(stringGetWidth(titleLab.text, 14));
    }];

    BYSwitch *syncLiveSwitch = [[BYSwitch alloc] initWithFrame:CGRectMake(130, 0, 34, 20)];
    [syncLiveSwitch setTintColor:kColorRGBValue(0xea6438) forOn:YES];
    [syncLiveSwitch setTintColor:kColorRGBValue(0xc8c8c8) forOn:NO];
    [syncLiveSwitch setCornerColor:kColorRGBValue(0xea6438) forOn:YES];
    [syncLiveSwitch setCornerColor:kColorRGBValue(0xc8c8c8) forOn:NO];
    [syncLiveSwitch addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventValueChanged];
    [self.contentView addSubview:syncLiveSwitch];
    self.syncLiveSwitch = syncLiveSwitch;
    [syncLiveSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(34);
        make.height.mas_equalTo(20);
        make.centerY.mas_equalTo(titleLab);
        make.left.mas_equalTo(titleLab.mas_right).offset(15);
    }];
}

@end
