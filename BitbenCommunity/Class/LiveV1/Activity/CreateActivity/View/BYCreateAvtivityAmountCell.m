//
//  BYCreateAvtivityAmountCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/23.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCreateAvtivityAmountCell.h"
#import "BYCreateActivityModel.h"

@interface BYCreateAvtivityAmountCell ()<UITextFieldDelegate>

/** textField */
@property (nonatomic ,strong) UITextField *textField;
/** model */
@property (nonatomic ,strong) BYCreateActivityModel *model;


@end

@implementation BYCreateAvtivityAmountCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYCreateActivityModel *model = object[indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    if (model.amount > 0) {
        self.textField.text = stringFormatInteger(model.amount);
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@""]) return YES;
    NSString *numberRegex = @"[0-9]";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegex];
    return [numberTest evaluateWithObject:string];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (!textField.text.length) return;
    NSInteger amount = [textField.text integerValue];
    if (amount < 20 || amount > 3000) {
        showToastView(@"请输入正确的金额范围", kCommonWindow);
        return;
    }
    self.model.amount = amount;
}

#pragma mark - configUI

- (void)setContentView{

    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    titleLab.text = @"输入金额：";
    titleLab.textColor = kColorRGBValue(0x313131);
    [self.contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(20);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.width.mas_equalTo(stringGetWidth(titleLab.text, 14));
    }];
    
    UILabel *amountUnit = [UILabel by_init];
    amountUnit.text = @"¥";
    amountUnit.font = [UIFont boldSystemFontOfSize:22];
    amountUnit.textColor = kColorRGBValue(0xea6438);
    CGSize size = [amountUnit.text getStringSizeWithFont:amountUnit.font maxWidth:MAXFLOAT];
    [self.contentView addSubview:amountUnit];
    [amountUnit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(titleLab).offset(5);
        make.left.mas_equalTo(titleLab.mas_right).offset(15);
        make.height.mas_equalTo(ceil(size.height));
        make.width.mas_equalTo(ceil(size.width));
    }];
    
    self.textField = [[UITextField alloc] init];
    self.textField.delegate = self;
    CGFloat fontSize = iPhone5 ? 12 : 14;
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"请输入20 - 3000整数金额" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSForegroundColorAttributeName:kColorRGBValue(0xc8c8c8)}];
    self.textField.attributedPlaceholder = attributedString;
    self.textField.font = [UIFont systemFontOfSize:14];
    self.textField.textColor = kColorRGBValue(0x313131);
    self.textField.keyboardType = UIKeyboardTypeNumberPad;
    [self.contentView addSubview:self.textField];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(124);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(55);
    }];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
}

@end
