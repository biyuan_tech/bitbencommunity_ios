//
//  BYCreateActivityLiveTypeCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/23.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCreateActivityLiveTypeCell.h"
#import "BYCreateActivityModel.h"

static NSInteger baseTag = 0x431;
@interface BYCreateActivityLiveTypeCell ()

/** model */
@property (nonatomic ,strong) BYCreateActivityModel *model;


@end

@implementation BYCreateActivityLiveTypeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYCreateActivityModel *model = object[indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    NSInteger selectIndex = model.live_type == BY_NEWLIVE_TYPE_ILIVE ? 0 : 1;
    for (int i = 0; i < 2; i ++) {
        UIButton *button = [self.contentView viewWithTag:baseTag + i];
        button.selected = i == selectIndex ? YES : NO;
    }
}

#pragma mark - action
- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - baseTag;
    for (int i = 0; i < 2; i ++) {
        UIButton *button = [self.contentView viewWithTag:baseTag + i];
        button.selected = i == index ? YES : NO;
    }
    self.model.live_type = index == 0 ? BY_NEWLIVE_TYPE_ILIVE : BY_NEWLIVE_TYPE_VIDEO;
    [self sendActionName:@"liveTypeSelAction" param:nil indexPath:self.indexPath];
}

#pragma mark - configUI

- (void)setContentView{
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    titleLab.text = @"直播类型：";
    titleLab.textColor = kColorRGBValue(0x313131);
    [self.contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(20);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.width.mas_equalTo(stringGetWidth(titleLab.text, 14));
    }];
    
    NSArray *titles = @[@"手机直播",@"推流直播"];
    for (int i = 0; i < 2; i ++) {
        UIButton *button = [UIButton by_buttonWithCustomType];
        [button setBy_imageName:@"activiry_create_normal" forState:UIControlStateNormal];
        [button setBy_imageName:@"activiry_create_selected" forState:UIControlStateSelected];
        [button setBy_attributedTitle:@{@"title":titles[i],
                                        NSFontAttributeName:[UIFont systemFontOfSize:14],
                                        NSForegroundColorAttributeName:kColorRGBValue(0x313131)
                                        } forState:UIControlStateNormal];
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 4);
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 4, 0, 0);
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = baseTag + i;
        button.selected = i == 0 ? YES : NO;
        [self.contentView addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(titleLab.mas_right).offset(25 + (78 + 30)*i);
            make.centerY.mas_equalTo(titleLab);
            make.height.mas_equalTo(25);
            make.width.mas_equalTo(78);
        }];
    }
}

@end
