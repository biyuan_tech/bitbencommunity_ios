//
//  BYCreateActivityBindWxController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"
#import "BYActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYCreateActivityBindWxController : BYCommonViewController

/** 当前是编辑流程(分新建直播流程、编辑直播信息流程) */
@property (nonatomic ,assign) BOOL isEdit;
/** 编辑页数据 */
@property (nonatomic ,strong) NSArray *tableData;

// 会议创建公用
/** 是否为会议 */
@property (nonatomic ,assign) BOOL isActivity;
/** 活动model */
@property (nonatomic ,strong) BYActivityModel *activityModel;

@end

NS_ASSUME_NONNULL_END
