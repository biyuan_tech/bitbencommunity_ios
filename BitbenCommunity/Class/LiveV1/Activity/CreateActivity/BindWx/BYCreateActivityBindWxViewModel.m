//
//  BYCreateActivityBindWxViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCreateActivityBindWxViewModel.h"
#import "BYCreateActivityBindWxController.h"
#import "BYEditLiveWxController.h"
#import "BYCreateActivityController.h"
#import "BYEditLiveIntroController.h"

#import "BYActivityBindWxModel.h"
#import "BYCreateActivityModel.h"
#import "BYEditLiveModel.h"

#define K_VC ((BYCreateActivityBindWxController *)S_VC)
@interface BYCreateActivityBindWxViewModel ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** toastView */
@property (nonatomic ,strong) BYToastView *toastView;
/** wxiD */
@property (nonatomic ,copy) NSString *wxId;

@end

@implementation BYCreateActivityBindWxViewModel

- (void)setContentView{
    [self addTableView];
    [self addConfirmBtn];

}


- (void)pushLiveController{
    [self destoryController];
    BYEditLiveWxController *wxController = [[BYEditLiveWxController alloc] init];
    wxController.activity_id = K_VC.activityModel.activity_id;
    wxController.wxId = self.wxId;
    wxController.navigationTitle = @"会议创建";
    [S_V_NC pushViewController:wxController animated:YES];
}

- (void)destoryController{
    NSMutableArray *array = [NSMutableArray arrayWithArray:S_V_NC.viewControllers];
    [array enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[BYCreateActivityController class]] ||
            [obj isKindOfClass:[BYEditLiveIntroController class]]) {
            [S_VC.rt_navigationController removeViewController:obj];
        }
    }];
}

- (void)selectImage:(NSIndexPath *)indexPath{
    BYActivityBindWxModel *model = self.tableView.tableData[indexPath.row];
    UIImage *image;
   
    if (model.wxImage) {
        image = model.wxImage;
    }
    
    if (image) {
        UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *albumAction = [UIAlertAction actionWithTitle:@"从手机相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @weakify(self);
            [self assetsLibrarySelImgCanCropper:NO cb:^(UIImage *image) {
                @strongify(self);
                [self setImageAtIndexPath:indexPath image:image];
            }];
        }];
        
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [self setImageAtIndexPath:indexPath image:nil];
        }];
        
        [sheetController addAction:albumAction];
        [sheetController addAction:confirmAction];
        [sheetController addAction:cancelAction];
        [S_V_NC presentViewController:sheetController animated:YES completion:nil];
    }else{
        @weakify(self);
        [self assetsLibrarySelImgCanCropper:NO cb:^(UIImage *image) {
            @strongify(self);
            [self setImageAtIndexPath:indexPath image:image];
        }];
    }
}

- (void)setImageAtIndexPath:(NSIndexPath *)indexPath image:(UIImage *)image{
    BYActivityBindWxModel *model = self.tableView.tableData[indexPath.row];
    model.wxImage = image;
    model.wxImage_url = @"";
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)assetsLibrarySelImgCanCropper:(BOOL)canCropper cb:(void(^)(UIImage *image))cb{
    CGFloat cropperH = kCommonScreenWidth*180/345;
    CGRect cropperRect = CGRectMake(0, (kCommonScreenHeight - cropperH)/2, kCommonScreenWidth, cropperH);
    GWAssetsLibraryViewController *assetVC = [[GWAssetsLibraryViewController alloc] init];
    assetVC.isCanCut = canCropper;
    assetVC.cropFrame = cropperRect;
    [assetVC selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
        if (cb) {
            cb(selectedImgArr[0]);
        }
    }];
    [S_V_NC pushViewController:assetVC animated:YES];
    
}

- (NSArray *)getGuestList{
    __block NSMutableArray *array = [NSMutableArray array];
    [K_VC.tableData enumerateObjectsUsingBlock:^(BYCommonModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.cellString isEqualToString:@"BYCreateActivityPeopleCell"]) {
            BYCreateActivityModel *model = (BYCreateActivityModel *)obj;
            K_VC.activityModel.limit_number = model.maxNum;
        }else if ([obj.cellString isEqualToString:@"BYCreateActivityGuestCell"]) {
            BYEditGuestInfoModel *model = (BYEditGuestInfoModel *)obj;
            NSDictionary *dic = [model mj_keyValuesWithIgnoredKeys:@[@"headImg"]];
            BYCommonLiveGuestModel *guestModel = [BYCommonLiveGuestModel mj_objectWithKeyValues:dic];
            NSDictionary *data = [guestModel mj_keyValues];
            [array addObject:data];
        }
    }];
    return [array copy];
}

#pragma mark - action
- (void)confirmBtnAction{
    BYActivityBindWxModel *model = self.tableView.tableData[0];
    if (!model.wxId.length) {
        [S_VC showToastView:@"请输入微信号"];
        return;
    }else if (!model.wxImage && !model.wxImage_url.length) {
        [S_VC showToastView:@"请上传微信二维码"];
        return;
    }
    K_VC.activityModel.user_wechat = model.wxId;
    self.toastView = [BYToastView toastViewPresentLoading];
    @weakify(self);
    [self uploadImage:^{
        @strongify(self);
        [self loadRequestActivity];
    }];
}

#pragma mark - BYCommonTableViewDelegate

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"selImgAction"]) { // 选择封面
        [self selectImage:indexPath];
        [S_V_VIEW endEditing:YES];
        
    }
}

#pragma mark - request
- (void)uploadImage:(void(^)(void))cb{
    NSMutableDictionary *modelData = [NSMutableDictionary dictionary];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [K_VC.tableData enumerateObjectsUsingBlock:^(BYCommonModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.cellString isEqualToString:@"BYCreateActivityTitleCell"]) {
            BYCreateActivityModel *model = (BYCreateActivityModel *)obj;
            if (model.coverImg && !model.live_cover_url.length) {
                [modelData setObject:obj forKey:@"activity_cover_url"];
                [dic setObject:model.coverImg forKey:@"activity_cover_url"];
            }
        }else if ([obj.cellString isEqualToString:@"BYCreateActivityTimeCell"]) {
            BYCreateActivityModel *model = (BYCreateActivityModel *)obj;
            if (model.adImg && !model.ad_img.length) {
                [modelData setObject:obj forKey:@"ad_img"];
                [dic setObject:model.adImg forKey:@"ad_img"];
            }
        }else if ([obj.cellString isEqualToString:@"BYCreateActivityGuestCell"]){
                BYEditGuestInfoModel *model = (BYEditGuestInfoModel *)obj;
                if (model.headImg && !model.head_img.length) {
                    NSString *key = [NSString stringWithFormat:@"avatar_guest_%i",(int)idx];
                    [modelData setObject:obj forKey:key];
                    [dic setObject:model.headImg forKey:key];
                }
            
        }
    }];
    
    BYActivityBindWxModel *binxWxModel = self.tableView.tableData[0];
    if (binxWxModel.wxImage && !binxWxModel.wxImage_url.length) {
        [modelData setObject:binxWxModel forKey:@"wx_img"];
        [dic setObject:binxWxModel.wxImage forKey:@"wx_img"];
    }
    
    if (!dic.allValues.count) {
        cb();
        return;
    }
    dispatch_group_t group = dispatch_group_create();
    // 控制最大并发数
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(dic.allValues.count);
    for (int i = 0; i < dic.allValues.count; i++) {
        OSSFileModel *fileModel = [[OSSFileModel alloc] init];
        NSString *str = dic.allKeys[i];
        fileModel.objcImage = dic.allValues[i];
        if (![str hasPrefix:@"avatar"]) {
            fileModel.objcName = [NSString stringWithFormat:@"image-%@-%i-%@",[NSDate by_stringFromDate:[NSDate by_date] dateformatter:@"yyyy-MM-dd-HH:mm:ss"],i,ACCOUNT_ID];
        }else {
            fileModel.objcName = [NSString stringWithFormat:@"avatar-%@-%i-%@",[NSDate by_stringFromDate:[NSDate by_date] dateformatter:@"yyyy-MM-dd-HH:mm:ss"],i,ACCOUNT_ID];
        }
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        // 用于监控请求线程在请求完成后回调
        // 多线程上传图片
        dispatch_group_async(group, dispatch_get_global_queue(0, 0), ^{
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            [[OSSManager sharedUploadManager] uploadImageManagerWithImageList:[@[fileModel] copy] withUrlBlock:^(NSArray *imgUrlArr) {
                BYCommonModel *obj = modelData[str];
                if ([str isEqualToString:@"activity_cover_url"]) {
                    K_VC.activityModel.activity_cover_url = imgUrlArr[0];
                    ((BYEditLiveModel *)obj).live_cover_url = imgUrlArr[0];
                }else if ([str isEqualToString:@"ad_img"]){
                    K_VC.activityModel.ad_img = imgUrlArr[0];
                    ((BYEditLiveModel *)obj).ad_img = imgUrlArr[0];
                }else if ([str isEqualToString:@"wx_img"]) {
                    K_VC.activityModel.user_wechat_pic = imgUrlArr[0];
                    ((BYActivityBindWxModel *)obj).wxImage_url = imgUrlArr[0];
                }
                else {
                    ((BYEditGuestInfoModel *)obj).head_img = imgUrlArr[0];
                }
                dispatch_semaphore_signal(semaphore);
                dispatch_semaphore_signal(sema);
            }];
            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        });
    }
    // 上传七牛全部回调
    dispatch_group_notify(group, dispatch_get_global_queue(0, 0), ^{
        if (cb) {
            cb();
        }
    });
}

- (void)loadRequestActivity{
  
    NSArray *guestlist = [self getGuestList];
    NSDictionary *param = @{@"activity_title":K_VC.activityModel.activity_title,
                            @"begin_time":K_VC.activityModel.begin_time,
                            @"end_time":K_VC.activityModel.end_time,
                            @"activity_type":@(K_VC.activityModel.activity_type),
                            @"ad_img":nullToEmpty(K_VC.activityModel.ad_img),
                            @"activity_cover_url":nullToEmpty(K_VC.activityModel.activity_cover_url),
                            @"scene_type":@(0),
                            @"activity_intro":nullToEmpty(K_VC.activityModel.activity_intro),
                            @"pay_fee":@(K_VC.activityModel.pay_fee),
                            @"limit_number":@(K_VC.activityModel.limit_number),
                            @"address":nullToEmpty(K_VC.activityModel.address),
                            @"user_wechat":nullToEmpty(K_VC.activityModel.user_wechat),
                            @"user_wechat_pic":nullToEmpty(K_VC.activityModel.user_wechat_pic),
                            @"interviewee_list":guestlist,
                            @"activity_id":nullToEmpty(K_VC.activityModel.activity_id)
                            };
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreateActivity:param successBlock:^(id object) {
        @strongify(self);
        [self.toastView dissmissToastView];
        K_VC.activityModel.activity_id = object[@"activity_id"];
        self.wxId = nullToEmpty(object[@"assistant_wechat"]);
        [self pushLiveController];
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.toastView dissmissToastView];
    }];
}


#pragma mark - configUI

- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.group_delegate = self;
    [S_V_VIEW addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kSafe_Mas_Bottom(49), 0));
    }];
    
  
    self.tableView.tableData = [BYActivityBindWxModel getTableData:K_VC.activityModel];
}

- (void)addConfirmBtn{
    UIButton *confirmBtn = [UIButton by_buttonWithCustomType];
    [confirmBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    [confirmBtn setBy_attributedTitle:@{@"title":@"下一步",
                                        NSFontAttributeName:[UIFont systemFontOfSize:15],
                                        NSForegroundColorAttributeName:kColorRGBValue(0xfefefe)
                                        } forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(confirmBtnAction) forControlEvents:UIControlEventTouchUpInside];
    confirmBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, kSafeAreaInsetsBottom, 0);
    [S_V_VIEW addSubview:confirmBtn];
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(49));
    }];
}


@end
