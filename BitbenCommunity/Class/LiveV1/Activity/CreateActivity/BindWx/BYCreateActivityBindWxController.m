//
//  BYCreateActivityBindWxController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCreateActivityBindWxController.h"
#import "BYCreateActivityBindWxViewModel.h"

@interface BYCreateActivityBindWxController ()

@end

@implementation BYCreateActivityBindWxController

- (Class)getViewModelClass{
    return [BYCreateActivityBindWxViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"信息绑定";
    // Do any additional setup after loading the view.
}


@end
