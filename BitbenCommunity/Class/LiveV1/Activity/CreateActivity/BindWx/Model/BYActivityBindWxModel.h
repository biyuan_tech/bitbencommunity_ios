//
//  BYActivityBindWxModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"
#import "BYActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYActivityBindWxModel : BYCommonModel

/** 微信号 */
@property (nonatomic ,copy) NSString *wxId;

/** 微信图片 */
@property (nonatomic ,strong) UIImage *wxImage;
/** 微信图片链接 */
@property (nonatomic ,copy) NSString *wxImage_url;

+ (NSArray *)getTableData:(BYActivityModel *)model;

@end

NS_ASSUME_NONNULL_END
