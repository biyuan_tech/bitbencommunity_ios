//
//  BYActivityBindWxModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityBindWxModel.h"

@implementation BYActivityBindWxModel

+ (NSArray *)getTableData:(BYActivityModel *)model{
    BYActivityBindWxModel *tmpModel = [[BYActivityBindWxModel alloc] init];
    tmpModel.cellString = @"BYActivityBindWxCell";
    tmpModel.cellHeight = 340;
    if (model) {
        tmpModel.wxId = model.user_wechat;
        tmpModel.wxImage_url = model.user_wechat_pic;
    }
    return @[tmpModel];
}

@end
