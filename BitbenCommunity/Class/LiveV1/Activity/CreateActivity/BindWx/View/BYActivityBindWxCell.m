//
//  BYActivityBindWxCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityBindWxCell.h"
#import "BYActivityBindWxModel.h"

@interface BYActivityBindWxCell ()<UITextFieldDelegate>

/** textField */
@property (nonatomic ,strong) UITextField *textField;
/** 封面 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** model */
@property (nonatomic ,strong) BYActivityBindWxModel *model;

@end

@implementation BYActivityBindWxCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYActivityBindWxModel *model = object[indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    _textField.text = model.wxId;
    if (model.wxImage) {
        self.coverImgView.image = model.wxImage;
        self.coverImgView.hidden = NO;
    }else if (model.wxImage_url.length) {
        @weakify(self);
        [self.coverImgView uploadHDImageWithURL:model.wxImage_url callback:^(UIImage *image) {
            @strongify(self);
            if (!image) return ;
            self.model.wxImage = image;
            self.coverImgView.hidden = NO;
        }];
    }else{
        self.coverImgView.hidden = YES;
    }
}
#pragma mark - action

- (void)addImageAction{
    [self sendActionName:@"selImgAction" param:nil indexPath:self.indexPath];
}

#pragma mark - <UITextFieldDelegate>
- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.model.wxId = textField.text;
}

#pragma mark - configUI

- (void)setContentView{
    UIImageView *mustNameImgView = [UIImageView by_init];
    [mustNameImgView by_setImageName:@"newlive_must"];
    [self.contentView addSubview:mustNameImgView];
    [mustNameImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(24);
        make.width.height.mas_equalTo(6);
    }];
    
    UILabel *wxIdTitleLab = [UILabel by_init];
    wxIdTitleLab.font = [UIFont boldSystemFontOfSize:16];
    wxIdTitleLab.text = @"微信号";
    wxIdTitleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:wxIdTitleLab];
    [wxIdTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mustNameImgView.mas_right).offset(10);
        make.centerY.mas_equalTo(mustNameImgView);
        make.height.mas_equalTo(wxIdTitleLab.font.pointSize);
        make.width.mas_equalTo(55);
    }];
    
    UIView *lineView = [UIView by_init];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.top.mas_equalTo(mustNameImgView.mas_bottom).offset(50);
        make.height.mas_equalTo(0.5);
        make.centerX.mas_equalTo(0);
    }];
    
    UITextField *textField = [UITextField by_init];
    NSString *placeholder = @"请填写正确的微信号";
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kColorRGBValue(0xc8c8c8)}];
    textField.textColor = kColorRGBValue(0x313131);
    textField.font = [UIFont systemFontOfSize:15];
    textField.attributedPlaceholder = attributedString;
    textField.delegate = self;
    [self.contentView addSubview:textField];
    self.textField = textField;
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wxIdTitleLab);
        make.top.mas_equalTo(wxIdTitleLab.mas_bottom);
        make.bottom.mas_equalTo(lineView.mas_top);
        make.right.mas_equalTo(-15);
    }];
    
    UIImageView *mustNameImgView1 = [UIImageView by_init];
    [mustNameImgView1 by_setImageName:@"newlive_must"];
    [self.contentView addSubview:mustNameImgView1];
    [mustNameImgView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(118);
        make.width.height.mas_equalTo(6);
    }];
    
    UILabel *wxPicTitleLab = [UILabel by_init];
    wxPicTitleLab.font = [UIFont boldSystemFontOfSize:16];
    wxPicTitleLab.text = @"上传微信二维码";
    wxPicTitleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:wxPicTitleLab];
    [wxPicTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mustNameImgView1.mas_right).offset(10);
        make.centerY.mas_equalTo(mustNameImgView1);
        make.height.mas_equalTo(wxIdTitleLab.font.pointSize);
        make.width.mas_equalTo(120);
    }];
    
    UIImageView *editImgView = [UIImageView by_init];
    [editImgView by_setImageName:@"newlive_addImg"];
    editImgView.userInteractionEnabled = YES;
    [self.contentView addSubview:editImgView];
    [editImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(90);
        make.top.mas_equalTo(mustNameImgView1.mas_bottom).offset(30);
        make.left.mas_equalTo(32);
    }];
    @weakify(self);
    [editImgView addTapGestureRecognizer:^{
        @strongify(self);
        [self addImageAction];
    }];
    
    UILabel *editImgLab = [UILabel by_init];
    [editImgLab setBy_font:12];
    editImgLab.textColor = kColorRGBValue(0xa3a3a3);
    editImgLab.numberOfLines = 2;
    editImgLab.textAlignment = NSTextAlignmentCenter;
    editImgLab.text = @"上传微信\n二维码";
    [editImgView addSubview:editImgLab];
    [editImgLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(30);
        make.bottom.mas_equalTo(-10);
    }];
    
    PDImageView *coverImgView = [[PDImageView alloc] init];
    coverImgView.layer.cornerRadius = 5.0;
    coverImgView.userInteractionEnabled = YES;
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.clipsToBounds = YES;
    coverImgView.hidden = YES;
    [self.contentView addSubview:coverImgView];
    self.coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(editImgView);
    }];
    [coverImgView addTapGestureRecognizer:^{
        @strongify(self);
        [self addImageAction];
    }];
    
    UILabel *coverTitleLab = [UILabel by_init];
    coverTitleLab.backgroundColor = kColorRGB(50, 50, 50, 0.8);
    [coverTitleLab setBy_font:11];
    coverTitleLab.textColor = [UIColor whiteColor];
    coverTitleLab.text = @"二维码";
    coverTitleLab.textAlignment = NSTextAlignmentCenter;
    [coverImgView addSubview:coverTitleLab];
    [coverTitleLab layerCornerRadius:4.0f byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight size:CGSizeMake(90, 20)];
    [coverTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(20);
    }];
    
    UILabel *bindTitleLab = [UILabel by_init];
    bindTitleLab.text = @"绑定信息说明";
    bindTitleLab.textColor = kColorRGBValue(0x323232);
    bindTitleLab.font = [UIFont systemFontOfSize:16];
    [self.contentView addSubview:bindTitleLab];
    [bindTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(editImgView);
        make.top.mas_equalTo(editImgView.mas_bottom).offset(30);
        make.height.mas_equalTo(bindTitleLab.font.pointSize);
        make.width.mas_equalTo(100);
    }];
    
    UILabel *bindLab = [UILabel by_init];
    bindLab.text = @"微信绑定后，用户报名后可直接复制添加微信好友";
    bindLab.textColor = kColorRGBValue(0xc7c7c7);
    bindLab.font = [UIFont systemFontOfSize:14];
    bindLab.numberOfLines = 2.0;
    [self.contentView addSubview:bindLab];
    CGSize size = [bindLab.text getStringSizeWithFont:bindLab.font maxWidth:kCommonScreenWidth - 47];
    [bindLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(32);
        make.top.mas_equalTo(bindTitleLab.mas_bottom).offset(11);
        make.width.mas_equalTo(ceil(size.width));
        make.height.mas_equalTo(ceil(size.height));
    }];
}

@end
