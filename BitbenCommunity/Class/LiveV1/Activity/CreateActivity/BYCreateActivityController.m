//
//  BYCreateActivityController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/23.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCreateActivityController.h"
#import "BYCreateActivityViewModel.h"

@interface BYCreateActivityController ()

@end

@implementation BYCreateActivityController

- (Class)getViewModelClass{
    return [BYCreateActivityViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"会议创建";
    // Do any additional setup after loading the view.
}

- (BYActivityModel *)model{
    if (!_model) {
        _model = [[BYActivityModel alloc] init];
        _model.activity_type = BY_NEWLIVE_TYPE_OTHER;
    }
    return _model;
}

@end
