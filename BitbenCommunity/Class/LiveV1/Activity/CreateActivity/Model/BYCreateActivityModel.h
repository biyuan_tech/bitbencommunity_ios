//
//  BYCreateActivityModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/23.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"
#import "BYActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYCreateActivityModel : BYCommonModel

/** 封面图 */
@property (nonatomic ,copy) NSString *live_cover_url;
/** 封面图 */
@property (nonatomic ,strong) UIImage *coverImg;
/** 活动名称 */
@property (nonatomic ,copy) NSString *activityName;
/** 活动地点 */
@property (nonatomic ,copy) NSString *activityLocation;
/** 直播时间 */
@property (nonatomic ,copy) NSString *begin_time;
/** 直播时间 */
@property (nonatomic ,copy) NSString *end_time;
/** 海报 */
@property (nonatomic ,copy) NSString *ad_img;
@property (nonatomic ,strong) UIImage *adImg;
/** 人数限制 */
@property (nonatomic ,assign) NSInteger maxNum;
/** 活动模式,是否免费 */
@property (nonatomic ,assign) BOOL isFree;
/** 是否同步线上直播 */
@property (nonatomic ,assign) BOOL isSyncLive;
/** 直播类型 */
@property (nonatomic ,assign) BY_NEWLIVE_TYPE live_type;
/** 直播简介 */
@property (nonatomic ,copy) NSString *activity_intro;
/** 费用 */
@property (nonatomic ,assign) NSInteger amount;


+ (NSArray *)getNewActivityTableData;

+ (NSArray *)getActivityTableData:(BYActivityModel *)dataModel;

+ (BYCreateActivityModel *)getAmountCellData;
+ (BYCreateActivityModel *)getSyncLiveCellData;
@end

NS_ASSUME_NONNULL_END
