//
//  BYCreateActivityModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/23.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCreateActivityModel.h"
#import "BYEditLiveModel.h"

@implementation BYCreateActivityModel

+ (NSArray *)getNewActivityTableData{
    NSMutableArray *tableData = [NSMutableArray array];
    for (int i = 0; i < 6; i ++) {
        BYCreateActivityModel *model = [[BYCreateActivityModel alloc] init];
        [tableData addObject:model];
        switch (i) {
            case 0:
                model.cellString = @"BYCreateActivityModelCell";
                model.isFree = YES;
                model.cellHeight = 61;
                break;
            case 1:
                model.cellString = @"BYCreateActivitySyncLiveCell";
                model.isSyncLive = NO;
                model.cellHeight = 55;
                break;
            case 2:
                model.cellString = @"BYCreateActivityTitleCell";
                model.cellHeight = 130;
                break;
            case 3:
                model.cellString = @"BYCreateActivityTimeCell";
                model.cellHeight = 130;
                break;
            case 4:
                model.cellString = @"BYCreateActivityPeopleCell";
                model.cellHeight = 59;
                break;
            default:
                model.cellString = @"BYEditLiveAddCell";
                model.cellHeight = 59;
                break;
        }
    }
    
    return tableData;
}

+ (NSArray *)getActivityTableData:(BYActivityModel *)dataModel{
    NSMutableArray *tableData = [NSMutableArray array];
    BOOL isFree = dataModel.pay_fee > 0 ? NO : YES;
    BOOL isSyncLive = dataModel.activity_type == BY_NEWLIVE_TYPE_OTHER ? NO : YES;
    for (int i = 0; i < 6; i ++) {
        BYCreateActivityModel *model = [[BYCreateActivityModel alloc] init];
        [tableData addObject:model];
        switch (i) {
            case 0:
                model.cellString = @"BYCreateActivityModelCell";
                model.isFree = isFree;
                model.cellHeight = 61;
                break;
            case 1:
                model.cellString = @"BYCreateActivitySyncLiveCell";
                model.isSyncLive = isSyncLive;
                model.cellHeight = 55;
                break;
            case 2:
                model.cellString = @"BYCreateActivityTitleCell";
                model.activityName = dataModel.activity_title;
                model.activityLocation = dataModel.address;
                model.live_cover_url = dataModel.activity_cover_url;
                model.cellHeight = 130;
                break;
            case 3:
                model.cellString = @"BYCreateActivityTimeCell";
                model.begin_time = dataModel.begin_time;
                model.end_time = dataModel.end_time;
                model.ad_img = dataModel.ad_img;
                model.cellHeight = 130;
                break;
            case 4:
                model.cellString = @"BYCreateActivityPeopleCell";
                model.maxNum = dataModel.limit_number;
                model.cellHeight = 59;
                break;
            default:
                model.cellString = @"BYEditLiveAddCell";
                model.cellHeight = 59;
                break;
        }
    }
    
    if (!isFree) {
        BYCreateActivityModel *model = [[BYCreateActivityModel alloc] init];
        model.cellString = @"BYCreateAvtivityAmountCell";
        model.cellHeight = 55;
        model.amount = dataModel.pay_fee;
        [tableData insertObject:model atIndex:1];
    }
    
    if (isSyncLive) {
        NSInteger row = isFree && !isSyncLive ? 2 : (!isFree && isSyncLive ? 3 : 2);
        BYCreateActivityModel *model = [[BYCreateActivityModel alloc] init];
        model.cellString = @"BYCreateActivityLiveTypeCell";
        model.cellHeight = 55;
        model.live_type = dataModel.activity_type;
        [tableData insertObject:model atIndex:row];
    }
    
    if (dataModel.interviewee_list.count) {
        [dataModel.interviewee_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dic = [obj mj_keyValues];
            BYEditGuestInfoModel *model = [BYEditGuestInfoModel mj_objectWithKeyValues:dic];
            model.cellString = @"BYCreateActivityGuestCell";
            model.cellHeight = 130;
            model.isExist = 1;
            [tableData insertObject:model atIndex:tableData.count - 1];
        }];
    }
    
    return [tableData copy];
}

+ (BYCreateActivityModel *)getAmountCellData{
    BYCreateActivityModel *model = [[BYCreateActivityModel alloc] init];
    model.cellString = @"BYCreateAvtivityAmountCell";
    model.cellHeight = 55;
    model.amount = 0;
    return model;
}

+ (BYCreateActivityModel *)getSyncLiveCellData{
    BYCreateActivityModel *model = [[BYCreateActivityModel alloc] init];
    model.cellString = @"BYCreateActivityLiveTypeCell";
    model.cellHeight = 55;
    model.live_type = BY_NEWLIVE_TYPE_ILIVE;
    return model;
}

@end
