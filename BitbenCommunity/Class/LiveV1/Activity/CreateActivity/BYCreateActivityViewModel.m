//
//  BYCreateActivityViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/23.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCreateActivityViewModel.h"
#import "BYCreateActivityController.h"
#import "BYEditLiveIntroController.h"

#import "BYSelectTimePickerView.h"


#import "BYCreateActivityModel.h"
#import "BYEditLiveModel.h"


#define K_VC ((BYCreateActivityController *)S_VC)

@interface BYCreateActivityViewModel ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** isFree */
@property (nonatomic ,assign) BOOL isFree;
/** isSync */
@property (nonatomic ,assign) BOOL isSyncLive;
/** 时间选择器 */
@property (nonatomic ,strong) BYSelectTimePickerView *timePickerView;

@end

@implementation BYCreateActivityViewModel

- (void)setContentView{
    self.isFree = YES;
    self.isSyncLive = NO;
    
    [self addTableView];
    [self addConfirmBtn];
}

#pragma mark - action
- (void)confirmBtnAction{
    if ([self verifyData]) {
        BYEditLiveIntroController *editLiveIntroController = [[BYEditLiveIntroController alloc] init];
        editLiveIntroController.isEdit = K_VC.isEdit;
//        editLiveIntroController.model = K_VC.model;
        editLiveIntroController.tableData = self.tableView.tableData;
        editLiveIntroController.isActivity = YES;
        editLiveIntroController.activityModel = K_VC.model;
        [S_V_NC pushViewController:editLiveIntroController animated:YES];
    }
}

- (BOOL)verifyData{
    // 校验付费金额
    if (!self.isFree) {
        BYCreateActivityModel *model = self.tableView.tableData[1];
        NSInteger amount = model.amount;
        K_VC.model.pay_fee = model.amount;
        if (amount < 20 || amount > 3000) {
            showToastView(@"请输入正确的金额范围", kCommonWindow);
            return NO;
        }
    }
    // 校验活动内容
    NSInteger row = self.isFree && !self.isSyncLive ? 2 : (!self.isFree && self.isSyncLive ? 4 : 3);
    BYCreateActivityModel *locationModel = self.tableView.tableData[row];
    K_VC.model.address = locationModel.activityLocation;
    K_VC.model.activity_title = locationModel.activityName;
    if (!locationModel.coverImg && !locationModel.live_cover_url) {
        [S_VC showToastView:@"请上传封面"];
        return NO;
    }else if (!locationModel.activityName.length) {
        [S_VC showToastView:@"请填写会议名称"];
        return NO;
    }else if (!locationModel.activityLocation.length) {
        [S_VC showToastView:@"请填写会议地点"];
        return NO;
    }
    
    // 校验时间内容
    BYCreateActivityModel *timeModel = self.tableView.tableData[row + 1];
//    if (!timeModel.adImg && !timeModel.ad_img) {
//        [S_VC showToastView:@"请上传海报"];
//        return NO;
//    }else
    if (!timeModel.begin_time.length) {
        [S_VC showToastView:@"请填写开始时间"];
        return NO;
    }else if (!timeModel.end_time.length) {
        [S_VC showToastView:@"请填写结束时间"];
        return NO;
    }
    
    return [self verifyGuest];
}

- (BOOL)verifyGuest{
    __block BOOL result = YES;
    [self.tableView.tableData enumerateObjectsUsingBlock:^(BYCommonModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([model isKindOfClass:[BYEditGuestInfoModel class]]) {
            BYEditGuestInfoModel *tmpModel = (BYEditGuestInfoModel *)model;
            
            if (!tmpModel.headImg && !tmpModel.head_img) {
                [S_VC showToastView:@"请完善嘉宾头像"];
                result = NO;
                *stop = YES;
            }

            if (!tmpModel.intro.length || !tmpModel.nickname.length) {
                [S_VC showToastView:@"请完善嘉宾信息"];
                result = NO;
                *stop = YES;
            }
        }
    }];
    return result;
}

- (void)selectImage:(NSIndexPath *)indexPath{
    BYCommonModel *model = self.tableView.tableData[indexPath.row];
    UIImage *image;
    BOOL canCropper = NO;
    if ([model.cellString isEqualToString:@"BYCreateActivityTitleCell"]) {
        canCropper = YES;
        if (((BYCreateActivityModel *)model).coverImg) {
            image = ((BYCreateActivityModel *)model).coverImg;
        }
    }else if ([model.cellString isEqualToString:@"BYCreateActivityTimeCell"]) {
        if (((BYCreateActivityModel *)model).adImg) {
            image = ((BYCreateActivityModel *)model).adImg;
        }
    }else {
        if (((BYEditGuestInfoModel *)model).headImg) {
            image = ((BYEditGuestInfoModel *)model).headImg;
        }
    }
    if (image) {
        UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *albumAction = [UIAlertAction actionWithTitle:@"从手机相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @weakify(self);
            [self assetsLibrarySelImgCanCropper:canCropper cb:^(UIImage *image) {
                @strongify(self);
                [self setImageAtIndexPath:indexPath image:image];
            }];
        }];
        
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [self setImageAtIndexPath:indexPath image:nil];
        }];
        
        [sheetController addAction:albumAction];
        [sheetController addAction:confirmAction];
        [sheetController addAction:cancelAction];
        [S_V_NC presentViewController:sheetController animated:YES completion:nil];
    }else{
        @weakify(self);
        [self assetsLibrarySelImgCanCropper:canCropper cb:^(UIImage *image) {
            @strongify(self);
            [self setImageAtIndexPath:indexPath image:image];
        }];
    }
}

- (void)setImageAtIndexPath:(NSIndexPath *)indexPath image:(UIImage *)image{
    BYCommonModel *model = self.tableView.tableData[indexPath.row];
    if ([model.cellString isEqualToString:@"BYCreateActivityTitleCell"]) {
        ((BYCreateActivityModel *)model).coverImg = image;
        ((BYCreateActivityModel *)model).live_cover_url = @"";
    }else if ([model.cellString isEqualToString:@"BYCreateActivityTimeCell"]) {
        ((BYCreateActivityModel *)model).adImg = image;
        ((BYCreateActivityModel *)model).ad_img = @"";
    }else {
        ((BYEditGuestInfoModel *)model).headImg = image;
        ((BYEditGuestInfoModel *)model).head_img = @"";
    }
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)assetsLibrarySelImgCanCropper:(BOOL)canCropper cb:(void(^)(UIImage *image))cb{
    CGFloat cropperH = kCommonScreenWidth*180/345;
    CGRect cropperRect = CGRectMake(0, (kCommonScreenHeight - cropperH)/2, kCommonScreenWidth, cropperH);
    GWAssetsLibraryViewController *assetVC = [[GWAssetsLibraryViewController alloc] init];
    assetVC.isCanCut = canCropper;
    assetVC.cropFrame = cropperRect;
    [assetVC selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
        if (cb) {
            cb(selectedImgArr[0]);
        }
    }];
    [S_V_NC pushViewController:assetVC animated:YES];
    
}

- (void)addAmountCell:(BOOL)isAdd{
    if (isAdd) {
        [self.tableView beginUpdates];
        BYCreateActivityModel *model = [BYCreateActivityModel getAmountCellData];
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
        [array insertObject:model atIndex:1];
        self.tableView.tableData = [array copy];
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
        [self.tableView endUpdates];
    } else {
        [self.tableView beginUpdates];
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
        [array removeObjectAtIndex:1];
        self.tableView.tableData = [array copy];
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });

}

- (void)addLiveTypeCell:(BOOL)isAdd{
    NSInteger row = self.isFree ? 2 : 3;
    if (isAdd) {
        K_VC.model.activity_type = BY_NEWLIVE_TYPE_ILIVE;
        [self.tableView beginUpdates];
        BYCreateActivityModel *model = [BYCreateActivityModel getSyncLiveCellData];
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
        [array insertObject:model atIndex:row];
        self.tableView.tableData = [array copy];
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
        [self.tableView endUpdates];
    }else {
        K_VC.model.activity_type = BY_NEWLIVE_TYPE_OTHER;
        [self.tableView beginUpdates];
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
        [array removeObjectAtIndex:row];
        self.tableView.tableData = [array copy];
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });

}

- (void)delGuestData:(NSIndexPath *)indexPath{
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:@"删除提醒" message:@"确定要删除嘉宾吗？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.tableView beginUpdates];
        self.tableView.notAutoReload = YES;
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
        [array removeObjectAtIndex:indexPath.row];
        self.tableView.tableData = [array copy];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
        [self.tableView endUpdates];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
    
    [sheetController addAction:confirmAction];
    [sheetController addAction:cancelAction];
    [S_V_NC presentViewController:sheetController animated:YES completion:nil];
}

#pragma mark - BYCommonTableViewDelegate

- (void)by_tableViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.dragging) {
        [S_V_VIEW endEditing:YES];
    }
}

- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     if (indexPath.row == tableView.tableData.count - 1) { // 添加嘉宾
        if (tableView.tableData.count >= 26) {
            showToastView(@"嘉宾数达到上限", S_V_VIEW);
            return;
        }
       
        [self.tableView beginUpdates];
        tableView.notAutoReload = YES;
        BYEditGuestInfoModel *guestModel = [BYEditLiveModel getSingleGuestCreateActivityCellData];
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
        [array insertObject:guestModel atIndex:array.count - 1];
        self.tableView.tableData = array;
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:array.count - 2 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
        [self.tableView endUpdates];
        
        [self.tableView scrollToBottom:YES];
    }
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"switchSyncLiveAction"]) {  // 同步线上直播
        BYCreateActivityModel *model = tableView.tableData[indexPath.row];
        self.isSyncLive = model.isSyncLive;
        [self addLiveTypeCell:self.isSyncLive];
    } else if ([actionName isEqualToString:@"activityModel"]) { // 付费模式
        BYCreateActivityModel *model = tableView.tableData[indexPath.row];
        self.isFree = model.isFree;
        [self addAmountCell:!self.isFree];
    } else if ([actionName isEqualToString:@"liveTypeSelAction"]) { // 选择直播类型
        BYCreateActivityModel *model = tableView.tableData[indexPath.row];
        K_VC.model.activity_type = model.live_type;
    } else if ([actionName isEqualToString:@"selBeginTime"]) { // 选择开始时间
        BYCreateActivityModel *model = tableView.tableData[indexPath.row];
        [self.timePickerView showAnimation];
        @weakify(self);
        _timePickerView.didSelectTimeString = ^(NSString *data,NSString *showTimeData) {
            @strongify(self);
            model.begin_time = showTimeData;
            K_VC.model.begin_time = data;
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        };
        [S_V_VIEW endEditing:YES];
    } else if ([actionName isEqualToString:@"selEndTime"]) { // 选择结束时间
        BYCreateActivityModel *model = tableView.tableData[indexPath.row];
        [self.timePickerView showAnimation];
        @weakify(self);
        _timePickerView.didSelectTimeString = ^(NSString *data,NSString *showTimeData) {
            @strongify(self);
            model.end_time = showTimeData;
            K_VC.model.end_time = data;
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        };
        [S_V_VIEW endEditing:YES];
    } else if ([actionName isEqualToString:@"selImgAction"]) { // 选择封面
        [self selectImage:indexPath];
        [S_V_VIEW endEditing:YES];
    } else if ([actionName isEqualToString:@"delCellAction"]) { // 删除嘉宾
        [self delGuestData:indexPath];
    }
}

#pragma mark - configUI

- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.backgroundColor = kColorRGBValue(0xf2f4f5);
    self.tableView.group_delegate = self;
    [S_V_VIEW addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kSafe_Mas_Bottom(49), 0));
    }];
    
    UIButton *confimBtn = [UIButton by_buttonWithCustomType];
    [confimBtn setBy_attributedTitle:@{@"title":@"完成",
                                       NSFontAttributeName:[UIFont systemFontOfSize:15],
                                       NSForegroundColorAttributeName:[UIColor whiteColor]
                                       } forState:UIControlStateNormal];
    [confimBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    [S_V_VIEW addSubview:confimBtn];
    if (!K_VC.isEdit) {
        self.tableView.tableData = [BYCreateActivityModel getNewActivityTableData];
    }else{
        self.isFree = K_VC.model.pay_fee > 0 ? NO : YES;
        self.isSyncLive = K_VC.model.activity_type != BY_NEWLIVE_TYPE_OTHER ? YES : NO;
        self.tableView.tableData = [BYCreateActivityModel getActivityTableData:K_VC.model];
    }
}

- (void)addConfirmBtn{
    UIButton *confirmBtn = [UIButton by_buttonWithCustomType];
    [confirmBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    [confirmBtn setBy_attributedTitle:@{@"title":@"下一步",
                                        NSFontAttributeName:[UIFont systemFontOfSize:15],
                                        NSForegroundColorAttributeName:kColorRGBValue(0xfefefe)
                                        } forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(confirmBtnAction) forControlEvents:UIControlEventTouchUpInside];
    confirmBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, kSafeAreaInsetsBottom, 0);
    [S_V_VIEW addSubview:confirmBtn];
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(49));
    }];
}

- (BYSelectTimePickerView *)timePickerView{
    if (!_timePickerView) {
        _timePickerView = [[BYSelectTimePickerView alloc] initWithFathureView:kCommonWindow];
    
    }
    return _timePickerView;
}


@end
