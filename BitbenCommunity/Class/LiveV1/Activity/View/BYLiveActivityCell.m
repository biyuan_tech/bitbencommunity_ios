//
//  BYLiveActivityCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveActivityCell.h"
#import <NSAttributedString+YYText.h>


@interface BYLiveActivityCell ()




@end

@implementation BYLiveActivityCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYActivityModel *model = object[indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.activity_title)];
    messageAttributed.yy_lineSpacing = 6.0f;
    messageAttributed.yy_font = [UIFont boldSystemFontOfSize:15];
    messageAttributed.yy_color = kColorRGBValue(0x323232);
    YYTextContainer *textContainer = [YYTextContainer new];
    textContainer.size = CGSizeMake(kCommonScreenWidth - 157, 42);
    YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
    self.titleLab.attributedText = messageAttributed;
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(ceil(layout.textBoundingSize.height));
    }];
    
    [self.coverImgView uploadHDImageWithURL:model.activity_cover_url callback:nil];
    
    [self verifyStatus];
    
    self.timeLab.text = model.begin_time;
    
    self.locationLab.text = model.address;
    
    self.costLab.text = model.pay_fee > 0 ? [NSString stringWithFormat:@"¥ %i",(int)model.pay_fee] : @"限时免费";
    [self.costLab sizeToFit];
}

- (void)verifyStatus{
    switch (self.model.status) {
        case BY_ACTIVITY_STATUS_OVERTIME:
            self.statusLab.text = @"已超时";
            self.statusLab.backgroundColor = kColorRGBValue(0xaaaaaa);
            break;
        case BY_ACTIVITY_STATUS_REVIEW_FAIL:
            self.statusLab.text = @"被驳回";
            self.statusLab.backgroundColor = kColorRGBValue(0xeb0f0f);
            break;
        case BY_ACTIVITY_STATUS_REVIEW_PASS:
            self.statusLab.text = @"报名中";
            self.statusLab.backgroundColor = kColorRGBValue(0xffb400);
            break;
        case BY_ACTIVITY_STATUS_REVIEW_WAITING:
            self.statusLab.text = @"待审核";
            self.statusLab.backgroundColor = kColorRGBValue(0x1a86ff);
            break;
        default:
            self.statusLab.text = @"已结束";
            self.statusLab.backgroundColor = kColorRGBValue(0xaaaaaa);
            break;
    }
}

- (void)setContentView{
    
    [self.contentView addSubview:self.coverImgView];
    [self.contentView addSubview:self.statusLab];
    [self.contentView addSubview:self.titleLab];
    [self.contentView addSubview:self.timeIcon];
    [self.contentView addSubview:self.timeLab];
    [self.contentView addSubview:self.locationIcon];
    [self.contentView addSubview:self.locationLab];
    [self.contentView addSubview:self.costLab];
    
    [self.coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(15);
        make.width.mas_equalTo(115);
        make.height.mas_equalTo(90);
    }];
    
    [self.statusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(22);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(19);
    }];
    
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.coverImgView.mas_right).offset(12);
        make.top.mas_equalTo(self.coverImgView);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    [self.timeIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(13);
        make.left.mas_equalTo(self.coverImgView.mas_right).offset(12);
        make.top.mas_equalTo(self.titleLab.mas_bottom).offset(13);
    }];
    
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.timeIcon.mas_right).offset(6);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(self.timeLab.font.pointSize);
        make.centerY.mas_equalTo(self.timeIcon);
    }];
    
    [self.locationIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(13);
        make.height.mas_equalTo(15);
        make.left.mas_equalTo(self.coverImgView.mas_right).offset(12);
        make.top.mas_equalTo(self.timeIcon.mas_bottom).offset(11);
    }];
    
    [self.locationLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.locationIcon.mas_right).offset(7);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(self.locationLab.font.pointSize);
        make.centerY.mas_equalTo(self.locationIcon);
    }];
    
    [self.costLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.coverImgView.mas_right).offset(12);
        make.top.mas_equalTo(self.locationIcon.mas_bottom).offset(11);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(self.costLab.font.pointSize);
    }];
    
    UIView *bottomLineView = [UIView by_init];
    [bottomLineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:bottomLineView];
    [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
}

- (PDImageView *)coverImgView{
    if (!_coverImgView) {
        _coverImgView = [[PDImageView alloc] init];
        _coverImgView.layer.cornerRadius = 4.0;
        _coverImgView.clipsToBounds = YES;
        _coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _coverImgView;
}

- (YYLabel *)statusLab{
    if (!_statusLab) {
        _statusLab = [[YYLabel alloc] init];
        _statusLab.font = [UIFont systemFontOfSize:11];
        _statusLab.textColor = [UIColor whiteColor];
        _statusLab.textAlignment = NSTextAlignmentCenter;
        _statusLab.layer.cornerRadius = 2.0f;
    }
    return _statusLab;
}

- (YYLabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [[YYLabel alloc] init];
        _titleLab.font = [UIFont boldSystemFontOfSize:15];
        _titleLab.textColor = kColorRGBValue(0x323232);
    }
    return _titleLab;
}

- (UILabel *)timeLab{
    if (!_timeLab) {
        _timeLab = [UILabel by_init];
        _timeLab.font = [UIFont systemFontOfSize:12];
        _timeLab.textColor = kColorRGBValue(0x828282);
    }
    return _timeLab;
}

- (UIImageView *)timeIcon{
    if (!_timeIcon) {
        _timeIcon = [[UIImageView alloc] init];
        [_timeIcon by_setImageName:@"common_time_black"];
    }
    return _timeIcon;
}

- (UILabel *)locationLab{
    if (!_locationLab) {
        _locationLab = [UILabel by_init];
        _locationLab.font = [UIFont systemFontOfSize:13];
        _locationLab.textColor = kColorRGBValue(0x828282);
    }
    return _locationLab;
}

- (UIImageView *)locationIcon{
    if (!_locationIcon) {
        _locationIcon = [UIImageView by_init];
        [_locationIcon by_setImageName:@"common_location"];
    }
    return _locationIcon;
}

- (UILabel *)costLab{
    if (!_costLab) {
        _costLab = [UILabel by_init];
        _costLab.font = [UIFont systemFontOfSize:13];
        _costLab.textColor = kColorRGBValue(0xeb0f0f);
    }
    return _costLab;
}
@end
