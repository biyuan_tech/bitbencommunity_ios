//
//  BYLiveActivityCell.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonTableViewCell.h"
#import <YYLabel.h>
#import "BYActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYLiveActivityCell : BYCommonTableViewCell

/** 封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 状态 */
@property (nonatomic ,strong) YYLabel *statusLab;
/** title */
@property (nonatomic ,strong) YYLabel *titleLab;
/** 开始时间 */
@property (nonatomic ,strong) UILabel *timeLab;
/** timeIcon */
@property (nonatomic ,strong) UIImageView *timeIcon;
/** 地点 */
@property (nonatomic ,strong) UILabel *locationLab;
/** 地点Icon */
@property (nonatomic ,strong) UIImageView *locationIcon;
/** 费用 */
@property (nonatomic ,strong) UILabel *costLab;
/** model */
@property (nonatomic ,strong) BYActivityModel *model;


- (void)setContentView;
@end

NS_ASSUME_NONNULL_END
