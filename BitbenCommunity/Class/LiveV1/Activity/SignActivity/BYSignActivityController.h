//
//  BYSignActivityController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYSignActivityController : BYCommonViewController

/** 活动id */
@property (nonatomic ,copy) NSString *activity_id;


@end

NS_ASSUME_NONNULL_END
