//
//  BYSignActivityCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYSignActivityCell.h"
#import "BYSignActivityModel.h"

@interface BYSignActivityCell ()<UITextFieldDelegate>
/** titleLab */
@property (nonatomic ,strong) UILabel *titleLab;

/** textField */
@property (nonatomic ,strong) UITextField *textField;
/** model */
@property (nonatomic ,strong) BYSignActivityModel *model;

@end

@implementation BYSignActivityCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYSignActivityModel *model = object[indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    if (model.content.length) {
        self.textField.text = model.content;
    }
    
    self.titleLab.text = model.title;
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:model.placeholder attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kColorRGBValue(0xc8c8c8)}];
    self.textField.attributedPlaceholder = attributedString;
    
    self.textField.keyboardType = model.keyboardType;
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (self.indexPath.row == 1) {
        return [Tool validateOnlyNumbers:string];
    }else if (self.indexPath.row == 2) {
        return YES;
    }else if (self.indexPath.row == 3) {
        return YES;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.model.content = textField.text;
}

#pragma mark - configUI

- (void)setContentView{
    
    UIImageView *mustNameImgView = [UIImageView by_init];
    [mustNameImgView by_setImageName:@"newlive_must"];
    [self.contentView addSubview:mustNameImgView];
    [mustNameImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(32);
        make.top.mas_equalTo(25);
        make.width.height.mas_equalTo(6);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    titleLab.font = [UIFont boldSystemFontOfSize:18];
    titleLab.text = @"微信号";
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mustNameImgView.mas_right).offset(10);
        make.centerY.mas_equalTo(mustNameImgView);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.width.mas_equalTo(70);
    }];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth - 60);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
        make.centerX.mas_equalTo(0);
    }];
    
    UITextField *textField = [UITextField by_init];
    NSString *placeholder = @"请填写正确的微信号";
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kColorRGBValue(0xc8c8c8)}];
    textField.textColor = kColorRGBValue(0x323232);
    textField.font = [UIFont systemFontOfSize:15];
    textField.attributedPlaceholder = attributedString;
    textField.delegate = self;
    [self.contentView addSubview:textField];
    self.textField = textField;
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(titleLab);
        make.top.mas_equalTo(titleLab.mas_bottom);
        make.bottom.mas_equalTo(lineView.mas_top);
        make.right.mas_equalTo(-30);
    }];
}

@end
