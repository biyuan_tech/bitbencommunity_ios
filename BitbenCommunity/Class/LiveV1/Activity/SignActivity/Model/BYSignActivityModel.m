//
//  BYSignActivityModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYSignActivityModel.h"

@implementation BYSignActivityModel

+ (NSArray *)getTableViewData{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < 4; i ++) {
        BYSignActivityModel *model = [[BYSignActivityModel alloc] init];
        model.cellString = @"BYSignActivityCell";
        model.cellHeight = 83;
        switch (i) {
            case 0:
                model.title = @"姓名";
                model.placeholder = @"请输入姓名";
                model.keyboardType = UIKeyboardTypeDefault;
                break;
            case 1:
                model.title = @"手机号";
                model.placeholder = @"请输入手机号";
                model.keyboardType = UIKeyboardTypePhonePad;
                break;
            case 2:
                model.title = @"微信号";
                model.placeholder = @"请输入微信号";
                model.keyboardType = UIKeyboardTypeNamePhonePad;
                break;
            default:
                model.title = @"邮箱";
                model.placeholder = @"请输入邮箱";
                model.keyboardType = UIKeyboardTypeEmailAddress;
                break;
        }
        [array addObject:model];
    }
    return [array copy];
}

@end
