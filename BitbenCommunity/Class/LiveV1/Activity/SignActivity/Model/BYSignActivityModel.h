//
//  BYSignActivityModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYSignActivityModel : BYCommonModel

/** placeholder */
@property (nonatomic ,copy) NSString *placeholder;
/** KeyboardType */
@property (nonatomic ,assign) UIKeyboardType keyboardType;
/** content */
@property (nonatomic ,copy) NSString *content;


+ (NSArray *)getTableViewData;

@end

NS_ASSUME_NONNULL_END
