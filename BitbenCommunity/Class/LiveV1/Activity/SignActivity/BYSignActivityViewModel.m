//
//  BYSignActivityViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYSignActivityViewModel.h"
#import "BYSignActivityController.h"
#import "BYSignActivityModel.h"
#import "BYSignActivitySucController.h"

#define K_VC ((BYSignActivityController *)S_VC)
@interface BYSignActivityViewModel ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;


@end

@implementation BYSignActivityViewModel

- (void)setContentView{
    
    [S_V_VIEW addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kSafe_Mas_Bottom(79), 0));
    }];
    
    [self addFooterView];
}

#pragma mark - action
- (void)signBtnAction{
    
//    BYSignActivitySucController *sucController = [[BYSignActivitySucController alloc] init];
//    sucController.wechat = @"siifaisdasd";
//    sucController.wechat_img = @"image-2019-12-26-14:21:59-2-949342896";
//    [S_V_NC pushViewController:sucController animated:YES];
    
//    return;
    __block NSString *name;
    __block NSString *phone;
    __block NSString *wechat;
    __block NSString *email;
    [self.tableView.tableData enumerateObjectsUsingBlock:^(BYSignActivityModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        switch (idx) {
            case 0:
                name = obj.content;
                break;
            case 1:
                phone = obj.content;
                break;
            case 2:
                wechat = obj.content;
                break;
            default:
                email = obj.content;
                break;
        }
    }];
    
    if (!name.length) {
        showToastView(@"请填写姓名", S_V_VIEW);
        return;
    }
    else if (!phone.length) {
        showToastView(@"请填写手机号", S_V_VIEW);
        return;
    }
    else if (!wechat.length) {
        showToastView(@"请填写微信号", S_V_VIEW);
        return;
    }
    else if (!email.length) {
        showToastView(@"请填写邮箱", S_V_VIEW);
        return;
    }
    if (![Tool isValidateEmail:email]) {
        showToastView(@"请检查邮箱格式", S_V_VIEW);
        return;
    }
    if (![Tool validateMobile:phone]) {
        showToastView(@"请检查手机格式", S_V_VIEW);
        return;
    }
    [self loadRequest:name phone:phone wechat:wechat email:email];
}

- (void)by_tableViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.dragging) {
        [S_V_VIEW endEditing:YES];
    }
}

#pragma mark - request
- (void)loadRequest:(NSString *)name phone:(NSString *)phone wechat:(NSString *)wechat email:(NSString *)email{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreateActivityEnroll:K_VC.activity_id name:name phone:phone wechat:wechat email:email successBlock:^(id object) {
        @strongify(self);
        
        BYSignActivitySucController *sucController = [[BYSignActivitySucController alloc] init];
        sucController.wechat = nullToEmpty(object[@"user_wechat"]);
        sucController.wechat_img = nullToEmpty(object[@"user_wechat_pic"]); //image-2019-12-26-14:21:59-2-949342896
        [S_V_NC pushViewController:sucController animated:YES];
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"报名失败", S_V_VIEW);
    }];
}

#pragma mark - configUI

- (void)addFooterView{
    UIView *footerView = [UIView by_init];
    footerView.backgroundColor = [UIColor whiteColor];
    [S_V_VIEW addSubview:footerView];
    [footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(79));
    }];
    
    UIButton *signBtn = [UIButton by_buttonWithCustomType];
    signBtn.backgroundColor = kColorRGBValue(0xea6438);
    [signBtn setBy_attributedTitle:@{@"title":@"立即报名",
                                     NSFontAttributeName:[UIFont systemFontOfSize:15],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]
                                     } forState:UIControlStateNormal];
    [signBtn addTarget:self action:@selector(signBtnAction) forControlEvents:UIControlEventTouchUpInside];
    signBtn.layer.cornerRadius = 6.0;
    [footerView addSubview:signBtn];
    [signBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(10, 30, kSafe_Mas_Bottom(25), 30));
    }];
}

- (UIView *)getTableViewHeaderView{
    UIView *headerView = [UIView by_init];
    headerView.frame = CGRectMake(0, 0, kCommonScreenWidth, 103);
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel *titleLab = [UILabel by_init];
    titleLab.font = [UIFont boldSystemFontOfSize:25];
    titleLab.textColor = kColorRGBValue(0x323232);
    titleLab.text = @"报名信息填写";
    [headerView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(32);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.width.mas_equalTo(170);
    }];
    
    UILabel *subTitleLab = [UILabel by_init];
    subTitleLab.font = [UIFont systemFontOfSize:14];
    subTitleLab.textColor = kColorRGBValue(0xc7c7c7);
    subTitleLab.text = @"请务必填写真实准确信息，否则报名将视为无效";
    subTitleLab.numberOfLines = 2.0;
    [headerView addSubview:subTitleLab];
    CGSize size = [subTitleLab.text getStringSizeWithFont:subTitleLab.font maxWidth:kCommonScreenWidth - 60];
    [subTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(70);
        make.width.mas_equalTo(ceil(size.width));
        make.height.mas_equalTo(ceil(size.height));
    }];
    
    return headerView;
}

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        _tableView.tableHeaderView = [self getTableViewHeaderView];
        _tableView.tableData = [BYSignActivityModel getTableViewData];
    }
    return _tableView;
}
@end
