//
//  BYActivityTicketController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/28.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"
#import "BYActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYActivityTicketController : BYCommonViewController


/** model */
@property (nonatomic ,strong) BYActivityModel *model;


@end

NS_ASSUME_NONNULL_END
