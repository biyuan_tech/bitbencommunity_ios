//
//  BYActivityTicketController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/28.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityTicketController.h"

@interface BYActivityTicketController ()

/** scrollView */
@property (nonatomic ,strong) UIScrollView *scrollView;
/** lineView */
@property (nonatomic ,strong) UIView *lineView;

@end

@implementation BYActivityTicketController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"入场券";
    [self setContentView];
}

- (void)locationBtnAction {
    [BYCommonTool openMapWithLocation:self.model.address];
}

#pragma mark - configUI

- (void)setContentView{
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    
    UIImageView *ticketTopImgView = [[UIImageView alloc] init];
    [ticketTopImgView by_setImageName:@"activity_ticket_top"];
    [self.scrollView addSubview:ticketTopImgView];
    [ticketTopImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(ticketTopImgView.image.size.width);
        make.height.mas_equalTo(ticketTopImgView.image.size.height);
        make.top.mas_equalTo(49);
    }];
    
    UILabel *ticketLab = [UILabel by_init];
    ticketLab.text = @"入场券";
    ticketLab.font = [UIFont systemFontOfSize:20];
    ticketLab.textColor = kColorRGBValue(0xea6438);
    [self.scrollView addSubview:ticketLab];
    [ticketLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(ticketLab.text, 20));
        make.height.mas_equalTo(stringGetHeight(ticketLab.text, 20));
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(ticketTopImgView.mas_bottom);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    titleLab.textAlignment = NSTextAlignmentCenter;
    [self.scrollView addSubview:titleLab];
    NSMutableParagraphStyle *titleParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    titleParagraphStyle.lineSpacing = 8.0; // 调整行间距
    NSDictionary *titleAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:20],
                                      NSForegroundColorAttributeName:kColorRGBValue(0x313131),
                                      NSParagraphStyleAttributeName:titleParagraphStyle
                                      };
    CGSize titleSize = [self.model.activity_title getStringSizeWithAttributes:titleAttributes maxSize:CGSizeMake(kCommonScreenWidth - 38*2, MAXFLOAT)];
    NSAttributedString *titleAttributedString = [[NSAttributedString alloc] initWithString:self.model.activity_title attributes:titleAttributes];
    titleLab.attributedText = titleAttributedString;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(120);
        make.width.mas_equalTo(ceil(titleSize.width));
        make.height.mas_equalTo(ceil(titleSize.height));
    }];
    
    UILabel *amountLab = [UILabel by_init];
    amountLab.font = [UIFont boldSystemFontOfSize:13];
    amountLab.textColor = kColorRGBValue(0x323232);
    [self.scrollView addSubview:amountLab];
    if (self.model.pay_fee > 0) {
        NSString *string = [NSString stringWithFormat:@"票种: ¥ %i",(int)self.model.pay_fee];
        NSString *tagKey = [NSString stringWithFormat:@"¥ %i",(int)self.model.pay_fee];
        NSAttributedString *amountAttributedString = [string tagKeyWordsStr:tagKey attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:22],NSForegroundColorAttributeName:kColorRGBValue(0xeb0f0f)}];
        amountLab.attributedText = amountAttributedString;
        CGSize size = [string getStringSizeWithAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:22],NSForegroundColorAttributeName:kColorRGBValue(0xeb0f0f)} maxSize:CGSizeMake(200, MAXFLOAT)];
        [amountLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(titleLab.mas_bottom).offset(60);
            make.left.mas_equalTo(35);
            make.width.mas_equalTo(ceil(size.width));
            make.height.mas_equalTo(ceil(size.height));
        }];

    }else{
        NSString *string = @"票种: 免费";
        NSString *tagKey = @"免费";
        NSAttributedString *amountAttributedString = [string tagKeyWordsStr:tagKey attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:13],NSForegroundColorAttributeName:kColorRGBValue(0xeb0f0f)}];
        amountLab.attributedText = amountAttributedString;
        CGSize size = [string getStringSizeWithAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:13],NSForegroundColorAttributeName:kColorRGBValue(0xeb0f0f)} maxSize:CGSizeMake(200, MAXFLOAT)];
        [amountLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(titleLab.mas_bottom).offset(60);
            make.left.mas_equalTo(35);
            make.width.mas_equalTo(ceil(size.width));
            make.height.mas_equalTo(ceil(size.height));
        }];
    }
    
    UILabel *nameTagLab = [UILabel by_init];
    nameTagLab.font = [UIFont boldSystemFontOfSize:13];
    nameTagLab.textColor = kColorRGBValue(0x323232);
    nameTagLab.text = @"姓名:";
    [self.scrollView addSubview:nameTagLab];
    CGSize tagSize = [nameTagLab.text getStringSizeWithFont:nameTagLab.font maxWidth:120];
    [nameTagLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(35);
        make.top.mas_equalTo(amountLab.mas_bottom).offset(15);
        make.height.mas_equalTo(tagSize.height);
        make.width.mas_equalTo(ceil(tagSize.width));
    }];
    
    UILabel *nameLab = [UILabel by_init];
    nameLab.numberOfLines = 0;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3.0; // 调整行间距
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                 NSForegroundColorAttributeName:kColorRGBValue(0x323232),
                                 NSParagraphStyleAttributeName:paragraphStyle
                                 };
    CGFloat maxLeftWidth = kCommonScreenWidth/2 - 35 - tagSize.width - 5 - 5;
    CGFloat maxRightWidth = kCommonScreenWidth/2 - 35 - tagSize.width - 5;
    
    CGSize nameSize = [self.model.enrollModel.name getStringSizeWithAttributes:attributes maxSize:CGSizeMake(maxLeftWidth, MAXFLOAT)];
    NSAttributedString *nameAttributed = [[NSAttributedString alloc] initWithString:self.model.enrollModel.name attributes:attributes];
    nameLab.attributedText = nameAttributed;
    
    [self.scrollView addSubview:nameLab];
    [nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameTagLab.mas_right).offset(5);
        make.top.mas_equalTo(nameTagLab);
        make.height.mas_equalTo(nameSize.height);
        make.width.mas_equalTo(nameSize.width);
    }];
    
    
    UILabel *wxIdTagLab = [UILabel by_init];
    wxIdTagLab.font = [UIFont boldSystemFontOfSize:13];
    wxIdTagLab.textColor = kColorRGBValue(0x323232);
    wxIdTagLab.text = @"微信:";
    [self.scrollView addSubview:wxIdTagLab];
    [wxIdTagLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCommonScreenWidth/2);
        make.top.mas_equalTo(nameTagLab);
        make.height.mas_equalTo(wxIdTagLab.font.pointSize);
        make.width.mas_equalTo(ceil(tagSize.width));
    }];
    
    UILabel *wxIdLab = [UILabel by_init];
    wxIdLab.numberOfLines = 0;
    CGSize wechatSize = [self.model.enrollModel.wechat getStringSizeWithAttributes:attributes maxSize:CGSizeMake(maxRightWidth, MAXFLOAT)];
    NSAttributedString *wechatAttributed = [[NSAttributedString alloc] initWithString:self.model.enrollModel.wechat attributes:attributes];
    wxIdLab.attributedText = wechatAttributed;
    [self.scrollView addSubview:wxIdLab];
    [wxIdLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wxIdTagLab.mas_right).offset(5);
        make.top.mas_equalTo(wxIdTagLab);
        make.height.mas_equalTo(ceil(wechatSize.height));
        make.width.mas_equalTo(wechatSize.width);
    }];
    
    CGFloat topH = nameSize.height <= wechatSize.height ? wechatSize.height + 12 : nameSize.height + 12;

    UILabel *telPhoneTagLab = [UILabel by_init];
    telPhoneTagLab.font = [UIFont boldSystemFontOfSize:13];
    telPhoneTagLab.textColor = kColorRGBValue(0x323232);
    telPhoneTagLab.text = @"电话:";
    [self.scrollView addSubview:telPhoneTagLab];
    [telPhoneTagLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(35);
        make.top.mas_equalTo(nameTagLab.mas_top).offset(topH);
        make.height.mas_equalTo(telPhoneTagLab.font.pointSize);
        make.width.mas_equalTo(ceil(tagSize.width));
    }];
    
    UILabel *telPhoneLab = [UILabel by_init];
    telPhoneLab.numberOfLines = 0;
    CGSize phoneSize = [self.model.enrollModel.phone getStringSizeWithAttributes:attributes maxSize:CGSizeMake(maxLeftWidth, MAXFLOAT)];
    NSAttributedString *phoneAttributed = [[NSAttributedString alloc] initWithString:self.model.enrollModel.phone attributes:attributes];
    telPhoneLab.attributedText = phoneAttributed;
    [self.scrollView addSubview:telPhoneLab];
    [telPhoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(telPhoneTagLab.mas_right).offset(5);
        make.top.mas_equalTo(telPhoneTagLab);
        make.height.mas_equalTo(phoneSize.height);
        make.width.mas_equalTo(phoneSize.width);
    }];
    
    UILabel *emailTagLab = [UILabel by_init];
    emailTagLab.font = [UIFont boldSystemFontOfSize:13];
    emailTagLab.textColor = kColorRGBValue(0x323232);
    emailTagLab.text = @"邮箱:";
    [self.scrollView addSubview:emailTagLab];
    [emailTagLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCommonScreenWidth/2);
        make.top.mas_equalTo(nameTagLab.mas_top).offset(topH);
        make.height.mas_equalTo(emailTagLab.font.pointSize);
        make.width.mas_equalTo(ceil(tagSize.width));
    }];
    
    UILabel *emailLab = [UILabel by_init];
    emailLab.numberOfLines = 0;
    CGSize emailSize = [self.model.enrollModel.email getStringSizeWithAttributes:attributes maxSize:CGSizeMake(maxRightWidth, MAXFLOAT)];
    NSAttributedString *emailAttributed = [[NSAttributedString alloc] initWithString:self.model.enrollModel.email attributes:attributes];
    emailLab.attributedText = emailAttributed;
    [self.scrollView addSubview:emailLab];
    [emailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(emailTagLab.mas_right).offset(5);
        make.top.mas_equalTo(emailTagLab);
        make.height.mas_equalTo(ceil(emailSize.height));
        make.width.mas_equalTo(emailSize.width);
    }];
    
    CGFloat bottomH = phoneSize.height <= emailSize.height ? emailSize.height + 14 : phoneSize.height + 14;

    
    UILabel *timeTagLab = [UILabel by_init];
    timeTagLab.font = [UIFont boldSystemFontOfSize:13];
    timeTagLab.textColor = kColorRGBValue(0x323232);
    timeTagLab.text = @"有效期：";
    CGSize timeTagSize = [timeTagLab.text getStringSizeWithFont:timeTagLab.font maxWidth:120];
    [self.scrollView addSubview:timeTagLab];
    [timeTagLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(35);
        make.top.mas_equalTo(emailTagLab.mas_top).offset(bottomH);
        make.height.mas_equalTo(timeTagSize.height);
        make.width.mas_equalTo(ceil(timeTagSize.width));
    }];
    
    UILabel *timeLab = [UILabel by_init];
    timeLab.font = [UIFont systemFontOfSize:iPhone5 ? 12 : 14];
    timeLab.textColor = kColorRGBValue(0x323232);
    timeLab.text = [NSString stringWithFormat:@"%@ - %@",self.model.begin_time,self.model.end_time];
    [self.scrollView addSubview:timeLab];
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(timeTagLab);
        make.width.mas_equalTo(stringGetWidth(timeLab.text, iPhone5 ? 12 : 14));
        make.height.mas_equalTo(timeLab.font.pointSize);
        make.left.mas_equalTo(timeTagLab.mas_right).offset(5);
    }];
   
    UIImageView *topBgImgView = [[UIImageView alloc] init];
    topBgImgView.userInteractionEnabled = YES;
    [topBgImgView setImage:[[UIImage imageNamed:@"activity_bg_top"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 11, 20)]];
    [self.scrollView addSubview:topBgImgView];
    [self.scrollView sendSubviewToBack:topBgImgView];
    [topBgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(25);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.bottom.mas_equalTo(timeLab).offset(30);
    }];
    
    UIView *lineView = [UIView by_init];
    lineView.frame = CGRectMake(0, 0, kCommonScreenWidth - 35*2, 0.5);
    [self.scrollView addSubview:lineView];
    [lineView drawDottedLineByShapeLayer:2 lineWidth:3 lineColor:kColorRGBValue(0xd3d3d3) isHorizonal:YES];
    self.lineView = lineView;
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(35);
        make.width.mas_equalTo(kCommonScreenWidth - 35*2);
        make.bottom.mas_equalTo(topBgImgView);
        make.height.mas_equalTo(0.5);
    }];
    
    
    UIImageView *timeIcon = [[UIImageView alloc] init];
    [timeIcon by_setImageName:@"common_time_black"];
    [self.scrollView addSubview:timeIcon];
    [timeIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(35);
        make.top.mas_equalTo(topBgImgView.mas_bottom).offset(23);
        make.width.mas_equalTo(14);
        make.height.mas_equalTo(14);
    }];
    
    UILabel *timeLab1 = [UILabel by_init];
    [timeLab1 setBy_font:14];
    timeLab1.text = [NSString stringWithFormat:@"%@ - %@",self.model.begin_time,self.model.end_time];
    timeLab1.textColor = kColorRGBValue(0x323232);
    [self.scrollView addSubview:timeLab1];
    [timeLab1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(timeIcon.mas_right).offset(15);
        make.centerY.mas_equalTo(timeIcon);
        make.height.mas_equalTo(timeLab1.font.pointSize);
        make.width.mas_equalTo(kCommonScreenWidth - 100);
    }];
    
    UILabel *locationLab = [UILabel by_init];
    [locationLab setBy_font:14];
    locationLab.text = self.model.address;
    locationLab.textColor = kColorRGBValue(0x323232);
    locationLab.numberOfLines = 0;
    
    NSMutableParagraphStyle *locationParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    locationParagraphStyle.lineSpacing = 6.0; // 调整行间距
    NSDictionary *locationAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14],
                                      NSForegroundColorAttributeName:kColorRGBValue(0x323232),
                                      NSParagraphStyleAttributeName:locationParagraphStyle
                                      };
    CGSize locationSize = [self.model.address getStringSizeWithAttributes:locationAttributes maxSize:CGSizeMake(kCommonScreenWidth - 119, MAXFLOAT)];
    
    [self.scrollView addSubview:locationLab];
    [locationLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(64);
        make.top.mas_equalTo(timeLab1.mas_bottom).offset(17);
        make.height.mas_equalTo(ceil(locationSize.height));
        make.width.mas_equalTo(ceil(locationSize.width));
    }];
    
    UIImageView *locationIcon = [[UIImageView alloc] init];
    [locationIcon by_setImageName:@"common_location"];
    [self.scrollView addSubview:locationIcon];
    [locationIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(35);
        make.centerY.mas_equalTo(locationLab);
        make.width.mas_equalTo(14);
        make.height.mas_equalTo(16);
    }];
    
    UIImageView *rightIcon = [[UIImageView alloc] init];
    [rightIcon by_setImageName:@"livehome_rightIcon"];
    [self.scrollView addSubview:rightIcon];
    [rightIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(7);
        make.height.mas_equalTo(13);
        make.left.mas_equalTo(kCommonScreenWidth - 35 - 7);
        make.centerY.mas_equalTo(locationLab);
    }];
    
    UIButton *locationBtn = [UIButton by_buttonWithCustomType];
    [locationBtn addTarget:self action:@selector(locationBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:locationBtn];
    [locationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(locationLab);
        make.bottom.mas_equalTo(locationLab);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
    }];

    UIImageView *bottomBgImgView = [[UIImageView alloc] init];
    [bottomBgImgView setImage:[[UIImage imageNamed:@"activity_bg_bottom"] resizableImageWithCapInsets:UIEdgeInsetsMake(13, 20, 22, 20)]];
    [self.scrollView addSubview:bottomBgImgView];
    [self.scrollView sendSubviewToBack:bottomBgImgView];
    [bottomBgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(topBgImgView.mas_bottom);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.bottom.mas_equalTo(locationLab.mas_bottom).offset(30);
    }];
    
    UILabel *youqingLab = [UILabel by_init];
    youqingLab.font = [UIFont boldSystemFontOfSize:15];
    youqingLab.textColor = [UIColor whiteColor];
    youqingLab.text = @"友情提示：";
    [self.scrollView addSubview:youqingLab];
    [youqingLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(35);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(youqingLab.font.pointSize);
        make.top.mas_equalTo(bottomBgImgView.mas_bottom).offset(20);
    }];
    
    UILabel *oneLab = [UILabel initTitle:@"1.请不要把入场券分享给其他人，以免被盗用" font:13 textColor:[UIColor whiteColor]];
    [self.scrollView addSubview:oneLab];
    [oneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(35);
        make.top.mas_equalTo(youqingLab.mas_bottom).offset(15);
        make.width.mas_equalTo(kCommonScreenWidth - 35);
        make.height.mas_equalTo(oneLab.font.pointSize);
    }];
    
    UILabel *twoLab = [UILabel initTitle:@"2.入场时，请出示入场券" font:13 textColor:[UIColor whiteColor]];
    [self.scrollView addSubview:twoLab];
    [twoLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(35);
        make.top.mas_equalTo(oneLab.mas_bottom).offset(12);
        make.width.mas_equalTo(kCommonScreenWidth - 35);
        make.height.mas_equalTo(oneLab.font.pointSize);
    }];
    
    [self.scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(twoLab.mas_bottom).offset(kSafe_Mas_Bottom(58)).priorityLow();
        make.bottom.mas_greaterThanOrEqualTo(self.view);
    }];
}


- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
        _scrollView.backgroundColor = kColorRGBValue(0xea6438);
    }
    return _scrollView;
}

@end
