//
//  BYSignActivitySucController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYSignActivitySucController.h"
#import <Photos/Photos.h>

@interface BYSignActivitySucController ()

/** scrollView */
@property (nonatomic ,strong) UIScrollView *scrollView;
/** 微信码 */
@property (nonatomic ,strong) PDImageView *wechatImgView;
/** 微信号 */
@property (nonatomic ,strong) UILabel *wechatLab;


@end

@implementation BYSignActivitySucController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.popGestureRecognizerEnale = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"立即报名";
    self.view.backgroundColor = kColorRGBValue(0xea6438);
    [self leftBarButtonWithTitle:@"" barNorImage:nil barHltImage:nil action:NULL];
    [self setContentView];
}

#pragma mark - action

- (void)saveBtnAction{
    UIImage *image = self.wechatImgView.image;
    if (!image) return;
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        [PHAssetChangeRequest creationRequestForAssetFromImage:image];
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                showToastView(@"保存失败", self.view);
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                showToastView(@"保存成功", self.view);
            });
        }
    }];
}

- (void)copyBtnAction{
    [Tool copyWithString:self.wechat callback:NULL];
    showToastView(@"已复制", self.view);
}

- (void)confirmBtnAction{
    NSInteger count = self.navigationController.viewControllers.count;
    UIViewController *viewController = self.navigationController.viewControllers[count - 3];
    [self.navigationController popToViewController:viewController animated:YES];
}

#pragma mark - configUI

- (void)setContentView{
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    UIImageView *topBgImgView = [[UIImageView alloc] init];
    topBgImgView.userInteractionEnabled = YES;
    [topBgImgView setImage:[[UIImage imageNamed:@"activity_bg_top"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 11, 20)]];
    [self.scrollView addSubview:topBgImgView];
    [topBgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(25);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.height.mas_equalTo(342);
    }];
    
    UIImageView *sucImgView = [[UIImageView alloc] init];
    [sucImgView by_setImageName:@"activity_sign_suc"];
    [topBgImgView addSubview:sucImgView];
    [sucImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.left.mas_equalTo(kCommonScreenWidth/2 - 25 - 34 - 25/2);
        make.width.height.mas_equalTo(25);
    }];
    
    UILabel *sucLab = [UILabel by_init];
    sucLab.text = @"报名成功";
    sucLab.textColor = kColorRGBValue(0xea6438);
    sucLab.font = [UIFont boldSystemFontOfSize:20];
    [topBgImgView addSubview:sucLab];
    [sucLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(sucImgView.mas_right).offset(10);
        make.centerY.mas_equalTo(sucImgView);
        make.height.mas_equalTo(sucLab.font.pointSize);
        make.width.mas_equalTo(82);
    }];
    
    PDImageView *wechatImgView = [[PDImageView alloc] init];
    wechatImgView.contentMode = UIViewContentModeScaleAspectFill;
    wechatImgView.clipsToBounds = YES;
    [topBgImgView addSubview:wechatImgView];
    self.wechatImgView = wechatImgView;
    [wechatImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(130);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(75);
    }];
    [wechatImgView uploadHDImageWithURL:self.wechat_img callback:nil];
    
    UIButton *saveBtn = [UIButton by_buttonWithCustomType];
    [saveBtn setBy_attributedTitle:@{@"title":@"保存至相册",
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                     } forState:UIControlStateNormal];
    [saveBtn addTarget:self action:@selector(saveBtnAction) forControlEvents:UIControlEventTouchUpInside];
    saveBtn.layer.cornerRadius = 4.0f;
    saveBtn.layer.borderWidth = 0.5;
    saveBtn.layer.borderColor = kColorRGBValue(0xea6438).CGColor;
    [topBgImgView addSubview:saveBtn];
    [saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(30);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(wechatImgView.mas_bottom).offset(20);
    }];
    
    UILabel *hostTagLab = [UILabel by_init];
    hostTagLab.font = [UIFont systemFontOfSize:15];
    hostTagLab.textColor = kColorRGBValue(0x8c8c8c);
    hostTagLab.text = @"主办方微信号：";
    [topBgImgView addSubview:hostTagLab];
    [hostTagLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(iPhone5 ? 15 : 30);
        make.top.mas_equalTo(294);
        make.width.mas_equalTo(stringGetWidth(hostTagLab.text, 15));
        make.height.mas_equalTo(hostTagLab.font.pointSize);
    }];
    
    UIButton *copyBtn = [UIButton by_buttonWithCustomType];
    [copyBtn setBy_attributedTitle:@{@"title":@"复制",
                                     NSFontAttributeName:[UIFont systemFontOfSize:15],
                                     NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                     } forState:UIControlStateNormal];
    [copyBtn addTarget:self action:@selector(copyBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [topBgImgView addSubview:copyBtn];
    [copyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(49);
        make.height.mas_equalTo(35);
        make.right.mas_equalTo(iPhone5 ? -5 : -20);
        make.centerY.mas_equalTo(hostTagLab);
    }];
    
    UILabel *hostLab = [UILabel by_init];
    hostLab.font = [UIFont boldSystemFontOfSize:15];
    hostLab.textColor = kColorRGBValue(0x323232);
    hostLab.text = self.wechat;
    [topBgImgView addSubview:hostLab];
    self.wechatLab = hostLab;
    [hostLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(hostTagLab.mas_right).offset(5);
        make.centerY.mas_equalTo(hostTagLab);
        make.right.mas_equalTo(copyBtn.mas_left);
        make.height.mas_equalTo(hostTagLab.font.pointSize);
    }];
    
    NSString *string = @"提示：报名成功并非以获取通行证。若为付费活动，需联系主办方微信完成支付后可获取通行证；若为免费活动，请联系主办方及时通过审核。";
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 8.0; // 调整行间距
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14],
                                 NSForegroundColorAttributeName:kColorRGBValue(0x8f8f8f),
                                 NSParagraphStyleAttributeName:paragraphStyle
                                 };
    CGSize size = [string getStringSizeWithAttributes:attributes maxSize:CGSizeMake(kCommonScreenWidth - 60, MAXFLOAT)];
   
    UIImageView *bottomBgImgView = [[UIImageView alloc] init];
    [bottomBgImgView setImage:[[UIImage imageNamed:@"activity_bg_bottom"] resizableImageWithCapInsets:UIEdgeInsetsMake(13, 20, 22, 20)]];
    [self.scrollView addSubview:bottomBgImgView];
    [bottomBgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(topBgImgView.mas_bottom);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.height.mas_equalTo(ceil(size.height + 56));
    }];
    
    UILabel *messageLab = [UILabel by_init];
    messageLab.numberOfLines = 0;
    messageLab.attributedText = [[NSAttributedString alloc] initWithString:string attributes:attributes];
    [bottomBgImgView addSubview:messageLab];
    [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(25);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(ceil(size.width));
        make.height.mas_equalTo(ceil(size.height));
    }];
    

    UIView *lineView = [UIView by_init];
    lineView.frame = CGRectMake(36, 367, kCommonScreenWidth - 72, 0.5);
    [self.scrollView addSubview:lineView];
    [lineView drawDottedLineByShapeLayer:2 lineWidth:3 lineColor:kColorRGBValue(0xd3d3d3) isHorizonal:YES];

    
    UIButton *confirmBtn = [UIButton by_buttonWithCustomType];
    [confirmBtn setBy_attributedTitle:@{@"title":@"确定",
                                        NSFontAttributeName:[UIFont systemFontOfSize:15],
                                        NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                        } forState:UIControlStateNormal];
    [confirmBtn setBackgroundColor:[UIColor whiteColor]];
    [confirmBtn addTarget:self action:@selector(confirmBtnAction) forControlEvents:UIControlEventTouchUpInside];
    confirmBtn.layer.cornerRadius = 22.0f;
    [self.scrollView addSubview:confirmBtn];
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(bottomBgImgView.mas_bottom).offset(36);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.height.mas_equalTo(44);
    }];
    
    [self.scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(confirmBtn.mas_bottom).offset(kSafe_Mas_Bottom(35)).priorityLow();
        make.bottom.mas_greaterThanOrEqualTo(self.view);
    }];
}

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
        _scrollView.backgroundColor = kColorRGBValue(0xea6438);
    }
    return _scrollView;
}


@end
