//
//  BYSignActivitySucController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYSignActivitySucController : BYCommonViewController

/** 微信码 */
@property (nonatomic ,copy) NSString *wechat_img;
/** 微信号 */
@property (nonatomic ,copy) NSString *wechat;


@end

NS_ASSUME_NONNULL_END
