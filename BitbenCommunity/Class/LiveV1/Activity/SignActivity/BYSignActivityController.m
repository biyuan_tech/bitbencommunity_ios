//
//  BYSignActivityController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYSignActivityController.h"
#import "BYSignActivityViewModel.h"

@interface BYSignActivityController ()

@end

@implementation BYSignActivityController

- (Class)getViewModelClass{
    return [BYSignActivityViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"立即报名";
    [[UINavigationBar appearance] setShadowImage:[UIImage imageWithColor:[UIColor colorWithWhite:0 alpha:0.6]]];
}



@end
