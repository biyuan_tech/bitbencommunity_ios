//
//  BYActivityDetailSignModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/26.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYActivityDetailSignModel : BYActivityEnrollModel

/** nameSize */
@property (nonatomic ,assign) CGSize nameSize;
/** nameAttributed */
@property (nonatomic ,strong) NSAttributedString *nameAttributed;
/** nameSize */
@property (nonatomic ,assign) CGSize wxIdSize;
/** nameAttributed */
@property (nonatomic ,strong) NSAttributedString *wxIdAttributed;
/** nameSize */
@property (nonatomic ,assign) CGSize phoneSize;
/** nameAttributed */
@property (nonatomic ,strong) NSAttributedString *phoneAttributed;
/** nameSize */
@property (nonatomic ,assign) CGSize emailSize;
/** nameAttributed */
@property (nonatomic ,strong) NSAttributedString *emailAttributed;
/** mas_top */
@property (nonatomic ,assign) CGFloat mas_top;


+ (NSArray *)getSignTabelData:(NSArray *)respond;

@end

NS_ASSUME_NONNULL_END
