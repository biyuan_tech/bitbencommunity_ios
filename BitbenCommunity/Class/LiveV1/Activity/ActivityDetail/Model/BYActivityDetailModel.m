//
//  BYActivityDetailModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityDetailModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>
#import "BYLiveDetailModel.h"

@implementation BYActivityDetailModel


+ (NSArray *)getTableData:(BYActivityModel *)model{
    NSMutableArray *data = [NSMutableArray array];
    for (int i = 0; i < 3; i++) {
        NSDictionary *dic = [model mj_keyValues];
        if (i == 0) {
            BYActivityDetailModel *detailModel = [BYActivityDetailModel mj_objectWithKeyValues:dic];
            [data addObject:@{@"data":@[detailModel]}];
            detailModel.cellString = @"BYActivityDetailCell";
            // 封面图高度
            CGFloat coverH = (kCommonScreenWidth - 30)*180/345;
            NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.activity_title)];
            messageAttributed.yy_lineSpacing = 8.0f;
            messageAttributed.yy_font = [UIFont boldSystemFontOfSize:15];
            messageAttributed.yy_color = kColorRGBValue(0x323232);
            YYTextContainer *textContainer = [YYTextContainer new];
            textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
            YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
            
            detailModel.cellHeight = coverH + layout.textBoundingSize.height + 163 + 30;
            if (detailModel.pay_fee == 0) {
                detailModel.cellHeight = detailModel.cellHeight - 22 + 14;
            }
        }
        else if (i == 1){
            NSMutableArray *cellData = [NSMutableArray array];
            if (model.host_list.count) {
                BYCommonModel *titleModel = [self getSignleTitleModel:@"创建人"];
                [cellData addObject:titleModel];
                [model.host_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary *tmpDic = [obj mj_keyValues];
                    BYLiveDetailUserModel *userModel = [BYLiveDetailUserModel mj_objectWithKeyValues:tmpDic];
                    userModel.cellString = @"BYLiveDetailHostCell";
                    userModel.cellHeight = 68;
                    userModel.showBottomLine = model.interviewee_list.count ? YES : NO;
                    [cellData addObject:userModel];
                }];
            }
            if (model.interviewee_list.count) {
                BYCommonModel *titleModel = [self getSignleTitleModel:@"嘉宾"];
                [cellData addObject:titleModel];
                [model.interviewee_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary *tmpDic = [obj mj_keyValues];
                    BYLiveDetailUserModel *userModel = [BYLiveDetailUserModel mj_objectWithKeyValues:tmpDic];
                    userModel.cellString = @"BYLiveDetailHostCell";
                    userModel.cellHeight = 68;
                    userModel.isActivity = YES;
                    userModel.showBottomLine = idx != model.host_list.count - 1 ? YES : NO;
                    [cellData addObject:userModel];
                }];
            }
            [data addObject:@{@"data":[cellData copy]}];
        }
        else{
            BYActivityDetailModel *detailModel = [BYActivityDetailModel mj_objectWithKeyValues:dic];
            [data addObject:@{@"data":@[detailModel]}];
            detailModel.cellString = @"BYLiveDetailIntroCell";
            
            NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(detailModel.activity_intro)];
            messageAttributed.yy_lineSpacing = 12.0f;
            messageAttributed.yy_font = [UIFont systemFontOfSize:14];
            messageAttributed.yy_color = kColorRGBValue(0x585858);
            YYTextContainer *textContainer = [YYTextContainer new];
            textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
            YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
            
            detailModel.titleSize = layout.textBoundingSize;
            if (!detailModel.activity_intro.length && !detailModel.ad_img.length) {
                detailModel.cellHeight = 300;
            }else {
                detailModel.cellHeight = 73 + layout.textBoundingSize.height + 10;
            }
        }
    }
    return [data copy];
}
+ (BYCommonModel *)getSignleTitleModel:(NSString *)title{
    BYCommonModel *model = [[BYCommonModel alloc] init];
    model.cellString = @"BYLiveDetailSingleTitleCell";
    model.cellHeight = 30;
    model.title = title;
    return model;
}

@end
