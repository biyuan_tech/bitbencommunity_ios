//
//  BYActivityDetailSignModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/26.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityDetailSignModel.h"

@implementation BYActivityDetailSignModel

+ (NSArray *)getSignTabelData:(NSArray *)respond{
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (!respond.count) {
        return @[];
    }
    __block NSMutableArray *data = [NSMutableArray array];
    [respond enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BYActivityDetailSignModel *model = [BYActivityDetailSignModel mj_objectWithKeyValues:obj];
        model.cellString = @"BYActivityDetailSignCell";
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3.0; // 调整行间距
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                     NSForegroundColorAttributeName:kColorRGBValue(0x8f8f8f),
                                     NSParagraphStyleAttributeName:paragraphStyle
                                     };
        
        
        CGSize spaceLabSize = [@"姓名:" getStringSizeWithFont:[UIFont boldSystemFontOfSize:13] maxWidth:120];
        CGFloat maxLeftWidth = kCommonScreenWidth/2 - 30 - spaceLabSize.width - 5;
        CGFloat maxRightWidth = kCommonScreenWidth/2 - 30 - spaceLabSize.width - 10;
        
        model.nameSize = [model.name getStringSizeWithAttributes:attributes maxSize:CGSizeMake(maxLeftWidth, MAXFLOAT)];
        model.nameAttributed = [[NSAttributedString alloc] initWithString:model.name attributes:attributes];

        model.wxIdSize = [model.wechat getStringSizeWithAttributes:attributes maxSize:CGSizeMake(maxRightWidth, MAXFLOAT)];
        model.wxIdAttributed = [[NSAttributedString alloc] initWithString:model.wechat attributes:attributes];

        model.phoneSize = [model.phone getStringSizeWithAttributes:attributes maxSize:CGSizeMake(maxLeftWidth, MAXFLOAT)];
        model.phoneAttributed = [[NSAttributedString alloc] initWithString:model.phone attributes:attributes];
        
        model.emailSize = [model.email getStringSizeWithAttributes:attributes maxSize:CGSizeMake(maxRightWidth, MAXFLOAT)];
        model.emailAttributed = [[NSAttributedString alloc] initWithString:model.email attributes:attributes];
        
        CGFloat topH = model.nameSize.height <= model.wxIdSize.height ? model.wxIdSize.height : model.nameSize.height;
        CGFloat bottomH = model.phoneSize.height <= model.emailSize.height ? model.emailSize.height : model.phoneSize.height;
        
        model.mas_top = topH - spaceLabSize.height + 12;
        
        model.cellHeight = 90 + topH + 12 + bottomH + 17;

        [data addObject:model];
    }];
    return [data copy];
}

@end
