//
//  BYActivityDetailViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityDetailViewModel.h"
#import "BYActivityDetailController.h"
#import "BYActivityDetailSubController.h"
#import "BYActivityDetailSignSubController.h"
#import "HGSegmentedPageViewController.h"
#import "HGCenterBaseTableView.h"

#import <YYLabel.h>

#import "BYLiveDetailModel.h"
#import "BYActivityModel.h"
#import <MapKit/MapKit.h>

#define K_VC ((BYActivityDetailController *)S_VC)
@interface BYActivityDetailViewModel ()<HGSegmentedPageViewControllerDelegate, HGPageViewControllerDelegate,UITableViewDelegate,UITableViewDataSource>

/** segmentViewq */
@property (nonatomic ,strong) HGSegmentedPageViewController *segmentView;
/** 详情 */
@property (nonatomic ,strong) BYActivityDetailSubController *detailSubController;
/** 报名详情 */
@property (nonatomic ,strong) BYActivityDetailSignSubController *signSubController;
/** tableView */
@property (nonatomic ,strong) HGCenterBaseTableView *tableView;
/** 是否可滑动 */
@property (nonatomic ,assign) BOOL cannotScroll;
/** tableFooterView */
@property (nonatomic ,strong) UIView *tableFooterView;
/** model */
@property (nonatomic ,strong) BYActivityModel *model;
/** redDotNum */
@property (nonatomic ,strong) YYLabel *redDotNumLab;


@end

@implementation BYActivityDetailViewModel

- (void)setContentView{
    
    [self loadRequestGetDetail];
}

- (void)viewWillAppear{
    if (_detailSubController) {
        [self loadRequestGetDetail];
    }
}

#pragma mark - BYCommonTableViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat contentOffsetY = scrollView.contentOffset.y;
    CGFloat criticalPointOffsetY = 0;
    
    //利用contentOffset处理内外层scrollView的滑动冲突问题
    if (contentOffsetY >= criticalPointOffsetY) {
        
        self.cannotScroll = YES;
        scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        [self.segmentView.currentPageViewController makePageViewControllerScroll:YES];
    } else {
        
        if (self.cannotScroll) {
            //“维持吸顶状态”
            scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        } else {
            /* 吸顶状态 -> 不吸顶状态
             * categoryView的子控制器的tableView或collectionView在竖直方向上的contentOffsetY小于等于0时，会通过代理的方式改变当前控制器self.canScroll的值；
             */
        }
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    [self.segmentView.currentPageViewController makePageViewControllerScrollToTop];
    return YES;
}

#pragma mark - HGSegmentedPageViewControllerDelegate
- (void)segmentedPageViewControllerWillBeginDragging {
    self.tableView.scrollEnabled = NO;
}

- (void)segmentedPageViewControllerDidEndDragging {
    self.tableView.scrollEnabled = YES;
}

#pragma mark - HGPageViewControllerDelegate
- (void)pageViewControllerLeaveTop {
    self.cannotScroll = NO;
}


#pragma mark - request
- (void)loadRequestGetDetail{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetActivityDetail:K_VC.activiry_id successBlock:^(id object) {
        @strongify(self);
        self.model = object;
        [self configUI];
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"获取详情失败", S_V_VIEW);
    }];
}

- (void)loadRequestDelLive{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestDelTheme_id:@"" theme_type:BY_THEME_TYPE_LIVE successBlock:^(id object) {
        @strongify(self);
        [S_V_NC popViewControllerAnimated:YES];
        showToastView(@"删除成功", kCommonWindow);
    } faileBlock:^(NSError *error) {
        showToastView(@"删除失败", kCommonWindow);
    }];
}


#pragma mark - configUI

- (void)configUI{
    if (!_detailSubController) {
        if ([self.model.user_id isEqualToString:ACCOUNT_ID]) {
            [self addTableView];
            [S_V_VIEW addSubview:self.redDotNumLab];
            NSString *num = stringFormatInteger(self.model.enroll_pending_number);
            self.redDotNumLab.text = num;
            CGFloat width = stringGetWidth(num, 11) + 8;
            width = width < 14 ? 14 : width;
            [self.redDotNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(3*kCommonScreenWidth/4 + 30);
                make.width.mas_equalTo(width);
                make.height.mas_equalTo(14);
                make.top.mas_equalTo((26 - 14)/2);
            }];
            self.redDotNumLab.hidden = self.model.enroll_pending_number > 0 ? NO : YES;
        }
        else {
            [S_V_VIEW addSubview:self.detailSubController.view];
            [self.detailSubController.view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsZero);
            }];
        }
    }
    [_detailSubController reloadData:self.model];
    [_signSubController reloadData:self.model];
}
- (void)addTableView{
    HGCenterBaseTableView *tableView = [[HGCenterBaseTableView alloc] init];
    tableView.categoryViewHeight = 34;
    tableView.bounces = NO;
    tableView.delegate = self;
    tableView.tableFooterView = self.tableFooterView;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.showsHorizontalScrollIndicator = NO;
    [S_V_VIEW addSubview:tableView];
    [S_V_VIEW sendSubviewToBack:tableView];
    self.tableView = tableView;
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (BYActivityDetailSubController *)detailSubController{
    if (!_detailSubController) {
        _detailSubController = [[BYActivityDetailSubController alloc] init];
        [S_VC addChildViewController:_detailSubController];
    }
    return _detailSubController;
}

- (BYActivityDetailSignSubController *)signSubController{
    if (!_signSubController) {
        _signSubController = [[BYActivityDetailSignSubController alloc] init];
        @weakify(self);
        _signSubController.didSignHandle = ^{
            @strongify(self);
            self.model.enroll_pending_number = self.model.enroll_pending_number - 1;
            NSString *num = stringFormatInteger(self.model.enroll_pending_number);
            self.redDotNumLab.text = num;
            CGFloat width = stringGetWidth(num, 11) + 8;
            width = width < 14 ? 14 : width;
            [self.redDotNumLab mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(width);
                make.height.mas_equalTo(14);
            }];
            self.redDotNumLab.hidden = self.model.enroll_pending_number > 0 ? NO : YES;
        };
        [S_VC addChildViewController:_signSubController];
    }
    return _signSubController;
}

- (HGSegmentedPageViewController *)segmentView {
    if (!_segmentView) {
        NSMutableArray *controllers = [NSMutableArray array];
        NSArray *titles = @[@"会议详情", @"报名详情"];
        for (int i = 0; i < titles.count; i++) {
            HGPageViewController *controller;
            if (i == 0) {
                controller = self.detailSubController;
            } else {
                controller = self.signSubController;
            }
            controller.delegate = self;
            [controllers addObject:controller];
        }
        _segmentView = [[HGSegmentedPageViewController alloc] init];
        _segmentView.pageViewControllers = controllers;
        _segmentView.categoryView.titles = titles;
        _segmentView.categoryView.height = 34;
        _segmentView.categoryView.itemSpacing = 0;
        _segmentView.categoryView.alignment = HGCategoryViewAlignmentCenter;
        _segmentView.categoryView.originalIndex = 0;
        _segmentView.categoryView.isEqualParts = YES;
        _segmentView.categoryView.titleNomalFont = [UIFont boldSystemFontOfSize:15];
        _segmentView.categoryView.titleSelectedFont = [UIFont boldSystemFontOfSize:15];
        _segmentView.categoryView.titleNormalColor = kColorRGBValue(0x8f8f8f);
        _segmentView.categoryView.titleSelectedColor = kColorRGBValue(0x323232);
        _segmentView.categoryView.backgroundColor = [UIColor whiteColor];
        _segmentView.categoryView.vernierHeight = 3;
        _segmentView.categoryView.vernierWidth = 20;
        _segmentView.categoryView.vernierBottomSapce = 0.0f;
        _segmentView.categoryView.vernier.backgroundColor = kColorRGBValue(0xea6441);
        _segmentView.categoryView.topBorder.hidden = YES;
        _segmentView.categoryView.bottomBorder.backgroundColor = kColorRGBValue(0xe7e7ea);
        _segmentView.delegate = self;
    }
    return _segmentView;
}

- (UIView *)tableFooterView{
    if (!_tableFooterView) {
        _tableFooterView = [[UIView alloc] init];
        _tableFooterView.frame = CGRectMake(0, 0, kCommonScreenWidth, kCommonScreenHeight - kNavigationHeight);
        [S_VC addChildViewController:self.segmentView];
        [_tableFooterView addSubview:self.segmentView.view];
        [self.segmentView didMoveToParentViewController:S_VC];
        @weakify(self);
        [self.segmentView.view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.edges.equalTo(self.tableFooterView);
        }];
    }
    return _tableFooterView;
}

- (YYLabel *)redDotNumLab{
    if (!_redDotNumLab) {
        _redDotNumLab = [[YYLabel alloc] init];
        _redDotNumLab.backgroundColor = kColorRGBValue(0xef1212);
        _redDotNumLab.textColor = [UIColor whiteColor];
        _redDotNumLab.layer.cornerRadius = 7.0;
        _redDotNumLab.textAlignment = NSTextAlignmentCenter;
        _redDotNumLab.font = [UIFont systemFontOfSize:11];
    }
    return _redDotNumLab;
}

@end
