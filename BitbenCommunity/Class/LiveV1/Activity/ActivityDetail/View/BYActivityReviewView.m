//
//  BYActivityReviewView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityReviewView.h"

static NSInteger baseTag = 0x111;
@interface BYActivityReviewView ()
{
    CGPoint _point;
}
/** moreView */
@property (nonatomic ,strong) UIView *moreView;
/** maskView */
@property (nonatomic ,strong) UIControl *maskView;
/**  superView */
@property (nonatomic ,strong) UIView *fathuerView;
/** cb */
@property (nonatomic ,copy) reviewHandle handle;

@end

@implementation BYActivityReviewView

+ (instancetype)initReviewViewShowInView:(UIView *)view{
    BYActivityReviewView *reviewView= [[BYActivityReviewView alloc] init];
    reviewView.fathuerView = view ? view : kCommonWindow;
    reviewView.frame = reviewView.fathuerView.bounds;
    [reviewView setContentView];
    return reviewView;
}

- (void)showAnimationWithPoint:(CGPoint)point cb:(reviewHandle)cb{
    self.handle = cb;
    _point = point;
    self.moreView.frame = CGRectMake(point.x - 80 - 15, point.y, 80, 80);
    if (!self.superview) {
        [self.fathuerView addSubview:self];
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.moreView.alpha = 1.0;
        self.maskView.alpha = 1.0;
    }];
}

- (void)hiddenAnimation{
    [UIView animateWithDuration:0.1 animations:^{
        self.maskView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


- (void)maskViewAction{
    if (self.handle) {
        self.handle(0, NO);
    }
    [self hiddenAnimation];
}

- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - baseTag;
    BY_ACTIVITY_STATUS status = index == 0 ? BY_ACTIVITY_STATUS_REVIEW_PASS : BY_ACTIVITY_STATUS_REVIEW_FAIL;
    [self loadRequestReviewSign:status];
    [self hiddenAnimation];
}

#pragma mark - request

- (void)loadRequestReviewSign:(BY_ACTIVITY_STATUS)status{
    if (!_activity_enroll_id.length) {
        showToastView(@"审核id为空", self);
        return;
    }
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestReviewSign:status activity_enroll_id:_activity_enroll_id successBlock:^(id object) {
        @strongify(self);
        if (self.handle) {
            self.handle(status, YES);
        }
    } faileBlock:^(NSError *error) {
        @strongify(self);
        if (self.handle) {
            self.handle(status, NO);
        }
    }];
}

#pragma mark - configUI

- (void)setContentView{
    self.maskView = [UIControl by_init];
    [self.maskView addTarget:self action:@selector(maskViewAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    self.moreView = [UIView by_init];
    [self.moreView setBackgroundColor:[UIColor whiteColor]];
    self.moreView.frame = CGRectMake(0, 0, 80, 80);
    self.moreView.layer.cornerRadius = 4.0f;
    self.moreView.layer.shadowColor = kColorRGB(198, 198, 198, 0.3).CGColor;
    self.moreView.layer.shadowOffset = CGSizeMake(0,0);
    self.moreView.layer.shadowOpacity = 1.0;
    self.moreView.layer.shadowRadius = 4;
    self.moreView.alpha = 1.0;
    self.moreView.clipsToBounds = NO;
    [self addSubview:self.moreView];
    
    NSArray *titles = @[@"通过",@"驳回"];
    for (int i = 0; i < 2; i ++) {
        UIButton *button = [UIButton by_buttonWithCustomType];
        [button setBy_attributedTitle:@{@"title":titles[i],
                                        NSFontAttributeName:[UIFont boldSystemFontOfSize:13],
                                        NSForegroundColorAttributeName:kColorRGBValue(0x323232)
                                        } forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = baseTag + i;
        [self.moreView addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.top.mas_equalTo(40*i);
            make.height.mas_equalTo(40);
        }];
        
        if (i == 0) {
            UIView *lineView = [UIView by_init];
            [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
            [self.moreView addSubview:lineView];
            [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.mas_equalTo(0);
                make.top.mas_equalTo(40);
                make.height.mas_equalTo(0.5);
            }];
        }
    }
}

@end
