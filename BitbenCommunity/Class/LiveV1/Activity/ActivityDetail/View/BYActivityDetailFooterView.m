//
//  BYActivityDetailFooterView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityDetailFooterView.h"
#import "BYILiveController.h"
#import "BYPushFlowController.h"
#import "BYSignActivityController.h"

@interface BYActivityDetailFooterView ()

/** button */
@property (nonatomic ,strong) UIButton *otherBtn;
/** watchBtn */
@property (nonatomic ,strong) UIButton *watchLiveBtn;
/** model */
@property (nonatomic ,strong) BYActivityModel *model;

@end

@implementation BYActivityDetailFooterView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setContentView];
    }
    return self;
}

- (void)reloadData:(BYActivityModel *)model{
    self.model = model;
    if ([self.model.user_id isEqualToString:ACCOUNT_ID]) { // 创建者
        if (model.activity_type != BY_NEWLIVE_TYPE_OTHER) { // 同步直播
            [self setOtherBtnTitle:@"进入直播" forState:UIControlStateNormal];
//            [self reloadSubViewLayout];
            self.watchLiveBtn.hidden = YES;
            [self.otherBtn mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(15);
            }];
        }
    }else { // 观众
        [self setGuestFooterView];
    }
}

- (void)setGuestFooterView{
    // 刷新布局
    [self reloadSubViewLayout];
    
    BOOL disabled = NO;
    NSString *title;
    if (self.model.status == BY_ACTIVITY_STATUS_END) { // 活动已结束
        disabled = YES;
        title = @"活动已结束";
    }
    else
    {
        if (self.model.enroll_status == BY_ACTIVITY_STATUS_NOT_ENROLL) { // 未报名
            if (self.model.limit_number <= self.model.pass_number && self.model.limit_number != 0) { // 名额已满
                disabled = YES;
                title = @"名额已满，下次记得早点来哦";
            }
            else
            {
                title = @"立即报名";
            }
        }
        else
        {
            if (self.model.enroll_status == BY_ACTIVITY_STATUS_REVIEW_PASS) { // 已通过
                disabled = YES;
                title = @"已获得入场券，请届时参加";
            }
            else if (self.model.enroll_status == BY_ACTIVITY_STATUS_REVIEW_WAITING) { // 待审核
                if (self.model.limit_number <= self.model.pass_number && self.model.limit_number != 0) { // 名额已满
                    disabled = YES;
                    title = @"名额已满，下次记得早点来哦";
                }
                else
                {
                    disabled = YES;
                    title = @"已报名，等待审核";
                }
            }
            else if (self.model.enroll_status == BY_ACTIVITY_STATUS_REVIEW_FAIL) { // 已驳回
                if (self.model.limit_number <= self.model.pass_number && self.model.limit_number != 0) { // 名额已满
                    disabled = YES;
                    title = @"名额已满，下次记得早点来哦";
                }
                else
                {
                    title = @"被驳回，再次报名";
                }
            }
        }
    }
    UIControlState state = disabled ? UIControlStateDisabled : UIControlStateNormal;
    [self setOtherBtnTitle:title forState:state];
}

- (void)setOtherBtnTitle:(NSString *)title forState:(UIControlState)state{
    [self.otherBtn setBy_attributedTitle:@{@"title":title,
                                           NSFontAttributeName:[UIFont systemFontOfSize:16],
                                           NSForegroundColorAttributeName:[UIColor whiteColor]
                                           } forState:state];
    UIColor *bgColor = state == UIControlStateNormal ? kColorRGBValue(0xea6438) : kColorRGBValue(0xc8c8c8);
    [self.otherBtn setBackgroundColor:bgColor];
    BOOL enabled = state == UIControlStateNormal ? YES : NO;
    self.otherBtn.enabled = enabled;
}

- (void)reloadSubViewLayout{
    if (self.model.activity_type != BY_NEWLIVE_TYPE_OTHER) { // 同步直播
        self.watchLiveBtn.hidden = NO;
        [self.otherBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(130);
        }];
    }
    else
    {
        self.watchLiveBtn.hidden = YES;
        [self.otherBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
        }];
    }
    [self layoutIfNeeded];
}

#pragma mark - action
- (void)watchBtnAction{
    if (self.model.status == BY_ACTIVITY_STATUS_REVIEW_WAITING ||
        self.model.status == BY_ACTIVITY_STATUS_REVIEW_FAIL) {
        NSString *title;
        if (self.model.status == BY_LIVE_STATUS_REVIEW_WAITING) {
            title = @"审核中无法进入直播间";
        }else if (self.model.status == BY_LIVE_STATUS_REVIEW_FAIL){
            title = @"审核失败无法进入直播间";
        }else {
            title = @"直播超时无法进入直播间";
        }
        showToastView(title, CURRENT_VC.view);
        return;
    }
    if (self.model.activity_type == BY_NEWLIVE_TYPE_ILIVE) {
        BYILiveController *iliveController = [[BYILiveController alloc] init];
        iliveController.live_record_id = self.model.live_record_id;
        iliveController.isHost = [ACCOUNT_ID isEqualToString:self.model.user_id] ? YES : NO;
        [CURRENT_VC.navigationController pushViewController:iliveController animated:YES];
    }
    else if (self.model.activity_type == BY_NEWLIVE_TYPE_VIDEO) {
        BYPushFlowController *pushFlowController = [[BYPushFlowController alloc] init];
        pushFlowController.live_record_id = self.model.live_record_id;
        pushFlowController.isHost = [ACCOUNT_ID isEqualToString:self.model.user_id] ? YES : NO;
        [CURRENT_VC.navigationController pushViewController:pushFlowController animated:YES];
    }
}

- (void)otherBtnAction{
    if ([self.model.user_id isEqualToString:ACCOUNT_ID]) { // 创建者
        if (self.model.activity_type == BY_NEWLIVE_TYPE_ILIVE) {
            BYILiveController *iliveController = [[BYILiveController alloc] init];
            iliveController.live_record_id = self.model.live_record_id;
            iliveController.isHost = [ACCOUNT_ID isEqualToString:self.model.user_id] ? YES : NO;
            [CURRENT_VC.navigationController pushViewController:iliveController animated:YES];
        }
        else if (self.model.activity_type == BY_NEWLIVE_TYPE_VIDEO) {
            BYPushFlowController *pushFlowController = [[BYPushFlowController alloc] init];
            pushFlowController.live_record_id = self.model.live_record_id;
            pushFlowController.isHost = [ACCOUNT_ID isEqualToString:self.model.user_id] ? YES : NO;
            [CURRENT_VC.navigationController pushViewController:pushFlowController animated:YES];
        }
    }
    else
    {
        BYSignActivityController *signActivityController = [[BYSignActivityController alloc] init];
        signActivityController.activity_id = self.model.activity_id;
//        [CURRENT_VC.navigationController pushViewController:signActivityController animated:YES];
        [(BYCommonViewController *)CURRENT_VC authorizePush:signActivityController animation:YES];
    }
}

#pragma mark - configUI

- (void)setContentView{

    UIButton *watchLiveBtn = [UIButton by_buttonWithCustomType];
    [watchLiveBtn setBy_imageName:@"artivity_detail_watch" forState:UIControlStateNormal];
    [watchLiveBtn setBy_attributedTitle:@{@"title":@"看直播",
                                          NSFontAttributeName:[UIFont boldSystemFontOfSize:14],
                                          NSForegroundColorAttributeName:kColorRGBValue(0x323232)
                                          } forState:UIControlStateNormal];
    watchLiveBtn.layer.cornerRadius = 22.0f;
    [watchLiveBtn setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    watchLiveBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 3);
    watchLiveBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
    [watchLiveBtn addTarget:self action:@selector(watchBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:watchLiveBtn];
    self.watchLiveBtn = watchLiveBtn;
    [watchLiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(11);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(44);
    }];
    
    UIButton *otherBtn = [UIButton by_buttonWithCustomType];
    otherBtn.layer.cornerRadius = 22.0f;
    [otherBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    [otherBtn addTarget:self action:@selector(otherBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:otherBtn];
    self.otherBtn = otherBtn;
    [otherBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(130);
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(11);
        make.height.mas_equalTo(44);
    }];
    
    UIView *lineView = [UIView by_init];
    lineView.backgroundColor = kColorRGBValue(0xe7e7ea);
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
}

@end
