//
//  BYActivityReviewView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^reviewHandle)(BY_ACTIVITY_STATUS status,BOOL isSuccess);
@interface BYActivityReviewView : UIView

/** 活动报名id */
@property (nonatomic ,copy) NSString *activity_enroll_id;


- (void)showAnimationWithPoint:(CGPoint)point cb:(reviewHandle)cb;

+ (instancetype)initReviewViewShowInView:(UIView *)view;

@end

NS_ASSUME_NONNULL_END
