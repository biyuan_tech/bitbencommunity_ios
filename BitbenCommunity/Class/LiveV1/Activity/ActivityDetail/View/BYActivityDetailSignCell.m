//
//  BYActivityDetailSignCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/26.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityDetailSignCell.h"

#import "BYPersonHomeController.h"

#import "BYActivityDetailSignModel.h"

@interface BYActivityDetailSignCell ()

/** bgView */
@property (nonatomic ,strong) UIView *bgView;
/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogo;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 发布时间 */
@property (nonatomic ,strong) UILabel *timeLab;
/** 审核状态 */
@property (nonatomic ,strong) UILabel *statusLab;
/** 审核按钮 */
@property (nonatomic ,strong) UIButton *moreBtn;
/** 姓名 */
@property (nonatomic ,strong) UILabel *nameLab;
/** nameTag */
@property (nonatomic ,strong) UILabel *nameTagLab;
/** 微信 */
@property (nonatomic ,strong) UILabel *wxIdLab;
/** wxIdTag */
@property (nonatomic ,strong) UILabel *wxIdTagLab;
/** 电话 */
@property (nonatomic ,strong) UILabel *telPhoneLab;
/** telPhoneTagLab */
@property (nonatomic ,strong) UILabel *telPhoneTagLab;
/** 邮箱 */
@property (nonatomic ,strong) UILabel *emailLab;
/** emailLab */
@property (nonatomic ,strong) UILabel *emailTagLab;
/** model */
@property (nonatomic ,strong) BYActivityDetailSignModel *model;


@end

@implementation BYActivityDetailSignCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYActivityDetailSignModel *model = object[indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    [self.userlogo uploadHDImageWithURL:model.head_img callback:nil];
    self.userlogo.style = model.cert_badge;
    self.userNameLab.text = nullToEmpty(model.nickname);
    self.timeLab.text = nullToEmpty(model.create_time);
    
    self.nameLab.attributedText = model.nameAttributed;
    self.wxIdLab.attributedText = model.wxIdAttributed;
    self.telPhoneLab.attributedText = model.phoneAttributed;
    self.emailLab.attributedText = model.emailAttributed;
    
    [self verifyStatus];
    
    [self.nameLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceil(model.nameSize.width));
        make.height.mas_equalTo(ceil(model.nameSize.height));
    }];
    
    [self.wxIdLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceil(model.wxIdSize.width));
        make.height.mas_equalTo(ceil(model.wxIdSize.height));
    }];
    
    [self.telPhoneTagLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameTagLab.mas_bottom).offset(model.mas_top);
    }];
    
    [self.emailTagLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.wxIdTagLab.mas_bottom).offset(model.mas_top);
    }];
    
    [self.telPhoneLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceil(model.phoneSize.width));
        make.height.mas_equalTo(ceil(model.phoneSize.height));
    }];
    
    [self.emailLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceil(model.emailSize.width));
        make.height.mas_equalTo(ceil(model.emailSize.height));
    }];
        
}

- (void)verifyStatus{
    switch (self.model.enroll_status) {
        case BY_ACTIVITY_STATUS_OVERTIME:
            self.statusLab.text = @"已过期";
            self.statusLab.textColor = kColorRGBValue(0xce1e1e);
            break;
        case BY_ACTIVITY_STATUS_REVIEW_FAIL:
            self.statusLab.text = @"已驳回";
            self.statusLab.textColor = kColorRGBValue(0xce1e1e);
            break;
        case BY_ACTIVITY_STATUS_REVIEW_WAITING:
            self.statusLab.text = @"待审核";
            self.statusLab.textColor = kColorRGBValue(0x1a86ff);
            break;
        case BY_ACTIVITY_STATUS_REVIEW_PASS:
            self.statusLab.text = @"已通过";
            self.statusLab.textColor = kColorRGBValue(0x10d041);
            break;
        default:
            self.statusLab.text = @"已过期";
            self.statusLab.textColor = kColorRGBValue(0xce1e1e);
            break;
    }
    
    self.moreBtn.hidden = self.model.enroll_status == BY_ACTIVITY_STATUS_REVIEW_WAITING ? NO : YES;
    CGFloat mas_right = self.model.enroll_status == BY_ACTIVITY_STATUS_REVIEW_WAITING ? -55 : -30;
    [self.statusLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(mas_right);
    }];
}

#pragma mark - action
- (void)userlogoAction{
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    if ([currentController isKindOfClass:[BYPersonHomeController class]]) {
        return;
    }
    BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
    personHomeController.user_id = self.model.user_id;
    [(AbstractViewController *)currentController authorizeWithCompletionHandler:^(BOOL successed) {
        if (!successed) return ;
        [currentController.navigationController pushViewController:personHomeController animated:YES];
    }];
}

// 举报
- (void)reportBtnAction:(UIButton *)sender{
    CGPoint point = [self.contentView convertPoint:sender.center toView:kCommonWindow];
    NSValue *value = [NSValue valueWithCGPoint:CGPointMake(point.x + CGRectGetWidth(sender.frame)/2, point.y + CGRectGetHeight(sender.frame)/2)];
    [self sendActionName:@"moreAction" param:@{@"point":value} indexPath:self.indexPath];
}

#pragma mark - configUI

- (void)setContentView{
    self.bgView = [UIView by_init];
    self.bgView.layer.cornerRadius = 10.0f;
    self.bgView.backgroundColor = kColorRGBValue(0xf7f8f8);
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(15, 15, 0, 15));
    }];
    
    PDImageView *userlogo = [[PDImageView alloc] init];
    userlogo.contentMode = UIViewContentModeScaleAspectFill;
    userlogo.layer.cornerRadius = 17;
    userlogo.clipsToBounds = YES;
    userlogo.userInteractionEnabled = YES;
    @weakify(self);
    [userlogo addTapGestureRecognizer:^{
        @strongify(self);
        [self userlogoAction];
    }];
    [self.contentView addSubview:userlogo];
    self.userlogo = userlogo;
    [userlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.width.height.mas_equalTo(34);
        make.top.mas_equalTo(25);
    }];
    
    // 用户昵称
    UILabel *userNameLab = [UILabel by_init];
    userNameLab.font = [UIFont boldSystemFontOfSize:15];
    userNameLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:userNameLab];
    self.userNameLab = userNameLab;
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(9);
        make.top.mas_equalTo(userlogo);
        make.height.mas_equalTo(userNameLab.font.pointSize);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    // 发布时间
    UILabel *timeLab  = [UILabel by_init];
    [timeLab setBy_font:13];
    timeLab.textColor = kColorRGBValue(0x8f8f8f);
    timeLab.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:timeLab];
    self.timeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(9);
        make.bottom.mas_equalTo(userlogo);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(timeLab.font.pointSize);
    }];
    
    UILabel *statusLab  = [UILabel by_init];
    [statusLab setBy_font:13];
    statusLab.textColor = kColorRGBValue(0x1a86ff);
    statusLab.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:statusLab];
    self.statusLab = statusLab;
    [statusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(55);
        make.centerY.mas_equalTo(userlogo);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(statusLab.font.pointSize);
    }];
    
    // 更多(审核)
    UIButton *moreBtn = [UIButton by_buttonWithCustomType];
    [moreBtn setBy_imageName:@"icon_live_more" forState:UIControlStateNormal];
    [moreBtn addTarget:self action:@selector(reportBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:moreBtn];
    self.moreBtn = moreBtn;
    self.moreBtn.hidden = YES;
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(48);
        make.centerY.mas_equalTo(userlogo.mas_centerY).mas_offset(0);
        make.height.mas_equalTo(28);
        make.right.mas_equalTo(-15);
    }];
    
    UIView *lineView = [UIView by_init];
    lineView.frame = CGRectMake(30, 74, kCommonScreenWidth - 60, 0.5);
    [self.contentView addSubview:lineView];
    [lineView drawDottedLineByShapeLayer:2 lineWidth:3 lineColor:kColorRGBValue(0xd3d3d3) isHorizonal:YES];
    
    UILabel *nameTagLab = [UILabel by_init];
    nameTagLab.font = [UIFont boldSystemFontOfSize:13];
    nameTagLab.textColor = kColorRGBValue(0x323232);
    nameTagLab.text = @"姓名:";
    [self.contentView addSubview:nameTagLab];
    self.nameTagLab = nameTagLab;
    CGSize size = [nameTagLab.text getStringSizeWithFont:nameTagLab.font maxWidth:120];
    [nameTagLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(89);
        make.height.mas_equalTo(nameTagLab.font.pointSize);
        make.width.mas_equalTo(ceil(size.width));
    }];
    
    UILabel *nameLab = [UILabel by_init];
    nameLab.font = [UIFont systemFontOfSize:13];
    nameLab.textColor = kColorRGBValue(0x8f8f8f);
    nameLab.numberOfLines = 0;
    [self.contentView addSubview:nameLab];
    self.nameLab = nameLab;
    [nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameTagLab.mas_right).offset(5);
        make.top.mas_equalTo(nameTagLab);
        make.height.mas_greaterThanOrEqualTo(0);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    
    UILabel *wxIdTagLab = [UILabel by_init];
    wxIdTagLab.font = [UIFont boldSystemFontOfSize:13];
    wxIdTagLab.textColor = kColorRGBValue(0x323232);
    wxIdTagLab.text = @"微信:";
    [self.contentView addSubview:wxIdTagLab];
    self.wxIdTagLab = wxIdTagLab;
    [wxIdTagLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCommonScreenWidth/2 + 5);
        make.top.mas_equalTo(88);
        make.height.mas_equalTo(wxIdTagLab.font.pointSize);
        make.width.mas_equalTo(ceil(size.width));
    }];
    
    UILabel *wxIdLab = [UILabel by_init];
    wxIdLab.font = [UIFont systemFontOfSize:13];
    wxIdLab.textColor = kColorRGBValue(0x8f8f8f);
    wxIdLab.numberOfLines = 0;
    [self.contentView addSubview:wxIdLab];
    self.wxIdLab = wxIdLab;
    [wxIdLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wxIdTagLab.mas_right).offset(5);
        make.top.mas_equalTo(wxIdTagLab);
        make.height.mas_greaterThanOrEqualTo(0);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    UILabel *telPhoneTagLab = [UILabel by_init];
    telPhoneTagLab.font = [UIFont boldSystemFontOfSize:13];
    telPhoneTagLab.textColor = kColorRGBValue(0x323232);
    telPhoneTagLab.text = @"电话:";
    [self.contentView addSubview:telPhoneTagLab];
    self.telPhoneTagLab = telPhoneTagLab;
    [telPhoneTagLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(nameTagLab.mas_bottom).offset(12);
        make.height.mas_equalTo(telPhoneTagLab.font.pointSize);
        make.width.mas_equalTo(ceil(size.width));
    }];
    
    UILabel *telPhoneLab = [UILabel by_init];
    telPhoneLab.font = [UIFont systemFontOfSize:13];
    telPhoneLab.textColor = kColorRGBValue(0x8f8f8f);
    telPhoneLab.numberOfLines = 0;
    [self.contentView addSubview:telPhoneLab];
    self.telPhoneLab = telPhoneLab;
    [telPhoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(telPhoneTagLab.mas_right).offset(5);
        make.top.mas_equalTo(telPhoneTagLab);
        make.height.mas_greaterThanOrEqualTo(0);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    UILabel *emailTagLab = [UILabel by_init];
    emailTagLab.font = [UIFont boldSystemFontOfSize:13];
    emailTagLab.textColor = kColorRGBValue(0x323232);
    emailTagLab.text = @"邮箱:";
    [self.contentView addSubview:emailTagLab];
    self.emailTagLab = emailTagLab;
    [emailTagLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCommonScreenWidth/2 + 5);
        make.top.mas_equalTo(wxIdTagLab.mas_bottom).offset(12);
        make.height.mas_equalTo(emailTagLab.font.pointSize);
        make.width.mas_equalTo(ceil(size.width));
    }];
    
    UILabel *emailLab = [UILabel by_init];
    emailLab.font = [UIFont systemFontOfSize:13];
    emailLab.textColor = kColorRGBValue(0x8f8f8f);
    emailLab.numberOfLines = 0;
    [self.contentView addSubview:emailLab];
    self.emailLab = emailLab;
    [emailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(emailTagLab.mas_right).offset(5);
        make.top.mas_equalTo(emailTagLab);
        make.height.mas_greaterThanOrEqualTo(0);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
}

@end
