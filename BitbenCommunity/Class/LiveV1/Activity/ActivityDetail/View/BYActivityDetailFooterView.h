//
//  BYActivityDetailFooterView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYActivityDetailFooterView : UIView

/** watchBtnHandle */
@property (nonatomic ,copy) void (^watchBtnActionHandle)(void);
/** otherBtnHandle */
@property (nonatomic ,copy) void (^otherBtnActionHandle)(void);


- (void)reloadData:(BYActivityModel *)model;
@end

NS_ASSUME_NONNULL_END
