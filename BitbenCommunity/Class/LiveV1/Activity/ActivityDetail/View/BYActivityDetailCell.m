//
//  BYActivityDetailCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityDetailCell.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>
#import "BYActivityDetailModel.h"

static NSInteger kBaseTag = 584;

@interface BYActivityDetailCell ()

/** 直播封面 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 直播标题 */
@property (nonatomic ,strong) YYLabel *titleLab;
/** 通过数 */
@property (nonatomic ,strong) UILabel *passNumLab;
/** 费用 */
@property (nonatomic ,strong) UILabel *costLab;
/** 直播开始时间 */
@property (nonatomic ,strong) UILabel *timeLab;
/** 地点 */
@property (nonatomic ,strong) UILabel *locationLab;
/** model */
@property (nonatomic ,strong) BYActivityDetailModel *model;
/** 直播中动画 */
//@property (nonatomic ,strong) FLAnimatedImageView *livIngImgView;
/** 审核视图 */
@property (nonatomic ,strong) UIView *reviewView;
/** 审核title */
@property (nonatomic ,strong) UILabel *reviewLab;
/** infoBtn */
@property (nonatomic ,strong) UIButton *infoBtn;

@end

@implementation BYActivityDetailCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYActivityDetailModel *model = dic.allValues[0][indexPath.row];
   
    self.model = model;
    self.indexPath = indexPath;

    self.reviewView.hidden = [model.user_id isEqualToString:ACCOUNT_ID] ? NO : YES;
//    if (![model.user_id isEqualToString:[AccountModel sharedAccountModel].account_id]) {
//        for (int i = 0; i < 2; i ++) {
//            UIButton *btn = [self viewWithTag:2*kBaseTag + i];
//            btn.hidden = YES;
//        }
//    }else{
//        UIButton *btn = [self viewWithTag:2*kBaseTag + 1];
//        if (model.status == BY_LIVE_STATUS_REVIEW_FAIL ||
//            model.status == BY_LIVE_STATUS_REVIEW_WAITING) {
//            self.infoBtn.hidden = NO;
//            btn.hidden = NO;
//        }else{
//            self.infoBtn.hidden = YES;
//            btn.hidden = YES;
//        }
//    }

    [_coverImgView uploadHDImageWithURL:model.activity_cover_url callback:nil];

    NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.activity_title)];
    messageAttributed.yy_lineSpacing = 8.0f;
    messageAttributed.yy_font = [UIFont boldSystemFontOfSize:15];
    messageAttributed.yy_color = kColorRGBValue(0x323232);
    YYTextContainer *textContainer = [YYTextContainer new];
    textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
    YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
    [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(ceil(layout.textBoundingSize.height));
    }];
    self.titleLab.attributedText = messageAttributed;
    
    NSString *maxCap = model.limit_number == 0 ? @"无限制" : stringFormatInteger(model.limit_number);
    self.passNumLab.text = [NSString stringWithFormat:@"已通过： %i/%@",(int)model.pass_number,maxCap];

    if (model.pay_fee == 0) {
        self.costLab.text = @"免费";
        self.costLab.font = [UIFont systemFontOfSize:14];
        [self.costLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(self.costLab.font.pointSize);
        }];
    }else{
        self.costLab.text = [NSString stringWithFormat:@"¥ %i",(int)model.pay_fee];
    }
    
    [self verifyStatus];
    
    self.locationLab.text = nullToEmpty(model.address);
    
    self.timeLab.text = [NSString stringWithFormat:@"%@ - %@",model.begin_time,model.end_time];
    [self.timeLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(self.timeLab.text, self.timeLab.font.pointSize));
    }];
}

- (void)verifyStatus{
    switch (self.model.status) {
        case BY_ACTIVITY_STATUS_OVERTIME:
            self.reviewLab.text = @"审核已超时";
            _reviewLab.textColor = kColorRGBValue(0xea6438);
            break;
        case BY_ACTIVITY_STATUS_REVIEW_FAIL:
            _reviewLab.text = @"审核失败";
            _reviewLab.textColor = kColorRGBValue(0xea6438);
            break;
        case BY_ACTIVITY_STATUS_REVIEW_PASS:
            _reviewLab.text = @"审核已通过";
            _reviewLab.textColor = kColorRGBValue(0x6384ff);
            break;
        case BY_ACTIVITY_STATUS_REVIEW_WAITING:
            _reviewLab.text = @"审核中";
            _reviewLab.textColor = kColorRGBValue(0xea6438);
            break;
        default:
            _reviewLab.text = @"审核已通过";
            _reviewLab.textColor = kColorRGBValue(0xea6438);
            break;
    }
   
    
    if (![self.model.user_id isEqualToString:[AccountModel sharedAccountModel].account_id]) {
        for (int i = 0; i < 2; i ++) {
            UIButton *btn = [self viewWithTag:2*kBaseTag + i];
            btn.hidden = YES;
        }
    }else{
        UIButton *btn = [self viewWithTag:2*kBaseTag + 1]; // 编辑
        UIButton *btn1 = [self viewWithTag:2*kBaseTag]; // 删除
        if (self.model.status == BY_ACTIVITY_STATUS_REVIEW_WAITING ||
            self.model.status == BY_ACTIVITY_STATUS_REVIEW_FAIL) {
            self.infoBtn.hidden = NO;
            btn.hidden = NO;
        }else{
            self.infoBtn.hidden = YES;
            btn.hidden = YES;
            btn1.hidden = self.model.pass_number > 0 ? YES : NO;
        }
    }
    
}

#pragma mark - action
- (void)buttonAction:(UIButton *)sender{
    NSInteger i = sender.tag - 2*kBaseTag;
    if (i == 0) { // 删除
        [self sendActionName:@"delAction" param:nil indexPath:self.indexPath];
    }else{ // 编辑
        [self sendActionName:@"editAction" param:nil indexPath:self.indexPath];
    }
}

- (void)infoBtnAction{
    [self sendActionName:@"infoBtnAction" param:nil indexPath:self.indexPath];
}

- (void)locationBtnAction{
    [self sendActionName:@"locationBtnAction" param:nil indexPath:self.indexPath];
}

#pragma mark - configUI

- (void)setContentView{
    
    // 直播封面图
    self.coverImgView = [[PDImageView alloc] init];
    [self.contentView addSubview:self.coverImgView];
    CGFloat coverH = (kCommonScreenWidth - 30)*180/345;
    [self.coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(coverH);
    }];
    
    // 直播标题
    self.titleLab = [YYLabel by_init];
    self.titleLab.font = [UIFont systemFontOfSize:15];
    self.titleLab.textColor = kColorRGBValue(0x323232);
    self.titleLab.numberOfLines = 2.0f;
    [self.contentView addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.coverImgView.mas_bottom).mas_offset(15);
        make.height.mas_greaterThanOrEqualTo(0);
        make.right.mas_equalTo(-15);
    }];
    
    self.passNumLab = [UILabel by_init];
    [self.passNumLab setBy_font:13];
    self.passNumLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:self.passNumLab];
    [self.passNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.titleLab.mas_bottom).offset(13);
        make.height.mas_equalTo(self.passNumLab.font.pointSize);
        make.width.mas_equalTo(150);
    }];
    
    self.costLab = [UILabel by_init];
    [self.costLab setBy_font:22];
    self.costLab.textColor = kColorRGBValue(0xeb0f0f);
    [self.contentView addSubview:self.costLab];
    [self.costLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.passNumLab.mas_bottom).offset(19);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(self.costLab.font.pointSize);
    }];
    
    UIImageView *timeIcon = [[UIImageView alloc] init];
    [timeIcon by_setImageName:@"common_time_black"];
    [self.contentView addSubview:timeIcon];
    [timeIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.costLab.mas_bottom).offset(18);
        make.width.mas_equalTo(14);
        make.height.mas_equalTo(14);
    }];
    
    self.timeLab = [UILabel by_init];
    [self.timeLab setBy_font:14];
    self.timeLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:self.timeLab];
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(timeIcon.mas_right).offset(10);
        make.centerY.mas_equalTo(timeIcon);
        make.height.mas_equalTo(self.timeLab.font.pointSize);
        make.right.mas_equalTo(0);
    }];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(timeIcon.mas_bottom).offset(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(0.5);
    }];
    
    UIButton *locationBtn = [UIButton by_buttonWithCustomType];
    [locationBtn addTarget:self action:@selector(locationBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:locationBtn];
    [locationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.top.mas_equalTo(lineView.mas_bottom);
    }];
    
    UIImageView *locationIcon = [[UIImageView alloc] init];
    [locationIcon by_setImageName:@"common_location"];
    [self.contentView addSubview:locationIcon];
    [locationIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(lineView.mas_bottom).offset(17);
        make.width.mas_equalTo(14);
        make.height.mas_equalTo(16);
    }];
    
    self.locationLab = [UILabel by_init];
    [self.locationLab setBy_font:14];
    self.locationLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:self.locationLab];
    [self.locationLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(locationIcon.mas_right).offset(10);
        make.centerY.mas_equalTo(locationIcon);
        make.height.mas_equalTo(self.locationLab.font.pointSize);
        make.right.mas_equalTo(-30);
    }];
    
    UIImageView *rightIcon = [[UIImageView alloc] init];
    [rightIcon by_setImageName:@"livehome_rightIcon"];
    [self.contentView addSubview:rightIcon];
    [rightIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(7);
        make.height.mas_equalTo(13);
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(self.locationLab);
    }];
    
    self.reviewView = [UIView by_init];
    [self.contentView addSubview:self.reviewView];
    [self.reviewView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(self.passNumLab);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(72);
    }];
    
    self.reviewLab = [UILabel by_init];
    [self.reviewLab setBy_font:13];
    self.reviewLab.textColor = kColorRGBValue(0xea6438);
    [self.reviewView addSubview:self.reviewLab];
    [self.reviewLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.centerY.mas_equalTo(0);
        make.height.mas_equalTo(self.reviewLab.font.pointSize);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    self.infoBtn = [UIButton by_buttonWithCustomType];
    [self.infoBtn setBy_imageName:@"newlive_info" forState:UIControlStateNormal];
    [self.infoBtn addTarget:self action:@selector(infoBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.reviewView addSubview:self.infoBtn];
    [self.infoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.reviewLab.mas_right).offset(5);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(20);
    }];
    
    NSArray *images = @[@"newlive_delete",@"newlive_edit"];
    for (int i = 0; i < 2; i ++) {
        UIButton *button = [UIButton by_buttonWithCustomType];
        [button setBy_imageName:images[i] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = 2*kBaseTag + i;
        [self.contentView addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.passNumLab);
            make.right.mas_equalTo(-40*i);
            make.width.height.mas_equalTo(40);
        }];
    }
}
@end
