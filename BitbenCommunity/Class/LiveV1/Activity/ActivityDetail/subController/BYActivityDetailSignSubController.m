//
//  BYActivityDetailSignSubController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityDetailSignSubController.h"

#import "BYActivityReviewView.h"

#import "BYActivityDetailSignModel.h"

@interface BYActivityDetailSignSubController ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;
/** model */
@property (nonatomic ,strong) BYActivityModel *model;
/** 审核view */
@property (nonatomic ,strong) BYActivityReviewView *reviewView;


@end

@implementation BYActivityDetailSignSubController

- (void)viewDidLoad {
    self.pageNum = 0;
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (void)reloadData:(BYActivityModel *)model{
    self.model = model;
    [self reloadData];
}

- (void)reloadData{
    self.pageNum = 0;
    [self loadRequest];
}

- (void)loadMoreData{
    [self loadRequest];
}

- (void)showReviewView:(CGPoint)point model:(BYActivityDetailSignModel *)model indexPath:(NSIndexPath *)indexPath{
    self.reviewView.activity_enroll_id = model.activity_enroll_id;
    @weakify(self);
    [self.reviewView showAnimationWithPoint:point cb:^(BY_ACTIVITY_STATUS status, BOOL isSuccess) {
        @strongify(self);
        if (!isSuccess) return ;
        if (self.didSignHandle) self.didSignHandle();
        BYActivityDetailSignModel *model = self.tableView.tableData[indexPath.row];
        model.enroll_status = status;
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"moreAction"]) { // 更多按钮
        BYActivityDetailSignModel *model = tableView.tableData[indexPath.row];
        NSValue *value = param[@"point"];
        CGPoint point = [value CGPointValue];
        [self showReviewView:point model:model indexPath:indexPath];
    }
}

#pragma mark - request
- (void)loadRequest{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestActivityEnroll:self.model.activity_id pageNum:self.pageNum successBlock:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        [self.tableView removeEmptyView];
        if (self.pageNum == 0) {
            self.tableView.tableData = object;
            if (![object count]) {
                [self.tableView addEmptyView:@"暂无内容"];
                return ;
            }
        }else{
            NSMutableArray *data = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [data addObjectsFromArray:object];
            self.tableView.tableData = [data copy];
        }
        self.pageNum ++;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

#pragma mark - configUI

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        [_tableView addHeaderRefreshTarget:self action:@selector(reloadData)];
        [_tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    }
    return _tableView;
}

- (BYActivityReviewView *)reviewView{
    if (!_reviewView) {
        _reviewView = [BYActivityReviewView initReviewViewShowInView:kCommonWindow];
        
    }
    return _reviewView;
}

@end
