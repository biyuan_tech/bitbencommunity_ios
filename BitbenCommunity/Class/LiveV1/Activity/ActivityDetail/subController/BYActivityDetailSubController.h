//
//  BYActivityDetailSubController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "HGPageViewController.h"
#import "BYActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYActivityDetailSubController : HGPageViewController

- (void)reloadData:(BYActivityModel *)model;

@end

NS_ASSUME_NONNULL_END
