//
//  BYActivityDetailSubController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityDetailSubController.h"
#import "MsgPicViewController.h"
#import "BYCreateActivityController.h"

#import "BYActivityDetailFooterView.h"

#import "BYLiveDetailModel.h"
#import "BYActivityDetailModel.h"

#import <MapKit/MapKit.h>
@interface BYActivityDetailSubController ()<BYCommonTableViewDelegate>


/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** footerView */
@property (nonatomic ,strong) BYActivityDetailFooterView *footerView;
/** model */
@property (nonatomic ,strong) BYActivityModel *model;

@end

@implementation BYActivityDetailSubController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTableView];
    [self addFooterView];
    // Do any additional setup after loading the view.
}

- (void)reloadData:(BYActivityModel *)model{
    self.model = model;
    NSArray *tableData = [BYActivityDetailModel getTableData:model];
    self.tableView.tableData = tableData;
    [self reloadSubViewLayout];
}

- (void)reloadSubViewLayout{
    BOOL footerHidden = NO;
    if ([self.model.user_id isEqualToString:ACCOUNT_ID]) { // 创建者
        if (self.model.activity_type == BY_NEWLIVE_TYPE_OTHER) { // 未同步直播
            footerHidden = YES;
        }
    }
    
    self.footerView.hidden = footerHidden;
    [self.footerView reloadData:self.model];
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(footerHidden ? 0 : -kSafe_Mas_Bottom(65));
    }];
}

#pragma mark - action

- (void)infoBtnAction{
    NSString *title = self.model.status == BY_ACTIVITY_STATUS_REVIEW_WAITING ? @"审核提示" : @"审核未通过提示";
    NSString *message;;
    if (self.model.status == BY_ACTIVITY_STATUS_REVIEW_WAITING) {
        message = [NSString stringWithFormat:@"%@“%@”",review_waiting,self.model.assistant_wechat];
    }else{
        message = [NSString stringWithFormat:@"审核失败原因：%@。有疑问请添加微信“%@”",self.model.remark,self.model.assistant_wechat];
    }
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleCancel handler:nil];
    
    [sheetController addAction:cancelAction];
    [CURRENT_VC.navigationController presentViewController:sheetController animated:YES completion:nil];
}

- (void)attentionBtnAction:(NSIndexPath *)indexPath{
    NSDictionary *dic = self.tableView.tableData[indexPath.section];
    BYLiveDetailUserModel *model = dic.allValues[0][indexPath.row];
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:model.user_id
                                             isAttention:!model.isAttention
                                            successBlock:^(id object) {
                                                @strongify(self);
                                                model.isAttention = !model.isAttention;
                                                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
                                            } faileBlock:nil];
}

- (void)openMapWithLocation:(NSString *)location{
    [BYCommonTool openMapWithLocation:location];
}

#pragma mark - BYCommonTableViewDelegate
- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForFooterInSection:(NSInteger)section{
    UIView *sectonView = [UIView by_init];
    [sectonView setBackgroundColor:[UIColor clearColor]];
    return sectonView;
}

- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForFooterInSection:(NSInteger)section{
    return 10;
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"delAction"]) { // 删除
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYActivityDetailModel *model = dic.allValues[0][indexPath.row];
        UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:@"确认删除此条会议吗？" message:@"删除后将无法恢复" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self loadRequestDelLive:model.activity_id];
        }];
        
        [sheetController addAction:confirmAction];
        [sheetController addAction:cancelAction];
        [CURRENT_VC.navigationController presentViewController:sheetController animated:YES completion:nil];
    }else if ([actionName isEqualToString:@"editAction"]) { // 编辑
        BYCreateActivityController *createActivityController = [[BYCreateActivityController alloc] init];
        createActivityController.isEdit = YES;
        createActivityController.model = self.model;
        [CURRENT_VC.navigationController pushViewController:createActivityController animated:YES];
      
    }else if ([actionName isEqualToString:@"imageViewAction"]) { // 封面图点击放大
        UIImageView *imageView = param[@"imageView"];
        if (!imageView.image) return;
        CGRect newe =  [imageView.superview convertRect:imageView.frame toView:kCommonWindow];
        [MsgPicViewController addToRootViewController:imageView.image ofMsgId:@"" in:newe from:@[]];
    }else if ([actionName isEqualToString:@"attentionAction"]) {
        if (![[AccountModel sharedAccountModel] hasLoggedIn]){
            [(BYCommonViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
        }
        else {
            [self attentionBtnAction:indexPath];
        }
    }else if ([actionName isEqualToString:@"infoBtnAction"]) {
        [self infoBtnAction];
    }else if ([actionName isEqualToString:@"locationBtnAction"]) {
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYActivityDetailModel *model = dic.allValues[0][indexPath.row];
        [self openMapWithLocation:model.address];
    }
}

#pragma mark - request
- (void)loadRequestDelLive:(NSString *)activity_id{
    [[BYLiveHomeRequest alloc] loadRequestDeleteActivity:activity_id successBlock:^(id object) {
        [CURRENT_VC.navigationController popViewControllerAnimated:YES];
        showToastView(@"删除成功", kCommonWindow);
    } faileBlock:^(NSError *error) {
        showToastView(@"删除失败", kCommonWindow);
    }];
}

#pragma mark - configUI
- (void)addTableView{
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (void)addFooterView{
    [self.view addSubview:self.footerView];
    [self.footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(65));
    }];
}

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        [_tableView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    }
    return _tableView;
}

- (BYActivityDetailFooterView *)footerView{
    if (!_footerView) {
        _footerView = [[BYActivityDetailFooterView alloc] init];
        _footerView.hidden = YES;
    }
    return _footerView;
}

@end
