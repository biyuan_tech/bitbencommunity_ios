//
//  BYActivityDetailController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityDetailController.h"
#import "BYActivityDetailViewModel.h"
#import "BYEditLiveWxController.h"

@interface BYActivityDetailController ()

@end

@implementation BYActivityDetailController

- (Class)getViewModelClass{
    return [BYActivityDetailViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"会议详情";
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [array enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[BYEditLiveWxController class]]) {
            [self.rt_navigationController removeViewController:obj];
        }
    }];
}

@end
