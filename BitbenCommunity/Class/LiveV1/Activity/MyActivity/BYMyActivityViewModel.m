//
//  BYMyActivityViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYMyActivityViewModel.h"
#import "BYMyActivitySubController.h"

#import "HGSegmentedPageViewController.h"
#import "HGCenterBaseTableView.h"

@interface BYMyActivityViewModel ()<HGSegmentedPageViewControllerDelegate, HGPageViewControllerDelegate,UITableViewDelegate,UITableViewDataSource>

/** tableView */
@property (nonatomic ,strong) HGCenterBaseTableView *tableView;
/** segmentControllView */
@property (nonatomic ,strong) HGSegmentedPageViewController *segmentedPageViewController;
/** 是否可滑动 */
@property (nonatomic ,assign) BOOL cannotScroll;
/** tableFooterView */
@property (nonatomic ,strong) UIView *tableFooterView;
/** 有效票 */
@property (nonatomic ,strong) BYMyActivitySubController *effectiveController;
/** 未完成 */
@property (nonatomic ,strong) BYMyActivitySubController *unfinishController;
/** 已结束 */
@property (nonatomic ,strong) BYMyActivitySubController *finishController;
/** 我管理的 */
@property (nonatomic ,strong) BYMyActivitySubController *manageController;
@end

@implementation BYMyActivityViewModel

- (void)setContentView{
    [self addTableView];

}

#pragma mark - BYCommonTableViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat contentOffsetY = scrollView.contentOffset.y;
    CGFloat criticalPointOffsetY = 0;
    
    //利用contentOffset处理内外层scrollView的滑动冲突问题
    if (contentOffsetY >= criticalPointOffsetY) {
        
        self.cannotScroll = YES;
        scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        [self.segmentedPageViewController.currentPageViewController makePageViewControllerScroll:YES];
    } else {
        
        if (self.cannotScroll) {
            //“维持吸顶状态”
            scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        } else {
            /* 吸顶状态 -> 不吸顶状态
             * categoryView的子控制器的tableView或collectionView在竖直方向上的contentOffsetY小于等于0时，会通过代理的方式改变当前控制器self.canScroll的值；
             */
        }
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    [self.segmentedPageViewController.currentPageViewController makePageViewControllerScrollToTop];
    return YES;
}

#pragma mark - HGSegmentedPageViewControllerDelegate
- (void)segmentedPageViewControllerWillBeginDragging {
    self.tableView.scrollEnabled = NO;
}

- (void)segmentedPageViewControllerDidEndDragging {
    self.tableView.scrollEnabled = YES;
}

#pragma mark - HGPageViewControllerDelegate
- (void)pageViewControllerLeaveTop {
    self.cannotScroll = NO;
}

#pragma mark - configUI

- (void)addTableView{
    HGCenterBaseTableView *tableView = [[HGCenterBaseTableView alloc] init];
    tableView.categoryViewHeight = 34;
    tableView.bounces = NO;
    tableView.delegate = self;
    tableView.tableFooterView = self.tableFooterView;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.showsHorizontalScrollIndicator = NO;
    [S_V_VIEW addSubview:tableView];
    [S_V_VIEW sendSubviewToBack:tableView];
    self.tableView = tableView;
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (UIView *)tableFooterView{
    if (!_tableFooterView) {
        _tableFooterView = [[UIView alloc] init];
        _tableFooterView.frame = CGRectMake(0, 0, kCommonScreenWidth, kCommonScreenHeight - kNavigationHeight);
        [S_VC addChildViewController:self.segmentedPageViewController];
        [_tableFooterView addSubview:self.segmentedPageViewController.view];
        [self.segmentedPageViewController didMoveToParentViewController:S_VC];
        @weakify(self);
        [self.segmentedPageViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.edges.equalTo(self.tableFooterView);
        }];
    }
    return _tableFooterView;
}

- (HGSegmentedPageViewController *)segmentedPageViewController {
    if (!_segmentedPageViewController) {
        NSMutableArray *controllers = [NSMutableArray array];
        NSArray *titles = @[@"有效票", @"未完成",@"已结束",@"我管理的"];
        for (int i = 0; i < titles.count; i++) {
            BYMyActivitySubController *controller;
            if (i == 0) {
                controller = self.effectiveController;
            } else if (i == 1) {
                controller = self.unfinishController;
            } else if (i == 2) {
                controller = self.finishController;
            } else if (i == 3) {
                controller = self.manageController;
            }
            controller.delegate = self;
            [controllers addObject:controller];
        }
        _segmentedPageViewController = [[HGSegmentedPageViewController alloc] init];
        _segmentedPageViewController.pageViewControllers = controllers;
        _segmentedPageViewController.categoryView.titles = titles;
        _segmentedPageViewController.categoryView.height = 34;
        _segmentedPageViewController.categoryView.alignment = HGCategoryViewAlignmentLeft;
        _segmentedPageViewController.categoryView.originalIndex = 0;
        if (iPhone5) {
            _segmentedPageViewController.categoryView.leftAndRightMargin = 15.0f;
            _segmentedPageViewController.categoryView.itemSpacing = 30;
        }else{
            _segmentedPageViewController.categoryView.isEqualParts = YES;
        }

        _segmentedPageViewController.categoryView.titleNomalFont = [UIFont systemFontOfSize:15];
        _segmentedPageViewController.categoryView.titleSelectedFont = [UIFont boldSystemFontOfSize:15];
        _segmentedPageViewController.categoryView.titleNormalColor = kColorRGBValue(0x8f8f8f);
        _segmentedPageViewController.categoryView.titleSelectedColor = kColorRGBValue(0x323232);
        _segmentedPageViewController.categoryView.backgroundColor = [UIColor whiteColor];
        _segmentedPageViewController.categoryView.vernierHeight = 3;
        _segmentedPageViewController.categoryView.vernierWidth = 20;
        _segmentedPageViewController.categoryView.vernierBottomSapce = 0;
        _segmentedPageViewController.categoryView.vernier.backgroundColor = kColorRGBValue(0xea6441);
        _segmentedPageViewController.categoryView.topBorder.hidden = YES;
        _segmentedPageViewController.categoryView.bottomBorder.backgroundColor = kColorRGBValue(0xe7e7ea);
//        _segmentedPageViewController.categoryView.bottomBorder.hidden = YES;
        _segmentedPageViewController.delegate = self;
    }
    return _segmentedPageViewController;
}

- (BYMyActivitySubController *)effectiveController{
    if (!_effectiveController) {
        _effectiveController = [[BYMyActivitySubController alloc] init];
    }
    return _effectiveController;
}

- (BYMyActivitySubController *)unfinishController{
    if (!_unfinishController) {
        _unfinishController = [[BYMyActivitySubController alloc] init];
    }
    return _unfinishController;
}

- (BYMyActivitySubController *)finishController{
    if (!_finishController) {
        _finishController = [[BYMyActivitySubController alloc] init];
    }
    return _finishController;
}

- (BYMyActivitySubController *)manageController{
    if (!_manageController) {
        _manageController = [[BYMyActivitySubController alloc] init];
    }
    return _manageController;
}

@end
