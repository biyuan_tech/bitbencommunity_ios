//
//  BYMyActivityManagerCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/26.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYMyActivityManagerCell.h"

@interface BYMyActivityManagerCell ()

/** 待审核数 */
@property (nonatomic ,strong) UILabel *watiAuditNumLab;


@end

@implementation BYMyActivityManagerCell

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    [super setContentWithObject:object indexPath:indexPath];
    
    self.watiAuditNumLab.hidden = self.model.status == BY_ACTIVITY_STATUS_REVIEW_PASS ? NO : YES;
    self.watiAuditNumLab.text = [NSString stringWithFormat:@"%i人待审核",(int)self.model.enroll_pending_number];
}

#pragma mark - configUI

- (void)setContentView{
    [super setContentView];
    
    [self.contentView addSubview:self.watiAuditNumLab];
    [self.watiAuditNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(self.watiAuditNumLab.font.pointSize);
        make.centerY.mas_equalTo(self.costLab);
        make.left.mas_equalTo(self.costLab.mas_right).offset(15);
    }];
}

- (UILabel *)watiAuditNumLab{
    if (!_watiAuditNumLab) {
        _watiAuditNumLab = [UILabel by_init];
        _watiAuditNumLab.font = [UIFont systemFontOfSize:12];
        _watiAuditNumLab.textColor = kColorRGBValue(0x828282);
        _watiAuditNumLab.textAlignment = NSTextAlignmentRight;
        _watiAuditNumLab.hidden = YES;
    }
    return _watiAuditNumLab;
}

@end
