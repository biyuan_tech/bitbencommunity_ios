//
//  BYMyActivityEffectiveCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/26.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYMyActivityEffectiveCell.h"

@interface BYMyActivityEffectiveCell ()

/** 入场券 */
@property (nonatomic ,strong) UIButton *ticketBtn;


@end

@implementation BYMyActivityEffectiveCell


- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    [super setContentWithObject:object indexPath:indexPath];
    
    self.ticketBtn.hidden = self.model.enroll_status == BY_ACTIVITY_STATUS_REVIEW_PASS ? NO : YES;
}

- (void)verifyStatus{
    switch (self.model.enroll_status) {
        case BY_ACTIVITY_STATUS_OVERTIME:
            self.statusLab.text = @"已超时";
            self.statusLab.backgroundColor = kColorRGBValue(0xaaaaaa);
            break;
        case BY_ACTIVITY_STATUS_REVIEW_FAIL:
            self.statusLab.text = @"被驳回";
            self.statusLab.backgroundColor = kColorRGBValue(0xeb0f0f);
            break;
        case BY_ACTIVITY_STATUS_REVIEW_PASS:
            self.statusLab.text = @"有效票";
            self.statusLab.backgroundColor = kColorRGBValue(0x10d041);
            break;
        case BY_ACTIVITY_STATUS_REVIEW_WAITING:
            self.statusLab.text = @"待审核";
            self.statusLab.backgroundColor = kColorRGBValue(0x1a86ff);
            break;
        default:
            self.statusLab.text = @"已结束";
            self.statusLab.backgroundColor = kColorRGBValue(0xaaaaaa);
            break;
    }
}

- (void)ticketBtnAction{
    [self sendActionName:@"ticketAction" param:nil indexPath:self.indexPath];
}

#pragma mark - configUI

- (void)setContentView{
    [super setContentView];
    [self.contentView addSubview:self.ticketBtn];
    [self.ticketBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.width.mas_equalTo(46);
        make.height.mas_equalTo(19);
        make.centerY.mas_equalTo(self.costLab);
    }];
}

- (UIButton *)ticketBtn{
    if (!_ticketBtn) {
        _ticketBtn = [UIButton by_buttonWithCustomType];
        [_ticketBtn setBy_imageName:@"activity_ticket" forState:UIControlStateNormal];
        [_ticketBtn addTarget:self action:@selector(ticketBtnAction) forControlEvents:UIControlEventTouchUpInside];
        _ticketBtn.hidden = YES;
    }
    return _ticketBtn;
}
@end
