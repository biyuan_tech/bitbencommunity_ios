//
//  BYMyActivitySubController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYMyActivitySubController.h"
#import "BYActivityDetailController.h"
#import "BYActivityTicketController.h"

#import "BYActivityModel.h"

@interface BYMyActivitySubController ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;


@end

@implementation BYMyActivitySubController



- (void)viewDidLoad{
    [super viewDidLoad];
    [self addTableView];
    [self loadRequest];
}

- (void)reloadData{
    self.pageNum = 0;
    [self loadRequest];
}

- (void)loadMoreData{
    [self loadRequest];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BYActivityModel *model = tableView.tableData[indexPath.row];
    BYActivityDetailController *activityDetailController = [[BYActivityDetailController alloc] init];
    activityDetailController.activiry_id = model.activity_id;
    [CURRENT_VC.navigationController pushViewController:activityDetailController animated:YES];
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    BYActivityModel *model = tableView.tableData[indexPath.row];
    if ([actionName isEqualToString:@"ticketAction"]) { // 入场券
        BYActivityTicketController *ticketController = [[BYActivityTicketController alloc] init];
        ticketController.model = model;
        [CURRENT_VC.navigationController pushViewController:ticketController animated:YES];
    }
}

#pragma mark - request

- (void)loadRequest{
    if (self.pageIndex != 3) {
        [self loadRequestGetMyEnroll];
    }else{
        [self loadRequestGetMyActivity];
    }
}

- (void)loadRequestGetMyEnroll{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetMyActivityEnroll:self.pageIndex + 1 pageNum:self.pageNum successBlock:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        [self.tableView removeEmptyView];
        if (self.pageNum == 0) {
            self.tableView.tableData = object;
            if (![object count]) {
                [self.tableView addEmptyView:@"暂无内容"];
                return ;
            }
        }else{
            if (![object count]) return;
            NSMutableArray *data = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [data addObjectsFromArray:object];
            self.tableView.tableData = [data copy];
        }
        self.pageNum ++;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

- (void)loadRequestGetMyActivity{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetMyActivity:self.pageNum successBlock:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        [self.tableView removeEmptyView];
        if (self.pageNum == 0) {
            self.tableView.tableData = object;
            if (![object count]) {
                [self.tableView addEmptyView:@"暂无内容"];
                return ;
            }
        }else{
            if (![object count]) return;
            NSMutableArray *data = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [data addObjectsFromArray:object];
            self.tableView.tableData = [data copy];
        }
        self.pageNum ++;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

#pragma mark - configUI

- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    [self.tableView addHeaderRefreshTarget:self action:@selector(reloadData)];
    [self.tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

@end
