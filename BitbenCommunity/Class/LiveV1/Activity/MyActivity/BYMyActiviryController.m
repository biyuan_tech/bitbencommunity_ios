//
//  BYMyActiviryController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYMyActiviryController.h"
#import "BYMyActivityViewModel.h"

@interface BYMyActiviryController ()

@end

@implementation BYMyActiviryController

- (Class)getViewModelClass{
    return [BYMyActivityViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"我的会议";
    // Do any additional setup after loading the view.
}



@end
