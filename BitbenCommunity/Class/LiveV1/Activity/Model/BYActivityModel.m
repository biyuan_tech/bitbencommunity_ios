//
//  BYActivityModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYActivityModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

@implementation BYActivityEnrollModel


@end

@implementation BYActivityModel

- (NSMutableDictionary *)mj_keyValues{
    return [self mj_keyValuesWithIgnoredKeys:@[@"enrollModel"]];
}

/** 解析活动列表 */
+ (NSArray *)getActivityListData:(NSArray *)respond{
    return [BYActivityModel getActivityCommonData:respond cellString:@"BYLiveActivityCell"];
}

+ (NSArray *)getActivityMyJoinData:(NSArray *)respond{
    return [BYActivityModel getActivityCommonData:respond cellString:@"BYMyActivityEffectiveCell"];
}

/** 解析我管理的活动列表 */
+ (NSArray *)getActivityMyManangerData:(NSArray *)respond{
    return [BYActivityModel getActivityCommonData:respond cellString:@"BYMyActivityManagerCell"];
}

+ (NSArray *)getActivityCommonData:(NSArray *)respond cellString:(NSString *)cellString{
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (!respond.count) {
        return @[];
    }
    __block NSMutableArray *data = [NSMutableArray array];
    [respond enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BYActivityModel *model = [BYActivityModel mj_objectWithKeyValues:obj];
        
        NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.activity_title)];
        messageAttributed.yy_lineSpacing = 6.0f;
        messageAttributed.yy_font = [UIFont boldSystemFontOfSize:15];
        messageAttributed.yy_color = kColorRGBValue(0x323232);
        YYTextContainer *textContainer = [YYTextContainer new];
        textContainer.size = CGSizeMake(kCommonScreenWidth - 157, 42);
        YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
        
        NSDate *date = [NSDate by_dateFromString:model.begin_time dateformatter:K_D_F];
        NSString *time = [NSDate by_stringFromDate:date dateformatter:@"yyyy-MM-dd HH:mm"];
        
        model.begin_time = time;
        
        NSDate *endDate = [NSDate by_dateFromString:model.end_time dateformatter:K_D_F];
        NSString *endTime = [NSDate by_stringFromDate:endDate dateformatter:@"yyyy-MM-dd HH:mm"];
        model.end_time = endTime;
        
        if ([cellString isEqualToString:@"BYMyActivityEffectiveCell"]) {
            model.enrollModel = [BYActivityEnrollModel mj_objectWithKeyValues:obj];
        }
        
        model.cellString = cellString;
        model.cellHeight = 15 + layout.textBoundingSize.height + 90;
        [data addObject:model];
    }];
    return [data copy];
}
@end
