//
//  BYActivityModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonLiveModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYActivityEnrollModel : BYCommonModel

/** 报名id */
@property (nonatomic ,copy) NSString *activity_enroll_id;
/** 活动id */
@property (nonatomic ,copy) NSString *activity_id;
/** 用户id */
@property (nonatomic ,copy) NSString *user_id;
/** 用户头像 */
@property (nonatomic ,copy) NSString *head_img;
/** 用户昵称 */
@property (nonatomic ,copy) NSString *nickname;
/** 姓名 */
@property (nonatomic ,copy) NSString *name;
/** 手机号 */
@property (nonatomic ,copy) NSString *phone;
/** 微信号 */
@property (nonatomic ,copy) NSString *wechat;
/** 邮箱 */
@property (nonatomic ,copy) NSString *email;
/** 报名状态 */
@property (nonatomic ,assign) BY_ACTIVITY_STATUS enroll_status;
/** 创建时间 */
@property (nonatomic ,copy) NSString *create_time;
/** 徽章 */
@property (nonatomic ,assign) NSInteger cert_badge;


@end

@interface BYActivityModel : BYCommonModel

/** 活动ID */
@property (nonatomic ,copy) NSString *activity_id;
/** 直播记录id */
@property (nonatomic ,copy) NSString *live_record_id;
/** 用户id */
@property (nonatomic ,copy) NSString *user_id;
/** 作者的用户ID，和user_id为同一个值 */
@property (nonatomic ,copy) NSString *author_id;
/** 活动主题 */
@property (nonatomic ,copy) NSString *activity_title;
/** 开始时间 */
@property (nonatomic ,copy) NSString *begin_time;
/** 创建时间 */
@property (nonatomic ,copy) NSString *datetime;
/** 活动种类 */
@property (nonatomic ,assign) BY_NEWLIVE_TYPE activity_type;
/** 活动状态 */
@property (nonatomic ,assign) BY_ACTIVITY_STATUS status;
/** 活动简介 */
@property (nonatomic ,copy) NSString *activity_intro;
/** 活动封面图 */
@property (nonatomic ,copy) NSString *activity_cover_url;
/** 活动海报 */
@property (nonatomic ,copy) NSString *ad_img;
/** 场景类型 */
@property (nonatomic ,assign) BY_NEWLIVE_MICRO_TYPE scene_type;
/** 付费金额 */
@property (nonatomic ,assign) NSInteger pay_fee;
/** 人数限制数量 */
@property (nonatomic ,assign) NSInteger limit_number;
/** 报名通过人数数量 */
@property (nonatomic ,assign) NSInteger pass_number;
/** 活动地址 */
@property (nonatomic ,copy) NSString *address;
/** 结束时间 */
@property (nonatomic ,copy) NSString *end_time;
/** 主办方微信号 */
@property (nonatomic ,copy) NSString *user_wechat;
/** 主办方微信图片 */
@property (nonatomic ,copy) NSString *user_wechat_pic;
/** 主持人 */
@property (nonatomic ,strong) NSArray <BYCommonLiveGuestModel *> *host_list;
/** 嘉宾 */
@property (nonatomic ,strong) NSArray <BYCommonLiveGuestModel *> *interviewee_list;

/** 我报名的活动状态 */
@property (nonatomic ,assign) BY_ACTIVITY_STATUS enroll_status;
/** 待审核的人数 */
@property (nonatomic ,assign) NSInteger enroll_pending_number;

/** 提示 */
@property (nonatomic ,copy) NSString *remark;
/** 工作人员微信 */
@property (nonatomic ,copy) NSString *assistant_wechat;

/** enrollModel */
@property (nonatomic ,strong) BYActivityEnrollModel *enrollModel;


///** 解析活动详情数据  */
//+ (NSArray *)getActivityDetailData:(BYActivityModel *)model;

/** 解析活动自身状态的列表 */
+ (NSArray *)getActivityListData:(NSArray *)respond;

/** 解析显示我参加活动状态的列表 */
+ (NSArray *)getActivityMyJoinData:(NSArray *)respond;

/** 解析我管理的活动列表 */
+ (NSArray *)getActivityMyManangerData:(NSArray *)respond;
@end

NS_ASSUME_NONNULL_END
