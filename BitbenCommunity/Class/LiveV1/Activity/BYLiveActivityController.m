//
//  BYLiveActivityController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveActivityController.h"
#import "BYLiveActivityViewModel.h"

@interface BYLiveActivityController ()

@end

@implementation BYLiveActivityController


- (Class)getViewModelClass{
    return [BYLiveActivityViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"会议";
}


@end
