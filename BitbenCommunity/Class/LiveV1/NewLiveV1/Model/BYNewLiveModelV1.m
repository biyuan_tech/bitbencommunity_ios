//
//  BYNewLiveModelV1.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/9.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYNewLiveModelV1.h"

@implementation BYNewLiveModelV1

+ (NSArray *)getTableData{
    NSMutableArray *data = [NSMutableArray array];
    for (int i = 0; i < 2; i ++) {
        BYNewLiveModelV1 *model = [[BYNewLiveModelV1 alloc] init];
        if (i == 0) {
            model.cellString = @"BYNewLiveTitleCell";
            model.cellHeight = 100;
        }else{
            model.cellString = @"BYNewLiveSelLiveCell";
            model.cellHeight = 371;
        }
        [data addObject:model];
    }
    return [data copy];
}

@end
