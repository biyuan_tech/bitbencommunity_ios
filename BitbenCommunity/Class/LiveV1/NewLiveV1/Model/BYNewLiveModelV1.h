//
//  BYNewLiveModelV1.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/9.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYNewLiveModelV1 : BYCommonModel


+ (NSArray *)getTableData;

@end

NS_ASSUME_NONNULL_END
