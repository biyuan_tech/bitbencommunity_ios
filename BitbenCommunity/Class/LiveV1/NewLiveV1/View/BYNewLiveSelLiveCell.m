//
//  BYNewLiveSelLiveCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYNewLiveSelLiveCell.h"

static NSInteger kBaseTag = 0x589;

@interface BYNewLiveSelLiveCell ()

/** selBtn */
@property (nonatomic ,strong) UIButton *selView;

@end

@implementation BYNewLiveSelLiveCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    self.indexPath = indexPath;
}

- (void)buttonAction:(UIButton *)sender{
    if (self.selView == sender) return;
    [self setSelViewSelected:NO btn:self.selView];
    self.selView = sender;
    [self setSelViewSelected:YES btn:self.selView];
    NSInteger index = sender.tag - kBaseTag;
    BY_NEWLIVE_TYPE type;
    switch (index) {
        case 0:
            type = BY_NEWLIVE_TYPE_AUDIO_LECTURE;
            break;
        case 1:
            type = BY_NEWLIVE_TYPE_ILIVE;
            break;
        default:
            type = BY_NEWLIVE_TYPE_VIDEO;
            break;
    } 
    [self sendActionName:@"selLiveAction" param:@{@"type":@(type)} indexPath:self.indexPath];
}

// 选择微直播类型
- (void)selMicroTypeBtnAction:(UIButton *)sender{
    for (int i = 0; i < 2; i ++) {
        UIButton *btn = [self.contentView viewWithTag:4*kBaseTag + i];
        btn.selected = btn == sender ? YES : NO;
    }
    BY_NEWLIVE_MICRO_TYPE micro_type = sender.tag - 4*kBaseTag == 0 ? BY_NEWLIVE_MICRO_TYPE_WX : BY_NEWLIVE_MICRO_TYPE_APP;
    [self sendActionName:@"selMicroTypeAction" param:@{@"type":@(micro_type)} indexPath:self.indexPath];
}

- (void)infoBtnAction:(UIButton *)sender{
    NSInteger index = sender.superview.tag - kBaseTag;
    BY_NEWLIVE_MICRO_TYPE micro_type = 0;
    BY_NEWLIVE_TYPE live_type = 0;
    if (sender.tag >= 5*kBaseTag) {
        live_type  = BY_NEWLIVE_TYPE_AUDIO_LECTURE;
        if (sender.tag - 5*kBaseTag == 1) {
            micro_type = BY_NEWLIVE_MICRO_TYPE_APP;
        }else{
            micro_type = BY_NEWLIVE_MICRO_TYPE_WX;
        }
    }
    else {
        if (index == 1) {
            live_type  = BY_NEWLIVE_TYPE_ILIVE;
        }else {
            live_type  = BY_NEWLIVE_TYPE_VIDEO;
        }
    }
   
    [self sendActionName:@"infoBtnAction" param:@{@"micro_type":@(micro_type),@"live_type":@(live_type)} indexPath:self.indexPath];
}

- (void)setSelViewSelected:(BOOL)selected btn:(UIButton *)btn{
    UILabel *titleLab = [btn viewWithTag:2*kBaseTag];
    UILabel *introLab = [btn viewWithTag:3*kBaseTag];
    titleLab.textColor = selected ? [UIColor whiteColor] : [UIColor blackColor];
    introLab.textColor = selected ? [UIColor whiteColor] : kColorRGBValue(0x8f8f8f);
    btn.selected = selected;
}

- (void)setContentView{
    for (int i = 0; i < 3; i ++) {
        BY_NEWLIVE_TYPE type;
        switch (i) {
            case 0:
                type = BY_NEWLIVE_TYPE_AUDIO_LECTURE;
                break;
            case 1:
                type = BY_NEWLIVE_TYPE_ILIVE;
                break;
            default:
                type = BY_NEWLIVE_TYPE_VIDEO;
                break;
        }
        UIButton *selView = [self getSingleLieView:type];
        selView.tag = kBaseTag + i;
        BOOL selected = i == 0 ? YES : NO;
        if (i == 0) {
            self.selView = selView;
        }
        [self setSelViewSelected:selected btn:selView];
        [self.contentView addSubview:selView];
        selView.imageView.layer.cornerRadius = 4.0f;
        [selView.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsZero);
        }];
        [selView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            if (i == 0) {
                make.top.mas_equalTo(0);
            }else{
                make.top.mas_equalTo(115*i + 41);
            }
            make.right.mas_equalTo(-15);
            make.height.mas_equalTo(100);
        }];
        
        if (i == 0) {
            UIView *selMicroTypeView = [self getSelMicroLiveTypeView];
            [selMicroTypeView layerCornerRadius:4.0 byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight size:CGSizeMake(kCommonScreenWidth - 30, 43)];
            [self.contentView addSubview:selMicroTypeView];
            [selMicroTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(15);
                make.top.mas_equalTo(selView.mas_bottom).offset(-2);
                make.right.mas_equalTo(-15);
                make.height.mas_equalTo(43);
            }];
        }
        
    }
}

- (UIButton *)getSingleLieView:(BY_NEWLIVE_TYPE)type{
    UIButton *view = [UIButton by_buttonWithCustomType];
    [view setBy_imageName:@"newlive_sel_bg" forState:UIControlStateSelected];
    [view setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [view setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
    [view addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    view.imageView.contentMode = UIViewContentModeScaleAspectFill;
    view.layer.cornerRadius = 4.0f;
    view.clipsToBounds = YES;
    
    NSString *titleStr = @"";
    NSString *introStr = @"";
    switch (type) {
        case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
            titleStr = @"微直播";
            introStr = @"支持图片+文字+语音分享，手机即可完成操作";
            break;
        case BY_NEWLIVE_TYPE_ILIVE:
            titleStr = @"个人手机直播";
            introStr = @"适合个人手机摄像分享，手机即可完成操作";
            break;
        case BY_NEWLIVE_TYPE_VIDEO:
            titleStr = @"推流直播";
            introStr = @"适合屏幕分享、会议、沙龙、峰会的分享";
        default:
            break;
    }
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:18];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = titleStr;
    titleLab.tag = 2*kBaseTag;
    [view addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(25);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.width.mas_equalTo(kCommonScreenWidth/2);
    }];
    
    UILabel *introLab = [UILabel by_init];
    CGFloat fontSize = iPhone5 ? 12 : 14;
    [introLab setBy_font:fontSize];
    introLab.textColor = [UIColor whiteColor];
    introLab.text = introStr;
    introLab.tag = 3*kBaseTag;
    [view addSubview:introLab];
    [introLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(12);
        make.height.mas_equalTo(introLab.font.pointSize);
        make.right.mas_equalTo(0);
    }];
    
    if (type != BY_NEWLIVE_TYPE_AUDIO_LECTURE) {
        UIButton *infoBtn = [UIButton by_buttonWithCustomType];
        [infoBtn setBy_imageName:@"newlive_info" forState:UIControlStateNormal];
        [infoBtn addTarget:self action:@selector(infoBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:infoBtn];
        [infoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(15);
            make.centerY.mas_equalTo(titleLab);
            make.right.mas_equalTo(-20);
        }];
    }

    return view;
}

- (UIView *)getSelMicroLiveTypeView{
    UIView *contentView = [[UIView alloc] init];
    [contentView setBackgroundColor:[UIColor whiteColor]];
    NSArray *titles = @[@"微信群直播",@"币本APP内直播"];
    for (int i = 0; i < 2; i ++) {
        UIButton *selBtn = [UIButton by_buttonWithCustomType];
        [selBtn setBy_imageName:@"newlive_sel_normal" forState:UIControlStateNormal];
        [selBtn setBy_imageName:@"newlive_sel_select" forState:UIControlStateSelected];
        selBtn.selected = i == 0 ? YES : NO;
        selBtn.tag = 4*kBaseTag + i;
        [selBtn addTarget:self action:@selector(selMicroTypeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:selBtn];
        
        UILabel *titleLab = [UILabel by_init];
        [titleLab setBy_font:14];
        titleLab.textColor = kColorRGBValue(0x8f8f8f);
        titleLab.text = titles[i];
        [contentView addSubview:titleLab];
        
        UIButton *infoBtn = [UIButton by_buttonWithCustomType];
        [infoBtn setBy_imageName:@"newlive_info" forState:UIControlStateNormal];
        [infoBtn addTarget:self action:@selector(infoBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        infoBtn.tag = 5*kBaseTag + i;
        [contentView addSubview:infoBtn];
        
        [selBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(15);
            make.top.mas_equalTo(15);
            if (i == 0) {
                make.left.mas_equalTo(20);
            }else{
                make.right.mas_equalTo(-138 - 26);
            }
        }];
        
        [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(stringGetWidth(titles[i], 14));
            make.height.mas_equalTo(titleLab.font.pointSize);
            make.centerY.mas_equalTo(selBtn);
            make.left.mas_equalTo(selBtn.mas_right).offset(8);
        }];
        
        [infoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(15);
            make.centerY.mas_equalTo(selBtn);
            make.left.mas_equalTo(titleLab.mas_right).offset(7);
        }];
    }
    
    return contentView;
}

@end
