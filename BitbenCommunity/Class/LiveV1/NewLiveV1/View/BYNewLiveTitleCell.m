//
//  BYNewLiveTitleCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/9.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYNewLiveTitleCell.h"

@interface BYNewLiveTitleCell ()

@end

@implementation BYNewLiveTitleCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
        [self setContentView];
    }
    return self;
}

- (void)setContentView{
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:21];
    titleLab.textColor = kColorRGBValue(0x000000);
    titleLab.text = @"请选择直播类型";
    [self.contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(30);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.width.mas_equalTo(kCommonScreenWidth/2);
    }];
    
    UILabel *subTitleLab = [UILabel by_init];
    [subTitleLab setBy_font:14];
    subTitleLab.textColor = kColorRGBValue(0x8f8f8f);
    subTitleLab.text = @"请谨慎选择直播类型，创建成功后无法修改";
    [self.contentView addSubview:subTitleLab];
    [subTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(subTitleLab.font.pointSize);
        make.width.mas_equalTo(kCommonScreenWidth);
    }];
}
@end
