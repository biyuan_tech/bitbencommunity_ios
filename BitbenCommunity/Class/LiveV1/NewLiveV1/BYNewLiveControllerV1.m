//
//  BYNewLiveControllerV1.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/9.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYNewLiveControllerV1.h"
#import "BYNewLiveViewModelV1.h"

@interface BYNewLiveControllerV1 ()

@end

@implementation BYNewLiveControllerV1

- (Class)getViewModelClass{
    return [BYNewLiveViewModelV1 class];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:kColorRGBValue(0xf2f4f5)] forBarMetrics:UIBarMetricsDefault];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"新建直播";
    [self.navigationController.navigationBar setTintColor:kColorRGBValue(0xf2f4f5)];
    [self.view setBackgroundColor:kColorRGBValue(0xF2F4F5)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
