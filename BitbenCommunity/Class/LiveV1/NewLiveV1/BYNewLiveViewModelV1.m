//
//  BYNewLiveViewModelV1.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/9.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYNewLiveViewModelV1.h"
#import "BYNewLiveModelV1.h"

#import "BYEditLiveController.h"
#import "BYLiveTypeDesController.h"

@interface BYNewLiveViewModelV1 ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** liveType */
@property (nonatomic ,assign) BY_NEWLIVE_TYPE liveType;
/** 微直播类型 */
@property (nonatomic ,assign) BY_NEWLIVE_MICRO_TYPE scene_type;

@end

@implementation BYNewLiveViewModelV1

- (void)setContentView{
    self.liveType = BY_NEWLIVE_TYPE_AUDIO_LECTURE;
    self.scene_type = BY_NEWLIVE_MICRO_TYPE_WX;
    [self addTableView];
}

- (void)showLiveInfo:(BY_NEWLIVE_MICRO_TYPE)scene_type liveType:(BY_NEWLIVE_TYPE)liveType{
//    NSString *title;
//    NSString *message;
//    if (scene_type == BY_NEWLIVE_MICRO_TYPE_APP) {
//        title = @"币本APP内微直播";
//        message = @"可以在APP内直接进行直播，可以享有更流畅的直播模式及更多的互动方式。直播及回放可通过H5进行传播";
//    }else{
//        title = @"微信群微直播";
//        message = @"可以同步微信群所有聊天记录到APP，方便快捷，微信群直播的不二选择";
//    }
//    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//    }];
//
//    [sheetController addAction:cancelAction];
//    [S_V_NC presentViewController:sheetController animated:YES completion:nil];
    BYLiveTypeDesController *liveDesController = [[BYLiveTypeDesController alloc] init];
    liveDesController.live_type = liveType;
    liveDesController.micro_type = scene_type;
    [S_V_NC pushViewController:liveDesController animated:YES];
}

#pragma mark - action
- (void)buttonAction{
    BYEditLiveController *editLiveController = [[BYEditLiveController alloc] init];
//    editLiveController.navigationTitle = @"微直播";
    switch (self.liveType) {
        case BY_NEWLIVE_TYPE_ILIVE:
            editLiveController.navigationTitle = @"个人手机直播";
            break;
        case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
            editLiveController.navigationTitle = @"微直播";
            break;
        default:
            editLiveController.navigationTitle = @"推流直播";
            break;
    }
    editLiveController.liveType = self.liveType;
    if (self.liveType == BY_NEWLIVE_TYPE_AUDIO_LECTURE) {
        editLiveController.scene_type = self.scene_type;
    }
    [S_V_NC pushViewController:editLiveController animated:YES];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"selLiveAction"]) {
        self.liveType = [param[@"type"] integerValue];
    }else if ([actionName isEqualToString:@"selMicroTypeAction"]) {
        self.scene_type = [param[@"type"] integerValue];
    }else if ([actionName isEqualToString:@"infoBtnAction"]) {
        BY_NEWLIVE_MICRO_TYPE scene_type = [param[@"micro_type"] integerValue];
        BY_NEWLIVE_TYPE live_type = [param[@"live_type"] integerValue];
        [self showLiveInfo:scene_type liveType:live_type];
    }
}

#pragma mark - configUI

- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    [self.tableView setBackgroundColor:kColorRGBValue(0xF2F4F5)];
    self.tableView.group_delegate = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    [S_V_VIEW addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    UIView *footerView = [self getFooterView];
    self.tableView.tableFooterView = footerView;
    
    NSArray *tableData = [BYNewLiveModelV1 getTableData];
    self.tableView.tableData = tableData;
}

- (UIView *)getFooterView{
    UIView *footerView = [UIView by_init];
    footerView.frame = CGRectMake(0, 0, kCommonScreenWidth, 140);
    UIButton *nextBtn = [UIButton by_buttonWithCustomType];
    [nextBtn setBy_attributedTitle:@{@"title":@"下一步",
                                     NSFontAttributeName:[UIFont systemFontOfSize:15],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]
                                     } forState:UIControlStateNormal];
    [nextBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    nextBtn.layer.cornerRadius = 5.0f;
    nextBtn.frame = CGRectMake(15, 43, kCommonScreenWidth - 30, 46);
    [nextBtn addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:nextBtn];
    return footerView;
}

@end
