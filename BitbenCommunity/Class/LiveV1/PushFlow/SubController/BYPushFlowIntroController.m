//
//  BYPushFlowIntroController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPushFlowIntroController.h"

#import "BYLiveDetailModel.h"

@interface BYPushFlowIntroController ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;


@end

@implementation BYPushFlowIntroController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTableView];
    // Do any additional setup after loading the view.
}

- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    self.tableView.tableData = [BYLiveDetailModel getPushFlowIntrolTableData:model];
}

- (void)reloadAttentionStatus:(BOOL)isAttention{
    if (!self.tableView.tableData.count) {
        return;
    }
//    self.model.isAttention = isAttention;
    NSDictionary *dic = self.tableView.tableData[1];
    NSArray *data = dic.allValues[0];
    [data enumerateObjectsUsingBlock:^(BYLiveDetailUserModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[BYLiveDetailUserModel class]]) {
            if ([obj.user_id isEqualToString:self.model.user_id]) {
                obj.isAttention = isAttention;
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
                *stop = YES;
            }
        }
    }];
}

- (void)attentionBtnAction:(NSIndexPath *)indexPath{
    NSDictionary *dic = self.tableView.tableData[indexPath.section];
    BYLiveDetailUserModel *model = dic.allValues[0][indexPath.row];
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:model.user_id
                                             isAttention:!model.isAttention
                                            successBlock:^(id object) {
                                                @strongify(self);
                                                model.isAttention = !model.isAttention;
                                                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
                                                if ([model.user_id isEqualToString:self.model.user_id]) {
                                                    if (self.didUpdateHostAttention) {
                                                        self.didUpdateHostAttention(model.isAttention);
                                                    }
                                                }
                                            } faileBlock:nil];
}

#pragma mark - BYCommonTableViewDelegate

- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForFooterInSection:(NSInteger)section{
    UIView *sectonView = [UIView by_init];
    [sectonView setBackgroundColor:[UIColor clearColor]];
    return sectonView;
}

- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForFooterInSection:(NSInteger)section{
    return 10;
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"attentionAction"]) {
        [self attentionBtnAction:indexPath];
    }
}

#pragma mark - configUI

- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    self.tableView.backgroundColor = kColorRGBValue(0xf2f4f5);
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

@end
