//
//  BYPushFlowIntroController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "HGPageViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYPushFlowIntroController : HGPageViewController

/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 关注了主播状态变更多回调 */
@property (nonatomic ,copy) void (^didUpdateHostAttention)(BOOL isAttention);


- (void)reloadAttentionStatus:(BOOL)isAttention;

@end

NS_ASSUME_NONNULL_END
