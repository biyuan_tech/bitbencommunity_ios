//
//  BYPushFlowChatController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPushFlowChatController.h"
#import "MsgPicViewController.h"
#import "BYPFChatWelcomView.h"
#import "BYPFHostToolBar.h"
#import "BYPFAudienceToolBar.h"
#import "BYPFUserInfoView.h"
#import "BYPFRewardAnimationView.h"
#import "BYNewMsgBottomView.h"

#import "BYPFChatModel.h"

@interface BYPushFlowChatController ()<BYCommonTableViewDelegate>
/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** headerView */
@property (nonatomic ,strong) BYPFChatWelcomView *headerView;
/** toolBar */
@property (nonatomic ,strong) BYPFHostToolBar *hostToolBar;
/** toolBar */
@property (nonatomic ,strong) BYPFAudienceToolBar *audienceToolBar;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;
/** 记录当前发送中的图片信息 */
@property (nonatomic ,strong) NSMutableArray *image_msgs;
/** personView */
@property (nonatomic ,strong) BYPFUserInfoView *personView;
/** 打赏动画 */
@property (nonatomic ,strong) BYPFRewardAnimationView *rewardAnimationView;
/** newmsgBottomView */
@property (nonatomic ,strong) BYNewMsgBottomView *newMsgBottomView;
/** 记录当前未读消息 */
@property (nonatomic ,strong) NSMutableArray *noReadMsgs;

@end

@implementation BYPushFlowChatController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageNum = 0;
    self.image_msgs = [NSMutableArray array];
    self.noReadMsgs = [NSMutableArray array];
    [self addTableView];
    [self addToolBar];
    [self addRewardAnimationView];
}

- (void)setModel:(BYCommonLiveModel *)model{
    BOOL loadRequest = _model ? NO : YES;
    _model = model;
    _hostToolBar.model = model;
    _audienceToolBar.model = model;
    if (model.status == BY_LIVE_STATUS_SOON ||
        model.status == BY_LIVE_STATUS_LIVING) {
        _hostToolBar.enabled = YES;
        _audienceToolBar.enabled = YES;
    }else{
        _hostToolBar.enabled = NO;
        _audienceToolBar.enabled = NO;
    }
    if (loadRequest) {
        [self loadRequestGetHistoryMsg:nil];
    }
}

- (void)reloadSuppertNum{
    [_hostToolBar reloadSuppertNum];
}

- (void)showHostInfo{
    self.personView.user_id = self.model.user_id;
    [self.personView showAnimation];
}

- (void)loadMoreData{
    @weakify(self);
    [self loadRequestGetHistoryMsg:^(NSArray *data) {
        @strongify(self);
        [self.tableView addDataToTop:data];
    }];
}

- (void)addMessageData:(NSArray *)msgs{
    if (!msgs.count) return;
    NSDictionary *dic = msgs[0];
    if (dic) {
        if ([dic[@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
            NSDictionary *customData;
            if ([dic[@"content"] isKindOfClass:[NSString class]]) {
                // 现返回的为纯json字符串，需要转两次
                NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            }
            else{
                customData = dic[@"content"];
            }

            if ([customData[kBYCustomMessageType] integerValue] == BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM) { // 关播消息
//                [self.toolView setNoEdit:YES];
                _hostToolBar.enabled = NO;
                _audienceToolBar.enabled = NO;
            }
            
            if ([dic[@"type"] isEqualToString:msg_open_forbid] ||
                [dic[@"type"] isEqualToString:msg_close_forbid]) {
                NSString *forbid_user_id = dic[@"forbid_user_id"];
                if ([forbid_user_id isEqualToString:ACCOUNT_ID]) {
                    BOOL isForbid = [dic[@"type"] isEqualToString:msg_open_forbid] ? YES : NO;
                    [BYSIMManager shareManager].isUserForbid = isForbid;
                    _audienceToolBar.isForbid = isForbid;
                }
            }
            
            return ;
        }else if ([dic[@"content_type"] integerValue] == BY_SIM_MSG_TYPE_CUSTOME) { // 打赏消息
            NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
            NSMutableDictionary *tmpData = [NSMutableDictionary dictionary];
            [tmpData setDictionary:customData];
            [tmpData setObject:dic[@"nickname"] forKey:@"nickname"];
            [tmpData setObject:dic[@"head_img"] forKey:@"head_img"];
            [self.rewardAnimationView addRewardData:tmpData];
        }
    }
    
    NSArray *data = [BYPFChatModel transTableViewData:msgs liveModel:self.model];
    NSArray *msgData = [self removeSelfSendingMsg:data];
    if (!data.count) return;
    BOOL isFullScreen = _tableView.contentSize.height <= _tableView.frame.size.height ? NO : YES;
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
    [array addObjectsFromArray:msgData];
    BOOL scrollToBottom = _tableView.contentOffset.y >= floor(_tableView.contentSize.height - _tableView.frame.size.height) ? YES : NO;
    scrollToBottom = scrollToBottom || !isFullScreen ? scrollToBottom : NO;
    self.newMsgBottomView.hidden = scrollToBottom;
    self.tableView.tableData = array;
    if (scrollToBottom) {
        [self.tableView scrollToBottom:YES];
    }else{
        [self.noReadMsgs addObjectsFromArray:msgData];
        [self.newMsgBottomView setNewMsgNum:self.noReadMsgs.count];
    }
}

- (NSArray *)removeSelfSendingMsg:(NSArray *)msgs{
    if (!self.image_msgs.count) {
        return msgs;
    }
    NSMutableArray *data = [NSMutableArray arrayWithArray:msgs];
    NSMutableArray *tmpArr = msgs.mutableCopy;
    NSMutableArray *tmpImgsMsgArr = self.image_msgs.mutableCopy;
    for (BYPFChatModel *model in tmpArr) {
        for (BYPFChatModel *imgMsgModel in tmpImgsMsgArr) {
            if ([[model.image_url getFullImageUploadUrl] isEqualToString:[imgMsgModel.image_url getFullImageUploadUrl]]) {
//                imgMsgModel.messageId = model.messageId;
                [data removeObject:model];
                [self.image_msgs removeObject:model];
            }
        }
    }
    return data;
}

- (void)hiddenHeaderViewAnimation{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.headerView.frame;
        frame.origin.y = -24;
        self.headerView.frame = frame;
        [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    } completion:^(BOOL finished) {
//        self.tableView.tableHeaderView = nil;
    }];
}

#pragma mark - action
// 弹幕开关
- (void)barrageAction:(BOOL)isOn{
    self.rewardAnimationView.hidden = !isOn;
}

// 选择发送图片
- (void)didSelectImageData:(NSArray *)images{
    NSArray *msgArr = [BYPFChatModel getNoSendImgMsgData:images liveData:self.model];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
        [array addObjectsFromArray:msgArr];
        self.tableView.tableData = array;
        [self.tableView scrollToBottom:YES];
    });
    
    [self.image_msgs addObjectsFromArray:msgArr];
    [self loadUploadSelectImg:images msgArr:msgArr];
}

- (void)scrollToMsgBottom{
    [self.tableView scrollToBottom:YES];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableViewBecomeFirstResponder:(BYCommonTableView *)tableView{
    [_hostToolBar resignFirstResponder];
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"imageViewAction"]) {
        UIImageView *imageView = param[@"imageView"];
        if (!imageView.image) return;
        CGRect newe =  [imageView.superview convertRect:imageView.frame toView:kCommonWindow];
        [MsgPicViewController addToRootViewController:imageView.image ofMsgId:@"" in:newe from:@[]];
    }else if ([actionName isEqualToString:@"userNameAction"]) {
        if (![AccountModel sharedAccountModel].hasLoggedIn) {
            [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
            return;
        }
        NSString *userId = [param[@"user_id"] length] ? param[@"user_id"] : self.model.user_id;
        if ([userId isEqualToString:ACCOUNT_ID]) return;
        if (![userId verifyIsNum]) return;
        self.personView.user_id = userId;
        [self.personView showAnimation];
    }
}

- (void)by_tableViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height) {
        self.newMsgBottomView.hidden = YES;
        [self.noReadMsgs removeAllObjects];
    }
}

#pragma mark - request
- (void)loadRequestGetHistoryMsg:(void(^)(NSArray *data))cb{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetPageChatHistory:self.model.group_id pageNum:self.pageNum sendTime:[BYSIMManager shareManager].joinTime successBlock:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        if (!self) return ;
        if (![object[@"content"] isKindOfClass:[NSArray class]] || ![object[@"content"] count]) {
            if (cb) cb(@[]);
            return;
        }
        if (self.pageNum == 0) {
            NSMutableArray *data = [NSMutableArray array];
            NSArray *array = [BYPFChatModel transTableViewData:object[@"content"] liveModel:self.model];
            array = [[array reverseObjectEnumerator] allObjects];
            [data addObjectsFromArray:array];
            [data addObjectsFromArray:self.tableView.tableData];
            self.tableView.tableData = array;
            [self.tableView reloadData];
            [self.tableView scrollToBottom:YES];
        }
        else{
            NSArray *array = [BYPFChatModel transTableViewData:object[@"content"] liveModel:self.model];
            array = [[array reverseObjectEnumerator] allObjects];
            if (cb) cb(array);
        }
        self.pageNum ++;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

- (void)loadUploadSelectImg:(NSArray *)images msgArr:(NSArray *)msgArr{
    NSMutableArray *fileModels = [NSMutableArray array];
    for (int i = 0 ; i < images.count; i ++) {
        UIImage *image = images[i];
        OSSFileModel *fileModel = [[OSSFileModel alloc] init];
        fileModel.objcImage = image;
        fileModel.objcName  = [NSString stringWithFormat:@"%@-%@-%d",[NSDate by_stringFromDate:[NSDate by_date] dateformatter:@"yyyy-MM-dd-HH:mm:ss"],@"MSG_IMG",i];
        [fileModels addObject:fileModel];
    }
    [[OSSManager sharedUploadManager] uploadImageManagerWithImgList:[fileModels copy] progress:^(NSInteger index, float percent) {
        
    } finish:^(NSArray *url, NSInteger index, BOOL allUpload) {
        PDLog(@"接收到图片上传回调---index:%li",index);
        if (allUpload) return ;
        BYPFChatModel *msgModel = msgArr[index];
        BYSIMMessage *msg = [[BYSIMMessage alloc] init];
        msg.msg_type = BY_SIM_MSG_TYPE_IMAGE;
        msg.msg_sender = [AccountModel sharedAccountModel].account_id;
        msg.msg_senderName = nullToEmpty(LOGIN_MODEL.user.nickname);
        msg.msg_senderlogoUrl = nullToEmpty(LOGIN_MODEL.account.head_img);
        msg.time = [NSDate getCurrentTimeStr];
        BYSIMImageElem *elem = [[BYSIMImageElem alloc] init];
        elem.path = url[0];
        msg.elem = elem;
        msgModel.image_url = url[0];
        @weakify(self);
        [[BYSIMManager allocManager] sendMessage:msg suc:^{
            @strongify(self);
            dispatch_async(dispatch_get_main_queue(), ^{
                msgModel.msg_status = BY_SIM_MSG_STATE_NORMAL;
                NSInteger row = [self.tableView.tableData indexOfObject:msgModel];
                [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            });
        } fail:^{
            @strongify(self);
            dispatch_async(dispatch_get_main_queue(), ^{
                msgModel.msg_status = BY_SIM_MSG_STATE_NORMAL;
                NSInteger row = [self.tableView.tableData indexOfObject:msgModel];
                [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            });
        }];
    }];
}

#pragma mark - configUI

- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.backgroundColor = kColorRGBValue(0xf8f8f8);
    self.tableView.group_delegate = self;
    [self.tableView addHeaderRefreshTarget:self action:@selector(loadMoreData)];
    [self.tableView setContentInset:UIEdgeInsetsMake(24, 0, 0, 0)];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kSafe_Mas_Bottom(49), 0));
    }];
    
    [self.view addSubview:self.headerView];
    [self.headerView startAnimation];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(13 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self hiddenHeaderViewAnimation];
    });
    
    [self.view addSubview:self.newMsgBottomView];
    [self.newMsgBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(32);
        make.width.mas_greaterThanOrEqualTo(115);
        make.bottom.mas_equalTo(self.tableView).offset(-10);
    }];
    
}

- (void)addRewardAnimationView{
    self.rewardAnimationView = [[BYPFRewardAnimationView alloc] init];
    [self.view addSubview:self.rewardAnimationView];
    [self.rewardAnimationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(56);
        make.height.mas_equalTo(124);
    }];
}

- (void)addToolBar{
    if (_isHost) {
        [self.view addSubview:self.hostToolBar];
        [self.hostToolBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(0);
            make.height.mas_equalTo(kSafe_Mas_Bottom(49));
        }];
        return;
    }
    [self.view addSubview:self.audienceToolBar];
    [self.audienceToolBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(49));
    }];
}

- (BYPFChatWelcomView *)headerView{
    if (!_headerView) {
        _headerView = [[BYPFChatWelcomView alloc] init];
        _headerView.frame = CGRectMake(0, 0, kCommonScreenWidth, 24);
    }
    return _headerView;
}

- (BYPFHostToolBar *)hostToolBar{
    if (!_hostToolBar) {
        _hostToolBar = [[BYPFHostToolBar alloc] init];
        @weakify(self);
        _hostToolBar.barrageIsOnHandle = ^(BOOL isOn) {
            @strongify(self);
            [self barrageAction:isOn];
        };
        _hostToolBar.didSelectImageDataHandle = ^(NSArray * _Nonnull images) {
            @strongify(self);
            [self didSelectImageData:images];
        };
        _hostToolBar.didExitLiveBtnHandle = ^{
            @strongify(self);
            if (self.didExitLiveBtnHandle) {
                self.didExitLiveBtnHandle();
            }
        };
    }
    return _hostToolBar;
}

- (BYPFAudienceToolBar *)audienceToolBar{
    if (!_audienceToolBar) {
        _audienceToolBar = [[BYPFAudienceToolBar alloc] init];
        @weakify(self);
        _audienceToolBar.barrageIsOnHandle = ^(BOOL isOn) {
            @strongify(self);
            [self barrageAction:isOn];
        };
        _audienceToolBar.didShareActionHandle = ^{
            @strongify(self);
            if (self.didShareActionHandle) {
                self.didShareActionHandle();
            }
        };
    }
    return _audienceToolBar;
}

- (BYPFUserInfoView *)personView{
    if (!_personView) {
        _personView = [[BYPFUserInfoView alloc] initWithFathureView:CURRENT_VC.view];
        _personView.model = self.model;
        if (_isHost &&
            (_model.status == BY_LIVE_STATUS_SOON ||
             _model.status == BY_LIVE_STATUS_LIVING)) {
                _personView.hasForbid = YES;
            }else {
                _personView.hasForbid = NO;
            }
        @weakify(self);
        _personView.didUpdateHostAttention = ^(BOOL isAttention) {
            @strongify(self);
            if (self.didUpdateHostAttention) {
                self.didUpdateHostAttention(isAttention);
            }
        };
    }
    return _personView;
}

- (BYNewMsgBottomView *)newMsgBottomView{
    if (!_newMsgBottomView) {
        _newMsgBottomView = [[BYNewMsgBottomView alloc] init];
        _newMsgBottomView.hidden = YES;
        [_newMsgBottomView addTarget:self action:@selector(scrollToMsgBottom) forControlEvents:UIControlEventTouchUpInside];
    }
    return _newMsgBottomView;
}
@end
