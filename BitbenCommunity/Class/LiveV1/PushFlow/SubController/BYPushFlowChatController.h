//
//  BYPushFlowChatController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "HGPageViewController.h"
#import "BYPFAudienceToolBar.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYPushFlowChatController : HGPageViewController

/** toolBar */
@property (nonatomic ,strong ,readonly) BYPFAudienceToolBar *audienceToolBar;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** isHost */
@property (nonatomic ,assign) BOOL isHost;
/** 退出直播按钮回调 */
@property (nonatomic ,copy) void(^didExitLiveBtnHandle)(void);
/** 分享事件 */
@property (nonatomic ,copy) void (^didShareActionHandle)(void);
/** 关注了主播状态变更多回调 */
@property (nonatomic ,copy) void (^didUpdateHostAttention)(BOOL isAttention);

- (void)addMessageData:(NSArray *)msgs;

- (void)reloadSuppertNum;

- (void)showHostInfo;
@end

NS_ASSUME_NONNULL_END
