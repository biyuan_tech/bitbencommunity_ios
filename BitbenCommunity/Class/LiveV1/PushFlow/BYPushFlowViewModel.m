//
//  BYPushFlowViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPushFlowViewModel.h"
#import "BYPushFlowController.h"
#import "HGSegmentedPageViewController.h"
#import "BYPushFlowChatController.h"
#import "BYPushFlowIntroController.h"
#import "ShareRootViewController.h"

#import "HGCenterBaseTableView.h"
#import "BYPushFlowHeaderView.h"
#import "BYReportSheetView.h"

#import "AATAudioTool.h"

#define K_VC ((BYPushFlowController *)S_VC)
@interface BYPushFlowViewModel ()<HGSegmentedPageViewControllerDelegate, HGPageViewControllerDelegate,UITableViewDelegate,UITableViewDataSource>

/** tableView */
@property (nonatomic ,strong) HGCenterBaseTableView *tableView;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** headerView */
@property (nonatomic ,strong) BYPushFlowHeaderView *headerView;
/** tableFooterView */
@property (nonatomic ,strong) UIView *tableFooterView;
/** 聊天室 */
@property (nonatomic ,strong) BYPushFlowChatController *chatController;
/** 简介 */
@property (nonatomic ,strong) BYPushFlowIntroController *introController;
/** segmentControllView */
@property (nonatomic ,strong) HGSegmentedPageViewController *segmentedPageViewController;
/** 是否可滑动 */
@property (nonatomic ,assign) BOOL cannotScroll;
/** 心跳计时 */
@property (nonatomic ,strong) NSTimer *roomTimer;
/** 虚拟pv数 */
@property (nonatomic ,assign) NSInteger virtual_pv;
/** 举报 */
@property (nonatomic ,strong) BYReportSheetView *reportSheetView;

@end

@implementation BYPushFlowViewModel

- (void)dealloc
{
    [[AATAudioTool share] stopPlay];
    [[BYCommonTool shareManager] stopTimer];
    [_headerView.playerView resetPlayer];
    [_headerView removeFromSuperview];
    [_tableView removeFromSuperview];
    [_introController removeFromParentViewController];
    [_chatController removeFromParentViewController];
    [_introController.view removeFromSuperview];
    [_chatController.view removeFromSuperview];
    [_segmentedPageViewController removeFromParentViewController];
    [_segmentedPageViewController.view removeFromSuperview];
    _introController = nil;
    _chatController = nil;
    _headerView = nil;
    _tableView = nil;
    _segmentedPageViewController = nil;
    
}

- (void)viewWillAppear{
    [_headerView.playerView resume];
}

- (void)viewDidDisappear{
    [_headerView.playerView pause];
}

- (void)setContentView{
    self.virtual_pv = K_VC.isHost ? 0 : -1;
    [self addTableView];
    [self loadRequestGetDetail];
    [self addMessageListener];
    [self setReportView];
    [self loginNotic];
}

- (void)destoryTimer{
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
    [[BYCommonTool shareManager] stopTimer];
    
}

// 消息回调
- (void)addMessageListener{
    // 消息回调
    @weakify(self);
    [[BYSIMManager shareManager] setMessageListener:^(NSArray * _Nonnull msgs) {
        @strongify(self);
        NSDictionary *dic = msgs[0];
        if (dic) {
            if ([dic[@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
                NSDictionary *customData;
                if ([dic[@"content"] isKindOfClass:[NSString class]]) {
                    // 现返回的为纯json字符串，需要转两次
                    NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                    customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                }
                else{
                    customData = dic[@"content"];
                }
                if ([customData[kBYCustomMessageType] integerValue] == BY_GUEST_OPERA_TYPE_RECEIVE_STREAM) { // 开播消息
                    self.model.status = BY_LIVE_STATUS_LIVING;
                    [self.headerView reloadHeaderData:self.model];
                }
                else if ([customData[kBYCustomMessageType] integerValue] == BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM) { // 关播消息
                    self.model.status = BY_LIVE_STATUS_END;
                    [self destoryTimer];
                    [self.headerView reloadHeaderData:self.model];
                }
            }
        }
        
        [self.chatController addMessageData:msgs];
    }];
}

// 分享
- (void)shareAction:(BOOL)hasJubao{
    if (!self.model.shareMap) return;
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.transferCopyUrl = self.model.shareMap.share_url;
    shareViewController.collection_status = self.model.collection_status;
    if (![AccountModel sharedAccountModel].hasLoggedIn) {
        shareViewController.hasJubao = NO;
    }else {
        if (!hasJubao) {
            shareViewController.hasJubao = NO;
        }else{
            shareViewController.hasJubao = [self.model.user_id isEqualToString:[AccountModel sharedAccountModel].account_id] ? NO : YES;
        }
    }
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        NSString *mainImgUrl = [PDImageView appendingImgUrl:weakSelf.model.shareMap.share_img];
        NSString *imgurl = [mainImgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [ShareSDKManager shareManagerWithType:shareType title:weakSelf.model.shareMap.share_title desc:weakSelf.model.shareMap.share_intro img:imgurl url:weakSelf.model.shareMap.share_url callBack:NULL];
        [ShareSDKManager shareSuccessBack:shareTypeLive block:NULL];
    }];
    @weakify(self);
    [shareViewController actionClickWithJubaoBlock:^{
        @strongify(self);
        if (!self.model) return ;
        self.reportSheetView.theme_id = self.model.live_record_id;
        self.reportSheetView.theme_type = BY_THEME_TYPE_LIVE;
        self.reportSheetView.user_id = self.model.user_id;
        [self.reportSheetView showAnimation];
    }];
    
    [shareViewController actionClickWithCollectionBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.model.collection_status = !strongSelf.model.collection_status ;
    }];
    [shareViewController showInView:S_VC];
}

// 开始直播
- (void)beginLiving{
    [self startHeartBeatTimer];
    if (K_VC.isHost) {
        // 更新直播状态
        @weakify(self);
        [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_LIVING cb:^{
            @strongify(self);
            self.model.status = BY_LIVE_STATUS_LIVING;
            // 发送开播消息
            [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_RECEIVE_STREAM data:nil suc:nil fail:nil];
        }];
    }
}

- (void)endLiving{
    if (self.model.status == BY_LIVE_STATUS_END) {
        [S_V_NC popViewControllerAnimated:YES];
        return;
    }
    @weakify(self);
    [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_END cb:^{
        @strongify(self);
        @weakify(self);
        [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM data:nil suc:^{
            
        } fail:nil];
        [[BYSIMManager shareManager] closeRoom:self.model.group_id suc:^{
            @strongify(self);
            showDebugToastView(@"关闭聊天室成功",S_V_VIEW);
        } fail:^{
            @strongify(self);
            showDebugToastView(@"关闭聊天室失败",S_V_VIEW);
        }];
        self.model.status = BY_LIVE_STATUS_END;
        UIViewController *currentController = [BYTabbarViewController sharedController].currentController;
        NSInteger count = currentController.navigationController.viewControllers.count;
        UIViewController *viewController = currentController.navigationController.viewControllers[count - 2];
        [CURRENT_VC.navigationController popToViewController:viewController animated:YES];
    }];
}

// 开始发送心跳
- (void)startHeartBeatTimer{
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
    _roomTimer = [NSTimer scheduledTimerWithTimeInterval:kRoomTimerDuration target:self selector:@selector(postHeartBeat) userInfo:nil repeats:YES];
    [self postHeartBeat];
}

- (void)postHeartBeat{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestPostHeartBeat:_model.live_record_id
                                             virtual_pv:K_VC.isHost ? _virtual_pv : -1
                                           successBlock:^(id object) {
                                               @strongify(self);
                                               if (object && [object[@"content"] isEqualToString:@"success"]) {
                                                   self.virtual_pv = [object[@"virtualPV"] integerValue];
                                                   self.model.count_support = [object[@"count_support"] integerValue];
                                                   [self.chatController reloadSuppertNum];
                                                   [self.headerView.playerView setWatchNum:self.virtual_pv];
                                               }
                                           } faileBlock:nil];
}

- (void)loginNotic{
    @weakify(self);
    [S_VC loginSuccessedNotif:^(BOOL isSuccessed) {
        @strongify(self);
        if (!isSuccessed) return ;
        [self loadRequestGetDetail];
//        if (self.model.status == BY_LIVE_STATUS_SOON ||
//            self.model.status == BY_LIVE_STATUS_LIVING) {
//            @weakify(self);
//            [[BYSIMManager shareManager] joinRoom:self.model.group_id suc:^{
//                @strongify(self);
//                self.chatController.audienceToolBar.isForbid = [BYSIMManager shareManager].isUserForbid;
//                showDebugToastView(@"进入房间成功", S_V_VIEW);
//            } fail:^{
//                @strongify(self);
//                showDebugToastView(@"进入房间失败", S_V_VIEW);
//            }];
//        }
    }];
}

#pragma mark - request
- (void)loadRequestGetDetail{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLiveDetail:K_VC.live_record_id successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        if (!object) return ;
        self.model = object;
        self.chatController.model = object;
        self.introController.model = object;
        [self.headerView reloadHeaderData:self.model];
        if (self.model.status == BY_LIVE_STATUS_SOON ||
            self.model.status == BY_LIVE_STATUS_LIVING) {
            @weakify(self);
            [[BYSIMManager shareManager] joinRoom:self.model.group_id suc:^{
                @strongify(self);
                self.chatController.audienceToolBar.isForbid = [BYSIMManager shareManager].isUserForbid;
                showDebugToastView(@"进入房间成功", S_V_VIEW);
            } fail:^{
                @strongify(self);
                showDebugToastView(@"进入房间失败", S_V_VIEW);
            }];
        }else{
            
        }
        
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"获取直播详情失败", S_V_VIEW);
    }];
}

// 更新直播间状态
- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status cb:(void(^)(void))cb{
    if (!self.model.real_begin_time.length) {
        self.model.real_begin_time = [NSDate by_stringFromDate:[NSDate by_date] dateformatter:K_D_F];
    }
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveStatus:status live_record_id:_model.live_record_id stream_id:nil real_begin_time:self.model.real_begin_time successBlock:^(id object) {
        if (cb) cb();
        PDLog(@"更新直播间状态成功");
    } faileBlock:^(NSError *error) {
        PDLog(@"更新直播间状态失败");
    }];
}
#pragma mark - TableViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat contentOffsetY = scrollView.contentOffset.y;
    CGFloat criticalPointOffsetY = 0;
    
    //利用contentOffset处理内外层scrollView的滑动冲突问题
    if (contentOffsetY >= criticalPointOffsetY) {
        
        self.cannotScroll = YES;
        scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        [self.segmentedPageViewController.currentPageViewController makePageViewControllerScroll:YES];
    } else {
        
        if (self.cannotScroll) {
            //“维持吸顶状态”
            scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        } else {
            /* 吸顶状态 -> 不吸顶状态
             * categoryView的子控制器的tableView或collectionView在竖直方向上的contentOffsetY小于等于0时，会通过代理的方式改变当前控制器self.canScroll的值；
             */
        }
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    [self.segmentedPageViewController.currentPageViewController makePageViewControllerScrollToTop];
    return YES;
}

#pragma mark - HGSegmentedPageViewControllerDelegate
- (void)segmentedPageViewControllerWillBeginDragging {
    self.tableView.scrollEnabled = NO;
}

- (void)segmentedPageViewControllerDidEndDragging {
    self.tableView.scrollEnabled = YES;
}

#pragma mark - HGPageViewControllerDelegate
- (void)pageViewControllerLeaveTop {
    self.cannotScroll = NO;
}

#pragma mark - configUI
- (void)addTableView{
    self.tableView = [[HGCenterBaseTableView alloc] init];
    self.tableView.categoryViewHeight = 37;
    self.tableView.bounces = NO;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = self.tableFooterView;
    self.tableView.tableHeaderView = self.headerView;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    [S_V_VIEW addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (void)setReportView{
    self.reportSheetView = [BYReportSheetView initReportSheetViewShowInView:kCommonWindow];
}


- (BYPushFlowHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[BYPushFlowHeaderView alloc] init];
        CGFloat detal = IS_PhoneXAll ? kStatusBarHeight : 0;
        CGFloat height = kCommonScreenWidth*9.0f/16.0f + 48 + detal;
        _headerView.frame = CGRectMake(0, 0, kCommonScreenWidth, height);
        @weakify(self);
        _headerView.didBackActionHandle = ^{
            @strongify(self);
            [S_V_NC popViewControllerAnimated:YES];
        };
        _headerView.didLivingHandle = ^{
            @strongify(self);
            [self beginLiving];
        };
        _headerView.didMoreActionHandle = ^{
            @strongify(self);
            [self shareAction:YES];
        };
        _headerView.didClickUserlogoHandle = ^{
            @strongify(self);
            [self.chatController showHostInfo];
        };
    }
    return _headerView;
}

- (UIView *)tableFooterView{
    if (!_tableFooterView) {
        _tableFooterView = [[UIView alloc] init];
        CGFloat detal = IS_PhoneXAll ? kStatusBarHeight : 0;
        CGFloat height = kCommonScreenWidth*9.0f/16.0f + 48 + detal;
        _tableFooterView.frame = CGRectMake(0, 0, kCommonScreenWidth, kCommonScreenHeight - height);
        [S_VC addChildViewController:self.segmentedPageViewController];
        [_tableFooterView addSubview:self.segmentedPageViewController.view];
        [self.segmentedPageViewController didMoveToParentViewController:S_VC];
        @weakify(self);
        [self.segmentedPageViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.edges.equalTo(self.tableFooterView);
        }];
    }
    return _tableFooterView;
}

- (HGSegmentedPageViewController *)segmentedPageViewController {
    if (!_segmentedPageViewController) {
        NSMutableArray *controllers = [NSMutableArray array];
        NSArray *titles = @[@"聊天", @"简介"];
        for (int i = 0; i < titles.count; i++) {
            HGPageViewController *controller;
            if (i == 0) {
                controller = self.chatController;
            } else {
                controller = self.introController;
            }
            controller.delegate = self;
            [controllers addObject:controller];
        }
        _segmentedPageViewController = [[HGSegmentedPageViewController alloc] init];
        _segmentedPageViewController.pageViewControllers = controllers;
        _segmentedPageViewController.categoryView.titles = titles;
        _segmentedPageViewController.categoryView.height = 37;
        _segmentedPageViewController.categoryView.alignment = HGCategoryViewAlignmentLeft;
        _segmentedPageViewController.categoryView.originalIndex = 0;
        _segmentedPageViewController.categoryView.leftAndRightMargin = 15.0f;
        _segmentedPageViewController.categoryView.itemSpacing = 26;
        _segmentedPageViewController.categoryView.titleNomalFont = [UIFont systemFontOfSize:14];
        _segmentedPageViewController.categoryView.titleSelectedFont = [UIFont boldSystemFontOfSize:14];
        _segmentedPageViewController.categoryView.titleNormalColor = kColorRGBValue(0x333333);
        _segmentedPageViewController.categoryView.titleSelectedColor = kColorRGBValue(0x313131);
        _segmentedPageViewController.categoryView.backgroundColor = [UIColor whiteColor];
        _segmentedPageViewController.categoryView.vernierHeight = 2;
        _segmentedPageViewController.categoryView.vernierWidth = 16;
        _segmentedPageViewController.categoryView.vernier.backgroundColor = kColorRGBValue(0xea6438);
        _segmentedPageViewController.categoryView.topBorder.hidden = YES;
        _segmentedPageViewController.categoryView.bottomBorder.backgroundColor = kColorRGBValue(0xe7e7ea);
        _segmentedPageViewController.delegate = self;
    }
    return _segmentedPageViewController;
}

- (BYPushFlowChatController *)chatController{
    if (!_chatController) {
        _chatController = [[BYPushFlowChatController alloc] init];
        _chatController.isHost = K_VC.isHost;
        @weakify(self);
        _chatController.didShareActionHandle = ^{
            @strongify(self);
            [self shareAction:YES];
        };
        _chatController.didExitLiveBtnHandle = ^{
            @strongify(self);
            [self endLiving];
        };
        _chatController.didUpdateHostAttention = ^(BOOL isAttention) {
            @strongify(self);
            self.model.isAttention = isAttention;
//            [self.headerView reloadHeaderData:self.model];
            [self.headerView reloadAttentionStatus:isAttention];
            [self.introController reloadAttentionStatus:isAttention];
        };
        [S_VC addChildViewController:_chatController];
    }
    return _chatController;
}

- (BYPushFlowIntroController *)introController{
    if (!_introController) {
        _introController = [[BYPushFlowIntroController alloc] init];
        [S_VC addChildViewController:_introController];
        @weakify(self);
        _introController.didUpdateHostAttention = ^(BOOL isAttention) {
            @strongify(self);
            self.model.isAttention = isAttention;
            [self.headerView reloadAttentionStatus:isAttention];
        };
    }
    return _introController;
}

@end
