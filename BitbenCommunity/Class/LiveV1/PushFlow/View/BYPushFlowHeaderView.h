//
//  BYPushFlowHeaderView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYCommonPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYPushFlowHeaderView : UIView

/** 返回按钮事件 */
@property (nonatomic ,copy) void (^didBackActionHandle)(void);
/** 更多按钮事件 */
@property (nonatomic ,copy) void (^didMoreActionHandle)(void);
/** 开播状态修改回调 */
@property (nonatomic ,copy) void (^didLivingHandle)(void);
/** 头像点击 */
@property (nonatomic ,copy) void (^didClickUserlogoHandle)(void);
/** 播放器 */
@property (nonatomic ,strong ,readonly) BYCommonPlayerView      *playerView;


- (void)reloadHeaderData:(BYCommonLiveModel *)model;

- (void)reloadAttentionStatus:(BOOL)isAttention;

@end

NS_ASSUME_NONNULL_END
