//
//  BYPushFlowHeaderView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPushFlowHeaderView.h"


@interface BYPushFlowHeaderView ()<SuperPlayerDelegate>

/** topView */
@property (nonatomic, strong) UIImageView             *topImageView;
/** 标题 */
@property (nonatomic, strong) UILabel                 *titleLabel;
/** 更多按钮 */
@property (nonatomic, strong) UIButton                *moreBtn;
/** 返回按钮*/
@property (nonatomic, strong) UIButton                *backBtn;
/** 遮罩 */
@property (nonatomic ,strong) UIView                  *maskView;
/** 直播封面 */
@property (nonatomic ,strong) PDImageView             *coverImgView;
/** 倒计时 */
@property (nonatomic ,strong) UILabel                 *timeLab;
/** 播放器 */
@property (nonatomic ,strong) BYCommonPlayerView      *playerView;
/** bottomView */
@property (nonatomic ,strong) UIView                  *bottomView;
/** 头像 */
@property (nonatomic ,strong) PDImageView             *userlogoView;
/** 昵称 */
@property (nonatomic ,strong) UILabel                 *userNameLab;
/** 粉丝数 */
@property (nonatomic ,strong) UILabel                 *fansLab;
/** 关注按钮 */
@property (nonatomic ,strong) UIButton                *attentionBtn;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel       *model;
/** 当前是否到了开播时间 */
@property (nonatomic ,assign) BOOL                    isTimeLive;
/** 是否接收到远端视频 */
@property (nonatomic ,assign) BOOL                    isReceiveLive;

@end

@implementation BYPushFlowHeaderView

- (void)dealloc
{
    [[BYCommonTool shareManager] stopTimer];
    [_playerView resetPlayer];
    _playerView = nil;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.isTimeLive      = NO;
        self.isReceiveLive   = NO;
        [self setBackgroundColor:[UIColor blackColor]];
        [self setContentView];
    }
    return self;
}

- (void)reloadHeaderData:(BYCommonLiveModel *)model{
    self.model = model;
    _playerView.autoPlay = [ACCOUNT_ID isEqualToString:self.model.user_id] ? YES : NO;
    if (model.status == BY_LIVE_STATUS_SOON) {
        @weakify(self);
        [self verifyLiveTime:^{
            @strongify(self);
            // 开播时间到后主播直接放置播放器，观众待收到开播通知后放置播放器
            if ([ACCOUNT_ID isEqualToString:self.model.user_id]) {
                [self addPlayerView];
            }else{
                self.timeLab.font = [UIFont systemFontOfSize:22];
                self.timeLab.text = @"信号源未接入";
            }
        }];
    }else if (model.status == BY_LIVE_STATUS_LIVING){
        self.isTimeLive = YES;
        [self addPlayerView];
    }else {
        [self setEndLiveStatus];
    }
    self.titleLabel.text = model.live_title;
    [self.coverImgView uploadHDImageWithURL:model.live_cover_url callback:nil];
    self.attentionBtn.hidden = [ACCOUNT_ID isEqualToString:model.user_id] ? YES : NO;
    [self.userlogoView uploadHDImageWithURL:model.head_img callback:nil];
    self.userlogoView.style = model.cert_badge;
    self.userNameLab.text = model.nickname;
    self.fansLab.text = [NSString stringWithFormat:@"%@粉丝",[NSString transformIntegerShow:model.fans]];
    [self reloadAttentionStatus];
}

- (void)reloadAttentionStatus:(BOOL)isAttention{
    self.model.isAttention = isAttention;
    self.attentionBtn.hidden = [ACCOUNT_ID isEqualToString:_model.user_id] ? YES : NO;
    [self reloadAttentionStatus];
}

// 开播时间与当前时间比对
- (void)verifyLiveTime:(void(^)(void))cb{
    NSDate *date = [NSDate by_dateFromString:self.model.begin_time dateformatter:K_D_F];
    NSDate *nowDate = [NSDate by_date];
    NSComparisonResult result = [date compare:nowDate];
    if (result == NSOrderedAscending ||
        result == NSOrderedSame) {
        self.isTimeLive = YES;
        if (cb) cb();
        [self verifyLiveStatus];
    }
    else // 未到开播时间，倒计时自动开播
    {
        // 直播倒计时
        @weakify(self);
        [[BYCommonTool shareManager] timerCutdown:self.model.begin_time cb:^(NSDictionary * _Nonnull param, BOOL isFinish) {
            @strongify(self);
            if (isFinish) {
                self.isTimeLive = YES;
                if (cb) cb();
                [self verifyLiveStatus];
            }
            else{
                self.timeLab.text = [NSString stringWithFormat:@"直播倒计时 : %02d天 %02d时 %02d分 %02d秒", [param[@"days"] intValue], [param[@"hours"] intValue], [param[@"minute"] intValue], [param[@"second"] intValue]];
            }
        }];
    }
}

// 检测当前开播状态
- (void)verifyLiveStatus{
    if (self.model.status == BY_LIVE_STATUS_END ||
        self.model.status == BY_LIVE_STATUS_OVERTIME) {
        return;
    }
    
    if (![ACCOUNT_ID isEqualToString:self.model.user_id]) { // 非主播
        if (self.didLivingHandle) {
            self.didLivingHandle();
        }
        return;
    }
    
    // 未到开播时间或未接收到直播信号源则返回
    if (!_isTimeLive || !_isReceiveLive) return;

    if (self.didLivingHandle) {
        self.didLivingHandle();
    }
}

- (void)reloadAttentionStatus{
    self.attentionBtn.selected = self.model.isAttention;
    UIColor *color = !_model.isAttention ? kColorRGBValue(0xea6438) : kColorRGBValue(0xc3c3c3);
    [_attentionBtn setBackgroundColor:color];
    _attentionBtn.layer.borderColor = !_model.isAttention ? kColorRGBValue(0xea6441).CGColor : kColorRGBValue(0x9b9b9b).CGColor;
}

- (void)setEndLiveStatus{
    if (_playerView || !_model.video_url.length) {
        if (_playerView.playerConfig.type == PLAYER_TYPE_PF_DEFULT) {
            return;
        }
        [_playerView resetPlayer];
        [_playerView removeFromSuperview];
        _playerView              = nil;
        self.timeLab.font        = [UIFont systemFontOfSize:22];
        if (!_model.video_url.length && !_playerView) {
            self.timeLab.text        = @"直播回放生成中";
        }else{
            self.timeLab.text        = @"直播已经结束";
        }
        self.topImageView.hidden = NO;
        self.timeLab.hidden      = NO;
        [self.timeLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(self.timeLab.font.pointSize);
        }];
    }else{
        [self addPlayerView];
    }
}

#pragma mark - action
- (void)backBtnClick:(UIButton *)sender{
    if (self.didBackActionHandle) {
        self.didBackActionHandle();
    }
}

- (void)moreBtnClick:(UIButton *)sender{
    if (self.didMoreActionHandle) {
        self.didMoreActionHandle();
    }
}

- (void)userlogoClick{
    if (!self.userlogoView.image) return;
    if ([self.model.user_id isEqualToString:ACCOUNT_ID]) return;
    if (![AccountModel sharedAccountModel].hasLoggedIn) {
        [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
        return;
    }
    if (self.didClickUserlogoHandle) {
        self.didClickUserlogoHandle();
    }
}

- (void)attentionBtnAction{
    if (!_model.user_id.length) {
        return;
    }
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:_model.user_id
                                             isAttention:!_attentionBtn.selected
                                            successBlock:^(id object) {
                                                @strongify(self);
                                                self.model.isAttention = !self.attentionBtn.selected;
                                                [self reloadAttentionStatus];
                                            } faileBlock:nil];
}

#pragma mark - SuperPlayerDelegate
/** 返回事件 */
- (void)superPlayerBackAction:(SuperPlayerView *)player{
    if (self.didBackActionHandle) {
        self.didBackActionHandle();
    }
}

- (void)superPlayerMoreAction:(SuperPlayerView *)player{
    if (self.didMoreActionHandle) {
        self.didMoreActionHandle();
    }
}

- (void)superPlayerFullScreenChanged:(SuperPlayerView *)player{
    
}

/// 播放开始通知
- (void)superPlayerDidStart:(SuperPlayerView *)player{
    self.isReceiveLive = YES;
    [self verifyLiveStatus];
}

#pragma mark - configUI

- (void)setContentView{
    [self addSubview:self.coverImgView];
    [self addSubview:self.maskView];
    [self addSubview:self.topImageView];
    [self.topImageView addSubview:self.backBtn];
    [self.topImageView addSubview:self.moreBtn];
    [self.topImageView addSubview:self.titleLabel];
    [self addSubview:self.timeLab];
    [self addSubview:self.bottomView];
    [self.bottomView addSubview:self.userlogoView];
    [self.bottomView addSubview:self.userNameLab];
    [self.bottomView addSubview:self.fansLab];
    [self.bottomView addSubview:self.attentionBtn];
    
    CGFloat safeTop = IS_PhoneXAll ? kStatusBarHeight : 0;
    [self.coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(safeTop, 0, 48, 0));
    }];
    
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(safeTop, 0, 0, 0));
    }];
    
    [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(self);
        make.top.equalTo(self.mas_top).offset(safeTop);
        make.height.mas_equalTo(72);
    }];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.topImageView.mas_leading).offset(5);
        make.top.equalTo(self.topImageView.mas_top).offset(IsIPhoneX ? 0 : 22);
        make.width.height.mas_equalTo(40);
    }];
    
    [self.moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(49);
        make.trailing.equalTo(self.topImageView.mas_trailing).offset(-10);
        make.centerY.equalTo(self.backBtn.mas_centerY);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.backBtn.mas_trailing).offset(5);
        make.centerY.equalTo(self.backBtn.mas_centerY);
        make.trailing.mas_equalTo(-70);
    }];
    
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.centerY.mas_equalTo(self.maskView).mas_offset(0);
        make.height.mas_equalTo(self.timeLab.font.pointSize);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(48);
    }];
    
    [self.userlogoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(10);
        make.width.height.mas_equalTo(34);
    }];
    
    [self.userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.userlogoView.mas_right).mas_offset(10);
        make.top.mas_equalTo(self.userlogoView).mas_offset(2);
        make.height.mas_equalTo(self.userNameLab.font.pointSize);
        make.right.mas_equalTo(-90);
    }];
    
    [self.fansLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.userNameLab);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(self.fansLab.font.pointSize);
        make.bottom.mas_equalTo(self.userlogoView).mas_offset(-2);
    }];
    
    [self.attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(46);
        make.height.mas_equalTo(22);
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(self.userlogoView);
    }];
}

- (void)addPlayerView{
    self.topImageView.hidden = YES;
    self.timeLab.hidden      = YES;
    if (_playerView) return;
    CGFloat safeTop = IS_PhoneXAll ? kStatusBarHeight : 0;
    UIView *fatherView = [UIView by_init];
    [self addSubview:fatherView];
    [self insertSubview:fatherView belowSubview:self.topImageView];
    CGFloat height = kCommonScreenWidth*9.0f/16.0f;
    [fatherView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(safeTop);
        make.height.mas_equalTo(height);
    }];
    self.playerView = [[BYCommonPlayerView alloc] init];
    if (self.model.status == BY_LIVE_STATUS_LIVING ||
        self.model.status == BY_LIVE_STATUS_SOON) {
        self.playerView.playerConfig.type = PLAYER_TYPE_LIVE;
    }else{
        self.playerView.playerConfig.type = PLAYER_TYPE_PF_DEFULT;
    }
    self.playerView.enableFloatWindow = NO;
    self.playerView.autoPlay = YES;
    NSString *url = [_model.live_cover_url getFullImageUploadUrl];
    [self.playerView.coverImageView sd_setImageWithURL:[NSURL URLWithString:url]];
    self.playerView.fatherView = fatherView;
    self.playerView.delegate = self;
    if (self.model.status == BY_LIVE_STATUS_LIVING ||
        self.model.status == BY_LIVE_STATUS_SOON) {
        [self.playerView playVideoWithUrl:_model.play_url title:_model.live_title];
    }else{
        [self.playerView playVideoWithUrl:_model.video_url title:_model.live_title];
    }
    [self.playerView setWatchNum:_model.watch_times];
}

- (UIImageView *)topImageView {
    if (!_topImageView) {
        _topImageView                        = [[UIImageView alloc] init];
        _topImageView.userInteractionEnabled = YES;
        _topImageView.image                  = [UIImage imageNamed:@"player_top_shadow"];
    }
    return _topImageView;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"icon_rank_nav_back"] forState:UIControlStateNormal];
        [_backBtn addTarget:self action:@selector(backBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backBtn;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont systemFontOfSize:15.0];
    }
    return _titleLabel;
}

- (UIButton *)moreBtn {
    if (!_moreBtn) {
        _moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_moreBtn setImage:[UIImage imageNamed:@"player_more"] forState:UIControlStateNormal];
        [_moreBtn setImage:[UIImage imageNamed:@"player_more"] forState:UIControlStateSelected];
        [_moreBtn addTarget:self action:@selector(moreBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreBtn;
}

- (UIView *)maskView{
    if (!_maskView) {
        _maskView = [[UIView alloc] init];
        [_maskView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.7]];
    }
    return _maskView;
}

- (PDImageView *)coverImgView{
    if (!_coverImgView) {
        _coverImgView = [[PDImageView alloc] init];
        _coverImgView.contentMode = UIViewContentModeScaleAspectFill;
        _coverImgView.clipsToBounds = YES;
    }
    return _coverImgView;
}

- (UILabel *)timeLab{
    if (!_timeLab) {
        _timeLab = [[UILabel alloc] init];
        _timeLab.textAlignment = NSTextAlignmentCenter;
        _timeLab.font = [UIFont systemFontOfSize:15];
        _timeLab.textColor = [UIColor whiteColor];
    }
    return _timeLab;
}

- (UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        [_bottomView setBackgroundColor:[UIColor whiteColor]];
    }
    return _bottomView;
}

- (PDImageView *)userlogoView{
    if (!_userlogoView) {
        _userlogoView = [[PDImageView alloc] init];
        _userlogoView.contentMode = UIViewContentModeScaleAspectFill;
        _userlogoView.layer.cornerRadius = 17;
        _userlogoView.clipsToBounds = YES;
        _userlogoView.userInteractionEnabled = YES;
        @weakify(self);
        [_userlogoView addTapGestureRecognizer:^{
            @strongify(self);
            [self userlogoClick];
        }];
    }
    return _userlogoView;
}

- (UILabel *)userNameLab{
    if (!_userNameLab) {
        _userNameLab = [[UILabel alloc] init];
        _userNameLab.font = [UIFont systemFontOfSize:15];
        _userNameLab.textColor = kColorRGBValue(0x333333);
    }
    return _userNameLab;
}

- (UILabel *)fansLab{
    if (!_fansLab) {
        _fansLab = [[UILabel alloc] init];
        _fansLab.font = [UIFont systemFontOfSize:12];
        _fansLab.textColor = kColorRGBValue(0x8f8f8f);
    }
    return _fansLab;
}

- (UIButton *)attentionBtn{
    if (!_attentionBtn) {
        _attentionBtn = [UIButton by_buttonWithCustomType];
        NSAttributedString *normalAtt = [[NSAttributedString alloc] initWithString:@"关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xffffff),NSFontAttributeName:[UIFont systemFontOfSize:12]}];
        NSAttributedString *selectAtt = [[NSAttributedString alloc] initWithString:@"已关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xffffff),NSFontAttributeName:[UIFont systemFontOfSize:12]}];
        [_attentionBtn setAttributedTitle:normalAtt forState:UIControlStateNormal];
        [_attentionBtn setAttributedTitle:selectAtt forState:UIControlStateSelected];
        [_attentionBtn addTarget:self action:@selector(attentionBtnAction) forControlEvents:UIControlEventTouchUpInside];
        _attentionBtn.layer.cornerRadius = 4.0f;
    }
    return _attentionBtn;
}

@end
