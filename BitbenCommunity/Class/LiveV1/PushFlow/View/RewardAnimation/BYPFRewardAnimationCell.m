//
//  BYPFRewardAnimationCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/4.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPFRewardAnimationCell.h"
#import "BYPFRewardAnimationModel.h"

@interface BYPFRewardAnimationCell ()

/** userlogo */
@property (nonatomic ,strong) PDImageView *userlogoImgView;
/** userName */
@property (nonatomic ,strong) UILabel *userNameLab;
/** amount */
@property (nonatomic ,strong) UILabel *amountLab;
/** masView */
@property (nonatomic ,strong) UIView *maskView;
/** amountImg */
@property (nonatomic ,strong) UIImageView *amountImgView;

@end

@implementation BYPFRewardAnimationCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYPFRewardAnimationModel *model = object[indexPath.row];
    [self.userlogoImgView uploadHDImageWithURL:model.userlogo_url callback:nil];
    self.userNameLab.text = model.userName;
    self.amountLab.text = model.amount;
    
    [self.maskView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(model.maskViewW);
    }];
    
    for (CALayer *layer in self.maskView.layer.sublayers) {
        if ([layer isKindOfClass:[CAGradientLayer class]]) {
            [layer removeFromSuperlayer];
        }
    }
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = CGRectMake(0, 0, model.maskViewW, 40);
    gradientLayer.colors = @[(id)kColorRGB(0, 0, 0, 0.55).CGColor,(id)kColorRGB(0, 0, 0, 0.05).CGColor];
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1, 0);
    [self.maskView.layer addSublayer:gradientLayer];
}

- (void)setContentView{
    [self.contentView addSubview:self.maskView];
    [self.contentView addSubview:self.userlogoImgView];
    [self.contentView addSubview:self.userNameLab];
    [self.contentView addSubview:self.amountLab];
    [self.contentView addSubview:self.amountImgView];
    
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(22);
        make.height.mas_equalTo(40);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    [self.userlogoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(18);
        make.top.mas_equalTo(25);
        make.width.height.mas_equalTo(34);
    }];
    
    [self.userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.userlogoImgView.mas_right).mas_offset(10);
        make.top.mas_equalTo(self.userlogoImgView).mas_offset(2);
        make.height.mas_equalTo(self.userNameLab.font.pointSize);
        make.width.mas_lessThanOrEqualTo(135);
    }];
    
    [self.amountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.userlogoImgView.mas_right).mas_offset(10);
        make.bottom.mas_equalTo(self.userlogoImgView).mas_offset(-2);
        make.height.mas_equalTo(self.amountLab.font.pointSize);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    [self.amountImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(13);
        make.right.mas_equalTo(self.maskView).mas_offset(13);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(48);
    }];
}

- (PDImageView *)userlogoImgView{
    if (!_userlogoImgView) {
        _userlogoImgView = [[PDImageView alloc] init];
        _userlogoImgView.contentMode = UIViewContentModeScaleAspectFill;
        _userlogoImgView.layer.cornerRadius = 17;
        _userlogoImgView.clipsToBounds = YES;
    }
    return _userlogoImgView;
}

- (UILabel *)userNameLab{
    if (!_userNameLab) {
        _userNameLab = [[UILabel alloc] init];
        _userNameLab.font = [UIFont boldSystemFontOfSize:14];
        _userNameLab.textColor = [UIColor whiteColor];
    }
    return _userNameLab;
}

- (UILabel *)amountLab{
    if (!_amountLab) {
        _amountLab = [[UILabel alloc] init];
        _amountLab.font = [UIFont boldSystemFontOfSize:12];
        _amountLab.textColor = kColorRGBValue(0xffdc1c);
    }
    return _amountLab;
}

- (UIView *)maskView{
    if (!_maskView) {
        _maskView = [[UIView alloc] init];
        _maskView.layer.cornerRadius = 20.f;
        _maskView.clipsToBounds = YES;
    }
    return _maskView;
}

- (UIImageView *)amountImgView{
    if (!_amountImgView) {
        _amountImgView = [[UIImageView alloc] init];
        [_amountImgView by_setImageName:@"videolive_reward_animation"];
    }
    return _amountImgView;
}
@end
