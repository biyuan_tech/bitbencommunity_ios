//
//  BYPFRewardAnimationModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/4.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPFRewardAnimationModel.h"

@implementation BYPFRewardAnimationModel

- (BYPFRewardAnimationModel *)getSingleModel:(NSDictionary *)data{
    self.cellString = @"BYPFRewardAnimationCell";
    self.cellHeight = 62;
    self.userlogo_url = nullToEmpty(data[@"head_img"]);
    self.userName = nullToEmpty(data[@"nickname"]);
    CGFloat amountF = [data[@"amount"] floatValue];
    self.amount = [NSString stringWithFormat:@"送出 %.2f BP",amountF];
    
    CGSize userNameSize = [self.userName getStringSizeWithFont:[UIFont boldSystemFontOfSize:14] maxWidth:135];
    CGSize amountSize   = [self.amount getStringSizeWithFont:[UIFont boldSystemFontOfSize:12] maxWidth:135];
    
    CGFloat width = userNameSize.width < amountSize.width ? amountSize.width : userNameSize.width;
    
    self.maskViewW = ceil(width) + 48 + 56;
    
    return self;
}

@end
