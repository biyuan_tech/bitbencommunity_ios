//
//  BYPFRewardAnimationView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/4.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYPFRewardAnimationView : UIView

/** 打赏动画队列 */
//@property (nonatomic ,strong ,readonly, getter=rewardAnimations) NSArray *rawardDatas;

/** 队列中新增打赏数据 */
- (void)addRewardData:(NSDictionary *)data;

@end

NS_ASSUME_NONNULL_END
