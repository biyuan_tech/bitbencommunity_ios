//
//  BYPFRewardAnimationModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/4.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYPFRewardAnimationModel : BYCommonModel

/** userlogo */
@property (nonatomic ,copy) NSString *userlogo_url;
/** userName */
@property (nonatomic ,copy) NSString *userName;
/** amount */
@property (nonatomic ,copy) NSString *amount;
/** 蒙版宽度 */
@property (nonatomic ,assign) CGFloat maskViewW;


- (BYPFRewardAnimationModel *)getSingleModel:(NSDictionary *)data;

@end

NS_ASSUME_NONNULL_END
