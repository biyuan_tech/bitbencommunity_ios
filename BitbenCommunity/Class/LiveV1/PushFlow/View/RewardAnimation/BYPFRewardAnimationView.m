//
//  BYPFRewardAnimationView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/4.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPFRewardAnimationView.h"
#import "BYPFRewardAnimationModel.h"

@interface BYPFRewardAnimationView ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** 打赏动画组 */
@property (nonatomic ,strong) NSMutableArray<NSDictionary *> *rewardAnimations;
/** 当前是否处于动画当中 */
@property (nonatomic ,assign) BOOL isAnimationing;
/** nstimer */
@property (nonatomic ,strong) NSTimer *timer;

@end

@implementation BYPFRewardAnimationView

- (void)dealloc
{
    [self destoryTimer];
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.rewardAnimations = [NSMutableArray array];
        self.clipsToBounds = NO;
        [self setContentView];
    }
    return self;
}

- (void)addRewardData:(NSDictionary *)data{
    if (self.hidden) return;
    @synchronized (self) {
        [self.rewardAnimations addObject:data];
        [self beginRewardAnimation];
    }
}

- (void)beginRewardAnimation{
    [self destoryTimer];
    self.tableView.alpha = 1.0f;
    if (self.isAnimationing) return;
    if (!self.rewardAnimations.count) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(hiddenTableView) userInfo:nil repeats:NO];
        return;
    }
    NSDictionary *data = self.rewardAnimations[0];
    [self.rewardAnimations removeObjectAtIndex:0];
    BYPFRewardAnimationModel *model = [[BYPFRewardAnimationModel alloc] init];
    [model getSingleModel:data];
    
    self.isAnimationing = YES;
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
    [array addObject:model];
    self.tableView.tableData = [array copy];
    [self.tableView scrollToBottom:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.isAnimationing = NO;
        [self beginRewardAnimation];
    });
}

- (void)destoryTimer{
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)hiddenTableView{
    [UIView animateWithDuration:0.1 animations:^{
        self.tableView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        self.tableView.tableData = @[];
    }];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == self.tableView.tableData.count - 1) {
        cell.transform = CGAffineTransformMakeTranslation(-kCommonScreenWidth, 0);
        [UIView animateWithDuration:0.5 animations:^{
            cell.transform = CGAffineTransformMakeTranslation(0, 0);
        }];
    }
}

#pragma mark - configUI

- (void)setContentView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.notResponder = YES;
    self.tableView.group_delegate = self;
    self.tableView.clipsToBounds = NO;
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView *hitView = [super hitTest:point withEvent:event];
    if (hitView) {
        return nil;
    }
    return hitView;
}

@end
