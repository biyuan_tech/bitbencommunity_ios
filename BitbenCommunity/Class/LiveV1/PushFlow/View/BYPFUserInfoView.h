//
//  BYPFUserInfoView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/2.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYPFUserInfoView : UIView

/** user_id */
@property (nonatomic ,copy) NSString *user_id;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;

/** 关注了主播状态变更多回调 */
@property (nonatomic ,copy) void (^didUpdateHostAttention)(BOOL isAttention);
/** 能否头像点击跳转 */
@property (nonatomic ,assign) BOOL noEnable;
/** 是否可禁言 */
@property (nonatomic ,assign) BOOL hasForbid;


/** 初始化 */
- (instancetype)initWithFathureView:(UIView *)fathureView;

- (void)showAnimation;

@end

NS_ASSUME_NONNULL_END
