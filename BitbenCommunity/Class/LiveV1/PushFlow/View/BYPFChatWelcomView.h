//
//  BYPFChatWelcomView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYPFChatWelcomView : UIView

- (void)startAnimation;

- (void)stop;

@end

NS_ASSUME_NONNULL_END
