//
//  BYPFChatTextCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/31.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPFChatTextCell.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

#import "BYPFChatModel.h"

@interface BYPFChatTextCell ()

/** 身份 */
//@property (nonatomic ,strong) UIImageView *identityImgView;
/** titleLab */
@property (nonatomic ,strong) YYLabel *titleLab;
/** 身份 */
@property (nonatomic ,strong) YYLabel *identityLab;

@end

@implementation BYPFChatTextCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYPFChatModel *model = object[indexPath.row];
    self.indexPath = indexPath;
    self.identityLab.text = model.identity;
    self.identityLab.hidden = !model.identity.length;

//    self.identityImgView.hidden = !model.isHost;
    @weakify(self);
    [model.attributedString yy_setTextHighlightRange:NSMakeRange(0, model.userName.length + 2) color:kColorRGBValue(0x8f8f8f) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        @strongify(self);
        [self sendActionName:@"userNameAction" param:@{@"user_id":model.user_id} indexPath:self.indexPath];
    }];
    self.titleLab.attributedText = model.attributedString;
    [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(model.topBottomInset);
        make.height.mas_equalTo(ceil(model.titleSize.height));
    }];
    
    [self.identityLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(model.identity, 10) + 4);
        make.height.mas_equalTo(stringGetHeight(model.identity, 10) + 6);
    }];
//    [self.identityImgView mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(model.topBottomInset);
//    }];
}

- (void)setContentView{
//    UIImageView *identityImgView = [[UIImageView alloc] init];
//    [identityImgView by_setImageName:@"videolive_identity"];
//    identityImgView.hidden = YES;
//    [self.contentView addSubview:identityImgView];
//    self.identityImgView = identityImgView;
//    [identityImgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(15);
//        make.top.mas_equalTo(6);
//        make.width.mas_equalTo(34);
//        make.height.mas_equalTo(16);
//    }];
    
    self.identityLab = [[YYLabel alloc] init];
    self.identityLab.font = [UIFont systemFontOfSize:10];
    self.identityLab.textAlignment = NSTextAlignmentCenter;
    self.identityLab.textColor = [UIColor whiteColor];
    self.identityLab.backgroundColor = kColorRGBValue(0xf7b500);
    self.identityLab.layer.cornerRadius = 2.0f;
    self.identityLab.hidden = YES;
    [self.contentView addSubview:self.identityLab];
    [self.identityLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(5);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    YYLabel *titleLab = [[YYLabel alloc] init];
    titleLab.font = [UIFont systemFontOfSize:14];
    titleLab.textColor = kColorRGBValue(0x313131);
    titleLab.lineBreakMode = NSLineBreakByCharWrapping;
    titleLab.numberOfLines = 0;
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(6, 15, 6, 15));
    }];
}

@end
