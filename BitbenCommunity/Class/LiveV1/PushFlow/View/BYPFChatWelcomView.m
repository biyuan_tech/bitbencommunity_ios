//
//  BYPFChatWelcomView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPFChatWelcomView.h"
#import "BYPaomaView.h"

@interface BYPFChatWelcomView ()

/** 跑马灯 */
@property (nonatomic ,strong) BYPaomaView *paomaView;


@end

@implementation BYPFChatWelcomView

- (void)dealloc
{
    [_paomaView stop];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:kColorRGBValue(0xffc36d)];
        [self setContentView];
    }
    return self;
}

- (void)startAnimation{
    [self.paomaView startAnimation:@"欢迎来到币本直播间！币本倡导绿色健康直播，直播内容和评论严禁包含政治、低俗色情、吸烟酗酒等内容，如过违反，将视情节严重程度给予禁播、永久禁播或封停账号处理"];
}

- (void)stop{
    [self.paomaView stop];
}

#pragma mark - configUI

- (void)setContentView{
    UIImageView *hornImgView = [[UIImageView alloc] init];
    [hornImgView by_setImageName:@"videolive_horn"];
    [self addSubview:hornImgView];
    [hornImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(16);
        make.height.mas_equalTo(14);
        make.centerY.mas_equalTo(0);
    }];
    
    self.paomaView = [[BYPaomaView alloc] init];
    self.paomaView.frame = CGRectMake(40, 0, kCommonScreenWidth - 80, 24);
    self.paomaView.tiltleColor = [UIColor whiteColor];
    [self addSubview:self.paomaView];
//    [self.paomaView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(40);
//        make.top.bottom.right.mas_equalTo(0);
//    }];
}

@end
