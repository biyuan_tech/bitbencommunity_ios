//
//  BYPushFlowIntroTitleCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPushFlowIntroTitleCell.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

#import "BYLiveDetailModel.h"


static NSInteger kBaseTag = 890;
@interface BYPushFlowIntroTitleCell ()

/** 直播标题 */
@property (nonatomic ,strong) YYLabel *titleLab;
/** 直播开始时间 */
@property (nonatomic ,strong) UILabel *timeLab;
/** model */
@property (nonatomic ,strong) BYLiveDetailModel *model;


@end

@implementation BYPushFlowIntroTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYLiveDetailModel *model = dic.allValues[0][indexPath.row];
    self.model = model;
    self.indexPath = indexPath;

    NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.live_title)];
    messageAttributed.yy_lineSpacing = 8.0f;
    messageAttributed.yy_font = [UIFont boldSystemFontOfSize:15];
    messageAttributed.yy_color = kColorRGBValue(0x323232);
    YYTextContainer *textContainer = [YYTextContainer new];
    textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
    YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
    [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(ceil(layout.textBoundingSize.height));
    }];
    self.titleLab.attributedText = messageAttributed;
    
    NSDate *date = [NSDate by_dateFromString:model.begin_time dateformatter:K_D_F];
    NSString *time = [NSDate by_stringFromDate:date dateformatter:@"yyyy-MM-dd HH:mm"];
    self.timeLab.text = time;
    
    [self addTagsView];


}
- (void)addTagsView{
    NSArray *topicArr = self.model.topic_content;
    for (UIView *view in self.contentView.subviews) {
        if (view.tag >= kBaseTag && view.tag < kBaseTag + 20) {
            [view removeFromSuperview];
        }
    }
    
    for (int i = 0; i < topicArr.count; i ++) {
        UIButton *tagView = [UIButton by_buttonWithCustomType];
        NSString *title = topicArr[i];
        [tagView setTitle:title forState:UIControlStateNormal];
        tagView.titleLabel.font = [UIFont systemFontOfSize:11];
        [tagView setTitleColor:kColorRGBValue(0x4f86cb) forState:UIControlStateNormal];
        [tagView setBackgroundColor:kColorRGBValue(0xf7fbff)];
        CGFloat width = stringGetWidth(title, 11) + 12;
        CGFloat height = stringGetHeight(title, 11) + 10;
        tagView.layer.cornerRadius = 2;
        tagView.layer.borderWidth = 0.5;
        tagView.layer.borderColor = kColorRGBValue(0x92b8e8).CGColor;
        UIButton *lastTagView = [self viewWithTag:kBaseTag + i - 1];
        [self.contentView addSubview:tagView];
        tagView.tag = kBaseTag + i;
        [tagView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            if (lastTagView) {
                make.left.mas_equalTo(lastTagView.mas_right).mas_offset(5);
            }else{
                make.left.mas_equalTo(15);
            }
            make.top.mas_equalTo(self.titleLab.mas_bottom).mas_offset(10);
        }];
    }
}


- (void)setContentView{
    // 直播标题
    self.titleLab = [YYLabel by_init];
    self.titleLab.font = [UIFont systemFontOfSize:15];
    self.titleLab.textColor = kColorRGBValue(0x323232);
    self.titleLab.numberOfLines = 2.0f;
    [self.contentView addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(14);
        make.height.mas_greaterThanOrEqualTo(0);
        make.right.mas_equalTo(-15);
    }];
    
    self.timeLab = [UILabel by_init];
    [self.timeLab setBy_font:13];
    self.timeLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:self.timeLab];
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.titleLab.mas_bottom).mas_offset(49);
        make.height.mas_equalTo(self.timeLab.font.pointSize);
        make.width.mas_equalTo(150);
    }];
}

@end
