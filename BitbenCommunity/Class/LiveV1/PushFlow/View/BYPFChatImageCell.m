//
//  BYPFChatImageCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/31.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPFChatImageCell.h"

#import "BYPFChatModel.h"

@interface BYPFChatImageCell ()

/** 身份 */
@property (nonatomic ,strong) UIImageView *identityImgView;
/** titleLab */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 图片 */
@property (nonatomic ,strong) PDImageView *msg_imageView;
/** 图片蒙版 */
@property (nonatomic ,strong) UIView *imageMaskView;
/** 发送状态 */
@property (nonatomic ,strong) UIActivityIndicatorView *indicatorView;
/** model */
@property (nonatomic ,strong) BYPFChatModel *model;

@end

@implementation BYPFChatImageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    self.indexPath = indexPath;
    BYPFChatModel *model = object[indexPath.row];
    self.model = model;
    self.userNameLab.text = model.title;
    if (model.image_notSend) {
        self.msg_imageView.image = model.image_notSend;
    }else{
        [self.msg_imageView uploadMainImageWithURL:model.image_url placeholder:nil imgType:PDImgTypeOriginal callback:nil];
    }
    if (model.msg_status == BY_SIM_MSG_STATE_SENDING) {
        self.imageMaskView.hidden = NO;
        [self.indicatorView startAnimating];
    }
    else{
        self.imageMaskView.hidden = YES;
        [self.indicatorView stopAnimating];
    }
    
    [self.userNameLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(model.title, self.userNameLab.font.pointSize));
    }];
}

#pragma mark - action
- (void)imageViewAction{
    if (!self.msg_imageView.image) return;
    [self sendActionName:@"imageViewAction" param:@{@"imageView":self.msg_imageView} indexPath:self.indexPath];
}

- (void)userNameAction{
    [self sendActionName:@"userNameAction" param:@{@"user_id":self.model.user_id} indexPath:self.indexPath];
}
#pragma mark - configUI

- (void)setContentView{
    UIImageView *identityImgView = [[UIImageView alloc] init];
    [identityImgView by_setImageName:@"videolive_identity"];
//    identityImgView.hidden = YES;
    [self.contentView addSubview:identityImgView];
    self.identityImgView = identityImgView;
    [identityImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(7);
        make.width.mas_equalTo(34);
        make.height.mas_equalTo(16);
    }];
    
    UILabel *userNameLab = [[UILabel alloc] init];
    userNameLab.font = [UIFont systemFontOfSize:14];
    userNameLab.textColor = kColorRGBValue(0x8f8f8f);
    userNameLab.userInteractionEnabled = YES;
    [self.contentView addSubview:userNameLab];
    self.userNameLab = userNameLab;
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(56);
        make.top.mas_equalTo(7);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(userNameLab.font.pointSize);
    }];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userNameAction)];
    [userNameLab addGestureRecognizer:tapGesture];
    
    PDImageView *msg_imageView = [[PDImageView alloc] init];
    msg_imageView.contentMode = UIViewContentModeScaleAspectFill;
    msg_imageView.layer.cornerRadius = 4.0f;
    msg_imageView.clipsToBounds = YES;
    [self.contentView addSubview:msg_imageView];
    self.msg_imageView = msg_imageView;
    [msg_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(identityImgView.mas_bottom).mas_offset(10);
        make.width.height.mas_equalTo(80);
    }];
    @weakify(self);
    [msg_imageView addTapGestureRecognizer:^{
        @strongify(self);
        [self imageViewAction];
    }];
    
    UIView *imageMaskView = [[UIView alloc] init];
    [imageMaskView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.4]];
    imageMaskView.layer.cornerRadius = 4.0f;
    [self.contentView addSubview:imageMaskView];
    imageMaskView.hidden = YES;
    self.imageMaskView = imageMaskView;
    [imageMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(msg_imageView);
    }];
    
    UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [imageMaskView addSubview:indicatorView];
    self.indicatorView = indicatorView;
    [indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(20);
        make.center.mas_equalTo(0);
    }];
}

@end
