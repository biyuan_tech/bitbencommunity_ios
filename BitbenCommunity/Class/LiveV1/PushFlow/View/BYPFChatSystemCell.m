//
//  BYPFChatSystemCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/31.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPFChatSystemCell.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

#import "BYPFChatModel.h"

@interface BYPFChatSystemCell ()

/** titleLab */
@property (nonatomic ,strong) YYLabel *titleLab;

@end

@implementation BYPFChatSystemCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYPFChatModel *model = object[indexPath.row];
    if (model.msg_type == BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW) {
        NSRange range = [model.title rangeOfString:model.userName];
        @weakify(self);
        [model.attributedString yy_setTextHighlightRange:range color:kColorRGBValue(0xea9838) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            @strongify(self);
            [self sendActionName:@"userNameAction" param:@{@"user_id":model.user_id} indexPath:self.indexPath];
        }];
    }else if (model.msg_type == BY_SIM_MSG_TYPE_CUSTOME) {
        NSRange range = [model.title rangeOfString:model.userName];
        NSRange hostRange = [model.title rangeOfString:model.customData[@"receicer_user_Name"]];
        @weakify(self);
        [model.attributedString yy_setTextHighlightRange:range color:kColorRGBValue(0x8f8f8f) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            @strongify(self);
            [self sendActionName:@"userNameAction" param:@{@"user_id":model.user_id} indexPath:self.indexPath];
        }];
        [model.attributedString yy_setTextHighlightRange:hostRange color:kColorRGBValue(0x8f8f8f) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            @strongify(self);
            NSString *userId = model.customData[@"receicer_user_Id"];
            [self sendActionName:@"userNameAction" param:@{@"user_id":nullToEmpty(userId)} indexPath:self.indexPath];
        }];
    }
    
    self.titleLab.attributedText = model.attributedString;
    [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(model.topBottomInset);
        make.height.mas_equalTo(ceil(model.titleSize.height));
    }];
}

- (void)setContentView{
    YYLabel *titleLab = [[YYLabel alloc] init];
    titleLab.font = [UIFont systemFontOfSize:14];
    titleLab.textColor = kColorRGBValue(0x313131);
    titleLab.numberOfLines = 0;
    titleLab.textAlignment = NSTextAlignmentLeft;
    titleLab.lineBreakMode = NSLineBreakByCharWrapping;
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(6, 15, 6, 15));
    }];
}
@end
