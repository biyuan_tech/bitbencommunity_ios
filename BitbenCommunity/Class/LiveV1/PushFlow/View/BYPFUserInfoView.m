//
//  BYPFUserInfoView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/2.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPFUserInfoView.h"
#import "BYPersonHomeHeaderModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>
#import "BYReportSheetView.h"
#import "BYPersonHomeController.h"

@interface BYPFUserInfoView ()
{
    CGFloat _contentH;
}

/** maskViewq */
@property (nonatomic ,strong) UIControl *maskView;
/** contentView */
@property (nonatomic ,strong) UIView *contentView;
/** userlogo */
@property (nonatomic ,strong) PDImageView *userlogoImgView;
/** userName */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 成就logo */
@property (nonatomic ,strong) PDImageView *achievementlogo;
/** 成就名 */
@property (nonatomic ,strong) UILabel *achievementName;
/** 举报 */
@property (nonatomic ,strong) UIButton *reportBtn;
/** 禁言 */
@property (nonatomic ,strong) UIButton *forbidBtn;
/** 签名 */
@property (nonatomic ,strong) YYLabel *signLab;
/** 粉丝数 */
@property (nonatomic ,strong) UILabel *fansNumLab;
/** 关注数 */
@property (nonatomic ,strong) UILabel *attentionNumLab;
/** 关注状态 */
@property (nonatomic ,strong) UIButton *attentionBtn;
/** model */
@property (nonatomic ,strong) BYPersonHomeHeaderModel *userModel;
/** superVIew */
@property (nonatomic ,weak) UIView *fathureView;
/** reportView */
@property (nonatomic ,strong) BYReportSheetView *reportSheetView;

@end

@implementation BYPFUserInfoView

- (instancetype)initWithFathureView:(UIView *)fathureView{
    self = [super init];
    if (self) {
        self.fathureView = fathureView;
        self.frame = self.fathureView.bounds;
        [self setContentView];
    }
    return self;
}

- (void)setUser_id:(NSString *)user_id{
    _user_id = user_id;
    [self resetData];
    [self loadRequestGetPersonData];
}

- (void)resetData{
    self.userlogoImgView.image = nil;
    self.userNameLab.text = @"";
    self.signLab.text = @"";
    self.fansNumLab.text = @"0";
    self.attentionNumLab.text = @"0";
    self.attentionBtn.selected = NO;
    self.achievementName.hidden = YES;
    self.achievementlogo.hidden = YES;
    self.reportBtn.hidden = [_user_id isEqualToString:self.model.user_id] ? NO : YES;
    if (_hasForbid && ![_user_id isEqualToString:self.model.user_id]) {
        self.forbidBtn.hidden = NO;
    }else {
        self.forbidBtn.hidden = YES;
    }
}

- (void)reloadData:(BYPersonHomeHeaderModel *)model{
    self.userModel = model;
    [self.userlogoImgView uploadHDImageWithURL:model.head_img callback:nil];
    self.userNameLab.text = model.nickname;

    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:model.self_introduction];
    attributedString.yy_font = [UIFont systemFontOfSize:12];
    attributedString.yy_color = kColorRGBValue(0x969696);
    attributedString.yy_lineSpacing = 4.0;
    attributedString.yy_lineBreakMode = NSLineBreakByCharWrapping;
    attributedString.yy_alignment = NSTextAlignmentCenter;
    self.signLab.attributedText = attributedString;
    YYTextContainer *textContainer = [YYTextContainer new];
    textContainer.size = CGSizeMake(kCommonScreenWidth - 120, 40);
    YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:attributedString];

    [self.signLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(ceil(layout.textBoundingSize.height));
    }];
    
    self.fansNumLab.text = [NSString transformIntegerShow:[model.fans integerValue]];
    self.attentionNumLab.text = [NSString transformIntegerShow:[model.attention integerValue]];
    
    if (model.achievement_badge_list.count) {
        self.achievementName.hidden = NO;
        self.achievementlogo.hidden = NO;
        NSString *bagde_id = self.userModel.achievement_badge_list[0];
        ServerBadgeModel *bagdeModel = [BYCommonTool getAchievementData:bagde_id];
        [self.achievementlogo uploadHDImageWithURL:bagdeModel.picture callback:nil];
        self.achievementName.text = bagdeModel.name;
        [self.achievementName mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(stringGetHeight(bagdeModel.name, 12));
        }];
    }
    
    [self reloadAttentionStatus];
    
    [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(kSafe_Mas_Bottom(252) + ceil(layout.textBoundingSize.height));
    }];
}

- (void)showAnimation{
    [self.fathureView addSubview:self];
    CGFloat detal = 0.0;
    if (CGRectEqualToRect(self.contentView.frame, CGRectZero)) {
        detal = 0.2;
        [self setNeedsLayout];
    }
    self.hidden = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(detal * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.2 animations:^{
            [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
            }];
            [self layoutIfNeeded];
        }];
    });
}

- (void)hiddenAnimation{
    CGFloat height = self.contentView.size_height;
    [UIView animateWithDuration:0.2 animations:^{
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(height);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
        [self removeFromSuperview];
    }];
}

- (void)reloadAttentionStatus{
    self.attentionBtn.selected = self.userModel.isAttention;
    NSString *title = self.userModel.isAttention ? @"已关注" : @"关注";
    [self.attentionBtn setBy_attributedTitle:@{@"title":title,
                                               NSFontAttributeName:[UIFont systemFontOfSize:15],
                                               NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                               } forState:UIControlStateNormal];
    if ([self.userModel.user_id isEqualToString:self.model.user_id]) {
        if (self.didUpdateHostAttention) {
            self.didUpdateHostAttention(self.userModel.isAttention);
        }
    }
}

#pragma mark - action
- (void)maskViewAction{
    [self hiddenAnimation];
}

- (void)reportBtnAction{
    [self hiddenAnimation];
    self.reportSheetView.theme_id = self.model.live_record_id;
    self.reportSheetView.theme_type = BY_THEME_TYPE_LIVE;
    self.reportSheetView.user_id = self.model.user_id;
    [self.reportSheetView showAnimation];
}

- (void)userlogoAction{
    if (self.noEnable) return;
    BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
    personHomeController.user_id = self.user_id;
    [CURRENT_VC.navigationController pushViewController:personHomeController animated:YES];
}

- (void)attentionBtnAction{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:_userModel.user_id
                                             isAttention:!_attentionBtn.selected
                                            successBlock:^(id object) {
                                                @strongify(self);
                                                self.userModel.isAttention = !self.attentionBtn.selected;
                                                [self reloadAttentionStatus];
                                            } faileBlock:nil];
}

- (void)forbidBtnAction{
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:@"禁言提醒" message:@"确定要禁言该用户么" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[BYSIMManager shareManager] forbidMessage:YES user_id:_user_id suc:^{
            showToastView(@"禁言成功", CURRENT_VC.view);
        } fail:^{
            showToastView(@"禁言失败", CURRENT_VC.view);
        }];
    }];
    [sheetController addAction:confirmAction];
    [sheetController addAction:cancelAction];
    [CURRENT_VC.navigationController presentViewController:sheetController animated:YES completion:nil];

}

#pragma mark - request
- (void)loadRequestGetPersonData{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetPersonData:self.user_id successBlock:^(id object) {
        @strongify(self);
        [self reloadData:object];
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - configUI

- (void)setContentView{
    self.maskView = [[UIControl alloc] init];
    [self.maskView addTarget:self action:@selector(hiddenAnimation) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    self.contentView = [[UIView alloc] init];
    [self.contentView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(kSafe_Mas_Bottom(263));
        make.height.mas_greaterThanOrEqualTo(kSafe_Mas_Bottom(263));
    }];
    
    UIView *subView = [[UIView alloc] init];
    [subView setBackgroundColor:[UIColor whiteColor]];
    [self.contentView addSubview:subView];
    [subView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(33, 0, 0, 0));
    }];
    
    self.reportBtn = [UIButton by_buttonWithCustomType];
    [self.reportBtn setBy_attributedTitle:@{@"title":@"举报",
                                            NSFontAttributeName:[UIFont systemFontOfSize:12],
                                            NSForegroundColorAttributeName:kColorRGBValue(0x969696)
                                            } forState:UIControlStateNormal];
    [self.reportBtn addTarget:self action:@selector(reportBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.reportBtn];
    [self.reportBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(33);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(41);
        make.width.mas_equalTo(54);
    }];
    
    [self.contentView addSubview:self.forbidBtn];
    [self.forbidBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.reportBtn);
    }];
    
    UIView *userlogoBgView = [[UIView alloc] init];
    [userlogoBgView setBackgroundColor:[UIColor whiteColor]];
    userlogoBgView.layer.cornerRadius = 33;
    [self.contentView addSubview:userlogoBgView];
    [userlogoBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(66);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    
    self.userlogoImgView = [[PDImageView alloc] init];
    self.userlogoImgView.contentMode = UIViewContentModeScaleAspectFill;
    self.userlogoImgView.userInteractionEnabled = YES;
    self.userlogoImgView.layer.cornerRadius = 30;
    self.userlogoImgView.clipsToBounds = YES;
    [self.contentView addSubview:self.userlogoImgView];
    [self.userlogoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(userlogoBgView);
        make.width.height.mas_equalTo(60);
    }];
    @weakify(self);
    [self.userlogoImgView addTapGestureRecognizer:^{
        @strongify(self);
        [self userlogoAction];
    }];
    
    self.userNameLab = [[UILabel alloc] init];
    self.userNameLab.font = [UIFont boldSystemFontOfSize:16];
    self.userNameLab.textColor = kColorRGBValue(0x313131);
    self.userNameLab.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.userNameLab];
    [self.userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(self.userlogoImgView.mas_bottom).mas_offset(12);
        make.height.mas_equalTo(self.userNameLab.font.pointSize);
    }];
    
    UIView *achievementView = [[UIView alloc] init];
    [self.contentView addSubview:achievementView];
    
    self.achievementlogo = [[PDImageView alloc] init];
    [achievementView addSubview:self.achievementlogo];
    [self.achievementlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(16);
        make.left.mas_equalTo(0);
        make.centerY.mas_equalTo(0);
    }];
    
    self.achievementName = [[UILabel alloc] init];
    self.achievementName.font = [UIFont systemFontOfSize:12];
    self.achievementName.textColor = kColorRGBValue(0x313131);
    [achievementView addSubview:self.achievementName];
    [self.achievementName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.achievementlogo.mas_right).mas_offset(5);
        make.height.mas_equalTo(self.achievementName.font.pointSize);
        make.centerY.mas_equalTo(0);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    [achievementView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.userNameLab.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(16);
        make.centerX.mas_equalTo(0);
        make.right.mas_equalTo(self.achievementName);
    }];
    
    self.signLab = [[YYLabel alloc] init];
    self.signLab.numberOfLines = 2.0f;
    self.signLab.lineBreakMode = NSLineBreakByCharWrapping;
    [self.contentView addSubview:self.signLab];
    [self.signLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth - 120);
        make.height.mas_greaterThanOrEqualTo(0);
        make.top.mas_equalTo(achievementView.mas_bottom).mas_offset(10);
        make.centerX.mas_equalTo(0);
    }];
    
    self.fansNumLab = [[UILabel alloc] init];
    self.fansNumLab.font = [UIFont boldSystemFontOfSize:15];
    self.fansNumLab.textColor = kColorRGBValue(0x313131);
    [self.contentView addSubview:self.fansNumLab];
    [self.fansNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(-kCommonScreenWidth/4);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(self.fansNumLab.font.pointSize);
        make.top.mas_equalTo(self.signLab.mas_bottom).mas_offset(14);
    }];
    
    UILabel *fansLab = [UILabel by_init];
    [fansLab setBy_font:12];
    fansLab.textColor = kColorRGBValue(0x969696);
    fansLab.text = @"粉丝";
    [self.contentView addSubview:fansLab];
    [fansLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.fansNumLab.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(fansLab.font.pointSize);
        make.width.mas_equalTo(stringGetWidth(fansLab.text, 12));
        make.centerX.mas_equalTo(self.fansNumLab);
//        make.bottom.mas_equalTo(-kSafe_Mas_Bottom(70));
    }];
    
    self.attentionNumLab = [[UILabel alloc] init];
    self.attentionNumLab.font = [UIFont boldSystemFontOfSize:15];
    self.attentionNumLab.textColor = kColorRGBValue(0x313131);
    [self.contentView addSubview:self.attentionNumLab];
    [self.attentionNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(kCommonScreenWidth/4);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(self.attentionNumLab.font.pointSize);
        make.top.mas_equalTo(self.signLab.mas_bottom).mas_offset(14);
    }];
    
    UILabel *attentionLab = [UILabel by_init];
    [attentionLab setBy_font:12];
    attentionLab.textColor = kColorRGBValue(0x969696);
    attentionLab.text = @"关注";
    [self.contentView addSubview:attentionLab];
    [attentionLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.attentionNumLab.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(attentionLab.font.pointSize);
        make.width.mas_equalTo(stringGetWidth(attentionLab.text, 12));
        make.centerX.mas_equalTo(self.attentionNumLab);
    }];
    
    self.attentionBtn = [UIButton by_buttonWithCustomType];
    [self.attentionBtn setBy_attributedTitle:@{@"title":@"关注",
                                               NSFontAttributeName:[UIFont systemFontOfSize:15],
                                               NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                               } forState:UIControlStateNormal];
    [self.attentionBtn setBy_attributedTitle:@{@"title":@"已关注",
                                               NSFontAttributeName:[UIFont systemFontOfSize:15],
                                               NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                               } forState:UIControlStateSelected];
    self.attentionBtn.selected = NO;
    [self.attentionBtn addTarget:self action:@selector(attentionBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.attentionBtn];
    [self.attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(49);
        make.bottom.mas_equalTo(-kSafeAreaInsetsBottom);
    }];
    
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:kColorRGBValue(0xededed)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(-kSafe_Mas_Bottom(49));
    }];
    
    self.reportSheetView = [BYReportSheetView initReportSheetViewShowInView:self.fathureView];
}

- (UIButton *)forbidBtn{
    if (!_forbidBtn) {
        _forbidBtn = [UIButton by_buttonWithCustomType];
        [_forbidBtn setBy_attributedTitle:@{@"title":@"禁言",
                                            NSFontAttributeName:[UIFont systemFontOfSize:12],
                                            NSForegroundColorAttributeName:kColorRGBValue(0x969696)
                                            } forState:UIControlStateNormal];
        [_forbidBtn addTarget:self action:@selector(forbidBtnAction) forControlEvents:UIControlEventTouchUpInside];
        _forbidBtn.hidden = YES;
    }
    return _forbidBtn;
}


@end
