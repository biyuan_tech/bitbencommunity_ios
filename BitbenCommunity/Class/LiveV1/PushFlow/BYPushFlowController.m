//
//  BYPushFlowController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPushFlowController.h"
#import "BYPushFlowViewModel.h"

@interface BYPushFlowController ()<NetworkAdapterSocketDelegate>

@end

@implementation BYPushFlowController

- (void)dealloc
{
    [(BYPushFlowViewModel *)self.viewModel destoryTimer];
    [[BYSIMManager shareManager] resetScoketStatus];
}

- (Class)getViewModelClass{
    return [BYPushFlowViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    self.hasCancelSocket = NO;
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data{
    [[BYSIMManager shareManager] webSocketReceiveData:type data:data];
}

-(void)webSocketDidConnectedWithSocket:(WebSocketConnection *)webSocketConnection connectedStatus:(SocketConnectType)connectedStatus{
    if (webSocketConnection == [NetworkAdapter sharedAdapter].mainRootSocketConnection) {
        if (connectedStatus == SocketConnectTypeOpen) {
            [[BYSIMManager shareManager] webSocketDidConnectedWithSocket:webSocketConnection];
        }
    }
}


@end
