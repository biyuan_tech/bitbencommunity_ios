//
//  BYPFChatModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/31.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYPFChatModel : BYCommonModel

/** attributed */
@property (nonatomic ,strong) NSMutableAttributedString *attributedString;
/** titleSize */
@property (nonatomic ,assign) CGSize titleSize;
/** 身份 */
@property (nonatomic ,copy) NSString *identity;
/** 是否为主持人 */
@property (nonatomic ,assign) BOOL isHost;
/** userid */
@property (nonatomic ,copy) NSString *user_id;
/** username */
@property (nonatomic ,copy) NSString *userName;
/** 消息图片 */
@property (nonatomic ,copy) NSString *image_url;
/** 消息图片（图片消息未发送时） */
@property (nonatomic ,strong) UIImage *image_notSend;
/** 上下间距 */
@property (nonatomic ,assign) CGFloat topBottomInset;
/** 消息状态 */
@property (nonatomic ,assign) BY_SIM_MSG_STATE msg_status;
/** 消息类型 */
@property (nonatomic ,assign) BY_SIM_MSG_TYPE msg_type;
/** 自定义消息内容 */
@property (nonatomic ,strong) NSDictionary *customData;

// 推流直播data获取
+ (NSArray *)transTableViewData:(NSArray *)msgs liveModel:(BYCommonLiveModel *)liveModel;

+ (NSArray *)getNoSendImgMsgData:(NSArray *)images liveData:(BYCommonLiveModel *)liveData;

// 个人互动直播data获取
+ (NSArray *)transILiveTableViewData:(NSArray *)msgs liveModel:(BYCommonLiveModel *)liveModel;

@end

NS_ASSUME_NONNULL_END
