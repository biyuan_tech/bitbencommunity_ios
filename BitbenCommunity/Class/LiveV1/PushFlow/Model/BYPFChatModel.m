//
//  BYPFChatModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/31.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPFChatModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

@implementation BYPFChatModel

+ (NSArray *)transTableViewData:(NSArray *)msgs liveModel:(BYCommonLiveModel *)liveModel{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dic in msgs) {
        BYPFChatModel *model = [[BYPFChatModel alloc] init];
        BY_SIM_MSG_TYPE type = [dic[@"content_type"] integerValue];
        model.user_id = dic[@"user_id"];
//        model.isHost = [model.user_id isEqualToString:liveModel.user_id] ? YES : NO;
//        BY_IDENTITY_TYPE identity = [BYPFChatModel verifyIdentithy:liveModel userId:model.user_id];
        BY_IDENTITY_TYPE identity = [dic[@"identity_type"] integerValue];
        switch (identity) {
            case BY_IDENTITY_TYPE_HOST:
                model.identity = @"主持人";
                break;
            case BY_IDENTITY_TYPE_GUEST:
                model.identity = @"嘉宾";
                break;
            case BY_IDENTITY_TYPE_CREATOR:
                model.identity = @"主持人";
                break;
            default:
                model.identity = @"";
                break;
        }
        CGFloat firstLineHeadIndent = model.identity.length ? stringGetWidth(model.identity, 10) + 9 : 0;
        model.userName = dic[@"nickname"];
        model.topBottomInset = 6.0;
        model.msg_type = type;
        if (type == BY_SIM_MSG_TYPE_TEXT) { // 文案消息处理
            NSString *content = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
            
            model.title = [NSString stringWithFormat:@"%@: %@",model.userName,content];
            
            NSMutableAttributedString *attributedstring = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.title)];
            attributedstring.yy_lineSpacing = 4.0f;
            attributedstring.yy_font = [UIFont systemFontOfSize:14];
            attributedstring.yy_color = kColorRGBValue(0x313131);
            [attributedstring yy_setColor:kColorRGBValue(0x8f8f8f) range:NSMakeRange(0, model.userName.length + 2)];
            attributedstring.yy_lineBreakMode = NSLineBreakByCharWrapping;
            attributedstring.yy_firstLineHeadIndent = firstLineHeadIndent;
            YYTextContainer *textContainer = [YYTextContainer new];
            textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
            YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:attributedstring];
            model.titleSize = layout.textBoundingSize;
            model.attributedString = attributedstring;
            model.cellString = @"BYPFChatTextCell";
            model.cellHeight = layout.textBoundingSize.height + 2*model.topBottomInset;
            [array addObject:model];
        }else if (type == BY_SIM_MSG_TYPE_IMAGE){ // 图片消息处理
            model.image_url = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
            model.title = [NSString stringWithFormat:@"%@:",model.userName];
            model.cellString = @"BYPFChatImageCell";
            model.cellHeight = 120;
            [array addObject:model];
        }else if (type == BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW){ // 进入直播间消息处理
            if ([dic[@"type"] isEqualToString:@"join_room"]) {
                model.title = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
                NSMutableAttributedString *attributedstring = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.title)];
                attributedstring.yy_lineSpacing = 4.0f;
                attributedstring.yy_font = [UIFont systemFontOfSize:14];
                attributedstring.yy_color = kColorRGBValue(0x313131);
                NSRange range = [model.title rangeOfString:model.userName];
                if (range.length) {
                    [attributedstring yy_setColor:kColorRGBValue(0xea9838) range:range];
                }
                YYTextContainer *textContainer = [YYTextContainer new];
                textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
                YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:attributedstring];
                model.titleSize = layout.textBoundingSize;
                model.attributedString = attributedstring;
                
                model.cellString = @"BYPFChatSystemCell";
                model.cellHeight = layout.textBoundingSize.height + 2*model.topBottomInset;
                [array addObject:model];
            }
        }else if (type == BY_SIM_MSG_TYPE_CUSTOME) { // 打赏消息处理
            
            NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            model.customData = customData;
//            model.title = [nullToEmpty(customData[@"text"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
            model.title = [NSString stringWithFormat:@"%@ 打赏了主播 %@ %.2fBP",model.userName,customData[@"receicer_user_Name"],[customData[@"amount"] floatValue]];;
            NSMutableAttributedString *attributedstring = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.title)];
            attributedstring.yy_lineSpacing = 4.0f;
            attributedstring.yy_font = [UIFont systemFontOfSize:14];
            attributedstring.yy_color = kColorRGBValue(0x8f8f8f);
            attributedstring.yy_alignment = NSTextAlignmentLeft;
            NSRange range = [model.title rangeOfString:@"打赏了主播"];
            NSRange amountRange = [model.title rangeOfString:[NSString stringWithFormat:@" %.2fBP",[customData[@"amount"] floatValue]]];
            
            if (range.length) {
                [attributedstring yy_setColor:kColorRGBValue(0xea9838) range:range];
            }
            
            if (amountRange.length) {
                [attributedstring yy_setColor:kColorRGBValue(0xea9838) range:amountRange];
            }
            
            YYTextContainer *textContainer = [YYTextContainer new];
            textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
            YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:attributedstring];
            model.titleSize = layout.textBoundingSize;
            model.attributedString = attributedstring;
            
            model.cellString = @"BYPFChatSystemCell";
            model.cellHeight = layout.textBoundingSize.height + 2*model.topBottomInset;
            [array addObject:model];
        }
    }
    return array;
}

+ (NSArray *)getNoSendImgMsgData:(NSArray *)images liveData:(BYCommonLiveModel *)liveData{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < images.count; i ++) {
        BYPFChatModel *model = [[BYPFChatModel alloc] init];
        model.user_id = ACCOUNT_ID;
        model.isHost = YES;
        model.msg_type = BY_SIM_MSG_TYPE_IMAGE;
        model.msg_status = BY_SIM_MSG_STATE_SENDING;
        model.userName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
        model.image_notSend = images[i];
        model.topBottomInset = 6.0;
        model.title = [NSString stringWithFormat:@"%@:",model.userName];
        model.cellString = @"BYPFChatImageCell";
        model.cellHeight = 120;
        [array addObject:model];
    }
    return [array copy];
}

+ (NSArray *)transILiveTableViewData:(NSArray *)msgs liveModel:(BYCommonLiveModel *)liveModel{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dic in msgs) {
        BYPFChatModel *model = [[BYPFChatModel alloc] init];
        BY_SIM_MSG_TYPE type = [dic[@"content_type"] integerValue];
        model.user_id = dic[@"user_id"];
//        BY_IDENTITY_TYPE identity = [BYPFChatModel verifyIdentithy:liveModel userId:model.user_id];
        BY_IDENTITY_TYPE identity = [dic[@"identity_type"] integerValue];
        switch (identity) {
            case BY_IDENTITY_TYPE_HOST:
                model.identity = @"主持人";
                break;
            case BY_IDENTITY_TYPE_GUEST:
                model.identity = @"嘉宾";
                break;
            case BY_IDENTITY_TYPE_CREATOR:
                model.identity = @"主持人";
                break;
            default:
                break;
        }
//        if ([model.user_id isEqualToString:liveModel.user_id]) {
//            model.identity = @"主持人";
//        }
        
        CGFloat firstLineHeadIndent = model.identity.length ? stringGetWidth(model.identity, 10) + 9 : 0;
        model.userName = dic[@"nickname"];
        model.topBottomInset = 6.0;
        model.msg_type = type;
        if (type == BY_SIM_MSG_TYPE_TEXT) { // 文案消息处理
            NSString *content = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
            
            model.title = [NSString stringWithFormat:@"%@: %@",model.userName,content];
            
            NSMutableAttributedString *attributedstring = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.title)];
            attributedstring.yy_lineSpacing = 4.0f;
            attributedstring.yy_font = [UIFont boldSystemFontOfSize:13];
            attributedstring.yy_color = [UIColor whiteColor];
            [attributedstring yy_setColor:kColorRGBValue(0xffd050) range:NSMakeRange(0, model.userName.length + 2)];
            attributedstring.yy_lineBreakMode = NSLineBreakByCharWrapping;
            attributedstring.yy_firstLineHeadIndent = firstLineHeadIndent;
            YYTextContainer *textContainer = [YYTextContainer new];
            textContainer.size = CGSizeMake(kCommonScreenWidth - 135, MAXFLOAT);
            YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:attributedstring];
            model.titleSize = layout.textBoundingSize;
            model.attributedString = attributedstring;
            model.cellString = @"BYILiveChatTextCell";
            model.cellHeight = layout.textBoundingSize.height + 2*model.topBottomInset + 5;
            [array addObject:model];
        }else if (type == BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW){ // 进入直播间消息处理
            if ([dic[@"type"] isEqualToString:@"join_room"]) {
                model.title = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
                NSMutableAttributedString *attributedstring = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.title)];
                attributedstring.yy_lineSpacing = 4.0f;
                attributedstring.yy_font = [UIFont boldSystemFontOfSize:13];
                attributedstring.yy_color = [UIColor whiteColor];
                NSRange range = [model.title rangeOfString:model.userName];
                if (range.length) {
                    [attributedstring yy_setColor:kColorRGBValue(0xffd050) range:range];
                }
                YYTextContainer *textContainer = [YYTextContainer new];
                textContainer.size = CGSizeMake(kCommonScreenWidth - 135, MAXFLOAT);
                YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:attributedstring];
                model.titleSize = layout.textBoundingSize;
                model.attributedString = attributedstring;
                
                model.cellString = @"BYILiveChatSystemCell";
                model.cellHeight = layout.textBoundingSize.height + 2*model.topBottomInset + 5;
                [array addObject:model];
            }
        }else if (type == BY_SIM_MSG_TYPE_CUSTOME) { // 打赏消息处理
            
            NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            model.customData = customData;
            model.title = [NSString stringWithFormat:@"%@ 送给主播%.2fBP",model.userName,[customData[@"amount"] floatValue]];;
            NSMutableAttributedString *attributedstring = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.title)];
            attributedstring.yy_lineSpacing = 4.0f;
            attributedstring.yy_font = [UIFont boldSystemFontOfSize:13];
            attributedstring.yy_color = kColorRGBValue(0x80efff);
            attributedstring.yy_alignment = NSTextAlignmentLeft;
            [attributedstring yy_setColor:kColorRGBValue(0xb9b9b9) range:NSMakeRange(0, model.userName.length)];
            
            YYTextContainer *textContainer = [YYTextContainer new];
            textContainer.size = CGSizeMake(kCommonScreenWidth - 135, MAXFLOAT);
            YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:attributedstring];
            model.titleSize = layout.textBoundingSize;
            model.attributedString = attributedstring;
            
            model.cellString = @"BYILiveChatSystemCell";
            model.cellHeight = layout.textBoundingSize.height + 2*model.topBottomInset + 5;
            [array addObject:model];
        }
    }
    return array;
}

//+ (BY_IDENTITY_TYPE )verifyIdentithy:(BYCommonLiveModel *)model userId:(NSString *)userId{
//    __block BOOL hasResult = NO;
//    if (model.host_list.count) {
//        [model.host_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            if ([obj.user_id isEqualToString:userId]) {
//                *stop = YES;
//                hasResult = YES;
//            }
//        }];
//        if (hasResult) {
//            return BY_IDENTITY_TYPE_HOST;
//        }
//    }
//    if (model.interviewee_list.count) {
//        [model.interviewee_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            if ([obj.user_id isEqualToString:userId]) {
//                *stop = YES;
//                hasResult = YES;
//            }
//        }];
//        if (hasResult) {
//            return BY_IDENTITY_TYPE_GUEST;
//        }
//    }
//    if ([model.user_id isEqualToString:userId]) {
//        return BY_IDENTITY_TYPE_CREATOR;
//    }
//    return BY_IDENTITY_TYPE_NORMAL;
//}

@end
