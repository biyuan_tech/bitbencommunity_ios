//
//  BYPushFlowViewModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYPushFlowViewModel : BYCommonViewModel

- (void)destoryTimer;

@end

NS_ASSUME_NONNULL_END
