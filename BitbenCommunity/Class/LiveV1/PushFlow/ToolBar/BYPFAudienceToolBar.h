//
//  BYPFAudienceToolBar.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYPFAudienceToolBar : UIView

/** 弹幕开关 */
@property (nonatomic ,copy) void (^barrageIsOnHandle)(BOOL isOn);
/** 分享事件 */
@property (nonatomic ,copy) void (^didShareActionHandle)(void);
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 是否可操作(默认YES) */
@property (nonatomic ,assign) BOOL enabled;
/** 禁言状态 */
@property (nonatomic ,assign) BOOL isForbid;


// 刷新点赞数量
- (void)reloadSuppertNum;


@end

NS_ASSUME_NONNULL_END
