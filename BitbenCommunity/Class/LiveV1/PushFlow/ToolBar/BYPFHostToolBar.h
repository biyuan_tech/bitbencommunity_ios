//
//  BYPFHostToolBar.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYIMMessageInputView.h"


NS_ASSUME_NONNULL_BEGIN

@interface BYPFHostToolBar : UIView



/** 弹幕开关 */
@property (nonatomic ,copy) void (^barrageIsOnHandle)(BOOL isOn);
/** 退出直播按钮回调 */
@property (nonatomic ,copy) void(^didExitLiveBtnHandle)(void);
/** 选择发送的图片 */
@property (nonatomic ,copy) void (^didSelectImageDataHandle)(NSArray *images);
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 是否可操作(默认YES) */
@property (nonatomic ,assign) BOOL enabled;


@property (nonatomic ,readonly,strong) BYIMMessageTextView *inputView;

// 刷新点赞数量
- (void)reloadSuppertNum;

@end

NS_ASSUME_NONNULL_END
