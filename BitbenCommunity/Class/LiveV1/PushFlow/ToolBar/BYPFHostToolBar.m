//
//  BYPFHostToolBar.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPFHostToolBar.h"
#import "BYSwitch.h"
#import "GWAssetsLibraryViewController.h"
#import "BYLikeAnimationView.h"
#import "BYILiveForbidListView.h"


#import "BYIMManager+SendMsg.h"

static NSInteger contentH = 49;

@interface BYPFHostToolBar ()<UITextViewDelegate>

/** 弹幕开关 */
@property (nonatomic ,strong) BYSwitch *barrageSwitch;
/** 输入框 */
@property (nonatomic ,strong) BYIMMessageTextView *inputView;
/** 消息输入view */
@property (nonatomic ,strong) UIView *msgContentView;
/** 底部遮罩 */
@property (nonatomic ,strong) UIControl *msgContentMaskView;
/** placelab */
@property (nonatomic ,strong) UILabel *placeholderLab;
/** 消息触发去 */
@property (nonatomic ,strong) UIControl *msgView;
/** 图片选择按钮 */
@property (nonatomic ,strong) UIButton *selImgBtn;
/** 分享按钮 */
@property (nonatomic ,strong) UIButton *forbidBtn;
/** 点赞按钮 */
@property (nonatomic ,strong) UIButton *likeBtn;
/** 结束直播按钮 */
@property (nonatomic ,strong) UIButton *exitLiveBtn;
/** 点赞数量 */
@property (nonatomic ,strong) UILabel *suppertNumLab;
@property (nonatomic ,strong) UIImageView *suppertNumBg;
/** 点赞效果 */
@property (nonatomic ,strong) BYLikeAnimationView *animationView;
/** titleLab */
@property (nonatomic ,strong) UILabel *titleLab;
/** 禁言列表 */
@property (nonatomic ,strong) BYILiveForbidListView *forbidListView;

@end

@implementation BYPFHostToolBar

- (void)dealloc
{
    [_msgContentMaskView removeFromSuperview];
    [_msgContentView removeFromSuperview];
    _msgContentView = nil;
    _msgContentMaskView = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.enabled = YES;
        [self addNSNotification];
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setContentView];
    }
    return self;
}

- (void)didMoveToSuperview{
    if (self.superview) {
        [self configMsgContentView];
    }
}

- (BOOL)resignFirstResponder{
    [_inputView resignFirstResponder];
    return [super resignFirstResponder];
}

- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    [self reloadSuppertNum];
}

- (void)setEnabled:(BOOL)enabled{
    _enabled = enabled;
    _placeholderLab.text = enabled ? @"冒个泡吧…" : @"直播已结束…";
    _titleLab.text = enabled ? @"冒个泡吧…" : @"直播已结束…";
    _inputView.editable = enabled;
    _selImgBtn.enabled = enabled;
}

- (void)reloadSuppertNum{
    NSString *string = [NSString transformIntegerShow:self.model.count_support];
    [UIView animateWithDuration:1.0f animations:^{
        self.suppertNumLab.text = string;
    }];
    [self.suppertNumLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(string, 10) + 6);
    }];
}

- (void)resetToolView{
    [_inputView resignFirstResponder];
    _inputView.text = @"";
    
    self.placeholderLab.hidden      = NO;
    [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(contentH);
    }];
}

- (CGFloat)calculateTextViewHeight:(UITextView *)textView text:(NSString *)text{
    CGSize constraninSize = CGSizeMake(textView.contentSize.width, MAXFLOAT);
    CGSize size = [textView sizeThatFits:constraninSize];
    return size.height + 14 + textView.textContainerInset.top;
}

#pragma mark - action
- (void)switchAction{
    if (self.barrageIsOnHandle) {
        self.barrageIsOnHandle(self.barrageSwitch.on);
    }
}

- (void)selImgBtnAction{
    GWAssetsLibraryViewController *assetVC = [[GWAssetsLibraryViewController alloc] init];
    @weakify(self);
    [assetVC selectImageArrayFromImagePickerWithMaxSelected:9 andBlock:^(NSArray *selectedImgArr) {
        @strongify(self);
        if (self.didSelectImageDataHandle) {
            self.didSelectImageDataHandle(selectedImgArr);
        }
    }];
    [CURRENT_VC.navigationController pushViewController:assetVC animated:YES];
}

- (void)forbidBtnAction{
    @weakify(self);
    [[BYSIMManager shareManager] getForbidMembers:^(NSArray * _Nonnull members) {
        @strongify(self);
        self.forbidListView.members = members;
        [self.forbidListView showAnimation];
    } fail:^{
        showToastView(@"获取禁言列表失败", CURRENT_VC.view);
    }];
}

- (void)exitLiveBtnAction{
//    NSString *title = _model.status == BY_LIVE_STATUS_LIVING ? @"确定要结束直播吗？" : @"确定现在退出吗？";
    NSString *title = @"确定要结束直播吗？";
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    @weakify(self);
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [self resignFirstResponder];
        if (self.didExitLiveBtnHandle) self.didExitLiveBtnHandle();
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [sheetController addAction:confirmAction];
    [sheetController addAction:cancelAction];
    [CURRENT_VC presentViewController:sheetController animated:YES completion:nil];
}

// 冒泡
- (void)triggerMsgTextView{
    [self.inputView becomeFirstResponder];
}

- (void)sendMessageAction{
    NSString *string = [self.inputView.text removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
    if (!string.length) return;
    
    BYSIMMessage *msg = [[BYSIMMessage alloc] init];
    msg.msg_type = BY_SIM_MSG_TYPE_TEXT;
    msg.msg_sender = [AccountModel sharedAccountModel].account_id;
    msg.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
    msg.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
    msg.time = [NSDate getCurrentTimeStr];
    BYSIMTextElem *elem = [[BYSIMTextElem alloc] init];
    elem.text = string;
    msg.elem = elem;
    [[BYSIMManager shareManager] sendMessage:msg suc:nil fail:nil];
    [self resetToolView];
}

- (void)likeBtnAction:(UIButton *)sender{
    if (!self.model.isSupport) {
        @weakify(self);
        [[BYLiveHomeRequest alloc] loadRequestLiveGuestOpearType:BY_GUEST_OPERA_TYPE_UP theme_id:self.model.live_record_id theme_type:BY_THEME_TYPE_LIVE receiver_user_id:self.model.user_id successBlock:^(id object) {
            @strongify(self);
            self.model.isSupport = YES;
            self.model.count_support++;
            [self reloadSuppertNum];
        } faileBlock:^(NSError *error) {
            showToastView(@"顶操作失败", kCommonWindow);
        }];
    }
    CGRect rect = [sender convertRect:sender.bounds toView:self.superview];
    CGPoint point = CGPointMake(rect.origin.x, rect.origin.y);
    [self.animationView beginUpAniamtion:self.superview point:point type:BY_LIKE_ANIMATION_TYPE_PF_UP];}

#pragma mark - UITextViewDelegate 文本输入代理

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"] && textView.text.length) {
        [textView resignFirstResponder];
        [self sendMessageAction];
        return YES;
    }else if ([text isEqualToString:@"\n"]) {
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length) {
        self.placeholderLab.hidden      = YES;
    }else{
        self.placeholderLab.hidden      = NO;
    }
    CGFloat maxHeight = 70;
    CGFloat height = [self calculateTextViewHeight:textView text:textView.text];
    textView.scrollEnabled = height > maxHeight ? YES : NO;
    if (height < maxHeight ) {
        [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height < contentH ? contentH : height);
        }];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length) {
        self.placeholderLab.hidden      = YES;
    }else{
        self.placeholderLab.hidden      = NO;
    }
}

#pragma mark - keyBoardNSNotification

- (void)keyBoardWillShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardDidShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardWillHidden:(NSNotification *)notic{
    if (self.superview && self.inputView.isFirstResponder) {
        [UIView animateWithDuration:0.1 animations:^{
            self.msgContentMaskView.hidden = YES;
            [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(70);
                make.left.right.mas_equalTo(0);
            }];
            [kCommonWindow layoutIfNeeded];
        }];
    }
}

- (void)keyBoardShowAnimation:(NSValue *)value{
    if (self.superview && self.inputView.isFirstResponder) {
        CGFloat detalY = [value CGRectValue].size.height;
        [UIView animateWithDuration:0.1 animations:^{
            self.msgContentMaskView.hidden = NO;
            [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.right.mas_equalTo(0);
                make.bottom.mas_equalTo(-detalY);
            }];
            [kCommonWindow layoutIfNeeded];
        }];
    }
}

#pragma mark - regisetNSNotic

- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - configUI

- (void)setContentView{

    // 消息触发区
    UIControl *msgView = [[UIControl alloc] init];
    [msgView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    msgView.layer.cornerRadius = 4.0f;
    [msgView addTarget:self action:@selector(triggerMsgTextView) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:msgView];
    self.msgView = msgView;
    [msgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(7);
        make.height.mas_equalTo(36);
        make.right.mas_equalTo(-227);
    }];

    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    titleLab.textColor = kColorRGBValue(0x8f8f8f);
    titleLab.text = @"冒个泡吧…";
    [msgView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(14);
        make.centerY.mas_equalTo(0);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.right.mas_equalTo(0);
    }];
   
    [self addSubview:self.selImgBtn];
    [self addSubview:self.forbidBtn];
    [self addSubview:self.likeBtn];
    [self addSubview:self.exitLiveBtn];
    [self addSubview:self.suppertNumLab];
    [self addSubview:self.suppertNumBg];
    [self insertSubview:self.suppertNumBg belowSubview:self.suppertNumLab];
    
    [self.selImgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.msgView.mas_right).mas_offset(15);
        make.centerY.mas_equalTo(self.msgView);
        make.width.height.mas_equalTo(36);
    }];
    
    [self.forbidBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.selImgBtn.mas_right).mas_offset(15);
        make.centerY.mas_equalTo(self.msgView);
        make.width.height.mas_equalTo(36);
    }];
    
    [self.likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.forbidBtn.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(self.msgView);
        make.width.height.mas_equalTo(36);
    }];
    
    [self.exitLiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.centerY.mas_equalTo(self.msgView);
        make.width.mas_equalTo(55);
        make.height.mas_equalTo(contentH);
    }];
    
    [self.suppertNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(14);
        make.top.mas_equalTo(5);
        make.centerX.mas_equalTo(self.likeBtn).mas_offset(14);
    }];
    
    [self.suppertNumBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.suppertNumLab);
        make.height.mas_equalTo(14);
        make.top.mas_equalTo(5);
        make.centerX.mas_equalTo(self.likeBtn).mas_offset(14);
    }];
    
}

- (void)configMsgContentView{
    
    self.msgContentMaskView = [[UIControl alloc] init];
    self.msgContentMaskView.hidden = YES;
    [self.msgContentMaskView addTarget:self action:@selector(resignFirstResponder) forControlEvents:UIControlEventTouchUpInside];
    [kCommonWindow addSubview:self.msgContentMaskView];
    [self.msgContentMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    self.msgContentView = [UIView by_init];
    [self.msgContentView setBackgroundColor:[UIColor whiteColor]];
    [kCommonWindow addSubview:self.msgContentView];
    [self.msgContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(contentH);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(contentH);
    }];
    
    self.inputView = [[BYIMMessageTextView alloc] init];
    self.inputView.layer.cornerRadius = 4.0f;
    [self.inputView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    self.inputView.tintColor = kBgColor_238;
    self.inputView.font = [UIFont systemFontOfSize:16];
    self.inputView.textContainerInset = UIEdgeInsetsMake(7, 8, 0, 5);
    self.inputView.textContainer.lineFragmentPadding = 0;
    self.inputView.returnKeyType = UIReturnKeySend;
    self.inputView.delegate = self;
    [self.msgContentView addSubview:self.inputView];
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 61, 7, 58));
    }];
    
    self.placeholderLab = [UILabel by_init];
    [self.placeholderLab setBy_font:14];
    self.placeholderLab.textColor = kColorRGBValue(0x8f8f8f);
    self.placeholderLab.text = @"冒个泡吧…";
    [self.msgContentView addSubview:self.placeholderLab];
    @weakify(self);
    [self.placeholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.inputView).mas_offset(14);
        make.centerY.mas_equalTo(self.inputView).mas_offset(0);
        make.height.mas_equalTo(self.placeholderLab.font.pointSize);
        make.right.mas_equalTo(self.inputView);
    }];
    
    self.barrageSwitch = [[BYSwitch alloc] initWithFrame:CGRectMake(15, 15, 35, 22)];
    [self.barrageSwitch setThumbImage:[UIImage imageNamed:@"micro_barrage"] forOn:YES];
    [self.barrageSwitch setThumbImage:[UIImage imageNamed:@"micro_barrage"] forOn:NO];
    [self.barrageSwitch setOn:YES animated:YES];
    [self.barrageSwitch addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventValueChanged];
    [self.msgContentView addSubview:self.barrageSwitch];
    [self.barrageSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(self.inputView);
        make.width.mas_equalTo(35);
        make.height.mas_equalTo(22);
    }];
    
    UIButton *sendBtn = [UIButton by_buttonWithCustomType];
    [sendBtn setBy_attributedTitle:@{@"title":@"发送",
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                     } forState:UIControlStateNormal];
    [sendBtn setBy_attributedTitle:@{@"title":@"发送",
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                     } forState:UIControlStateDisabled];
    [sendBtn setBackgroundColor:[UIColor whiteColor]];
    [sendBtn addTarget:self action:@selector(sendMessageAction) forControlEvents:UIControlEventTouchUpInside];
    sendBtn.layer.cornerRadius = 4.0f;
    [self.msgContentView addSubview:sendBtn];
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(58);
        make.height.mas_equalTo(contentH);
        make.centerY.mas_equalTo(self.inputView).mas_offset(0);
    }];
}


- (UIButton *)selImgBtn{
    if (!_selImgBtn) {
        _selImgBtn = [UIButton by_buttonWithCustomType];
        [_selImgBtn setBy_imageName:@"videolive_selImg" forState:UIControlStateNormal];
        [_selImgBtn addTarget:self action:@selector(selImgBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selImgBtn;
}

- (UIButton *)forbidBtn{
    if (!_forbidBtn) {
        _forbidBtn = [UIButton by_buttonWithCustomType];
        [_forbidBtn setBy_imageName:@"pushflow_forbid" forState:UIControlStateNormal];
        [_forbidBtn addTarget:self action:@selector(forbidBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _forbidBtn;
}

- (UIButton *)likeBtn{
    if (!_likeBtn) {
        _likeBtn = [UIButton by_buttonWithCustomType];
        [_likeBtn setBy_imageName:@"videolive_like" forState:UIControlStateNormal];
        [_likeBtn addTarget:self action:@selector(likeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeBtn;
}

- (UIButton *)exitLiveBtn{
    if (!_exitLiveBtn) {
        _exitLiveBtn = [UIButton by_buttonWithCustomType];
        [_exitLiveBtn setBy_imageName:@"videolive_exitLive" forState:UIControlStateNormal];
        [_exitLiveBtn addTarget:self action:@selector(exitLiveBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _exitLiveBtn;
}

- (UILabel *)suppertNumLab{
    if (!_suppertNumLab) {
        _suppertNumLab = [UILabel by_init];
        _suppertNumLab.font = [UIFont boldSystemFontOfSize:10];
        _suppertNumLab.backgroundColor = [UIColor clearColor];
        _suppertNumLab.textColor = kColorRGBValue(0xf03434);
        _suppertNumLab.textAlignment = NSTextAlignmentCenter;

    }
    return _suppertNumLab;
}

- (UIImageView *)suppertNumBg{
    if (!_suppertNumBg) {
        _suppertNumBg = [[UIImageView alloc] init];
        _suppertNumBg.backgroundColor = [UIColor whiteColor];
        _suppertNumBg.layer.cornerRadius = 7.0;
        _suppertNumBg.layer.shadowColor = kColorRGBValue(0x575757).CGColor;
        _suppertNumBg.layer.shadowOffset = CGSizeMake(0, 0);
        _suppertNumBg.layer.shadowOpacity = 0.4;
        _suppertNumBg.layer.shadowRadius = 5.0;
    }
    return _suppertNumBg;
}

- (BYLikeAnimationView *)animationView{
    if (!_animationView) {
        _animationView = [[BYLikeAnimationView alloc] init];
    }
    return _animationView;
}

- (BYILiveForbidListView *)forbidListView{
    if (!_forbidListView) {
        _forbidListView = [BYILiveForbidListView initVideoListViewShowInView:CURRENT_VC.view];
        _forbidListView.superViewController = CURRENT_VC;
    }
    return _forbidListView;
}
@end
