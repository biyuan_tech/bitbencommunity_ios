//
//  BYPushFlowSetCodeCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPushFlowSetCodeCell.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

#import "BYPushFlowSetModel.h"

@interface BYPushFlowSetCodeCell ()

/** 推流码 */
@property (nonatomic ,strong) YYLabel *pushUrlLab;
/** model */
@property (nonatomic ,strong) BYPushFlowSetModel *model;

@end

@implementation BYPushFlowSetCodeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYPushFlowSetModel *model = object[indexPath.row];
    self.model = model;
    if (!model.codeUrl.length) return;
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.codeUrl)];
    attributed.yy_lineSpacing = 8.0f;
    attributed.yy_font = [UIFont systemFontOfSize:14];
    attributed.yy_color = kColorRGBValue(0x000000);

    self.pushUrlLab.attributedText = attributed;
    [self.pushUrlLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(ceil(model.codeSize.height));
    }];
}

#pragma mark - action
- (void)copyAction{
    UIViewController *currentController = [BYTabbarViewController sharedController].currentController;
    if (!self.model.codeUrl.length) {
        showToastView(@"暂未获取到推流码！请重新获取！", currentController.view);
        return;
    }
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setString:self.model.codeUrl];
    showToastView(@"已复制推流码", currentController.view);
}

- (void)tutorialBtnActoin{
    
}

#pragma mark - configUI

- (void)setContentView{
    UILabel *codeLab = [UILabel by_init];
    codeLab.text = @"推流码";
    codeLab.font = [UIFont boldSystemFontOfSize:14];
    [self.contentView addSubview:codeLab];
    [codeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(15);
        make.height.mas_equalTo(codeLab.font.pointSize);
        make.width.mas_equalTo(50);
    }];
    
    UIButton *copyBtn = [UIButton by_buttonWithCustomType];
    [copyBtn setBy_attributedTitle:@{@"title":@"复制",
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:kColorRGBValue(0x145cff)
                                     }
                          forState:UIControlStateNormal];
    [copyBtn addTarget:self action:@selector(copyAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:copyBtn];
    [copyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(44);
        make.width.mas_equalTo(57);
        make.top.mas_equalTo(0);
    }];
    
    
    YYLabel *pushUrlLab = [[YYLabel alloc] init];
    pushUrlLab.numberOfLines = 0;
    [self.contentView addSubview:pushUrlLab];
    self.pushUrlLab = pushUrlLab;
    [pushUrlLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(57);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    UIView *pushUrlBgView = [UIView by_init];
    [pushUrlBgView setBackgroundColor:[UIColor whiteColor]];
    [self.contentView addSubview:pushUrlBgView];
    [self.contentView insertSubview:pushUrlBgView belowSubview:pushUrlLab];
    [pushUrlBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(pushUrlLab).mas_offset(-13);
        make.bottom.mas_equalTo(pushUrlLab).mas_offset(17);
    }];
    
    
    UIView *tutorialView = [UIView by_init];
    [tutorialView setBackgroundColor:[UIColor whiteColor]];
    [self.contentView addSubview:tutorialView];
    [tutorialView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(pushUrlBgView.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(44);
    }];
    
    UILabel *tutorialLab = [UILabel by_init];
    [tutorialLab setBy_font:14];
    tutorialLab.text = @"推流直播教程";
    tutorialLab.textColor = kColorRGBValue(0x000000);
    [self.contentView addSubview:tutorialLab];
    [tutorialLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(tutorialView);
        make.height.mas_equalTo(tutorialLab.font.pointSize);
        make.width.mas_equalTo(90);
    }];
    
    UIButton *tutorialBtn = [UIButton by_buttonWithCustomType];
//    [tutorialBtn setBy_attributedTitle:@{@"title":@"查看",
//                                         NSFontAttributeName:[UIFont systemFontOfSize:14],
//                                         NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
//                                         }
//                              forState:UIControlStateNormal];
    [tutorialBtn setBy_attributedTitle:@{@"title":@"敬请期待",
                                         NSFontAttributeName:[UIFont systemFontOfSize:14],
                                         NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                         }
                              forState:UIControlStateNormal];
    [tutorialBtn addTarget:self action:@selector(tutorialBtnActoin) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:tutorialBtn];
    [tutorialBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(44);
//        make.width.mas_equalTo(57);
        make.width.mas_equalTo(80);
        make.centerY.mas_equalTo(tutorialView);
    }];
    
}

@end
