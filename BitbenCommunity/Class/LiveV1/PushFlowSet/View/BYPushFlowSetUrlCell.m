//
//  BYPushFlowSetUrlCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPushFlowSetUrlCell.h"
#import "BYPushFlowSetModel.h"

@interface BYPushFlowSetUrlCell ()

/** 推流码 */
@property (nonatomic ,strong) UILabel *codeUrlLab;
/** title */
@property (nonatomic ,strong)  UILabel *codeLab;
/** model */
@property (nonatomic ,strong) BYPushFlowSetModel *model;

@end

@implementation BYPushFlowSetUrlCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYPushFlowSetModel *model = object[indexPath.row];
    self.model = model;
    self.codeLab.text = model.title;
    self.codeUrlLab.text = model.codeUrl;
}

#pragma mark - action
- (void)copyAction{
    UIViewController *currentController = [BYTabbarViewController sharedController].currentController;
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setString:self.model.codeUrl];
    showToastView(@"已复制下载地址", currentController.view);
}

#pragma mark - configUI

- (void)setContentView{
    UILabel *codeLab = [UILabel by_init];
    codeLab.text = @"推流码";
    codeLab.font = [UIFont boldSystemFontOfSize:14];
    [self.contentView addSubview:codeLab];
    self.codeLab = codeLab;
    [codeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(15);
        make.height.mas_equalTo(codeLab.font.pointSize);
        make.width.mas_equalTo(150);
    }];
    
    
    UIView *tutorialView = [UIView by_init];
    [tutorialView setBackgroundColor:[UIColor whiteColor]];
    [self.contentView addSubview:tutorialView];
    [tutorialView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(43);
        make.height.mas_equalTo(44);
    }];
    
    UILabel *codeUrlLab = [UILabel by_init];
    [codeUrlLab setBy_font:14];
    codeUrlLab.textColor = kColorRGBValue(0x000000);
    [self.contentView addSubview:codeUrlLab];
    self.codeUrlLab = codeUrlLab;
    [codeUrlLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(tutorialView);
        make.height.mas_equalTo(codeUrlLab.font.pointSize);
        make.right.mas_equalTo(-90);
    }];
    
    UIButton *copyBtn = [UIButton by_buttonWithCustomType];
    [copyBtn setBy_attributedTitle:@{@"title":@"复制",
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:kColorRGBValue(0x145cff)
                                     }
                          forState:UIControlStateNormal];
    [copyBtn addTarget:self action:@selector(copyAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:copyBtn];
    [copyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(44);
        make.width.mas_equalTo(57);
        make.centerY.mas_equalTo(tutorialView);
    }];
    
}

@end
