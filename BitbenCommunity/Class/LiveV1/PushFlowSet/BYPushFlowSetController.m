//
//  BYPushFlowSetController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPushFlowSetController.h"
#import "BYPushFlowSetViewModel.h"

@interface BYPushFlowSetController ()

@end

@implementation BYPushFlowSetController

- (Class)getViewModelClass{
    return [BYPushFlowSetViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"推流直播";
}


@end
