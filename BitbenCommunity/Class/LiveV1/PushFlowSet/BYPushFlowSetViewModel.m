
//
//  BYPushFlowSetViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPushFlowSetViewModel.h"
#import "BYPushFlowSetController.h"

#import "BYPushFlowSetModel.h"

#define K_VC ((BYPushFlowSetController *)S_VC)
@interface BYPushFlowSetViewModel ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;

@end

@implementation BYPushFlowSetViewModel

- (void)setContentView{
    [self addTableView];
    [self getPushFlowURLRequest];
}

#pragma mark - request
- (void)getPushFlowURLRequest{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetPushURL:K_VC.live_record_id successBlock:^(id object) {
        @strongify(self);
        NSArray *tableData = [BYPushFlowSetModel getTableData:object];
        self.tableView.tableData = tableData;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"推流地址获取失败", S_V_VIEW);
    }];
}

#pragma mark  - configUI

- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    [self.tableView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    self.tableView.group_delegate = self;
    [S_V_VIEW addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

@end
