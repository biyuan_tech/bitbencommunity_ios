//
//  BYPushFlowSetModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYPushFlowSetModel : BYCommonModel

/** 复制链接 */
@property (nonatomic ,copy) NSString *codeUrl;
/** 教程 */
@property (nonatomic ,copy) NSString *tutorialUrl;
/** titleSize */
@property (nonatomic ,assign) CGSize codeSize;

+ (NSArray *)getTableData:(NSDictionary *)dic;

@end

NS_ASSUME_NONNULL_END
