//
//  BYPushFlowSetModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPushFlowSetModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>


@implementation BYPushFlowSetModel

+ (NSArray *)getTableData:(NSDictionary *)dic{
    NSMutableArray *tableData = [NSMutableArray array];
    for (int i = 0; i < 3; i ++) {
        BYPushFlowSetModel *model = [[BYPushFlowSetModel alloc] init];
        if (i == 0) {
            NSString *pushUrl = nullToEmpty(dic[@"pushUrl"]);
            NSString *streamCode = nullToEmpty(dic[@"streamCode"]);
            NSString *string = [NSString stringWithFormat:@"%@%@",pushUrl,streamCode];
            model.codeUrl = string;
            NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(string)];
            attributed.yy_lineSpacing = 8.0f;
            attributed.yy_font = [UIFont systemFontOfSize:14];
            attributed.yy_color = kColorRGBValue(0x000000);
            YYTextContainer *textContainer = [YYTextContainer new];
            textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
            YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:attributed];
            model.codeSize = layout.textBoundingSize;
            model.cellHeight = 44 + layout.textBoundingSize.height + 30 + 54;
        }else if (i == 1) {
            model.title = @"OBS软件下载地址";
            model.codeUrl = @"https://obsproject.com/";
            model.cellHeight = 87;
        }else {
            model.title = @"此刻软件下载地址";
            model.codeUrl = @"http://www.ciscik.com/";
            model.cellHeight = 87;
        }
        model.cellString = i == 0 ? @"BYPushFlowSetCodeCell" : @"BYPushFlowSetUrlCell";
        [tableData addObject:model];
    }
    
    return tableData;
}

@end
