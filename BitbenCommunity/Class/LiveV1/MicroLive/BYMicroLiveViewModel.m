//
//  BYMicroLiveViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYMicroLiveViewModel.h"
#import "HGSegmentedPageViewController.h"
#import "BYMicroLiveSubController.h"
#import "BYMicroLiveChatSubController.h"
#import "ShareRootViewController.h"
#import "BYMicroLiveController.h"

#import "HGCenterBaseTableView.h"
#import "BYReportSheetView.h"

#import "AATAudioTool.h"


#define K_VC ((BYMicroLiveController *)S_VC)
@interface BYMicroLiveViewModel ()<HGSegmentedPageViewControllerDelegate, HGPageViewControllerDelegate,UITableViewDelegate,UITableViewDataSource>

/** tableView */
@property (nonatomic ,strong) HGCenterBaseTableView *tableView;
/** segmentControllView */
@property (nonatomic ,strong) HGSegmentedPageViewController *segmentedPageViewController;
/** 直播区 */
@property (nonatomic ,strong) BYMicroLiveSubController *liveSubController;
/** 互动区 */
@property (nonatomic ,strong) BYMicroLiveChatSubController *chatSubController;
/** 是否可滑动 */
@property (nonatomic ,assign) BOOL cannotScroll;
/** tableFooterView */
@property (nonatomic ,strong) UIView *tableFooterView;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** titleLab */
@property (nonatomic ,strong) UILabel *titleLab;
/** titleLab */
@property (nonatomic ,strong) UILabel *subTitleLab;
/** 虚拟pv数 */
@property (nonatomic ,assign) NSInteger virtual_pv;
/** 心跳计时 */
@property (nonatomic ,strong) NSTimer *roomTimer;
/** 举报 */
@property (nonatomic ,strong) BYReportSheetView *reportSheetView;

@end

@implementation BYMicroLiveViewModel

- (void)dealloc
{
    [[AATAudioTool share] stopPlay];
    [_liveSubController removeFromParentViewController];
    [_chatSubController removeFromParentViewController];
    [_liveSubController.view removeFromSuperview];
    [_chatSubController.view removeFromSuperview];
    [_tableView removeFromSuperview];
    [_segmentedPageViewController removeFromParentViewController];
    [_segmentedPageViewController.view removeFromSuperview];
    [_reportSheetView removeFromSuperview];
    _liveSubController = nil;
    _chatSubController = nil;
    _tableView = nil;
    _segmentedPageViewController = nil;
    _reportSheetView = nil;
}

- (void)destoryTimer{
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
    [[BYCommonTool shareManager] stopTimer];
}

- (void)setContentView{
    self.virtual_pv = K_VC.isHost ? 0 : -1;
    [self addTableView];
    [self addNavigationTitle];
    [self setRightNavigationBtn];
    [self addMessageListener];
    [self loadRequestGetDetail];
    [self setReportView];
    [self loginNotic];
}

#pragma mark - custom
- (void)reloadLiveStatus{
//    UIColor *titleColor;
    
    NSString *titleStr;
    switch (self.model.status) {
        case BY_LIVE_STATUS_SOON:
            self.titleLab.hidden = YES;
            self.subTitleLab.hidden = YES;
            S_VC.navigationTitle = self.model.live_title;
            titleStr   = @"直播·未开始";
            break;
        case BY_LIVE_STATUS_LIVING:
            self.titleLab.hidden = NO;
            self.subTitleLab.hidden = NO;
            S_VC.navigationTitle = @"";
            self.titleLab.text = self.model.live_title;
            self.subTitleLab.text = [NSString stringWithFormat:@"%i人气值",(int)self.model.watch_times];
            titleStr   = @"直播·直播中";
            break;
        default:
            self.titleLab.hidden = NO;
            self.subTitleLab.hidden = NO;
            S_VC.navigationTitle = @"";
            self.titleLab.text = self.model.live_title;
            self.subTitleLab.text = [NSString stringWithFormat:@"%i人气值",(int)self.model.watch_times];
            titleStr   = @"直播·已结束";
            [self destoryTimer];
            break;
    }
    self.segmentedPageViewController.categoryView.titles = @[titleStr,@"互动区"];
}

// 消息回调
- (void)addMessageListener{
    // 消息回调
    @weakify(self);
    [[BYSIMManager shareManager] setMessageListener:^(NSArray * _Nonnull msgs) {
        @strongify(self);
        NSDictionary *dic = msgs[0];
        if (dic) {
            if ([dic[@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
                NSDictionary *customData;
                if ([dic[@"content"] isKindOfClass:[NSString class]]) {
                    // 现返回的为纯json字符串，需要转两次
                    NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                    customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                }
                else{
                    customData = dic[@"content"];
                }
                if ([customData[kBYCustomMessageType] integerValue] == BY_GUEST_OPERA_TYPE_RECEIVE_STREAM) { // 开播消息
                    self.model.status = BY_LIVE_STATUS_LIVING;
                    [self reloadLiveStatus];
                    [self startHeartBeatTimer];
                }
                else if ([customData[kBYCustomMessageType] integerValue] == BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM) { // 关播消息
                    self.model.status = BY_LIVE_STATUS_END;
                    [self reloadLiveStatus];
                }
            }
        }
        
//        NSArray *data = [BYHistoryIMModel getSIMTableData:msgs liveData:self.model];
//        NSArray *array = [self removeSelfSendingMsg:data];
//        if (!array.count) return;
//        [self.tableView addMessagesToBottom:array];
        [self.liveSubController addMessageData:msgs];
        [self.chatSubController addMessageData:msgs];
    }];
}

// 开播时间与当前时间比对
- (void)verifyLiveTime{
    NSDate *date = [NSDate by_dateFromString:self.model.begin_time dateformatter:K_D_F];
    NSDate *nowDate = [NSDate by_date];
    NSComparisonResult result = [date compare:nowDate];
    if (result == NSOrderedAscending ||
        result == NSOrderedSame) {
        @weakify(self);
        if ([self.model.user_id isEqualToString:ACCOUNT_ID]) {
            [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_LIVING cb:^{
                @strongify(self);
                [self reloadLiveStatus];
                [self startHeartBeatTimer];
            }];
        }
    }
    else // 未到开播时间，倒计时自动开播
    {
        // 直播倒计时
        @weakify(self);
        [[BYCommonTool shareManager] timerCutdown:self.model.begin_time cb:^(NSDictionary * _Nonnull param, BOOL isFinish) {
            @strongify(self);
            if (isFinish) {
                @weakify(self);
                if ([self.model.user_id isEqualToString:ACCOUNT_ID]) {
                    [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_LIVING cb:^{
                        @strongify(self);
                        [self reloadLiveStatus];
                        [self startHeartBeatTimer];
                    }];
                }
            }
            else{
            }
        }];
    }
}

- (void)loginNotic{
    @weakify(self);
    [S_VC loginSuccessedNotif:^(BOOL isSuccessed) {
        @strongify(self);
        if (!isSuccessed) return ;
        if (self.model.status == BY_LIVE_STATUS_SOON ||
            self.model.status == BY_LIVE_STATUS_LIVING) {
            @weakify(self);
            [[BYSIMManager shareManager] joinRoom:self.model.group_id suc:^{
                @strongify(self);
                self.chatSubController.toolView.isForbid = [BYSIMManager shareManager].isUserForbid;
                self.liveSubController.audienceToolBar.isForbid = [BYSIMManager shareManager].isUserForbid;
                showDebugToastView(@"进入房间成功", S_V_VIEW);
            } fail:^{
                @strongify(self);
                showDebugToastView(@"进入房间失败", S_V_VIEW);
            }];
        }
    }];
}

#pragma mark - action
// 分享
- (void)shareAction{
    if (!self.model.shareMap) return;
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.transferCopyUrl = self.model.shareMap.share_url;
    shareViewController.collection_status = self.model.collection_status;
    if (![AccountModel sharedAccountModel].hasLoggedIn) {
        shareViewController.hasJubao = NO;
    }else {
        shareViewController.hasJubao = [self.model.user_id isEqualToString:ACCOUNT_ID] ? NO : YES;
    }
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        NSString *mainImgUrl = [PDImageView appendingImgUrl:weakSelf.model.shareMap.share_img];
        NSString *imgurl = [mainImgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [ShareSDKManager shareManagerWithType:shareType title:weakSelf.model.shareMap.share_title desc:weakSelf.model.shareMap.share_intro img:imgurl url:weakSelf.model.shareMap.share_url callBack:NULL];
        [ShareSDKManager shareSuccessBack:shareTypeLive block:NULL];
    }];
    @weakify(self);
    [shareViewController actionClickWithJubaoBlock:^{
        @strongify(self);
        if (!self.model) return ;
        self.reportSheetView.theme_id = self.model.live_record_id;
        self.reportSheetView.theme_type = BY_THEME_TYPE_LIVE;
        self.reportSheetView.user_id = self.model.user_id;
        [self.reportSheetView showAnimation];
    }];
    
    [shareViewController actionClickWithCollectionBlock:^{
        @strongify(self);
        if (!self.model) return ;
        self.model.collection_status = !self.model.collection_status;
    }];
    [shareViewController showInView:S_VC];
}

// 开始发送心跳
- (void)startHeartBeatTimer{
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
    _roomTimer = [NSTimer scheduledTimerWithTimeInterval:kRoomTimerDuration target:self selector:@selector(postHeartBeat) userInfo:nil repeats:YES];
    [self postHeartBeat];
}

#pragma mark - request
- (void)loadRequestGetDetail{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLiveDetail:K_VC.live_record_id successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        if (!object) return ;
        self.model = object;
        self.titleLab.text = self.model.live_title;
        self.subTitleLab.text = [NSString stringWithFormat:@"0人在看"];
        [self reloadLiveStatus];
        if (self.model.status == BY_LIVE_STATUS_SOON) {
            [self verifyLiveTime];
        }else if (self.model.status == BY_LIVE_STATUS_LIVING){
            [self startHeartBeatTimer];
        }
        if (self.model.status == BY_LIVE_STATUS_SOON ||
            self.model.status == BY_LIVE_STATUS_LIVING) {
            @weakify(self);
            [[BYSIMManager shareManager] joinRoom:self.model.group_id suc:^{
                @strongify(self);
                self.liveSubController.model = self.model;
                self.chatSubController.model = self.model;
                showDebugToastView(@"进入房间成功", S_V_VIEW);
            } fail:^{
                @strongify(self);
                showDebugToastView(@"进入房间失败", S_V_VIEW);
            }];
        }else{
            self.liveSubController.model = self.model;
            self.chatSubController.model = self.model;
        }
        
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"获取直播详情失败", S_V_VIEW);
    }];
}

- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status cb:(void(^)(void))cb{
    if (!self.model.real_begin_time.length) {
        self.model.real_begin_time = [NSDate by_stringFromDate:[NSDate by_date] dateformatter:K_D_F];
    }
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveStatus:status live_record_id:_model.live_record_id stream_id:nil real_begin_time:self.model.real_begin_time successBlock:^(id object) {
        @strongify(self);
        self.model.status = status;
        if (cb) cb();
        PDLog(@"更新直播间状态成功");
    } faileBlock:^(NSError *error) {
        PDLog(@"更新直播间状态失败");
    }];
}

// 心跳包请求
- (void)postHeartBeat{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestPostHeartBeat:K_VC.live_record_id
                                             virtual_pv:K_VC.isHost ? _virtual_pv : -1
                                           successBlock:^(id object) {
                                               @strongify(self);
                                               if (object && [object[@"content"] isEqualToString:@"success"]) {
                                                   self.virtual_pv = [object[@"virtualPV"] integerValue];
                                                   self.model.count_support = [object[@"count_support"] integerValue];
                                                   self.subTitleLab.text = [NSString stringWithFormat:@"%i人气值",(int)self.virtual_pv];
                                                   [self.model setValue:@(self.model.count_support) forKey:@"count_support"];

                                               }
                                           } faileBlock:nil];
}

#pragma mark - configUI
- (void)addTableView{
    HGCenterBaseTableView *tableView = [[HGCenterBaseTableView alloc] init];
    tableView.categoryViewHeight = 34;
    tableView.bounces = NO;
    tableView.delegate = self;
    tableView.tableFooterView = self.tableFooterView;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.showsHorizontalScrollIndicator = NO;
    [S_V_VIEW addSubview:tableView];
    [S_V_VIEW sendSubviewToBack:tableView];
    self.tableView = tableView;
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (void)addNavigationTitle{
    self.titleLab = [UILabel by_init];
    self.titleLab.font = [UIFont boldSystemFontOfSize:16];
    self.titleLab.textColor = kColorRGBValue(0x000000);
    self.titleLab.textAlignment = NSTextAlignmentCenter;
//    self.titleLab.text = [NSString stringWithFormat:@"#%@#",K_VC.topic];
    [S_V_NC.navigationBar addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth - 120);
        make.height.mas_equalTo(self.titleLab.font.pointSize);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(5);
    }];
    
    self.subTitleLab = [UILabel by_init];
    self.subTitleLab.font = [UIFont systemFontOfSize:12];
    self.subTitleLab.textColor = kColorRGBValue(0x8f8f8f);
    self.subTitleLab.textAlignment = NSTextAlignmentCenter;
    [S_V_NC.navigationBar addSubview:self.subTitleLab];
    [self.subTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth - 120);
        make.height.mas_equalTo(self.subTitleLab.font.pointSize);
        make.centerX.mas_equalTo(0);
        make.bottom.mas_equalTo(-5);
    }];
}

- (void)setRightNavigationBtn{
    @weakify(self);
    [S_VC rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_article_more"] barHltImage:[UIImage imageNamed:@"icon_article_more"] action:^{
        @strongify(self);
        [self shareAction];
    }];
}

- (void)setReportView{
    self.reportSheetView = [BYReportSheetView initReportSheetViewShowInView:kCommonWindow];
}

- (UIView *)tableFooterView{
    if (!_tableFooterView) {
        _tableFooterView = [[UIView alloc] init];
        _tableFooterView.frame = CGRectMake(0, 0, kCommonScreenWidth, kCommonScreenHeight - kNavigationHeight);
        [S_VC addChildViewController:self.segmentedPageViewController];
        [_tableFooterView addSubview:self.segmentedPageViewController.view];
        [self.segmentedPageViewController didMoveToParentViewController:S_VC];
        @weakify(self);
        [self.segmentedPageViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.edges.equalTo(self.tableFooterView);
        }];
    }
    return _tableFooterView;
}

- (HGSegmentedPageViewController *)segmentedPageViewController {
    if (!_segmentedPageViewController) {
        NSMutableArray *controllers = [NSMutableArray array];
        NSArray *titles = @[@"直播·直播区", @"互动区"];
        for (int i = 0; i < titles.count; i++) {
            HGPageViewController *controller;
            if (i == 0) {
                controller = self.liveSubController;
            } else {
                controller = self.chatSubController;
            }
            controller.delegate = self;
            [controllers addObject:controller];
        }
        _segmentedPageViewController = [[HGSegmentedPageViewController alloc] init];
        _segmentedPageViewController.pageViewControllers = controllers;
        _segmentedPageViewController.categoryView.titles = titles;
        _segmentedPageViewController.categoryView.height = 34;
        _segmentedPageViewController.categoryView.alignment = HGCategoryViewAlignmentCenter;
        _segmentedPageViewController.categoryView.originalIndex = 0;
        _segmentedPageViewController.categoryView.itemSpacing = 10;
        _segmentedPageViewController.categoryView.itemWidth = 100;
//        _segmentedPageViewController.categoryView.leftAndRightMargin = 60;
        _segmentedPageViewController.categoryView.titleNomalFont = [UIFont systemFontOfSize:12];
        _segmentedPageViewController.categoryView.titleSelectedFont = [UIFont boldSystemFontOfSize:12];
        _segmentedPageViewController.categoryView.titleNormalColor = kColorRGBValue(0x323232);
        _segmentedPageViewController.categoryView.titleSelectedColor = kColorRGBValue(0x323232);
        _segmentedPageViewController.categoryView.backgroundColor = [UIColor whiteColor];
        _segmentedPageViewController.categoryView.vernierHeight = 2;
        _segmentedPageViewController.categoryView.vernierWidth = 16;
        _segmentedPageViewController.categoryView.vernierBottomSapce = 0.0f;
        _segmentedPageViewController.categoryView.vernier.backgroundColor = kColorRGBValue(0xea6438);
        _segmentedPageViewController.categoryView.topBorder.hidden = YES;
        _segmentedPageViewController.categoryView.bottomBorder.hidden = YES;
        _segmentedPageViewController.delegate = self;
//        @weakify(self);
//        _segmentedPageViewController.categoryView.selectedItemHelper = ^(NSUInteger index) {
//            @strongify(self);
//            [S_V_VIEW endEditing:YES];
//            [self.segmentedPageViewController.categoryView changeItemToTargetIndex:index];
//        };
    }
    return _segmentedPageViewController;
}

- (BYMicroLiveSubController *)liveSubController{
    if (!_liveSubController) {
        _liveSubController = [[BYMicroLiveSubController alloc] init];
        _liveSubController.identityType = K_VC.identityType;
    }
    return _liveSubController;
}

- (BYMicroLiveChatSubController *)chatSubController{
    if (!_chatSubController) {
        _chatSubController = [[BYMicroLiveChatSubController alloc] init];
//        _chatSubController.isHost = [self.model.user_id isEqualToString:[AccountModel sharedAccountModel].account_id] ? YES : NO;
        _chatSubController.identityType = K_VC.identityType;
    }
    return _chatSubController;
}

@end
