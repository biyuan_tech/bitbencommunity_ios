//
//  BYBarrageModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYBarrageModel.h"
#import <YYText/NSAttributedString+YYText.h>
#import <YYLabel.h>

@implementation BYBarrageModel

+ (NSArray *)getTableData:(NSArray *)respondData liveModel:(BYCommonLiveModel *)liveModel{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dic in respondData) {
        BYBarrageModel *model = [[BYBarrageModel alloc] init];
        model.msg_userId = dic[@"user_id"];
        BY_IDENTITY_TYPE identity = [dic[@"identity_type"] integerValue];
        
        BY_IDENTITY_TYPE selfIdentity = [BYBarrageModel verifyIdentithy:liveModel userId:ACCOUNT_ID];
//        BY_IDENTITY_TYPE identity = [BYBarrageModel verifyIdentithy:liveModel userId:model.msg_userId];
        if (selfIdentity == BY_IDENTITY_TYPE_CREATOR ||
            selfIdentity == BY_IDENTITY_TYPE_GUEST ||
            selfIdentity == BY_IDENTITY_TYPE_HOST) {
            model.isLeft = YES;
        }else{
            model.isLeft = NO;
        }
        BY_SIM_MSG_TYPE type = [dic[@"content_type"] integerValue];
        model.msg_headImg = [NSString stringWithFormat:@"%@%@",[PDImageView getUploadBucket:dic[@"head_img"]],dic[@"head_img"]];
        if (type == BY_SIM_MSG_TYPE_TEXT) {
            if (identity == BY_IDENTITY_TYPE_NORMAL) {
                model.msg_title = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
                
                NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.msg_title)];
                messageAttributed.yy_lineSpacing = 4.0f;
                messageAttributed.yy_font = [UIFont systemFontOfSize:13];
                messageAttributed.yy_color = [UIColor whiteColor];
                YYTextContainer *textContainer = [YYTextContainer new];
                textContainer.size = CGSizeMake(kCommonScreenWidth - 120 - 50, MAXFLOAT);
                YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
                model.titleSize = layout.textBoundingSize;
                model.attributedString = messageAttributed;
                
                model.cellString = @"BYBarrageCell";
                model.cellHeight = layout.textBoundingSize.height + 16 + 8;
                
                [array addObject:model];
            }
        }else if (type == BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW){
            if ([dic[@"type"] isEqualToString:@"join_room"]) {
                model.msg_title = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
                
                NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.msg_title)];
                messageAttributed.yy_lineSpacing = 4.0f;
                messageAttributed.yy_font = [UIFont systemFontOfSize:13];
                messageAttributed.yy_color = [UIColor whiteColor];
                YYTextContainer *textContainer = [YYTextContainer new];
                textContainer.size = CGSizeMake(kCommonScreenWidth - 27 - 87, MAXFLOAT);
                YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
                model.titleSize = layout.textBoundingSize;
                model.attributedString = messageAttributed;
                
                model.cellString = @"BYBarrageSystemCell";
                model.cellHeight = layout.textBoundingSize.height + 12 + 8;
                [array addObject:model];
            }
        }
    }
    return array;
}

+ (BY_IDENTITY_TYPE )verifyIdentithy:(BYCommonLiveModel *)model userId:(NSString *)userId{
    __block BOOL hasResult = NO;
    if (model.host_list.count) {
        [model.host_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.user_id isEqualToString:userId]) {
                *stop = YES;
                hasResult = YES;
            }
        }];
        if (hasResult) {
            return BY_IDENTITY_TYPE_HOST;
        }
    }
    if (model.interviewee_list.count) {
        [model.interviewee_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.user_id isEqualToString:userId]) {
                *stop = YES;
                hasResult = YES;
            }
        }];
        if (hasResult) {
            return BY_IDENTITY_TYPE_GUEST;
        }
    }
    if ([model.user_id isEqualToString:userId]) {
        return BY_IDENTITY_TYPE_CREATOR;
    }
    return BY_IDENTITY_TYPE_NORMAL;
}


@end
