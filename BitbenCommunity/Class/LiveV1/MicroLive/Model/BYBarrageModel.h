//
//  BYBarrageModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYBarrageModel : BYCommonModel

/** messagetitle */
@property (nonatomic ,copy) NSString *msg_title;
/** 头像 */
@property (nonatomic ,copy) NSString *msg_headImg;
/** userid */
@property (nonatomic ,copy) NSString *msg_userId;
/** titleSize */
@property (nonatomic ,assign) CGSize titleSize;
/** attributedString */
@property (nonatomic ,strong) NSAttributedString *attributedString;
@property (nonatomic ,assign) NSInteger cert_badge;
/** isLeft */
@property (nonatomic ,assign) BOOL isLeft;



+ (NSArray *)getTableData:(NSArray *)respondData liveModel:(BYCommonLiveModel *)liveModel;

@end

NS_ASSUME_NONNULL_END
