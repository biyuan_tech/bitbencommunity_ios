//
//  BYMicroAudienceToolBar.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYMicroAudienceToolBar.h"
#import "BYSwitch.h"
#import "BYRewardView.h"
#import "BYLikeAnimationView.h"

#import "BYIMManager+SendMsg.h"


static NSInteger contentH = 49;
@interface BYMicroAudienceToolBar ()<UITextViewDelegate>

/** 弹幕开关 */
@property (nonatomic ,strong) BYSwitch *barrageSwitch;
/** 打赏 */
@property (nonatomic ,strong) BYRewardView *rewardView;
/** 消息输入view */
@property (nonatomic ,strong) UIView *msgContentView;
/** 冒泡框 */
@property (nonatomic ,strong) UIControl *msgView;
/** 冒泡提示文案 */
@property (nonatomic ,strong) UILabel *titleLab;
/** 提问按钮 */
@property (nonatomic ,strong) UIButton *askBtn;
/** 输入框 */
//@property (nonatomic ,strong) BYIMMessageTextView *inputView;
/** 提问输入框 */
@property (nonatomic ,strong) BYIMMessageTextView *askInputView;
/** 消息发送 */
@property (nonatomic ,strong) UIButton *sendBtn;
/** placelab */
@property (nonatomic ,strong) UILabel *placeholderLab;
/** placelab */
@property (nonatomic ,strong) UILabel *askPlaceholderLab;
/** currentController */
@property (nonatomic ,strong) UIViewController *targetController;
/** 用于区分是冒泡还是提问 */
@property (nonatomic ,assign) BOOL isAsk;
/** 记录冒泡输入文案 */
@property (nonatomic ,copy) NSString *bubbleText;
/** 记录提问输入文案 */
@property (nonatomic ,copy) NSString *askText;
/** 点赞按钮 */
@property (nonatomic ,strong) UIButton *likeBtn;
/** 点赞数量 */
@property (nonatomic ,strong) UILabel *suppertNumLab;
@property (nonatomic ,strong) UIImageView *suppertNumBg;
/** 点赞效果 */
@property (nonatomic ,strong) BYLikeAnimationView *animationView;

@end

@implementation BYMicroAudienceToolBar

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.enabled = YES;
        [self addNSNotification];
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setContentView];
    }
    return self;
}

- (void)setEnabled:(BOOL)enabled{
    _enabled = enabled;
    self.msgView.enabled = enabled;
    self.titleLab.text = enabled ? @"冒个泡吧…" : @"直播已结束…";
//    self.inputView.editable = enabled;
    self.placeholderLab.text = enabled ? @"冒个泡吧…" : @"直播已结束…";
    self.askBtn.enabled = enabled;
}

- (void)setIsForbid:(BOOL)isForbid{
    _isForbid = isForbid;
    _placeholderLab.text = isForbid ? @"您已被主播禁言" : @"冒个泡吧…";
    _titleLab.text = isForbid ? @"您已被主播禁言" : @"冒个泡吧…";
    self.askBtn.enabled = !isForbid;
    self.msgView.enabled = !isForbid;
}

- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    [self reloadSuppertNum];
}


- (void)setIsAsk:(BOOL)isAsk{
    _isAsk = isAsk;
//    [self resetToolView];
}

- (void)didMoveToSuperview{
    if (self.superview) {
        [self configMsgContentView];
    }
}

- (BOOL)resignFirstResponder{
//    [_inputView resignFirstResponder];
    [_askInputView resignFirstResponder];
    return [super resignFirstResponder];
}

- (UIViewController *)targetController{
    return [BYTabbarViewController sharedController].currentController;
}

- (void)reloadSuppertNum{
    NSString *string = [NSString transformIntegerShow:self.model.count_support];
    [UIView animateWithDuration:1.0f animations:^{
        self.suppertNumLab.text = string;
    }];
    [self.suppertNumLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(string, 10) + 6);
    }];
}

- (void)reloadTextField{
    NSString *title = _isAsk ? @"提问" : @"发送";
    [self.sendBtn setBy_attributedTitle:@{@"title":title,
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]
                                     } forState:UIControlStateNormal];
    [self.sendBtn setBy_attributedTitle:@{@"title":title,
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]
                                     } forState:UIControlStateDisabled];
    _askPlaceholderLab.text = _isAsk ? @"输入问题…" : @"冒个泡吧…";
}

- (CGFloat)calculateTextViewHeight:(UITextView *)textView text:(NSString *)text{
    CGSize constraninSize = CGSizeMake(textView.contentSize.width, MAXFLOAT);
    CGSize size = [textView sizeThatFits:constraninSize];
    return size.height + 14 + textView.textContainerInset.top;
}

- (void)resetToolView{
//    if (_isAsk) {
//        [_askInputView resignFirstResponder];
//        _askInputView.text = @"";
//        self.askPlaceholderLab.hidden      = NO;
//        [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(contentH);
//        }];
//    }else{
//        [_inputView resignFirstResponder];
//        _inputView.text = @"";
//        self.placeholderLab.hidden      = NO;
//        [self mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(kSafe_Mas_Bottom(contentH));
//        }];
//    }
    [_askInputView resignFirstResponder];
    _askInputView.text = @"";
    self.askPlaceholderLab.hidden      = NO;
    [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(contentH);
    }];
}

#pragma mark - action
- (void)switchAction{
    if (self.barrageIsOnHandle) {
        self.barrageIsOnHandle(self.barrageSwitch.on);
    }
}

// 提问
- (void)askBtnAction{
    [self resignFirstResponder];
    self.isAsk = YES;
    [self reloadTextField];
    self.askInputView.text = nullToEmpty(self.askText);
    [self textViewDidChange:self.askInputView];
    [self.askInputView becomeFirstResponder];
}

// 打赏
- (void)rewardBtnAction{
    [self resignFirstResponder];
    self.rewardView.receicer_name = self.model.nickname;
    [self.rewardView showAnimation];
}

// 冒泡
- (void)triggerMsgTextView{
    self.isAsk = NO;
    [self reloadTextField];
    self.askInputView.text = nullToEmpty(self.bubbleText);
    [self textViewDidChange:self.askInputView];
    [self.askInputView becomeFirstResponder];
}

- (void)sendMessageAction{
    if (!self.enabled) {
        showToastView(@"直播已经结束，禁止发言", self.superview);
        [self resignFirstResponder];
        return;
    }
    
    if (_isForbid) {
        showToastView(@"您已被主播禁言", self.superview);
        [self resignFirstResponder];
        return;
    }
    
    if (_isAsk) {
        self.askText = @"";
    }else{
        self.bubbleText = @"";
    }
//    NSString *inputStr = _isAsk ? self.askInputView.text : self.inputView.text;
    NSString *inputStr = self.askInputView.text;
    NSString *string = [inputStr removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
    if (!string.length) return;
    
    BYSIMMessage *msg = [[BYSIMMessage alloc] init];
    msg.msg_type = _isAsk ? BY_SIM_MSG_TYPE_ASK : BY_SIM_MSG_TYPE_TEXT;
    msg.msg_sender = [AccountModel sharedAccountModel].account_id;
    msg.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
    msg.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
    msg.time = [NSDate getCurrentTimeStr];
    BYSIMTextElem *elem = [[BYSIMTextElem alloc] init];
    elem.text = string;
    msg.elem = elem;
    [[BYSIMManager shareManager] sendMessage:msg suc:nil fail:nil];
    [self resetToolView];
}

- (void)likeBtnAction:(UIButton *)sender{
    if (!self.model.isSupport) {
        @weakify(self);
        [[BYLiveHomeRequest alloc] loadRequestLiveGuestOpearType:BY_GUEST_OPERA_TYPE_UP theme_id:self.model.live_record_id theme_type:BY_THEME_TYPE_LIVE receiver_user_id:self.model.user_id successBlock:^(id object) {
            @strongify(self);
            self.model.isSupport = YES;
            self.model.count_support++;
            [self reloadSuppertNum];
        } faileBlock:^(NSError *error) {
            showToastView(@"顶操作失败", kCommonWindow);
        }];
    }
    CGRect rect = [sender convertRect:sender.bounds toView:self.superview];
    CGPoint point = CGPointMake(rect.origin.x, rect.origin.y);
    [self.animationView beginUpAniamtion:self.superview point:point type:BY_LIKE_ANIMATION_TYPE_PF_UP];
}

#pragma mark - request
- (void)loadRequestReward:(CGFloat)amount{
    UIViewController *currentController = [BYTabbarViewController sharedController].currentController;
    if (!_model) {
        showToastView(@"未获取到打赏人信息", currentController.view);
        return;
    }
    if ([_model.user_id isEqualToString:[AccountModel sharedAccountModel].account_id]) {
        showToastView(@"不能给自己打赏", currentController.view);
        return;
    }
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestReward:_model.user_id reward_amount:amount successBlock:^(id object) {
        @strongify(self);
        showToastView(@"打赏成功", currentController.view);
        [self.rewardView reloadSurplusBBT:amount];
        // 发送打赏消息
        [[BYIMManager sharedInstance] sendRewardMessage:amount receicer_user_Name:self.model.nickname receicer_user_Id:self.model.user_id text:nil succ:^{
        } fail:nil];
        
    } faileBlock:^(NSError *error) {
    }];
}

#pragma mark - UITextViewDelegate 文本输入代理

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"] && textView.text.length) {
//        self.isAsk = textView == self.inputView ? NO : YES;
        [textView resignFirstResponder];
        [self sendMessageAction];
        return YES;
    }else if ([text isEqualToString:@"\n"]) {
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length) {
//        if (textView == self.inputView) {
//            self.placeholderLab.hidden      = YES;
//        }else{
        self.askPlaceholderLab.hidden      = YES;
//        }
    }else{
//        if (textView == self.inputView) {
//            self.placeholderLab.hidden      = NO;
//        }else{
        self.askPlaceholderLab.hidden      = NO;
//        }
    }
    if (_isAsk) {
        self.askText = textView.text;
    }else{
        self.bubbleText = textView.text;
    }
    CGFloat maxHeight = 70;
    CGFloat height = [self calculateTextViewHeight:textView text:textView.text];
    textView.scrollEnabled = height > maxHeight ? YES : NO;
    if (height < maxHeight ) {
//        if (textView == self.inputView) {
//            [self mas_updateConstraints:^(MASConstraintMaker *make) {
//                make.height.mas_equalTo(kSafe_Mas_Bottom(height) < kSafe_Mas_Bottom(contentH) ? kSafe_Mas_Bottom(contentH) : kSafe_Mas_Bottom(height));
//            }];
//        }else{
        [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height < contentH ? contentH : height);
        }];
//        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
//    self.isAsk = textView == self.inputView ? NO : YES;
    if (textView.text.length) {
//        if (textView == self.inputView) {
//            self.placeholderLab.hidden      = YES;
//        }else{
            self.askPlaceholderLab.hidden      = YES;
//        }
    }else{
//        if (textView == self.inputView) {
//            self.placeholderLab.hidden      = NO;
//        }else{
            self.askPlaceholderLab.hidden      = NO;
//        }
    }

}

#pragma mark - keyBoardNSNotification

- (void)keyBoardWillShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardDidShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardWillHidden:(NSNotification *)notic{
    if (self.superview ) {
//        if (self.inputView.isFirstResponder) {
//            [UIView animateWithDuration:0.1 animations:^{
//                [self mas_updateConstraints:^(MASConstraintMaker *make) {
//                    make.bottom.mas_equalTo(0);
//                    make.left.right.mas_equalTo(0);
//                }];
//                [self.superview layoutIfNeeded];
//            }];
//        }else
        if (self.askInputView.isFirstResponder) {
            [UIView animateWithDuration:0.1 animations:^{
                [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.bottom.mas_equalTo(contentH);
                    make.left.right.mas_equalTo(0);
                }];
                [self.superview layoutIfNeeded];
            }];
        }
    }
}

- (void)keyBoardShowAnimation:(NSValue *)value{
    if (self.superview) {
        CGFloat detalY = [value CGRectValue].size.height;
//        if (self.inputView.isFirstResponder) {
//            [UIView animateWithDuration:0.1 animations:^{
//                [self mas_updateConstraints:^(MASConstraintMaker *make) {
//                    make.left.right.mas_equalTo(0);
//                    make.bottom.mas_equalTo(-detalY + kSafeAreaInsetsBottom);
//                }];
//                [self.superview layoutIfNeeded];
//            }];
//        }else
        if (self.askInputView.isFirstResponder) {
            [UIView animateWithDuration:0.1 animations:^{
                [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.left.right.mas_equalTo(0);
                    make.bottom.mas_equalTo(-detalY);
                }];
                [self.superview layoutIfNeeded];
            }];
        }
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView *view = [super hitTest:point withEvent:event];
    if (view != self && view != nil) {
        if (![AccountModel sharedAccountModel].hasLoggedIn) {
            [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
            return nil;
        }
    }
    return view;
}

#pragma mark - regisetNSNotic

- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - configUI

- (void)setContentView{
 
//    self.inputView = [[BYIMMessageTextView alloc] init];
//    self.inputView.layer.cornerRadius = 5.0f;
//    [self.inputView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
//    self.inputView.tintColor = kBgColor_238;
//    self.inputView.font = [UIFont systemFontOfSize:16];
//    self.inputView.textContainerInset = UIEdgeInsetsMake(7, 8, 0, 5);
//    self.inputView.textContainer.lineFragmentPadding = 0;
//    self.inputView.returnKeyType = UIReturnKeySend;
//    self.inputView.delegate = self;
//    [self addSubview:self.inputView];
//    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(62);
//        make.top.mas_equalTo(7);
//        make.bottom.mas_equalTo(-kSafe_Mas_Bottom(7));
//        make.right.mas_equalTo(-102);
//    }];
//
//    UILabel *placeholderLab = [UILabel by_init];
//    [placeholderLab setBy_font:14];
//    placeholderLab.textColor = kColorRGBValue(0x8f8f8f);
//    placeholderLab.text = @"冒个泡吧…";
//    [self addSubview:placeholderLab];
//    self.placeholderLab = placeholderLab;
//    @weakify(self);
//    [placeholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        @strongify(self);
//        make.left.mas_equalTo(self.inputView).mas_offset(13);
//        make.centerY.mas_equalTo(self.inputView).mas_offset(0);
//        make.height.mas_equalTo(placeholderLab.font.pointSize);
//        make.right.mas_equalTo(self.inputView);
//    }];
    
    // 消息触发区
    UIControl *msgView = [[UIControl alloc] init];
    [msgView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    msgView.layer.cornerRadius = 4.0f;
    [msgView addTarget:self action:@selector(triggerMsgTextView) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:msgView];
    self.msgView = msgView;
    [msgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(62);
        make.top.mas_equalTo(7);
        make.height.mas_equalTo(36);
        make.right.mas_equalTo(-147);
    }];

    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    titleLab.textColor = kColorRGBValue(0x8f8f8f);
    titleLab.text = @"冒个泡吧…";
    [msgView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(0);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.right.mas_equalTo(msgView);
    }];
    
    self.barrageSwitch = [[BYSwitch alloc] initWithFrame:CGRectMake(15, 15, 35, 22)];
    [self.barrageSwitch setThumbImage:[UIImage imageNamed:@"micro_barrage"] forOn:YES];
    [self.barrageSwitch setThumbImage:[UIImage imageNamed:@"micro_barrage"] forOn:NO];
    [self.barrageSwitch setOn:YES animated:YES];
    [self.barrageSwitch addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventValueChanged];
    [self addSubview:self.barrageSwitch];
    [self.barrageSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(msgView);
        make.width.mas_equalTo(35);
        make.height.mas_equalTo(22);
    }];
    
    // 提问
    UIButton *askBtn = [UIButton by_buttonWithCustomType];
    [askBtn setBy_imageName:@"microlive_ask_nor" forState:UIControlStateNormal];
    [askBtn setBy_imageName:@"microlive_ask_disabled" forState:UIControlStateDisabled];
    [askBtn addTarget:self action:@selector(askBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:askBtn];
    self.askBtn = askBtn;
    [askBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(msgView.mas_right).mas_offset(12);
        make.centerY.mas_equalTo(msgView);
        make.width.height.mas_equalTo(28);
    }];
    
    // 打赏
    UIButton *rewardBtn = [UIButton by_buttonWithCustomType];
    [rewardBtn setBy_imageName:@"microlive_reward" forState:UIControlStateNormal];
    [rewardBtn addTarget:self action:@selector(rewardBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:rewardBtn];
    [rewardBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(askBtn.mas_right).mas_offset(12);
        make.centerY.mas_equalTo(msgView);
        make.width.height.mas_equalTo(28);
    }];
    
    UIViewController *currentController = [BYTabbarViewController sharedController].currentController;
    self.rewardView = [[BYRewardView alloc] initWithFathureView:currentController.view];
    @weakify(self);
    _rewardView.confirmBtnHandle = ^(CGFloat amount) {
        @strongify(self);
        [self loadRequestReward:amount];
    };
    
    [self addSubview:self.likeBtn];
    [self addSubview:self.suppertNumLab];
    [self addSubview:self.suppertNumBg];
    [self insertSubview:self.suppertNumBg belowSubview:self.suppertNumLab];
    
    [self.likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(rewardBtn.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(msgView);
        make.width.height.mas_equalTo(36);
    }];
    
    [self.suppertNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(14);
        make.top.mas_equalTo(5);
        make.centerX.mas_equalTo(self.likeBtn).mas_offset(14);
    }];
    
    [self.suppertNumBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.suppertNumLab);
        make.height.mas_equalTo(14);
        make.top.mas_equalTo(5);
        make.centerX.mas_equalTo(self.likeBtn).mas_offset(14);
    }];
    
}

- (void)configMsgContentView{
    self.msgContentView = [UIView by_init];
    [self.msgContentView setBackgroundColor:[UIColor whiteColor]];
    [self.superview addSubview:self.msgContentView];
    [self.msgContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(contentH);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(contentH);
    }];
    
    self.askInputView = [[BYIMMessageTextView alloc] init];
    self.askInputView.layer.cornerRadius = 5.0f;
    [self.askInputView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    self.askInputView.tintColor = kBgColor_238;
    self.askInputView.font = [UIFont systemFontOfSize:16];
    self.askInputView.textContainerInset = UIEdgeInsetsMake(7, 8, 0, 5);
    self.askInputView.textContainer.lineFragmentPadding = 0;
    self.askInputView.returnKeyType = UIReturnKeySend;
    self.askInputView.delegate = self;
    [self.msgContentView addSubview:self.askInputView];
    [self.askInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 15, 7, 90));
    }];
    
    self.askPlaceholderLab = [UILabel by_init];
    [self.askPlaceholderLab setBy_font:14];
    self.askPlaceholderLab.textColor = kColorRGBValue(0x8f8f8f);
    self.askPlaceholderLab.text = @"输入问题…";
    [self.msgContentView addSubview:self.askPlaceholderLab];
    @weakify(self);
    [self.askPlaceholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(29);
        make.centerY.mas_equalTo(self.askInputView).mas_offset(0);
        make.height.mas_equalTo(self.askPlaceholderLab.font.pointSize);
        make.right.mas_equalTo(self.askInputView);
    }];
    
    UIButton *sendBtn = [UIButton by_buttonWithCustomType];
    [sendBtn setBy_attributedTitle:@{@"title":@"提问",
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]
                                     } forState:UIControlStateNormal];
    [sendBtn setBy_attributedTitle:@{@"title":@"提问",
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]
                                     } forState:UIControlStateDisabled];
    [sendBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    [sendBtn addTarget:self action:@selector(sendMessageAction) forControlEvents:UIControlEventTouchUpInside];
    sendBtn.layer.cornerRadius = 4.0f;
    [self.msgContentView addSubview:sendBtn];
    self.sendBtn = sendBtn;
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(36);
        make.centerY.mas_equalTo(self.askInputView).mas_offset(0);
    }];
}

- (UIButton *)likeBtn{
    if (!_likeBtn) {
        _likeBtn = [UIButton by_buttonWithCustomType];
        [_likeBtn setBy_imageName:@"videolive_like" forState:UIControlStateNormal];
        [_likeBtn addTarget:self action:@selector(likeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeBtn;
}

- (UILabel *)suppertNumLab{
    if (!_suppertNumLab) {
        _suppertNumLab = [UILabel by_init];
        _suppertNumLab.font = [UIFont boldSystemFontOfSize:10];
        _suppertNumLab.backgroundColor = [UIColor clearColor];
        _suppertNumLab.textColor = kColorRGBValue(0xf03434);
        _suppertNumLab.textAlignment = NSTextAlignmentCenter;
        
    }
    return _suppertNumLab;
}

- (UIImageView *)suppertNumBg{
    if (!_suppertNumBg) {
        _suppertNumBg = [[UIImageView alloc] init];
        _suppertNumBg.backgroundColor = [UIColor whiteColor];
        _suppertNumBg.layer.cornerRadius = 7.0;
        _suppertNumBg.layer.shadowColor = kColorRGBValue(0x575757).CGColor;
        _suppertNumBg.layer.shadowOffset = CGSizeMake(0, 0);
        _suppertNumBg.layer.shadowOpacity = 0.4;
        _suppertNumBg.layer.shadowRadius = 5.0;
    }
    return _suppertNumBg;
}

- (BYLikeAnimationView *)animationView{
    if (!_animationView) {
        _animationView = [[BYLikeAnimationView alloc] init];
    }
    return _animationView;
}


@end
