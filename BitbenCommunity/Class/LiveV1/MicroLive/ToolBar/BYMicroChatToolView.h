//
//  BYMicroChatToolView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYIMMessageInputView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYMicroChatToolView : UIView

@property (nonatomic ,strong ,readonly) BYIMMessageTextView *inputView;
/** 消息发送回调 */
@property (nonatomic ,copy) void (^didSendMessageHandle)(NSString *string);
/** 是否不可编辑 */
@property (nonatomic ,assign) BOOL noEdit;
/** 禁言状态 */
@property (nonatomic ,assign) BOOL isForbid;

- (void)resetToolView;

@end

NS_ASSUME_NONNULL_END
