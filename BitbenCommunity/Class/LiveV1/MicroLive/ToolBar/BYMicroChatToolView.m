//
//  BYMicroChatToolView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYMicroChatToolView.h"

static NSInteger defultH = 49;
@interface BYMicroChatToolView ()<UITextFieldDelegate>

/** 输入框 */
@property (nonatomic ,strong) BYIMMessageTextView *inputView;
/** 消息发送 */
@property (nonatomic ,strong) UIButton *sendBtn;
/** 评论图标 */
@property (nonatomic ,strong) UIImageView *placeholderImgView;
/** 评论文案 */
@property (nonatomic ,strong) UILabel *placeholderLab;

@end

@implementation BYMicroChatToolView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setContentView];
        [self addNSNotification];
    }
    return self;
}

- (void)setNoEdit:(BOOL)noEdit{
    _noEdit = noEdit;
    _inputView.text = @"";
    _placeholderLab.text = @"直播已结束，禁止发言...";
    _inputView.editable = NO;
    _sendBtn.enabled = NO;
    [_sendBtn setBackgroundColor:kColorRGBValue(0xd2d2d2)];
}

- (void)setIsForbid:(BOOL)isForbid{
    _isForbid = isForbid;
    _inputView.text = @"";
    _placeholderLab.text = isForbid ? @"您已被主播禁言" : @"冒个泡吧…";
    _inputView.editable = !isForbid;
    _sendBtn.enabled = !isForbid;
}

- (void)resetToolView{
    [_inputView resignFirstResponder];
    _inputView.text = @"";
    self.placeholderImgView.hidden  = NO;
    self.placeholderLab.hidden      = NO;
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(kSafe_Mas_Bottom(defultH));
    }];
}

- (CGFloat)calculateTextViewHeight:(UITextView *)textView text:(NSString *)text{
    CGSize constraninSize = CGSizeMake(textView.contentSize.width, MAXFLOAT);
    CGSize size = [textView sizeThatFits:constraninSize];
    return size.height + 14 + textView.textContainerInset.top;
}

#pragma mark - action

- (void)sendMessageAction{
    if (_isForbid) {
        showToastView(@"您已被主播禁言", CURRENT_VC.view);
        [self resignFirstResponder];
        return;
    }
    if (self.didSendMessageHandle) {
        NSString *string = [self.inputView.text removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
        self.didSendMessageHandle(string);
    }
}
#pragma mark - UITextViewDelegate 文本输入代理

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"] && textView.text.length) {
        [textView resignFirstResponder];
        [self sendMessageAction];
        return YES;
    }else if ([text isEqualToString:@"\n"]) {
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length) {
        self.placeholderImgView.hidden  = YES;
        self.placeholderLab.hidden      = YES;
    }else{
        self.placeholderImgView.hidden  = NO;
        self.placeholderLab.hidden      = NO;
    }
    CGFloat maxHeight = 70;
    CGFloat height = [self calculateTextViewHeight:textView text:textView.text];
    textView.scrollEnabled = height > maxHeight ? YES : NO;
    if (height < maxHeight ) {
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(kSafe_Mas_Bottom(height) < kSafe_Mas_Bottom(defultH) ? kSafe_Mas_Bottom(defultH) : kSafe_Mas_Bottom(height));
        }];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length) {
        self.placeholderImgView.hidden  = YES;
        self.placeholderLab.hidden      = YES;
    }else{
        self.placeholderImgView.hidden  = NO;
        self.placeholderLab.hidden      = NO;
    }
}

#pragma mark - keyBoardNSNotification

- (void)keyBoardWillShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardDidShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardWillHidden:(NSNotification *)notic{
    if (self.superview && self.inputView.isFirstResponder) {
        [UIView animateWithDuration:0.1 animations:^{
            @weakify(self);
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
                make.left.right.mas_equalTo(0);
            }];
            [self.superview layoutIfNeeded];
        }];
    }
}

- (void)keyBoardShowAnimation:(NSValue *)value{
    if (self.superview && self.inputView.isFirstResponder) {
        CGFloat detalY = [value CGRectValue].size.height;
        [UIView animateWithDuration:0.1 animations:^{
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.right.mas_equalTo(0);
                make.bottom.mas_equalTo(-detalY + kSafeAreaInsetsBottom);
            }];
            [self.superview layoutIfNeeded];
        }];
    }
}

#pragma mark - regisetNSNotic
- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView *view = [super hitTest:point withEvent:event];
    if (view != self && view != nil) {
        if (![AccountModel sharedAccountModel].hasLoggedIn) {
            [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
            return nil;
        }
    }
    return view;
}

#pragma mark - configUI

- (void)setContentView{
    self.inputView = [[BYIMMessageTextView alloc] init];
    self.inputView.layer.cornerRadius = 5.0f;
    [self.inputView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    self.inputView.tintColor = kBgColor_238;
    self.inputView.font = [UIFont systemFontOfSize:16];
    self.inputView.textContainerInset = UIEdgeInsetsMake(7, 8, 0, 5);
    self.inputView.textContainer.lineFragmentPadding = 0;
    self.inputView.returnKeyType = UIReturnKeySend;
    self.inputView.delegate = self;
    [self addSubview:self.inputView];
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 15, kSafe_Mas_Bottom(7), 90));
    }];
    
//    self.placeholderImgView = [[UIImageView alloc] init];
//    [self.placeholderImgView by_setImageName:@"icon_article_detail_draw_input"];
//    [self addSubview:self.placeholderImgView];
//    @weakify(self);
//    [self.placeholderImgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        @strongify(self);
//        make.left.mas_equalTo(27);
//        make.width.mas_equalTo(self.placeholderImgView.image.size.width);
//        make.height.mas_equalTo(self.placeholderImgView.image.size.height);
//        make.centerY.mas_equalTo(0);
//    }];
    
    self.placeholderLab = [UILabel by_init];
    [self.placeholderLab setBy_font:14];
    self.placeholderLab.textColor = kColorRGBValue(0x8f8f8f);
    self.placeholderLab.text = @"冒个泡吧...";
    [self addSubview:self.placeholderLab];
    @weakify(self);
    [self.placeholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(29);
        make.centerY.mas_equalTo(self.inputView).mas_offset(0);
        make.height.mas_equalTo(self.placeholderLab.font.pointSize);
        make.right.mas_equalTo(self.inputView);
    }];
    
    UIButton *sendBtn = [UIButton by_buttonWithCustomType];
    [sendBtn setBy_attributedTitle:@{@"title":@"发送",
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]
                                     } forState:UIControlStateNormal];
    [sendBtn setBy_attributedTitle:@{@"title":@"发送",
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]
                                     } forState:UIControlStateDisabled];
//    [sendBtn setBackgroundImage:[UIImage imageWithColor:kColorRGBValue(0xd2d2d2)] forState:UIControlStateDisabled];
//    [sendBtn setBackgroundImage:[UIImage imageWithColor:kColorRGBValue(0xea6438)] forState:UIControlStateNormal];
    [sendBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    [sendBtn addTarget:self action:@selector(sendMessageAction) forControlEvents:UIControlEventTouchUpInside];
    sendBtn.layer.cornerRadius = 4.0f;
    [self addSubview:sendBtn];
    self.sendBtn = sendBtn;
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(36);
        make.centerY.mas_equalTo(self.inputView).mas_offset(0);
    }];
    
//    UIView *topLineView = [UIView new];
//    [topLineView setBackgroundColor:kColorRGBValue(0xededed)];
//    [self addSubview:topLineView];
//    [topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.top.mas_equalTo(0);
//        make.height.mas_equalTo(0.5);
//        make.width.mas_equalTo(kCommonScreenWidth);
//    }];
}


@end
