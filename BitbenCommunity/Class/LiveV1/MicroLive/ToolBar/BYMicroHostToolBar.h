//
//  BYMicroHostToolBar.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYIMMessageInputView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYMicroHostToolBar : UIView

/** 弹幕开关 */
@property (nonatomic ,copy) void (^barrageIsOnHandle)(BOOL isOn);
/** 退出直播按钮回调 */
@property (nonatomic ,copy) void(^didExitLiveBtnHandle)(void);
/** 选择发送的图片 */
@property (nonatomic ,copy) void (^didSelectImageDataHandle)(NSArray *images);

/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
//* targetView 
//@property (nonatomic ,weak) UIView *targetView;
@property (nonatomic ,readonly,strong) BYIMMessageTextView *inputView;
/** 身份 */
@property (nonatomic ,assign) BY_IDENTITY_TYPE identityType;
/** 是否可操作(默认YES) */
@property (nonatomic ,assign) BOOL enabled;

- (void)reloadSuppertNum;

@end

NS_ASSUME_NONNULL_END
