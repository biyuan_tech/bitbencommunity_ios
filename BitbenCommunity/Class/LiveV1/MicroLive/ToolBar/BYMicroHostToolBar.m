//
//  BYMicroHostToolBar.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYMicroHostToolBar.h"
#import "BYSwitch.h"
#import "BYPluginAudioMoreView.h"
#import "BYPluginMoreView.h"
#import "GWAssetsLibraryViewController.h"
#import "BYLikeAnimationView.h"
#import "BYILiveForbidListView.h"

#import "BYIMManager+SendMsg.h"
#import "AATAudioTool.h"

static NSInteger contentH = 49;
static NSInteger audioViewH = 128;
static NSInteger moreViewH = 110;
@interface BYMicroHostToolBar ()<UITextViewDelegate>

/** 弹幕开关 */
@property (nonatomic ,strong) BYSwitch *barrageSwitch;
/** 输入框 */
@property (nonatomic ,strong) BYIMMessageTextView *inputView;
/** 消息输入view */
@property (nonatomic ,strong) UIView *msgContentView;
/** placelab */
@property (nonatomic ,strong) UILabel *placeholderLab;
/** 音频输入 */
@property (nonatomic ,strong) BYPluginAudioMoreView *audioMoreView;
/** 更多 */
@property (nonatomic ,strong) BYPluginMoreView *moreView;
/** 音频按钮 */
@property (nonatomic ,strong) UIButton *voiceBtn;
/** 更多按钮 */
@property (nonatomic ,strong) UIButton *moreBtn;
/** 点赞按钮 */
@property (nonatomic ,strong) UIButton *likeBtn;
/** 点赞数量 */
@property (nonatomic ,strong) UILabel *suppertNumLab;
@property (nonatomic ,strong) UIImageView *suppertNumBg;
/** 点赞效果 */
@property (nonatomic ,strong) BYLikeAnimationView *animationView;
/** titleLab */
@property (nonatomic ,strong) UILabel *titleLab;
/** 禁言列表 */
@property (nonatomic ,strong) BYILiveForbidListView *forbidListView;
/** 分享按钮 */
@property (nonatomic ,strong) UIButton *forbidBtn;
@end

@implementation BYMicroHostToolBar

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didMoveToSuperview{
    if (self.superview) {
        [self configMsgContentView];
        [self configPluginView];
    }
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.enabled = YES;
        [self addNSNotification];
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setContentView];
    }
    return self;
}

- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    self.moreView.isHost = [model.user_id isEqualToString:ACCOUNT_ID] ? YES : NO;
    [self reloadSuppertNum];
}

- (void)setEnabled:(BOOL)enabled{
    _enabled = enabled;
    self.titleLab.text = enabled ? @"冒个泡吧…" : @"直播已结束";
}

- (void)reloadSuppertNum{
    NSString *string = [NSString transformIntegerShow:self.model.count_support];
    [UIView animateWithDuration:1.0f animations:^{
        self.suppertNumLab.text = string;
    }];
    [self.suppertNumLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(string, 10) + 6);
    }];
}

- (CGFloat)calculateTextViewHeight:(UITextView *)textView text:(NSString *)text{
    CGSize constraninSize = CGSizeMake(textView.contentSize.width, MAXFLOAT);
    CGSize size = [textView sizeThatFits:constraninSize];
    return size.height + 14 + textView.textContainerInset.top;
}

- (void)resetToolView{
    [_inputView resignFirstResponder];
    _inputView.text = @"";
    
    self.placeholderLab.hidden      = NO;
    [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(contentH);
    }];
    
    [self setAudioMoreViewShowAnimation:NO];
    [self setMoreViewShowAnimation:NO];
}

- (BOOL)resignFirstResponder{
    [_inputView resignFirstResponder];
    [self setAudioMoreViewShowAnimation:NO];
    [self setMoreViewShowAnimation:NO];
    return [super resignFirstResponder];
}

- (void)setAudioMoreViewShowAnimation:(BOOL)isShow{
    if (_moreBtn.selected && isShow) {
        [self setMoreViewShowAnimation:NO];
    }
    self.voiceBtn.selected = isShow;
    CGFloat bottom = isShow ? -audioViewH : 0;
    CGFloat audioBottom = isShow ? 0 : kSafe_Mas_Bottom(audioViewH);
    if (isShow) {
        [self.superview bringSubviewToFront:self.audioMoreView];
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.audioMoreView.hidden = NO;
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(bottom);
        }];
        
        [self.audioMoreView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(audioBottom);
        }];
        
        [self.superview layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.audioMoreView.hidden = !isShow;
    }];
}

- (void)setMoreViewShowAnimation:(BOOL)isShow{
    if (_voiceBtn.selected && isShow) {
        [self setAudioMoreViewShowAnimation:NO];
    }
    self.moreBtn.selected = isShow;
    CGFloat bottom = isShow ? -moreViewH : 0;
    CGFloat moreBottom = isShow ? 0 : kSafe_Mas_Bottom(moreViewH);
    if (isShow) {
        [self.superview bringSubviewToFront:self.moreView];
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.moreView.hidden = NO;
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(bottom);
        }];
        
        [self.moreView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(moreBottom);
        }];
        
        [self.superview layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.moreView.hidden = !isShow;
    }];
}

#pragma mark - action
- (void)switchAction{
    if (self.barrageIsOnHandle) {
        self.barrageIsOnHandle(self.barrageSwitch.on);
    }
}

- (void)voiceBtnAction:(UIButton *)sender{
    [_inputView resignFirstResponder];
    if (!_enabled) {
        showToastView(@"直播已经结束，禁止操作", CURRENT_VC.view);
        return;
    }
    // 检查麦克风权限
    @weakify(self);
    [AATAudioTool checkCameraAuthorizationGrand:^{
        @strongify(self);
        sender.selected = !sender.selected;
        [self setAudioMoreViewShowAnimation:sender.selected];
    } withNoPermission:^{
        @strongify(self);
        showToastView(@"请先打开麦克风权限！", self.superview);
        [self setAudioMoreViewShowAnimation:NO];
    }];
    
}

- (void)moreBtnAction:(UIButton *)sender{
//    [self resignFirstResponder];
    [_inputView resignFirstResponder];
    if (!_enabled) {
        showToastView(@"直播已经结束，禁止操作", CURRENT_VC.view);
        return;
    }
    sender.selected = !sender.selected;
    [self setMoreViewShowAnimation:sender.selected];
}

// 冒泡
- (void)triggerMsgTextView{
    [self.inputView becomeFirstResponder];
}

- (void)sendMessageAction{
    if (!_enabled) {
        [self resetToolView];
        showToastView(@"直播已经结束，禁止发言", CURRENT_VC.view);
        return;
    }
    NSString *string = [self.inputView.text removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
    if (!string.length) return;
    
    BYSIMMessage *msg = [[BYSIMMessage alloc] init];
    msg.msg_type = BY_SIM_MSG_TYPE_TEXT;
    msg.msg_sender = [AccountModel sharedAccountModel].account_id;
    msg.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
    msg.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
    msg.time = [NSDate getCurrentTimeStr];
    BYSIMTextElem *elem = [[BYSIMTextElem alloc] init];
    elem.text = string;
    msg.elem = elem;
    [[BYSIMManager shareManager] sendMessage:msg suc:nil fail:nil];
    [self resetToolView];
}

// 结束直播弹框
- (void)showExitSheetView{
    NSString *title = @"确定要结束直播吗？";
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    @weakify(self);
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [self resignFirstResponder];
        if (self.didExitLiveBtnHandle) self.didExitLiveBtnHandle();
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [sheetController addAction:confirmAction];
    [sheetController addAction:cancelAction];
    [CURRENT_VC presentViewController:sheetController animated:YES completion:nil];
}

// 选择发送图片
- (void)selectMessagePic{
    if (!_enabled) {
        showToastView(@"直播已经结束，禁止发言", CURRENT_VC.view);
        return;
    }
    UIViewController *currentController = [BYTabbarViewController sharedController].currentController;
    GWAssetsLibraryViewController *assetVC = [[GWAssetsLibraryViewController alloc] init];
    @weakify(self);
    [assetVC selectImageArrayFromImagePickerWithMaxSelected:9 andBlock:^(NSArray *selectedImgArr) {
        @strongify(self);
        if (!self.enabled) {
            showToastView(@"直播已经结束，禁止发言", CURRENT_VC.view);
            return;
        }
        if (self.didSelectImageDataHandle) {
            self.didSelectImageDataHandle(selectedImgArr);
        }
    }];
    [currentController.navigationController pushViewController:assetVC animated:YES];
    
}

- (void)likeBtnAction:(UIButton *)sender{
    if (!self.model.isSupport) {
        @weakify(self);
        [[BYLiveHomeRequest alloc] loadRequestLiveGuestOpearType:BY_GUEST_OPERA_TYPE_UP theme_id:self.model.live_record_id theme_type:BY_THEME_TYPE_LIVE receiver_user_id:self.model.user_id successBlock:^(id object) {
            @strongify(self);
            self.model.isSupport = YES;
            self.model.count_support++;
            [self reloadSuppertNum];
        } faileBlock:^(NSError *error) {
            showToastView(@"顶操作失败", kCommonWindow);
        }];
    }
    CGRect rect = [sender convertRect:sender.bounds toView:self.superview];
    CGPoint point = CGPointMake(rect.origin.x, rect.origin.y);
    [self.animationView beginUpAniamtion:self.superview point:point type:BY_LIKE_ANIMATION_TYPE_PF_UP];
}

- (void)forbidBtnAction{
    @weakify(self);
    [[BYSIMManager shareManager] getForbidMembers:^(NSArray * _Nonnull members) {
        @strongify(self);
        self.forbidListView.members = members;
        [self.forbidListView showAnimation];
    } fail:^{
        showToastView(@"获取禁言列表失败", CURRENT_VC.view);
    }];
}
#pragma mark - UITextViewDelegate 文本输入代理

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"] && textView.text.length) {
        [textView resignFirstResponder];
        [self sendMessageAction];
        return YES;
    }else if ([text isEqualToString:@"\n"]) {
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length) {
        self.placeholderLab.hidden      = YES;
    }else{
        self.placeholderLab.hidden      = NO;
    }
    CGFloat maxHeight = 70;
    CGFloat height = [self calculateTextViewHeight:textView text:textView.text];
    textView.scrollEnabled = height > maxHeight ? YES : NO;
    if (height < maxHeight ) {
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(kSafe_Mas_Bottom(height) < kSafe_Mas_Bottom(contentH) ? kSafe_Mas_Bottom(contentH) : kSafe_Mas_Bottom(height));
        }];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length) {
        self.placeholderLab.hidden      = YES;
    }else{
        self.placeholderLab.hidden      = NO;
    }
}

#pragma mark - keyBoardNSNotification

- (void)keyBoardWillShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardDidShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardWillHidden:(NSNotification *)notic{
    if (self.superview && self.inputView.isFirstResponder) {
        [UIView animateWithDuration:0.1 animations:^{
            [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(contentH);
                make.left.right.mas_equalTo(0);
            }];
            [self.superview layoutIfNeeded];
        }];
    }
}

- (void)keyBoardShowAnimation:(NSValue *)value{
    if (self.superview && self.inputView.isFirstResponder) {
        CGFloat detalY = [value CGRectValue].size.height;
        [UIView animateWithDuration:0.1 animations:^{
            [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.right.mas_equalTo(0);
                make.bottom.mas_equalTo(-detalY);
            }];
            [self.superview layoutIfNeeded];
        }];
    }
}

#pragma mark - regisetNSNotic

- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - configUI

- (void)setContentView{
//    self.inputView = [[BYIMMessageTextView alloc] init];
//    self.inputView.layer.cornerRadius = 5.0f;
//    [self.inputView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
//    self.inputView.tintColor = kBgColor_238;
//    self.inputView.font = [UIFont systemFontOfSize:16];
//    self.inputView.textContainerInset = UIEdgeInsetsMake(7, 8, 0, 5);
//    self.inputView.textContainer.lineFragmentPadding = 0;
//    self.inputView.returnKeyType = UIReturnKeySend;
//    self.inputView.delegate = self;
//    [self addSubview:self.inputView];
//    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(54);
//        make.top.mas_equalTo(7);
////        make.height.mas_equalTo(36);
//        make.bottom.mas_equalTo(-kSafe_Mas_Bottom(7));
//        make.right.mas_equalTo(-102);
//    }];
//
//    UILabel *placeholderLab = [UILabel by_init];
//    [placeholderLab setBy_font:14];
//    placeholderLab.textColor = kColorRGBValue(0x8f8f8f);
//    placeholderLab.text = @"请输入内容…";
//    [self addSubview:placeholderLab];
//    self.placeholderLab = placeholderLab;
//    @weakify(self);
//    [placeholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        @strongify(self);
//        make.left.mas_equalTo(self.inputView).mas_offset(13);
//        make.centerY.mas_equalTo(self.inputView).mas_offset(0);
//        make.height.mas_equalTo(placeholderLab.font.pointSize);
//        make.right.mas_equalTo(self.inputView);
//    }];
    
    // 消息触发区
    UIControl *msgView = [[UIControl alloc] init];
    [msgView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    msgView.layer.cornerRadius = 4.0f;
    [msgView addTarget:self action:@selector(triggerMsgTextView) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:msgView];
    [msgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(54);
        make.top.mas_equalTo(7);
        make.height.mas_equalTo(36);
        make.right.mas_equalTo(-157);
    }];

    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    titleLab.textColor = kColorRGBValue(0x8f8f8f);
    titleLab.text = @"冒个泡吧…";
    [msgView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(0);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.right.mas_equalTo(msgView);
    }];
    
    UIButton *voiceBtn = [UIButton by_buttonWithCustomType];
    [voiceBtn setBy_imageName:@"microlive_voice" forState:UIControlStateNormal];
    [voiceBtn addTarget:self action:@selector(voiceBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:voiceBtn];
    self.voiceBtn = voiceBtn;
    [voiceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(msgView);
        make.width.height.mas_equalTo(28);
    }];

    
    [self addSubview:self.forbidBtn];
    [self.forbidBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-110);
        make.width.height.mas_equalTo(24);
        make.centerY.mas_equalTo(msgView);
    }];
    
    // 更多
    UIButton *moreBtn = [UIButton by_buttonWithCustomType];
    [moreBtn setBy_imageName:@"microlive_more" forState:UIControlStateNormal];
    [moreBtn addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:moreBtn];
    self.moreBtn = moreBtn;
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.forbidBtn.mas_right).offset(15);
        make.centerY.mas_equalTo(msgView);
        make.width.height.mas_equalTo(28);
    }];
    
    [self addSubview:self.likeBtn];
    [self addSubview:self.suppertNumLab];
    [self addSubview:self.suppertNumBg];
    [self insertSubview:self.suppertNumBg belowSubview:self.suppertNumLab];
    
    [self.likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(moreBtn.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(msgView);
        make.width.height.mas_equalTo(36);
    }];
    
    [self.suppertNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(14);
        make.top.mas_equalTo(5);
        make.centerX.mas_equalTo(self.likeBtn).mas_offset(14);
    }];
    
    [self.suppertNumBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.suppertNumLab);
        make.height.mas_equalTo(14);
        make.top.mas_equalTo(5);
        make.centerX.mas_equalTo(self.likeBtn).mas_offset(14);
    }];
}

- (void)configMsgContentView{
    self.msgContentView = [UIView by_init];
    [self.msgContentView setBackgroundColor:[UIColor whiteColor]];
    [self.superview addSubview:self.msgContentView];
    [self.msgContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(contentH);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(contentH);
    }];
    
    CGFloat orginX = 15;
    self.barrageSwitch = [[BYSwitch alloc] initWithFrame:CGRectMake(orginX, 14, 35, 22)];
    [self.barrageSwitch setThumbImage:[UIImage imageNamed:@"micro_barrage"] forOn:YES];
    [self.barrageSwitch setThumbImage:[UIImage imageNamed:@"micro_barrage"] forOn:NO];
    [self.barrageSwitch setOn:YES animated:YES];
    [self.barrageSwitch addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventValueChanged];
    [self.msgContentView addSubview:self.barrageSwitch];
    [self.barrageSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(orginX);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(35);
        make.height.mas_equalTo(22);
    }];
    
    self.inputView = [[BYIMMessageTextView alloc] init];
    self.inputView.layer.cornerRadius = 5.0f;
    [self.inputView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    self.inputView.tintColor = kBgColor_238;
    self.inputView.font = [UIFont systemFontOfSize:16];
    self.inputView.textContainerInset = UIEdgeInsetsMake(7, 8, 0, 5);
    self.inputView.textContainer.lineFragmentPadding = 0;
    self.inputView.returnKeyType = UIReturnKeySend;
    self.inputView.delegate = self;
    [self.msgContentView addSubview:self.inputView];
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 61, 7, 90));
    }];
    
    UILabel *placeholderLab = [UILabel by_init];
    [placeholderLab setBy_font:14];
    placeholderLab.textColor = kColorRGBValue(0x8f8f8f);
    placeholderLab.text = @"冒个泡吧…";
    [self.msgContentView addSubview:placeholderLab];
    self.placeholderLab = placeholderLab;
    @weakify(self);
    [placeholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.inputView).offset(14);
        make.centerY.mas_equalTo(self.inputView).mas_offset(0);
        make.height.mas_equalTo(placeholderLab.font.pointSize);
        make.right.mas_equalTo(self.inputView);
    }];
    
    UIButton *sendBtn = [UIButton by_buttonWithCustomType];
    [sendBtn setBy_attributedTitle:@{@"title":@"发送",
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]
                                     } forState:UIControlStateNormal];
    [sendBtn setBy_attributedTitle:@{@"title":@"发送",
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]
                                     } forState:UIControlStateDisabled];
    [sendBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    [sendBtn addTarget:self action:@selector(sendMessageAction) forControlEvents:UIControlEventTouchUpInside];
    sendBtn.layer.cornerRadius = 4.0f;
    [self.msgContentView addSubview:sendBtn];
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.width.mas_equalTo(65);
        make.height.mas_equalTo(36);
        make.centerY.mas_equalTo(self.inputView).mas_offset(0);
    }];
}

- (void)configPluginView{
    BYPluginAudioMoreView *audioMoreView = [[BYPluginAudioMoreView alloc] init];
    audioMoreView.hidden = YES;
    [self.superview addSubview:audioMoreView];
    self.audioMoreView = audioMoreView;
    [audioMoreView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(audioViewH));
        make.bottom.mas_equalTo(kSafe_Mas_Bottom(audioViewH));
    }];
    
    BYPluginMoreView *moreView = [[BYPluginMoreView alloc] init];
    moreView.hidden = YES;
    @weakify(self);
    moreView.didTapPluginAtIndexHandle = ^(NSInteger index) {
        @strongify(self);
        if (index == 0) {
            [self selectMessagePic];
        }else if (index == 1){
            [self showExitSheetView];
        }
    };
    [self.superview addSubview:moreView];
    self.moreView = moreView;
    [moreView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(moreViewH));
        make.bottom.mas_equalTo(kSafe_Mas_Bottom(moreViewH));
    }];
}

- (UIButton *)likeBtn{
    if (!_likeBtn) {
        _likeBtn = [UIButton by_buttonWithCustomType];
        [_likeBtn setBy_imageName:@"videolive_like" forState:UIControlStateNormal];
        [_likeBtn addTarget:self action:@selector(likeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeBtn;
}

- (UILabel *)suppertNumLab{
    if (!_suppertNumLab) {
        _suppertNumLab = [UILabel by_init];
        _suppertNumLab.font = [UIFont boldSystemFontOfSize:10];
        _suppertNumLab.backgroundColor = [UIColor clearColor];
        _suppertNumLab.textColor = kColorRGBValue(0xf03434);
        _suppertNumLab.textAlignment = NSTextAlignmentCenter;
        
    }
    return _suppertNumLab;
}

- (UIImageView *)suppertNumBg{
    if (!_suppertNumBg) {
        _suppertNumBg = [[UIImageView alloc] init];
        _suppertNumBg.backgroundColor = [UIColor whiteColor];
        _suppertNumBg.layer.cornerRadius = 7.0;
        _suppertNumBg.layer.shadowColor = kColorRGBValue(0x575757).CGColor;
        _suppertNumBg.layer.shadowOffset = CGSizeMake(0, 0);
        _suppertNumBg.layer.shadowOpacity = 0.4;
        _suppertNumBg.layer.shadowRadius = 5.0;
    }
    return _suppertNumBg;
}

- (BYLikeAnimationView *)animationView{
    if (!_animationView) {
        _animationView = [[BYLikeAnimationView alloc] init];
    }
    return _animationView;
}

- (UIButton *)forbidBtn{
    if (!_forbidBtn) {
        _forbidBtn = [UIButton by_buttonWithCustomType];
        [_forbidBtn setBy_imageName:@"microlive_forbid" forState:UIControlStateNormal];
        [_forbidBtn addTarget:self action:@selector(forbidBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _forbidBtn;
}

- (BYILiveForbidListView *)forbidListView{
    if (!_forbidListView) {
        _forbidListView = [BYILiveForbidListView initVideoListViewShowInView:CURRENT_VC.view];
        _forbidListView.superViewController = CURRENT_VC;
    }
    return _forbidListView;
}
@end
