//
//  BYMicroAudienceToolBar.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYIMMessageInputView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYMicroAudienceToolBar : UIView

/** 弹幕开关 */
@property (nonatomic ,copy) void (^barrageIsOnHandle)(BOOL isOn);

/** 是否可操作(默认YES) */
@property (nonatomic ,assign) BOOL enabled;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** targetView */
//@property (nonatomic ,weak) UIView *targetView;
@property (nonatomic ,readonly,strong) BYIMMessageTextView *inputView;
/** 禁言状态 */
@property (nonatomic ,assign) BOOL isForbid;

- (void)resetToolView;

- (void)reloadSuppertNum;

@end

NS_ASSUME_NONNULL_END
