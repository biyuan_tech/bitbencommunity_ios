//
//  BYMicroLiveController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYMicroLiveController.h"
#import "BYMicroLiveViewModel.h"

@interface BYMicroLiveController ()<NetworkAdapterSocketDelegate>

@end

@implementation BYMicroLiveController

- (void)dealloc
{
    [(BYMicroLiveViewModel *)self.viewModel destoryTimer];
    [[BYSIMManager shareManager] resetScoketStatus];
}

- (Class)getViewModelClass{
    return [BYMicroLiveViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    self.hasCancelSocket = NO;
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    [super viewDidDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:kColorRGBValue(0xe7e7ea)]];
    // Do any additional setup after loading the view.
}

-(void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data{
    [[BYSIMManager shareManager] webSocketReceiveData:type data:data];
}

-(void)webSocketDidConnectedWithSocket:(WebSocketConnection *)webSocketConnection connectedStatus:(SocketConnectType)connectedStatus{
    if (webSocketConnection == [NetworkAdapter sharedAdapter].mainRootSocketConnection) {
        if (connectedStatus == SocketConnectTypeOpen) {
            [[BYSIMManager shareManager] webSocketDidConnectedWithSocket:webSocketConnection];
        }
    }
}


@end
