//
//  BYMicroLiveController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYMicroLiveController : BYCommonViewController

/** 直播记录id */
@property (nonatomic ,copy) NSString *live_record_id;
/** 是否为主播 */
@property (nonatomic ,assign) BOOL isHost;
/** 身份 */
@property (nonatomic ,assign) BY_IDENTITY_TYPE identityType;


@end

NS_ASSUME_NONNULL_END
