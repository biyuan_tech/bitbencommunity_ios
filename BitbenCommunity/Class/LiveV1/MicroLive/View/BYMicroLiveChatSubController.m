//
//  BYMicroLiveChatSubController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYMicroLiveChatSubController.h"
#import "BYPersonHomeController.h"

#import "BYMicroChatToolView.h"
#import "CDChatList.h"
#import "BYNewMsgBottomView.h"
#import "BYPFUserInfoView.h"


#import "BYHistoryIMModel.h"

@interface BYMicroLiveChatSubController ()<ChatListProtocol>

/** tableview */
@property (nonatomic ,strong) CDChatListView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;
/** 底部输入 */
@property (nonatomic ,strong) BYMicroChatToolView *toolView;
/** newmsgBottomView */
@property (nonatomic ,strong) BYNewMsgBottomView *newMsgBottomView;
/** 记录当前未读消息 */
@property (nonatomic ,strong) NSMutableArray *noReadMsgs;
/** personView */
@property (nonatomic ,strong) BYPFUserInfoView *personView;

@end

@implementation BYMicroLiveChatSubController
- (void)dealloc
{
    [_tableView removeFromSuperview];
    [_toolView removeFromSuperview];
    _tableView = nil;
    _toolView = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageNum = 0;
    self.noReadMsgs = [NSMutableArray array];
    // 非主播则底部添加输入控件
    if (_identityType == BY_IDENTITY_TYPE_NORMAL) {
        [self addInputView];
    }
    [self addTableView];
    [self configDefaultResource];
}

- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    if (model.status == BY_LIVE_STATUS_END ||
        model.status == BY_LIVE_STATUS_OVERTIME) {
        [self.toolView setNoEdit:YES];
    }
    self.toolView.isForbid = [BYSIMManager shareManager].isUserForbid;
    [self loadRequestGetHistoryMsg:nil];
    [self updateConfig];
}

- (void)addMessageData:(NSArray *)msgs{
    if (!msgs.count) return;
    NSDictionary *dic = msgs[0];
    if (dic) {
        if ([dic[@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
            NSDictionary *customData;
            if ([dic[@"content"] isKindOfClass:[NSString class]]) {
                // 现返回的为纯json字符串，需要转两次
                NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            }
            else{
                customData = dic[@"content"];
            }

            if ([customData[kBYCustomMessageType] integerValue] == BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM) { // 关播消息
                [self.toolView setNoEdit:YES];
            }
            
            if ([dic[@"type"] isEqualToString:msg_open_forbid] ||
                [dic[@"type"] isEqualToString:msg_close_forbid]) {
                NSString *forbid_user_id = dic[@"forbid_user_id"];
                if ([forbid_user_id isEqualToString:ACCOUNT_ID]) {
                    BOOL isForbid = [dic[@"type"] isEqualToString:msg_open_forbid] ? YES : NO;
                    [BYSIMManager shareManager].isUserForbid = isForbid;
                    _toolView.isForbid = isForbid;
                }
            }
            
            return ;
        }
    }
    BOOL isFullScreen = _tableView.contentSize.height <= _tableView.frame.size.height ? NO : YES;
    NSArray *data = [BYHistoryIMModel getMicroChatTableData:msgs liveData:self.model];
    if (!data.count) return;
    BOOL scrollToBottom = _tableView.contentOffset.y >= floor(_tableView.contentSize.height - _tableView.frame.size.height) ? YES : NO;
    scrollToBottom = scrollToBottom || !isFullScreen ? scrollToBottom : NO;
    [self.tableView addMessagesToBottom:data scrollToBottom:scrollToBottom];
    self.newMsgBottomView.hidden = scrollToBottom;
    if (!scrollToBottom) {
        [self.noReadMsgs addObjectsFromArray:data];
        [self.newMsgBottomView setNewMsgNum:self.noReadMsgs.count];
    }
}

#pragma mark - action
// 消息发送
- (void)sendMessage:(NSString *)message{
    NSString *string = [message removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
    if (!string.length) return;
    BYSIMMessage *msg = [[BYSIMMessage alloc] init];
    msg.msg_type = BY_SIM_MSG_TYPE_TEXT;
    msg.msg_sender = [AccountModel sharedAccountModel].account_id;
    msg.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
    msg.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
    msg.time = [NSDate getCurrentTimeStr];
    BYSIMTextElem *elem = [[BYSIMTextElem alloc] init];
    elem.text = string;
    msg.elem = elem;
    [[BYSIMManager shareManager] sendMessage:msg suc:nil fail:nil];
    [self.toolView resetToolView];
}

- (void)scrollToMsgBottom{
    [self.tableView relayoutTable:YES];
}

#pragma mark - chatListViewDelegate
- (void)chatlistBecomeFirstResponder{
    [self.view endEditing:YES];
    [_toolView.inputView resignFirstResponder];
}

-(void)chatlistLoadMoreMsg: (CDChatMessage)topMessage
                  callback: (void(^)(CDChatMessageArray, BOOL))finnished{
    [self loadRequestGetHistoryMsg:^(NSArray *data) {
        finnished(data,data.count ? YES : NO);
    }];
}

-(CGSize)chatlistSizeForMsg:(CDChatMessage)msg ofList:(CDChatListView *)list{
    if (![msg.customData[@"text"] length]) {
        return CGSizeZero;
    }
    CGSize size = [msg.customData[@"text"] getStringSizeWithFont:[UIFont systemFontOfSize:12] maxWidth:kCommonScreenWidth-60];
    return CGSizeMake(kCommonScreenWidth, ceil(size.height) + 42);
}

-(void)chatlistClickMsgEvent: (ChatListInfo *)listInfo{
    switch (listInfo.eventType) {
        case ChatClickEventTypeHeadImg:
        {
//            if (![AccountModel sharedAccountModel].hasLoggedIn) {
//                [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
//                return;
//            }
//            if (![listInfo.msgModel.userId verifyIsNum]) return;
//            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
//            personHomeController.user_id = listInfo.msgModel.userId;
//            [CURRENT_VC.navigationController pushViewController:personHomeController animated:YES];
            if (![AccountModel sharedAccountModel].hasLoggedIn) {
                [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
                return;
            }
            NSString *userId = listInfo.msgModel.userId ? listInfo.msgModel.userId: self.model.user_id;
            if ([userId isEqualToString:ACCOUNT_ID]) return;
            if (![userId verifyIsNum]) return;
            self.personView.user_id = userId;
            [self.personView showAnimation];
        }
            break;
        default:
            break;
    }
}

- (void)chatlistDidDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height) {
        self.newMsgBottomView.hidden = YES;
        [self.noReadMsgs removeAllObjects];
    }
}

#pragma mark - request
- (void)loadRequestGetHistoryMsg:(void(^)(NSArray *data))cb{
    @weakify(self);
    
    [[BYLiveHomeRequest alloc] loadRequestGetPageChatHistory:self.model.group_id type:1 pageNum:self.pageNum sendTime:[BYSIMManager shareManager].joinTime successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        if (![object[@"content"] isKindOfClass:[NSArray class]] || ![object[@"content"] count]) {
            if (cb) cb(@[]);
            return;
        }
        if (self.pageNum == 0) {
            NSMutableArray *data = [NSMutableArray array];
            NSArray *array = [BYHistoryIMModel getMicroChatTableData:object[@"content"] liveData:self.model];
            array = [[array reverseObjectEnumerator] allObjects];
            [data addObjectsFromArray:array];
            [data addObjectsFromArray:self.tableView.msgArr];
            self.tableView.msgArr = array;
            [self.tableView reloadData];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.tableView relayoutTable:NO];
            });
        }
        else{
            NSArray *array = [BYHistoryIMModel getMicroChatTableData:object[@"content"] liveData:self.model];
            array = [[array reverseObjectEnumerator] allObjects];
            cb(array);
        }
        self.pageNum ++;
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - configUI

- (void)updateConfig{
    if (_identityType != BY_IDENTITY_TYPE_NORMAL) {
        self.toolView.hidden = YES;
    }
    CGFloat bottom = _identityType == BY_IDENTITY_TYPE_NORMAL ? -kSafe_Mas_Bottom(49) : 0;
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(bottom);
    }];
}

- (void)addInputView{
    self.toolView = [[BYMicroChatToolView alloc] init];
    @weakify(self);
    self.toolView.didSendMessageHandle = ^(NSString * _Nonnull string) {
        @strongify(self);
        [self sendMessage:string];
    };
    [self.view addSubview:self.toolView];
    [self.toolView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(49));
    }];
}

- (void)addTableView{
    if (_tableView) return;
    self.tableView = [[CDChatListView alloc] initWithFrame:CGRectZero];
    self.tableView.msgDelegate = self;
    [self.tableView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.view addSubview:self.tableView];
    [self.view sendSubviewToBack:self.tableView];
    CGFloat bottom = _identityType == BY_IDENTITY_TYPE_NORMAL ? kSafe_Mas_Bottom(49) : 0;
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, bottom, 0));
    }];
    
    [self.view addSubview:self.newMsgBottomView];
    [self.newMsgBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(32);
        make.width.mas_greaterThanOrEqualTo(115);
        make.bottom.mas_equalTo(self.tableView).offset(-10);
    }];
}

#pragma mark - config
-(void)configDefaultResource{
    
    // 聊天页面图片资源配置
    
    ChatHelpr.share.config.msgBackGroundColor = [UIColor whiteColor];
    NSMutableDictionary *resDic = [NSMutableDictionary dictionaryWithDictionary:ChatHelpr.share.imageDic];
    [resDic setObject:[UIImage imageNamed:@"talk_voice_left_1"] forKey:@"voice_left_1"];
    [resDic setObject:[UIImage imageNamed:@"talk_voice_left_2"] forKey:@"voice_left_2"];
    [resDic setObject:[UIImage imageNamed:@"talk_voice_left_3"] forKey:@"voice_left_3"];
    [resDic setObject:[UIImage imageNamed:@"talk_voice_right_1"] forKey:@"voice_right_1"];
    [resDic setObject:[UIImage imageNamed:@"talk_voice_right_2"] forKey:@"voice_right_2"];
    [resDic setObject:[UIImage imageNamed:@"talk_voice_right_3"] forKey:@"voice_right_3"];
//    [resDic setObject:[[UIImage imageNamed:@"talk_pop_l_nor"] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 8, 6, 6)] forKey:@"talk_pop_l_nor"];
    [resDic setObject:[[UIImage imageNamed:@"talk_pop_l_nor"] resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6)] forKey:@"talk_pop_l_nor"];
    [resDic setObject:[UIImage imageNamed:@"common_userlogo_bg"] forKey:@"common_userlogo_bg"];
    [resDic setObject:[UIImage imageNamed:@"iliveroom_reward"] forKey:@"reward_left"];
    [resDic setObject:[UIImage imageNamed:@"icon_live_banned"] forKey:@"banned_left"];
    NSDictionary *drawImages = [ChatImageDrawer defaultImageDic];
    for (NSString *imageName in drawImages) {
        resDic[imageName] = drawImages[imageName];
    }
    
    ChatHelpr.share.imageDic = resDic;
    ChatHelpr.share.config.left_box = @"talk_pop_l_nor";
    ChatHelpr.share.config.right_box = @"talk_pop_l_nor";
    // 添加语音输入功能
    [CTinputHelper.share.config addVoice];
    
}

- (BYPFUserInfoView *)personView{
    if (!_personView) {
        _personView = [[BYPFUserInfoView alloc] initWithFathureView:CURRENT_VC.view];
        _personView.model = self.model;
        if ((_identityType == BY_IDENTITY_TYPE_HOST ||
             _identityType == BY_IDENTITY_TYPE_CREATOR) &&
            (_model.status == BY_LIVE_STATUS_SOON ||
             _model.status == BY_LIVE_STATUS_LIVING)) {
                _personView.hasForbid = YES;
            }else {
                _personView.hasForbid = NO;
            }

    }
    return _personView;
}

- (BYNewMsgBottomView *)newMsgBottomView{
    if (!_newMsgBottomView) {
        _newMsgBottomView = [[BYNewMsgBottomView alloc] init];
        _newMsgBottomView.hidden = YES;
        [_newMsgBottomView addTarget:self action:@selector(scrollToMsgBottom) forControlEvents:UIControlEventTouchUpInside];
    }
    return _newMsgBottomView;
}

@end
