//
//  BYMicroLiveSubController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYMicroLiveSubController.h"
#import "CDChatList.h"
#import "BYMicroHostToolBar.h"      // 主播toolbar操作台
#import "BYMicroAudienceToolBar.h"  // 观众toolbar操作台
#import "MsgPicViewController.h"
#import "BYPersonHomeController.h"
#import "BYVideoLiveRewradTableCell.h"
#import "BYImageBrowseController.h"
#import "BYPFUserInfoView.h"
#import "BYNewMsgBottomView.h"

#import "BYHistoryIMModel.h"
#import "BYBarrageModel.h"

@interface BYMicroLiveSubController ()<ChatListProtocol,BYCommonTableViewDelegate>

/** tableview */
@property (nonatomic ,strong) CDChatListView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;
/** 弹幕 */
@property (nonatomic ,strong) BYCommonTableView *barrageTableView;
/** 弹幕页码 */
@property (nonatomic ,assign) NSInteger barragePageNum;
/** 主播toolbar */
@property (nonatomic ,strong) BYMicroHostToolBar *hostToolBar;
/** 观众toolbar */
@property (nonatomic ,strong) BYMicroAudienceToolBar *audienceToolBar;
/** 记录当前发送中的图片信息 */
@property (nonatomic ,strong) NSMutableArray *image_msgs;
/** personView */
@property (nonatomic ,strong) BYPFUserInfoView *personView;
/** newmsgBottomView */
@property (nonatomic ,strong) BYNewMsgBottomView *newMsgBottomView;
/** 记录当前未读消息 */
@property (nonatomic ,strong) NSMutableArray *noReadMsgs;

@end

@implementation BYMicroLiveSubController

- (void)dealloc
{
    [_tableView removeFromSuperview];
    [_barrageTableView removeFromSuperview];
    [_hostToolBar removeFromSuperview];
    [_audienceToolBar removeFromSuperview];
    [_personView removeFromSuperview];
    [_model removeObserver:self forKeyPath:@"count_support"];
    _tableView = nil;
    _barrageTableView = nil;
    _hostToolBar = nil;
    _audienceToolBar = nil;
    _personView = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageNum = 0;
    self.barragePageNum = 0;
    self.image_msgs = [NSMutableArray array];
    self.noReadMsgs = [NSMutableArray array];
    [self configDefaultResource];
    
}

- (void)configUI{
    [self addTableView];
    [self addToolBar];
    [self addBarrageTableView];
    [self addTopBottomFastBtn];
}

- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    if (model.status == BY_LIVE_STATUS_END ||
        model.status == BY_LIVE_STATUS_OVERTIME) {
//        [self.toolView setNoEdit:YES];
        _audienceToolBar.enabled = NO;
    }
    [self configUI];
    [self addModelObserver];
    _audienceToolBar.model = model;
    _hostToolBar.model = model;
    [self loadRequestGetHistoryMsg:NO cb:nil];
    [self loadRequestGetHistoryMsg:YES cb:nil];
}

- (void)addMessageData:(NSArray *)msgs{
    if (!msgs.count) return;
    NSDictionary *dic = msgs[0];
    if (dic) {
        if ([dic[@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
            NSDictionary *customData;
            if ([dic[@"content"] isKindOfClass:[NSString class]]) {
                // 现返回的为纯json字符串，需要转两次
                NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            }
            else{
                customData = dic[@"content"];
            }

            if ([customData[kBYCustomMessageType] integerValue] == BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM) { // 关播消息
                [self.audienceToolBar setEnabled:NO];
                _hostToolBar.enabled = NO;
//                [self.toolView setNoEdit:YES];
            }
            
            if ([dic[@"type"] isEqualToString:msg_open_forbid] ||
                [dic[@"type"] isEqualToString:msg_close_forbid]) {
                NSString *forbid_user_id = dic[@"forbid_user_id"];
                if ([forbid_user_id isEqualToString:ACCOUNT_ID]) {
                    BOOL isForbid = [dic[@"type"] isEqualToString:msg_open_forbid] ? YES : NO;
                    [BYSIMManager shareManager].isUserForbid = isForbid;
                    _audienceToolBar.isForbid = isForbid;
                }
            }
            
            return ;
        }
    }
    
    NSArray *data = [BYHistoryIMModel getMicroLiveTableData:msgs liveData:self.model];
    NSArray *array = [self removeSelfSendingMsg:data];
    if (array.count) {
        BOOL isFullScreen = _tableView.contentSize.height <= _tableView.frame.size.height ? NO : YES;
        BOOL scrollToBottom = _tableView.contentOffset.y >= floor(_tableView.contentSize.height - _tableView.frame.size.height) ? YES : NO;
        scrollToBottom = scrollToBottom || !isFullScreen ? scrollToBottom : NO;
        [self.tableView addMessagesToBottom:array scrollToBottom:scrollToBottom];
        self.newMsgBottomView.hidden = scrollToBottom;
        if (!scrollToBottom) {
            [self.noReadMsgs addObjectsFromArray:array];
            [self.newMsgBottomView setNewMsgNum:self.noReadMsgs.count];
        }
    }
    
    NSArray *barrage = [BYBarrageModel getTableData:msgs liveModel:self.model];
    if (barrage.count) {
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.barrageTableView.tableData];
        [array addObjectsFromArray:barrage];
        self.barrageTableView.tableData = array;
        [self.barrageTableView scrollToBottom:YES];
    }
}

- (NSArray *)removeSelfSendingMsg:(NSArray *)msgs{
    if (!self.image_msgs.count) {
        return msgs;
    }
    NSMutableArray *data = [NSMutableArray arrayWithArray:msgs];
    NSMutableArray *tmpArr = msgs.mutableCopy;
    NSMutableArray *tmpImgsMsgArr = self.image_msgs.mutableCopy;
    for (BYHistoryIMModel *model in tmpArr) {
        for (BYHistoryIMModel *imgMsgModel in tmpImgsMsgArr) {
            if ([[model.msg getFullImageUploadUrl] isEqualToString:[imgMsgModel.msg getFullImageUploadUrl]]) {
                imgMsgModel.messageId = model.messageId;
                imgMsgModel.msg = [model.msg getFullImageUploadUrl];
                [data removeObject:model];
                [self.image_msgs removeObject:model];
            }
        }
    }
    return data;
}

- (void)barrageLoadMoreData{
    @weakify(self);
    [self loadRequestGetHistoryMsg:YES cb:^(NSArray *data) {
        @strongify(self);
        NSMutableArray *array = [NSMutableArray arrayWithArray:data];
        [array addObjectsFromArray:self.barrageTableView.tableData];
        self.barrageTableView.tableData = array;
    }];
}

- (void)barrageTableViewIsShow:(BOOL)isShow{
    [UIView animateWithDuration:0.2 animations:^{
        self.barrageTableView.alpha = isShow ? 1.0 : 0.0;
    }];
}

- (void)getImageIndex:(NSString *)imageUrl cb:(void(^)(NSInteger index,NSArray *images))cb{
    __block NSMutableArray *array = [NSMutableArray array];
    [self.tableView.msgArr enumerateObjectsUsingBlock:^(CDChatMessage  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.msgType == CDMessageTypeImage) {
            [array addObject:obj.msg];
        }
    }];
    NSInteger imageIndex = [array indexOfObject:imageUrl];
    cb(imageIndex,[array copy]);
}

#pragma mark - action
// 消息发送
- (void)sendMessage:(NSString *)message{
    
}

// 结束直播
- (void)finishLiveAction{
    @weakify(self);
    [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_END cb:^{
        @strongify(self);
        @weakify(self);
        [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM data:nil suc:^{
            
        } fail:nil];
        [[BYSIMManager shareManager] closeRoom:self.model.group_id suc:^{
            @strongify(self);
            showDebugToastView(@"关闭聊天室成功",self.view);
        } fail:^{
            @strongify(self);
            showDebugToastView(@"关闭聊天室失败",self.view);
        }];
        self.model.status = BY_LIVE_STATUS_END;
        UIViewController *currentController = [BYTabbarViewController sharedController].currentController;
        UIViewController *viewController = currentController.navigationController.viewControllers[1];
        [currentController.navigationController popToViewController:viewController animated:YES];
    }];
}

- (void)sendImageMessage:(NSArray *)images{
    NSArray *msgArr = [BYHistoryIMModel getNoSendImgMsgData:images liveData:self.model];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView addMessagesToBottom:msgArr];
    });
    [self.image_msgs addObjectsFromArray:msgArr];
    [self loadUploadSelectImg:images msgArr:msgArr];
}

- (void)scrollToMsgBottom{
    [self.tableView relayoutTable:YES];
}

#pragma mark - chatListViewDelegate
- (void)chatlistBecomeFirstResponder{
//    [self.view endEditing:YES];
    [_hostToolBar resignFirstResponder];
    [_audienceToolBar resignFirstResponder];
}

-(void)chatlistLoadMoreMsg: (CDChatMessage)topMessage
                  callback: (void(^)(CDChatMessageArray, BOOL))finnished{
    [self loadRequestGetHistoryMsg:NO cb:^(NSArray *data) {
        finnished(data,data.count ? YES : NO);
    }];
}

- (NSDictionary<NSString *,Class> *)chatlistCustomeCellsAndClasses{
    return @{@"RewardCell": BYVideoLiveRewradTableCell.class};
}

-(CGSize)chatlistSizeForMsg:(CDChatMessage)msg ofList:(CDChatListView *)list{
    if (![msg.customData[@"text"] length]) {
        return CGSizeZero;
    }
    CGFloat maxWidth = iPhone5 ? kCommonScreenWidth - 70*2 : kCommonScreenWidth - 80*2;
    CGSize size = [msg.customData[@"text"] getStringSizeWithFont:[UIFont systemFontOfSize:12] maxWidth:maxWidth];
    return CGSizeMake(kCommonScreenWidth, ceil(size.height) + 14 + 15);
}

-(void)chatlistClickMsgEvent: (ChatListInfo *)listInfo{
    switch (listInfo.eventType) {
        case ChatClickEventTypeIMAGE:
        {
            CGRect newe =  [listInfo.containerView.superview convertRect:listInfo.containerView.frame toView:kCommonWindow];
            @weakify(self);
            [self getImageIndex:listInfo.msgModel.msg cb:^(NSInteger index, NSArray *images) {
                @strongify(self);
                [BYImageBrowseController addToRootViewController:listInfo.image index:index rect:newe images:images inView:self];
            }];
//            [MsgPicViewController addToRootViewController:listInfo.image ofMsgId:listInfo.msgModel.messageId in:newe from:self.tableView.msgArr];
        }
            break;
        case ChatClickEventTypeHeadImg:
        {
//            if ([listInfo.msgModel.userId verifyIsNum]) {
//                BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
//                personHomeController.user_id = listInfo.msgModel.userId;
//                [CURRENT_VC.navigationController pushViewController:personHomeController animated:YES];
//            }
            if (![AccountModel sharedAccountModel].hasLoggedIn) {
                [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
                return;
            }
            NSString *userId = listInfo.msgModel.userId ? listInfo.msgModel.userId: self.model.user_id;
            if ([userId isEqualToString:ACCOUNT_ID]) return;
            if (![userId verifyIsNum]) return;
            self.personView.user_id = userId;
            [self.personView showAnimation];
        }
            break;
        case ChatClickEventTypeAUDIO:
            break;
        case ChatClickEventTypeREWARD: // 点击赞赏
            break;
        case ChatClickEventTypeBanned: // 点击禁言
            showToastView(@"点击禁言", self.view);
            break;
        default:
            break;
    }
}

- (void)chatlistAudioMsgReadStatusDidChange:(CDChatMessage)msg{
    // 设置语音消息为已读
    [[BYIMMessageDB shareManager] modifyIMMessageIsRead:msg.messageId];
}

- (void)chatlistDidDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height) {
        self.newMsgBottomView.hidden = YES;
        [self.noReadMsgs removeAllObjects];
    }
}

#pragma mark - request
- (void)loadRequestGetHistoryMsg:(BOOL)isBarrage cb:(void(^)(NSArray *data))cb{
    NSInteger pageNum = isBarrage ? self.barragePageNum : self.pageNum;
    NSInteger type    = isBarrage ? 1 : 0;
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetPageChatHistory:self.model.group_id type:type pageNum:pageNum sendTime:[BYSIMManager shareManager].joinTime successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        if (![object[@"content"] isKindOfClass:[NSArray class]] || ![object[@"content"] count]) {
            if (cb) cb(@[]);
            if (isBarrage) {
                [self.barrageTableView endRefreshing];
            }
            return;
        }
        // 是否为弹幕区内容加载
        if (isBarrage) {
            [self.barrageTableView endRefreshing];
            if (self.barragePageNum == 0) {
                // 弹幕内容
                NSMutableArray *barrageData = [NSMutableArray array];
                NSArray *barrageArr = [BYBarrageModel getTableData:object[@"content"] liveModel:self.model];
                barrageArr = [[barrageArr reverseObjectEnumerator] allObjects];
                [barrageData addObjectsFromArray:barrageArr];
                [barrageData addObjectsFromArray:self.barrageTableView.tableData];
                self.barrageTableView.tableData = barrageArr;
                [self.barrageTableView scrollToBottom:YES];
            }else{
                NSArray *array = [BYBarrageModel getTableData:object[@"content"] liveModel:self.model];
                array = [[array reverseObjectEnumerator] allObjects];
                cb(array);
            }
            self.barragePageNum++;
        }else{
            if (self.pageNum == 0) {
                // 直播区内容
                NSMutableArray *data = [NSMutableArray array];
                NSArray *array = [BYHistoryIMModel getMicroLiveTableData:object[@"content"] liveData:self.model];
                array = [[array reverseObjectEnumerator] allObjects];
                [data addObjectsFromArray:array];
                [data addObjectsFromArray:self.tableView.msgArr];
                self.tableView.msgArr = data;
                [self.tableView reloadData];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.tableView relayoutTable:NO];
                });
            }
            else{
                NSArray *array = [BYHistoryIMModel getMicroLiveTableData:object[@"content"] liveData:self.model];
                array = [[array reverseObjectEnumerator] allObjects];
                cb(array);
            }
            self.pageNum ++;
        }
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.barrageTableView endRefreshing];
    }];
}

- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status cb:(void(^)(void))cb{
    if (!self.model.real_begin_time.length) {
        self.model.real_begin_time = [NSDate by_stringFromDate:[NSDate by_date] dateformatter:K_D_F];
    }
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveStatus:status live_record_id:_model.live_record_id stream_id:nil real_begin_time:self.model.real_begin_time successBlock:^(id object) {
        if (cb) cb();
        PDLog(@"更新直播间状态成功");
    } faileBlock:^(NSError *error) {
        PDLog(@"更新直播间状态失败");
    }];
}

- (void)loadUploadSelectImg:(NSArray *)images msgArr:(NSArray *)msgArr{
    NSMutableArray *fileModels = [NSMutableArray array];
    for (int i = 0 ; i < images.count; i ++) {
        UIImage *image = images[i];
        OSSFileModel *fileModel = [[OSSFileModel alloc] init];
        fileModel.objcImage = image;
        fileModel.objcName  = [NSString stringWithFormat:@"%@-%@-%d",[NSDate by_stringFromDate:[NSDate by_date] dateformatter:@"yyyy-MM-dd-HH:mm:ss"],@"MSG_IMG",i];
        [fileModels addObject:fileModel];
    }
    [[OSSManager sharedUploadManager] uploadImageManagerWithImgList:[fileModels copy] progress:^(NSInteger index, float percent) {
        
    } finish:^(NSArray *url, NSInteger index, BOOL allUpload) {
        PDLog(@"接收到图片上传回调---index:%li",index);
        if (allUpload) return ;
        BYHistoryIMModel *msgModel = msgArr[index];
        BYSIMMessage *msg = [[BYSIMMessage alloc] init];
        msg.msg_type = BY_SIM_MSG_TYPE_IMAGE;
        msg.msg_sender = [AccountModel sharedAccountModel].account_id;
        msg.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
        msg.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
        msg.time = [NSDate getCurrentTimeStr];
        BYSIMImageElem *elem = [[BYSIMImageElem alloc] init];
        elem.path = url[0];
        msg.elem = elem;
        msgModel.msg = url[0];
        @weakify(self);
        [[BYSIMManager allocManager] sendMessage:msg suc:^{
            @strongify(self);
            dispatch_async(dispatch_get_main_queue(), ^{
                msgModel.msgState = CDMessageStateNormal;
                [self.tableView updateMessage:msgModel];
//                BOOL isFullScreen = self.tableView.contentSize.height <= self.tableView.frame.size.height ? NO : YES;
//                BOOL scrollToBottom = self.tableView.contentOffset.y >= floor(self.tableView.contentSize.height - self.tableView.frame.size.height) ? YES : NO;
//                scrollToBottom = scrollToBottom || !isFullScreen ? scrollToBottom : NO;
//                if (scrollToBottom) {
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                        [self.tableView relayoutTable:YES];
//                    });
//                }
            });
        } fail:^{
            @strongify(self);
            dispatch_async(dispatch_get_main_queue(), ^{
                msgModel.msgState = CDMessageStateSendFaild;
                [self.tableView updateMessage:msgModel];
            });
        }];
    }];
}

#pragma mark - KVO
- (void)addModelObserver{
    [_model addObserver:self forKeyPath:@"count_support" options:NSKeyValueObservingOptionNew context:nil];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"count_support"]){
        [_hostToolBar reloadSuppertNum];
        [_audienceToolBar reloadSuppertNum];
    }
}

#pragma mark - configUI

- (void)addTableView{
    if (_tableView) return;
    self.tableView = [[CDChatListView alloc] initWithFrame:CGRectZero];
    self.tableView.msgDelegate = self;
    [self.tableView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.view addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kSafe_Mas_Bottom(49), 0));
    }];
    
    [self.view addSubview:self.newMsgBottomView];
    [self.newMsgBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(32);
        make.width.mas_greaterThanOrEqualTo(115);
        make.bottom.mas_equalTo(self.tableView).offset(-10);
    }];
}

- (void)addToolBar{
    if ((_identityType == BY_IDENTITY_TYPE_HOST || _identityType == BY_IDENTITY_TYPE_GUEST || _identityType == BY_IDENTITY_TYPE_CREATOR) && (self.model.status == BY_LIVE_STATUS_LIVING || self.model.status == BY_LIVE_STATUS_SOON)) {
        self.hostToolBar = [[BYMicroHostToolBar alloc] init];
        @weakify(self);
        self.hostToolBar.barrageIsOnHandle = ^(BOOL isOn) {
            @strongify(self);
            [self barrageTableViewIsShow:isOn];
        };
        self.hostToolBar.didExitLiveBtnHandle = ^{
            @strongify(self);
            [self finishLiveAction];
        };
        self.hostToolBar.didSelectImageDataHandle = ^(NSArray * _Nonnull images) {
            @strongify(self);
            [self sendImageMessage:images];
        };
        [self.view addSubview:self.hostToolBar];
        [self.hostToolBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(0);
            make.height.mas_equalTo(kSafe_Mas_Bottom(49));
        }];
        return;
    }
    
    self.audienceToolBar = [[BYMicroAudienceToolBar alloc] init];
//    self.audienceToolBar.targetView = self.view;
    self.audienceToolBar.model = self.model;
    if (self.model.status == BY_LIVE_STATUS_SOON ||
        self.model.status == BY_LIVE_STATUS_LIVING) {
        self.audienceToolBar.enabled = YES;
    }else{
        self.audienceToolBar.enabled = NO;
    }
    self.audienceToolBar.isForbid = [BYSIMManager shareManager].isUserForbid;
    @weakify(self);
    self.audienceToolBar.barrageIsOnHandle = ^(BOOL isOn) {
        @strongify(self);
        [self barrageTableViewIsShow:isOn];
    };
    [self.view addSubview:self.audienceToolBar];
    [self.audienceToolBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(49));
    }];
}

- (void)addBarrageTableView{
    self.barrageTableView = [[BYCommonTableView alloc] init];
    self.barrageTableView.group_delegate = self;
    self.barrageTableView.backgroundColor = [UIColor clearColor];
    self.barrageTableView.scrollEnabled = NO;
    self.barrageTableView.notResponder = YES;
    [self.barrageTableView addHeaderRefreshTarget:self action:@selector(barrageLoadMoreData)];
    [self.view addSubview:self.barrageTableView];
    if (_hostToolBar) {
        [self.view insertSubview:self.barrageTableView belowSubview:self.hostToolBar];
    }else{
        [self.view insertSubview:self.barrageTableView belowSubview:self.audienceToolBar];
    }
    [self.barrageTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-kSafe_Mas_Bottom(49));
        make.height.mas_equalTo(148);
    }];
}

- (void)addTopBottomFastBtn{
    if (_identityType != BY_IDENTITY_TYPE_NORMAL) return;
    NSArray *imageNames = @[@"microlive_fast_top",@"microlive_fast_bottom"];
    for (int i = 0; i < 2; i ++) {
        UIButton *btn = [UIButton by_buttonWithCustomType];
        [btn setBy_imageName:imageNames[i] forState:UIControlStateNormal];
        @weakify(self);
        [btn buttonWithBlock:^(UIButton *button) {
            @strongify(self);
            if (i == 0) {
                [self.tableView setContentOffset:CGPointZero animated:YES];
            }else {
                [self.tableView relayoutTable:YES];
            }
        }];
        [self.view addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(50);
            make.top.mas_equalTo(16 + 53*i);
            make.right.mas_equalTo(-7);
        }];
    }
}


#pragma mark - config
-(void)configDefaultResource{
    
    // 聊天页面图片资源配置
    
    ChatHelpr.share.config.msgBackGroundColor = [UIColor whiteColor];
    NSMutableDictionary *resDic = [NSMutableDictionary dictionaryWithDictionary:ChatHelpr.share.imageDic];
//    [resDic setObject:[UIImage imageNamed:@"voice_left_1"] forKey:@"voice_left_1"];
//    [resDic setObject:[UIImage imageNamed:@"voice_left_2"] forKey:@"voice_left_2"];
//    [resDic setObject:[UIImage imageNamed:@"voice_left_3"] forKey:@"voice_left_3"];
    [resDic setObject:[UIImage imageNamed:@"talk_voice_left_1"] forKey:@"voice_left_1"];
    [resDic setObject:[UIImage imageNamed:@"talk_voice_left_2"] forKey:@"voice_left_2"];
    [resDic setObject:[UIImage imageNamed:@"talk_voice_left_3"] forKey:@"voice_left_3"];
    [resDic setObject:[UIImage imageNamed:@"talk_voice_right_1"] forKey:@"voice_right_1"];
    [resDic setObject:[UIImage imageNamed:@"talk_voice_right_2"] forKey:@"voice_right_2"];
    [resDic setObject:[UIImage imageNamed:@"talk_voice_right_3"] forKey:@"voice_right_3"];
    //    [resDic setObject:[[UIImage imageNamed:@"talk_pop_l_nor"] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 8, 6, 6)] forKey:@"talk_pop_l_nor"];
    [resDic setObject:[[UIImage imageNamed:@"talk_pop_l_nor"] resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6)] forKey:@"talk_pop_l_nor"];
    [resDic setObject:[[UIImage imageNamed:@"talk_pop_l_nor"] resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6)] forKey:@"bg_mask_right"];
    [resDic setObject:[[UIImage imageNamed:@"talk_pop_l_nor"] resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6)] forKey:@"bg_mask_left"];
    [resDic setObject:[UIImage imageNamed:@"common_userlogo_bg"] forKey:@"common_userlogo_bg"];
    [resDic setObject:[UIImage imageNamed:@"iliveroom_reward"] forKey:@"reward_left"];
    [resDic setObject:[UIImage imageNamed:@"icon_live_banned"] forKey:@"banned_left"];
    NSDictionary *drawImages = [ChatImageDrawer defaultImageDic];
    for (NSString *imageName in drawImages) {
        resDic[imageName] = drawImages[imageName];
    }
    
    ChatHelpr.share.imageDic = resDic;
    ChatHelpr.share.config.left_box = @"talk_pop_l_nor";
    ChatHelpr.share.config.right_box = @"talk_pop_l_nor";
    ChatHelpr.share.config.bg_mask_left = @"talk_pop_l_nor";
    ChatHelpr.share.config.bg_mask_right = @"talk_pop_l_nor";
    // 添加语音输入功能
    [CTinputHelper.share.config addVoice];
    
}

- (BYPFUserInfoView *)personView{
    if (!_personView) {
        _personView = [[BYPFUserInfoView alloc] initWithFathureView:CURRENT_VC.view];
        _personView.model = self.model;
        if ((_identityType == BY_IDENTITY_TYPE_HOST ||
             _identityType == BY_IDENTITY_TYPE_CREATOR) &&
            (_model.status == BY_LIVE_STATUS_SOON ||
             _model.status == BY_LIVE_STATUS_LIVING)) {
                _personView.hasForbid = YES;
            }else {
                _personView.hasForbid = NO;
            }
    }
    return _personView;
}

- (BYNewMsgBottomView *)newMsgBottomView{
    if (!_newMsgBottomView) {
        _newMsgBottomView = [[BYNewMsgBottomView alloc] init];
        _newMsgBottomView.hidden = YES;
        [_newMsgBottomView addTarget:self action:@selector(scrollToMsgBottom) forControlEvents:UIControlEventTouchUpInside];
    }
    return _newMsgBottomView;
}


@end
