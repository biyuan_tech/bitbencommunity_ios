//
//  BYMicroLiveSubController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "HGPageViewController.h"
#import "BYMicroAudienceToolBar.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYMicroLiveSubController : HGPageViewController

/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
///** isHost */
//@property (nonatomic ,assign) BOOL isHost;
/** 直播记录id */
@property (nonatomic ,copy) NSString *live_record_id;
/** 身份 */
@property (nonatomic ,assign) BY_IDENTITY_TYPE identityType;
/** 观众toolbar */
@property (nonatomic ,strong ,readonly) BYMicroAudienceToolBar *audienceToolBar;


- (void)addMessageData:(NSArray *)msgs;

@end

NS_ASSUME_NONNULL_END
