//
//  BYNewMsgBottomView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger ,BYNEWMSG_BOTTOM_TYPE) {
    BYNEWMSG_BOTTOM_TYPE_NORMAL = 0,
    BYNEWMSG_BOTTOM_TYPE_ILIVE,
};

NS_ASSUME_NONNULL_BEGIN

@interface BYNewMsgBottomView : UIControl

/** 用于区分样式 */
@property (nonatomic ,assign) BYNEWMSG_BOTTOM_TYPE type;


// 设置新消息数
- (void)setNewMsgNum:(NSInteger)num;

@end

NS_ASSUME_NONNULL_END
