//
//  CDAskTableViewCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/21.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "CDAskTableViewCell.h"
#import "ChatMacros.h"
#import "CDChatListView.h"
#import "CDLabel.h"
#import "ChatHelpr.h"
#import "ChatListInfo.h"

@interface CDAskTableViewCell ()<CDLabelDelegate>

/**
 左侧文字label
 */
@property(nonatomic, strong) CDLabel *textContent_left;

/**
 右侧文字label
 */
@property(nonatomic, strong) CDLabel *textContent_right;

/** 提问提示文案 */
@property (nonatomic ,strong) UILabel *askTagLab_left;
@property (nonatomic ,strong) UILabel *askTagLab_right;


@end

@implementation CDAskTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    // 左侧气泡中添加label
    self.textContent_left = [[CDLabel alloc] init];
    self.textContent_left.frame = CGRectZero;
    self.textContent_left.labelDelegate = self;
    [self.bubbleImage_left addSubview:self.textContent_left];
    self.bubbleImage_left.clipsToBounds = NO;
    
    // 右侧气泡中添加label
    self.textContent_right = [[CDLabel alloc] init];
    self.textContent_right.frame = CGRectZero;
    self.textContent_right.labelDelegate = self;
    [self.bubbleImage_right addSubview:self.textContent_right];
    self.bubbleImage_right.clipsToBounds = NO;
    
    self.askTagLab_left = [[UILabel alloc] init];
    self.askTagLab_left.text = @"提问";
    self.askTagLab_left.textAlignment = NSTextAlignmentLeft;
    self.askTagLab_left.font = [UIFont systemFontOfSize:12];
    self.askTagLab_left.textColor = kColorRGBValue(0x6282fc);
    self.askTagLab_left.frame = CGRectZero;
    [self.msgContent_left addSubview:self.askTagLab_left];
    
    self.askTagLab_right = [[UILabel alloc] init];
    self.askTagLab_right.text = @"提问";
    self.askTagLab_right.textAlignment = NSTextAlignmentRight;
    self.askTagLab_right.font = [UIFont systemFontOfSize:12];
    self.askTagLab_right.textColor = kColorRGBValue(0x6282fc);
    self.askTagLab_right.frame = CGRectZero;
    [self.msgContent_right addSubview:self.askTagLab_right];
    
    return self;
}

#pragma mark CDLabelDelegate

-(void)labelDidSelectText:(CTLinkData *)link{
    ChatListInfo *info = [ChatListInfo new];
    info.eventType = ChatClickEventTypeTEXT;
    info.msgText = self.msgModal.msg;
    info.containerView = self;
    info.clickedText = link.title;
    info.clickedTextContent = link.url;
    info.range = link.range;
    info.msgModel = self.msgModal;
    if ([self.tableView.msgDelegate respondsToSelector:@selector(chatlistClickMsgEvent:)]) {
        [self.tableView.msgDelegate chatlistClickMsgEvent:info];
    }else{
#ifdef DEBUG
        NSLog(@"[CDChatList] chatlistClickMsgEvent未实现，不能响应点击事件");
#endif
    }
}


#pragma mark MessageCellDelegate

- (void)configCellByData:(CDChatMessage)data table:(CDChatListView *)table{
    [super configCellByData:data table:table];
    
    if (data.isLeft) {
        // 左侧
        //     设置消息内容, 并调整UI
        [self configText_Left:data];
    } else {
        // 右侧
        //     设置消息内容, 并调整UI
        [self configText_Right:data];
    }
}

-(void)configText_Left:(CDChatMessage)data{
    self.bubbleImage_left.image = [[UIImage imageNamed:@"talk_ask_l_nor"] resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6)];
//    _bubbleImage_left = [[UIImageView alloc] initWithImage:left_box];

    // 给label复制文字内容
    self.textContent_left.data = data.textlayout;
    CGRect textRect = self.textContent_left.frame;
    textRect.origin = CGPointMake(data.chatConfig.bubbleRoundAnglehorizInset + data.chatConfig.bubbleShareAngleWidth, data.chatConfig.bubbleRoundAnglehorizInset);
    textRect.size = data.textlayout.contents.size;
    self.textContent_left.frame = textRect;
    
    self.askTagLab_right.hidden = YES;
    self.askTagLab_left.hidden = NO;
    CGFloat originX = CGRectGetMinX(self.userName_left.frame) + stringGetWidth(data.userName, 12) + 8;
    self.askTagLab_left.frame = CGRectMake(originX, CGRectGetMinY(self.userName_left.frame), 26, self.askTagLab_left.font.pointSize);
}

-(void)configText_Right:(CDChatMessage)data{
    self.bubbleImage_right.image = [[UIImage imageNamed:@"talk_ask_l_nor"] resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6)];
    // 给label复制文字内容
    self.textContent_right.data = data.textlayout;
    CGRect textRect = self.textContent_right.frame;
    textRect.origin = CGPointMake(data.chatConfig.bubbleRoundAnglehorizInset, data.chatConfig.bubbleRoundAnglehorizInset);
    textRect.size = data.textlayout.contents.size;
    self.textContent_right.frame = textRect;
    
    self.askTagLab_right.hidden = NO;
    self.askTagLab_left.hidden = YES;
    CGFloat originX = CGRectGetMaxX(self.userName_right.frame) - stringGetWidth(data.userName, 12) - 8 - 26;
    self.askTagLab_right.frame = CGRectMake(originX, CGRectGetMinY(self.userName_right.frame), 26, self.askTagLab_right.font.pointSize);
}
@end
