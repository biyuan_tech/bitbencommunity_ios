//
//  BYNewMsgBottomView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYNewMsgBottomView.h"

@interface BYNewMsgBottomView ()

/** bottomImg */
@property (nonatomic ,strong) UIImageView *bottomImgView;
/** title */
@property (nonatomic ,strong) UILabel *titleLab;


@end

@implementation BYNewMsgBottomView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
        self.layer.cornerRadius = 16.0f;
        self.type = BYNEWMSG_BOTTOM_TYPE_NORMAL;
        [self setContentView];
    }
    return self;
}

- (void)setNewMsgNum:(NSInteger)num{
    NSString *string = [NSString stringWithFormat:@"%@条新消息",stringFormatInteger(num)];
    self.titleLab.text = string;
    CGFloat titleLabW = stringGetWidth(string, self.titleLab.font.pointSize);
    CGFloat detal = _type == BYNEWMSG_BOTTOM_TYPE_NORMAL ? 49 : 33;
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(titleLabW + detal);
    }];
    
    [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(titleLabW);
    }];
}

- (void)setType:(BYNEWMSG_BOTTOM_TYPE)type{
    _type = type;
    if (type == BYNEWMSG_BOTTOM_TYPE_ILIVE) {
        self.layer.cornerRadius = 14.0f;
        self.backgroundColor = kColorRGB(249, 180, 46, 0.95);
        [self.bottomImgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
        }];
        
        self.titleLab.font = [UIFont systemFontOfSize:13];
        [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(self.titleLab.font.pointSize);
            make.left.mas_equalTo(self.bottomImgView.mas_right).offset(4);
        }];
    }
}

- (void)setContentView{
    [self addSubview:self.bottomImgView];
    [self addSubview:self.titleLab];
    
    [self.bottomImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(9);
        make.height.mas_equalTo(10);
        make.left.mas_equalTo(17);
    }];
    
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(self.titleLab.font.pointSize);
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(self.bottomImgView.mas_right).offset(6);
    }];
}

- (UIImageView *)bottomImgView{
    if (!_bottomImgView) {
        _bottomImgView = [[UIImageView alloc] init];
        [_bottomImgView by_setImageName:@"common_newmsg_up"];
    }
    return _bottomImgView;
}

- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:14];
        _titleLab.textColor = [UIColor whiteColor];
    }
    return _titleLab;
}

@end
