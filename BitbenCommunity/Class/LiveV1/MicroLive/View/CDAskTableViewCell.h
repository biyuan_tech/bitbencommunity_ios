//
//  CDAskTableViewCell.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/21.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "CDBaseMsgCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CDAskTableViewCell : CDBaseMsgCell<MessageCellProtocal>

@end

NS_ASSUME_NONNULL_END
