//
//  BYBarrageCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYBarrageCell.h"
#import <YYLabel.h>
#import <YYText/NSAttributedString+YYText.h>

#import "BYBarrageModel.h"

@interface BYBarrageCell ()

/** maskView */
@property (nonatomic ,strong) UIView *maskView_left;
@property (nonatomic ,strong) UIView *maskView_right;

/** titleLab */
@property (nonatomic ,strong) YYLabel *titleLab_left;
@property (nonatomic ,strong) YYLabel *titleLab_right;

/** logo */
@property (nonatomic ,strong) PDImageView *userlogoImgView_left;
@property (nonatomic ,strong) PDImageView *userlogoImgView_right;


@end

@implementation BYBarrageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYBarrageModel *model = object[indexPath.row];
    if (model.isLeft) {
        self.titleLab_right.hidden          = YES;
        self.userlogoImgView_right.hidden   = YES;
        self.maskView_right.hidden          = YES;
        self.titleLab_left.hidden          = NO;
        self.userlogoImgView_left.hidden   = NO;
        self.maskView_left.hidden          = NO;
        self.titleLab_left.attributedText = model.attributedString;
        [self.userlogoImgView_left uploadHDImageWithURL:model.msg_headImg callback:nil];
        self.userlogoImgView_left.style = model.cert_badge;
        
        [self.titleLab_left mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(ceil(model.titleSize.width));
            make.height.mas_equalTo(ceil(model.titleSize.height));
        }];
    }else{
        self.titleLab_left.hidden          = YES;
        self.userlogoImgView_left.hidden   = YES;
        self.maskView_left.hidden          = YES;
        self.titleLab_right.hidden          = NO;
        self.userlogoImgView_right.hidden   = NO;
        self.maskView_right.hidden          = NO;
        
        self.titleLab_right.attributedText = model.attributedString;
        [self.userlogoImgView_right uploadHDImageWithURL:model.msg_headImg callback:nil];
        self.userlogoImgView_right.style = model.cert_badge;
        
        [self.titleLab_right mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(ceil(model.titleSize.width));
            make.height.mas_equalTo(ceil(model.titleSize.height));
        }];
    }
   
}

- (void)setContentView{
    [self configLeft];
    [self configRight];
}

- (void)configLeft{
    self.titleLab_left = [[YYLabel alloc] init];
    self.titleLab_left.font = [UIFont systemFontOfSize:13];
    self.titleLab_left.textColor = [UIColor whiteColor];
    self.titleLab_left.textAlignment = NSTextAlignmentLeft;
    self.titleLab_left.numberOfLines = 0;
    [self.contentView addSubview:self.titleLab_left];
    [self.titleLab_left mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(50);
        make.top.mas_equalTo(8);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    self.maskView_left = [UIView by_init];
    [self.maskView_left setBackgroundColor:kColorRGB(0, 0, 0, 1.0)];
    self.maskView_left.alpha = 0.35;
    self.maskView_left.layer.cornerRadius = 4.f;
    [self.contentView addSubview:self.maskView_left];
    [self.contentView insertSubview:self.maskView_left belowSubview:self.titleLab_left];
    [self.maskView_left mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(self.titleLab_left).mas_offset(9);
        make.bottom.mas_equalTo(self.titleLab_left).mas_offset(8);
    }];
    
    self.userlogoImgView_left = [[PDImageView alloc] init];
    self.userlogoImgView_left.contentMode = UIViewContentModeScaleAspectFill;
    self.userlogoImgView_left.layer.cornerRadius = 10;
    self.userlogoImgView_left.clipsToBounds = YES;
    [self.contentView addSubview:self.userlogoImgView_left];
    [self.userlogoImgView_left mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(5);
        make.left.mas_equalTo(23);
        make.width.height.mas_equalTo(20);
    }];
}

- (void)configRight{
    self.titleLab_right = [[YYLabel alloc] init];
    self.titleLab_right.font = [UIFont systemFontOfSize:13];
    self.titleLab_right.textColor = [UIColor whiteColor];
    self.titleLab_right.textAlignment = NSTextAlignmentRight;
    self.titleLab_right.numberOfLines = 0;
    [self.contentView addSubview:self.titleLab_right];
    [self.titleLab_right mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-50);
        make.top.mas_equalTo(8);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    self.maskView_right = [UIView by_init];
    [self.maskView_right setBackgroundColor:kColorRGB(0, 0, 0, 1.0)];
    self.maskView_right.alpha = 0.35;
    self.maskView_right.layer.cornerRadius = 4.f;
    [self.contentView addSubview:self.maskView_right];
    [self.contentView insertSubview:self.maskView_right belowSubview:self.titleLab_right];
    [self.maskView_right mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(self.titleLab_right).mas_offset(-9);
        make.bottom.mas_equalTo(self.titleLab_right).mas_offset(8);
    }];
    
    self.userlogoImgView_right = [[PDImageView alloc] init];
    self.userlogoImgView_right.contentMode = UIViewContentModeScaleAspectFill;
    self.userlogoImgView_right.layer.cornerRadius = 10;
    self.userlogoImgView_right.clipsToBounds = YES;
    [self.contentView addSubview:self.userlogoImgView_right];
    [self.userlogoImgView_right mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(5);
        make.right.mas_equalTo(-23);
        make.width.height.mas_equalTo(20);
    }];
}

@end
