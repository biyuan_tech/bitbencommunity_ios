//
//  BYILiveController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYILiveController : BYCommonViewController

/** id */
@property (nonatomic ,copy) NSString *live_record_id;
/** isHost */
@property (nonatomic ,assign) BOOL isHost;

@end

NS_ASSUME_NONNULL_END
