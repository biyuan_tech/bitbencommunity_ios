//
//  BYILiveScrollView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class BYILiveChatController;
@interface BYILiveScrollView : UIScrollView

/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 直播配置 */
@property (nonatomic ,strong) BYLiveConfig *config;
/** 开始直播回调 */
@property (nonatomic ,copy) void (^didBeginLiveHandle)(void);
/** 操作控制台 */
@property (nonatomic ,strong ,readonly) BYILiveChatController *chatController;


- (void)addMessageData:(NSArray *)msgs;
@end

NS_ASSUME_NONNULL_END
