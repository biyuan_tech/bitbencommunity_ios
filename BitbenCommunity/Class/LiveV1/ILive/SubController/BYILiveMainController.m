//
//  BYILiveMainController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveMainController.h"
#import "BYIMDefine.h"

#import "TRTCVideoView.h"
#import "TRTCVideoViewLayout.h"

@interface BYILiveMainController ()<TRTCCloudDelegate,TRTCVideoViewDelegate>


/** TRTC SDK 实例对象 */
@property (nonatomic, retain) TRTCCloud *trtc;
/** 本地画面的View */
@property (nonatomic, strong) TRTCVideoView *localView;
@property (nonatomic ,strong) UIView *holderView;
@property (nonatomic ,strong) TRTCVideoViewLayout *layoutEngine;

@end

@implementation BYILiveMainController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)addLocalPreView{
    // 本地预览view
    if (_config.isHost) {
        _localView = [TRTCVideoView newVideoViewWithType:VideoViewType_Local userId:_model.user_id];
        _localView.delegate = self;
        [_localView setBackgroundColor:[UIColor clearColor]];
    }
    
    // 默认大画面未主播端
    _mainPreviewId = _model.user_id;
    
    _holderView = [[UIView alloc] initWithFrame:self.view.bounds];
    [_holderView setBackgroundColor:[UIColor clearColor]];
    [self.view insertSubview:_holderView atIndex:0];
    
    _layoutEngine = [[TRTCVideoViewLayout alloc] init];
    _layoutEngine.view = _holderView;
    
}


- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    [self addLocalPreView];
    [self addNSNotification];
    TRTCParams *params = [[TRTCParams alloc] init];
    params.sdkAppId = kTRTCSDKAppId;
    params.userId = ACCOUNT_ID;
    params.userSig = self.model.userSig.length ?  self.model.userSig : [AccountModel sharedAccountModel].userSig;
    NSMutableString *string = [NSMutableString stringWithString:self.model.live_record_id];
    params.roomId = [[string substringWithRange:NSMakeRange(string.length - 8, 8)] integerValue];
    params.role = _config.isHost ? TRTCRoleAnchor : TRTCRoleAudience;
    self.param = params;
}

- (void)setRoomStatus:(TRTCStatus)roomStatus{
    _roomStatus = roomStatus;
    switch (_roomStatus) {
        case TRTC_IDLE:
            [self exitRoomSuc];
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
            break;
        case TRTC_ENTERED:
            [self enterRoomSuc];
            [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
            break;
        default:
            break;
    }
}

- (void)addMessageData:(NSArray *)msgs{
    if (!msgs.count) return;
    NSDictionary *dic = msgs[0];
    if (dic) {
        if ([dic[@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
            NSDictionary *customData;
            if ([dic[@"content"] isKindOfClass:[NSString class]]) {
                // 现返回的为纯json字符串，需要转两次
                NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            }
            else{
                customData = dic[@"content"];
            }
            BY_GUEST_OPERA_TYPE opearType = [customData[kBYCustomMessageType] integerValue];
            
            if (opearType == BY_GUEST_OPERA_TYPE_UP_VIDEO) { // 主播接收到上麦消息
                if (_config.isHost) {
                    // 接收上麦请求添加记录
                    [[BYIMManager sharedInstance] receiveUpToVideoRequestWithAudienceData:dic];
                }
            }else if (opearType == BY_GUEST_OPERA_TYPE_AGREE_UP_VIDEO) { // 同意上麦消息
                if (!_config.isHost) {
                    if ([ACCOUNT_ID isEqualToString:customData[@"user_id"]]) {
                        [self receiveAgreeUpToVideo:YES];
                    }
                }
            }else if (opearType == BY_GUEST_OPERA_TYPE_DOWN_VIDEO) { // 请求下麦消息
                if (!_config.isHost) {
                    if ([ACCOUNT_ID isEqualToString:customData[@"user_id"]]) {
                        [self receiveAgreeUpToVideo:NO];
                    }
                }
            }
            return ;
        }
    }
}

- (void)receiveAgreeUpToVideo:(BOOL)isUpToVideo{
    if (isUpToVideo) { // 上麦
        _localView = [TRTCVideoView newVideoViewWithType:VideoViewType_Remote userId:_param.userId];
        _localView.delegate = self;
        [_trtc switchRole:TRTCRoleAnchor];
        _param.role = TRTCRoleAnchor;
        [self startPreview];
    }else{             // 下麦
        _localView = nil;
        [_trtc switchRole:TRTCRoleAudience];
        _param.role = TRTCRoleAudience;
        [self stopPreview];
    }
}

// 校验是否主播离开房间
//- (void)verifyHostIsExit:(NSString *)user_id{
//    if ([user_id isEqualToString:self.model.user_id]) {
//        [self stopLocalPreview];
//    }
//}


// 成功进入音视频房间
- (void)enterRoomSuc{
    // 设置美颜
    CGFloat beautyValue = [[[NSUserDefaults standardUserDefaults] objectForKey:kBeautyValue] floatValue];
    CGFloat whiteValue = [[[NSUserDefaults standardUserDefaults] objectForKey:kWhiteValue] floatValue];
    [BYLiveConfig setBeautyLevel:beautyValue whiteLevel:whiteValue];
    [self startPreview];
    if (self.enterRoomHandle) {
        self.enterRoomHandle();
    }
}

// 成功退出音视频房间
- (void)exitRoomSuc{
    if (self.exitRoomHandle) {
        self.exitRoomHandle();
    }
}

// 退出房间
- (void)exitRoom:(ExitRoomHandle)cb{
    _exitRoomHandle = cb;
    [_trtc exitRoom];
}

// 进入房间
- (void)enterTRTCRoom:(EnterRoomHandle)cb{
    _enterRoomHandle = cb;
    [self enterTRTCRoom];
}


// 混流
- (void)updateCloudMixtureParams{
    if (!_config.isHost) return;
    
    int videoWidth  = 720;
    int videoHeight = 1280;
    
    // 小画面宽高
    int subWidth  = 105;
    int subHeight = 165;
    
    int offsetX = 15;
    int offsetY = 110;
    
    int bitrate = 200;
    
    videoWidth  = 720;
    videoHeight = 1280;
//    subWidth    = 138;
//    subHeight   = 240;
    subWidth    = 207;
    subHeight   = 360;
    bitrate     = 1500;
    
    TRTCTranscodingConfig* config = [TRTCTranscodingConfig new];
    config.appId = kTRTCAppId;
    config.bizId = kTRTCBizid;
    config.videoWidth = videoWidth;
    config.videoHeight = videoHeight;
    config.videoGOP = 2;
    config.videoFramerate = 15;
    config.videoBitrate = bitrate;
    config.audioSampleRate = 48000;
    config.audioBitrate = 64;
    config.audioChannels = 2;
    
    // 设置混流后主播的画面位置
    TRTCMixUser* broadCaster = [TRTCMixUser new];
    broadCaster.userId = _model.user_id; // 以主播uid为broadcaster为例
    broadCaster.zOrder = 0;
    broadCaster.rect = CGRectMake(0, 0, videoWidth, videoHeight);
    broadCaster.roomID = nil;
    broadCaster.streamType = TRTCVideoStreamTypeBig;
    
    NSMutableArray* mixUsers = [NSMutableArray new];
    [mixUsers addObject:broadCaster];
    
    // 设置混流后各个小画面的位置
    int index = 0;
    NSMutableDictionary *upToVideoDic = [NSMutableDictionary dictionary];
    [upToVideoDic setValuesForKeysWithDictionary:_remoteViewDic];
    [upToVideoDic removeObjectForKey:_model.user_id];
    for (NSString* userId in upToVideoDic.allKeys) {
        TRTCMixUser* audience = [TRTCMixUser new];
        audience.userId = userId;
        audience.zOrder = index + 1;
        audience.streamType = TRTCVideoStreamTypeSmall;
        //辅流判断：辅流的Id为原userId + "-sub"
        if ([userId hasSuffix:@"-sub"]) {
            NSArray* spritStrs = [userId componentsSeparatedByString:@"-"];
            if (spritStrs.count < 2)
                continue;
            NSString* realUserId = spritStrs[0];
            if (![_remoteViewDic.allKeys containsObject:realUserId])
                return;
            audience.userId = realUserId;
            audience.streamType = TRTCVideoStreamTypeSub;
        }
        if (index < 3) {
            // 前三个小画面靠右从下往上铺
            audience.rect = CGRectMake(videoWidth - offsetX - subWidth, videoHeight - offsetY - index * subHeight - subHeight, subWidth, subHeight);
        } else if (index < 6) {
            // 后三个小画面靠左从下往上铺
            audience.rect = CGRectMake(offsetX, videoHeight - offsetY - (index - 3) * subHeight - subHeight, subWidth, subHeight);
        } else {
            // 最多只叠加六个小画面
        }
        
        [mixUsers addObject:audience];
        ++index;
    }
    config.mixUsers = mixUsers;
    [_trtc setMixTranscodingConfig:config];
}

// 关闭本地推流
- (void)stopLocalPreview{
    [self stopPreview];
}

// 开始进入直播
- (void)enterTRTCRoom{
    // 大画面的编码器参数设置
    // 设置视频编码参数，包括分辨率、帧率、码率等等，这些编码参数来自于 TRTCSettingViewController 的设置
    // 注意（1）：不要在码率很低的情况下设置很高的分辨率，会出现较大的马赛克
    // 注意（2）：不要设置超过25FPS以上的帧率，因为电影才使用24FPS，我们一般推荐15FPS，这样能将更多的码率分配给画质
    TRTCVideoEncParam* encParam = [TRTCVideoEncParam new];
    encParam.videoResolution = VIDEO_RESOLUTION_720_1280;
    encParam.videoBitrate = 1150;
    encParam.videoFps = 15;
    encParam.resMode = TRTCVideoResolutionModePortrait;
    
    TRTCNetworkQosParam * qosParam = [TRTCNetworkQosParam new];
    qosParam.preference = TRTCVideoQosPreferenceClear;
    qosParam.controlMode = TRTCQosControlModeServer;
    [self.trtc setNetworkQosParam:qosParam];
    
    //小画面的编码器参数设置
    //TRTC SDK 支持大小两路画面的同时编码和传输，这样网速不理想的用户可以选择观看小画面
    //注意：iPhone & Android 不要开启大小双路画面，非常浪费流量，大小路画面适合 Windows 和 MAC 这样的有线网络环境
    TRTCVideoEncParam* smallVideoConfig = [TRTCVideoEncParam new];
    smallVideoConfig.videoResolution = TRTCVideoResolution_160_90;
    smallVideoConfig.videoFps = 15;
    smallVideoConfig.videoBitrate = 100;
    
    [_trtc setLocalViewFillMode:TRTCVideoFillMode_Fill];
    [_trtc setGSensorMode:TRTCGSensorMode_Disable];
    
    [_trtc setPriorRemoteVideoStreamType:TRTCVideoStreamTypeBig];
    [_trtc setAudioRoute:TRTCAudioModeSpeakerphone];
    [_trtc enableAudioVolumeEvaluation:300];
    
    [_trtc setVideoEncoderParam:encParam];
    [_trtc enableEncSmallVideoStream:NO withQuality:smallVideoConfig];
    // 进入音频房间
    [_trtc enterRoom:_param appScene:TRTCAppSceneLIVE];
}

// 刷新视图布局
- (void)relayout{
    NSMutableArray *views = @[].mutableCopy;
    TRTCVideoView *playerView = [self.remoteViewDic objectForKey:_mainPreviewId];
    if (playerView) { // 为主播端画面
        [views addObject:playerView];
    }
    
    for (id userID in _remoteViewDic) {
        TRTCVideoView *playerView = [_remoteViewDic objectForKey:userID];
        //        if ([_mainPreviewId isEqualToString:userID]) {
        //            _localView = playerView;
        //        }
        if (![userID isEqualToString:_mainPreviewId]) {
            playerView.enableMove = NO;
            [views addObject:playerView];
        }
    }
    
    [_layoutEngine relayout:views];
}

#pragma mark - startPreview
- (void)startPreview
{
    //开摄像头
    //视频通话默认开摄像头。直播模式主播才开摄像头
    if (_param.role == TRTCRoleAnchor) {
        // 开启屏幕分享
        [_trtc startLocalPreview:YES view:_localView];
        // 开启音频上行
        [_trtc startLocalAudio];
        [self.remoteViewDic setObject:_localView forKey:_param.userId];
        [BYIMManager sharedInstance].memberIds = self.remoteViewDic.allKeys;
    }
    [self relayout];
}

- (void)stopPreview
{
    // 关闭屏幕分享
    [_trtc stopLocalPreview];
    // 关闭音频上行
    [_trtc stopLocalAudio];
    [_remoteViewDic removeObjectForKey:_param.userId];
    [BYIMManager sharedInstance].memberIds = self.remoteViewDic.allKeys;
    [self relayout];
}

#pragma mark - Notification

- (void)onAppWillResignActive:(NSNotification *)notification {
    if (_trtc != nil) {
        ;
    }
}

- (void)onAppDidBecomeActive:(NSNotification *)notification {
    if (_trtc != nil) {
        ;
    }
}

- (void)onAppDidEnterBackGround:(NSNotification *)notification {
    if (_trtc != nil) {
        ;
    }
}

- (void)onAppWillEnterForeground:(NSNotification *)notification {
    if (_trtc != nil) {
        ;
    }
}

- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidEnterBackGround:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (TRTCCloud *)trtc{
    if (!_trtc) {
        _trtc = [TRTCCloud sharedInstance];
        _trtc.delegate = self;
    }
    return _trtc;
}

- (BYLiveConfig *)config{
    if (!_config) {
        _config = [[BYLiveConfig alloc] init];
    }
    return _config;
}

- (NSMutableDictionary *)remoteViewDic{
    if (!_remoteViewDic) {
        _remoteViewDic = [[NSMutableDictionary alloc] init];
    }
    return _remoteViewDic;
}
@end
