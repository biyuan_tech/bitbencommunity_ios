//
//  BYILiveMainController+TRTCCloudDelegate.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveMainController+TRTCCloudDelegate.h"
#import "TRTCVideoView.h"
#import "BYIMManager.h"

@implementation BYILiveMainController (TRTCCloudDelegate)


#pragma mark - TRtcEngineDelegate

/**
 * WARNING 大多是一些可以忽略的事件通知，SDK内部会启动一定的补救机制
 */
- (void)onWarning:(TXLiteAVWarning)warningCode warningMsg:(NSString *)warningMsg {
    
}

/**
 * WARNING 大多是不可恢复的错误，需要通过 UI 提示用户
 */
- (void)onError:(TXLiteAVError)errCode errMsg:(NSString *)errMsg extInfo:(nullable NSDictionary *)extInfo {
    
    
    if (errCode == ERR_ROOM_ENTER_FAIL) {
        showToastView([NSString stringWithFormat:@"无法进入音视频房间[%@]", errMsg], CURRENT_VC.view);
//        [self quitRoom];
        return;
    }
    
    
    NSString *msg = [NSString stringWithFormat:@"didOccurError: %@[%d]", errMsg, errCode];
    PDLog(@"%@",msg);
    showToastView(msg, CURRENT_VC.view);
}


- (void)onEnterRoom:(NSInteger)elapsed {
    NSString *msg = [NSString stringWithFormat:@"[%@]进房成功[%i]: elapsed[%ld]", self.param.userId, self.param.roomId, (long)elapsed];
    showDebugToastView(msg, CURRENT_VC.view);
    //    [self showToastView:msg];
    PDLog(@"%@",msg);
    [self setRoomStatus:TRTC_ENTERED];
}


- (void)onExitRoom:(NSInteger)reason {
    NSString *msg = [NSString stringWithFormat:@"离开房间[%i]: reason[%ld]", self.param.roomId, (long)reason];
    //    [self showToastView:msg];
    showDebugToastView(msg, CURRENT_VC.view);
    PDLog(@"%@",msg);
    //    if (reason == 0) {
    [self setRoomStatus:TRTC_IDLE];
    //    }
}

/**
 * 有新的用户加入了当前视频房间
 */
- (void)onUserEnter:(NSString *)userId {
    // 创建一个新的 View 用来显示新的一路画面
    
    VideoViewType type = [userId isEqualToString:self.model.user_id] ? VideoViewType_Local : VideoViewType_Remote;
    TRTCVideoView *remoteView = [TRTCVideoView newVideoViewWithType:type userId:userId];
    //    if (![TRTCMoreViewController isAudioVolumeEnable]) {
    [remoteView showAudioVolume:NO];
    //    }
    remoteView.delegate = self;
    [remoteView setBackgroundColor:UIColorFromRGB(0x262626)];
    //    [self.view addSubview:remoteView];
    [self.remoteViewDic setObject:remoteView forKey:userId];
    [BYIMManager sharedInstance].memberIds = self.remoteViewDic.allKeys;
    // 将新进来的成员设置成大画面
    //    _mainPreviewId = userId;
    
    if ([userId isEqualToString:self.model.user_id]) { // 当前是主播离开房间
        if (self.hostJoinRoomHandle) {
            self.hostJoinRoomHandle();
        }
    }
    
    [self relayout];
    [self updateCloudMixtureParams];
}

/**
 * 有用户离开了当前视频房间
 */
- (void)onUserExit:(NSString *)userId reason:(NSInteger)reason {
    // 更新UI
    UIView *playerView = [self.remoteViewDic objectForKey:userId];
    [playerView removeFromSuperview];
    [self.remoteViewDic removeObjectForKey:userId];
    
    if ([userId isEqualToString:self.param.userId]) {
        [self stopLocalPreview];
    }
    
    if ([userId isEqualToString:self.model.user_id]) { // 当前是主播离开房间
        if (self.hostLeavingRoomInLivingHandle) {
            self.hostLeavingRoomInLivingHandle();
        }
    }
    
    NSString* subViewId = [NSString stringWithFormat:@"%@-sub", userId];
    UIView *subStreamPlayerView = [self.remoteViewDic objectForKey:subViewId];
    [subStreamPlayerView removeFromSuperview];
    [self.remoteViewDic removeObjectForKey:subViewId];
    
    [BYIMManager sharedInstance].memberIds = self.remoteViewDic.allKeys;
    [[BYIMManager sharedInstance] removeUpToVideoMember:userId];
    
    // 如果该成员是大画面，则当其离开后，大画面设置为本地推流画面
    if ([userId isEqual:_mainPreviewId] || [subViewId isEqualToString:_mainPreviewId]) {
        _mainPreviewId = self.model.user_id;
    }
    
    [self relayout];
    [self updateCloudMixtureParams];
    
}

- (void)onUserAudioAvailable:(NSString *)userId available:(BOOL)available
{
    TRTCVideoView *playerView = [self.remoteViewDic objectForKey:userId];
    if (!available) {
        [playerView setAudioVolumeRadio:0.f];
    }
    NSLog(@"onUserAudioAvailable:userId:%@ alailable:%u", userId, available);
}


- (void)onUserVideoAvailable:(NSString *)userId available:(BOOL)available
{
    if (userId != nil) {
        TRTCVideoView* remoteView = [self.remoteViewDic objectForKey:userId];
        if (available) {
            // 启动远程画面的解码和显示逻辑，FillMode 可以设置是否显示黑边
            [[TRTCCloud sharedInstance] startRemoteView:userId view:remoteView];
            [[TRTCCloud sharedInstance] setRemoteViewFillMode:userId mode:TRTCVideoFillMode_Fill];
            
        }
        else {
            [[TRTCCloud sharedInstance] stopRemoteView:userId];
        }
        
        //        [remoteView showVideoCloseTip:!available];
    }
    
    NSLog(@"onUserVideoAvailable:userId:%@ alailable:%u", userId, available);
    
}

- (void)onUserSubStreamAvailable:(NSString *)userId available:(BOOL)available
{
    NSLog(@"onUserSubStreamAvailable:userId:%@ alailable:%u", userId, available);
    NSString* viewId = [NSString stringWithFormat:@"%@-sub", userId];
    if (available) {
        TRTCVideoView *remoteView = [TRTCVideoView newVideoViewWithType:VideoViewType_Remote userId:userId];
        //        if (![TRTCMoreViewController isAudioVolumeEnable]) {
        [remoteView showAudioVolume:NO];
        //        }
        remoteView.delegate = self;
        [remoteView setBackgroundColor:UIColorFromRGB(0x262626)];
        [self.view addSubview:remoteView];
        [self.remoteViewDic setObject:remoteView forKey:viewId];
        [BYIMManager sharedInstance].memberIds = self.remoteViewDic.allKeys;

        [[TRTCCloud sharedInstance] startRemoteSubStreamView:userId view:remoteView];
        [[TRTCCloud sharedInstance] setRemoteSubStreamViewFillMode:userId mode:TRTCVideoFillMode_Fill];
    }
    else {
        UIView *playerView = [self.remoteViewDic objectForKey:viewId];
        [playerView removeFromSuperview];
        [self.remoteViewDic removeObjectForKey:viewId];
        [BYIMManager sharedInstance].memberIds = self.remoteViewDic.allKeys;

        [[TRTCCloud sharedInstance] stopRemoteSubStreamView:userId];
        [[TRTCCloud sharedInstance] setRemoteSubStreamViewFillMode:userId mode:TRTCVideoFillMode_Fill];
        //        if ([viewId isEqual:_mainPreviewId]) {
        //            _mainPreviewId = self.param.userId;
        //        }
    }
    [self relayout];
}

- (void)onAudioRouteChanged:(TRTCAudioRoute)route fromRoute:(TRTCAudioRoute)fromRoute {
    NSLog(@"TRTC onAudioRouteChanged %ld -> %ld", (long)fromRoute, route);
}


- (void)onSwitchRole:(TXLiteAVError)errCode errMsg:(NSString *)errMsg
{
    NSLog(@"onSwitchRole errCode:%d, errMsg:%@", errCode, errMsg);
}

#pragma mark - TRTCVideoViewDelegate
- (void)onMuteVideoBtnClick:(TRTCVideoView*)view stateChanged:(BOOL)stateChanged{
    
}

- (void)onViewTap:(TRTCVideoView *)view touchCount:(NSInteger)touchCount{
    if (view.userId == _mainPreviewId) { // 当前点击的为大画面则return
        return;
    }
    _mainPreviewId = view.userId;
    [self relayout];
}

@end
