//
//  BYILiveMainController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TXLiteAVSDK_Professional/TXLiteAVSDK.h>
#import "BYILiveRoomSubControllerV1.h"
#import "BYLiveConfig.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^ExitRoomHandle)(void);
typedef void (^EnterRoomHandle)(void);
@interface BYILiveMainController : UIViewController
{
    NSString *_mainPreviewId;
}

/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 直播类型状态 */
@property (nonatomic ,assign) TRTCStatus roomStatus;
/** 参数 */
@property (nonatomic ,strong) TRTCParams *param;
/** 记录所有用户画面 */
@property (nonatomic ,strong) NSMutableDictionary *remoteViewDic;
/** 直播配置 */
@property (nonatomic ,strong) BYLiveConfig *config;
/** 退出房间回到 */
@property (nonatomic ,copy) ExitRoomHandle exitRoomHandle;
/** 加入房间成功回调 */
@property (nonatomic ,copy) EnterRoomHandle enterRoomHandle;
/** 主播中途离开房间 */
@property (nonatomic ,copy) void (^hostLeavingRoomInLivingHandle)(void);
/** 主播进入房间 */
@property (nonatomic ,copy) void (^hostJoinRoomHandle)(void);


- (void)addMessageData:(NSArray *)msgs;

// 退出音视频房间
- (void)exitRoom:(ExitRoomHandle)cb;
// 混流
- (void)updateCloudMixtureParams;
// 关闭本地推流
- (void)stopLocalPreview;
//开始进入直播
- (void)enterTRTCRoom:(EnterRoomHandle)cb;
// 刷新视图布局
- (void)relayout;

// 校验是否主播离开房间
//- (void)verifyHostIsExit:(NSString *)user_id;

@end

NS_ASSUME_NONNULL_END
