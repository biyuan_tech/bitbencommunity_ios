//
//  BYILiveVodController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveVodController.h"
#import "BYILiveChatController.h"

@interface BYILiveVodController ()<BYILiveChatControllerDelegate>


/** 播放器View的父视图*/
@property (nonatomic) UIView *playerFatherView;
/** 播放器 */
@property (nonatomic ,strong) BYCommonPlayerView *playerView;


@end

@implementation BYILiveVodController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_playerView resetPlayer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    if (!model.video_url.length) return;
    [self setContentView];
}

#pragma mark - BYILiveChatControllerDelegate
/// 代理设置播放器的播放和暂停状态
- (void)controlViewPlayingStatus:(BYILiveChatController *)controlView isPlaying:(BOOL)isPlaying{
    if (isPlaying) {
        [self.playerView resume];
    }else{
        [self.playerView pause];
    }
}
// 滑块滑动
- (void)controlViewSliderValueChange:(BYILiveChatController *)controlView slider:(UISlider *)slider{
    [self.playerView.controlView.delegate controlViewPreview:self.playerView.controlView where:slider.value];

}
// 滑动停止
- (void)controlViewSliderTouchEnded:(BYILiveChatController *)controlView slider:(UISlider *)slider{
    [self.playerView.controlView.delegate controlViewSeek:self.playerView.controlView where:slider.value];
}

#pragma mark - configUI

- (void)setContentView{
    [self.view addSubview:self.playerFatherView];
    self.playerView.fatherView = self.playerFatherView;
    [_playerView playVideoWithUrl:_model.video_url];
    
    [self.playerFatherView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (BYLiveConfig *)config{
    if (!_config) {
        _config = [[BYLiveConfig alloc] init];
    }
    return _config;
}

- (UIView *)playerFatherView{
    if (!_playerFatherView){
        _playerFatherView = [[UIView alloc] init];
    }
    return _playerFatherView;
}

- (BYCommonPlayerView *)playerView {
    if (!_playerView) {
        _playerView = [[BYCommonPlayerView alloc] init];
        _playerView.enableFloatWindow = NO;
        _playerView.playerConfig.type = PLAYER_TYPE_ILIVE_END;
        _playerView.playerConfig.renderMode =  RENDER_MODE_FILL_EDGE;
        // 设置代理
        _playerView.delegate = self;
    }
    return _playerView;
}

@end
