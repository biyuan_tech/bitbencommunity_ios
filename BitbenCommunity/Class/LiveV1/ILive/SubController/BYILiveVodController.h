//
//  BYILiveVodController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYCommonPlayerView.h"

NS_ASSUME_NONNULL_BEGIN


@interface BYILiveVodController : UIViewController

/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 直播配置 */
@property (nonatomic ,strong) BYLiveConfig *config;
/** 播放器 */
@property (nonatomic ,strong ,readonly) BYCommonPlayerView *playerView;

@end

NS_ASSUME_NONNULL_END
