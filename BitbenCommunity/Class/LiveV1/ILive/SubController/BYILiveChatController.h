//
//  BYILiveChatController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYILiveScrollView.h"
#import "BYILiveIntroViewV1.h"
#import "BYILiveAudienceToolBar.h"

NS_ASSUME_NONNULL_BEGIN

@class BYILiveChatController;
@protocol BYILiveChatControllerDelegate <NSObject>

/// 代理设置播放器的播放和暂停状态
- (void)controlViewPlayingStatus:(BYILiveChatController *)controlView isPlaying:(BOOL)isPlaying;
// 滑块滑动
- (void)controlViewSliderValueChange:(BYILiveChatController *)controlView slider:(UISlider *)slider;
// 滑动停止
- (void)controlViewSliderTouchEnded:(BYILiveChatController *)controlView slider:(UISlider *)slider;


@end

@interface BYILiveChatController : UIViewController

/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 直播配置 */
@property (nonatomic ,strong) BYLiveConfig *config;
/** 开始直播回调 */
@property (nonatomic ,copy) void (^didBeginLiveHandle)(void);
/** delegate */
@property (nonatomic ,weak) id <BYILiveChatControllerDelegate>delegate;
/** 直播简介 */
@property (nonatomic ,strong ,readonly) BYILiveIntroViewV1 *introView;
/** 观众toolBar */
@property (nonatomic ,strong ,readonly) BYILiveAudienceToolBar *audienceToolBar;


- (void)addMessageData:(NSArray *)msgs;

- (void)showPromptTitle:(NSString *)title;
- (void)hiddenPrompt;
@end

NS_ASSUME_NONNULL_END
