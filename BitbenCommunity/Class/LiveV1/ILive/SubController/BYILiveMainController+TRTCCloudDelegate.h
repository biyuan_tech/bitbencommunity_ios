//
//  BYILiveMainController+TRTCCloudDelegate.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveMainController.h"
#import <TXLiteAVSDK_Professional/TXLiteAVSDK.h>
#import "TRTCVideoView.h"


NS_ASSUME_NONNULL_BEGIN

@interface BYILiveMainController (TRTCCloudDelegate)<TRTCCloudDelegate,TRTCVideoViewDelegate>

@end

NS_ASSUME_NONNULL_END
