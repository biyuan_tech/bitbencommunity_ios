//
//  BYILiveChatController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveChatController.h"
#import "ShareRootViewController.h"

#import "BYPFChatWelcomView.h"
#import "BYILiveHostOperationView.h"
#import "BYPFRewardAnimationView.h"
#import "BYPFUserInfoView.h"
#import "BYILiveHostToolBar.h"
#import "BYILiveVodToolBar.h"
#import "BYILiveAudienceToolBar.h"
#import "BYReportSheetView.h"
#import "BYNewMsgBottomView.h"

#import "BYPFChatModel.h"
#import "BYUpVideoCellModel.h"
#import "BYIMDefine.h"


@interface BYILiveChatController ()<BYCommonTableViewDelegate>

/** 主播直播信息 */
@property (nonatomic ,strong) UIView *hostInfoView;
/** 关注按钮 */
@property (nonatomic ,strong) UIButton *attentionBtn;
/** 主播头像 */
@property (nonatomic ,strong) PDImageView *logoImgView;
/** 直播人气值 */
@property (nonatomic ,strong) UILabel *watchNumLab;
/** 主播昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 简介 */
@property (nonatomic ,strong) UIControl *introControl;
/** 欢迎view */
@property (nonatomic ,strong) BYPFChatWelcomView *welcomeView;
/** 侧边操作台 */
@property (nonatomic ,strong) BYILiveHostOperationView *operationView;
/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** 打赏动画 */
@property (nonatomic ,strong) BYPFRewardAnimationView *rewardAnimationView;
/** 直播简介 */
@property (nonatomic ,strong) BYILiveIntroViewV1 *introView;
/** personView */
@property (nonatomic ,strong) BYPFUserInfoView *personView;
/** 页码 */
@property (nonatomic ,assign) NSInteger pageNum;
/** 直播倒计时view */
@property (nonatomic ,strong) UIView *timerView;
@property (nonatomic ,strong) UILabel *timerLab;
/** 直播提示 */
@property (nonatomic ,strong) UILabel *livePromptLab;
/** 开始直播按钮 */
@property (nonatomic ,strong) UIButton *beginLiveBtn;
/** 开始直播倒计时 */
@property (nonatomic ,strong) UILabel *beginLiveCountDownLab;
@property (nonatomic ,strong) NSTimer *beginLiveCountDownTimer;
/** 主持人toolbar */
@property (nonatomic ,strong) BYILiveHostToolBar *hostToolBar;
/** 观众toolBar */
@property (nonatomic ,strong) BYILiveAudienceToolBar *audienceToolBar;
/** 回放toolBar */
@property (nonatomic ,strong) BYILiveVodToolBar *vodToolBar;
/** 举报 */
@property (nonatomic ,strong) BYReportSheetView *reportSheetView;
/** 当前需要绘制UI */
@property (nonatomic ,assign) BOOL isNeedConfigUI;
/** newmsgBottomView */
@property (nonatomic ,strong) BYNewMsgBottomView *newMsgBottomView;
/** 记录当前未读消息 */
@property (nonatomic ,strong) NSMutableArray *noReadMsgs;

@end

@implementation BYILiveChatController

- (void)dealloc
{
    [self removeModelObserver];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isNeedConfigUI = YES;
        self.noReadMsgs = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageNum = 0;
    [self.view setBackgroundColor:[UIColor clearColor]];
    // 设置美颜默认值
    [self setDefultBeautyValue];
}

- (void)removeModelObserver{
    [_model removeObserver:self forKeyPath:@"watch_times"];
    [_model removeObserver:self forKeyPath:@"status"];
    [_model removeObserver:self forKeyPath:@"count_support"];
}

- (void)setModel:(BYCommonLiveModel *)model{
    if (_model) {
        [self removeModelObserver];
    }
    _model = model;
    if (self.isNeedConfigUI) {
        self.isNeedConfigUI = NO;
        [self configUI];
        [self setContentData];
        if (model.status == BY_LIVE_STATUS_SOON) {
            [self verifyLiveTime];
        }else if (model.status == BY_LIVE_STATUS_LIVING) {
            [self beginLive];
        }else {
            if (!model.video_url.length) {
                [self showLiveWaitVideoGenerate];
            }
        }
        [self addModelObserver];
        [self loadRequestGetHistoryMsg:nil];
    }
    else {
        [self setContentData];
        [self addModelObserver];
        [self reloadHostInfoViewMas];
        [self.introView reloadData:model];
    }
}

// 设置美颜默认值
- (void)setDefultBeautyValue{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kBeautyValue] floatValue] == 0 ||
        [[[NSUserDefaults standardUserDefaults] objectForKey:kWhiteValue] floatValue] == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:@(7) forKey:kBeautyValue];
        [[NSUserDefaults standardUserDefaults] setObject:@(7) forKey:kWhiteValue];
    }
}

- (void)loadMoreMsg{
    @weakify(self);
    [self loadRequestGetHistoryMsg:^(NSArray *data) {
        @strongify(self);
        [self.tableView addDataToTop:data];
    }];
}

// 刷新主播信息的布局
- (void)reloadHostInfoViewMas{
    CGFloat width = _model.isAttention ? 120 : 144;
    CGFloat right = !_model.isAttention ? -47 : -10;
    [UIView animateWithDuration:0.1 animations:^{
        [self.hostInfoView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(width);
        }];
        [self.userNameLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(right);
        }];
        [self.watchNumLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(right);
        }];
        [self.view layoutIfNeeded];
    }];
    
    self.attentionBtn.hidden = _model.isAttention;
}

- (void)setContentData{
    [self.logoImgView uploadHDImageWithURL:_model.head_img callback:nil];
    self.userNameLab.text = nullToEmpty(_model.nickname);
    NSString *num = [NSString transformIntegerShow:_model.watch_times];
    self.watchNumLab.text = [NSString stringWithFormat:@"%@人气值",num];
}

- (void)addMessageData:(NSArray *)msgs{
    if (!msgs.count) return;
    NSDictionary *dic = msgs[0];
    if (dic) {
        if ([dic[@"content_type"] integerValue] == BY_SIM_MSG_TYPE_CUSTOME) { // 打赏消息
            NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
            NSMutableDictionary *tmpData = [NSMutableDictionary dictionary];
            [tmpData setDictionary:customData];
            [tmpData setObject:dic[@"nickname"] forKey:@"nickname"];
            [tmpData setObject:dic[@"head_img"] forKey:@"head_img"];
            [self.rewardAnimationView addRewardData:tmpData];
        }
        else if ([dic[@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
            if ([dic[@"type"] isEqualToString:msg_open_forbid] ||
                [dic[@"type"] isEqualToString:msg_close_forbid]) {
                NSString *forbid_user_id = dic[@"forbid_user_id"];
                if ([forbid_user_id isEqualToString:ACCOUNT_ID]) {
                    BOOL isForbid = [dic[@"type"] isEqualToString:msg_open_forbid] ? YES : NO;
                    [BYSIMManager shareManager].isUserForbid = isForbid;
                    _audienceToolBar.isForbid = isForbid;
                }
            }
        }
    }
    
    NSArray *data = [BYPFChatModel transILiveTableViewData:msgs liveModel:self.model];
    if (!data.count) return;
    BOOL isFullScreen = _tableView.contentSize.height <= _tableView.frame.size.height ? NO : YES;
    BOOL scrollToBottom = _tableView.contentOffset.y >= floor(_tableView.contentSize.height - _tableView.frame.size.height) ? YES : NO;
    scrollToBottom = scrollToBottom || !isFullScreen ? scrollToBottom : NO;
    self.newMsgBottomView.hidden = scrollToBottom;

    NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
    [array addObjectsFromArray:data];
    self.tableView.tableData = array;
    if (scrollToBottom) {
        [self.tableView scrollToBottom:YES];
    }else{
        [self.noReadMsgs addObjectsFromArray:data];
        [self.newMsgBottomView setNewMsgNum:self.noReadMsgs.count];
    }
}

// 开播时间与当前时间比对
- (void)verifyLiveTime{
    NSDate *date = [NSDate by_dateFromString:self.model.begin_time dateformatter:K_D_F];
    NSDate *nowDate = [NSDate by_date];
    NSComparisonResult result = [date compare:nowDate];
    if (result == NSOrderedAscending ||
        result == NSOrderedSame) {
        [self showLivePromptLab];
    }
    else // 未到开播时间
    {
        self.timerView.layer.opacity = 1.0f;
        @weakify(self);
        [[BYCommonTool shareManager] timerCutdown:self.model.begin_time cb:^(NSDictionary * _Nonnull param, BOOL isFinish) {
            @strongify(self);
            if (isFinish) {
                [self showLivePromptLab];
            }
            else{
                self.timerLab.text = [NSString stringWithFormat:@"%02d : %02d : %02d : %02d", [param[@"days"] intValue], [param[@"hours"] intValue], [param[@"minute"] intValue], [param[@"second"] intValue]];
            }
        }];
    }
}

- (void)liveCountDown{
    NSInteger time = [self.beginLiveCountDownLab.text integerValue];
    if (time == 0) {
        [_beginLiveCountDownTimer invalidate];
        _beginLiveCountDownTimer = nil;
        _beginLiveCountDownLab.hidden = YES;
        if (self.didBeginLiveHandle) {
            self.didBeginLiveHandle();
        }
        return;
    }
    time--;
    self.beginLiveCountDownLab.text = stringFormatInteger(time);
}

- (void)showLivePromptLab{
    if (!self.config.isHost) {
        self.livePromptLab.text = @"等待主播开播";
        self.livePromptLab.textColor = [UIColor whiteColor];
        self.livePromptLab.font = [UIFont systemFontOfSize:20];
        [self.livePromptLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(125);
            make.height.mas_equalTo(stringGetHeight(self.livePromptLab.text, 20));
        }];
    }
    self.livePromptLab.hidden = NO;
}

- (void)showLiveWaitVideoGenerate{
    [self showPromptTitle:@"直播回放生成中"];
}

- (void)showPromptTitle:(NSString *)title{
    [_livePromptLab removeFromSuperview];
    [self.view addSubview:self.livePromptLab];
    self.livePromptLab.text = title;
    self.livePromptLab.textColor = [UIColor whiteColor];
    self.livePromptLab.font = [UIFont systemFontOfSize:20];
    CGSize size = [self.livePromptLab.text getStringSizeWithFont:[UIFont systemFontOfSize:20] maxWidth:256];
    CGFloat top = iPhone5 ? 210 : 315;
    [self.livePromptLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(top);
        make.width.mas_equalTo(ceil(size.width));
        make.height.mas_equalTo(ceil(size.height));
        make.centerX.mas_equalTo(0);
    }];
    _livePromptLab.hidden = NO;
}

- (void)hiddenPrompt{
    self.livePromptLab.hidden = YES;
}

// 开始直播倒计时
- (void)beginLive{
    if (_config.isHost) { // 主播进入倒计时准备开播
        self.timerView.hidden = YES;
        self.beginLiveCountDownLab.hidden = NO;
        [self.timerView removeFromSuperview];
        _beginLiveCountDownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(liveCountDown) userInfo:nil repeats:YES];
    }else{
        self.timerView.hidden = YES;
        if (self.didBeginLiveHandle) {
            self.didBeginLiveHandle();
        }
    }
}


#pragma mark - action
// 主播头像点击
- (void)userlogoAction{
    if (![AccountModel sharedAccountModel].hasLoggedIn) {
        [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
        return;
    }
    if ([ACCOUNT_ID isEqualToString:self.model.user_id]) return;
    self.personView.user_id = self.model.user_id;
    [self.personView showAnimation];
}

// 开始直播
- (void)beginLiveBtnAction{
    NSDate *date = [NSDate by_dateFromString:self.model.begin_time dateformatter:K_D_F];
    NSDate *nowDate = [NSDate by_date];
    NSComparisonResult result = [date compare:nowDate];
    if (result == NSOrderedAscending ||
        result == NSOrderedSame) {
        [self beginLive];
    }else{
        UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:@"开播提示" message:@"还未到开播时间，确定要开播吗？" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            // 开始直播
            [self beginLive];
        }];
        
        [sheetController addAction:confirmAction];
        [sheetController addAction:cancelAction];
        [CURRENT_VC.navigationController presentViewController:sheetController animated:YES completion:nil];
    }
}

// 主播关注
- (void)attentionBtnAction{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:_model.user_id
                                             isAttention:!self.model.isAttention
                                            successBlock:^(id object) {
                                                @strongify(self);
                                                self.model.isAttention = !self.model.isAttention;
                                                [self reloadHostInfoViewMas];
                                                [self.introView reloadAttentionStatus:self.model.isAttention];
                                            } faileBlock:nil];
}

// 打开简介
- (void)introlControlAction{
    [self.introView showAnimation];
}

// 弹幕开关
- (void)barrageAction:(BOOL)isOn{
    self.rewardAnimationView.hidden = !isOn;
}

// 分享
- (void)shareAction{
    if (!self.model.shareMap) return;
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.collection_status = self.model.collection_status;
    shareViewController.transferCopyUrl = self.model.shareMap.share_url;
    if (![AccountModel sharedAccountModel].hasLoggedIn) {
        shareViewController.hasJubao = NO;
    }else {
        shareViewController.hasJubao = [self.model.user_id isEqualToString:ACCOUNT_ID] ? NO : YES;
    }
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        NSString *mainImgUrl = [PDImageView appendingImgUrl:weakSelf.model.shareMap.share_img];
        NSString *imgurl = [mainImgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [ShareSDKManager shareManagerWithType:shareType title:weakSelf.model.shareMap.share_title desc:weakSelf.model.shareMap.share_intro img:imgurl url:weakSelf.model.shareMap.share_url callBack:NULL];
        [ShareSDKManager shareSuccessBack:shareTypeLive block:NULL];
    }];
    @weakify(self);
    [shareViewController actionClickWithJubaoBlock:^{
        @strongify(self);
        if (!self.model) return ;
        self.reportSheetView.theme_id = self.model.live_record_id;
        self.reportSheetView.theme_type = BY_THEME_TYPE_LIVE;
        self.reportSheetView.user_id = self.model.user_id;
        [self.reportSheetView showAnimation];
    }];
    
    [shareViewController actionClickWithCollectionBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.model.collection_status = !strongSelf.model.collection_status;
    }];
    [shareViewController showInView:self];
}

// 移除欢迎语
- (void)hiddenWelcomeViewAnimation{
    self.welcomeView.transform = CGAffineTransformMakeScale(1, 1);
    [UIView animateWithDuration:2.0 animations:^{
        self.welcomeView.transform = CGAffineTransformMakeScale(1, 0);
    }];
}

- (void)scrollToMsgBottom{
    [self.tableView scrollToBottom:YES];
}

#pragma mark - KVO
- (void)addModelObserver{
    [_model addObserver:self forKeyPath:@"watch_times" options:NSKeyValueObservingOptionNew context:nil];
    [_model addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    [_model addObserver:self forKeyPath:@"count_support" options:NSKeyValueObservingOptionNew context:nil];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"watch_times"]) {
        NSString *num = [NSString transformIntegerShow:[change[NSKeyValueChangeNewKey] integerValue]];
        self.watchNumLab.text = [NSString stringWithFormat:@"%@人气值",num];
    }else if ([keyPath isEqualToString:@"status"]) {
        BY_LIVE_STATUS status = [change[NSKeyValueChangeNewKey] integerValue];
        if (status != BY_LIVE_STATUS_LIVING) return;
        if (self.config.isHost) {
            [self addHostOperationView];
        }else{
            self.timerView.hidden = YES;
            [_audienceToolBar reloadLiveStatus];
        }
    }else if ([keyPath isEqualToString:@"count_support"]){
        [_hostToolBar reloadSuppertNum];
        [_audienceToolBar reloadSuppertNum];
    }
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
   if ([actionName isEqualToString:@"userNameAction"]) {
       if (![AccountModel sharedAccountModel].hasLoggedIn) {
           [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
           return;
       }
        NSString *userId = [param[@"user_id"] length] ? param[@"user_id"] : self.model.user_id;
        if ([userId isEqualToString:ACCOUNT_ID]) return;
        if (![userId verifyIsNum]) return;
        self.personView.user_id = userId;
        [self.personView showAnimation];
    }
}

- (void)by_tableViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height) {
        self.newMsgBottomView.hidden = YES;
        [self.noReadMsgs removeAllObjects];
    }
}


#pragma mark - request
- (void)loadRequestGetHistoryMsg:(void(^)(NSArray *data))cb{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetPageChatHistory:self.model.group_id pageNum:self.pageNum sendTime:[BYSIMManager shareManager].joinTime successBlock:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        if (!self) return ;
        if (![object[@"content"] isKindOfClass:[NSArray class]] || ![object[@"content"] count]) {
            if (cb) cb(@[]);
            return;
        }
        if (self.pageNum == 0) {
            NSMutableArray *data = [NSMutableArray array];
            NSArray *array = [BYPFChatModel transILiveTableViewData:object[@"content"] liveModel:self.model];
            array = [[array reverseObjectEnumerator] allObjects];
            [data addObjectsFromArray:array];
            [data addObjectsFromArray:self.tableView.tableData];
            self.tableView.tableData = array;
            [self.tableView reloadData];
            [self.tableView scrollToBottom:YES];
        }
        else{
            NSArray *array = [BYPFChatModel transILiveTableViewData:object[@"content"] liveModel:self.model];
            array = [[array reverseObjectEnumerator] allObjects];
            cb(array);
        }
        self.pageNum ++;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

#pragma mark - configUI

- (void)configUI{
  
    [self addHostLiveInfoView];
    [self addIntroControl];
    [self addWelComeView];
    if (_model.status == BY_LIVE_STATUS_SOON ||
        _model.status == BY_LIVE_STATUS_LIVING) {
        [self addTimerView];
    }
    
    if (_model.status != BY_LIVE_STATUS_SOON &&
        _model.status != BY_LIVE_STATUS_LIVING) {
        [self.view addSubview:self.vodToolBar];
        [self.vodToolBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(0);
            make.height.mas_equalTo(kSafe_Mas_Bottom(76));
        }];
    }else{
        if (_config.isHost) {
            [self.view addSubview:self.beginLiveCountDownLab];
            CGFloat top = iPhone5 ? 100 : 205;
            [self.beginLiveCountDownLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(0);
                make.width.mas_equalTo(stringGetWidth(self.beginLiveCountDownLab.text, 120));
                make.height.mas_equalTo(stringGetHeight(self.beginLiveCountDownLab.text, 120));
                make.top.mas_equalTo(top);
            }];
            
            [self.view addSubview:self.hostToolBar];
            [self.hostToolBar mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.bottom.mas_equalTo(0);
                make.height.mas_equalTo(kSafe_Mas_Bottom(49));
            }];
        }else{
            [self.view addSubview:self.audienceToolBar];
            [self.audienceToolBar mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.bottom.mas_equalTo(0);
                make.height.mas_equalTo(kSafe_Mas_Bottom(49));
            }];
        }
    }

    [self.view addSubview:self.tableView];
    CGFloat bottom = 0;
    if (_model.status == BY_LIVE_STATUS_SOON ||
        _model.status == BY_LIVE_STATUS_LIVING){
        bottom = -kSafe_Mas_Bottom(59);
    }else{
        bottom = -kSafe_Mas_Bottom(76);
    }
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(bottom);
        make.height.mas_equalTo(176);
    }];
    
    [self.view addSubview:self.newMsgBottomView];
    [self.newMsgBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.height.mas_equalTo(28);
        make.width.mas_greaterThanOrEqualTo(90);
        make.bottom.mas_equalTo(self.tableView);
    }];
    
    [self.view addSubview:self.rewardAnimationView];
    [self.rewardAnimationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.tableView.mas_top).offset(-20);
        make.height.mas_equalTo(124);
    }];
    
    [self.view addSubview:self.introView];
    [self.introView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (void)addHostLiveInfoView{
    [self.view addSubview:self.hostInfoView];
    [self.hostInfoView addSubview:self.logoImgView];
    [self.hostInfoView addSubview:self.userNameLab];
    [self.hostInfoView addSubview:self.watchNumLab];
    if (!_config.isHost && _config) {
        [self.hostInfoView addSubview:self.attentionBtn];
    }
    
    CGFloat width = 0;
    if (_attentionBtn.hidden) {
        width = 120;
    }else{
        width = _config.isHost ? 120 : 144;
    }
    [self.hostInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(kSafe_Mas_Top(25));
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(34);
    }];
    
    [self.logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(28);
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(3);
    }];
    
    CGFloat right = !_attentionBtn.hidden ? (_attentionBtn ? -47 : -10) : -10;
    [self.userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(5);
        make.left.mas_equalTo(37);
        make.right.mas_equalTo(right);
        make.height.mas_equalTo(self.userNameLab.font.pointSize);
    }];
    
   
    [self.watchNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-5);
        make.left.mas_equalTo(37);
        make.height.mas_equalTo(self.watchNumLab.font.pointSize);
        make.right.mas_equalTo(right);
    }];
   
    if (_attentionBtn) {
        [self.attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-5);
            make.height.mas_equalTo(22);
            make.width.mas_equalTo(35);
            make.centerY.mas_equalTo(0);
        }];
    }
}

- (void)addHostOperationView{
    if (_operationView) return;
    [self.view addSubview:self.operationView];
    [self.operationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(kSafe_Mas_Top(98));
        make.width.mas_equalTo(49);
        make.height.mas_equalTo(184);
    }];
}

- (void)addIntroControl{
    [self.view addSubview:self.introControl];
    
    UIImageView *homeIcon = [UIImageView by_init];
    [homeIcon by_setImageName:@"ilive_intro_home"];
    [self.introControl addSubview:homeIcon];
    
    UILabel *introLab = [UILabel by_init];
    [introLab setBy_font:11];
    introLab.textColor = [UIColor whiteColor];
    introLab.text = @"简介";
    [self.introControl addSubview:introLab];
    
    UIImageView *rightIcon = [UIImageView by_init];
    [rightIcon by_setImageName:@"ilive_intro_rightIcon"];
    [self.introControl addSubview:rightIcon];
    
    [self.introControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.hostInfoView.mas_right).offset(10);
        make.centerY.mas_equalTo(self.hostInfoView);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(24);
    }];
    
    [homeIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(homeIcon.image.size.width);
        make.height.mas_equalTo(homeIcon.image.size.height);
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(8);
    }];
    
    [introLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(24);
        make.centerY.mas_equalTo(0);
        make.height.mas_equalTo(introLab.font.pointSize);
        make.width.mas_equalTo(24);
    }];
    
    [rightIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-7);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(rightIcon.image.size.width);
        make.height.mas_equalTo(rightIcon.image.size.height);
    }];
}

- (void)addWelComeView{
    [self.view addSubview:self.welcomeView];
    [self.welcomeView startAnimation];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(13 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self hiddenWelcomeViewAnimation];
    });
}

// 添加倒计时view
- (void)addTimerView{
    if (_timerLab) return;
    
    [self.view addSubview:self.timerView];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setTextColor:[UIColor whiteColor]];
    [titleLab setBy_font:15];
    titleLab.text = @"直 播 倒 计 时";
    [self.timerView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(titleLab.text, 15));
        make.height.mas_equalTo(stringGetHeight(titleLab.text, 15));
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    
    [self.timerView addSubview:self.timerLab];
    @weakify(self);
    [_timerLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(kCommonScreenWidth);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(stringGetHeight(self.timerLab.text, 30));
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(25);
    }];
    
    UILabel *unitLab = [UILabel by_init];
    unitLab.textColor = [UIColor whiteColor];
    [unitLab setBy_font:11];
    unitLab.text = @"天               时               分               秒";
    [self.timerView addSubview:unitLab];
    [unitLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(stringGetWidth(unitLab.text, 11));
        make.height.mas_equalTo(stringGetHeight(unitLab.text, 11));
        make.top.mas_equalTo(self.timerLab.mas_bottom).mas_offset(0);
        make.centerX.mas_equalTo(0);
    }];
    
    [self.timerView addSubview:self.livePromptLab];
    CGSize size = [self.livePromptLab.text getStringSizeWithFont:[UIFont systemFontOfSize:13] maxWidth:256];
    [self.livePromptLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(110);
        make.width.mas_equalTo(ceil(size.width));
        make.height.mas_equalTo(ceil(size.height));
        make.centerX.mas_equalTo(0);
    }];
    
    if (self.config.isHost) {
        [self.timerView addSubview:self.beginLiveBtn];
        [self.beginLiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.livePromptLab.mas_bottom).offset(15);
            make.width.mas_equalTo(150);
            make.height.mas_equalTo(38);
            make.centerX.mas_equalTo(0);
        }];
    }

    
    CGFloat top = iPhone5 ? 100 : 205;
    [self.timerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(top);
        make.height.mas_equalTo(195);
    }];
}

- (UIView *)hostInfoView{
    if (!_hostInfoView) {
        _hostInfoView = [UIView by_init];
        _hostInfoView.layer.cornerRadius = 17;
        _hostInfoView.backgroundColor = kColorRGB(0, 0, 0, 0.25);
    }
    return _hostInfoView;
}

- (PDImageView *)logoImgView{
    if (!_logoImgView) {
        _logoImgView = [[PDImageView alloc] init];
        _logoImgView.contentMode = UIViewContentModeScaleAspectFill;
        _logoImgView.clipsToBounds = YES;
        _logoImgView.layer.cornerRadius = 14.0f;
        @weakify(self);
        [_logoImgView addTapGestureRecognizer:^{
            @strongify(self);
            [self userlogoAction];
        }];
    }
    return _logoImgView;
}

- (UILabel *)userNameLab{
    if (!_userNameLab) {
        _userNameLab = [UILabel by_init];
        _userNameLab.font = [UIFont systemFontOfSize:12];
        _userNameLab.textColor = [UIColor whiteColor];
    }
    return _userNameLab;
}

- (UILabel *)watchNumLab{
    if (!_watchNumLab) {
        _watchNumLab = [UILabel by_init];
        _watchNumLab.font = [UIFont systemFontOfSize:10];
        _watchNumLab.textColor = [UIColor whiteColor];
    }
    return _watchNumLab;
}

- (UIButton *)attentionBtn{
    if (!_attentionBtn) {
        _attentionBtn = [UIButton by_buttonWithCustomType];
        NSAttributedString *normalAtt = [[NSAttributedString alloc] initWithString:@"关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xffffff),NSFontAttributeName:[UIFont systemFontOfSize:11]}];
        NSAttributedString *selectAtt = [[NSAttributedString alloc] initWithString:@"关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xffffff),NSFontAttributeName:[UIFont systemFontOfSize:11]}];
        [_attentionBtn setAttributedTitle:normalAtt forState:UIControlStateNormal];
        [_attentionBtn setAttributedTitle:selectAtt forState:UIControlStateSelected];
        [_attentionBtn addTarget:self action:@selector(attentionBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [_attentionBtn setBackgroundColor:kColorRGBValue(0xea6438)];
        _attentionBtn.layer.cornerRadius = 11.0f;
        _attentionBtn.selected = _model.isAttention;
        _attentionBtn.hidden = _model.isAttention;
    }
    return _attentionBtn;
}

- (UIControl *)introControl{
    if (!_introControl) {
        _introControl = [[UIControl alloc] init];
        _introControl.layer.cornerRadius = 12;
        _introControl.backgroundColor = kColorRGB(0, 0, 0, 0.25);
        [_introControl addTarget:self action:@selector(introlControlAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _introControl;
}

- (BYPFChatWelcomView *)welcomeView{
    if (!_welcomeView) {
        _welcomeView = [[BYPFChatWelcomView alloc] init];
        _welcomeView.layer.cornerRadius = 12.0f;
        _welcomeView.frame = CGRectMake(15, kSafe_Mas_Top(64), kCommonScreenWidth - 30, 24);
    }
    return _welcomeView;
}

- (BYILiveHostOperationView *)operationView{
    if (!_operationView) {
        _operationView = [[BYILiveHostOperationView alloc] init];
        _operationView.didChangeBeautyHandle = ^(CGFloat beauty, CGFloat white) {
            [BYLiveConfig setBeautyLevel:beauty*9 whiteLevel:white*9];
            [[NSUserDefaults standardUserDefaults] setObject:@(beauty*9) forKey:kBeautyValue];
            [[NSUserDefaults standardUserDefaults] setObject:@(white*9) forKey:kWhiteValue];
        };
    }
    return _operationView;
}

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        [_tableView addHeaderRefreshTarget:self action:@selector(loadMoreMsg)];
    }
    return _tableView;
}

- (BYPFRewardAnimationView *)rewardAnimationView{
    if (!_rewardAnimationView) {
        _rewardAnimationView = [[BYPFRewardAnimationView alloc] init];
    }
    return _rewardAnimationView;
}

- (BYILiveIntroViewV1 *)introView{
    if (!_introView) {
        _introView = [[BYILiveIntroViewV1 alloc] init];
        [_introView reloadData:self.model];
        _introView.hidden = YES;
        @weakify(self);
        _introView.didUpdateHostAttention = ^(BOOL isAttention) {
            @strongify(self);
            self.model.isAttention = isAttention;
            [self reloadHostInfoViewMas];
        };
    }
    return _introView;
}

- (BYPFUserInfoView *)personView{
    if (!_personView) {
        _personView = [[BYPFUserInfoView alloc] initWithFathureView:self.view];
        _personView.model = self.model;
        _personView.noEnable = YES;
        if (_config.isHost &&
            (_model.status == BY_LIVE_STATUS_SOON ||
             _model.status == BY_LIVE_STATUS_LIVING)) {
                _personView.hasForbid = YES;
            }else {
                _personView.hasForbid = NO;
            }
        @weakify(self);
        _personView.didUpdateHostAttention = ^(BOOL isAttention) {
            @strongify(self);
            self.model.isAttention = isAttention;
            [self reloadHostInfoViewMas];
            [self.introView reloadAttentionStatus:isAttention];
        };
    }
    return _personView;
}

- (UIView *)timerView{
    if (!_timerView) {
        _timerView = [[UIView alloc] init];
        _timerView.backgroundColor = [UIColor clearColor];
    }
    return _timerView;
}

- (UILabel *)timerLab{
    if (!_timerLab) {
        _timerLab = [[UILabel alloc] init];
        _timerLab.font = [UIFont systemFontOfSize:30];
        _timerLab.textColor = [UIColor whiteColor];
        _timerLab.text = @"00 : 00 : 00 : 00";
        _timerLab.textAlignment = NSTextAlignmentCenter;
    }
    return _timerLab;
}

- (UILabel *)livePromptLab{
    if (!_livePromptLab) {
        _livePromptLab = [[UILabel alloc] init];
        [_livePromptLab setBy_font:13];
        _livePromptLab.textColor = kColorRGBValue(0xfbca43);
        _livePromptLab.numberOfLines = 2.0;
        _livePromptLab.textAlignment = NSTextAlignmentCenter;
        _livePromptLab.text = @"开播时间已到，请您及时开播，距开播时间15分钟后仍未开播，直播将自动删除";
        _livePromptLab.hidden = YES;
    }
    return _livePromptLab;
}

- (UIButton *)beginLiveBtn{
    if (!_beginLiveBtn) {
        _beginLiveBtn = [UIButton by_buttonWithCustomType];
        [_beginLiveBtn setBy_attributedTitle:@{@"title":@"开始直播",
                                               NSFontAttributeName:[UIFont systemFontOfSize:15],
                                               NSForegroundColorAttributeName:[UIColor whiteColor]
                                               } forState:UIControlStateNormal];
        [_beginLiveBtn addTarget:self action:@selector(beginLiveBtnAction) forControlEvents:UIControlEventTouchUpInside];
        _beginLiveBtn.backgroundColor = kColorRGBValue(0xea6438);
        _beginLiveBtn.layer.cornerRadius = 19;
    }
    return _beginLiveBtn;
}

- (UILabel *)beginLiveCountDownLab{
    if (!_beginLiveCountDownLab) {
        _beginLiveCountDownLab = [[UILabel alloc] init];
        _beginLiveCountDownLab.font = [UIFont systemFontOfSize:120];
        _beginLiveCountDownLab.textColor = [UIColor whiteColor];
        _beginLiveCountDownLab.text = @"3";
        _beginLiveCountDownLab.hidden = YES;
    }
    return _beginLiveCountDownLab;
}

- (BYILiveHostToolBar *)hostToolBar{
    if (!_hostToolBar) {
        _hostToolBar = [[BYILiveHostToolBar alloc] init];
        _hostToolBar.model = self.model;
        @weakify(self);
        _hostToolBar.barrageIsOnHandle = ^(BOOL isOn) {
            @strongify(self);
            [self barrageAction:isOn];
        };
        _hostToolBar.didShareActionHandle = ^{
            @strongify(self);
            [self shareAction];
        };
    }
    return _hostToolBar;
}

- (BYILiveAudienceToolBar *)audienceToolBar{
    if (!_audienceToolBar) {
        _audienceToolBar = [[BYILiveAudienceToolBar alloc] init];
        _audienceToolBar.model = self.model;
        _audienceToolBar.isForbid = [BYSIMManager shareManager].isUserForbid;
        @weakify(self);
        _audienceToolBar.barrageIsOnHandle = ^(BOOL isOn) {
            @strongify(self);
            [self barrageAction:isOn];
        };
        _audienceToolBar.didShareActionHandle = ^{
            @strongify(self);
            [self shareAction];
        };
    }
    return _audienceToolBar;
}

- (BYILiveVodToolBar *)vodToolBar{
    if (!_vodToolBar) {
        _vodToolBar = [[BYILiveVodToolBar alloc] init];
        _vodToolBar.model = self.model;
        @weakify(self);
        _vodToolBar.didShareActionHandle = ^{
            @strongify(self);
            [self shareAction];
        };
        _vodToolBar.progressSliderValueChanged = ^(UISlider * _Nonnull slider) {
            @strongify(self);
            if ([self.delegate respondsToSelector:@selector(controlViewSliderValueChange:slider:)]) {
                [self.delegate controlViewSliderValueChange:self slider:slider];
            }
        };
        _vodToolBar.progressSliderTouchEnded = ^(UISlider * _Nonnull slider) {
            @strongify(self);
            if ([self.delegate respondsToSelector:@selector(controlViewSliderTouchEnded:slider:)]) {
                [self.delegate controlViewSliderTouchEnded:self slider:slider];
            }
        };
        _vodToolBar.playerBtnHandle = ^(BOOL isPlaying) {
            @strongify(self);
            if ([self.delegate respondsToSelector:@selector(controlViewPlayingStatus:isPlaying:)]) {
                [self.delegate controlViewPlayingStatus:self isPlaying:isPlaying];
            }
        };
    }
    return _vodToolBar;
}

- (BYReportSheetView *)reportSheetView{
    if (!_reportSheetView) {
        _reportSheetView = [BYReportSheetView initReportSheetViewShowInView:self.view];
    }
    return _reportSheetView;
}

- (BYNewMsgBottomView *)newMsgBottomView{
    if (!_newMsgBottomView) {
        _newMsgBottomView = [[BYNewMsgBottomView alloc] init];
        _newMsgBottomView.hidden = YES;
        _newMsgBottomView.type = BYNEWMSG_BOTTOM_TYPE_ILIVE;
        [_newMsgBottomView addTarget:self action:@selector(scrollToMsgBottom) forControlEvents:UIControlEventTouchUpInside];
    }
    return _newMsgBottomView;
}
@end
