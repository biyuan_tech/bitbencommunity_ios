//
//  BYILiveScrollView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveScrollView.h"
#import "BYILiveChatController.h"
#import "BYIMDefine.h"
#import "BYIMCommon.h"

@interface BYILiveScrollView ()

/** 操作控制台 */
@property (nonatomic ,strong) BYILiveChatController *chatController;

@end

@implementation BYILiveScrollView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        self.pagingEnabled = YES;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        [self setContentSize:CGSizeMake(kCommonScreenWidth*2, 0)];
        [self setContentView];
    }
    return self;
}

- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    _chatController.config = self.config;
    _chatController.model = model;
}

- (void)addMessageData:(NSArray *)msgs{
    [self.chatController addMessageData:msgs];
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    BOOL res = [super pointInside:point withEvent:event];
    if (res) {
        NSInteger count = [BYIMManager sharedInstance].memberIds.count;
        // 如果当前存在上麦者
        if (count >= 2 && !self.chatController.introView.isShowIng) {
            NSInteger page = point.x/kCommonScreenWidth;
            CGPoint touchPoint = CGPointMake(point.x - page*kCommonScreenWidth, point.y);
            CGRect rect = [RENDERVIEW_FRAMES[0] CGRectValue];
            BOOL isPoint = CGRectContainsPoint(rect, touchPoint);
            return !isPoint;
        }else{
            [self endEditing:YES];
        }
        // 直播回放时使滑块区域scrollView向下传递事件
        if (_model.status == BY_LIVE_STATUS_END &&
            _model.video_url.length) {
            CGRect rect = CGRectMake(kCommonScreenWidth, kCommonScreenHeight - kSafe_Mas_Top(76), kCommonScreenWidth, 30);
            BOOL isPoint = CGRectContainsPoint(rect, point);
            return !isPoint;
        }
    }
    return res;
}

#pragma mark - configUI
- (void)setContentView{
    [self addSubview:self.chatController.view];
    [self.chatController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCommonScreenWidth);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(kCommonScreenHeight);
        make.width.mas_equalTo(kCommonScreenWidth);
    }];
    [self setContentOffset:CGPointMake(kCommonScreenWidth, 0) animated:NO];
}

- (BYILiveChatController *)chatController{
    if (!_chatController) {
        _chatController = [[BYILiveChatController alloc] init];
        @weakify(self);
        _chatController.didBeginLiveHandle = ^{
            @strongify(self);
            if (self.didBeginLiveHandle) self.didBeginLiveHandle();
        };
        [CURRENT_VC addChildViewController:_chatController];
    }
    return _chatController;
}

- (BYLiveConfig *)config{
    if (!_config) {
        _config = [[BYLiveConfig alloc] init];
    }
    return _config;
}

@end
