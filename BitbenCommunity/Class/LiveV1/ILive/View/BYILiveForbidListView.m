//
//  BYILiveForbidListView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/5.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveForbidListView.h"
#import "BYUpVideoCellModel.h"

static NSInteger const contentH = 274.f;
@interface BYILiveForbidListView ()<BYCommonTableViewDelegate>

/**  superView */
@property (nonatomic ,strong) UIView *fathuerView;
/** contentView */
@property (nonatomic ,strong) UIView *contentView;
/** maskView */
@property (nonatomic ,strong) UIView *maskView;
/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** 当前hidden状态 */
@property (nonatomic ,assign) BOOL isHidden;

@end

@implementation BYILiveForbidListView

+ (instancetype)initVideoListViewShowInView:(UIView *)view{
    BYILiveForbidListView *listView = [[BYILiveForbidListView alloc] init];
    listView.fathuerView = view ? view : kCommonWindow;
    listView.frame = listView.fathuerView.bounds;
    listView.hidden = YES;
    [listView configView];
    return listView;
}

- (void)setMembers:(NSArray *)members{
    _members = members;
    if (!members.count) {
        self.tableView.tableData = @[];
        return;
    }
    NSArray *data = [BYUpVideoCellModel getForbidMembersData:members];
    self.tableView.tableData = data;
}

- (void)showAnimation{
    self.hidden = NO;
    [self.fathuerView addSubview:self];
//    self.tableView.tableData = [BYIMManager sharedInstance].upToVideoMembers;
    self.maskView.layer.opacity = 1.0f;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.2 animations:^{
            [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
            }];
            [self layoutIfNeeded];
        }];
    });
}

- (void)hiddenAnimation{
    self.maskView.layer.opacity = 0.0f;
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.2 animations:^{
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(contentH);
        }];
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
        self.hidden = YES;
        [self removeFromSuperview];
    }];
}


#pragma mark - BYCommonTableViewDelegate

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    BYUpVideoCellModel *model = tableView.tableData[indexPath.row];
    if ([actionName isEqualToString:@"removeAction"]) {
        @weakify(self);
        [[BYSIMManager shareManager] forbidMessage:NO user_id:model.userId suc:^{
            @strongify(self);
            NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [array removeObject:model];
            self.tableView.tableData = [array copy];
        } fail:^{
            showToastView(@"移除失败", CURRENT_VC.view);
        }];
    }
}

#pragma mark - configUI

- (void)configView{
    self.maskView = [UIView by_init];
    self.maskView.layer.opacity = 0.0f;
    [self addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenAnimation)];
    [self.maskView addGestureRecognizer:tapGestureRecognizer];
    
    self.contentView = [UIView by_init];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    self.contentView.layer.shadowColor = kColorRGBValue(0x4e4e4e).CGColor;
    self.contentView.layer.shadowOpacity = 0.2;
    self.contentView.layer.shadowRadius = 25;
    [self addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(contentH);
        make.height.mas_equalTo(contentH);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    titleLab.font = [UIFont boldSystemFontOfSize:16];
    titleLab.text = @"禁言列表";
    titleLab.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(20);
    }];
    
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    [self.contentView addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(40, 0, 0, 0));
    }];
    
    UIView *footerView = [UIView by_init];
    footerView.frame = CGRectMake(0, 0, kCommonScreenWidth, 10);
    footerView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableFooterView = footerView;
}


@end
