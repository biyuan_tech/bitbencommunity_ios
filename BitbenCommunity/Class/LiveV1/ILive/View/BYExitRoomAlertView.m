//
//  BYExitRoomAlertView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYExitRoomAlertView.h"

static NSInteger alertW = 270;
static NSInteger alertH = 150;
@interface BYExitRoomAlertView ()

@property (nonatomic ,strong) UIView *contentView;
@property (nonatomic ,weak)   UIView *superView;
// ** 直接退出
@property (nonatomic ,strong) UIButton *cancleBtn;
/** 关注并退出 */
@property (nonatomic ,strong) UIButton *exitBtn;
// ** 标题
@property (nonatomic ,strong) UILabel *titleLab;
// ** 内容
@property (nonatomic ,strong) UILabel *messageLab;

@end

@implementation BYExitRoomAlertView

+ (instancetype)initWithTitle:(NSString *)title message:(NSString * __nullable)message inView:(UIView *)view{
    BYExitRoomAlertView *alertView = [[BYExitRoomAlertView alloc] initWithFrame:CGRectMake(0, 0, view.bounds.size.width, view.bounds.size.height)];
    alertView.title = nullToEmpty(title);
    alertView.message = nullToEmpty(message);
    alertView.superView = view;
    return alertView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setTitle:(NSString *)title{
    _title = title;
    self.titleLab.text = title;

}

- (void)setMessage:(NSString *)message{
    _message = message;
    self.messageLab.text = message;
}

- (void)showAnimation{
    [self.superView addSubview:self];
    [self.superView bringSubviewToFront:self];
    self.contentView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView beginAnimations:@"animation" context:nil];
    [UIView setAnimationDuration:0.1];
    self.contentView.alpha = 1.0;
    self.contentView.layer.transform = CATransform3DMakeScale(1.1, 1.1, 1);
    [UIView setAnimationDelay:0.1];
    [UIView setAnimationDuration:0.1];
    self.contentView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView commitAnimations];
}

- (void)hiddenAnimation{
    [UIView beginAnimations:@"hidden" context:nil];
    [UIView setAnimationDuration:0.2];
    self.contentView.alpha = 0;
    [UIView commitAnimations];
    [self removeFromSuperview];
}

#pragma mark - action
- (void)cancleBtnAction{
    [self hiddenAnimation];
    if (self.didExitHandle) {
        self.didExitHandle();
    }
}

- (void)exitBtnAction{
    [self hiddenAnimation];
    if (self.didExitAndAttentionHandle) {
        self.didExitAndAttentionHandle();
    }
}

#pragma mark - configUI

- (void)setContentView{
    UIView *contentView = [[UIView alloc] init];
    [contentView setBackgroundColor:[UIColor whiteColor]];
    contentView.layer.cornerRadius = 10.0f;
    [self addSubview:contentView];
    contentView.alpha = 0.0;
    _contentView = contentView;
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(alertW);
        make.height.mas_equalTo(alertH);
        make.centerX.centerY.mas_equalTo(0);
    }];
    
    // 标题
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.font = [UIFont boldSystemFontOfSize:16];
    titleLab.textColor = kColorRGBValue(0x323232);
    titleLab.textAlignment = NSTextAlignmentCenter;
    titleLab.numberOfLines = 0;
    [contentView addSubview:titleLab];
    _titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(28);
        make.centerX.mas_equalTo(0);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(titleLab.font.pointSize);
    }];
    
    // 内容
    UILabel *messageLab = [[UILabel alloc] init];
    messageLab.font = [UIFont systemFontOfSize:14];
    messageLab.textColor = kColorRGBValue(0x818181);
    messageLab.textAlignment = NSTextAlignmentCenter;
    messageLab.numberOfLines = 0;
    [contentView addSubview:messageLab];
    _messageLab = messageLab;
    [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(58);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(messageLab.font.pointSize);
    }];
    
    // 取消按钮
    UIButton *cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    NSAttributedString *cancleAttributedString = [[NSAttributedString alloc] initWithString:@"直接退出" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13],NSForegroundColorAttributeName:kColorRGBValue(0xbfbfbf)}];
    [cancleBtn setAttributedTitle:cancleAttributedString forState:UIControlStateNormal];
    [cancleBtn addTarget:self action:@selector(cancleBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [cancleBtn setBackgroundColor:[UIColor whiteColor]];
    cancleBtn.layer.borderWidth = 0.5;
    cancleBtn.layer.borderColor = kColorRGBValue(0x8f8f8f).CGColor;
    cancleBtn.layer.cornerRadius = 17;
    [contentView addSubview:cancleBtn];
    _cancleBtn = cancleBtn;
    [cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(34);
        make.left.mas_equalTo(30);
        make.width.mas_equalTo(100);
        make.bottom.mas_equalTo(-21);
    }];
    
    UIButton *exitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    NSAttributedString *exitAttributedString = [[NSAttributedString alloc] initWithString:@"关注并退出" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13],NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [exitBtn setAttributedTitle:exitAttributedString forState:UIControlStateNormal];
    [exitBtn addTarget:self action:@selector(exitBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [exitBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    exitBtn.layer.cornerRadius = 17;
    [contentView addSubview:exitBtn];
    _exitBtn = exitBtn;
    [exitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(34);
        make.right.mas_equalTo(-30);
        make.width.mas_equalTo(100);
        make.bottom.mas_equalTo(-21);
    }];
    
    [self addTarget:self action:@selector(hiddenAnimation) forControlEvents:UIControlEventTouchUpInside];
}

@end
