//
//  BYILiveForbidMemberCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/5.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveForbidMemberCell.h"
#import "BYUpVideoCellModel.h"

@interface BYILiveForbidMemberCell ()

/** userlogo */
@property (nonatomic ,strong) PDImageView *userlogoImgView;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 移除 */
@property (nonatomic ,strong) UIButton *removeBtn;


@end

@implementation BYILiveForbidMemberCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    [super setContentWithObject:object indexPath:indexPath];
    BYUpVideoCellModel *model = object[indexPath.row];
    [self.userlogoImgView uploadHDImageWithURL:model.userlogoUrl callback:nil];
    
    self.userNameLab.text = model.userName;
}

- (void)removeBtnAction{
    [self sendActionName:@"removeAction" param:nil indexPath:self.indexPath];
}

- (void)setContentView{
    self.userlogoImgView = [[PDImageView alloc] init];
    self.userlogoImgView.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:self.userlogoImgView];
    self.userlogoImgView.layer.cornerRadius = 18.0;
    self.userlogoImgView.clipsToBounds = YES;
    [self.userlogoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(36);
        make.height.mas_equalTo(36);
    }];
    
    self.userNameLab = [UILabel by_init];
    self.userNameLab.textColor = kColorRGBValue(0x313131);
    self.userNameLab.textAlignment = NSTextAlignmentLeft;
    [self.userNameLab setBy_font:15];
    [self.contentView addSubview:self.userNameLab];
    @weakify(self);
    [self.userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.userlogoImgView.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(0);
        make.height.mas_equalTo(self.userNameLab.font.pointSize);
        make.right.mas_equalTo(-80);
    }];
    
    self.removeBtn = [UIButton by_buttonWithCustomType];
    [self.removeBtn setBy_attributedTitle:@{@"title":@"移除",
                                            NSFontAttributeName:[UIFont systemFontOfSize:12],
                                            NSForegroundColorAttributeName:kColorRGBValue(0xfefefe)
                                            } forState:UIControlStateNormal];
    [self.removeBtn addTarget:self action:@selector(removeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.removeBtn setBackgroundColor:kColorRGBValue(0x000000)];
    [self.contentView addSubview:self.removeBtn];
    self.removeBtn.layer.cornerRadius = 4.0;
    [self.removeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.right.mas_equalTo(-15);
        make.width.mas_equalTo(46);
        make.height.mas_equalTo(22);
    }];
}

@end
