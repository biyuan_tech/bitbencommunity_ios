//
//  BYILiveHostOperationView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYILiveHostOperationView : UIView

/** 美颜回调 */
@property (nonatomic ,copy) void (^didChangeBeautyHandle)(CGFloat beauty,CGFloat white);

@end

NS_ASSUME_NONNULL_END
