//
//  BYILiveChatSystemCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveChatSystemCell.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

#import "BYPFChatModel.h"


@interface BYILiveChatSystemCell ()

/** titleLab */
@property (nonatomic ,strong) YYLabel *titleLab;

@property (nonatomic ,strong) UIView *maskBgView;

@end

@implementation BYILiveChatSystemCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYPFChatModel *model = object[indexPath.row];
    if (model.msg_type == BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW) {
        NSRange range = [model.title rangeOfString:model.userName];
        @weakify(self);
        [model.attributedString yy_setTextHighlightRange:range color:kColorRGBValue(0xffd050) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            @strongify(self);
            [self sendActionName:@"userNameAction" param:@{@"user_id":model.user_id} indexPath:self.indexPath];
        }];
    }else if (model.msg_type == BY_SIM_MSG_TYPE_CUSTOME) {
        NSRange range = [model.title rangeOfString:model.userName];
        @weakify(self);
        [model.attributedString yy_setTextHighlightRange:range color:kColorRGBValue(0x8f8f8f) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            @strongify(self);
            [self sendActionName:@"userNameAction" param:@{@"user_id":model.user_id} indexPath:self.indexPath];
        }];
    }
    
    self.titleLab.attributedText = model.attributedString;
    [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(model.topBottomInset);
        make.width.mas_equalTo(ceil(model.titleSize.width));
        make.height.mas_equalTo(ceil(model.titleSize.height));
    }];
}

- (void)setContentView{
    YYLabel *titleLab = [[YYLabel alloc] init];
    titleLab.font = [UIFont systemFontOfSize:13];
    titleLab.textColor = kColorRGBValue(0x313131);
    titleLab.numberOfLines = 0;
    titleLab.textAlignment = NSTextAlignmentLeft;
    titleLab.lineBreakMode = NSLineBreakByCharWrapping;
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
        make.top.mas_equalTo(6);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_greaterThanOrEqualTo(14);
    }];
    
    UIView *maskBgView = [UIView by_init];
    [maskBgView setBackgroundColor:kColorRGB(0, 0, 0, 0.25)];
    maskBgView.layer.cornerRadius = 12;
    [self.contentView addSubview:maskBgView];
    [self.contentView sendSubviewToBack:maskBgView];
    _maskBgView = maskBgView;
    [maskBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(-5);
        make.right.mas_equalTo(self.titleLab).offset(10);
    }];
}
@end
