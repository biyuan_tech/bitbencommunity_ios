//
//  BYExitRoomAlertView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYExitRoomAlertView : UIControl

@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *message;
/** 直接退出 */
@property (nonatomic ,copy) void (^didExitHandle)(void);
/** 关注并退出 */
@property (nonatomic ,copy) void (^didExitAndAttentionHandle)(void);

/**
 初始化创建
 
 @param title 标题
 @param message 内容
 @param view 父视图
 @return return value description
 */
+ (instancetype)initWithTitle:(NSString *)title message:(NSString * __nullable)message inView:(UIView *)view;

- (void)showAnimation;

@end

NS_ASSUME_NONNULL_END
