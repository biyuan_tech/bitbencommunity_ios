//
//  BYILiveChatTextCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveChatTextCell.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>
#import "BYPFChatModel.h"

@interface BYILiveChatTextCell ()

@property (nonatomic ,strong) UIView *maskBgView;
/** 发表的信息 */
@property (nonatomic ,strong) YYLabel *message;
/** 身份 */
@property (nonatomic ,strong) YYLabel *identityLab;

@end

@implementation BYILiveChatTextCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYPFChatModel *model = object[indexPath.row];
    
    NSRange range = [model.title rangeOfString:model.userName];
    @weakify(self);
    [model.attributedString yy_setTextHighlightRange:range color:kColorRGBValue(0xffd050) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        @strongify(self);
        [self sendActionName:@"userNameAction" param:@{@"user_id":model.user_id} indexPath:self.indexPath];
    }];
    
    self.message.attributedText = model.attributedString;
    self.identityLab.text = model.identity;
    self.identityLab.hidden = !model.identity.length;
    
    [self.identityLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(model.identity, 10) + 4);
        make.height.mas_equalTo(stringGetHeight(model.identity, 10) + 6);
    }];
    
    [self.message mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(model.titleSize.width);
        make.height.mas_equalTo(model.titleSize.height);
    }];
    
   
}

- (void)setContentView{
    
    self.identityLab = [[YYLabel alloc] init];
    self.identityLab.font = [UIFont systemFontOfSize:10];
    self.identityLab.textAlignment = NSTextAlignmentCenter;
    self.identityLab.textColor = [UIColor whiteColor];
    self.identityLab.backgroundColor = kColorRGBValue(0xf7b500);
    self.identityLab.layer.cornerRadius = 2.0f;
    self.identityLab.hidden = YES;
    [self.contentView addSubview:self.identityLab];
    [self.identityLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
        make.top.mas_equalTo(5);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    // 消息
    self.message = [[YYLabel alloc] init];
    _message.font = [UIFont systemFontOfSize:13];
    _message.lineBreakMode = NSLineBreakByCharWrapping;
    _message.numberOfLines = 0;
    [self.contentView addSubview:_message];
    [_message mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
        make.top.mas_equalTo(6);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_greaterThanOrEqualTo(12);
    }];
    
    UIView *maskBgView = [UIView by_init];
    [maskBgView setBackgroundColor:kColorRGB(0, 0, 0, 0.25)];
    maskBgView.layer.cornerRadius = 12;
    [self.contentView addSubview:maskBgView];
    [self.contentView sendSubviewToBack:maskBgView];
    _maskBgView = maskBgView;
    [maskBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(-5);
        make.right.mas_equalTo(self.message).offset(10);
    }];
}
@end
