//
//  BYILiveIntroViewV1.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveIntroViewV1.h"
#import "BYLiveDetailModel.h"

@interface BYILiveIntroViewV1 ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** uicontrol */
@property (nonatomic ,strong) UIControl *maskView;
/** contentView */
@property (nonatomic ,strong) UIView *contentView;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;


@end

@implementation BYILiveIntroViewV1

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)reloadData:(BYCommonLiveModel *)model{
    self.model = model;
    NSArray *data = [BYLiveDetailModel getILiveIntrolTableData:model];
    self.tableView.tableData = data;
}

- (void)showAnimation{
    [self.superview bringSubviewToFront:self];
    [self.tableView setContentOffset:CGPointZero];
    self.hidden = NO;
    self.isShowIng = YES;
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(0);
        }];
        [self layoutIfNeeded];
    }];
}

- (void)hiddenAnimation{
    CGFloat height = kCommonScreenHeight*2/3;
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(height);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
        self.isShowIng = NO;
    }];
}

- (void)attentionBtnAction:(NSIndexPath *)indexPath{
    NSDictionary *dic = self.tableView.tableData[indexPath.section];
    BYLiveDetailUserModel *model = dic.allValues[0][indexPath.row];
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:model.user_id
                                             isAttention:!model.isAttention
                                            successBlock:^(id object) {
                                                @strongify(self);
                                                model.isAttention = !model.isAttention;
                                                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
                                                if ([model.user_id isEqualToString:self.model.user_id]) {
                                                    if (self.didUpdateHostAttention) {
                                                        self.didUpdateHostAttention(model.isAttention);
                                                    }
                                                }
                                            } faileBlock:nil];
}

- (void)reloadAttentionStatus:(BOOL)isAttention{
    if (!self.tableView.tableData.count) {
        return;
    }
    //    self.model.isAttention = isAttention;
    NSDictionary *dic = self.tableView.tableData[1];
    NSArray *data = dic.allValues[0];
    [data enumerateObjectsUsingBlock:^(BYLiveDetailUserModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[BYLiveDetailUserModel class]]) {
            if ([obj.user_id isEqualToString:self.model.user_id]) {
                obj.isAttention = isAttention;
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
                *stop = YES;
            }
        }
    }];
}

#pragma mark - BYCommonTableViewDelegate

- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForFooterInSection:(NSInteger)section{
    UIView *sectonView = [UIView by_init];
    [sectonView setBackgroundColor:[UIColor clearColor]];
    return sectonView;
}

- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForFooterInSection:(NSInteger)section{
    return 10;
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"attentionAction"]) {
        [self attentionBtnAction:indexPath];
    }
}

#pragma mark - configUI

- (void)setContentView{
    
    [self addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    [self addSubview:self.contentView];
    CGFloat height = kCommonScreenHeight*2/3;
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(height);
        make.bottom.mas_equalTo(height);
    }];
    [self.contentView layerCornerRadius:10.0f byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight size:CGSizeMake(kCommonScreenWidth, height)];
    
    UILabel *introLab = [UILabel by_init];
    introLab.font = [UIFont boldSystemFontOfSize:16];
    introLab.textColor = kColorRGBValue(0x323232);
    introLab.text = @"直播简介";
    [self.contentView addSubview:introLab];
    [introLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(16);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(introLab.font.pointSize);
    }];
    
    [self.contentView addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(45, 0, 0, 0));
    }];

}

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        _tableView.backgroundColor = kColorRGBValue(0xf2f4f5);
    }
    return _tableView;
}

- (UIControl *)maskView{
    if (!_maskView) {
        _maskView = [[UIControl alloc] init];
        [_maskView addTarget:self action:@selector(hiddenAnimation) forControlEvents:UIControlEventTouchUpInside];
    }
    return _maskView;
}

- (UIView *)contentView{
    if (!_contentView) {
        _contentView = [UIView by_init];
        [_contentView setBackgroundColor:[UIColor whiteColor]];
    }
    return _contentView;
}
@end
