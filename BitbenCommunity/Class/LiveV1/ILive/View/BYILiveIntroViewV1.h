//
//  BYILiveIntroViewV1.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYILiveIntroViewV1 : UIView

/** 当前的显示状态 */
@property (nonatomic ,assign) BOOL isShowIng;
/** 关注了主播状态变更多回调 */
@property (nonatomic ,copy) void (^didUpdateHostAttention)(BOOL isAttention);


- (void)showAnimation;

- (void)reloadData:(BYCommonLiveModel *)model;

- (void)reloadAttentionStatus:(BOOL)isAttention;

@end

NS_ASSUME_NONNULL_END
