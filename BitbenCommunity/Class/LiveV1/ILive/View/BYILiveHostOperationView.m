//
//  BYILiveHostOperationView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveHostOperationView.h"
#import "BYUPVideoListView.h"
#import "BYBeautyView.h"
#import "BYSIMManager.h"
#import "BYIMManager+SendMsg.h"


static NSInteger baseTag = 0x422;
@interface BYILiveHostOperationView ()

/** listView */
@property (nonatomic ,strong) BYUPVideoListView *listView;
/** beautyView美颜 */
@property (nonatomic ,strong) BYBeautyView *beautyView;

@end

@implementation BYILiveHostOperationView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setContentView];
    }
    return self;
}

- (void)hostButtonAction:(UIButton *)sender{
    switch (sender.tag - baseTag) {
        case 0: // 切换摄像头
            [BYLiveConfig changeCameraPos];
            break;
        case 1: // 显示连麦列表
            [self.listView showAnimation];
            break;
        case 2: // 美颜
            [self.beautyView showAnimation];
            break;
        default:
            break;
    }
}

- (void)upToVideoMembersValueDidChangle{
    UIView *bgView = [self viewWithTag:2*baseTag];
    UILabel *numLab = [self viewWithTag:2*baseTag + 1];
    if (![BYIMManager sharedInstance].upToVideoMembers.count) {
        bgView.layer.opacity = 0.0f;
        numLab.layer.opacity = 0.0f;
    }
    else{
        numLab.text = stringFormatInteger([BYIMManager sharedInstance].upToVideoMembers.count);
        bgView.layer.opacity = 1.0f;
        numLab.layer.opacity = 1.0f;
    }
}

- (void)setContentView{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upToVideoMembersValueDidChangle) name:NSNotification_upToVideoMembersValueDidChangle object:nil];

    
    NSArray *imageNames = @[@"ilive_switch_camera",@"ilive_up_mic",@"ilive_beauty"];
    NSArray *titles     = @[@"翻转",@"连麦",@"美化"];
    for (int i = 0; i < imageNames.count; i ++) {
        UIButton *button = [UIButton by_buttonWithCustomType];
        [button setBy_imageName:imageNames[i] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(hostButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        button.tag = baseTag + i;
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(66*i);
            make.height.width.mas_equalTo(34);
        }];
        
        UILabel *titleLab = [UILabel by_init];
        [titleLab setBy_font:11];
        titleLab.textColor = [UIColor whiteColor];
        titleLab.text = titles[i];
        titleLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:titleLab];
        [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(34);
            make.top.mas_equalTo(button.mas_bottom).offset(5);
            make.height.mas_equalTo(titleLab.font.pointSize);
        }];
        
        if (i == 1) {
            UIImageView *bgView = [[UIImageView alloc] init];
            [bgView setBackgroundColor:kColorRGBValue(0xea6438)];
            bgView.layer.cornerRadius = 7.0f;
            bgView.layer.opacity = 0.0f;
            [self addSubview:bgView];
            bgView.tag = 2*baseTag;
            [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.height.mas_equalTo(15);
                make.centerX.mas_equalTo(button).offset(13);
                make.centerY.mas_equalTo(button).offset(-11);
            }];
            
            UILabel *numLab = [UILabel by_init];
            [numLab setBy_font:11];
            numLab.textColor = [UIColor whiteColor];
            numLab.textAlignment = NSTextAlignmentCenter;
            numLab.backgroundColor = [UIColor clearColor];
            numLab.layer.opacity = 0.0f;
            [self addSubview:numLab];
            numLab.tag = 2*baseTag + 1;
            [numLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.height.mas_equalTo(14);
                make.center.mas_equalTo(bgView);
            }];
        }
    }
    
    self.listView = [BYUPVideoListView initVideoListViewShowInView:CURRENT_VC.view];
    self.listView.superViewController = CURRENT_VC;
    
    self.beautyView = [BYBeautyView initViewShowInView:CURRENT_VC.view];
    self.beautyView.superViewController = CURRENT_VC;
    @weakify(self);
    self.beautyView.didChangeBeautyHandle = ^(CGFloat beauty, CGFloat white) {
        @strongify(self);
        if (self.didChangeBeautyHandle) {
            self.didChangeBeautyHandle(beauty, white);
        }
    };
    
    CGFloat beautyValue = [[[NSUserDefaults standardUserDefaults] objectForKey:kBeautyValue] floatValue];
    CGFloat whiteValue = [[[NSUserDefaults standardUserDefaults] objectForKey:kWhiteValue] floatValue];
    [self.beautyView setBeautyValue:beautyValue/9.0];
    [self.beautyView setWhiteValue:whiteValue/9.0];
}
@end
