//
//  BYILiveForbidListView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/5.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYILiveForbidListView : UIView

/** superController */
@property (nonatomic ,weak) UIViewController *superViewController;
/** forbidMembers */
@property (nonatomic ,strong) NSArray *members;


+ (instancetype)initVideoListViewShowInView:(UIView *)view;

- (void)showAnimation;

@end

NS_ASSUME_NONNULL_END
