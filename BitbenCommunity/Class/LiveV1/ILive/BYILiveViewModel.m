//
//  BYILiveViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveViewModel.h"
#import "BYIMDefine.h"

#import "BYILiveController.h"
#import "BYILiveChatController.h"
#import "BYILiveEndController.h"
#import "BYILiveMainController.h"
#import "BYILiveVodController.h"
#import "BYLiveDetailController.h"

#import "BYExitRoomAlertView.h"
#import "BYILiveScrollView.h"

#define K_VC ((BYILiveController *)S_VC)

@interface BYILiveViewModel ()

/** 直播承载view */
@property (nonatomic ,strong) BYILiveMainController *mainController;
/** 直播回放承载view */
@property (nonatomic ,strong) BYILiveVodController *vodController;
/** 提供侧滑清屏 */
@property (nonatomic ,strong) BYILiveScrollView *scrollView;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 退出按钮 */
@property (nonatomic ,strong) UIButton *exitBtn;
/** alertview */
@property (nonatomic ,strong) BYExitRoomAlertView *exitAlertView;
/** maskImgView */
@property (nonatomic ,strong) PDImageView *maskImgView;
/** 模糊蒙版 */
@property (nonatomic ,strong) UIVisualEffectView *effectView;
/** 虚拟pv数 */
@property (nonatomic ,assign) NSInteger virtual_pv;
/** 心跳计时 */
@property (nonatomic ,strong) NSTimer *roomTimer;


@end

@implementation BYILiveViewModel

- (void)dealloc
{
    [BYIMManager sharedInstance].memberIds = @[];
    [_vodController removeFromParentViewController];
    [_mainController removeFromParentViewController];
    [_scrollView removeFromSuperview];
    [_exitAlertView removeFromSuperview];
    [TRTCCloud destroySharedIntance];
    _vodController = nil;
    _mainController = nil;
    _scrollView = nil;
    _exitAlertView = nil;
}

- (void)destoryTimer{
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
    [[BYCommonTool shareManager] stopTimer];
}

- (void)setContentView{
    [self configUI];
    [self loadRequestGetDetail];
    [self addMessageListener];
    [self loginNotic];
}

// 消息回调
- (void)addMessageListener{
    // 消息回调
    @weakify(self);
    [[BYSIMManager shareManager] setMessageListener:^(NSArray * _Nonnull msgs) {
        @strongify(self);
        NSDictionary *dic = msgs[0];
        if (dic) {
            if ([dic[@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
                NSDictionary *customData;
                if ([dic[@"content"] isKindOfClass:[NSString class]]) {
                    // 现返回的为纯json字符串，需要转两次
                    NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                    customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                }
                else{
                    customData = dic[@"content"];
                }
                if ([customData[kBYCustomMessageType] integerValue] == BY_GUEST_OPERA_TYPE_RECEIVE_STREAM) { // 开播消息
                    self.model.status = BY_LIVE_STATUS_LIVING;
                    [self.model setValue:@(BY_LIVE_STATUS_LIVING) forKey:@"status"];
                    if (!K_VC.isHost) {
                        [self enterTRTCRoom];
                    }
                }
                else if ([customData[kBYCustomMessageType] integerValue] == BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM) { // 关播消息
                    self.model.status = BY_LIVE_STATUS_END;
                    if (!K_VC.isHost) {
                        [self receiveEndLiveNotic:customData];
                    }
                }
            }
            
        }
        
        // 消息转发
        [self.scrollView addMessageData:msgs];
        [self.mainController addMessageData:msgs];
    }];
}

- (void)loginNotic{
    @weakify(self);
    [S_VC loginSuccessedNotif:^(BOOL isSuccessed) {
        @strongify(self);
        if (!isSuccessed) return ;
        [self loadRequestGetDetail];
    }];
}

// 退出房间
- (void)exitRoom_host{
    if (_model.status != BY_LIVE_STATUS_LIVING) {
        [S_V_NC popViewControllerAnimated:YES];
    }else{
        UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:@"关闭直播" message:@"是否确定关闭直播" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @weakify(self);
            [self.mainController exitRoom:^{
                @strongify(self);
                
                [self destoryTimer];
                
                @weakify(self);
                [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_END cb:^(id object) {
                    @strongify(self);
                    if (!object) return ;
                    NSMutableDictionary *data = [NSMutableDictionary dictionary];
                    [data setDictionary:object];
                    [data setObject:self.model.real_begin_time forKey:@"real_begin_time"];
                    // 向当前还存在的连麦观众发送下麦消息
                    [[BYIMManager sharedInstance] sendStopPushStreamMessage:data succ:^{
                        // 清空上麦成员
                        [[BYIMManager sharedInstance] removeAllUpToVideoMember];
                    } fail:nil];
                    
                    @weakify(self);
                    [[BYSIMManager shareManager] closeRoom:self.model.group_id suc:^{
                        @strongify(self);
                        showDebugToastView(@"关闭聊天室成功",S_V_VIEW);
                    } fail:^{
                        @strongify(self);
                        showDebugToastView(@"关闭聊天室失败",S_V_VIEW);
                    }];
                    
                    BYILiveEndController *liveEndController = [[BYILiveEndController alloc] init];
                    liveEndController.model = self.model;
                    liveEndController.rewardAmount = [object[@"this_reward"] floatValue];
                    liveEndController.newFansNum = [object[@"new_fans"] integerValue];
                    liveEndController.watchNum = [object[@"watch_times"] integerValue];
                    liveEndController.isHost = K_VC.isHost;
                    [S_V_NC pushViewController:liveEndController animated:YES];
                }];
                
            }];
        }];
        
        
        [sheetController addAction:confirmAction];
        [sheetController addAction:cancelAction];
        [S_V_NC presentViewController:sheetController animated:YES completion:nil];
    }
}

- (void)exitRoom_audience{
    
    // 直播状态时需调用退出房间接口先
    if (_model.status == BY_LIVE_STATUS_LIVING) {
        if (!_model.isAttention) {
            [self.exitAlertView showAnimation];
        }else{
            @weakify(self);
            [_mainController exitRoom:^{
                @strongify(self);
                [S_V_NC popViewControllerAnimated:YES];
            }];
        }
       
        return;
    }
    
    if (!_model.isAttention)
        [self.exitAlertView showAnimation];
    else
        [S_V_NC popViewControllerAnimated:YES];
}

- (void)exitRoom:(void(^)(void))cb{
    if (_model.status == BY_LIVE_STATUS_LIVING) {
        [_mainController exitRoom:^{
            if (cb) cb();
        }];
        return;
    }
    cb();
}

- (void)enterTRTCRoom{
    // 先进入音视频房间
    [self.mainController enterTRTCRoom:^{
        if (K_VC.isHost) {
            if (self.model.status != 2) {
                // 重置播放起始时间
                // 更新直播间状态
                [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_LIVING cb:NULL];
                // 发送开播通知
                [self sendOpenLiveNotic];
            }else{
                // 手动触发KVO
                self.model.status = BY_LIVE_STATUS_LIVING;
                [self.model setValue:@(BY_LIVE_STATUS_LIVING) forKey:@"status"];
            }
        }
        // 开始发送心跳包
        [self startHeartBeatTimer];
    }];
}

- (void)sendOpenLiveNotic{
    // 向当前聊天室发送开播通知
    [[BYIMManager sharedInstance] sendOpenLiveMessage:nil fail:nil];
}

// 观众接收到主播结束直播消息
- (void)receiveEndLiveNotic:(NSDictionary *)data{
    @weakify(self);
    [self.mainController exitRoom:^{
        @strongify(self);
        [self destoryController];
        BYILiveEndController *liveEndController = [[BYILiveEndController alloc] init];
        liveEndController.model = self.model;
        liveEndController.model.real_begin_time = data[@"real_begin_time"];
        liveEndController.watchNum = [data[@"watch_times"] integerValue];
        liveEndController.isHost = K_VC.isHost;
        [S_V_NC pushViewController:liveEndController animated:YES];
    }];
}

- (void)destoryController{
    NSMutableArray *array = [NSMutableArray arrayWithArray:CURRENT_VC.navigationController.viewControllers];
    [array enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[BYLiveDetailController class]]) {
            [CURRENT_VC.rt_navigationController removeViewController:obj];
        }
    }];
}

#pragma mark - action
// 退出直播
- (void)exitBtnAction{
    if (!_model || ![AccountModel sharedAccountModel].hasLoggedIn) {
        [S_V_NC popViewControllerAnimated:YES];
        return;
    }
    
    // 观众端退出
    if (![ACCOUNT_ID isEqualToString:_model.user_id]) {
        [self exitRoom_audience];
        return;
    }
    
    // 主播端退出
    [self exitRoom_host];
}

#pragma mark - 心跳(房间保活)

// 开始发送心跳
- (void)startHeartBeatTimer{
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
    _roomTimer = [NSTimer scheduledTimerWithTimeInterval:kRoomTimerDuration target:self selector:@selector(postHeartBeat) userInfo:nil repeats:YES];
    [self postHeartBeat];
}

#pragma mark - request
- (void)loadRequestGetDetail{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLiveDetail:K_VC.live_record_id successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        if (!object) return ;
        self.model = object;
        self.scrollView.config.isHost = [ACCOUNT_ID isEqualToString:self.model.user_id] ? YES : NO;
        self.scrollView.model = object;
        if (!self.maskImgView.image) {
            @weakify(self);
            [self.maskImgView uploadHDImageWithURL:self.model.head_img callback:^(UIImage *image) {
                @strongify(self);
                self.effectView.hidden = NO;
                self.maskImgView.image = [UIImage boxblurImage:image withBlurNumber:1.0];
            }];
            [self addBackController];
        }
        if (self.model.status == BY_LIVE_STATUS_SOON ||
            self.model.status == BY_LIVE_STATUS_LIVING) {
            @weakify(self);
            [[BYSIMManager shareManager] joinRoom:self.model.group_id suc:^{
                @strongify(self);
                self.scrollView.chatController.audienceToolBar.isForbid = [BYSIMManager shareManager].isUserForbid;
                showDebugToastView(@"进入房间成功", S_V_VIEW);
            } fail:^{
                @strongify(self);
                showDebugToastView(@"进入房间失败", S_V_VIEW);
            }];
        }
        
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"获取直播详情失败", S_V_VIEW);
    }];
}

// 更新直播间状态
- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status cb:(void(^)(id object))cb{
    if (!self.model.real_begin_time.length) {
        self.model.real_begin_time = [NSDate by_stringFromDate:[NSDate by_date] dateformatter:K_D_F];
    }
    NSMutableString *string = [NSMutableString stringWithString:self.model.live_record_id];
    NSString *roomId = [self.model.live_record_id substringWithRange:NSMakeRange(string.length - 8, 8)];
;
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveStatus:status live_record_id:self.model.live_record_id stream_id:roomId real_begin_time:self.model.real_begin_time successBlock:^(id object) {
        @strongify(self);
        if (cb) cb(object);
        self.model.status = status;
        PDLog(@"更新直播间状态成功");
    } faileBlock:^(NSError *error) {
        if (cb) cb(nil);
        PDLog(@"更新直播间状态失败");
    }];
}

- (void)attentionRequest{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:_model.user_id
                                             isAttention:!self.model.isAttention
                                            successBlock:^(id object) {
                                                @strongify(self);
                                                self.model.isAttention = !self.model.isAttention;
                                                [S_V_NC popViewControllerAnimated:YES];
                                            } faileBlock:nil];
    
}

// 心跳包请求
- (void)postHeartBeat{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestPostHeartBeat:_model.live_record_id
                                             virtual_pv:K_VC.isHost ? _virtual_pv : -1
                                           successBlock:^(id object) {
                                               @strongify(self);
                                               if (object && [object[@"content"] isEqualToString:@"success"]) {
                                                   self.virtual_pv = [object[@"virtualPV"] integerValue];
                                                   self.model.count_support = [object[@"count_support"] integerValue];
                                                   self.model.watch_times = self.virtual_pv;
                                                   [self.model setValue:@(self.model.count_support) forKey:@"count_support"];
                                                   [self.model setValue:@(self.model.watch_times) forKey:@"watch_times"];
                                               }
                                           } faileBlock:nil];
}


#pragma mark - configUI

- (void)configUI{
    [self addBackMaskView];
    
    [S_V_VIEW addSubview:self.scrollView];
    [S_V_VIEW addSubview:self.exitBtn];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    [self.exitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.width.height.mas_equalTo(35);
        make.top.mas_equalTo(kSafe_Mas_Top(25));
    }];
}

// 添加底部view
- (void)addBackController{
    if (self.model.status == BY_LIVE_STATUS_SOON ||
        self.model.status == BY_LIVE_STATUS_LIVING) {
        [S_V_VIEW addSubview:self.mainController.view];
        [S_V_VIEW insertSubview:self.mainController.view belowSubview:self.scrollView];
        [self.mainController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsZero);
        }];
    }else{
        [S_V_VIEW addSubview:self.vodController.view];
        [S_V_VIEW insertSubview:self.vodController.view belowSubview:self.scrollView];
        [self.vodController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsZero);
        }];
    }
}

- (void)addBackMaskView{
    [S_V_VIEW addSubview:self.maskImgView];
    [self.maskImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    effectView.frame = CGRectMake(0, 0, S_V_VIEW.bounds.size.width, S_V_VIEW.bounds.size.height);
    effectView.layer.opacity = 0.7;
    effectView.hidden = YES;
    [S_V_VIEW addSubview:effectView];
    self.effectView = effectView;
}

- (BYILiveScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[BYILiveScrollView alloc] init];
        @weakify(self);
        _scrollView.didBeginLiveHandle = ^{
            @strongify(self);
            [self enterTRTCRoom];
        };
    }
    return _scrollView;
}

- (UIButton *)exitBtn{
    if (!_exitBtn) {
        _exitBtn = [UIButton by_buttonWithCustomType];
        [_exitBtn setBy_imageName:@"ilive_exit" forState:UIControlStateNormal];
        [_exitBtn addTarget:self action:@selector(exitBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _exitBtn;
}

- (BYExitRoomAlertView *)exitAlertView{
    if (!_exitAlertView) {
        _exitAlertView = [BYExitRoomAlertView initWithTitle:@"退出直播间" message:@"关注主播不再错过更多精彩直播！" inView:S_V_VIEW];
        @weakify(self);
        _exitAlertView.didExitHandle = ^{
            @strongify(self);
            @weakify(self);
            [self exitRoom:^{
                @strongify(self);
                [S_V_NC popViewControllerAnimated:YES];
            }];
        };
        _exitAlertView.didExitAndAttentionHandle = ^{
            @strongify(self);
            @weakify(self);
            [self exitRoom:^{
                @strongify(self);
                [self attentionRequest];
            }];
        };
    }
    return _exitAlertView;
}

- (BYILiveMainController *)mainController{
    if (!_mainController) {
        _mainController = [[BYILiveMainController alloc] init];
        _mainController.config.isHost = [ACCOUNT_ID isEqualToString:_model.user_id] ? YES : NO;
        _mainController.model = _model;
        @weakify(self);
        _mainController.hostLeavingRoomInLivingHandle = ^{
            @strongify(self);
            [self.scrollView.chatController showPromptTitle:@"主播开小差了~"];
        };
        _mainController.hostJoinRoomHandle = ^{
            @strongify(self);
            [self.scrollView.chatController hiddenPrompt];
        };
        [S_VC addChildViewController:_mainController];
    }
    return _mainController;
}

- (BYILiveVodController *)vodController{
    if (!_vodController) {
        _vodController = [[BYILiveVodController alloc] init];
        _vodController.config.isHost = [ACCOUNT_ID isEqualToString:_model.user_id] ? YES : NO;
        _vodController.model = _model;
        _scrollView.chatController.delegate = _vodController;
        [S_VC addChildViewController:_vodController];
    }
    return _vodController;
}

- (PDImageView *)maskImgView{
    if (!_maskImgView) {
        _maskImgView = [[PDImageView alloc] init];
        _maskImgView.clipsToBounds = YES;
        _maskImgView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _maskImgView;
}
@end
