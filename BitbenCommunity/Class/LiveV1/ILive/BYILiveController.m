//
//  BYILiveController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveController.h"
#import "BYILiveViewModel.h"

@interface BYILiveController ()<NetworkAdapterSocketDelegate>

@end

@implementation BYILiveController

- (void)dealloc
{
    [(BYILiveViewModel *)self.viewModel destoryTimer];
    [[BYSIMManager shareManager] resetScoketStatus];
}

- (Class)getViewModelClass{
    return [BYILiveViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    self.hasCancelSocket = NO;
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    self.popGestureRecognizerEnale = NO;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (BOOL)shouldAutorotate{
    return NO;
}

// 支持哪些屏幕方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

// 默认的屏幕方向（当前ViewController必须是通过模态出来的UIViewController（模态带导航的无效）方式展现出来的，才会调用这个方法）
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data{
    [[BYSIMManager shareManager] webSocketReceiveData:type data:data];
}

-(void)webSocketDidConnectedWithSocket:(WebSocketConnection *)webSocketConnection connectedStatus:(SocketConnectType)connectedStatus{
    if (webSocketConnection == [NetworkAdapter sharedAdapter].mainRootSocketConnection) {
        if (connectedStatus == SocketConnectTypeOpen) {
            [[BYSIMManager shareManager] webSocketDidConnectedWithSocket:webSocketConnection];
        }
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
}
@end
