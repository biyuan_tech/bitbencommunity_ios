//
//  BYILiveViewModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewModel.h"
#import <TXLiteAVSDK_Professional/TXLiteAVSDK.h>
#import "BYILiveRoomSubControllerV1.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYILiveViewModel : BYCommonViewModel

/** 直播类型状态 */
@property (nonatomic ,assign) TRTCStatus roomStatus;

- (void)destoryTimer;

@end

NS_ASSUME_NONNULL_END
