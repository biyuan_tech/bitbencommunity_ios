//
//  BYILiveVodToolBar.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveVodToolBar.h"
#import "BYRewardView.h"
#import "BYLikeAnimationView.h"
#import "PlayerSlider.h"


@interface BYILiveVodToolBar ()<PlayerSliderDelegate>

/** 播放 */
@property (nonatomic ,strong) UIButton *playBtn;
/** 分享按钮 */
@property (nonatomic ,strong) UIButton *shareBtn;
/** 点赞按钮 */
@property (nonatomic ,strong) UIButton *likeBtn;
/** 打赏按钮 */
@property (nonatomic ,strong) UIButton *rewardBtn;
/** 点赞数量 */
@property (nonatomic ,strong) UILabel *suppertNumLab;
@property (nonatomic ,strong) UIImageView *suppertNumBg;
/** 打赏 */
@property (nonatomic ,strong) BYRewardView *rewardView;
/** 点赞效果 */
@property (nonatomic ,strong) BYLikeAnimationView *animationView;
/** 滑杆 */
@property (nonatomic, strong) PlayerSlider   *videoSlider;
/** 当前播放时长label */
@property (nonatomic, strong) UILabel *currentTimeLabel;
/** 视频总时长label */
@property (nonatomic, strong) UILabel *totalTimeLabel;
/** isDragging */
@property (nonatomic ,assign) BOOL isDragging;

@end

@implementation BYILiveVodToolBar

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setContentView];
        [self addNotification];
    }
    return self;
}

- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    if (model.video_url.length) {
        self.videoSlider.hidden      = NO;
        self.currentTimeLabel.hidden = NO;
        self.totalTimeLabel.hidden   = NO;
        self.playBtn.hidden          = NO;
    }
    [self reloadSuppertNum];
}

- (void)reloadSuppertNum{
    NSString *string = [NSString transformIntegerShow:self.model.count_support];
    [UIView animateWithDuration:1.0f animations:^{
        self.suppertNumLab.text = string;
    }];
    [self.suppertNumLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(string, 10) + 6);
    }];
}

- (void)setProgressTime:(NSString *)currentTime
              totalTime:(NSString *)totalTime
          progressValue:(CGFloat)progress
          playableValue:(CGFloat)playable {
    if (!self.isDragging) {
        // 更新slider
        self.videoSlider.value = progress;
    }
    self.currentTimeLabel.text = currentTime;
    self.totalTimeLabel.text   = totalTime;
    [self.videoSlider.progressView setProgress:playable animated:NO];
}

#pragma mark - action
- (void)playBtnAction:(UIButton *)sender{
    sender.selected = !sender.selected;
    if (self.playerBtnHandle) {
        self.playerBtnHandle(!sender.selected);
    }
}

- (void)shareBtnAction{
    if (self.didShareActionHandle) {
        self.didShareActionHandle();
    }
}

- (void)rewardBtnAction{
    [self resignFirstResponder];
    self.rewardView.receicer_name = self.model.nickname;
    [self.rewardView showAnimation];
}

- (void)likeBtnAction:(UIButton *)sender{
    if (!self.model.isSupport) {
        @weakify(self);
        [[BYLiveHomeRequest alloc] loadRequestLiveGuestOpearType:BY_GUEST_OPERA_TYPE_UP theme_id:self.model.live_record_id theme_type:BY_THEME_TYPE_LIVE receiver_user_id:self.model.user_id successBlock:^(id object) {
            @strongify(self);
            self.model.isSupport = YES;
            self.model.count_support++;
            [self reloadSuppertNum];
        } faileBlock:^(NSError *error) {
            showToastView(@"顶操作失败", kCommonWindow);
        }];
    }
    CGRect rect = [sender convertRect:sender.bounds toView:self.superview];
    CGPoint point = CGPointMake(rect.origin.x, rect.origin.y);
    [self.animationView beginUpAniamtion:self.superview point:point type:BY_LIKE_ANIMATION_TYPE_PF_UP];
}

- (void)progressSliderTouchBegan:(UISlider *)sender {
    self.isDragging = YES;
//    [self cancelFadeOut];
}

- (void)progressSliderValueChanged:(UISlider *)sender {
    if (self.progressSliderValueChanged) {
        self.progressSliderValueChanged(sender);
    }
//    [self.delegate controlViewPreview:self where:sender.value];
}

- (void)progressSliderTouchEnded:(UISlider *)sender {
    if (self.progressSliderTouchEnded) {
        self.progressSliderTouchEnded(sender);
    }
//    [self.delegate controlViewSeek:self where:sender.value];
    self.isDragging = NO;
}

#pragma mark - PlayerSliderDelegate
- (void)onPlayerPointSelected:(PlayerPoint *)point{
    
}

#pragma mark - request
- (void)loadRequestReward:(CGFloat)amount{
    if (!_model) {
        showToastView(@"未获取到打赏人信息", CURRENT_VC.view);
        return;
    }
    if ([_model.user_id isEqualToString:[AccountModel sharedAccountModel].account_id]) {
        showToastView(@"不能给自己打赏", CURRENT_VC.view);
        return;
    }
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestReward:_model.user_id reward_amount:amount successBlock:^(id object) {
        @strongify(self);
        [self.rewardView hiddenAnimation];
        showToastView(@"打赏成功", CURRENT_VC.view);
        [self.rewardView reloadSurplusBBT:amount];
        // 发送打赏消息
//        NSString *string = [NSString stringWithFormat:@"%@ 打赏了主播 %@ %.2fBP",[AccountModel sharedAccountModel].loginServerModel.user.nickname,self.model.nickname,amount];
//        [[BYIMManager sharedInstance] sendRewardMessage:amount receicer_user_Name:self.model.nickname receicer_user_Id:self.model.user_id text:string succ:^{
//        } fail:nil];
        
    } faileBlock:^(NSError *error) {
    }];
}

#pragma mark - NSNotification
- (void)addNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerProgressTime_notic:) name:KNSNotification_ProgressTime object:nil];
}

- (void)playerProgressTime_notic:(NSNotification *)notic{
    NSDictionary *dic = notic.object;
    NSString *currentTime = dic[@"currentTime"];
    NSString *totalTime   = dic[@"totalTime"];
    CGFloat  progress     = [dic[@"progress"] floatValue];
    CGFloat  playable     = [dic[@"playable"] floatValue];
    [self setProgressTime:currentTime totalTime:totalTime progressValue:progress playableValue:playable];
}


#pragma mark - configUI

- (void)setContentView{
    
    self.rewardView = [[BYRewardView alloc] initWithFathureView:CURRENT_VC.view];
    @weakify(self);
    _rewardView.confirmBtnHandle = ^(CGFloat amount) {
        @strongify(self);
        [self loadRequestReward:amount];
    };
    [self addSubview:self.currentTimeLabel];
    [self addSubview:self.totalTimeLabel];
    [self addSubview:self.videoSlider];
    [self addSubview:self.playBtn];
    [self addSubview:self.shareBtn];
    [self addSubview:self.rewardBtn];
    [self addSubview:self.likeBtn];
    [self addSubview:self.suppertNumLab];
    [self addSubview:self.suppertNumBg];
    [self insertSubview:self.suppertNumBg belowSubview:self.suppertNumLab];
    
    
    [self.currentTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(7);
        make.top.mas_equalTo(13);
        make.width.mas_equalTo(52);
        make.height.mas_equalTo(self.currentTimeLabel.font.pointSize);
    }];
    
    [self.totalTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-7);
        make.width.mas_equalTo(56);
        make.top.mas_equalTo(13);
        make.height.mas_equalTo(self.totalTimeLabel.font.pointSize);
    }];
    
    [self.videoSlider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.currentTimeLabel.mas_trailing);
        make.trailing.equalTo(self.totalTimeLabel.mas_leading);
        make.centerY.equalTo(self.currentTimeLabel.mas_centerY).offset(-1);
    }];
    
    [self.playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(35);
        make.width.height.mas_equalTo(34);
    }];
    
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-121);
        make.centerY.mas_equalTo(self.playBtn);
        make.width.height.mas_equalTo(34);
    }];
    
    [self.rewardBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.shareBtn.mas_right).mas_offset(11);
        make.centerY.mas_equalTo(self.shareBtn);
        make.width.mas_equalTo(43);
        make.height.mas_equalTo(43);
    }];
    
    [self.likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.rewardBtn.mas_right).mas_offset(7);
        make.centerY.mas_equalTo(self.shareBtn);
        make.width.height.mas_equalTo(34);
    }];
    
    [self.suppertNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(14);
        make.top.mas_equalTo(32);
        make.centerX.mas_equalTo(self.likeBtn).mas_offset(14);
    }];
    
    [self.suppertNumBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.suppertNumLab);
        make.height.mas_equalTo(14);
        make.top.mas_equalTo(32);
        make.centerX.mas_equalTo(self.likeBtn).mas_offset(14);
    }];
    
}

- (UIButton *)shareBtn{
    if (!_shareBtn) {
        _shareBtn = [UIButton by_buttonWithCustomType];
        [_shareBtn setBy_imageName:@"ilive_toolbar_share" forState:UIControlStateNormal];
        [_shareBtn addTarget:self action:@selector(shareBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareBtn;
}

- (UIButton *)likeBtn{
    if (!_likeBtn) {
        _likeBtn = [UIButton by_buttonWithCustomType];
        [_likeBtn setBy_imageName:@"ilive_toolbar_like" forState:UIControlStateNormal];
        [_likeBtn addTarget:self action:@selector(likeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeBtn;
}

- (UIButton *)rewardBtn{
    if (!_rewardBtn) {
        _rewardBtn = [UIButton by_buttonWithCustomType];
        [_rewardBtn setBy_imageName:@"videolive_reward" forState:UIControlStateNormal];
        [_rewardBtn addTarget:self action:@selector(rewardBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rewardBtn;
}

- (UILabel *)suppertNumLab{
    if (!_suppertNumLab) {
        _suppertNumLab = [UILabel by_init];
        _suppertNumLab.font = [UIFont boldSystemFontOfSize:10];
        _suppertNumLab.backgroundColor = [UIColor clearColor];
        _suppertNumLab.textColor = kColorRGBValue(0xf03434);
        _suppertNumLab.textAlignment = NSTextAlignmentCenter;
        
    }
    return _suppertNumLab;
}

- (UIImageView *)suppertNumBg{
    if (!_suppertNumBg) {
        _suppertNumBg = [[UIImageView alloc] init];
        _suppertNumBg.backgroundColor = [UIColor whiteColor];
        _suppertNumBg.layer.cornerRadius = 7.0;
        _suppertNumBg.layer.shadowColor = kColorRGBValue(0x575757).CGColor;
        _suppertNumBg.layer.shadowOffset = CGSizeMake(0, 0);
        _suppertNumBg.layer.shadowOpacity = 0.4;
        _suppertNumBg.layer.shadowRadius = 5.0;
    }
    return _suppertNumBg;
}

- (BYLikeAnimationView *)animationView{
    if (!_animationView) {
        _animationView = [[BYLikeAnimationView alloc] init];
    }
    return _animationView;
}

- (UIButton *)playBtn{
    if (!_playBtn) {
        _playBtn = [UIButton by_buttonWithCustomType];
        [_playBtn setBy_imageName:@"ilive_play" forState:UIControlStateSelected];
        [_playBtn setBy_imageName:@"ilive_pause" forState:UIControlStateNormal];
        [_playBtn addTarget:self action:@selector(playBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        _playBtn.hidden = YES;
    }
    return _playBtn;
}

- (PlayerSlider *)videoSlider {
    if (!_videoSlider) {
        _videoSlider                       = [[PlayerSlider alloc] init];
        [_videoSlider setThumbImage:[UIImage imageNamed:@"videoPlay_progress"] forState:UIControlStateNormal];
        _videoSlider.minimumTrackTintColor = kColorRGBValue(0xea6438);
        // slider开始滑动事件
        [_videoSlider addTarget:self action:@selector(progressSliderTouchBegan:) forControlEvents:UIControlEventTouchDown];
        // slider滑动中事件
        [_videoSlider addTarget:self action:@selector(progressSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        // slider结束滑动事件
        [_videoSlider addTarget:self action:@selector(progressSliderTouchEnded:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
        _videoSlider.delegate              = self;
        _videoSlider.hidden                = YES;
    }
    return _videoSlider;
}

- (UILabel *)currentTimeLabel{
    if (!_currentTimeLabel) {
        _currentTimeLabel               = [[UILabel alloc] init];
        _currentTimeLabel.textColor     = [UIColor whiteColor];
        _currentTimeLabel.font          = [UIFont systemFontOfSize:12.0f];
        _currentTimeLabel.text          = @"00:00";
        _currentTimeLabel.textAlignment = NSTextAlignmentCenter;
        _currentTimeLabel.hidden        = YES;
    }
    return _currentTimeLabel;
}

- (UILabel *)totalTimeLabel {
    if (!_totalTimeLabel) {
        _totalTimeLabel               = [[UILabel alloc] init];
        _totalTimeLabel.textColor     = [UIColor whiteColor];
        _totalTimeLabel.font          = [UIFont systemFontOfSize:12.0f];
        _totalTimeLabel.text          = @"00:00:00";
        _totalTimeLabel.textAlignment = NSTextAlignmentCenter;
        _totalTimeLabel.hidden        = YES;
    }
    return _totalTimeLabel;
}


@end
