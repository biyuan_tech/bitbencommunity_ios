//
//  BYILiveVodToolBar.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYILiveVodToolBar : UIView

/** 分享事件 */
@property (nonatomic ,copy) void (^didShareActionHandle)(void);
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 滑块滑动 */
@property (nonatomic ,copy) void (^progressSliderValueChanged)(UISlider *slider);
/** 滑块滑动停止 */
@property (nonatomic ,copy) void (^progressSliderTouchEnded)(UISlider *slider);
/** 播放暂停 */
@property (nonatomic ,copy) void (^playerBtnHandle)(BOOL isPlaying);


@end

NS_ASSUME_NONNULL_END
