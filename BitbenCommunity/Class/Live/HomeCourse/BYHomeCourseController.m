//
//  BYHomeCourseController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeCourseController.h"
#import "BYHomeCourseViewModel.h"

@interface BYHomeCourseController ()

@end

@implementation BYHomeCourseController

- (Class)getViewModelClass{
    return [BYHomeCourseViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"课程";
}

@end
