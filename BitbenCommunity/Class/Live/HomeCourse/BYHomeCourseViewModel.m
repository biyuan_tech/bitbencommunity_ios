//
//  BYHomeCourseViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeCourseViewModel.h"
#import "BYCourseDetailController.h"

#import "BYCourseHeaderView.h"

#import "BYHomeCourseModel.h"

@interface BYHomeCourseViewModel ()<BYCommonTableViewDelegate>
{
    BY_COURSE_TYPE _type;
    NSString *_topic;
    NSArray *_titles;
}
/** 选择器 */
@property (nonatomic ,strong) BYCourseHeaderView *headerView;
/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;

@end

@implementation BYHomeCourseViewModel
- (void)setContentView{
    _type = 0;
    _topic = @"";
    _titles = @[@"",@"新手入门",@"投资指南",@"大咖分享",@"技术进阶"];
    self.pageNum = 0;
    [self addHeaderView];
    [self addTableView];
    [self loadRequest];
}

- (void)reloadData{
    self.pageNum = 0;
    [self loadRequest];
}

- (void)loadMoreData{
    [self loadRequest];
}

#pragma mark - action
- (void)didSelectAtIndex:(NSInteger)index level:(NSInteger)level{
    if (level == 0) {
        _type = index;
    }else{
        _topic = _titles[index];
    }
    [self reloadData];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BYHomeCourseModel *model = tableView.tableData[indexPath.row];
    BYCourseDetailController *courseDetailController = [[BYCourseDetailController alloc] init];
    courseDetailController.course_id = model.course_id;
    [S_V_NC pushViewController:courseDetailController animated:YES];
}

#pragma mark - request
- (void)loadRequest{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCourseList:_type topic:_topic pageNum:_pageNum successBlock:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        [self.tableView removeEmptyView];
        if (self.pageNum == 0) {
            self.tableView.tableData = object;
            if (![object count]) {
                [self.tableView addEmptyView:@"暂无内容"];
                return ;
            }
        }else{
            NSMutableArray *tmpData = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [tmpData addObjectsFromArray:object];
            self.tableView.tableData = tmpData;
        }
        self.pageNum ++;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

#pragma mark - configUI

- (void)addHeaderView{
    BYCourseHeaderView *headerView = [[BYCourseHeaderView alloc] init];
    @weakify(self);
    headerView.didSelectAtIndex = ^(NSInteger index, NSInteger level) {
        @strongify(self);
        [self didSelectAtIndex:index level:level];
    };
    [S_V_VIEW addSubview:headerView];
    self.headerView = headerView;
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(78);
    }];
}

- (void)addTableView{
    BYCommonTableView *tableView = [[BYCommonTableView alloc] init];
    [tableView addHeaderRefreshTarget:self action:@selector(reloadData)];
    [tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    tableView.group_delegate = self;
    [S_V_VIEW addSubview:tableView];
    self.tableView = tableView;
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(78, 0, 0, 0));
    }];
    
    UIView *headerView = [UIView by_init];
    [headerView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    headerView.frame = CGRectMake(0, 0, 0, 8);
    tableView.tableHeaderView = headerView;
    
    UIView *footerView = [UIView by_init];
    [footerView setBackgroundColor:[UIColor whiteColor]];
    footerView.frame = CGRectMake(0, 0, 0, kSafe_Mas_Bottom(40));
    tableView.tableFooterView = footerView;
}

@end
