//
//  BYHomeCourseModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeCourseModel.h"

@implementation BYHomeCourseModel

+ (NSArray *)getTableData:(NSArray *)respondData{
    NSMutableArray *data = [NSMutableArray array];
    for (NSDictionary *dic in respondData) {
        BYHomeCourseModel *model = [BYHomeCourseModel mj_objectWithKeyValues:dic];
        model.cellString = @"BYHomeCourseCell";
        model.cellHeight = 130;
        [data addObject:model];
    }
    return [data copy];
}

@end
