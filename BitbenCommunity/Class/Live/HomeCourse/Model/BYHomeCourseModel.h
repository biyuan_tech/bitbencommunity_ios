//
//  BYHomeCourseModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"

NS_ASSUME_NONNULL_BEGIN


@interface BYHomeCourseModel : BYCommonModel

/** 课程Id */
@property (nonatomic ,copy) NSString *course_id;
/** 课程名 */
@property (nonatomic ,copy) NSString *course_name;
/** 课程标签 */
@property (nonatomic ,strong) NSArray *course_topic;
/** 课程宣传语 */
@property (nonatomic ,copy) NSString *course_publicize;
/** 课程外封面 */
@property (nonatomic ,copy) NSString *course_out_cover;
/** 课程简介 */
@property (nonatomic ,copy) NSString *course_intro;
/** 课程数量 */
@property (nonatomic ,assign) NSInteger course_quantity;
/** 课程学习次数 */
@property (nonatomic ,assign) NSInteger watch_times;
/** 课程状态(0 下架 1 上架) */
@property (nonatomic ,assign) NSInteger status;
/** 创建时间 */
@property (nonatomic ,copy) NSString *datetime;
/** 更新时间 */
@property (nonatomic ,copy) NSString *update_time;

+ (NSArray *)getTableData:(NSArray *)respondData;

@end

NS_ASSUME_NONNULL_END
