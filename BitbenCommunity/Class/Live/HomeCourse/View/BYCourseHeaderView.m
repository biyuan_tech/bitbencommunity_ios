//
//  BYCourseHeaderView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCourseHeaderView.h"

static NSInteger kBaseTag = 423;

@interface BYCourseHeaderView ()

/** 被选择类型 */
@property (nonatomic ,strong) UIButton *fLevelBtn;
@property (nonatomic ,strong) UIButton *sLevelBtn;
/** 间隔width */
@property (nonatomic ,assign) CGFloat spaceW;


@end

@implementation BYCourseHeaderView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)buttenAction:(UIButton *)sender{
    NSInteger level = sender.tag >= 5*kBaseTag ? 1 : 0;
    NSInteger count = level == 0 ? 3 : 5;
    NSInteger index = sender.tag - count*kBaseTag;
    UIButton *lastSelBtn = level == 0 ? _fLevelBtn : _sLevelBtn;
    if (lastSelBtn == sender) return;
    for (int i = 0; i < count; i ++) {
        UIButton *btn = [self viewWithTag:count*kBaseTag + i];
        btn.selected = sender == btn ? YES : NO;
    }
    if (level == 0) {
        self.fLevelBtn = sender;
    }else{
        self.sLevelBtn = sender;
    }
    if (self.didSelectAtIndex) {
        self.didSelectAtIndex(index, level);
    }
}

- (void)setContentView{
    CGFloat fontSize = iPhone5 ? 12 : 14;
    NSArray *fLevelTitles = @[@"默认",@"最多学习",@"最新上架"];
    NSArray *sLevelTitles = @[@"全部",@"新手入手",@"投资指南",@"大咖分享",@"技术进阶"];
    CGFloat maxWidth = stringGetWidth(@"默认", fontSize) + (stringGetWidth(@"新手入手", fontSize) + 10)*4 + 10 + 20;
    self.spaceW = (kCommonScreenWidth - maxWidth)/4.0f;
    [self addSegmentView:fLevelTitles originY:5];
    [self addSegmentView:sLevelTitles originY:38];
}

- (void)addSegmentView:(NSArray *)titles originY:(CGFloat)originY{
    NSInteger baseNum = titles.count;
    for (int i = 0; i < titles.count; i ++) {
        UIButton *button = [UIButton by_buttonWithCustomType];
        CGFloat fontSize = iPhone5 ? 12 : 14;
        UIButton *lastBtn = [self viewWithTag:kBaseTag*baseNum + i - 1];
        [button setBy_attributedTitle:@{@"title":titles[i],
                                        NSFontAttributeName:[UIFont systemFontOfSize:fontSize],
                                        NSForegroundColorAttributeName:kColorRGBValue(0x8f8f8f)
                                        } forState:UIControlStateNormal];
        [button setBy_attributedTitle:@{@"title":titles[i],
                                        NSFontAttributeName:[UIFont systemFontOfSize:fontSize],
                                        NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                        } forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttenAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        button.selected = i == 0 ? YES : NO;
        if (i == 0) {
            if (titles.count == 3) {
                self.fLevelBtn = button;
            }else{
                self.sLevelBtn = button;
            }
        }
        button.tag = kBaseTag*baseNum + i;
        CGFloat width = stringGetWidth(titles[i], fontSize) + 10;
        CGFloat height = stringGetHeight(titles[i], fontSize) + 10;
        @weakify(self);
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.top.mas_equalTo(originY);
            if (i == 0) {
                make.left.mas_equalTo(10);
            }else{
                make.left.mas_equalTo(lastBtn.mas_right).mas_offset(self.spaceW);
            }
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
        }];
    }
}

@end
