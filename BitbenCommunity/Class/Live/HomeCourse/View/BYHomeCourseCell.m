//
//  BYHomeCourseCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeCourseCell.h"
#import "BYHomeCourseModel.h"

@interface BYHomeCourseCell ()
/** 课程封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 课程标题 */
@property (nonatomic ,strong) UILabel *titleLab;
/** 课程副标题 */
@property (nonatomic ,strong) UILabel *subTitleLab;
/** 课程数 */
@property (nonatomic ,strong) UILabel *courseNumLab;
/** 学习人数 */
@property (nonatomic ,strong) UILabel *learnNumLab;


@end

@implementation BYHomeCourseCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYHomeCourseModel *model = object[indexPath.row];
    [self.coverImgView uploadHDImageWithURL:model.course_out_cover callback:nil];
    self.titleLab.text = model.course_name;
    self.subTitleLab.text = model.course_publicize;
    self.courseNumLab.text = [NSString stringWithFormat:@"%i讲",(int)model.course_quantity];
    self.learnNumLab.text = [NSString stringWithFormat:@"%@人加入学习",[NSString transformIntegerShow:model.watch_times]];
}

- (void)setContentView{
    // 封面图
    PDImageView *coverImgView = [[PDImageView alloc] init];
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.layer.cornerRadius = 4.0f;
    coverImgView.clipsToBounds = YES;
    [self.contentView addSubview:coverImgView];
    self.coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(15);
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(115);
    }];
    
    // 标题
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:15];
    titleLab.textColor = kColorRGBValue(0x323232);
    titleLab.numberOfLines = 2;
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(120);
        make.top.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(titleLab.font.pointSize);
    }];
    
    // 副标题
    UILabel *subTitleLab = [UILabel by_init];
    [subTitleLab setBy_font:13];
    subTitleLab.textColor = kColorRGBValue(0x8f8f8f);
    subTitleLab.numberOfLines = 2;
    [self.contentView addSubview:subTitleLab];
    self.subTitleLab = subTitleLab;
    [subTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(120);
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(10);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(titleLab.font.pointSize);
    }];
    
    UILabel *freeLab = [UILabel by_init];
    freeLab.text = @"免费";
    [freeLab setBy_font:13];
    freeLab.textColor = kColorRGBValue(0xea6438);
    [self.contentView addSubview:freeLab];
    [freeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(120);
        make.top.mas_equalTo(subTitleLab.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(stringGetWidth(freeLab.text, 13));
        make.height.mas_equalTo(stringGetHeight(freeLab.text, 13));
    }];
    
    // 课程数
    UILabel *courseNumLab = [UILabel by_init];
    [courseNumLab setBy_font:13];
    courseNumLab.textAlignment = NSTextAlignmentLeft;
    courseNumLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:courseNumLab];
    self.courseNumLab = courseNumLab;
    [courseNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-5);
        make.left.mas_equalTo(120);
        make.height.mas_equalTo(courseNumLab.font.pointSize);
        make.width.mas_equalTo(40);
    }];
    
    UILabel *learnNumLab = [UILabel by_init];
    [learnNumLab setBy_font:11];
    learnNumLab.textColor = kColorRGBValue(0x8f8f8f);
    learnNumLab.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:learnNumLab];
    self.learnNumLab = learnNumLab;
    [learnNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.width.mas_equalTo(120);
        make.bottom.mas_equalTo(-5);
        make.height.mas_equalTo(learnNumLab.font.pointSize);
    }];
}

@end
