//
//  BYCourseHeaderView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYCourseHeaderView : UIView

/** 选择回调 */
@property (nonatomic ,copy) void (^didSelectAtIndex)(NSInteger index,NSInteger level);


@end

NS_ASSUME_NONNULL_END
