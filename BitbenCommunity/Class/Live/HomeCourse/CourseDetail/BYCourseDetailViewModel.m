//
//  BYCourseDetailViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCourseDetailViewModel.h"
#import "BYCourseDetailController.h"
#import "BYCourseIntroController.h"
#import "BYCourseCatalogController.h"

#import "BYCourseDetailHeaderView.h"
#import "BYCourseDetailTableHeaderView.h"
#import "HGSegmentedPageViewController.h"
#import "HGCenterBaseTableView.h"

#import "BYCourseDetailModel.h"

#define K_VC ((BYCourseDetailController *)S_VC)
@interface BYCourseDetailViewModel ()<HGSegmentedPageViewControllerDelegate, HGPageViewControllerDelegate,UITableViewDelegate,UITableViewDataSource>

/** playerView */
@property (nonatomic ,strong) BYCourseDetailHeaderView *headerView;
/** tableView */
@property (nonatomic ,strong) HGCenterBaseTableView *tableView;
/** 返回按钮 */
@property (nonatomic ,strong) UIButton *backBtn;
/** tableHeaderView */
@property (nonatomic ,strong) BYCourseDetailTableHeaderView *tableHeaderView;
/** tableFooterView */
@property (nonatomic ,strong) UIView *tableFooterView;
/** segmentControllView */
@property (nonatomic ,strong) HGSegmentedPageViewController *segmentedPageViewController;
/** 简介view */
@property (nonatomic ,strong) BYCourseIntroController *introController;
/** 目录view */
@property (nonatomic ,strong) BYCourseCatalogController *catalogController;
/** 详情数据 */
@property (nonatomic ,strong) BYCourseDetailModel *detailModel;
/** 课程数据 */
@property (nonatomic ,strong) NSArray *catalogData;
/** 是否可滑动 */
@property (nonatomic ,assign) BOOL cannotScroll;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;

@end

@implementation BYCourseDetailViewModel

- (void)setContentView{
    self.pageNum = 0;
    [self addPlayerHeaderView];
    [self addTableView];
    [self addBackBtn];
    [self loadRequestGetIntro:nil];
    [self loadRequestGetCatalog];
}

- (void)backBtnAction{
    [S_V_NC popViewControllerAnimated:YES];
}

- (void)reloadDetailData:(BOOL)isAutoPlay{
    [self.headerView reloadHeaderData:self.detailModel.videoModel autoPlay:isAutoPlay];
    [self.tableHeaderView reloadHeaderData:self.detailModel.videoModel];
    self.introController.introTableView.tableData = self.detailModel.courseIntroTableData;
}

- (void)reloadCatalogData{
    self.catalogController.catalogTableView.tableData = self.catalogData;
}

- (void)loadMoreCatalog{
    [self loadRequestGetCatalog];
}

#pragma mark - BYCommonTableViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat contentOffsetY = scrollView.contentOffset.y;
    CGFloat criticalPointOffsetY = 98;
    
    //利用contentOffset处理内外层scrollView的滑动冲突问题
    if (contentOffsetY >= criticalPointOffsetY) {
       
        self.cannotScroll = YES;
        scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        [self.segmentedPageViewController.currentPageViewController makePageViewControllerScroll:YES];
    } else {
   
        if (self.cannotScroll) {
            //“维持吸顶状态”
            scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        } else {
            /* 吸顶状态 -> 不吸顶状态
             * categoryView的子控制器的tableView或collectionView在竖直方向上的contentOffsetY小于等于0时，会通过代理的方式改变当前控制器self.canScroll的值；
             */
        }
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    [self.segmentedPageViewController.currentPageViewController makePageViewControllerScrollToTop];
    return YES;
}

#pragma mark - HGSegmentedPageViewControllerDelegate
- (void)segmentedPageViewControllerWillBeginDragging {
    self.tableView.scrollEnabled = NO;
}

- (void)segmentedPageViewControllerDidEndDragging {
    self.tableView.scrollEnabled = YES;
}

#pragma mark - HGPageViewControllerDelegate
- (void)pageViewControllerLeaveTop {
    self.cannotScroll = NO;
}

#pragma mark - request
// 获取简介
- (void)loadRequestGetIntro:(NSString *)videoId{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCourseIntro:K_VC.course_id videoId:videoId successBlock:^(id object) {
        @strongify(self);
        BYCourseDetailModel *model = object;
        self.detailModel = model;
        [self reloadDetailData:videoId.length ? YES : NO];
    } faileBlock:^(NSError *error) {
        
    }];
}

// 获取目录
- (void)loadRequestGetCatalog{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCourseCatalog:K_VC.course_id pageNum:self.pageNum successBlock:^(id object) {
        @strongify(self);
        [self.catalogController.catalogTableView endRefreshing];
        [self.catalogController.catalogTableView removeEmptyView];
        if (self.pageNum == 0) {
            self.catalogData = object;
            if (![object count]) {
                [self.catalogController.catalogTableView addEmptyView:@"暂无内容"];
            }else{
                BYCourseDetailCatalogModel *model = object[0];
                model.isPlaying = YES;
            }
        }else{
            if ([object count]) {
                NSMutableArray *tmpArr = [NSMutableArray arrayWithArray:self.catalogData];
                [tmpArr addObjectsFromArray:object];
                self.catalogData = tmpArr;
            }
            else return ;
        }
        self.pageNum ++;
        [self reloadCatalogData];
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.catalogController.catalogTableView endRefreshing];
    }];
}

#pragma mark - configUI

- (void)addPlayerHeaderView{
    BYCourseDetailHeaderView *headerView = [[BYCourseDetailHeaderView alloc] init];
    [S_V_VIEW addSubview:headerView];
    self.headerView = headerView;
    CGFloat height = kCommonScreenWidth*9.0f/16.0f;
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Top(height));
    }];
    @weakify(self);
    headerView.didSelectCatalogHandle = ^(NSString * _Nonnull videoId) {
        @strongify(self);
        for (BYCourseDetailCatalogModel *model in self.catalogData) {
            model.isPlaying = [model.video_id isEqualToString:videoId] ? YES : NO;
        }
        [self.catalogController.catalogTableView reloadData];
        [self loadRequestGetIntro:videoId];
    };
}

- (void)addTableView{
    HGCenterBaseTableView *tableView = [[HGCenterBaseTableView alloc] init];
    tableView.categoryViewHeight = 43.0f;
    tableView.delegate = self;
    tableView.tableHeaderView = self.tableHeaderView;
    tableView.tableFooterView = self.tableFooterView;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.showsHorizontalScrollIndicator = NO;
    [S_V_VIEW addSubview:tableView];
    self.tableView = tableView;
    CGFloat height = kCommonScreenWidth*9.0f/16.0f;
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(kSafe_Mas_Top(height), 0, 0, 0));
    }];
}

- (void)addBackBtn{
    self.backBtn = [UIButton by_buttonWithCustomType];
    [self.backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [S_V_VIEW addSubview:self.backBtn];
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(55);
        make.height.mas_equalTo(50);
        make.top.mas_equalTo(kSafe_Mas_Top(25));
    }];
    
    UIImageView *backImgView = [[UIImageView alloc] init];
    [backImgView by_setImageName:@"icon_center_bbt_back"];
    [S_V_VIEW addSubview:backImgView];
    [backImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(kSafe_Mas_Top(34));
        make.width.mas_equalTo(backImgView.image.size.width);
        make.height.mas_equalTo(backImgView.image.size.height);
    }];
}

- (BYCourseDetailTableHeaderView *)tableHeaderView{
    if (!_tableHeaderView) {
        _tableHeaderView = [[BYCourseDetailTableHeaderView alloc] init];
        _tableHeaderView.frame = CGRectMake(0, 0, kCommonScreenWidth, 98);
    }
    return _tableHeaderView;
}

- (UIView *)tableFooterView{
    if (!_tableFooterView) {
        CGFloat height = kCommonScreenWidth*9.0f/16.0f;
        _tableFooterView = [[UIView alloc] init];
        _tableFooterView.frame = CGRectMake(0, 0, kCommonScreenWidth, kCommonScreenHeight - kSafe_Mas_Top(height));
        [S_VC addChildViewController:self.segmentedPageViewController];
        [_tableFooterView addSubview:self.segmentedPageViewController.view];
        [self.segmentedPageViewController didMoveToParentViewController:S_VC];
        @weakify(self);
        [self.segmentedPageViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.edges.equalTo(self.tableFooterView);
        }];
    }
    return _tableFooterView;
}

- (HGSegmentedPageViewController *)segmentedPageViewController {
    if (!_segmentedPageViewController) {
        NSMutableArray *controllers = [NSMutableArray array];
        NSArray *titles = @[@"简介", @"目录"];
        for (int i = 0; i < titles.count; i++) {
            HGPageViewController *controller;
            if (i == 0) {
                controller = self.introController;
            } else {
                controller = self.catalogController;
            }
            controller.delegate = self;
            [controllers addObject:controller];
        }
        _segmentedPageViewController = [[HGSegmentedPageViewController alloc] init];
        _segmentedPageViewController.pageViewControllers = controllers;
        _segmentedPageViewController.categoryView.titles = titles;
        _segmentedPageViewController.categoryView.itemSpacing = 50.0f;
        _segmentedPageViewController.categoryView.height = 43;
        _segmentedPageViewController.categoryView.alignment = HGCategoryViewAlignmentCenter;
        _segmentedPageViewController.categoryView.originalIndex = 0;
        _segmentedPageViewController.categoryView.itemSpacing = 25;
        _segmentedPageViewController.categoryView.titleNomalFont = [UIFont systemFontOfSize:14];
        _segmentedPageViewController.categoryView.titleSelectedFont = [UIFont systemFontOfSize:14];
        _segmentedPageViewController.categoryView.titleNormalColor = kColorRGBValue(0x8f8f8f);
        _segmentedPageViewController.categoryView.titleSelectedColor = kColorRGBValue(0x323232);
        _segmentedPageViewController.categoryView.backgroundColor = [UIColor whiteColor];
        _segmentedPageViewController.categoryView.vernierHeight = 0.5;
        _segmentedPageViewController.categoryView.topBorder.hidden = YES;
        _segmentedPageViewController.categoryView.bottomBorder.backgroundColor = kColorRGBValue(0xdfdfdf);
        _segmentedPageViewController.delegate = self;
    }
    return _segmentedPageViewController;
}

- (BYCourseIntroController *)introController{
    if (!_introController) {
        _introController = [[BYCourseIntroController alloc] init];
        @weakify(self);
        _introController.didSelectRecommendHandle = ^(BYHomeCourseModel * _Nonnull model) {
            @strongify(self);
            K_VC.course_id = model.course_id;
            self.pageNum = 0;
            [self loadRequestGetIntro:nil];
            [self loadRequestGetCatalog];
        };
    }
    return _introController;
}

- (BYCourseCatalogController *)catalogController{
    if (!_catalogController) {
        _catalogController = [[BYCourseCatalogController alloc] init];
        @weakify(self);
        _catalogController.didSelectCatalogHandle = ^(BYCourseDetailCatalogModel * _Nonnull model) {
            @strongify(self);
            for (BYCourseDetailCatalogModel *tmpModel in self.catalogData) {
                tmpModel.isPlaying = tmpModel == model ? YES : NO;
            }
            [self.catalogController.catalogTableView reloadData];
            [self loadRequestGetIntro:model.video_id];
        };
        _catalogController.loadMoreDataHandle = ^{
            @strongify(self);
            [self loadMoreCatalog];
        };
    }
    return _catalogController;
}
    
@end
