//
//  BYCourseIntroController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/21.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCourseIntroController.h"
#import "BYHomeCourseModel.h"

@interface BYCourseIntroController ()<BYCommonTableViewDelegate>
/** 简介 */
@property (nonatomic ,strong) BYCommonTableView *introTableView;

@end

@implementation BYCourseIntroController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.introTableView];
    [self.introTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    // Do any additional setup after loading the view.
}
- (void)by_tableViewDidScroll:(UIScrollView *)scrollView{
    [self scrollViewDidScroll:scrollView];
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"recommendAction"]) {
        BYHomeCourseModel *model = param[@"model"];
        if (self.didSelectRecommendHandle) {
            self.didSelectRecommendHandle(model);
        }
    }
}

- (BYCommonTableView *)introTableView{
    if (!_introTableView) {
        _introTableView = [[BYCommonTableView alloc] init];
        _introTableView.group_delegate = self;
        _introTableView.showsVerticalScrollIndicator = NO;
        _introTableView.showsHorizontalScrollIndicator = NO;
    }
    return _introTableView;
}

@end
