//
//  BYCourseCatalogController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/21.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCourseCatalogController.h"

@interface BYCourseCatalogController ()<BYCommonTableViewDelegate>

/** 目录 */
@property (nonatomic ,strong) BYCommonTableView *catalogTableView;
@end

@implementation BYCourseCatalogController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.catalogTableView];
    [self.catalogTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    // Do any additional setup after loading the view.
}

- (void)loadMoreData{
    if (self.loadMoreDataHandle) {
        self.loadMoreDataHandle();
    }
}

#pragma mark - BYCommonTableViewDelegate

- (void)by_tableViewDidScroll:(UIScrollView *)scrollView{
    [self scrollViewDidScroll:scrollView];
}

- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BYCourseDetailCatalogModel *model = tableView.tableData[indexPath.row];
    if (self.didSelectCatalogHandle) {
        self.didSelectCatalogHandle(model);
    }
}

- (BYCommonTableView *)catalogTableView{
    if (!_catalogTableView) {
        _catalogTableView = [[BYCommonTableView alloc] init];
        _catalogTableView.backgroundColor = kColorRGBValue(0xeeeeee);
        _catalogTableView.group_delegate = self;
        _catalogTableView.showsVerticalScrollIndicator = NO;
        _catalogTableView.showsHorizontalScrollIndicator = NO;
        [_catalogTableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
        UIView *footerView = [UIView by_init];
        [footerView setBackgroundColor:[UIColor whiteColor]];
        footerView.frame = CGRectMake(0, 0, kCommonScreenWidth, 40);
        _catalogTableView.tableFooterView = footerView;
    }
    return _catalogTableView;
}

@end
