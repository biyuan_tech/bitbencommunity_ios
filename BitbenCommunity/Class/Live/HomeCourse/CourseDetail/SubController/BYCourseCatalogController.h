//
//  BYCourseCatalogController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/21.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "HGPageViewController.h"
#import "BYCourseDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYCourseCatalogController : HGPageViewController

/** 目录 */
@property (nonatomic ,strong,readonly) BYCommonTableView *catalogTableView;
/** 点击回调 */
@property (nonatomic ,copy) void (^didSelectCatalogHandle)(BYCourseDetailCatalogModel *model);
/** 加载更多目录 */
@property (nonatomic ,copy) void (^loadMoreDataHandle)(void);


@end

NS_ASSUME_NONNULL_END
