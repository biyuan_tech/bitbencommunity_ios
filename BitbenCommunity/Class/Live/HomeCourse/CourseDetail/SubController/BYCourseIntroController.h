//
//  BYCourseIntroController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/21.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "HGPageViewController.h"
#import "BYHomeCourseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYCourseIntroController : HGPageViewController

/** 简介 */
@property (nonatomic ,strong,readonly) BYCommonTableView *introTableView;
/** 推荐点击回调 */
@property (nonatomic ,copy) void (^didSelectRecommendHandle)(BYHomeCourseModel *model);

@end

NS_ASSUME_NONNULL_END
