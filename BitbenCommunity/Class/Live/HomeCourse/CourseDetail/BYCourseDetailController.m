//
//  BYCourseDetailController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCourseDetailController.h"
#import "BYCourseDetailViewModel.h"

@interface BYCourseDetailController ()

@end

@implementation BYCourseDetailController


- (Class)getViewModelClass{
    return [BYCourseDetailViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    self.extendedLayoutIncludesOpaqueBars = YES;
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end
