//
//  BYCourseRecommendCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCourseRecommendCell.h"
#import "BYCourseRecommendSingleCell.h"
#import "BYCourseDetailModel.h"
#import "BYHomeCourseModel.h"

@interface BYCourseRecommendCell ()<UICollectionViewDelegate,UICollectionViewDataSource>

/** collection */
@property (nonatomic ,strong) UICollectionView *collectionView;
/** model */
@property (nonatomic ,strong) BYCourseDetailModel *model;

@end

@implementation BYCourseRecommendCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYCourseDetailModel *model = object[indexPath.row];
    self.indexPath = indexPath;
    self.model = model;
    [self.collectionView reloadData];
    if (model.recommend_course_list.count) {
        [self.collectionView removeEmptyView];
    }else{
        [self.collectionView addEmptyView:@"暂无内容"];
    }
}

#pragma mark - UICollectionViewDelegate/DataScoure
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.model.recommend_course_list.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BYCourseRecommendSingleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell setContentWithObject:self.model.recommend_course_list[indexPath.row] indexPath:indexPath];
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 15, 0, 15);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    BYHomeCourseModel *model = self.model.recommend_course_list[indexPath.row];
    [self sendActionName:@"recommendAction" param:@{@"model":model} indexPath:self.indexPath];
}

- (void)setContentView{
    UIView *topView = [UIView by_init];
    [topView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.contentView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(8);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    titleLab.text = @"为你推荐";
    [titleLab setBy_font:16];
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(28);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(titleLab.font.pointSize);
    }];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    CGFloat space = iPhone5 ? 8 : 13;
    CGFloat width = (kCommonScreenWidth - 30 - space*2)/3;
    layout.itemSize = CGSizeMake(width, 210);
    layout.minimumInteritemSpacing = space;
    layout.minimumLineSpacing = 0.0f;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    UICollectionView *collection = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [collection registerClass:[BYCourseRecommendSingleCell class] forCellWithReuseIdentifier:@"cell"];
    [collection setBackgroundColor:[UIColor whiteColor]];
    collection.delegate = self;
    collection.dataSource = self;
    collection.scrollEnabled = NO;
    [self.contentView addSubview:collection];
    self.collectionView = collection;
    [collection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(60, 0, 0, 0));
    }];
    
}

@end
