//
//  BYCourseCatalogCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCourseCatalogCell.h"
#import <SDWebImage/UIImage+GIF.h>
//#import "FLAnimatedImageView.h"
#import <SDWebImage/FLAnimatedImageView+WebCache.h>
#import "BYCourseDetailModel.h"

@interface BYCourseCatalogCell ()

/** title */
@property (nonatomic ,strong) UILabel *titleLab;
/** 学习人数 */
@property (nonatomic ,strong) UILabel *learnNumLab;
/** 学习中动画 */
@property (nonatomic ,strong) FLAnimatedImageView *learnImgView;
/** 封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;


@end

@implementation BYCourseCatalogCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYCourseDetailCatalogModel *model = object[indexPath.row];
    self.titleLab.text = [NSString stringWithFormat:@"%i%@",(int)indexPath.row + 1,model.video_title];
    self.titleLab.textColor = model.isPlaying ? kColorRGBValue(0xea6438) : kColorRGBValue(0x323232);
    [self.coverImgView uploadHDImageWithURL:model.video_cover callback:nil];
    self.learnNumLab.text = [NSString stringWithFormat:@"%@人学过",[NSString transformIntegerShow:model.watch_times]];
    self.learnImgView.hidden = !model.isPlaying;
}

- (void)setContentView{
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    titleLab.numberOfLines = 2.0f;
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(12);
        make.right.mas_equalTo(-150);
        make.height.mas_greaterThanOrEqualTo(titleLab.font.pointSize);
    }];
    
    UILabel *learnNumLab = [UILabel by_init];
    [learnNumLab setBy_font:11];
    learnNumLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:learnNumLab];
    self.learnNumLab = learnNumLab;
    [learnNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(learnNumLab.font.pointSize);
    }];
    
    PDImageView *coverImgView = [PDImageView by_init];
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.layer.cornerRadius = 5.0f;
    coverImgView.clipsToBounds = YES;
    [self.contentView addSubview:coverImgView];
    self.coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(80);
        make.top.mas_equalTo(12);
    }];
    
    NSString *filePath = [[NSBundle bundleWithPath:[[NSBundle mainBundle] bundlePath]] pathForResource:@"course_play_animation.gif" ofType:nil];
    NSData *imageData = [NSData dataWithContentsOfFile:filePath];

    FLAnimatedImageView *learnImgView = [FLAnimatedImageView by_init];
    learnImgView.animatedImage = [FLAnimatedImage animatedImageWithGIFData:imageData];
    learnImgView.hidden = YES;
    [self.contentView addSubview:learnImgView];
    self.learnImgView = learnImgView;
    [learnImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-150);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(10);
        make.height.mas_equalTo(12);
    }];
}
@end
