//
//  BYCourseRecommendSingleCell.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYHomeCourseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYCourseRecommendSingleCell : UICollectionViewCell

- (void)setContentWithObject:(BYHomeCourseModel *)object indexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
