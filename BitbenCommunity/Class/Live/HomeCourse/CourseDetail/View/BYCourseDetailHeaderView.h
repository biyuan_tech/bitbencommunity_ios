//
//  BYCourseDetailHeaderView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYCourseDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYCourseDetailHeaderView : UIView

/** 点击上一集下一集回调 */
@property (nonatomic ,copy) void (^didSelectCatalogHandle)(NSString *videoId);


- (void)reloadHeaderData:(BYCourseDetailCatalogModel *)model autoPlay:(BOOL)autoPlay;

@end

NS_ASSUME_NONNULL_END
