//
//  BYCourseIntroCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCourseIntroCell.h"
#import "BYCourseDetailModel.h"

@interface BYCourseIntroCell ()<WKUIDelegate,
WKNavigationDelegate>

/** webView */
@property (nonatomic ,strong) WKWebView *webView;
/** model */
@property (nonatomic ,strong) BYCourseDetailModel *model;

@end

@implementation BYCourseIntroCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYCourseDetailModel *model = object[indexPath.row];
    self.model = model;
    if (!model.isLoad && model.course_intro.length) {
        NSString *documentDir = [[NSBundle mainBundle] pathForResource:@"layui.css" ofType:nil];//
        NSString *resultStr = [NSString stringWithContentsOfFile:documentDir encoding:NSUTF8StringEncoding error:nil];

        NSString *smartUrl = @"";
        smartUrl = @"<meta content=\"width=device-width, initial-scale=1.0, maximum-scale=3.0, user-scalable=0;\" name=\"viewport\" /> ";
        smartUrl = [smartUrl stringByAppendingString:resultStr];
        smartUrl = [smartUrl stringByAppendingString:@"<div class=\"ql-container ql-snow\"><div class=\"ql-editor\">"];
        smartUrl = [smartUrl stringByAppendingString:model.course_intro];
        smartUrl = [smartUrl stringByAppendingString:@"<div id=\"testDiv\" style = \"height:0px; width:1px\">"];
        smartUrl = [smartUrl stringByAppendingString:@"</div>"];
        smartUrl = [smartUrl stringByAppendingString:@"</div>"];
        smartUrl = [smartUrl stringByAppendingString:@"</div>"];

        [_webView loadHTMLString:smartUrl baseURL:[NSURL fileURLWithPath: [[NSBundle mainBundle]  bundlePath]]];
    }
}


#pragma mark - wkWebViewDelegate
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    @weakify(self);
    [webView evaluateJavaScript:@"document.getElementById(\"testDiv\").offsetTop"completionHandler:^(id _Nullable result,NSError * _Nullable error) {
        @strongify(self);
        //获取页面高度，并重置webview的frame
        CGFloat height  = [result doubleValue] + 50;
        self.model.cellHeight = height;
        self.model.isLoad = YES;
        [self.tableView reloadData];
    }];
}

#pragma mark - congifUI
- (void)setContentView{
    WKWebView *webView = [[WKWebView alloc] init];
    webView.UIDelegate = self;
    webView.navigationDelegate = self;
    webView.scrollView.scrollEnabled = NO;
    webView.scrollView.alwaysBounceVertical = YES;
    [self.contentView addSubview:webView];
    self.webView = webView;
    [webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

@end
