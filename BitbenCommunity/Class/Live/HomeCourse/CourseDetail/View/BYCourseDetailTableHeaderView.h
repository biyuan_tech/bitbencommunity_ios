//
//  BYCourseDetailTableHeaderView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYCourseDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYCourseDetailTableHeaderView : UIView



- (void)reloadHeaderData:(BYCourseDetailCatalogModel *)model;

@end

NS_ASSUME_NONNULL_END
