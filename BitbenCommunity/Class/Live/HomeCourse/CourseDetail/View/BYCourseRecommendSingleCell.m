//
//  BYCourseRecommendSingleCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCourseRecommendSingleCell.h"

@interface BYCourseRecommendSingleCell ()

/** 封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 标题 */
@property (nonatomic ,strong) UILabel *titleLab;
/** 学习人数 */
@property (nonatomic ,strong) UILabel *learnNumLab;


@end

@implementation BYCourseRecommendSingleCell


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(BYHomeCourseModel *)object indexPath:(NSIndexPath *)indexPath{
    [_coverImgView uploadHDImageWithURL:object.course_out_cover callback:NULL];
    _titleLab.text = object.course_name;
    _learnNumLab.text = [NSString stringWithFormat:@"%@人在学习",[NSString transformIntegerShow:object.watch_times]];
}

- (void)setContentView{
    PDImageView *coverImgView = [PDImageView by_init];
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.layer.cornerRadius = 4.0f;
    coverImgView.clipsToBounds = YES;
    [self.contentView addSubview:coverImgView];
    self.coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(140);
    }];
    
    CGFloat space = iPhone5 ? 8 : 13;
    CGFloat width = (kCommonScreenWidth - 30 - space*2)/3;
    UIView *maskView = [UIView by_init];
    [maskView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.5]];
    [maskView layerCornerRadius:4.0f byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight size:CGSizeMake(width, 26)];
    [self.contentView addSubview:maskView];
    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(26);
        make.top.mas_equalTo(114);
    }];
    
    UILabel *learnNumLab = [UILabel by_init];
    [learnNumLab setBy_font:11];
    learnNumLab.textColor = [UIColor whiteColor];
    learnNumLab.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:learnNumLab];
    self.learnNumLab = learnNumLab;
    [learnNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(114);
        make.height.mas_equalTo(26);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    titleLab.textColor = kColorRGBValue(0x323232);
    titleLab.numberOfLines = 2.0f;
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(150);
        make.height.mas_greaterThanOrEqualTo(18);
    }];

}

@end
