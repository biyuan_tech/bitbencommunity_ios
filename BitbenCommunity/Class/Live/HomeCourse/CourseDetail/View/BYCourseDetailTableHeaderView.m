//
//  BYCourseDetailTableHeaderView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/16.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCourseDetailTableHeaderView.h"


@interface BYCourseDetailTableHeaderView ()

/** title */
@property (nonatomic ,strong) UILabel *titleLab;
/** 学习人数 */
@property (nonatomic ,strong) UILabel *learnNumLab;


@end

@implementation BYCourseDetailTableHeaderView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)reloadHeaderData:(BYCourseDetailCatalogModel *)model{
    self.titleLab.text = model.video_title;
    self.learnNumLab.text = [NSString stringWithFormat:@"%@人加入学习",[NSString transformIntegerShow:model.watch_times]];
}

- (void)reloadHeaderData{
    self.titleLab.text = @"1、全国首个区域司法链成立长三角法院引入蚂蚁区法链成立长三角法院引入蚂";
    self.learnNumLab.text = @"5639人加入学习";
}

- (void)setContentView{
    self.titleLab = [UILabel by_init];
    self.titleLab.numberOfLines = 2;
    [self.titleLab setBy_font:15];
    self.titleLab.textColor = kColorRGBValue(0x323232);
    [self addSubview:self.titleLab];
    @weakify(self);
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.top.mas_equalTo(10);
        make.centerX.mas_equalTo(0);
        make.height.mas_greaterThanOrEqualTo(self.titleLab.font.pointSize);
    }];
    
    UILabel *freeLab = [UILabel by_init];
    freeLab.text = @"免费";
    [freeLab setBy_font:13];
    freeLab.textColor = kColorRGBValue(0xea6438);
    [self addSubview:freeLab];
    [freeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.bottom.mas_equalTo(-20);
        make.width.mas_equalTo(stringGetWidth(freeLab.text, 13));
        make.height.mas_equalTo(stringGetHeight(freeLab.text, 13));
    }];
    
    UILabel *learnNumLab = [UILabel by_init];
    [learnNumLab setBy_font:11];
    learnNumLab.textColor = kColorRGBValue(0x8f8f8f);
    learnNumLab.textAlignment = NSTextAlignmentRight;
    [self addSubview:learnNumLab];
    self.learnNumLab = learnNumLab;
    [learnNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.width.mas_equalTo(120);
        make.centerY.mas_equalTo(freeLab.mas_centerY).mas_offset(0);
        make.height.mas_equalTo(learnNumLab.font.pointSize);
    }];
    
    
    UIView *bottomView = [UIView by_init];
    bottomView.backgroundColor = kColorRGBValue(0xf2f4f5);
    [self addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(8);
    }];
    
}

@end
