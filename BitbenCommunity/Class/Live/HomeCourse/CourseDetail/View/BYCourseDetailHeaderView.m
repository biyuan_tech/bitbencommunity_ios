//
//  BYCourseDetailHeaderView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCourseDetailHeaderView.h"
#import "BYCommonPlayerView.h"

@interface BYCourseDetailHeaderView ()<SuperPlayerDelegate>

/** 播放器 */
@property (nonatomic ,strong) BYCommonPlayerView *playerView;
/** 播放按钮 */
@property (nonatomic ,strong) UIButton *playBtn;
/** 遮罩 */
@property (nonatomic ,strong) UIView *maskView;
/** model */
@property (nonatomic ,strong) BYCourseDetailCatalogModel *model;


@end

@implementation BYCourseDetailHeaderView

- (void)dealloc
{
    [_playerView resetPlayer];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)reloadHeaderData:(BYCourseDetailCatalogModel *)model autoPlay:(BOOL)autoPlay{
    self.model = model;
    NSString *imageurl = [model.video_cover getFullImageUploadUrl];
    [self.playerView resetPlayer];
    [self.playerView.coverImageView sd_setImageWithURL:[NSURL URLWithString:imageurl]];
    [self.playerView setWatchNum:model.watch_times];
    if (autoPlay) {
        self.playerView.autoPlay = autoPlay;
        [self playBtnAction];
    }else{
        self.playBtn.hidden = NO;
        self.maskView.hidden = NO;
    }
}

- (void)playBtnAction{
    if (!self.model.video_url.length) {
        showToastView(@"当前课程暂无视频", [BYTabbarViewController sharedController].currentController.view);
        return;
    }
    [self.playerView playVideoWithUrl:self.model.video_url title:self.model.video_title];
    self.playerView.back_Enabled = [self.model.video_id isEqualToString:self.model.last] ? NO : YES;
    self.playerView.forward_Enabled = [self.model.video_id isEqualToString:self.model.next] ? NO : YES;
    self.playBtn.hidden = YES;
    self.maskView.hidden = YES;
}

#pragma mark - SuperPlayerDelegate

- (void)superPlayerBackAction:(SuperPlayerView *)player{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    // 是竖屏时候响应关
    if (orientation == UIInterfaceOrientationPortrait &&
        (self.playerView.state == StatePlaying)) {
 
    } else {
        [self.playerView resetPlayer];  //非常重要
    }
}

- (void)superPlayerDidStart:(SuperPlayerView *)player{
    PDLog(@"开播");
}

- (void)superPlayerGoForwardAction:(SuperPlayerView *)player{
    [player pause];
    if (self.didSelectCatalogHandle) {
        self.didSelectCatalogHandle(self.model.next);
    }
    PDLog(@"%s",__func__);
}

- (void)superPlayerGoBackAction:(SuperPlayerView *)player{
    [player pause];
    if (self.didSelectCatalogHandle) {
        self.didSelectCatalogHandle(self.model.last);
    }
    PDLog(@"%s",__func__);
}

- (void)superPlayerDidEnd:(SuperPlayerView *)player{
    PDLog(@"%s",__func__);
}

- (void)superPlayerError:(SuperPlayerView *)player errCode:(int)code errMessage:(NSString *)why{
    PDLog(@"%s",__func__);
}

- (void)setContentView{
    
    UIView *fatherView = [UIView by_init];
    fatherView.clipsToBounds = YES;
    [self addSubview:fatherView];
    [fatherView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(kSafeAreaInsetsTop, 0, 0, 0));
    }];
    
    BYCommonPlayerView *playerView = [[BYCommonPlayerView alloc] init];
    playerView.enableFloatWindow = NO;
    playerView.fatherView = fatherView;
    playerView.delegate = self;
    playerView.playerConfig.type = PLAYER_TYPE_COURSE;
//    [playerView.coverImageView by_setImageName:@"defult_liveroom_bg_02"];
    playerView.autoPlay = YES;
    self.playerView = playerView;
    
    self.maskView = [UIView by_init];
    [self.maskView setBackgroundColor:kColorRGB(50, 50, 50, 0.3)];
    [self addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    self.playBtn = [UIButton by_buttonWithCustomType];
    [self.playBtn setBy_imageName:@"livehome_play" forState:UIControlStateNormal];
    [self.playBtn addTarget:self action:@selector(playBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.playBtn];
    [self.playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(50);
        make.center.mas_equalTo(0);
    }];
    
  
}



@end
