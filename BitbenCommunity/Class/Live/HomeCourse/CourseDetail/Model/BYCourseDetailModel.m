//
//  BYCourseDetailModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCourseDetailModel.h"

@implementation BYCourseDetailModel

+ (BYCourseDetailModel *)getDetailModel:(NSDictionary *)respondDic{
    BYCourseDetailModel *model = [[BYCourseDetailModel alloc] init];
    BYCourseDetailCatalogModel *videoModel = [BYCourseDetailCatalogModel mj_objectWithKeyValues:respondDic];
    model.videoModel = videoModel;
    model.course_intro = respondDic[@"course_intro"];
    NSArray *course_list = respondDic[@"recommend_course_list"];
    NSMutableArray *courseData = [NSMutableArray array];
    if (course_list.count) {
        for (NSDictionary *dic in course_list) {
            BYHomeCourseModel *courseModel = [BYHomeCourseModel mj_objectWithKeyValues:dic];
            [courseData addObject:courseModel];
        }
    }
    model.recommend_course_list = courseData;
    
    NSMutableArray *tableData = [NSMutableArray array];
    for (int i = 0; i < 2; i ++) {
        BYCourseDetailModel *tmpModel = [[BYCourseDetailModel alloc] init];
        tmpModel.course_intro = model.course_intro;
        tmpModel.recommend_course_list = model.recommend_course_list;
        tmpModel.cellHeight = i == 0 ? 0 : 10;
        if (i == 1) {
            NSInteger section = !tmpModel.recommend_course_list.count ? 0 : ceil(tmpModel.recommend_course_list.count/3.0);
            CGFloat height = 60 + 210*section;
            tmpModel.cellHeight = height < 450 ? 450 : height;
        }
        tmpModel.cellString = i == 0 ? @"BYCourseIntroCell" : @"BYCourseRecommendCell";
        [tableData addObject:tmpModel];
    }
    model.courseIntroTableData = tableData;
    
    return model;
}

+ (NSArray *)getMainTableData{
    BYCourseDetailModel *model = [[BYCourseDetailModel alloc] init];
    model.cellString = @"BYCourseDetailCell";
    CGFloat topHeight = kSafe_Mas_Top(kCommonScreenWidth*9.0f/16.0f) + 98 + 55;
    model.cellHeight = kCommonScreenHeight - topHeight;
    return @[model];
}

@end

@implementation BYCourseDetailCatalogModel

+ (NSArray *)getCatalogTableData:(NSArray *)respondData{
    NSMutableArray *data = [NSMutableArray array];
    for (NSDictionary *dic in respondData) {
        BYCourseDetailCatalogModel *model = [BYCourseDetailCatalogModel mj_objectWithKeyValues:dic];
        model.cellString = @"BYCourseCatalogCell";
        model.cellHeight = 92;
        [data addObject:model];
    }
    return data;
}

@end
