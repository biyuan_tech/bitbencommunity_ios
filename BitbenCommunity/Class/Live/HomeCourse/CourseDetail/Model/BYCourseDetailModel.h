//
//  BYCourseDetailModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"
#import "BYHomeCourseModel.h"

NS_ASSUME_NONNULL_BEGIN

@class BYCourseDetailCatalogModel;

@interface BYCourseDetailModel : BYCommonModel

/** 视频详情 */
@property (nonatomic ,strong) BYCourseDetailCatalogModel *videoModel;
/** 课程简介 */
@property (nonatomic ,copy) NSString *course_intro;
/** 推荐 */
@property (nonatomic ,strong) NSArray <BYHomeCourseModel *>*recommend_course_list;

/** 界面列表数据 */
@property (nonatomic ,strong) NSArray *courseIntroTableData;
/** 是否被加载（针对简介加载） */
@property (nonatomic ,assign) BOOL isLoad;


+ (BYCourseDetailModel *)getDetailModel:(NSDictionary *)respondDic;
+ (NSArray *)getMainTableData;

@end

@interface BYCourseDetailCatalogModel : BYCommonModel

/** 课程id */
@property (nonatomic ,copy) NSString *course_id;
/** 视频id */
@property (nonatomic ,copy) NSString *video_id;
/** 标题 */
@property (nonatomic ,copy) NSString *video_title;
/** 视频地址 */
@property (nonatomic ,copy) NSString *video_url;
/** 学习人数 */
@property (nonatomic ,assign) NSInteger watch_times;
/** 封面图 */
@property (nonatomic ,copy) NSString *video_cover;
/** 上一个视频id */
@property (nonatomic ,copy) NSString *last;
/** 下一条视频id */
@property (nonatomic ,copy) NSString *next;
/** 课程集数位置 */
@property (nonatomic ,assign) NSInteger order;


/** 播放状态 */
@property (nonatomic ,assign) BOOL isPlaying;

+ (NSArray *)getCatalogTableData:(NSArray *)respondData;

@end

NS_ASSUME_NONNULL_END
