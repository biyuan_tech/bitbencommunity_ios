//
//  BYLiveSetViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/10.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveSetViewModel.h"
// controller
#import "BYLiveEditBgImgController.h"
#import "BYLiveSetController.h"
#import "ArticleDetailHuatiListViewController.h"
#import "BYPushFlowTutorialController.h"
// model
#import "BYLiveSetModel.h"
#import "BYCommonLiveModel.h"

#import "BYAVMediaUtil.h"

#import "BYEditingTextView.h"
// request

#define S_DM ((BYLiveSetModel *)self.dataModel)
#define S_LIVESET_VC ((BYLiveSetController *)S_VC)
@interface BYLiveSetViewModel ()<BYCommonTableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic ,strong) BYCommonTableView *tableView;
/** 主题名称编辑 */
@property (nonatomic ,strong) BYEditingTextView *themeEditingView;
/** 主讲人编辑 */
@property (nonatomic ,strong) BYEditingTextView *speakerEditingView;
/** 直播简介 */
@property (nonatomic ,strong) BYEditingTextView *introEditingView;

@end

@implementation BYLiveSetViewModel

- (Class)getDataModelClass{
    return [BYLiveSetModel class];
}

- (void)setContentView{
    [S_V_VIEW addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    _tableView.tableData = [S_DM getTableData:S_LIVESET_VC.model];
}

#pragma mark - customMethod

- (void)changeUserLogo{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:0];
    @weakify(self);
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"立即拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [self openCamera];
        
    }];
    UIAlertAction *action2= [UIAlertAction actionWithTitle:@"相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [self openPhotoLibrary];
    }];
    [alertController addAction:cancleAction];
    [alertController addAction:action2];
    [alertController addAction:action1];
    [self.viewController presentViewController:alertController animated:YES completion:nil];
}

// 更新列表头像数据
- (void)reloadUserlogo:(UIImage *)image{
    NSDictionary *dic = _tableView.tableData[0];
    BYLiveSetCellModel *model = dic.allValues[0][0];
    model.rightlogo = image;
    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];

}

// 打开相机
- (void)openCamera{
    // 打开系统相机拍照
    // 相机权限判断
    if(![BYAVMediaUtil verifyAVAuthorization:BYAVMedioTypeVideo]) return;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *cameraIPC = [[UIImagePickerController alloc] init];
        cameraIPC.delegate = self;
        cameraIPC.allowsEditing = YES;
        cameraIPC.sourceType = UIImagePickerControllerSourceTypeCamera;

        [self.viewController presentViewController:cameraIPC animated:YES completion:nil];
        return;
    }
}

// 打开相册
- (void)openPhotoLibrary{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.navigationBar.translucent=NO;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self.viewController presentViewController:imagePicker animated:YES completion:nil];
        return;
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    UIImage *cutImage = [image cutImageSize:CGSizeMake(kCommonScreenWidth, kCommonScreenWidth)];
    @weakify(self);
    OSSFileModel *fileModel = [[OSSFileModel alloc] init];
    fileModel.objcName  = [NSString stringWithFormat:@"%@-speaker_head_img",[NSDate by_stringFromDate:[NSDate by_date] dateformatter:@"yyyy-MM-dd-HH:mm:ss"]];
    fileModel.objcImage = cutImage;
    [[OSSManager sharedUploadManager] uploadImageManagerWithImageList:[@[fileModel] copy] withUrlBlock:^(NSArray *imgUrlArr) {
        @strongify(self);
        if (!imgUrlArr.count) return ;
        @weakify(self);
        [self loadRequestUpdateLiveRoom:@{@"speaker_head_img":imgUrlArr[0]} cb:^{
            @strongify(self);
            if (!self) return ;
            S_LIVESET_VC.model.speaker_head_img = imgUrlArr[0];
            if (S_LIVESET_VC.reloadLiveRoomHandel) {
                S_LIVESET_VC.reloadLiveRoomHandel();
            }
        }];
    }];
    
    //如果是相机拍照，则保存到相册
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - BYCommonTableViewDelegate

- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForHeaderInSection:(NSInteger)section{
    return 12;
}

- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [UIView by_init];
    sectionView.backgroundColor = [UIColor clearColor];
    return sectionView;
}

- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYLiveSetCellModel *model = dic.allValues[0][indexPath.row];
        switch (indexPath.row) {
            case 0: // 设置头像
                [self changeUserLogo];
                break;
            case 1: // 修改主题名称
                [self.themeEditingView showAnimation];
                break;
            case 2: // 填写主讲人
                [self.speakerEditingView showAnimation];
                break;
            default: // 选择话题
            {
                ArticleDetailHuatiListViewController *huatiListVC = [[ArticleDetailHuatiListViewController alloc]init];
                huatiListVC.transferSelectedHuatiArr = model.topicArr;
                huatiListVC.maxCount = 3;
                @weakify(self);
                [huatiListVC actionClickWithSelectedBlock:^(NSArray *selectedArr) {
                    @strongify(self);
                    model.topicArr = selectedArr;
                    [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
                    [self updateTopic:selectedArr];
                }];
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:huatiListVC];
                [S_V_NC presentViewController:nav animated:YES completion:NULL];
            }
//                huatiListVC.transferSelectedHuatiArr = self.tagsMutableArr;
                break;
        }
    }
    else if (indexPath.section == 1){
        if (indexPath.row == 0) { // 直播封面图
            BYLiveEditBgImgController *editBgImgController = [[BYLiveEditBgImgController alloc] init];
            editBgImgController.edit_type = EDIT_IMG_TYPE_LIVE;
            editBgImgController.model = S_LIVESET_VC.model;
            editBgImgController.reloadLiveRoomHandel = S_LIVESET_VC.reloadLiveRoomHandel;
            [S_V_NC pushViewController:editBgImgController animated:YES];
        }
        else { // 直播简介
            [self.introEditingView showAnimation];
        }
    }
    else if (indexPath.section == 2) // 直播宣传图
    {
        BYLiveEditBgImgController *editBgImgController = [[BYLiveEditBgImgController alloc] init];
        editBgImgController.edit_type = EDIT_IMG_TYPE_PUBLICITY;
        editBgImgController.model = S_LIVESET_VC.model;
        editBgImgController.reloadLiveRoomHandel = S_LIVESET_VC.reloadLiveRoomHandel;
        [S_V_NC pushViewController:editBgImgController animated:YES];
    }
    else{           // 推流设置
        BYPushFlowTutorialController *pushFlowTutorialController = [[BYPushFlowTutorialController alloc] init];
        pushFlowTutorialController.live_record_id = S_LIVESET_VC.model.live_record_id;
        [S_V_NC pushViewController:pushFlowTutorialController animated:YES];
    }
}

#pragma mark - requestMethod
- (void)updateTopic:(NSArray *)topics{
    NSString *topic = @"";
    if (topics.count) {
        for (int i = 0; i < topics.count; i ++) {
            NSString *str = topics[i];
            topic = [topic stringByAppendingString:[NSString stringWithFormat:@"%@%@",str,i == topics.count - 1 ? @"" : @","]];
        }
    }
    @weakify(self);
    [self loadRequestUpdateLiveRoom:@{@"topic_content":topic} cb:^{
        @strongify(self);
        S_LIVESET_VC.model.topic_content = topics;
    }];
}

- (void)loadRequestUpdateLiveRoom:(NSDictionary *)param cb:(void(^)(void))cb{
    @weakify(self);
    BYToastView *toastView = [BYToastView toastViewPresentLoading];
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveSet:S_LIVESET_VC.model.live_record_id param:param successBlock:^(id object) {
        @strongify(self);
        [toastView dissmissToastView];
        if (cb) cb();
        if (S_LIVESET_VC.reloadLiveRoomHandel) {
            S_LIVESET_VC.reloadLiveRoomHandel();
        }
        self.tableView.tableData = [S_DM getTableData:S_LIVESET_VC.model];
        [self.tableView reloadData];
        showToastView(@"修改成功", S_V_VIEW);
    } faileBlock:^(NSError *error) {
        showToastView(@"修改失败", S_V_VIEW);
        [toastView dissmissToastView];
    }];
}

#pragma mark - initMethod

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.backgroundColor = kBgColor_248;
        _tableView.group_delegate = self;
    }
    return _tableView;
}

- (BYEditingTextView *)themeEditingView{
    if (!_themeEditingView) {
        _themeEditingView = [[BYEditingTextView alloc] initWithFathureView:kCommonWindow editingType:BYEditingTextViewTypeNormal];
        _themeEditingView.placeholder = @"请输入主题名称...";
        _themeEditingView.textView.text = nullToEmpty(S_LIVESET_VC.model.live_title);
        @weakify(self);
        _themeEditingView.didConfirmHandle = ^(NSString *text) {
            @strongify(self);
            @weakify(self);
            [self loadRequestUpdateLiveRoom:@{@"live_title":text} cb:^{
                @strongify(self);
                S_LIVESET_VC.model.live_title = text;
            }];;
        };
    }
    return _themeEditingView;
}

- (BYEditingTextView *)speakerEditingView{
    if (!_speakerEditingView) {
        _speakerEditingView = [[BYEditingTextView alloc] initWithFathureView:kCommonWindow editingType:BYEditingTextViewTypeNormal];
        _speakerEditingView.placeholder = @"请输入主讲人...";
        _speakerEditingView.textView.text = nullToEmpty(S_LIVESET_VC.model.speaker);
        @weakify(self);
        _speakerEditingView.didConfirmHandle = ^(NSString *text) {
            @strongify(self);
            @weakify(self);
            [self loadRequestUpdateLiveRoom:@{@"speaker":text} cb:^{
                @strongify(self);
                S_LIVESET_VC.model.speaker = text;
            }];
        };
    }
    return _speakerEditingView;
}

- (BYEditingTextView *)introEditingView{
    if (!_introEditingView) {
        _introEditingView = [[BYEditingTextView alloc] initWithFathureView:kCommonWindow editingType:BYEditingTextViewTypeIntro];
        _introEditingView.placeholder = @"请输入直播简介...";
        // 有简介则加载否则加载默认
        if (S_LIVESET_VC.model.live_intro.length) {
            _introEditingView.textView.text = S_LIVESET_VC.model.live_intro;
        }
        else
        {
            _introEditingView.textView.text = @"【主持人简介】\n\n\n【直播简介】\n\n\n【直播概要】";
        }
        _introEditingView.maxlength = 1000;
        @weakify(self);
        _introEditingView.didConfirmHandle = ^(NSString *text) {
            @strongify(self);
            @weakify(self);
            [self loadRequestUpdateLiveRoom:@{@"live_intro":text} cb:^{
                @strongify(self);
                S_LIVESET_VC.model.live_intro = text;
            }];
        };
    }
    return _introEditingView;
}
@end
