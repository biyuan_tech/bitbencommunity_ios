//
//  BYLiveSetModel.h
//  BY
//
//  Created by Belief on 2018/9/10.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonModel.h"

@class BYCommonLiveModel;
@interface BYLiveSetModel : BYCommonModel

- (NSArray *)getTableData:(BYCommonLiveModel *)liveModel;

@end

@interface BYLiveSetCellModel : BYCommonModel

@property (nonatomic ,strong) NSString *rightTitle;
/** 右侧头像链接 */
@property (nonatomic ,strong) NSString *rightlogoName;
/** 右侧头像（优先选取logo） */
@property (nonatomic ,strong) UIImage *rightlogo;
/** 是否显示底部line */
@property (nonatomic ,assign) BOOL showBottomLine;
/** 话题组 */
@property (nonatomic ,strong) NSArray *topicArr;

@end
