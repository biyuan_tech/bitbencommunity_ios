//
//  BYLiveSetController.h
//  BY
//
//  Created by 黄亮 on 2018/9/10.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonViewController.h"

@class BYCommonLiveModel;
@interface BYLiveSetController : BYCommonViewController

@property (nonatomic ,strong) BYCommonLiveModel *model;
@property (nonatomic ,copy) void (^reloadLiveRoomHandel)(void);

@end
