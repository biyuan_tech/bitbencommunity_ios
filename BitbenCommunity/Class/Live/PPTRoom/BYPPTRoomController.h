//
//  BYPPTRoomController.h
//  BY
//
//  Created by 黄亮 on 2018/8/21.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonViewController.h"

@interface BYPPTRoomController : BYCommonViewController

/** 直播记录id */
@property (nonatomic ,copy) NSString *live_record_id;
/** 是否为主播 */
@property (nonatomic ,assign) BOOL isHost;

@end
