//
//  BYLiveReviewCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveReviewCell.h"
#import "BYPersonHomeController.h"
#import "BYLiveReviewModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

@interface BYLiveReviewCell ()

/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogo;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 点赞 */
@property (nonatomic ,strong) UIButton *likeBtn;
/** 标题 */
@property (nonatomic ,strong) YYLabel *titleLab;
/** 发布时间 */
@property (nonatomic ,strong) UILabel *timeLab;
/** 回复数 */
@property (nonatomic ,strong) UILabel *replyNumLab;
@property (nonatomic ,strong) UIView *replyNumBg;
/** bbt奖励图标 */
@property (nonatomic ,strong) UIImageView *bbtIcon;
/** bbt奖励数 */
@property (nonatomic ,strong) UILabel *bbtNumLab;
/** 承载成就图标 */
@property (nonatomic ,strong) UIView *achievementView;
/** 原创标签 */
@property (nonatomic ,strong) UIImageView *originalImgView;
/** model */
@property (nonatomic ,strong) BYLiveReviewModel *model;


@end

@implementation BYLiveReviewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYLiveReviewModel *model = object[indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    self.userlogo.style = model.cert_badge;
    [self.userlogo uploadHDImageWithURL:model.comment_pic callback:nil];
    self.userNameLab.text = nullToEmpty(model.comment_name);
    
    [self setTitleLabText];
    
    self.timeLab.text = model.show_time;
    self.bbtNumLab.text = nullToEmpty(model.reward);
    
    self.likeBtn.selected = model.isSupport;
    if (model.count_support > 0) {
        [_likeBtn setBy_attributedTitle:@{@"title":[NSString transformIntegerShow:model.count_support],
                                         NSFontAttributeName:[UIFont systemFontOfSize:12],
                                         NSForegroundColorAttributeName:kColorRGBValue(0x323232)
                                         } forState:UIControlStateNormal];
        [_likeBtn setBy_attributedTitle:@{@"title":[NSString transformIntegerShow:model.count_support],
                                         NSFontAttributeName:[UIFont systemFontOfSize:12],
                                         NSForegroundColorAttributeName:kColorRGBValue(0xea6441)
                                         } forState:UIControlStateSelected];
    }else{
        [_likeBtn setBy_attributedTitle:@{@"title":@"赞",
                                         NSFontAttributeName:[UIFont systemFontOfSize:12],
                                         NSForegroundColorAttributeName:kColorRGBValue(0x323232)
                                         } forState:UIControlStateNormal];
        [_likeBtn setBy_attributedTitle:@{@"title":@"赞",
                                         NSFontAttributeName:[UIFont systemFontOfSize:12],
                                         NSForegroundColorAttributeName:kColorRGBValue(0xea6441)
                                         } forState:UIControlStateSelected];
    }
    _likeBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
    _likeBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
    
    
    NSString *replayStr = model.childCount > 0 ? [NSString stringWithFormat:@"%i 回复",(int)model.childCount] : @"回复";
    self.replyNumLab.text = replayStr;
    [self.replyNumLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(replayStr, 12) + 16);
    }];
    
    self.replyNumBg.hidden = model.childCount > 0 ? NO : YES;
    [self.replyNumBg mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(replayStr, 12) + 16);
    }];
    
    // 添加成就
    
    for (UIView *subView in self.achievementView.subviews) {
        [subView removeFromSuperview];
    }
    
    NSArray *achievementData = self.model.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [self.achievementView addSubview:imageView];
    }
}

- (void)setTitleLabText{
    
    self.titleLab.numberOfLines = self.model.isShowMaxH ? 0 : 4;
    
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc]initWithString:self.model.content];
    attributed.yy_color = kColorRGBValue(0x323232);
    attributed.yy_lineSpacing = 4;
    attributed.yy_font = [UIFont systemFontOfSize:15];
    self.titleLab.attributedText = attributed;
    
    NSString *moreString = @" 全文";
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"... %@", moreString]];
    NSRange expandRange = [text.string rangeOfString:moreString];
    
    [text addAttribute:NSForegroundColorAttributeName value:kColorRGBValue(0x426db4) range:expandRange];
    [text addAttribute:NSForegroundColorAttributeName value:kColorRGBValue(0x323232) range:NSMakeRange(0, expandRange.location)];
    
    //添加点击事件
    YYTextHighlight *hi = [YYTextHighlight new];
    [text yy_setTextHighlight:hi range:[text.string rangeOfString:moreString]];
    
    @weakify(self);
    hi.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        @strongify(self);
        //点击展开
        self.model.isShowMaxH = YES;
        self.model.cellHeight = self.model.maxCellHeight;
        [self.tableView reloadData];
    };
    
    text.yy_font = [UIFont systemFontOfSize:15];
    YYLabel *seeMore = [YYLabel new];
    seeMore.attributedText = text;
    [seeMore sizeToFit];
    
    NSAttributedString *truncationToken = [NSAttributedString yy_attachmentStringWithContent:seeMore contentMode:UIViewContentModeCenter attachmentSize:seeMore.frame.size alignToFont:text.yy_font alignment:YYTextVerticalAlignmentTop];
    
    self.titleLab.truncationToken = self.model.isShowMaxH ? nil : truncationToken;
    
    [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.height.mas_equalTo(self.model.isShowMaxH ? self.model.maxContentH : self.model.minContentH);
    }];
}

- (void)reloadAttentionAnimation{
    self.likeBtn.selected = _model.isSupport;
    NSString *title = [NSString transformIntegerShow:self.model.count_support];
    [_likeBtn setBy_attributedTitle:@{@"title":title,
                                      NSFontAttributeName:[UIFont systemFontOfSize:12],
                                      NSForegroundColorAttributeName:kColorRGBValue(0x323232)
                                      } forState:UIControlStateNormal];
    [_likeBtn setBy_attributedTitle:@{@"title":title,
                                      NSFontAttributeName:[UIFont systemFontOfSize:12],
                                      NSForegroundColorAttributeName:kColorRGBValue(0xea6441)
                                      } forState:UIControlStateSelected];
    self.likeBtn.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView beginAnimations:@"animation" context:nil];
    [UIView setAnimationDuration:0.2];
    self.likeBtn.layer.transform = CATransform3DMakeScale(1.2, 1.1, 1);
    [UIView setAnimationDelay:0.4];
    [UIView setAnimationDuration:0.2];
    self.likeBtn.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView commitAnimations];
    _likeBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
    _likeBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
}

#pragma mark - action
- (void)userlogoAction{
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    if ([currentController isKindOfClass:[BYPersonHomeController class]]) {
        return;
    }
    BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
    personHomeController.user_id = self.model.user_id;
    [(BYCommonViewController *)currentController authorizePush:personHomeController animation:YES];
}

- (void)likeBtnAction{
    [self sendActionName:@"likeAction" param:nil indexPath:self.indexPath];
}

#pragma mark - configUI

- (void)setContentView{
    PDImageView *userlogo = [[PDImageView alloc] init];
    userlogo.contentMode = UIViewContentModeScaleAspectFill;
    userlogo.layer.cornerRadius = 20;
    userlogo.clipsToBounds = YES;
    userlogo.userInteractionEnabled = YES;
    @weakify(self);
    [userlogo addTapGestureRecognizer:^{
        @strongify(self);
        [self userlogoAction];
    }];
    [self.contentView addSubview:userlogo];
    self.userlogo = userlogo;
    [userlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.height.mas_equalTo(40);
        make.top.mas_equalTo(13);
    }];
    
    // 用户昵称
    UILabel *userNameLab = [UILabel by_init];
    [userNameLab setBy_font:14];
    userNameLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:userNameLab];
    self.userNameLab = userNameLab;
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(12);
        make.top.mas_equalTo(userlogo.mas_top).mas_offset(5);
        make.height.mas_equalTo(userNameLab.font.pointSize);
        make.width.mas_lessThanOrEqualTo(kCommonScreenWidth - 130);
    }];
    
    // 点赞
    UIButton *likeBtn = [UIButton by_buttonWithCustomType];
    [likeBtn setBy_imageName:@"livehome_like_normal" forState:UIControlStateNormal];
    [likeBtn setBy_imageName:@"livehome_like_highlight" forState:UIControlStateSelected];
    [likeBtn setBy_attributedTitle:@{@"title":@"赞",
                                     NSFontAttributeName:[UIFont systemFontOfSize:12],
                                     NSForegroundColorAttributeName:kColorRGBValue(0x323232)
                                     } forState:UIControlStateNormal];
    [likeBtn setBy_attributedTitle:@{@"title":@"赞",
                                     NSFontAttributeName:[UIFont systemFontOfSize:12],
                                     NSForegroundColorAttributeName:kColorRGBValue(0xea6441)
                                     } forState:UIControlStateSelected];
    [likeBtn addTarget:self action:@selector(likeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:likeBtn];
    self.likeBtn = likeBtn;
    [likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(20);
        make.centerY.mas_equalTo(userNameLab).offset(0);
        make.width.mas_equalTo(50);
    }];
    
    // 评论内容
    YYLabel *titleLab = [[YYLabel alloc] init];
    titleLab.font = [UIFont systemFontOfSize:14];
    titleLab.numberOfLines = 4;
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(12);
        make.top.mas_equalTo(userNameLab.mas_bottom).mas_offset(10);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    // 发布时间
    UILabel *timeLab  = [UILabel by_init];
    [timeLab setBy_font:12];
    timeLab.textColor = kColorRGBValue(0xa2a2a2);
    timeLab.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:timeLab];
    self.timeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(12);
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(10);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(timeLab.font.pointSize);
    }];
    
    // 回复数
    UILabel *replyNumLab = [UILabel by_init];
    [replyNumLab setBy_font:12];
    replyNumLab.textColor = kColorRGBValue(0x323232);
    replyNumLab.textAlignment = NSTextAlignmentCenter;
    replyNumLab.layer.cornerRadius = 11.0f;
    [self.contentView addSubview:replyNumLab];
    self.replyNumLab = replyNumLab;
    [replyNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(22);
        make.centerY.mas_equalTo(timeLab);
        make.left.mas_equalTo(timeLab.mas_right).mas_offset(10);
    }];
    
    UIView *replyNumBg = [UIView by_init];
    replyNumBg.backgroundColor = kColorRGBValue(0xf5f5f5);
    replyNumBg.layer.cornerRadius = 11.0f;
    [self.contentView addSubview:replyNumBg];
    [self.contentView insertSubview:replyNumBg belowSubview:replyNumLab];
    self.replyNumBg = replyNumBg;
    [replyNumBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(22);
        make.centerY.mas_equalTo(timeLab);
        make.left.mas_equalTo(timeLab.mas_right).mas_offset(10);
    }];
    
    // bbt奖励图标
    UIImageView *bbtIcon = [UIImageView by_init];
    [bbtIcon by_setImageName:@"common_bp"];
    [self.contentView addSubview:bbtIcon];
    self.bbtIcon = bbtIcon;
    [bbtIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(timeLab);
        make.left.mas_equalTo(replyNumLab.mas_right).mas_offset(28);
        make.width.height.mas_equalTo(15);
    }];
    
    // bbt奖励数
    UILabel *bbtNumLab = [UILabel by_init];
    [bbtNumLab setBy_font:12];
    bbtNumLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:bbtNumLab];
    self.bbtNumLab = bbtNumLab;
    [bbtNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(bbtIcon);
        make.left.mas_equalTo(bbtIcon.mas_right).mas_offset(5);
        make.height.mas_equalTo(bbtNumLab.font.pointSize);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    // 承载成就
    self.achievementView = [[UIView alloc] init];
    [self.contentView addSubview:self.achievementView];
    [self.achievementView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.userNameLab.mas_right).mas_offset(5);
        make.centerY.equalTo(self.userNameLab);
        make.width.mas_equalTo(58);
        make.height.mas_equalTo(16);
    }];
    
    
}

@end
