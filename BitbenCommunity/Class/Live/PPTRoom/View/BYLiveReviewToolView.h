//
//  BYLiveReviewToolView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYIMMessageInputView.h"


NS_ASSUME_NONNULL_BEGIN

@interface BYLiveReviewToolView : UIView

@property (nonatomic ,strong ,readonly) BYIMMessageTextView *inputView;
/** 需适配底部 */
@property (nonatomic ,assign) BOOL needSafeBottom;
/** 消息发送回调 */
@property (nonatomic ,copy) void (^didSendMessageHandle)(NSString *string);


@end

NS_ASSUME_NONNULL_END
