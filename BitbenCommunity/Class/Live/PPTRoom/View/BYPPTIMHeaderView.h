//
//  BYPPTIMHeaderView.h
//  BibenCommunity
//
//  Created by 随风 on 2018/10/13.
//  Copyright © 2018 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYPPTIMHeaderView : UIView


/**
 广告数据
 */
@property (nonatomic ,strong) NSArray *advertData;
/** 开始时间*/
@property (nonatomic,copy)NSString *begin_time;

@property (nonatomic ,weak) UIViewController *viewController;

/**
 广告点击index
 */
@property (nonatomic ,copy) void (^didSelectAdvertIndex)(NSInteger index);

/**
 点击分类标题
 */
@property (nonatomic ,copy) void (^didSelectHeaderTitle)(NSInteger index);

@property (nonatomic ,copy) void (^beginLiveHandle)(void);

- (void)reloadData;

/** 启动定时 */
//- (void)startTimer;
///** 销毁定时 */
- (void)destoryTimer;

@end

NS_ASSUME_NONNULL_END
