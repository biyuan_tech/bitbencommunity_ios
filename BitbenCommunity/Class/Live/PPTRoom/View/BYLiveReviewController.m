//
//  BYLiveReviewController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveReviewController.h"
#import "BYLiveReviewToolView.h"
#import "LiveForumDetailViewController.h"

#import "BYLiveReviewModel.h"

#import "BYLiveReviewCell.h"

//@interface BYCommonTableView (touch)
//
//@end

@interface BYLiveReviewController ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** 底部输入 */
@property (nonatomic ,strong) BYLiveReviewToolView *inputView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;
@end

@implementation BYLiveReviewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 键盘输入
    [self addInputView];
    // 列表
    [self addTableView];
    [self loadRequestGetComment];
}

- (void)reloadData{
    self.pageNum = 0;
    [self loadRequestGetComment];
}

- (void)loadMoreData{
    [self loadRequestGetComment];
}

#pragma mark - action
- (void)sendComment:(NSString *)string{
    self.inputView.inputView.text = @"";
    [self.view endEditing:YES];
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestSendReveiewComment:_theme_id type:0 content:string successBlock:^(id object) {
        @strongify(self);
        [self.tableView removeEmptyView];
        BYLiveReviewModel *model = [[BYLiveReviewModel alloc] init];
        model = [model getSingleData:string];
        model.comment_id = object[@"comment_id"];
        NSMutableArray *data = [NSMutableArray arrayWithArray:self.tableView.tableData];
        [data insertObject:model atIndex:0];
        self.tableView.tableData = data;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"评论发送失败", self.view);
    }];
}

#pragma mark - BYCommonTableViewDelegate

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"likeAction"]) { // 点赞
        BYLiveReviewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        BYLiveReviewModel *model = tableView.tableData[indexPath.row];
        if (model.isSupport) return;
        BY_GUEST_OPERA_TYPE type = model.isSupport ? 1 : 0;
        [self loadRequestSupport:type model:model suc:^{
            model.isSupport = !model.isSupport;
            model.count_support = type == 0 ? ++model.count_support : --model.count_support;
            [cell reloadAttentionAnimation];
        }];
    }
}

- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self.inputView.inputView isFirstResponder]) {
        [self.inputView.inputView resignFirstResponder];
        return;
    }
    BYLiveReviewModel *model = tableView.tableData[indexPath.row];
    LiveFourumRootListSingleModel *singleModel = [model transSameModel];
    LiveForumDetailViewController *detailController = [[LiveForumDetailViewController alloc] init];
    detailController.transferFourumRootModel = singleModel;
//    detailController.transferPageType = ArticleDetailRootViewControllerTypeLive;
    detailController.transferRoomId = model.comment_id;
    UIViewController *currentController = [BYTabbarViewController sharedController].currentController;
    [currentController.navigationController pushViewController:detailController animated:YES];
}

#pragma mark - request
- (void)loadRequestGetComment{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetReview:_theme_id pageNum:_pageNum successBlock:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        [self.tableView removeEmptyView];
        if (self.pageNum == 0) {
            if (![object count]) {
                [self.tableView addEmptyView:@"暂无评论"];
            }
            self.tableView.tableData = object;
        }else{
            NSMutableArray *array = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [array addObjectsFromArray:object];
            self.tableView.tableData = array;
        }
        if ([object count]) {
            self.pageNum ++;
        }
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

- (void)loadRequestSupport:(BY_GUEST_OPERA_TYPE)opear_type model:(BYLiveReviewModel *)model suc:(void(^)(void))suc{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCommentOpearType:opear_type comment_id:model.comment_id theme_id:_theme_id theme_type:BY_THEME_TYPE_LIVE successBlock:^(id object) {
        if (suc) suc();
    } faileBlock:^(NSError *error) {
        @strongify(self);
        NSString *string = opear_type == 0 ? @"顶" : @"踩";
        showToastView([NSString stringWithFormat:@"%@操作失败",string], self.view);
    }];
}
#pragma mark - configUI
- (void)addInputView{
    self.inputView = [[BYLiveReviewToolView alloc] init];
    self.inputView.needSafeBottom = self.needSafeBottom;
    @weakify(self);
    self.inputView.didSendMessageHandle = ^(NSString * _Nonnull string) {
        @strongify(self);
        [self sendComment:string];
    };
    [self.view addSubview:self.inputView];
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.needSafeBottom ? -kSafe_Mas_Bottom(0) : 0);
        make.height.mas_equalTo(49);
    }];
}

- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.tableView addHeaderRefreshTarget:self action:@selector(reloadData)];
    [self.tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    [self.view addSubview:self.tableView];
    @weakify(self);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.mas_equalTo(self.safeTopY);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.inputView.mas_top).mas_offset(0);
    }];
}

@end

//@implementation BYCommonTableView (touch)
//
//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
//    [self.superview endEditing:YES];
//    return [super hitTest:point withEvent:event];
//}
//
//@end
