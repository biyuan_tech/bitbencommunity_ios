//
//  BYLiveReviewController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYLiveReviewController : UIViewController

/** id */
@property (nonatomic ,copy) NSString *theme_id;
/** 需适配底部 */
@property (nonatomic ,assign) BOOL needSafeBottom;
/** 顶部适配距离 */
@property (nonatomic ,assign) CGFloat safeTopY;


@end

NS_ASSUME_NONNULL_END
