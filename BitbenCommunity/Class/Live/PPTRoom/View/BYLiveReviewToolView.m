//
//  BYLiveReviewToolView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveReviewToolView.h"

static NSInteger defultH = 49;
@interface BYLiveReviewToolView ()<UITextViewDelegate>

/** 输入框 */
@property (nonatomic ,strong) BYIMMessageTextView *inputView;
/** 消息发送 */
@property (nonatomic ,strong) UIButton *sendBtn;
/** 评论图标 */
@property (nonatomic ,strong) UIImageView *placeholderImgView;
/** 评论文案 */
@property (nonatomic ,strong) UILabel *placeholderLab;


@end

@implementation BYLiveReviewToolView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
        [self addNSNotification];
    }
    return self;
}

- (CGFloat)calculateTextViewHeight:(UITextView *)textView text:(NSString *)text{
    CGSize constraninSize = CGSizeMake(textView.contentSize.width, MAXFLOAT);
    CGSize size = [textView sizeThatFits:constraninSize];
    return size.height + 14 + textView.textContainerInset.top;
}

#pragma mark - action

- (void)sendMessageAction{
    if (self.didSendMessageHandle) {
        NSString *string = [self.inputView.text removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
        self.didSendMessageHandle(string);
    }
}
#pragma mark - UITextViewDelegate 文本输入代理

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"] && textView.text.length) {
        [textView resignFirstResponder];
        [self sendMessageAction];
        return YES;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length) {
        self.placeholderImgView.hidden  = YES;
        self.placeholderLab.hidden      = YES;
    }else{
        self.placeholderImgView.hidden  = NO;
        self.placeholderLab.hidden      = NO;
    }
    CGFloat maxHeight = 70;
    CGFloat height = [self calculateTextViewHeight:textView text:textView.text];
    textView.scrollEnabled = height > maxHeight ? YES : NO;
    if (height < maxHeight ) {
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height < defultH ? defultH : height);
        }];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length) {
        self.placeholderImgView.hidden  = YES;
        self.placeholderLab.hidden      = YES;
    }else{
        self.placeholderImgView.hidden  = NO;
        self.placeholderLab.hidden      = NO;
    }
}

#pragma mark - keyBoardNSNotification

- (void)keyBoardWillShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardDidShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardWillHidden:(NSNotification *)notic{
    if (self.superview && self.inputView.isFirstResponder) {
        [UIView animateWithDuration:0.1 animations:^{
            @weakify(self);
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                @strongify(self);
                make.bottom.mas_equalTo(self.needSafeBottom ? kiPhoneX_Mas_buttom(0) : 0);
                make.left.right.mas_equalTo(0);
            }];
            [self.superview layoutIfNeeded];
        }];
    }
}

- (void)keyBoardShowAnimation:(NSValue *)value{
    if (self.superview && self.inputView.isFirstResponder) {
        CGFloat detalY = [value CGRectValue].size.height;
        [UIView animateWithDuration:0.1 animations:^{
            @weakify(self);
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                @strongify(self);
                make.left.right.mas_equalTo(0);
                make.bottom.mas_equalTo(self.needSafeBottom ? -detalY : -detalY + kSafeAreaInsetsBottom);
            }];
            [self.superview layoutIfNeeded];
        }];
    }
}

#pragma mark - regisetNSNotic
- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}


#pragma mark - configUI

- (void)setContentView{
    self.inputView = [[BYIMMessageTextView alloc] init];
    self.inputView.layer.cornerRadius = 5.0f;
    [self.inputView setBackgroundColor:kColorRGBValue(0xeeeeee)];
    self.inputView.tintColor = kBgColor_238;
    self.inputView.font = [UIFont systemFontOfSize:16];
    self.inputView.textContainerInset = UIEdgeInsetsMake(7, 8, 0, 5);
    self.inputView.textContainer.lineFragmentPadding = 0;
    self.inputView.returnKeyType = UIReturnKeySend;
    self.inputView.delegate = self;
    [self addSubview:self.inputView];
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 15, 7, 57));
    }];
    
    self.placeholderImgView = [[UIImageView alloc] init];
    [self.placeholderImgView by_setImageName:@"icon_article_detail_draw_input"];
    [self addSubview:self.placeholderImgView];
    @weakify(self);
    [self.placeholderImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(27);
        make.width.mas_equalTo(self.placeholderImgView.image.size.width);
        make.height.mas_equalTo(self.placeholderImgView.image.size.height);
        make.centerY.mas_equalTo(0);
    }];
    
    self.placeholderLab = [UILabel by_init];
    [self.placeholderLab setBy_font:14];
    self.placeholderLab.textColor = kColorRGBValue(0xb6b6b7);
    self.placeholderLab.text = @"写评论...";
    [self addSubview:self.placeholderLab];
    [self.placeholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.placeholderImgView.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(0);
        make.height.mas_equalTo(self.placeholderLab.font.pointSize);
        make.width.mas_equalTo(60);
    }];
    
    UIButton *sendBtn = [UIButton by_buttonWithCustomType];
    [sendBtn setBy_attributedTitle:@{@"title":@"发送",
                                     NSFontAttributeName:[UIFont systemFontOfSize:15],
                                     NSForegroundColorAttributeName:kColorRGBValue(0xeb6947)
                                     } forState:UIControlStateNormal];
    [sendBtn addTarget:self action:@selector(sendMessageAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sendBtn];
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(57);
    }];
    
    UIView *topLineView = [UIView new];
    [topLineView setBackgroundColor:kColorRGBValue(0xededed)];
    [self addSubview:topLineView];
    [topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(kCommonScreenWidth);
    }];
}

@end
