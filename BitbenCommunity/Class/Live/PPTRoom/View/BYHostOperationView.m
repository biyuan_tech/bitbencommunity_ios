//
//  BYHostOperationView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/10/11.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYHostOperationView.h"

@interface BYHostOperationView ()
/** 禁言开关 */
@property (nonatomic ,strong) UISwitch *shutupSwitch;
@property (nonatomic ,strong) UIView *contentView;
@property (nonatomic ,weak) UIView *fathureView;
/** 遮罩层 */
@property (nonatomic ,strong) UIView *maskView;

@end

//static CGFloat conteneViewH = 118 ;//190
static CGFloat conteneViewH = 190 ;//190
static NSInteger baseTag = 0x241;
@implementation BYHostOperationView
- (instancetype)initWithFathureView:(UIView *)fathureView
{
    self = [super init];
    if (self) {
        self.fathureView = fathureView;
        self.frame = fathureView.bounds;
        [self setContentView];
    }
    return self;
}


#pragma mark - customMethod
- (void)showAnimation{
    NSAssert(self.fathureView, @"父视图不能为nil");
    
    [_fathureView addSubview:self];
    self.center = CGPointMake(_fathureView.center.x, _fathureView.center.y);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self setNeedsUpdateConstraints];
        [UIView animateWithDuration:0.2 animations:^{
            self.maskView.opaque = 1.0f;
            [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
            }];
            [self layoutIfNeeded];
        }];
    });
}

- (void)hiddenAnimation{
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.2 animations:^{
        self.maskView.opaque = 0.0f;
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(conteneViewH);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - action Method
- (void)switchAction:(UISwitch *)sender{
    
}

- (void)endLiveBtnAction:(UIButton *)sender{
    [self hiddenAnimation];
    if (_didEndLiveHandle) {
        _didEndLiveHandle();
    }
}

#pragma mark - initMehtod/configSubView

- (void)setContentView{
    // 背景遮罩
    self.maskView = [UIView new];
    _maskView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.47];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenAnimation)];
    [_maskView addGestureRecognizer:tapGestureRecognizer];
    [self addSubview:self.maskView];
    [_maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    self.contentView = [UIView new];
    [_contentView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:self.contentView];
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(conteneViewH);
        make.height.mas_equalTo(conteneViewH);
    }];

    UILabel *titleLab = [UILabel by_init];
    titleLab.text = @"操作";
    titleLab.font = [UIFont systemFontOfSize:15];
    titleLab.textColor = kTextColor_53;
    titleLab.textAlignment = NSTextAlignmentCenter;
    [_contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(48);
        make.centerX.top.mas_equalTo(0);
    }];
    
    // 隐藏按钮
    UIButton *downBtn = [UIButton by_buttonWithCustomType];
    [downBtn setBy_imageName:@"icon_live_opera_down" forState:UIControlStateNormal];
    [downBtn addTarget:self action:@selector(hiddenAnimation) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:downBtn];
    [downBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(42);
        make.height.mas_equalTo(48);
        make.top.right.mas_equalTo(0);
    }];
    
    NSArray *iconNames = @[@"icon_live_opera_shutup"];
    NSArray *titles = @[@"用户禁言模式"];
    for (int i = 0; i < 1; i ++) {
        UIView *view = [self getSingleSwithView:iconNames[i] title:titles[i] index:i bottomLine:NO];
        [_contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.height.mas_equalTo(48);
            make.top.mas_equalTo(60 + 48*i);
        }];
    }
    
    for (int i = 0; i < 2; i ++) {
        UIView *view = [UIView by_init];
        [view setBackgroundColor:kBgColor_248];
        [_contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.height.mas_equalTo(12);
            make.top.mas_equalTo(i == 0 ? 48 : 108);
        }];
    }
    
    UIButton *endLiveBtn = [UIButton by_buttonWithCustomType];
    [endLiveBtn setBackgroundColor:kBgColor_238];
    [endLiveBtn setBy_attributedTitle:@{@"title":@"结束直播",NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [endLiveBtn addTarget:self action:@selector(endLiveBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    endLiveBtn.layer.cornerRadius = 10;
    [_contentView addSubview:endLiveBtn];
    [endLiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-13);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(46);
    }];
}

- (UIView *)getSingleSwithView:(NSString *)iconName title:(NSString *)title index:(NSInteger)index bottomLine:(BOOL)bottomLine{
    UIView *view = [UIView by_init];
    [view setBackgroundColor:[UIColor whiteColor]];
    UIImageView *iconImgView = [[UIImageView alloc] init];
    [iconImgView by_setImageName:iconName];
    [view addSubview:iconImgView];
    [iconImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(iconImgView.image.size.width);
        make.height.mas_equalTo(iconImgView.image.size.height);
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:13];
    titleLab.textColor = kTextColor_48;
    titleLab.text = title;
    titleLab.textAlignment = NSTextAlignmentLeft;
    [view addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconImgView.mas_right).with.offset(12);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(18);
    }];
    
    UISwitch *switchView = [[UISwitch alloc] init];
    switchView.onTintColor = kColorRGBValue(0xee4944);
//    switchView.tintColor = kColorRGBValue(0xcfcfcf);
    [switchView addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    [switchView setOn:[BYSIMManager shareManager].isForbid];
    [view addSubview:switchView];
    self.shutupSwitch = switchView;
    [switchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.right.mas_equalTo(-15);
        make.size.mas_equalTo(CGSizeMake(46, 25));
    }];
    
    if (bottomLine) {
        UIView *lineView = [UIView by_init];
        [lineView setBackgroundColor:kBgColor_237];
        [view addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.bottom.mas_equalTo(-0.5);
            make.height.mas_equalTo(0.5);
        }];
    }
    return view;
}
@end
