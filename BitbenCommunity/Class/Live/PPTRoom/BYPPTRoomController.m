//
//  BYPPTRoomController.m
//  BY
//
//  Created by 黄亮 on 2018/8/21.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYPPTRoomController.h"
#import "BYPPTRoomViewModel.h"
#import "BYLiveHomeVideoModel.h"

#import <AFNetworking.h>
@interface BYPPTRoomController () <NetworkAdapterSocketDelegate>

@end

@implementation BYPPTRoomController

- (void)dealloc
{
    [[BYSIMManager shareManager] resetScoketStatus];
}

- (Class)getViewModelClass{
    return [BYPPTRoomViewModel class];
}


- (void)viewWillAppear:(BOOL)animated{
    self.hasCancelSocket = NO;
    [super viewWillAppear:animated];
    self.popGestureRecognizerEnale = NO;
    [NetworkAdapter sharedAdapter].delegate = self;
    [[BYSIMManager shareManager] connectionSocket];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[BYSIMManager shareManager] closeSocket];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data{
    [[BYSIMManager shareManager] webSocketReceiveData:type data:data];
}

-(void)webSocketDidConnectedWithSocket:(WebSocketConnection *)webSocketConnection connectedStatus:(SocketConnectType)connectedStatus{
    if (webSocketConnection == [NetworkAdapter sharedAdapter].mainRootSocketConnection) {
        if (connectedStatus == SocketConnectTypeOpen) {
            [[BYSIMManager shareManager] webSocketDidConnectedWithSocket:webSocketConnection];
        }
    }
}

@end
