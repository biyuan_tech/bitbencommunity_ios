//
//  BYHistotyIMViewController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/14.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYHistotyIMViewController.h"
#import "CDChatList.h"
#import "BYHistoryIMModel.h"
#import "MsgPicViewController.h"
#import "BYPPTBottomView.h"
#import "BYReportSheetView.h"
#import "BYRewardView.h"
#import "BYVideoLiveRewradTableCell.h"

@interface BYHistotyIMViewController ()<ChatListProtocol,UIScrollViewDelegate>

/** tableview */
@property (nonatomic ,strong) CDChatListView *tableView;
/** bottom */
@property (nonatomic ,strong) BYPPTBottomView *bottomView;
/** tableData */
@property (nonatomic ,strong) NSMutableArray *tableData;
/** currentpage */
@property (nonatomic ,assign) NSInteger pageNum;
/** 举报 */
@property (nonatomic ,strong) UIImageView *reportMarkView;
/** maskView */
@property (nonatomic ,strong) UIView *reportMaskView;
/** reportView */
@property (nonatomic ,strong) BYReportSheetView *reportSheetView;
/** 打赏 */
@property (nonatomic ,strong) BYRewardView *rewardView;



@end

@implementation BYHistotyIMViewController

- (void)dealloc
{
    [[AATAudioTool share] stopPlay];
    [_tableView removeFromSuperview];
    [_bottomView removeFromSuperview];
    [_reportSheetView removeFromSuperview];
    [_reportMarkView removeFromSuperview];
    _tableView = nil;
    _bottomView = nil;
    _reportSheetView = nil;
    _reportMarkView = nil;
}

- (void)viewDidLoad {
    self.pageNum = 0;
    [super viewDidLoad];
    [self configDefaultResource];
    [self addTableView];
    [self addBottomView];
    [self addRepeortMarkView];
    [self addRewardView];
//    [self loadHistoryIMData:nil];
    [self loadRequestGetHistoryMsg:nil];
}

-(void)configDefaultResource{
    
    // 聊天页面图片资源配置
    
    ChatHelpr.share.config.msgBackGroundColor = [UIColor whiteColor];
    NSMutableDictionary *resDic = [NSMutableDictionary dictionaryWithDictionary:ChatHelpr.share.imageDic];
    [resDic setObject:[UIImage imageNamed:@"voice_left_1"] forKey:@"voice_left_1"];
    [resDic setObject:[UIImage imageNamed:@"voice_left_2"] forKey:@"voice_left_2"];
    [resDic setObject:[UIImage imageNamed:@"voice_left_3"] forKey:@"voice_left_3"];
    [resDic setObject:[[UIImage imageNamed:@"talk_pop_l_nor"] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 8, 6, 6)] forKey:@"talk_pop_l_nor"];
    [resDic setObject:[UIImage imageNamed:@"common_userlogo_bg"] forKey:@"common_userlogo_bg"];
    [resDic setObject:[UIImage imageNamed:@"iliveroom_reward"] forKey:@"reward_left"];
    [resDic setObject:[UIImage imageNamed:@"icon_live_banned"] forKey:@"banned_left"];
    NSDictionary *drawImages = [ChatImageDrawer defaultImageDic];
    for (NSString *imageName in drawImages) {
        resDic[imageName] = drawImages[imageName];
    }
    
    ChatHelpr.share.imageDic = resDic;
    ChatHelpr.share.config.left_box = @"talk_pop_l_nor";

    // 添加语音输入功能
    [CTinputHelper.share.config addVoice];

}

#pragma mark - UITableViewDelegate/DataScource

- (void)addTableView{
    if (_tableView) return;
    self.tableView = [[CDChatListView alloc] initWithFrame:CGRectZero];
    self.tableView.msgDelegate = self;
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 45, 0));
    }];
}

- (void)addBottomView{
    if (_bottomView) return;
    self.bottomView = [[BYPPTBottomView alloc] init];
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(45);
    }];
    @weakify(self);
    self.bottomView.moreBtnHandle = ^{
        @strongify(self);
        [self showReportMarkViewHidden:NO];
    };
}

- (void)addRepeortMarkView{
    if (_reportMarkView) return;
    
    self.reportSheetView = [BYReportSheetView initReportSheetViewShowInView:kCommonWindow];
    self.reportSheetView.theme_id = self.model.live_record_id;
    self.reportSheetView.theme_type = BY_THEME_TYPE_LIVE;
    self.reportSheetView.user_id = self.model.user_id;
    
    self.reportMaskView = [UIView by_init];
    self.reportMaskView.layer.opacity = 0.0f;
    [self.superController.view addSubview:self.reportMaskView];
    [self.reportMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reportMaskViewAction)];
    [self.reportMaskView addGestureRecognizer:tapGestureRecognizer];
    
    self.reportMarkView = [[UIImageView alloc] init];
    [self.reportMarkView by_setImageName:@"common_report_single"];
    [self.superController.view addSubview:self.reportMarkView];
    self.reportMarkView.userInteractionEnabled = YES;
    self.reportMarkView.layer.shadowColor = kColorRGBValue(0x4e4e4e).CGColor;
    self.reportMarkView.layer.shadowOpacity = 0.2;
    self.reportMarkView.layer.shadowRadius = 25;
    self.reportMarkView.layer.shadowOffset = CGSizeMake(0, -2);
    self.reportMarkView.layer.opacity = 0.0f;
    @weakify(self);
    [self.reportMarkView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(-40);
        make.width.mas_equalTo(self.reportMarkView.image.size.width);
        make.height.mas_equalTo(self.reportMarkView.image.size.height);
    }];
    
    UIButton *reportBtn = [UIButton by_buttonWithCustomType];
    [reportBtn setBy_attributedTitle:@{@"title":@"举报",NSForegroundColorAttributeName:kColorRGBValue(0x4f4f4f),NSFontAttributeName:[UIFont systemFontOfSize:13]} forState:UIControlStateNormal];
    [reportBtn addTarget:self action:@selector(reportBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.reportMarkView addSubview:reportBtn];
    [reportBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
}

- (void)addRewardView{
    self.rewardView = [[BYRewardView alloc] initWithFathureView:self.superController.view];
    @weakify(self);
    _rewardView.confirmBtnHandle = ^(CGFloat amount) {
        @strongify(self);
        [self loadRequestReward:amount];
    };
}

- (void)reportMaskViewAction{
    [self showReportMarkViewHidden:YES];
}

- (void)showReportMarkViewHidden:(BOOL)hidden{
    [self.superController.view bringSubviewToFront:self.reportMaskView];
    [self.superController.view bringSubviewToFront:self.reportMarkView];
    [UIView animateWithDuration:0.1 animations:^{
        self.reportMarkView.layer.opacity = !hidden;
        self.reportMaskView.layer.opacity = !hidden;
    }];
}

#pragma mark - buttonAction
- (void)reportBtnAction:(UIButton *)sender{
    [self showReportMarkViewHidden:YES];
    [self.reportSheetView showAnimation];
}

#pragma mark - ChatListProtocol

- (void)chatlistClickMsgEvent:(ChatListInfo *)listInfo {
    switch (listInfo.eventType) {
        case ChatClickEventTypeIMAGE:
        {
            CGRect newe =  [listInfo.containerView.superview convertRect:listInfo.containerView.frame toView:kCommonWindow];
            [MsgPicViewController addToRootViewController:listInfo.image ofMsgId:listInfo.msgModel.messageId in:newe from:self.tableView.msgArr];
        }
            break;
        case ChatClickEventTypeTEXT:
            break;
        case ChatClickEventTypeAUDIO:
        {

        }
            break;
        case ChatClickEventTypeREWARD: // 点击赞赏
        {
            self.rewardView.receicer_id = listInfo.msgModel.userId;
            self.rewardView.receicer_name = listInfo.msgModel.userName;
            [self.rewardView showAnimation];
        }
            break;
        case ChatClickEventTypeBanned: // 点击禁言
            showToastView(@"点击禁言", self.view);
            break;
    }
}

- (void)chatlistLoadMoreMsg:(CDChatMessage)topMessage callback:(void (^)(CDChatMessageArray,BOOL))finnished {
//    @weakify(self);
//    [self loadHistoryIMData:^{
//        @strongify(self);
//        finnished(self.tableData,YES);
//    }];
    [self loadRequestGetHistoryMsg:^(NSArray *data) {
        finnished(data,data.count ? YES : NO);
    }];
}

- (void)chatlistDidDidScroll:(UIScrollView *)scrollView{
    if (self.scrollViewDidScrollView) {
        self.scrollViewDidScrollView(scrollView);
    }
}

- (void)chatlistAudioMsgReadStatusDidChange:(CDChatMessage)msg{
    // 设置语音消息为已读
    [[BYIMMessageDB shareManager] modifyIMMessageIsRead:msg.messageId];
}

- (NSDictionary<NSString *,Class> *)chatlistCustomeCellsAndClasses{
    return @{@"RewardCell": BYVideoLiveRewradTableCell.class};
    
}
-(CGSize)chatlistSizeForMsg:(CDChatMessage)msg ofList:(CDChatListView *)list{
    if (![msg.customData[@"text"] length]) {
        return CGSizeZero;
    }
    CGFloat maxWidth = iPhone5 ? kCommonScreenWidth - 70*2 : kCommonScreenWidth - 80*2;
    CGSize size = [msg.customData[@"text"] getStringSizeWithFont:[UIFont systemFontOfSize:12] maxWidth:maxWidth];
    return CGSizeMake(kCommonScreenWidth, ceil(size.height) + 14 + 15);
}

#pragma mark - request

- (void)loadHistoryIMData:(void(^)(void))cb{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetChatHistory:self.model.live_record_id groupId:self.model.group_id pageNum:_pageNum successBlock:^(id object) {
        @strongify(self);
        if (![object isKindOfClass:[NSDictionary class]]) {
            self.tableData = [@[] copy];
            if (cb) cb();
            return ;
        }
        if (![object[@"content"] count]) {
            self.tableData = [@[] copy];
            if (cb) cb();
            return;
        }
        self.tableData = [[BYHistoryIMModel getTableData:object[@"content"] liveData:self.model] copy];
        if (self.pageNum == 0) {
            self.tableView.msgArr = self.tableData;

        }
        self.pageNum ++;
        if (cb) cb();
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)loadRequestGetHistoryMsg:(void(^)(NSArray *data))cb{
    NSString *sendTime = [NSDate getCurrentTimeStr];
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetPageChatHistory:self.model.group_id pageNum:self.pageNum sendTime:sendTime successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        if (![object[@"content"] isKindOfClass:[NSArray class]] || ![object[@"content"] count]) {
            if (cb) cb(@[]);
            return;
        }
        if (self.pageNum == 0) {
            NSMutableArray *data = [NSMutableArray array];
            NSArray *array = [BYHistoryIMModel getSIMTableData:object[@"content"] liveData:self.model];
            array = [[array reverseObjectEnumerator] allObjects];
            [data addObjectsFromArray:array];
            [data addObjectsFromArray:self.tableView.msgArr];
            self.tableView.msgArr = array;
            [self.tableView reloadData];
        }
        else{
            NSArray *array = [BYHistoryIMModel getSIMTableData:object[@"content"] liveData:self.model];
            array = [[array reverseObjectEnumerator] allObjects];
            cb(array);
        }
        self.pageNum ++;
        
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)loadRequestReward:(CGFloat)amount{
    if ([_rewardView.receicer_id isEqualToString:[AccountModel sharedAccountModel].account_id]) {
        showToastView(@"不能给自己打赏", self.superController.view);
        return;
    }
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestReward:_rewardView.receicer_id reward_amount:amount successBlock:^(id object) {
        @strongify(self);
        showToastView(@"打赏成功", self.superController.view);
        [self.rewardView reloadSurplusBBT:amount];
    } faileBlock:^(NSError *error) {
    }];
}

@end
