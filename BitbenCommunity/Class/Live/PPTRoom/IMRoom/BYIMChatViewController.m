//
//  BYIMChatViewController.m
//  BY
//
//  Created by 黄亮 on 2018/9/4.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYIMChatViewController.h"
#import "BYLiveRoomHomeController.h"
#import "BYHistotyIMViewController.h"
#import "BYSIMController.h"
#import "BYPersonHomeController.h"

#import "BYPPTIMHeaderView.h"
#import "BYILiveOpeartionView.h"

#import "BYCommonLiveModel.h"

@interface BYIMChatViewController ()
{
    CGRect _rect;
}

/** imController */
@property (nonatomic ,strong) BYSIMController *imController;

/** 历史消息展示 */
@property (nonatomic ,strong) BYHistotyIMViewController *historyImController;

/** headerView */
@property (nonatomic ,strong) UIView *topHeaderView;
@property (nonatomic,strong)BYPPTIMHeaderView *headerView;
@property (nonatomic,strong)BYILiveOpeartionView *opeartionView;
/** 关注按钮 */
@property (nonatomic ,strong) UIButton *attentionBtn;


@end

@implementation BYIMChatViewController

- (void)dealloc
{
    [_headerView destoryTimer];
    [_imController.view removeFromSuperview];
    [_imController removeFromParentViewController];
    _imController = nil;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.view.frame = _rect;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    _rect = self.view.frame;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.view.superview) {
        [self.view mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kSafeAreaInsetsBottom, 0));
        }];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:kColorRGBValue(0xECEBEB)];
    self.hasCancelSocket = YES;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    if (!model) return;
    // 延迟加载避免界面跳动
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self addHeaderView];
        if (model.status == BY_LIVE_STATUS_END ||
            model.status == BY_LIVE_STATUS_OVERTIME) {
            [self creatHistoryIM];
        }
        else{
            [self createIMWithNUmber:model.group_id];
        }
        if (!_isHost) {
            [self addOpeartionView];
        }
    });
}

- (void)reloadAttentionStatus{
    self.attentionBtn.selected = self.model.isAttention;
    UIColor *color = !_model.isAttention ? kColorRGBValue(0xea6441) : kColorRGBValue(0xffffff);
    [_attentionBtn setBackgroundColor:color];
    _attentionBtn.layer.borderColor = !_model.isAttention ? kColorRGBValue(0xea6441).CGColor : kColorRGBValue(0x9b9b9b).CGColor;
}

#pragma mark - aciton

// 结束直播
- (void)finishLiveAction{
    @weakify(self);
    [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_END cb:^{
        @strongify(self);
        @weakify(self);
        [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM data:nil suc:^{
            @strongify(self);
            @weakify(self);
            [[BYSIMManager shareManager] closeRoom:self.model.group_id suc:^{
                @strongify(self);
                showDebugToastView(@"关闭聊天室成功",self.view);
            } fail:^{
                @strongify(self);
                showDebugToastView(@"关闭聊天室失败",self.view);
            }];
        } fail:nil];
        self.model.status = BY_LIVE_STATUS_END;
        UIViewController *viewController = self.viewController.navigationController.viewControllers[1];
        [self.viewController.navigationController popToViewController:viewController animated:YES];
    }];
}

- (void)userlogoAction{
    BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
    personHomeController.user_id = self.model.user_id;
    [self.viewController.navigationController pushViewController:personHomeController animated:YES];
}

#pragma mark - request
// 更新直播间状态
- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status cb:(void(^)(void))cb{
    if (!self.model.real_begin_time.length) {
        self.model.real_begin_time = [NSDate by_stringFromDate:[NSDate by_date] dateformatter:K_D_F];
    }
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveStatus:status live_record_id:_model.live_record_id stream_id:nil real_begin_time:self.model.real_begin_time successBlock:^(id object) {
        if (cb) cb();
        PDLog(@"更新直播间状态成功");
    } faileBlock:^(NSError *error) {
        PDLog(@"更新直播间状态失败");
    }];
}

- (void)attentionBtnAction{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:_model.user_id
                                             isAttention:!_attentionBtn.selected
                                            successBlock:^(id object) {
                                                @strongify(self);
                                                self.model.isAttention = !self.attentionBtn.selected;
                                                [self reloadAttentionStatus];
                                            } faileBlock:nil];
}

#pragma mark - 创建IM
- (void)addHeaderView{
    if (_topHeaderView) return;
    
    self.topHeaderView = [UIView by_init];
    [self.topHeaderView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.topHeaderView];
    CGFloat height = !_isHost ? LCFloat(195) + 48 : LCFloat(195);
    [self.topHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(height);
    }];
    
    self.headerView = [[BYPPTIMHeaderView alloc] init];
    self.headerView.viewController = self;
    @weakify(self);
    self.headerView.beginLiveHandle = ^{
        @strongify(self);
        if (self.isHost) {
            @weakify(self);
            [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_LIVING cb:^{
                @strongify(self);
                self.model.status = BY_LIVE_STATUS_LIVING;
                // 发送开播消息
                [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_RECEIVE_STREAM data:nil suc:nil fail:nil];
            }];
        }
    };
    self.headerView.begin_time = self.model.begin_time;
    self.headerView.advertData = self.model.courseware;
    [self.view addSubview:self.headerView];
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(LCFloat(195));
    }];
    [self.headerView reloadData];
    
    if (!_isHost) {
        [self addHeaderBottomView];
    }
}

- (void)addHeaderBottomView{
    PDImageView *userlogoView = [[PDImageView alloc] init];
    userlogoView.contentMode = UIViewContentModeScaleAspectFill;
    userlogoView.layer.cornerRadius = 16;
    userlogoView.clipsToBounds = YES;
    userlogoView.userInteractionEnabled = YES;
    [self.topHeaderView addSubview:userlogoView];
    userlogoView.style = self.model.cert_badge;
    [userlogoView uploadHDImageWithURL:_model.head_img callback:nil];
    [userlogoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(17);
        make.bottom.mas_equalTo(-8);
        make.width.height.mas_equalTo(32);
    }];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userlogoAction)];
    [userlogoView addGestureRecognizer:tapRecognizer];
    
    
    // 头像背景
    UIImageView *userlogoBg = [[UIImageView alloc] init];
    [userlogoBg by_setImageName:@"common_userlogo_bg"];
    [self.topHeaderView addSubview:userlogoBg];
    [self.topHeaderView insertSubview:userlogoBg belowSubview:userlogoView];
    [userlogoBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(userlogoBg.image.size.width + 2);
        make.height.mas_equalTo(userlogoBg.image.size.height + 2);
        make.centerX.equalTo(userlogoView.mas_centerX).with.offset(0);
        make.centerY.equalTo(userlogoView.mas_centerY).with.offset(0);
    }];
    
    UILabel *userNameLab = [[UILabel alloc] init];
    [userNameLab setBy_font:13];
    userNameLab.textColor = kColorRGBValue(0x2c2c2c);
    userNameLab.text = _model.nickname;
    [self.topHeaderView addSubview:userNameLab];
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogoView.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(userlogoView.mas_centerY).mas_offset(0);
        make.width.mas_lessThanOrEqualTo(kCommonScreenWidth - 196);
        make.height.mas_equalTo(15);
    }];
    
    UIView *achievementView = [UIView by_init];
    [self.topHeaderView addSubview:achievementView];
    [achievementView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userNameLab.mas_right).mas_offset(5);
        make.centerY.equalTo(userNameLab);
        make.width.mas_equalTo(58);
        make.height.mas_equalTo(16);
    }];
    
    NSArray *achievementData = _model.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [achievementView addSubview:imageView];
    }
    
    // 关注按钮
    UIButton *attentionBtn = [UIButton by_buttonWithCustomType];
    NSAttributedString *normalAtt = [[NSAttributedString alloc] initWithString:@"+ 关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xffffff),NSFontAttributeName:[UIFont systemFontOfSize:12]}];
    NSAttributedString *selectAtt = [[NSAttributedString alloc] initWithString:@"已关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xee4944),NSFontAttributeName:[UIFont systemFontOfSize:12]}];
    [attentionBtn setAttributedTitle:normalAtt forState:UIControlStateNormal];
    [attentionBtn setAttributedTitle:selectAtt forState:UIControlStateSelected];
    [attentionBtn addTarget:self action:@selector(attentionBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.topHeaderView addSubview:attentionBtn];
    self.attentionBtn = attentionBtn;
    attentionBtn.layer.cornerRadius = 2;
    attentionBtn.layer.borderWidth = 0.5;
    attentionBtn.selected = _model.isAttention;
    [attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(54);
        make.bottom.mas_equalTo(-11);
    }];
    [self reloadAttentionStatus];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xededed)];
    [self.topHeaderView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
}


-(void)createIMWithNUmber:(NSString *)number{
    if (!self.imController) {
        self.imController = [[BYSIMController alloc] init];
        self.imController.isHost = self.isHost;
        self.imController.model = self.model;
        self.imController.superController = self;
        [self.view addSubview:self.imController.view];
        [self addChildViewController:self.imController];
        CGFloat height = !_isHost ? LCFloat(195) + 48 : LCFloat(195);
        @weakify(self);
        [self.imController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.right.mas_equalTo(0);
            make.top.mas_equalTo(height);

        }];
        
        [self.view bringSubviewToFront:self.opeartionView];
        self.opeartionView.aConversationController = self.imController;
        
        // 结束直播
        self.imController.didFinishLiveHandle = ^{
            @strongify(self);
            [self finishLiveAction];
        };
        
        self.imController.didUpdatePPTImgs = ^(NSArray *imgs) {
            @strongify(self);
            self.headerView.advertData = imgs;
            [self.headerView reloadData];
        };
        
        // 聊天室滑动
        self.imController.scrollViewDidScroll = ^(UIScrollView *scrollView) {
            @strongify(self);
            if (self.scrollViewDidScrollView) self.scrollViewDidScrollView(scrollView);
        };
    }
}

- (void)creatHistoryIM{
    if (_historyImController) return;
    self.historyImController = [[BYHistotyIMViewController alloc] init];
    self.historyImController.model = self.model;
    self.historyImController.superController = self;
    @weakify(self);
    self.historyImController.scrollViewDidScrollView = ^(UIScrollView *scrollView) {
        @strongify(self);
        if (self.scrollViewDidScrollView) self.scrollViewDidScrollView(scrollView);
    };
    [self.view addSubview:self.historyImController.view];
    [self addChildViewController:self.historyImController];
    CGFloat height = !_isHost ? LCFloat(195) + 48 : LCFloat(195);
    [self.historyImController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(0);
        make.top.mas_equalTo(height);
    }];
}

- (void)addOpeartionView{
    if (_opeartionView) return;
    self.opeartionView = [[BYILiveOpeartionView alloc] initWithIsHost:_isHost target:self];
    _opeartionView.liveId = _model.live_record_id;
    _opeartionView.hostId = _model.user_id;
    _opeartionView.hostName = _model.nickname;
    _opeartionView.aConversationController = self.imController;
    [_opeartionView setSupperNum:_model.count_support];
    @weakify(self);
    _opeartionView.didSuppertSucHandle = ^{
        @strongify(self);
        if (self.model.isSupport) return ;
        self.model.isSupport = YES;
        self.model.count_support++;
        [self.opeartionView setSupperNum:self.model.count_support];
    };
    [self.view addSubview:_opeartionView];
    [self.view bringSubviewToFront:_opeartionView];
    [_opeartionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(53);
        make.height.mas_equalTo(170);
        make.bottom.mas_equalTo(-130);
    }];
}

@end
