//
//  BYPPTRoomViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/8/21.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYPPTRoomViewModel.h"
#import "ShareRootViewController.h"
#import "BYIMChatViewController.h"
#import "BYPPTRoomController.h"
#import "BYPersonHomeController.h"

#import "BYLiveRoomSegmentView.h"
#import "BYILiveIntroView.h"
#import "BYLiveForumView.h"
#import "BYLiveReviewController.h"

#import "BYCommonLiveModel.h"
#import "BYPPTImgModel.h"


@interface BYPPTRoomViewModel()<BYLiveRoomSegmentViewDelegate>
{
    CGFloat _lastOffsetY;
    BOOL _isRefreshMas;
}

@property (nonatomic ,strong) BYLiveRoomSegmentView *segmentView;
/** 简介 */
@property (nonatomic ,strong) BYILiveIntroView *introView;
/** 聊天室 */
@property (nonatomic,strong) BYIMChatViewController *imChatController;

@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 点评 */
//@property (nonatomic ,strong) BYLiveForumView *forumView;
/** 点评 */
@property (nonatomic ,strong) BYLiveReviewController *reviewView;

@end

#define K_VC ((BYPPTRoomController *)self.S_VC)
@implementation BYPPTRoomViewModel

- (void)viewWillAppear{
    [S_V_NC setNavigationBarHidden:NO];
}

- (void)setContentView{
    @weakify(self);
    [K_VC rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_live_share"] barHltImage:nil action:^{
        @strongify(self);
        [self shareAction];
    }];
//    [self addSegmentView];
    [self loadRequestGetDetail];
}

// 分享
- (void)shareAction{
    if (!self.model.shareMap) return;
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.transferCopyUrl = self.model.shareMap.share_url;
    shareViewController.collection_status = self.model.collection_status;
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        NSString *mainImgUrl = [PDImageView appendingImgUrl:weakSelf.model.shareMap.share_img];
        NSString *imgurl = [mainImgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [ShareSDKManager shareManagerWithType:shareType title:weakSelf.model.shareMap.share_title desc:weakSelf.model.shareMap.share_intro img:imgurl url:weakSelf.model.shareMap.share_url callBack:NULL];
        [ShareSDKManager shareSuccessBack:shareTypeLive block:NULL];
    }];
    
    [shareViewController actionClickWithCollectionBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.model.collection_status = !strongSelf.model.collection_status;
    }];
    [shareViewController showInView:S_VC];
}

- (void)showHeaderViewAnimation:(BOOL)isShow{
    _isRefreshMas = YES;
    [S_V_NC setNavigationBarHidden:!isShow animated:YES];
    
    [S_V_VIEW updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        [self.segmentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(isShow ? 0 : -64 + kSafeAreaInsetsTop);
        }];
        [S_V_VIEW layoutIfNeeded];
    }];
}

- (void)subScrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint offset = scrollView.contentOffset;
    
    CGFloat temOffsetY = _lastOffsetY;
    _lastOffsetY = offset.y;

    // 在刚刷新布局时不做滑动判断
    if (_isRefreshMas) {
        _isRefreshMas = NO;
        return;
    }

    if (temOffsetY >= 80 && self.segmentView.frame.origin.y > (-64 + kSafeAreaInsetsTop)) {
        [self showHeaderViewAnimation:NO];
    }
    else if (offset.y <= 30 && self.segmentView.frame.origin.y <= (-64 + kSafeAreaInsetsTop)){
        [self showHeaderViewAnimation:YES];
    }
}

#pragma mark - BYLiveRoomSegmentViewDelegate
- (void)by_segmentViewWillScrollToIndex:(NSInteger)index{
    [self.imChatController.view endEditing:YES];
    [self.reviewView.view endEditing:YES];
    [self showHeaderViewAnimation:YES];
}

- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    if (index == 0) {
        return self.introView;
    }
    else if (index == 1){
        return self.imChatController.view;
    }
//    return self.forumView.view;
    return self.reviewView.view;
}

#pragma mark - request
- (void)loadRequestGetDetail{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLiveDetail:K_VC.live_record_id successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        if (!object) return ;
        self.model = object;
        // 解析ppt课件数据
        if (self.model.courseware && self.model.courseware.count) {
            NSMutableArray *tmpData = [NSMutableArray array];
            for (id data in self.model.courseware) {
                if ([data isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *dic = (NSDictionary *)data;
                    BYPPTImgModel *imgModel = [BYPPTImgModel mj_objectWithKeyValues:dic];
                    [tmpData addObject:imgModel];
                }
            }
            self.model.courseware = tmpData;
        }
        K_VC.navigationTitle = self.model.live_title;
        self.imChatController.model = self.model;
        [self.introView reloadData:object];
        if (self.model.status == BY_LIVE_STATUS_END ||
            self.model.status == BY_LIVE_STATUS_OVERTIME) {
            [self addSegmentView];
        }
        else{
            @weakify(self);
            [[BYSIMManager shareManager] joinRoom:self.model.group_id suc:^{
                @strongify(self);
                [self addSegmentView];
                showDebugToastView(@"进入房间成功", kCommonWindow);
            } fail:^{
                showDebugToastView(@"进入房间失败", kCommonWindow);
            }];
        }
        
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - configUI
- (void)addSegmentView{
    self.segmentView = [[BYLiveRoomSegmentView alloc] initWithTitles:@"简介",@"直播",@"点评", nil];
    _segmentView.delegate = self;
    _segmentView.defultSelIndex = 1;
    [S_V_VIEW addSubview:self.segmentView];
    [_segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}


- (BYILiveIntroView *)introView{
    if (!_introView) {
        _introView = [[BYILiveIntroView alloc] init];
        @weakify(self);
        _introView.tapHeaderImgHandle = ^{
            @strongify(self);
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = self.model.user_id;
            [S_V_NC pushViewController:personHomeController animated:YES];
        };
    }
    return _introView;
}

- (BYIMChatViewController *)imChatController{
    if (!_imChatController) {
        _imChatController = [[BYIMChatViewController alloc] init];
        _imChatController.model = _model;
        [_imChatController.view setBackgroundColor:kColorRGBValue(0xfafafa)];
        _imChatController.viewController = S_VC;
        _imChatController.isHost = K_VC.isHost;
        [self.viewController addChildViewController:_imChatController];
        @weakify(self);
        _imChatController.scrollViewDidScrollView = ^(UIScrollView * _Nonnull scrollView) {
            @strongify(self);
            [self subScrollViewDidScroll:scrollView];
        };
    }
    return _imChatController;
}

//- (BYLiveForumView *)forumView{
//    if (!_forumView) {
//        _forumView = [[BYLiveForumView alloc] initWithFatureController:nil isReport:NO];
//        _forumView.transferRoomId = K_VC.live_record_id;
//        _forumView.viewController = S_VC;
//        _forumView.needAdaptiveBottom = YES;
//        [S_VC addChildViewController:_forumView];
//    }
//    return _forumView;
//}

- (BYLiveReviewController *)reviewView{
    if (!_reviewView) {
        _reviewView = [[BYLiveReviewController alloc] init];
        _reviewView.theme_id = K_VC.live_record_id;
        [S_VC addChildViewController:_reviewView];
    }
    return _reviewView;
}

@end
