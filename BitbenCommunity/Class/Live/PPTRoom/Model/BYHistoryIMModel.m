//
//  BYHistoryIMModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/14.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYHistoryIMModel.h"

@implementation BYHistoryIMModel

@synthesize audioSufix;

@synthesize audioText;

@synthesize audioTime;

@synthesize bubbleWidth;

@synthesize cellHeight;

@synthesize chatConfig;

@synthesize createTime;

@synthesize ctDataconfig;

@synthesize isLeft;

@synthesize messageId;

@synthesize modalInfo;

@synthesize msg;

@synthesize msgState;

@synthesize msgType;

@synthesize reuseIdentifierForCustomeCell;

@synthesize textlayout;

@synthesize userName;

@synthesize userThumImage;

@synthesize userThumImageURL;

@synthesize willDisplayTime;

@synthesize identity;

@synthesize identity_color;

@synthesize identity_type;

@synthesize isRead;

@synthesize hasReward;

@synthesize hasBanned;

@synthesize bubbleSize;

@synthesize userId;

@synthesize customData;

@synthesize audioPath;

@synthesize msg_image;

@synthesize cert_bagde;

+ (NSArray *)getTableData:(NSArray *)responData liveData:(BYCommonLiveModel *)liveData{
    NSMutableArray *data = [NSMutableArray array];
    NSMutableArray *tmpMuArr = [NSMutableArray arrayWithArray:responData];
//    NSArray *tmpArr = [[tmpMuArr reverseObjectEnumerator] allObjects];
    for (NSDictionary *tmpdic in tmpMuArr) {
        NSDictionary *dic = tmpdic[@"message"];
        BYHistoryIMModel *model = [[BYHistoryIMModel alloc] init];
        model.isRead = NO;
        model.hasReward = YES;
        model.hasBanned = NO;
        model.createTime = stringFormatInteger([dic[@"time"] integerValue]);
        model.isLeft = YES;
        model.userId = nullToEmpty(dic[@"from_id"][@"uid"]);
        if ([liveData.user_id isEqualToString:model.userId]) {
            model.identity = @"主持人";
        }
        model.userName = dic[@"nickname"];
        model.cert_bagde = [dic[@"cert_bagde"] integerValue];
        model.messageId = [NSString stringWithFormat:@"%i",[dic[@"uuid"] intValue]];
        model.userThumImageURL = [NSString stringWithFormat:@"%@%@",[PDImageView getUploadBucket:dic[@"head_img"]],dic[@"head_img"]];
        model.msgState = CDMessageStateNormal;
        ChatConfiguration *chatConfig = [[ChatConfiguration alloc] init];
        chatConfig.icon_head = @"icon_main_logo";
        chatConfig.headSideLength = 36;
        chatConfig.msgBackGroundColor = [UIColor whiteColor];
        chatConfig.msgContentBackGroundColor = [UIColor whiteColor];
//        chatConfig.headSideLength = 18.0f;
//        chatConfig.messageMargin = 15.0f;
        model.chatConfig = chatConfig;
        NSArray *contentArr = dic[@"content"][@"message_item"];
        if ([contentArr count] == 1) {
            NSDictionary *tmpDic = contentArr[0];
            if ([contentArr[0][@"type"] isEqualToString:@"T"]) { // 语音
                model.msg = tmpDic[@"value"];
                model.msgType = CDMessageTypeAudio;
                model.msgState = CDMessageStateSending;
                model.audioSufix = @"amr";
                model.audioTime = [[model.msg componentsSeparatedByString:@"duration="].lastObject intValue];
                model.isRead = [[BYIMMessageDB shareManager] selectIsReadWhereMsgId:model.messageId];
                // 语音消息已读状态存储
                [[BYIMMessageDB shareManager] addIMMessages:model];
            }
            else{
                continue;
            }
        }
        else{
            NSDictionary *tmpDic = contentArr[0];
            if ([tmpDic[@"type"] isEqualToString:@"T"]) { // 纯文案
                model.msg = tmpDic[@"value"];
                model.msgType = CDMessageTypeText;
            }
            else if ([tmpDic[@"type"] isEqualToString:@"P"]) { // 图片
                model.msg = tmpDic[@"value"];
                model.msgType = CDMessageTypeImage;
            }
        }
        [data addObject:model];
    }
    return data;
}

+ (NSArray *)getSIMTableData:(NSArray *)respondData liveData:(BYCommonLiveModel *)liveData{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dic in respondData) {
        BYHistoryIMModel *model = [[BYHistoryIMModel alloc] init];
        model.isRead = NO;
        model.hasReward = NO;
        model.hasBanned = NO;
        model.createTime = stringFormatInteger([dic[@"send_time"] integerValue]);
        model.isLeft = YES;
        model.userId = dic[@"user_id"];
        model.cert_bagde = [dic[@"cert_badge"] integerValue];
         model.userName = dic[@"nickname"];
        BY_SIM_MSG_TYPE type = [dic[@"content_type"] integerValue];
        if ([liveData.user_id isEqualToString:model.userId]) {
            model.identity = @"主持人";
        }
        model.messageId = dic[@"chat_id"];
        model.userThumImageURL = [NSString stringWithFormat:@"%@%@",[PDImageView getUploadBucket:dic[@"head_img"]],dic[@"head_img"]];
        model.msgState = CDMessageStateNormal;
        ChatConfiguration *chatConfig = [[ChatConfiguration alloc] init];
        chatConfig.icon_head = @"icon_main_logo";
        chatConfig.headSideLength = 36;
        chatConfig.msgBackGroundColor = [UIColor whiteColor];
        chatConfig.msgContentBackGroundColor = [UIColor whiteColor];
        chatConfig.sysInfoPadding = 6.0f;
        chatConfig.sysInfoMessageFont = [UIFont systemFontOfSize:11.0f];
        model.chatConfig = chatConfig;
        
        switch (type) {
            case BY_SIM_MSG_TYPE_TEXT:
            {
                model.msg = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
                model.msgType = CDMessageTypeText;
            }
                break;
            case BY_SIM_MSG_TYPE_IMAGE:
            {
                model.msgState = CDMessageStateDownloading;
                model.msg = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
                model.msgType = CDMessageTypeImage;
            }
                break;
            case BY_SIM_MSG_TYPE_AUDIO:
            {
                model.msg = nullToEmpty(dic[@"content"]);
                model.audioSufix = @"wav";
                model.msgType = CDMessageTypeAudio;
                model.isRead = [[BYIMMessageDB shareManager] selectIsReadWhereMsgId:model.messageId];
                // 语音消息已读状态存储
                [[BYIMMessageDB shareManager] addIMMessages:model];
            }
                break;
            case BY_SIM_MSG_TYPE_CUSTOME:
            {
                model.msgType = CDMessageTypeCustome;
                model.reuseIdentifierForCustomeCell = @"RewardCell";
                NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                model.customData = customData;
            }
                break;
            case BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW:
            {
                model.msg = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
                model.msgType = CDMessageTypeSystemInfo;
            }
                break;
            default:
                break;
        }
        [array addObject:model];
    }

    return array;
}

+ (NSArray *)getNoSendImgMsgData:(NSArray *)images liveData:(BYCommonLiveModel *)liveData{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < images.count; i ++) {
        UIImage *image = images[i];
        BYHistoryIMModel *model = [[BYHistoryIMModel alloc] init];
        model.isRead = NO;
        model.hasReward = NO;
        model.hasBanned = NO;
        model.createTime = [NSDate getCurrentTimeStr];
        model.userId = [AccountModel sharedAccountModel].account_id;
        model.isLeft = [model.userId isEqualToString:[AccountModel sharedAccountModel].account_id] ? NO : YES;
        model.userName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
        model.msgType = CDMessageTypeImage;
        model.cert_bagde = LOGIN_MODEL.user.cert_badge;
        BY_IDENTITY_TYPE identity = [BYHistoryIMModel verifyIdentithy:liveData userId:model.userId];
        model.identity_type = identity;
//        if ([liveData.user_id isEqualToString:[AccountModel sharedAccountModel].account_id]) {
//            model.identity = @"主持人";
//        }
        switch (identity) {
            case BY_IDENTITY_TYPE_HOST:
                model.identity = @"主持人";
                model.identity_color = kColorRGBValue(0xf7b500);
                break;
            case BY_IDENTITY_TYPE_GUEST:
                model.identity = @"嘉宾";
                model.identity_color = kColorRGBValue(0xea6438);
                break;
            case BY_IDENTITY_TYPE_CREATOR:
                model.identity = @"助理";
                model.identity_color = kColorRGBValue(0x6384ff);
                break;
            default:
                model.identity = @"";
                break;
        }
        NSString *headUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
        model.userThumImageURL = [NSString stringWithFormat:@"%@%@",[PDImageView getUploadBucket:headUrl],headUrl];
        model.msgState = CDMessageStateSending;
        ChatConfiguration *chatConfig = [[ChatConfiguration alloc] init];
        chatConfig.icon_head = @"icon_main_logo";
        chatConfig.headSideLength = 40;
        chatConfig.msgBackGroundColor = [UIColor whiteColor];
        chatConfig.msgContentBackGroundColor = kColorRGBValue(0xf2f4f5);
        chatConfig.nickNameColor = kColorRGBValue(0x8f8f8f);
        chatConfig.bubbleSharpAnglehorizInset = 8;
        chatConfig.bubbleShareAngleWidth = -1;
        chatConfig.sysInfoPadding = 6.0f;
        chatConfig.nickNameHeight = [UIFont systemFontOfSize:12].pointSize;
        chatConfig.sysInfoMessageFont = [UIFont systemFontOfSize:11.0f];
        model.chatConfig = chatConfig;
        model.msg_image = image;
        [array addObject:model];
    }
    return array;
}

+ (NSArray *)getMicroChatTableData:(NSArray *)respondData liveData:(BYCommonLiveModel *)liveData{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dic in respondData) {
        BYHistoryIMModel *model = [[BYHistoryIMModel alloc] init];
        model.isRead = NO;
        model.hasReward = NO;
        model.hasBanned = NO;
        model.createTime = stringFormatInteger([dic[@"send_time"] integerValue]);
        model.userId = dic[@"user_id"];
        model.isLeft = [model.userId isEqualToString:[AccountModel sharedAccountModel].account_id] ? NO : YES;
        model.cert_bagde = [dic[@"cert_badge"] integerValue];
        model.userName = dic[@"nickname"];
        BY_SIM_MSG_TYPE type = [dic[@"content_type"] integerValue];
//        if ([liveData.user_id isEqualToString:model.userId]) {
//            model.identity = @"主持人";
//        }
//        BY_IDENTITY_TYPE identity = [BYHistoryIMModel verifyIdentithy:liveData userId:model.userId];
        BY_IDENTITY_TYPE identity = [dic[@"identity_type"] integerValue];
        model.identity_type = identity;
        model.messageId = dic[@"chat_id"];
        model.userThumImageURL = [NSString stringWithFormat:@"%@%@",[PDImageView getUploadBucket:dic[@"head_img"]],dic[@"head_img"]];
        model.msgState = CDMessageStateNormal;
        ChatConfiguration *chatConfig = [[ChatConfiguration alloc] init];
        chatConfig.icon_head = @"icon_main_logo";
        chatConfig.headSideLength = 40;
        chatConfig.msgBackGroundColor = [UIColor whiteColor];
        chatConfig.msgContentBackGroundColor = kColorRGBValue(0xf2f4f5);
        chatConfig.nickNameColor = kColorRGBValue(0x8f8f8f);
        chatConfig.bubbleSharpAnglehorizInset = 8;
        chatConfig.bubbleShareAngleWidth = -1;
        chatConfig.sysInfoPadding = 6.0f;
        chatConfig.nickNameHeight = [UIFont systemFontOfSize:12].pointSize;
        chatConfig.sysInfoMessageFont = [UIFont systemFontOfSize:11.0f];
        model.chatConfig = chatConfig;
        
        if (identity != BY_IDENTITY_TYPE_NORMAL) {
            continue;
        }
        
        switch (type) {
            case BY_SIM_MSG_TYPE_TEXT:
            {
                model.msg = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
                model.msgType = CDMessageTypeText;
                [array addObject:model];
            }
                break;
            default:
                break;
        }
    }
        return array;
}

+ (NSArray *)getMicroLiveTableData:(NSArray *)respondData liveData:(BYCommonLiveModel *)liveData{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dic in respondData) {
        BYHistoryIMModel *model = [[BYHistoryIMModel alloc] init];
        model.isRead = NO;
        model.hasReward = NO;
        model.hasBanned = NO;
        model.createTime = stringFormatInteger([dic[@"send_time"] integerValue]);
        model.userId = dic[@"user_id"];
        model.isLeft = [model.userId isEqualToString:[AccountModel sharedAccountModel].account_id] ? NO : YES;
        model.cert_bagde = [dic[@"cert_badge"] integerValue];
        model.userName = dic[@"nickname"];
        BY_SIM_MSG_TYPE type = [dic[@"content_type"] integerValue];
//        BY_IDENTITY_TYPE identity = [BYHistoryIMModel verifyIdentithy:liveData userId:model.userId];
        BY_IDENTITY_TYPE identity = [dic[@"identity_type"] integerValue];
        model.identity_type = identity;
        switch (identity) {
            case BY_IDENTITY_TYPE_HOST:
                model.identity = @"主持人";
                model.identity_color = kColorRGBValue(0xf7b500);
                break;
            case BY_IDENTITY_TYPE_GUEST:
                model.identity = @"嘉宾";
                model.identity_color = kColorRGBValue(0xea6438);
                break;
            case BY_IDENTITY_TYPE_CREATOR:
                model.identity = @"助理";
                model.identity_color = kColorRGBValue(0x6384ff);
                break;
            default:
                model.identity = @"";
                break;
        }
//        if ([liveData.user_id isEqualToString:model.userId]) {
//            model.identity = @"主持人";
//        }
        model.messageId = dic[@"chat_id"];
        model.userThumImageURL = [NSString stringWithFormat:@"%@%@",[PDImageView getUploadBucket:dic[@"head_img"]],dic[@"head_img"]];
        model.msgState = CDMessageStateNormal;
        ChatConfiguration *chatConfig = [[ChatConfiguration alloc] init];
        chatConfig.icon_head = @"icon_main_logo";
        chatConfig.headSideLength = 40;
        chatConfig.msgBackGroundColor = [UIColor whiteColor];
        chatConfig.msgContentBackGroundColor = kColorRGBValue(0xf2f4f5);
        chatConfig.nickNameColor = kColorRGBValue(0x8f8f8f);
        chatConfig.bubbleSharpAnglehorizInset = 8;
        chatConfig.bubbleShareAngleWidth = -1;
        chatConfig.sysInfoPadding = 6.0f;
        chatConfig.nickNameHeight = [UIFont systemFontOfSize:12].pointSize;
        chatConfig.sysInfoMessageFont = [UIFont systemFontOfSize:11.0f];
        model.chatConfig = chatConfig;
        if (identity == BY_IDENTITY_TYPE_NORMAL) {
            if (type == BY_SIM_MSG_TYPE_ASK) {
                model.msg = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
                model.msgType = CDMessageTypeAsk;
                [array addObject:model];
            }else if (type == BY_SIM_MSG_TYPE_CUSTOME) {
                model.msgType = CDMessageTypeCustome;
                model.reuseIdentifierForCustomeCell = @"RewardCell";
                NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                model.customData = customData;
                [array addObject:model];
            }
            continue;
        }
        switch (type) {
            case BY_SIM_MSG_TYPE_TEXT:
            {
                model.msg = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
                model.msgType = CDMessageTypeText;
                [array addObject:model];
            }
                break;
            case BY_SIM_MSG_TYPE_IMAGE:
            {
                model.msgState = CDMessageStateDownloading;
                model.msg = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
                model.msgType = CDMessageTypeImage;
                [array addObject:model];
            }
                break;
            case BY_SIM_MSG_TYPE_AUDIO:
            {
                model.msg = nullToEmpty(dic[@"content"]);
                model.audioSufix = @"wav";
                model.msgType = CDMessageTypeAudio;
                model.isRead = [[BYIMMessageDB shareManager] selectIsReadWhereMsgId:model.messageId];
                // 语音消息已读状态存储
                [[BYIMMessageDB shareManager] addIMMessages:model];
                [array addObject:model];
            }
                break;
            case BY_SIM_MSG_TYPE_CUSTOME:
            {
                model.msgType = CDMessageTypeCustome;
                model.reuseIdentifierForCustomeCell = @"RewardCell";
                NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                model.customData = customData;
                if (customData[@"text"]) {
                    [array addObject:model];
                }
            }
                break;
            default:
                break;
        }
    }
    return array;
}

+ (BY_IDENTITY_TYPE )verifyIdentithy:(BYCommonLiveModel *)model userId:(NSString *)userId{
    __block BOOL hasResult = NO;
    if (model.host_list.count) {
        [model.host_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.user_id isEqualToString:userId]) {
                *stop = YES;
                hasResult = YES;
            }
        }];
        if (hasResult) {
            return BY_IDENTITY_TYPE_HOST;
        }
    }
    if (model.interviewee_list.count) {
        [model.interviewee_list enumerateObjectsUsingBlock:^(BYCommonLiveGuestModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.user_id isEqualToString:userId]) {
                *stop = YES;
                hasResult = YES;
            }
        }];
        if (hasResult) {
            return BY_IDENTITY_TYPE_GUEST;
        }
    }
    if ([model.user_id isEqualToString:userId]) {
        return BY_IDENTITY_TYPE_CREATOR;
    }
    return BY_IDENTITY_TYPE_NORMAL;
}

@end
