//
//  BYLiveReviewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveReviewModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

@implementation BYLiveReviewModel

+ (NSArray *)getReviewTableData:(NSArray *)respond{
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (!respond.count) {
        return @[];
    }
    NSMutableArray *data = [NSMutableArray array];
    for (NSDictionary *dic in respond) {
        BYLiveReviewModel *model = [BYLiveReviewModel mj_objectWithKeyValues:dic];
        model.show_time = [NSString stringWithFormat:@"%@ · ",[NSDate getTimeGap:model.create_time / 1000.]];
        
        NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc]initWithString:model.content];
        attributed.yy_color = [UIColor whiteColor];
        attributed.yy_lineSpacing = 4;
        attributed.yy_font = [UIFont systemFontOfSize:15];
        YYTextContainer *minTextContainer = [YYTextContainer new];
        minTextContainer.size = CGSizeMake(kCommonScreenWidth - 83, 90);
        YYTextContainer *maxTextContainer = [YYTextContainer new];
        maxTextContainer.size = CGSizeMake(kCommonScreenWidth - 83, MAXFLOAT);
        YYTextLayout *minLayout = [YYTextLayout layoutWithContainer:minTextContainer text:attributed];
        YYTextLayout *maxLayout = [YYTextLayout layoutWithContainer:maxTextContainer text:attributed];

        model.isShowMaxH = NO;
        model.minContentH = ceil(minLayout.textBoundingSize.height);
        model.maxContentH = ceil(maxLayout.textBoundingSize.height);
        model.minCellHeight = model.minContentH + 46 + 35;
        model.maxCellHeight = model.maxContentH + 46 + 35;
        model.cellHeight = model.minCellHeight;
        model.cellString = @"BYLiveReviewCell";
        [data addObject:model];
    }
    return data;
}

- (BYLiveReviewModel *)getSingleData:(NSString *)content{
    self.user_id = [AccountModel sharedAccountModel].account_id;
    self.comment_name = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
    self.comment_pic = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
    self.content = content;
    self.create_time = [NSDate getNSTimeIntervalWithCurrent] * 1000;
    self.show_time = [NSString stringWithFormat:@"%@ · ",[NSDate getTimeGap:self.create_time / 1000.]];
    self.count_support = 0;
    self.isSupport = NO;
    self.reward = @"待揭晓";
    self.cert_badge = [AccountModel sharedAccountModel].loginServerModel.user.cert_badge;
    self.achievement_badge_list = [AccountModel sharedAccountModel].loginServerModel.user.achievement_badge_list;
    
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc]initWithString:self.content];
    attributed.yy_color = [UIColor whiteColor];
    attributed.yy_lineSpacing = 4;
    attributed.yy_font = [UIFont systemFontOfSize:15];
    YYTextContainer *minTextContainer = [YYTextContainer new];
    minTextContainer.size = CGSizeMake(kCommonScreenWidth - 83, 90);
    YYTextContainer *maxTextContainer = [YYTextContainer new];
    maxTextContainer.size = CGSizeMake(kCommonScreenWidth - 83, MAXFLOAT);
    YYTextLayout *minLayout = [YYTextLayout layoutWithContainer:minTextContainer text:attributed];
    YYTextLayout *maxLayout = [YYTextLayout layoutWithContainer:maxTextContainer text:attributed];
    
    self.isShowMaxH = NO;
    self.minContentH = ceil(minLayout.textBoundingSize.height);
    self.maxContentH = ceil(maxLayout.textBoundingSize.height);
    self.minCellHeight = self.minContentH + 46 + 35;
    self.maxCellHeight = self.maxContentH + 46 + 35;
    self.cellHeight = self.minCellHeight;
    self.cellString = @"BYLiveReviewCell";
    
    return self;
}

- (LiveFourumRootListSingleModel *)transSameModel{
    LiveFourumRootListSingleModel *model = [[LiveFourumRootListSingleModel alloc] init];
    NSDictionary *dic = [self mj_keyValues];
    [model mj_setKeyValues:dic];
    return model;
}
@end
