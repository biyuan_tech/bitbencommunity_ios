//
//  BYLiveReviewModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"
#import "LiveFourumRootListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYLiveReviewModel : BYCommonModel

/** 回复评论者内容 */
@property (nonatomic ,strong) NSArray *child;
/** 回复数 */
@property (nonatomic ,assign) NSInteger childCount;
/** 评论id */
@property (nonatomic ,copy) NSString *comment_id;
@property (nonatomic ,copy) NSString *_id;

/** 用户id */
@property (nonatomic ,copy) NSString *user_id;
/** 评论者昵称 */
@property (nonatomic ,copy) NSString *comment_name;
/** 评论者头像 */
@property (nonatomic ,copy) NSString *comment_pic;
/** 评论内容 */
@property (nonatomic ,copy) NSString *content;
/** 评论时间 */
@property (nonatomic ,assign) NSTimeInterval create_time;
/** 显示的评论时间 */
@property (nonatomic ,copy) NSString *show_time;
/** 点赞数 */
@property (nonatomic ,assign) NSInteger count_support;
/** 点赞状态 */
@property (nonatomic ,assign) BOOL isSupport;
/** bbt奖励 */
@property (nonatomic ,copy) NSString *reward;
/** vip认证 */
@property (nonatomic ,assign) NSInteger cert_badge;
/** 成就组 */
@property (nonatomic ,strong) NSArray <NSString *>*achievement_badge_list;
/** 最高可展示cell高度 */
@property (nonatomic ,assign) CGFloat maxCellHeight;
/** 缩略cell高度 */
@property (nonatomic ,assign) CGFloat minCellHeight;
/** 最高可展示content高度 */
@property (nonatomic ,assign) CGFloat maxContentH;
/** 缩略content高度 */
@property (nonatomic ,assign) CGFloat minContentH;
/** 是否显示查看全文 */
@property (nonatomic ,assign) BOOL isShowMaxH;

/**  */
@property (nonatomic ,copy) NSString *reply_comment_name;
/**  */
@property (nonatomic ,copy) NSString *reply_comment_pic;
/**  */
@property (nonatomic ,copy) NSString *reply_user_id;
/**  */
@property (nonatomic ,copy) NSString *theme_id;
/**  */
@property (nonatomic ,assign) NSInteger theme_type;
/**  */
@property (nonatomic ,copy) NSString *nickname;



+ (NSArray *)getReviewTableData:(NSArray *)respond;

- (BYLiveReviewModel *)getSingleData:(NSString *)content;

- (LiveFourumRootListSingleModel *)transSameModel;

@end

NS_ASSUME_NONNULL_END
