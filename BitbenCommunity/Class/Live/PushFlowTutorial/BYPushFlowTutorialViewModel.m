//
//  BYPushFlowTutorialViewModel.m
//  BibenCommunity
//
//  Created by 随风 on 2019/1/17.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYPushFlowTutorialViewModel.h"
#import "BYCommonPlayerView.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>
#import "ArticleDetailRootViewController.h"
#import "BYPushFlowTutorialController.h"

#define K_VC ((BYPushFlowTutorialController *)S_VC)
@interface BYPushFlowTutorialViewModel ()

/** scrollView */
@property (nonatomic ,strong) UIScrollView *scrollView;
/** playerView */
@property (nonatomic ,strong) BYCommonPlayerView *playerView;
/** pushUrlLab */
@property (nonatomic ,strong) YYLabel *pushUrlLab;


@end

@implementation BYPushFlowTutorialViewModel
- (void)dealloc
{
    [_playerView resetPlayer];
    [_playerView removeFromSuperview];
    _playerView = nil;
}

- (void)viewWillAppear{
    [_playerView resume];
}

- (void)viewDidDisappear{
    [_playerView pause];
}

- (void)setContentView{
    [self configUI];
    [self getPushFlowURLRequest];
}

#pragma mark - request
- (void)getPushFlowURLRequest{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetPushURL:K_VC.live_record_id successBlock:^(id object) {
        @strongify(self);
        NSString *pushUrl = nullToEmpty(object[@"pushUrl"]);
        NSString *streamCode = nullToEmpty(object[@"streamCode"]);
        NSString *string = [NSString stringWithFormat:@"%@%@",pushUrl,streamCode];
        //        self.textView.text = [NSString stringWithFormat:@"%@\n%@",pushUrl,streamCode];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
        attributedString.yy_font = [UIFont systemFontOfSize:13];
        attributedString.yy_color = kColorRGBValue(0x272727);
        @weakify(self);
        [attributedString yy_setTextHighlightRange:NSMakeRange(0, string.length) color:kColorRGBValue(0x272727) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            @strongify(self);
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            NSString *string = [text.string substringWithRange:range];
            [pasteboard setString:string];
            showToastView(@"已复制推流地址", S_V_VIEW);
        }];
        self.pushUrlLab.attributedText = attributedString;
        CGSize size = [string getStringSizeWithFont:self.pushUrlLab.font maxWidth:kCommonScreenWidth - 30];
        [self.pushUrlLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(ceil(size.width));
            make.height.mas_equalTo(ceil(size.height));
        }];
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"推流地址获取失败", S_V_VIEW);
    }];
}

#pragma mark - UI
- (void)configUI{
    self.scrollView = [UIScrollView by_init];
    [S_V_VIEW addSubview:self.scrollView];
    [self.scrollView  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    UILabel *titleLab = [UILabel by_init];
    titleLab.text = @"推流地址";
    titleLab.textColor = kColorRGBValue(0x272727);
    [titleLab setBy_font:13];
    [self.scrollView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(40);
    }];
    
    UIView *bgView = [[UIView alloc] init];
    [bgView setBackgroundColor:[UIColor whiteColor]];
    [self.scrollView addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(kCommonScreenWidth);
        make.top.mas_equalTo(40);
        make.height.mas_equalTo(78);
    }];
    
    YYLabel *pushUrlLab = [YYLabel by_init];
    pushUrlLab.numberOfLines = 0;
    [bgView addSubview:pushUrlLab];
    self.pushUrlLab = pushUrlLab;
    [pushUrlLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(17);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_greaterThanOrEqualTo(0);
    }];

    UILabel *introLab = [UILabel by_init];
    [introLab setBy_font:13];
    introLab.numberOfLines = 0;
    introLab.textColor = kColorRGBValue(0x272727);
    introLab.text = @"如何使用OBS推流软件进行直播，请参考说明文档与演示教程：";
    [self.scrollView addSubview:introLab];
    CGSize introSize = [introLab.text getStringSizeWithFont:introLab.font maxWidth:kCommonScreenWidth - 30];
    [introLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.top.mas_equalTo(bgView.mas_bottom).mas_offset(20);
        make.height.mas_equalTo(ceil(introSize.height));
    }];
    
    UIView *fatherVIew = [UIView by_init];
    [self.scrollView addSubview:fatherVIew];
    [fatherVIew mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(introLab.mas_bottom).mas_offset(20);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.height.mas_equalTo((kCommonScreenWidth - 30)*9/16);
    }];
    
    // 播放器
    BYCommonPlayerView *playerView = [[BYCommonPlayerView alloc] init];
    playerView.fatherView = fatherVIew;
    self.playerView = playerView;
    [playerView setShowWatchNum:NO];
    NSString *audioPath = [[NSBundle mainBundle] pathForResource:@"tutorial.mp4" ofType:@""];
    [playerView playVideoWithUrl:audioPath title:@""];
    
    // obs下载
    YYLabel *downOBSUrlLab = [YYLabel by_init];
    NSString *downOBSUrl = @"一、OBS下载地址：https://obsproject.com/";
    NSRange downOBSUrlRange = NSMakeRange(10, downOBSUrl.length - 10);
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:downOBSUrl];
    attributedString.yy_font = [UIFont systemFontOfSize:13];
    attributedString.yy_color = kColorRGBValue(0x272727);
    [attributedString yy_setColor:kColorRGBValue(0xed4a45) range:NSMakeRange(10, downOBSUrl.length - 10)];
    @weakify(self);
    [attributedString yy_setTextHighlightRange:downOBSUrlRange color:kColorRGBValue(0xed4a45) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        @strongify(self);
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        NSString *string = [text.string substringWithRange:range];
        [pasteboard setString:string];
        showToastView(@"已复制链接", S_V_VIEW);
    }];
    downOBSUrlLab.attributedText = attributedString;
    [self.scrollView addSubview:downOBSUrlLab];
    [downOBSUrlLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(fatherVIew.mas_bottom).mas_offset(25);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.height.mas_equalTo(stringGetHeight(downOBSUrl, 13));
    }];
    
    // Mshow下载
    YYLabel *downMshowUrlLab = [YYLabel by_init];
    NSString *downMshowUrl = @"二、Mshow下载地址：https://mshow.yy.com/";
    NSRange downMshowUrlRange = NSMakeRange(12, downMshowUrl.length - 12);
    NSMutableAttributedString *attributedString1 = [[NSMutableAttributedString alloc] initWithString:downMshowUrl];
    attributedString1.yy_font = [UIFont systemFontOfSize:13];
    [attributedString1 yy_setColor:kColorRGBValue(0xed4a45) range:NSMakeRange(12, downMshowUrl.length - 12)];
    [attributedString1 yy_setTextHighlightRange:downMshowUrlRange color:kColorRGBValue(0xed4a45) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        @strongify(self);
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        NSString *string = [text.string substringWithRange:range];
        [pasteboard setString:string];
        showToastView(@"已复制链接", S_V_VIEW);
    }];
    downMshowUrlLab.attributedText = attributedString1;
    [self.scrollView addSubview:downMshowUrlLab];
    [downMshowUrlLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(downOBSUrlLab.mas_bottom).mas_offset(15);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.height.mas_equalTo(stringGetHeight(downMshowUrl, 13));
    }];
    
    // OBS教程
    YYLabel *OBSTorialLab = [YYLabel by_init];
    NSString *OBSTorialStr = @"三、查看OBS图文教程";
    NSRange OBSTorialRange = NSMakeRange(2, OBSTorialStr.length - 2);
    NSMutableAttributedString *OBSAttributedString = [[NSMutableAttributedString alloc] initWithString:OBSTorialStr];
    OBSAttributedString.yy_font = [UIFont systemFontOfSize:13];
    OBSAttributedString.yy_color = kColorRGBValue(0x272727);
    [OBSAttributedString yy_setColor:kColorRGBValue(0xed4a45) range:OBSTorialRange];
    [OBSAttributedString yy_setTextHighlightRange:OBSTorialRange color:kColorRGBValue(0xed4a45) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        @strongify(self);
        ArticleDetailRootViewController *articleController = [[ArticleDetailRootViewController alloc] init];
        articleController.transferArticleId = @"537317564867411968";
        articleController.transferPageType = ArticleDetailRootViewControllerTypeBanner;
        [S_VC authorizePush:articleController animation:YES];
    }];
    OBSTorialLab.attributedText = OBSAttributedString;
    [self.scrollView addSubview:OBSTorialLab];
    [OBSTorialLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(downMshowUrlLab.mas_bottom).mas_offset(15);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.height.mas_equalTo(stringGetHeight(OBSTorialStr, 13));
    }];
    
    // Mshow教程
    YYLabel *MshowTorialLab = [YYLabel by_init];
    NSString *MshowTorialStr = @"四、查看Mshow图文教程";
    NSRange MshowTorialRange = NSMakeRange(2, MshowTorialStr.length - 2);
    NSMutableAttributedString *MshowAttributedString = [[NSMutableAttributedString alloc] initWithString:MshowTorialStr];
    MshowAttributedString.yy_font = [UIFont systemFontOfSize:13];
    MshowAttributedString.yy_color = kColorRGBValue(0x272727);
    [MshowAttributedString yy_setColor:kColorRGBValue(0xed4a45) range:MshowTorialRange];
    [MshowAttributedString yy_setTextHighlightRange:MshowTorialRange color:kColorRGBValue(0xed4a45) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        @strongify(self);
        ArticleDetailRootViewController *articleController = [[ArticleDetailRootViewController alloc] init];
        articleController.transferArticleId = @"537320165986009088";
        articleController.transferPageType = ArticleDetailRootViewControllerTypeBanner;
        [S_VC authorizePush:articleController animation:YES];

    }];
    MshowTorialLab.attributedText = MshowAttributedString;
    [self.scrollView addSubview:MshowTorialLab];
    [MshowTorialLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(OBSTorialLab.mas_bottom).mas_offset(15);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.height.mas_equalTo(stringGetHeight(MshowTorialStr, 13));
    }];
    
    [self.scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.bottom.mas_equalTo(MshowTorialLab.mas_bottom).offset(50).priorityLow();
        make.bottom.mas_greaterThanOrEqualTo(S_V_VIEW);
    }];
}

@end
