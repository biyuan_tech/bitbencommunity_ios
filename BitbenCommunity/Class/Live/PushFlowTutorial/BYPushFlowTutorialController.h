//
//  BYPushFlowTutorialController.h
//  BibenCommunity
//
//  Created by 随风 on 2019/1/17.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYPushFlowTutorialController : BYCommonViewController

/** 直播记录id */
@property (nonatomic ,copy) NSString *live_record_id;


@end

NS_ASSUME_NONNULL_END
