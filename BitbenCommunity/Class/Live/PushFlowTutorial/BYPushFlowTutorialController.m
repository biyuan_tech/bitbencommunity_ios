//
//  BYPushFlowTutorialController.m
//  BibenCommunity
//
//  Created by 随风 on 2019/1/17.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYPushFlowTutorialController.h"
#import "BYPushFlowTutorialViewModel.h"

@interface BYPushFlowTutorialController ()

@end

@implementation BYPushFlowTutorialController

- (void)dealloc{
    
}

- (Class)getViewModelClass{
    return [BYPushFlowTutorialViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"直播设置";
    [self.view setBackgroundColor:kColorRGBValue(0xf8f8f8)];
}


@end
