//
//  BYLiveHomeRequest.m
//  BY
//
//  Created by 黄亮 on 2018/7/27.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLiveHomeRequest.h"
#import "BYLiveRPCDefine.h"

#import "BYLiveHomeVideoModel.h"
#import "BYCommonLiveModel.h"
#import "BYHomeBannerModel.h"
#import "BYPersonHomeHeaderModel.h"
#import "BYAppraiseModel.h"
#import "BYRecommendUserModel.h"
#import "BYHomeArticleModel.h"
#import "BYSearchHisModel.h"
#import "BYHomeCourseModel.h"
#import "BYCourseDetailModel.h"
#import "BYHomeTopicModel.h"
#import "BYLiveReviewModel.h"
#import "BYOfficialCertGroupModel.h"
#import "BYActivityModel.h"
#import "BYActivityDetailSignModel.h"

@implementation BYLiveHomeRequest

- (void)loadRequestDaySignIn:(void(^)(id object))successBlock
                  faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_attendance_home_page parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}


- (void)loadRequestLiveHome:(BY_LIVE_HOME_LIST_TYPE)listType
                    pageNum:(NSInteger)pageNum
                   sortType:(BY_SORT_TYPE)sortType
               successBlock:(void(^)(id object))successBlock
                 faileBlock:(void(^)(NSError *error))faileBlock{
    NSString *sortValue = sortType == BY_SORT_TYPE_HOT ? @"watch_times" : (sortType == BY_SORT_TYPE_TIME ? @"begin_time" : @"");
    NSDictionary *param = @{@"list_type":@(listType),@"page_number":[NSNumber numberWithInteger:pageNum],@"page_size":@(10),@"sort_param":sortValue};
    [BYRequestManager ansyRequestWithURLString:RPC_get_homepage_list parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [[BYLiveHomeVideoModel alloc] getRespondData:result type:listType pageNum:pageNum];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 获取首页推荐直播+感兴趣用户+banner */
- (void)loadRequestLiveHomeRecommendLive:(void(^)(id object))successBlock
                              faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_recommend_live parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        
        NSMutableArray *array = [NSMutableArray array];
        
        // banner数据
        NSArray *bannerData = [BYHomeBannerModel getBannerData:result[@"banner_zero"] msg:result[@"banner_two"]];
        [array addObject:bannerData];
        
        BYCommonLiveModel *recommendLive = [BYCommonLiveModel mj_objectWithKeyValues:result[@"live_result"]];
        recommendLive = recommendLive ? recommendLive : [[BYCommonLiveModel alloc] init];
        if (recommendLive.status == BY_LIVE_STATUS_SOON) {
            NSDate *date = [NSDate by_dateFromString:recommendLive.begin_time dateformatter:K_D_F];
            NSDate *nowDate = [NSDate by_date];
            NSComparisonResult comResult = [date compare:nowDate];
            if (comResult == NSOrderedAscending ||
                comResult == NSOrderedSame) {
                recommendLive.status = BY_LIVE_STATUS_NOTLIVE_ONTIME;
            }
        }

        recommendLive.cellString = @"BYRecommendLiveNoticeCell";
        recommendLive.cellHeight = 174;
        NSArray *recommendUser = [BYRecommendUserModel getRecommendUser:result[@"attention_users"]];
        
        [array addObject:@{@"data":@[recommendLive]}];
  
        [array addObject:@{@"data":recommendUser}];
        if (successBlock) successBlock(array);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 首页热门观点 + 视频推荐 */
- (void)loadRequestLiveHomeHotContent:(void(^)(id object))successBlock
                           faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_hot_content parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        NSMutableArray *array = [NSMutableArray array];
        NSArray *featuredArr = [BYRecommendUserModel getFeaturedRecommendData:result[@"selected_recommend"]];
        NSArray *hotArticle = [BYRecommendUserModel getHotArticleData:result[@"article_list"]];
        [array addObject:@{@"data":hotArticle}];
        [array addObject:@{@"data":featuredArr}];
        if (successBlock) successBlock(array);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 首页获取话题列表 */
- (void)loadRequestGetTopicList:(void(^)(id object))successBlock
                     faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_topic_list parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *array = [BYHomeTopicModel getHomeTopicData:result[@"topic_list"]];
        if (successBlock) successBlock(array);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 直播及预告 */
- (void)loadRequestGetMoreLives:(BY_LIVE_STATUS)status
                        pageNum:(NSInteger)pageNum
                   successBlock:(void(^)(id object))successBlock
                     faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *params = @{@"status":@(status),
                             @"page_number":@(pageNum),
                             @"page_size":@(10)};
    [BYRequestManager ansyRequestWithURLString:RPC_get_more_lives parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data;
//        if (status == BY_LIVE_STATUS_END) {
        data = [BYRecommendUserModel getMoreNoticData:result[@"content"]];
//        }else{
//            data = [BYRecommendUserModel getMoreNoticData:result];
//        }
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetHomeAttention:(NSInteger)pageNum
                       successBlock:(void(^)(id object , BOOL isHaveAttention))successBlock
                         faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *params = @{@"page_number":@(pageNum),
                             @"page_size":@(10)};
    [BYRequestManager ansyRequestWithURLString:RPC_attention_homepage parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        if ([result[@"hasAttention"] boolValue]) {
            NSArray *data = [BYHomeArticleModel getAttentionData:result[@"contentList"] topData:result[@"topList"] pageNum:pageNum];
            if (successBlock) successBlock(data ,YES);
        }else{
            NSArray *data = [BYHomeArticleModel getNoAttentionCellData:result[@"recommendUsers"]];
            if (successBlock) successBlock(data ,NO);
        }
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestTopicHomePage:(NSInteger)pageNum
                           topic:(NSString *)topic
                        sortType:(BY_SORT_TYPE)sortType
                    successBlock:(void(^)(id object , id topicDetailModel,NSInteger pageNum))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSString *key = sortType == BY_SORT_TYPE_HOT ? @"hot" : @"time";
    NSDictionary *params = @{@"page_number":@(pageNum),
                             @"topic":nullToEmpty(topic),
                             @"key":key};
    [BYRequestManager ansyRequestWithURLString:RPC_topic_homepage parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYHomeArticleModel getTopicCellData:result[@"contentList"] topData:@[] pageNum:pageNum];
        BYHomeTopicModel *topicModel = nil;
        if (pageNum == 0) {
            topicModel = [BYHomeTopicModel mj_objectWithKeyValues:result[@"topic_detail"]];
        }
        if (successBlock) successBlock(data,topicModel,pageNum);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestPageIndexArticle:(BY_HOME_ARTICLE_TYPE)type
                            pageNum:(NSInteger)pageNum
                       successBlock:(void(^)(id object))successBlock
                         faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *params = @{@"page_number":@(pageNum),
                            @"page_size":@(10),
                             @"type":@(type)};
    [BYRequestManager ansyRequestWithURLString:RPC_page_index_article parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data;
        if (type == BY_HOME_ARTICLE_TYPE_ANALYST) {
            data = [BYHomeArticleModel getAnalystTableData:result[@"content"]];
        }else{
            data = [BYHomeArticleModel getTopicCellData:result[@"content"] topData:@[] pageNum:pageNum];
        }
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestHotSearch:(void(^)(id object))successBlock
                  faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_hot_search parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYSearchHotModel getHotData:result[@"hot_list"]];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestSearchType:(BY_SEARCH_TYPE)type
                          key:(NSString *)key
                      pageNum:(NSInteger)pageNum
                 successBlock:(void(^)(id object))successBlock
                   faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *params = @{@"search_content":nullToEmpty(key),
                             @"type":@(type),
                             @"page_number":@(pageNum),
                             @"page_size":@(10)};
    [BYRequestManager ansyRequestWithURLString:RPC_search_list parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data;
        switch (type) {
            case BY_SEARCH_TYPE_ALL:
            {
                data = [BYSearchHisModel getSearchRecommendData:result tag:key];
            }
                break;
            case BY_SEARCH_TYPE_LIVE:
            {
                NSArray *tmpArr = [BYSearchHisModel getSearchLiveData:result[@"live_list"] tag:key];
                NSMutableArray *array = [NSMutableArray arrayWithArray:tmpArr];
                if (pageNum != 0 && array.count) {
                    [array removeObjectAtIndex:0];
                }
                data = array;
            }
                break;
            case BY_SEARCH_TYPE_VIDEO:
            {
                NSArray *tmpArr = [BYSearchHisModel getSearchVideoData:result[@"video_list"] tag:key];
                NSMutableArray *array = [NSMutableArray arrayWithArray:tmpArr];
                if (pageNum != 0 && array.count) {
                    [array removeObjectAtIndex:0];
                }
                data = array;
            }
                break;
            case BY_SEARCH_TYPE_ARTICLE:
            {
                NSArray *tmpArr = [BYSearchHisModel getSearchArticleData:result[@"article_list"] tag:key];
                NSMutableArray *array = [NSMutableArray arrayWithArray:tmpArr];
                if (pageNum != 0 && array.count) {
                    [array removeObjectAtIndex:0];
                }
                data = array;
            }
                break;
            default:
            {
                NSArray *tmpArr = [BYSearchHisModel getSearchUserData:result[@"user_list"] tag:key];
                NSMutableArray *array = [NSMutableArray arrayWithArray:tmpArr];
                if (pageNum != 0 && array.count >= 1) {
                    [array removeObjectAtIndex:0];
                }
                data = array;
            }
                break;
        }
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadGetUploadVodSignatureSuccessBlock:(void(^)(id object))successBlock
                                   faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_Get_upload_signature parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        NSString *sign = result[@"data"][@"sign"];
        if (successBlock) successBlock(sign);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)uploadVodInfoWithUserId:(NSInteger)userId
                          title:(NSString *)title
                       videoUrl:(NSString *)videoUrl
                        videoId:(NSString *)videoId
                       coverUrl:(NSString *)coverUrl
                   successBlock:(void(^)(id object))successBlock
                     faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"user_id":[NSNumber numberWithInteger:userId],
                            @"video_url":nullToEmpty(videoUrl),
                            @"file_id":nullToEmpty(videoId),
                            @"cover_Url":nullToEmpty(coverUrl),
                            @"title":nullToEmpty(title)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_insert_vod_record parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestCreatLiveWithUserId:(NSInteger)userId
                              coverUrl:(NSString *)coverUrl
                                 title:(NSString *)title
                               message:(NSString *)message
                             beginTime:(NSString *)beginTime
                             organType:(BY_LIVE_ORGAN_TYPE)organType
                              liveType:(BY_LIVE_TYPE)liveType
                          successBlock:(void(^)(id object))successBlock
                            faileBlock:(void(^)(NSError *error))faileBlock{
//    NSString *type = liveType == BY_LIVE_TYPE_AUDIO ? @"audio" : (liveType == BY_LIVE_TYPE_PPT ? @"ppt" : @"video");
//    NSDictionary *param = @{@"user_id":[NSNumber numberWithInteger:userId],
//                            @"coverUrl":nullToEmpty(coverUrl),
//                            @"title":nullToEmpty(title),
//                            @"subtitle":nullToEmpty(message),
//                            @"beginTime":nullToEmpty(beginTime),
//                            @"organType":[NSNumber numberWithInteger:organType],
//                            @"typeType":type,
//                            @"status":@"WAITING"
//                            };
//    [BYRequestManager ansyRequestWithURLString:RPC_create_live_room parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
//        if (successBlock) successBlock(result[@"data"][@"roomId"]);
//    } failureBlock:^(NSError *error) {
//        if (faileBlock) faileBlock(error);
//    }];
}

- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status
                     live_record_id:(NSString *)live_record_id
                       successBlock:(void(^)(id object))successBlock
                         faileBlock:(void(^)(NSError *error))faileBlock{
//    NSDictionary *param = @{@"status":@(status),
//                            @"live_record_id":nullToEmpty(live_record_id)
//                            };
//    [BYRequestManager ansyRequestWithURLString:RPC_switch_live parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
//        if (successBlock) successBlock(result);
//    } failureBlock:^(NSError *error) {
//        if (faileBlock) faileBlock(error);
//    }];
    [self loadRequestUpdateLiveStatus:status live_record_id:live_record_id stream_id:nil real_begin_time:nil successBlock:successBlock faileBlock:faileBlock];
}

- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status
                     live_record_id:(NSString *)live_record_id
                          stream_id:(NSString *)stream_id
                    real_begin_time:(NSString *)real_begin_time
                       successBlock:(void (^)(id))successBlock
                         faileBlock:(void (^)(NSError *))faileBlock{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:@(status) forKey:@"status"];
    [param setObject:nullToEmpty(live_record_id) forKey:@"live_record_id"];
    if (stream_id.length) {
        [param setObject:nullToEmpty(stream_id) forKey:@"streamer_id"];
    }
    if (real_begin_time.length) {
        [param setObject:nullToEmpty(real_begin_time) forKey:@"real_begin_time"];
    }
    [BYRequestManager ansyRequestWithURLString:RPC_switch_live parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

//- (void)loadRequestGetILiveLoginSigSuccessBlock:(void(^)(id object))successBlock
//                                     faileBlock:(void(^)(NSError *error))faileBlock{
//    [BYRequestManager ansyRequestWithURLString:RPC_get_user_sig parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
//        if (successBlock) successBlock(result[@"userSig"]);
//    } failureBlock:^(NSError *error) {
//        if (faileBlock) faileBlock(error);
//    }];
//}

// --------------------------

- (void)loadRequestCreatLiveRoom:(NSString *)roomTitle
                          weChat:(NSString *)weChat
                       organType:(BY_LIVE_ORGAN_TYPE)organType
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSString *defultRoomBg = [NSString stringWithFormat:@"defult_liveroom_bg_%02d.jpg",arc4random()%6];
    NSDictionary *param = @{@"room_title":roomTitle,
                            @"wechat":weChat,
                            @"organ_type":@(organType),
                            @"cover_url":defultRoomBg};
    [BYRequestManager ansyRequestWithURLString:RPC_create_live_room parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result[@"roomId"]);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestNewLive:(NSString *)title
                 beginTime:(NSString *)beginTime
                  liveType:(BY_NEWLIVE_TYPE)liveType
              successBlock:(void(^)(id object))successBlock
                faileBlock:(void(^)(NSError *error))faileBlock{
    NSString *defultRoomBg = [NSString stringWithFormat:@"defult_live_bg_%02d.jpg",arc4random()%6];
    NSDictionary *param = @{@"live_title":nullToEmpty(title),
                            @"begin_time":nullToEmpty(beginTime),
                            @"live_type":@(liveType),
                            @"live_cover_url":defultRoomBg
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_create_live_record parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestNewLiveV1:(NSString *)title
                   beginTime:(NSString *)beginTime
                    coverUrl:(NSString *)coverUrl
                       adUrl:(NSString *)adUrl
                      topics:(NSString *)topics
                       intro:(NSString *)intro
                    liveType:(BY_NEWLIVE_TYPE)liveType
                successBlock:(void(^)(id object))successBlock
                  faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_title":nullToEmpty(title),
                            @"begin_time":nullToEmpty(beginTime),
                            @"live_type":@(liveType),
                            @"live_cover_url":coverUrl,
                            @"ad_img":nullToEmpty(adUrl),
                            @"live_intro":nullToEmpty(intro),
                            @"topic_content":topics
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_create_live_record parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestNewLive:(NSString *)title
            live_record_id:(NSString *)live_record_id
                 beginTime:(NSString *)beginTime
            live_cover_url:(NSString *)live_cover_url
                    ad_img:(NSString *)ad_img
                    topics:(NSString *)topics
                     intro:(NSString *)intro
                  liveType:(BY_NEWLIVE_TYPE)liveType
                scene_type:(BY_NEWLIVE_MICRO_TYPE)scene_type
                 host_list:(NSArray *)host_list
                guest_list:(NSArray *)guest_list
              successBlock:(void(^)(id object))successBlock
                faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *dic = @{@"live_title":nullToEmpty(title),
                          @"live_record_id":nullToEmpty(live_record_id),
                          @"begin_time":nullToEmpty(beginTime),
                          @"live_type":@(liveType),
                          @"live_cover_url":live_cover_url,
                          @"ad_img":nullToEmpty(ad_img),
                          @"live_intro":nullToEmpty(intro),
                          @"topic_content":topics,
                          @"scene_type":@(scene_type),
                          @"host_list":host_list,
                          @"interviewee_list":guest_list,
                          };
//    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:dic];
//    if (host_list.count) {
//        [param setObject:host_list forKey:@"host_list"];
//    }
//    if (guest_list.count) {
//        [param setObject:guest_list forKey:@"interviewee_list"];
//    }
    [BYRequestManager ansyRequestWithURLString:RPC_create_live parameters:dic operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 校验账号id是否存在 */
- (void)verifyAccountId:(NSString *)accountId
           successBlock:(void(^)(id object))successBlock
             faileBlock:(void(^)(NSError *error))faileBlock{
    if (!accountId.length) {
        return;
    }
    [BYRequestManager ansyRequestWithURLString:RPC_is_exist_account_id parameters:@{@"account_id":accountId} operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestDelTheme_id:(NSString *)theme_id
                    theme_type:(BY_THEME_TYPE)theme_type
                  successBlock:(void(^)(id object))successBlock
                    faileBlock:(void(^)(NSError *error))faileBlock{
    NSString *url = theme_type == 0 ? RPC_delete_live : RPC_delete_article;
    NSDictionary *param = theme_type == 0 ? @{@"live_record_id":theme_id} : @{@"article_id":theme_id};
    [BYRequestManager ansyRequestWithURLString:url parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}


- (void)loadReuqestUpdateLiveRoom:(NSDictionary *)param
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock{
    NSArray *allKeys = @[@"room_title",@"room_intro",@"cover_url",@"organ_type",@"live_title",@"live_intro",@"speaker",@"speaker_head_img",@"ad_img",@"begin_time",@"live_cover_url"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [allKeys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([param.allKeys indexOfObject:key] != NSNotFound) {
            [params setObject:param[key] forKey:key];
        }
        else
        {
            [params setObject:key forKey:key];
        }
    }];
    [BYRequestManager ansyRequestWithURLString:RPC_update_live_room parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

// 更新直播信息
- (void)loadRequestUpdateLiveSet:(NSString*)liveId
                           param:(NSDictionary *)param
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSArray *allKeys = @[@"live_title",@"live_intro",@"speaker",@"speaker_head_img",@"ad_img",@"begin_time",@"live_cover_url",@"topic_content",@"real_begin_time"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:nullToEmpty(liveId) forKey:@"live_record_id"];
    [allKeys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([param.allKeys indexOfObject:key] != NSNotFound) {
            [params setObject:param[key] forKey:key];
        }
        else
        {
            [params setObject:key forKey:key];
        }
    }];
    [param.allKeys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([allKeys indexOfObject:key] == NSNotFound) {
            [params setObject:param[key] forKey:key];
        }
    }];
    [BYRequestManager ansyRequestWithURLString:RPC_update_live_record parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}


- (void)loadRequestGetLiveRoomInfoSuccessBlock:(void(^)(id object))successBlock
                                    faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_get_live_room parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        BYCommonLiveModel *liveModel = [BYCommonLiveModel shareManager];
        id keyValues = [result mj_JSONObject];
        liveModel = [liveModel mj_setKeyValues:keyValues];
        liveModel.room_id = liveModel.user_id;
        if (successBlock) successBlock(liveModel);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetLivelist:(NSString *)userId
                       pageNum:(NSInteger)pageNum
                  successBlock:(void(^)(id object))successBlock
                    faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"owner_id":userId,@"page_number":@(pageNum),@"page_size":@(10)};
    [BYRequestManager ansyRequestWithURLString:RPC_get_live_list parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSMutableArray *tableData = [NSMutableArray array];
        if ([result[@"content"] count]) {
            NSArray *tmpArr = (NSArray *)result[@"content"];
            [tmpArr enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                BYCommonLiveModel *liveModel = [BYCommonLiveModel mj_objectWithKeyValues:dic];
                liveModel.cellString = @"BYLiveRoomHomeCell";
                NSDate *date = [NSDate by_dateFromString:liveModel.begin_time dateformatter:K_D_F];
                NSString *time = [NSDate by_stringFromDate:date dateformatter:@"MM-dd HH:mm"];
//                liveModel.begin_time = time;
                liveModel.show_begin_time = time;
                liveModel.cellHeight = 118;
                [tableData addObject:liveModel];
            }];
        }
        if (successBlock) successBlock(tableData);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetHomeBannerSuccessBlock:(void(^)(id object))successBlock
                                  faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_get_banner_list parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYHomeBannerModel getBannerData:result[@"banner_list"]];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadReuqestIsHaveLiveRoomSuccessBlock:(void(^)(id object))successBlock
                                   faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_whether_has_room parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock([result isKindOfClass:[NSDictionary class]] ? result[@"ret"] : @(YES));
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestUpdateCourseware:(NSArray *)pptImgs
                     live_record_id:(NSString *)live_record_id
                       successBlock:(void(^)(id object))successBlock
                         faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"courseware":pptImgs,@"live_record_id":live_record_id};
    [BYRequestManager ansyRequestWithURLString:RPC_update_courseware parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestLiveGuestOpearType:(BY_GUEST_OPERA_TYPE)type
                             theme_id:(NSString *)theme_id
                           theme_type:(BY_THEME_TYPE)theme_type
                     receiver_user_id:(NSString *)receiver_user_id
                         successBlock:(void(^)(id object))successBlock
                           faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"theme_id":nullToEmpty(theme_id),
                            @"operate_type":@(type),
                            @"theme_type":@(theme_type),
                            @"receiver_user_id":nullToEmpty(receiver_user_id)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_create_interaction parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestDeltOpearType:(BY_GUEST_OPERA_TYPE)type
                        theme_id:(NSString *)theme_id
                      theme_type:(BY_THEME_TYPE)theme_type
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"theme_id":nullToEmpty(theme_id),
                            @"operate_type":@(type),
                            @"theme_type":@(theme_type)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_delete_interaction parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestReward:(NSString *)receicer_user_id
            reward_amount:(CGFloat)reward_amount
             successBlock:(void(^)(id object))successBlock
               faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"receiver_user_id":nullToEmpty(receicer_user_id),
                            @"reward_amount":[NSString stringWithFormat:@"%.2f",reward_amount],
                            @"operate_type":@(0)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_create_reward parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestSurplusBBTSuccessBlock:(void(^)(id object))successBlock
                               faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_reward_window parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestPostHeartBeat:(NSString *)live_record_id
                      virtual_pv:(NSInteger )virtual_pv
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_record_id":live_record_id,
                            @"virtual_PV":@(virtual_pv)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_get_heartbeat parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetLiveDetail:(NSString *)live_record_id
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_record_id":live_record_id};
    [BYRequestManager ansyRequestWithURLString:RPC_get_live_details parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        
        BYCommonLiveModel *model = [BYCommonLiveModel mj_objectWithKeyValues:result];
        model.shareMap = [BYCommonShareModel mj_objectWithKeyValues:result[@"shareMap"]];
        model.intro_begin_time = [NSDate transFromIntroTime:model.begin_time];
        if (successBlock) successBlock(model);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestAttentionLive:(NSString *)live_record_id
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_record_id":live_record_id};
    [BYRequestManager ansyRequestWithURLString:RPC_do_attention_live parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result[@"ret"]);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestCancelAttentionLive:(NSString *)live_record_id
                          successBlock:(void(^)(id object))successBlock
                            faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_record_id":live_record_id};
    [BYRequestManager ansyRequestWithURLString:RPC_delete_attention_live parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result[@"ret"]);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)extracted:(void (^)(NSError *))faileBlock param:(NSDictionary *)param successBlock:(void (^)(id))successBlock {
    [BYRequestManager ansyRequestWithURLString:RPC_get_chat_history parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 获取聊天记录 */
- (void)loadRequestGetChatHistory:(NSString *)live_record_id
                          groupId:(NSString *)groupId
                          pageNum:(NSInteger)pageNum
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_record_id":live_record_id,
                            @"page_number":@(pageNum),
                            @"page_size":@(10)
                            };
    [self extracted:faileBlock param:param successBlock:successBlock];
}

/** 举报 */
- (void)loadRequestReportThemeId:(NSString *)themeId
                       themeType:(BY_THEME_TYPE)themeType
                      reportType:(NSString *)reportType
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"theme_id":themeId,
                            @"theme_type":@(themeType),
                            @"report_type":reportType
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_create_report parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 关注/取消关注他人 */
- (void)loadRequestCreatAttention:(NSString *)attention_id
                      isAttention:(BOOL)isAttention
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"attention_user_id":attention_id};
    NSString *url = isAttention ? create_attention : delete_attention;
    [BYRequestManager ansyRequestWithURLString:url parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 获取他人主页 */
- (void)loadRequestGetPersonData:(NSString *)user_id
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"other_id":user_id};
    [BYRequestManager ansyRequestWithURLString:RPC_get_his_data parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        BYPersonHomeHeaderModel *model = [[BYPersonHomeHeaderModel alloc] init];
        if (successBlock) successBlock([model getHeaderData:result]);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 查询个人点评 */
- (void)loadRequestGetMyComment:(NSString *)user_id
                        pageNum:(NSInteger)pageNum
                   successBlock:(void(^)(id object))successBlock
                     faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"comment_user_id":user_id,
                            @"page_number":@(pageNum),
                            @"page_size":@(10)};
    [BYRequestManager ansyRequestWithURLString:RPC_page_my_comment parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *tableData = [BYAppraiseModel getTableData:result];
        if (successBlock) successBlock(tableData);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 查询他人文章列表 */
- (void)loadRequestGetPersonArticle:(NSString *)user_id
                            pageNum:(NSInteger)pageNum
                       successBlock:(void(^)(id object))successBlock
                         faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"author_id":user_id,
                            @"page_number":@(pageNum),
                            @"page_size":@(10),
                            @"type":@(0)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_page_author_article parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *tableData = [BYHomeArticleModel getPersonArticleData:result[@"content"]];
        if (successBlock) successBlock(tableData);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestMixStream:(NSString *)streamer_id
                   viewer_id:(NSString *)viewer_id
                successBlock:(void(^)(id object))successBlock
                  faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"streamer_id":streamer_id,
                            @"viewer_id":viewer_id,
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_mix_stream parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetPushURL:(NSString *)live_record_id
                 successBlock:(void(^)(id object))successBlock
                   faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"live_record_id":live_record_id};
    [BYRequestManager ansyRequestWithURLString:RPC_get_push_url parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetPageChatHistory:(NSString *)groupId
                              pageNum:(NSInteger)pageNum
                             sendTime:(NSString *)sendTime
                         successBlock:(void(^)(id object))successBlock
                           faileBlock:(void(^)(NSError *error))faileBlock{
    sendTime = sendTime.length ? sendTime : [NSDate getCurrentTimeStr];
    NSDictionary *param = @{@"room_id":nullToEmpty(groupId),
                            @"page_number":@(pageNum),
                            @"page_size":@(10),
                            @"send_time":nullToEmpty(sendTime)};
    [BYRequestManager ansyRequestWithURLString:RPC_page_chat_history parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetPageChatHistory:(NSString *)groupId
                                 type:(NSInteger)type
                              pageNum:(NSInteger)pageNum
                             sendTime:(NSString *)sendTime
                         successBlock:(void(^)(id object))successBlock
                           faileBlock:(void(^)(NSError *error))faileBlock{
    sendTime = sendTime.length ? sendTime : [NSDate getCurrentTimeStr];
    NSDictionary *param = @{@"room_id":nullToEmpty(groupId),
                            @"type":@(type),
                            @"page_number":@(pageNum),
                            @"page_size":@(10),
                            @"send_time":nullToEmpty(sendTime)};
    [BYRequestManager ansyRequestWithURLString:RPC_page_chat_history parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestClickBanner:(NSString *)bannerId{
    if (!bannerId.length) return;
    [BYRequestManager ansyRequestWithURLString:RPC_click_banner parameters:@{@"banner_id":nullToEmpty(bannerId)} operationType:BYOperationTypePost successBlock:^(id result) {
    } failureBlock:^(NSError *error) {
    }];
}

- (void)loadRequestPageComment:(NSDictionary *)param
                  successBlock:(void(^)(id object))successBlock
                    faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_page_comment parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestCreateCommentWithThemeId:(NSString *)themeId
                                       type:(NSInteger)type
                                  themeInfo:(LiveFourumRootListSingleModel *)themeinfo
                                    content:(NSString *)content
                               successBlock:(void(^)(id object))successBlock
                                 faileBlock:(void(^)(NSError *error))faileBlock{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:themeId forKey:@"theme_id"];
    [params setObject:@(type) forKey:@"theme_type"];
    if (!themeinfo){
        [params setObject:@"0" forKey:@"reply_comment_id"];
    } else {
        if (themeinfo.transferTempCommentId.length){
            [params setObject:themeinfo.transferTempCommentId forKey:@"reply_comment_id"];
        } else {
            [params setObject:themeinfo._id forKey:@"reply_comment_id"];
        }
        
        [params setObject:themeinfo.user_id forKey:@"reply_user_id"];
    }
    [params setObject:content forKey:@"content"];
    [BYRequestManager ansyRequestWithURLString:create_comment parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 获取点评评论 */
- (void)loadRequestGetReview:(NSString *)theme_id
                     pageNum:(NSInteger)pageNum
                successBlock:(void(^)(id object))successBlock
                  faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *params = @{@"theme_id":theme_id,@"theme_type":@"0",@"reply_comment_id":@"0",@"page_number":@(pageNum),@"page_size":@"10"};
    [BYRequestManager ansyRequestWithURLString:RPC_page_comment parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYLiveReviewModel getReviewTableData:result[@"content"]];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 点评发起评论 */
- (void)loadRequestSendReveiewComment:(NSString *)themeId
                                 type:(NSInteger)type
                              content:(NSString *)content
                         successBlock:(void(^)(id object))successBlock
                           faileBlock:(void(^)(NSError *error))faileBlock{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:themeId forKey:@"theme_id"];
    [params setObject:@(type) forKey:@"theme_type"];
    [params setObject:@"0" forKey:@"reply_comment_id"];
    [params setObject:content forKey:@"content"];
    [BYRequestManager ansyRequestWithURLString:create_comment parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 点评顶踩 */
- (void)loadRequestCommentOpearType:(BY_GUEST_OPERA_TYPE)type
                         comment_id:(NSString *)comment_id
                           theme_id:(NSString *)theme_id
                         theme_type:(BY_THEME_TYPE)theme_type
                       successBlock:(void(^)(id object))successBlock
                         faileBlock:(void(^)(NSError *error))faileBlock{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:theme_id forKey:@"theme_id"];
    [params setObject:@(theme_type) forKey:@"theme_type"];
    [params setObject:comment_id forKey:@"comment_id"];
    [params setObject:@(type) forKey:@"operate_type"];
    [BYRequestManager ansyRequestWithURLString:create_comment_operate parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}


#pragma mark - 课程
- (void)loadRequestCourseList:(BY_COURSE_TYPE)type
                        topic:(NSString *)topic
                      pageNum:(NSInteger)pageNum
                 successBlock:(void(^)(id object))successBlock
                   faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"type":@(type),
                            @"page_number":@(pageNum),
                            @"page_size":@(10),
                            @"course_topic":nullToEmpty(topic)};
    [BYRequestManager ansyRequestWithURLString:RPC_course_homepage parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYHomeCourseModel getTableData:result[@"content"]];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 课程简介 */
- (void)loadRequestCourseIntro:(NSString *)courseId
                       videoId:(NSString *)videoId
                  successBlock:(void(^)(id object))successBlock
                    faileBlock:(void(^)(NSError *error))faileBlock{
    if (!courseId.length) return;
    NSDictionary *param = @{@"course_id":nullToEmpty(courseId),
                            @"video_id":nullToEmpty(videoId)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_course_brief_intro parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        BYCourseDetailModel *model = [BYCourseDetailModel getDetailModel:result];
        if (successBlock) successBlock(model);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 课程目录 */
- (void)loadRequestCourseCatalog:(NSString *)coueseId
                         pageNum:(NSInteger)pageNum
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"course_id":nullToEmpty(coueseId),
                            @"page_number":@(pageNum),
                            @"page_size":@(10)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_course_catalogue parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYCourseDetailCatalogModel getCatalogTableData:result[@"content"]];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

#pragma mark - 社群
/** 获取社群列表 */
- (void)loadRequestCommunityList:(NSInteger)pageNum
                    successBlock:(void(^)(id object , id wxData))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"page_number":@(pageNum),
                            @"page_size":@(10)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_page_community parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYOfficialCertGroupModel getTableData:result[@"content"]];
        BYOfficialRemindData *wxData;
        if (pageNum == 0) {
            wxData = [BYOfficialRemindData getWechatData:result[@"remind_data"]];
        }
        if (successBlock) successBlock(data , wxData);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 获取社群详情 */
- (void)loadRequestCommuntiyDetail:(NSString *)community_id
                      successBlock:(void(^)(id object))successBlock
                        faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"community_id":community_id};
    [BYRequestManager ansyRequestWithURLString:RPC_get_community parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        BYOfficialCertGroupModel *data = [BYOfficialCertGroupModel getGroupDetailData:result];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

#pragma mark - 活动

/** 活动创建 */
- (void)loadRequestCreateActivity:(NSDictionary *)params
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:RPC_create_activity parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadRequestGetActivity:(NSInteger)pageNum
                  successBlock:(void(^)(id object))successBlock
                    faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"page_number":@(pageNum),
                            @"page_size":@(10)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_page_activity parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYActivityModel getActivityListData:result[@"content"]];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 活动详情 */
- (void)loadRequestGetActivityDetail:(NSString *)activity_id
                        successBlock:(void(^)(id object))successBlock
                          faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"activity_id":nullToEmpty(activity_id)};
    [BYRequestManager ansyRequestWithURLString:RPC_get_activity parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        BYActivityModel *model = [BYActivityModel mj_objectWithKeyValues:result];
        
        NSDate *begin_date = [NSDate by_dateFromString:model.begin_time dateformatter:K_D_F];
        NSString *begin_time = [NSDate by_stringFromDate:begin_date dateformatter:@"yyyy-MM-dd HH:mm"];
        model.begin_time = begin_time;
        
        NSDate *end_date = [NSDate by_dateFromString:model.end_time dateformatter:K_D_F];
        NSString *end_time = [NSDate by_stringFromDate:end_date dateformatter:@"yyyy-MM-dd HH:mm"];
        model.end_time = end_time;
        
        if (successBlock) successBlock(model);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 分页查询我报名的活动 (type: 1.有效票 2.未完成 3.已结束)  */
- (void)loadRequestGetMyActivityEnroll:(NSInteger)type
                               pageNum:(NSInteger)pageNum
                          successBlock:(void(^)(id object))successBlock
                            faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"type":@(type),
                            @"page_number":@(pageNum),
                            @"page_size":@(10)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_page_my_enroll parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        
        NSArray *data = [BYActivityModel getActivityMyJoinData:result[@"content"]];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 分页查询我创建的活动 */
- (void)loadRequestGetMyActivity:(NSInteger)pageNum
                    successBlock:(void(^)(id object))successBlock
                      faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"page_number":@(pageNum),
                            @"page_size":@(10)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_page_my_activity parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYActivityModel getActivityMyManangerData:result[@"content"]];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];

}

/** 审核活动报名 */
- (void)loadRequestReviewSign:(BY_ACTIVITY_STATUS)status
           activity_enroll_id:(NSString *)activity_enroll_id
                 successBlock:(void(^)(id object))successBlock
                   faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"activity_enroll_id":nullToEmpty(activity_enroll_id),
                            @"enroll_status":@(status),
                            @"remark":@""};
    [BYRequestManager ansyRequestWithURLString:RPC_handle_activity_enroll parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 分页查询活动报名 */
- (void)loadRequestActivityEnroll:(NSString *)activity_id
                          pageNum:(NSInteger)pageNum
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"activity_id":nullToEmpty(activity_id),
                            @"page_number":@(pageNum),
                            @"page_size":@(10)};
    [BYRequestManager ansyRequestWithURLString:RPC_page_activity_enroll parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYActivityDetailSignModel getSignTabelData:result[@"content"]];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 新增活动报名 */
- (void)loadRequestCreateActivityEnroll:(NSString *)activity_id
                                   name:(NSString *)name
                                  phone:(NSString *)phone
                                 wechat:(NSString *)wechat
                                  email:(NSString *)email
                           successBlock:(void(^)(id object))successBlock
                             faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"activity_id":nullToEmpty(activity_id),
                            @"name":nullToEmpty(name),
                            @"phone":nullToEmpty(phone),
                            @"wechat":nullToEmpty(wechat),
                            @"email":nullToEmpty(email)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_create_activity_enroll parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 删除单条活动 */
- (void)loadRequestDeleteActivity:(NSString *)activity_id
                     successBlock:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *param = @{@"activity_id":nullToEmpty(activity_id)
                            };
    [BYRequestManager ansyRequestWithURLString:RPC_delete_activity parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        if (successBlock) successBlock(result);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

@end
