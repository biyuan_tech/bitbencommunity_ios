//
//  BYRecommendUserModel.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYRecommendUserModel.h"
#import "BYHomeArticleModel.h"

@implementation BYRecommendUserModel

+ (NSArray *)getRecommendUser:(NSArray *)respond{
    BYRecommendUserModel *model = [[BYRecommendUserModel alloc] init];
    model.cellString = @"BYRecommendUserCell";
    model.cellHeight = 212 + 28;
    model.cellData = [BYRecommendUserCellModel getRecommendUser:respond];
    model.needReloadData = YES;
    return @[model];
}

+ (NSArray *)getFeaturedRecommendData:(NSArray *)respond{
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (!respond.count) {
        return @[];
    }
    NSMutableArray *array = [NSMutableArray array];
    BYCommonModel *titleModel = [[BYCommonModel alloc] init];
    titleModel.cellString = @"BYFeaturedHeaderCell";
    titleModel.title = @"视频推荐";
    titleModel.cellHeight = 60 - 8;
    [array addObject:titleModel];
    
    BYRecommendUserModel *featuredModel = [[BYRecommendUserModel alloc] init];
    featuredModel.cellString = @"BYFeaturedRecommendCell";
    featuredModel.cellHeight = 590;
    featuredModel.needReloadData = YES;
    if (respond.count <= 6) {
        // 竖数
        NSInteger i = ceil(respond.count/2.0f);
        featuredModel.cellHeight = 193*i;
    }
    NSMutableArray *data = [NSMutableArray array];
    for (NSDictionary *dic in respond) {
        BYCommonLiveModel *model = [BYCommonLiveModel mj_objectWithKeyValues:dic];
        [data addObject:model];
    }
    featuredModel.featuredData = data;
    NSArray *resultArr = [data sortedArrayUsingComparator:^NSComparisonResult(BYCommonLiveModel *obj1, BYCommonLiveModel *obj2) {
        int seed = arc4random_uniform(2);
        if (seed) {
            return [obj1.live_record_id compare:obj2.live_record_id];
        }else{
            return [obj2.live_record_id compare:obj1.live_record_id];
        }
    }];
    featuredModel.featuredCellData = resultArr;
    featuredModel.loadPageNum = 0;
    if (resultArr.count <= 6) {
        featuredModel.featuredLoadData = resultArr;
    }
    else{
        featuredModel.featuredLoadData = [resultArr subarrayWithRange:NSMakeRange(0, 6)];
    }
    [array addObject:featuredModel];
    
    if (resultArr.count > 6) {
        BYCommonModel *footerModel = [[BYCommonModel alloc] init];
        footerModel.cellString = @"BYFeaturedSingleFooterView";
        footerModel.cellHeight = 56;
        [array addObject:footerModel];
    }

    return array;
}

+ (NSArray *)getGuessLikeData:(NSArray *)respond{
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (!respond.count) {
        return @[];
    }
    NSMutableArray *array = [NSMutableArray array];
    BYCommonModel *titleModel = [[BYCommonModel alloc] init];
    titleModel.cellString = @"BYGuessLikeTitleCell";
    titleModel.cellHeight = 40;
    [array addObject:titleModel];
    for (NSDictionary *dic in respond) {
        BYCommonLiveModel *model = [BYCommonLiveModel mj_objectWithKeyValues:dic];
        model.cellString = @"BYGuessLikeCell";
        model.cellHeight = 92;
        [array addObject:model];
    }
    return array;
}

+ (NSArray *)getMoreNoticData:(NSArray *)respond{
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (!respond.count) {
        return @[];
    }
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dic in respond) {
        BYCommonLiveModel *model = [BYCommonLiveModel mj_objectWithKeyValues:dic];
        model.cellString = @"BYRecommendNoticMoreCell";
        model.cellHeight = 105;
        [array addObject:model];
    }
    return array;
}

/** 解析精选推荐综合数据 */
+ (NSArray *)getFeaturedRecommendAllData:(NSArray *)data{
    return [BYRecommendUserModel getFeaturedRecommendMoreData:data];
}

/** 解析精选推荐直播回放数据 */
+ (NSArray *)getFeaturedRecommendLiveEndData:(NSArray *)data{
    return [BYRecommendUserModel getFeaturedRecommendMoreData:data];
}

/** 解析精选推荐视频数据 */
+ (NSArray *)getFeaturedRecommendVideoData:(NSArray *)data{
    return [BYRecommendUserModel getFeaturedRecommendMoreData:data];
}

+ (NSArray *)getFeaturedRecommendMoreData:(NSArray *)data{
    if (![data isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (!data.count) {
        return @[];
    }
    NSMutableArray *result = [NSMutableArray array];
    BYRecommendUserModel *model = [[BYRecommendUserModel alloc] init];
    model.needReloadData = YES;
    model.cellHeight = 145;
    model.cellString = @"BYFeaturedMoreHeaderCell";
    if (data.count <= 4) {
        model.featuredCellData = data;
        [result addObject:@{@"data":@[model]}];
    }else{
        model.featuredCellData = [data subarrayWithRange:NSMakeRange(0, 4)];
        [result addObject:@{@"data":@[model]}];
        NSMutableArray *footerData = [NSMutableArray arrayWithArray:data];
        [footerData removeObjectsInRange:NSMakeRange(0, 4)];
        for (BYCommonLiveModel *tmpModel in footerData) {
            tmpModel.cellString = @"BYGuessLikeCell";
            tmpModel.cellHeight = 92;
        }
        [result addObject:@{@"data":[footerData copy]}];
    }
    return result;
}

+ (NSArray *)getHotArticleData:(NSArray *)respond{
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (!respond.count) {
        return @[];
    }
    
    NSMutableArray *array = [NSMutableArray array];
    BYCommonModel *titleModel = [[BYCommonModel alloc] init];
    titleModel.cellString = @"BYFeaturedHeaderCell";
    titleModel.title = @"社区热门观点";
    titleModel.cellHeight = 60 - 12;
    [array addObject:titleModel];
    for (NSDictionary *dic in respond) {
        BYHomeArticleModel *model = [BYHomeArticleModel mj_objectWithKeyValues:dic];
        model.showTime = [NSDate getTimeGap:model.create_time/1000];
        model.cellString = @"BYHomeHotArticleCell";
        model.cellHeight = 92 + 12;
        [array addObject:model];
    }
    return array;
}

@end

@implementation BYRecommendUserCellModel

+ (NSArray *)getRecommendUser:(NSArray *)respond{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dic in respond) {
        BYRecommendUserCellModel *model = [BYRecommendUserCellModel mj_objectWithKeyValues:dic];
        model.cellString = @"BYRecommendUserCell";
        model.cellHeight = 160;

        [array addObject:model];
    }
    return array;
    
}

@end

