//
//  BYRecommendUserModel.h
//  BibenCommunity
//
//  Created by 随风 on 2019/5/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class BYRecommendUserCellModel;
@interface BYRecommendUserModel : BYCommonModel

/** 数据源 */
@property (nonatomic ,strong) NSArray <BYRecommendUserCellModel  *>*cellData;
/** reloadData */
@property (nonatomic ,assign) BOOL needReloadData;

// featured 精选数据

/** 精选推荐数据源 */
@property (nonatomic ,strong) NSArray <BYCommonLiveModel *>*featuredCellData;
/** 精选推荐原数据（未随机打乱） */
@property (nonatomic ,strong) NSArray <BYCommonLiveModel *>*featuredData;
/** 加载的数据 */
@property (nonatomic ,strong) NSArray *featuredLoadData;
/** 页数 */
@property (nonatomic ,assign) NSInteger loadPageNum;




+ (NSArray *)getRecommendUser:(NSArray *)respond;

/** 解析精选推荐数据 */
+ (NSArray *)getFeaturedRecommendData:(NSArray *)respond;

/** 解析猜你喜欢数据 */
+ (NSArray *)getGuessLikeData:(NSArray *)respond;

/** 解析直播及预告数据 */
+ (NSArray *)getMoreNoticData:(NSArray *)respond;

/** 解析精选推荐综合数据 */
+ (NSArray *)getFeaturedRecommendAllData:(NSArray *)data;

/** 解析精选推荐直播回放数据 */
+ (NSArray *)getFeaturedRecommendLiveEndData:(NSArray *)data;

/** 解析精选推荐视频数据 */
+ (NSArray *)getFeaturedRecommendVideoData:(NSArray *)data;

/** 解析首页热门观点的数据 */
+ (NSArray *)getHotArticleData:(NSArray *)respond;

@end

@interface BYRecommendUserCellModel : BYCommonModel

/** 用户头像 */
@property (nonatomic ,copy) NSString *head_img;
/** 用户昵称 */
@property (nonatomic ,copy) NSString *nickname;
/** 用户id */
@property (nonatomic ,copy) NSString *account_id;
/** 关注状态 */
@property (nonatomic ,assign) BOOL isAttention;
/** vip */
@property (nonatomic ,assign) NSInteger cert_badge;
/** 成就组 */
@property (nonatomic ,strong) NSArray <NSString *>*achievement_badge_list;

+ (NSArray *)getRecommendUser:(NSArray *)respond;

@end

NS_ASSUME_NONNULL_END
