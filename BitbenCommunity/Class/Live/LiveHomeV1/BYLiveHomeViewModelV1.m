//
//  BYLiveHomeViewModelV1.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYLiveHomeViewModelV1.h"

#import "BYHomeRecommendController.h"
#import "BYHomeAttentionController.h"
#import "BYHomeTopicController.h"
#import "BYHomeSearchController.h"
#import "BYHomeAnalystController.h"
#import "BYHomeOrganController.h"
#import "BYHomeNewsletterController.h"
//#import "AppointmentViewController.h"
#import "BYOfficialCertGroupController.h"
#import "BYHomeVideoController.h"
#import "ArticleInfoListViewController.h"

#import "BYHomeSearchNavigationView.h"

@interface UIView (loginStatus)

/** 登录状态 */
@property (nonatomic ,assign) BOOL loginStatus;

- (UIViewController *)viewController;

@end


@interface BYLiveHomeViewModelV1 ()<BYLiveHomeSegmentViewDelegateV1>
{
    NSArray *_titles;
}
/** 分类器 */
@property (nonatomic ,strong) BYLiveHomeSegmentViewV1 *segmentView;
/** 推荐分类 */
@property (nonatomic ,strong) BYHomeRecommendController *recommendController;
/** 关注 */
@property (nonatomic ,strong) BYHomeAttentionController *attentionController;
/** searchView */
@property (nonatomic ,strong) BYHomeSearchNavigationView *searchView;
/** 分析师 */
@property (nonatomic ,strong) BYHomeAnalystController *anlystController;
/** 机构号 */
@property (nonatomic ,strong) BYHomeOrganController *organController;
/** 快讯 */
@property (nonatomic ,strong) BYHomeNewsletterController *newletterController;
/** 话题 */
@property (nonatomic ,strong) BYHomeTopicController *topicController;
/** 币本有约 */
@property (nonatomic ,strong) BYOfficialCertGroupController *appointController;
/** 视频 */
@property (nonatomic ,strong) BYHomeVideoController *videoController;
/** 资讯 */
@property (nonatomic ,strong) ArticleInfoListViewController *infoController;


@end

@implementation BYLiveHomeViewModelV1

- (void)viewWillAppear{
//    [_recommendController startTimer];
    [self reloadRedDot];
}

- (void)viewDidDisappear{
//    [_recommendController destoryTimer];
}

- (void)setContentView{
}

- (void)configUi{
    [self addSearchView];
    [self addSegmentView];
}

- (void)autoReload{
    NSInteger index = [self.segmentView.subViews indexOfObject:self.segmentView.currentSubView];
    if (!_segmentView) return;
    if (index == 0) {
        [self.attentionController autoReload];
    }else if (index == 1){
        [self.recommendController autoReload];
    }else if (index == 2){
        [self.infoController autoReload];
    }else if (index == 3){
        [self.newletterController autoReload];
    }else if (index == 4){
        [self.anlystController autoReload];
    }else if (index == 6){
        [self.organController autoReload];
    }else if (index == 5){
        [self.videoController autoReload];
    }else if (index == 7){
        [self.topicController autoReload];
    }
    else{
        [self.appointController autoReload];
    }
}

- (void)reloadCurrentView{
    [self reloadRedDot];
    if ([self.segmentView.currentSubView.viewController respondsToSelector:@selector(reloadData)]) {
        [self.segmentView.currentSubView.viewController performSelector:@selector(reloadData)];
    }
    
    if (self.segmentView.currentSubView != self.segmentView.subViews[0]) {
        UIView *subView = self.segmentView.subViews[0];
        if ([subView.viewController respondsToSelector:@selector(reloadData)]) {
            [subView.viewController performSelector:@selector(reloadData)];
        }
    }
}

// 刷新红点
- (void)reloadRedDot{
    [self.searchView reloadRedDot];
}

- (UIViewController *)getSegmentSubControllerAtIndex:(NSInteger)index{
    if (!_segmentView) return nil;
    if (index == 0) {
        return self.attentionController;
    }else if (index == 1){
        return self.recommendController;
    }else if (index == 2){
        return self.infoController;
    }else if (index == 3){
        return self.newletterController;
    }else if (index == 4){
        return self.anlystController;
    }else if (index == 6){
        return self.organController;
    }else if (index == 5){
        return self.videoController;
    }else if (index == 7){
        return self.topicController;
    }
    else{
        return self.appointController;
    }
}
#pragma mark - BYLiveHomeSegmentViewDelegateV1
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    if (index == 0) {
        return self.attentionController.view;
    }else if (index == 1){
        return self.recommendController.view;
    }else if (index == 2){
        return self.infoController.view;
    }else if (index == 3){
        return self.newletterController.view;
    }else if (index == 4){
        return self.anlystController.view;
    }else if (index == 6){
        return self.organController.view;
    }else if (index == 5){
        return self.videoController.view;
    }else if (index == 7){
        return self.topicController.view;
    }
    else{
        return self.appointController.view;
    }
}

- (void)by_segmentViewDidScrollToIndex:(NSInteger)index{
    // 登录状态变换后界面刷新
    UIView *subView = _segmentView.subViews[index];
    UIViewController *controller = subView.viewController;
    if (subView.loginStatus != [AccountModel sharedAccountModel].hasLoggedIn) {
#pragma clang diagnostic ignored "-Wundeclared-selector"
        if ([controller respondsToSelector:@selector(reloadData)]) {
            [controller performSelector:@selector(reloadData)];
        }
        subView.loginStatus = [AccountModel sharedAccountModel].hasLoggedIn;
    }
}

#pragma mark - configUI
- (void)addSearchView{
    self.searchView = [[BYHomeSearchNavigationView alloc] init];
    [S_V_VIEW addSubview:self.searchView];
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Top(60));
    }];
}

- (void)addSegmentView{
    
//    self.segmentView = [[BYLiveHomeSegmentViewV1 alloc] initWithTitles:@"关注",@"推荐",@"热点要闻",@"快讯",@"独家",@"小白入门",@"创投",@"技术",@"专题",@"研报",@"行情",@"安全",@"项目",@"游戏",@"知识库",@"访谈",@"通证经济",@"活动",@"政策",@"交易平台",@"DAPP",@"钱包",@"挖矿",@"脱口秀",@"比特币",@"以太坊",nil];
    self.segmentView = [[BYLiveHomeSegmentViewV1 alloc] initWithTitles:@"关注",@"推荐",@"资讯",@"快讯",@"分析师",@"视频",@"机构号",@"话题",@"社群",nil];
    self.segmentView.delegate = self;
    self.segmentView.defultSelIndex = 1;
    self.segmentView.vernierHeight = 3.0f;
    self.segmentView.selectAttributed = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:17],NSForegroundColorAttributeName:kColorRGBValue(0x323232)};
    self.segmentView.normalAttributed = @{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kColorRGBValue(0x8f8f8f)};
    self.segmentView.alignment = BYSegmentAlignmentLeft;
    [S_V_VIEW addSubview:self.segmentView];
    [_segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSafe_Mas_Top(60) + 7);
        make.left.right.bottom.mas_equalTo(0);
    }];
}

- (BYHomeRecommendController *)recommendController{
    if (!_recommendController) {
        _recommendController = [[BYHomeRecommendController alloc] init];
        _recommendController.view.loginStatus = [AccountModel sharedAccountModel].hasLoggedIn;
        [S_VC addChildViewController:_recommendController];
    }
    return _recommendController;
}

- (BYHomeAttentionController *)attentionController{
    if (!_attentionController) {
        _attentionController = [[BYHomeAttentionController alloc] init];
        _attentionController.segmentView = self.segmentView;
        _attentionController.view.loginStatus = [AccountModel sharedAccountModel].hasLoggedIn;
        [S_VC addChildViewController:_attentionController];
    }
    return _attentionController;
}

- (BYHomeAnalystController *)anlystController{
    if (!_anlystController) {
        _anlystController = [[BYHomeAnalystController alloc] init];
        _anlystController.view.loginStatus = [AccountModel sharedAccountModel].hasShowLogin;
        [S_VC addChildViewController:_anlystController];
    }
    return _anlystController;
}

- (BYHomeOrganController *)organController{
    if (!_organController) {
        _organController = [[BYHomeOrganController alloc] init];
        _organController.view.loginStatus = [AccountModel sharedAccountModel].hasShowLogin;
        [S_VC addChildViewController:_anlystController];
    }
    return _organController;
}

- (BYHomeNewsletterController *)newletterController{
    if (!_newletterController) {
        _newletterController = [[BYHomeNewsletterController alloc] init];
        _newletterController.view.loginStatus = [AccountModel sharedAccountModel].hasShowLogin;
        [S_VC addChildViewController:_newletterController];
    }
    return _newletterController;
}

- (BYHomeTopicController *)topicController{
    if (!_topicController) {
        _topicController = [[BYHomeTopicController alloc] init];
        _topicController.view.loginStatus = [AccountModel sharedAccountModel].hasLoggedIn;
        [S_VC addChildViewController:_topicController];
    }
    return _topicController;
}

- (BYOfficialCertGroupController *)appointController{
    if (!_appointController) {
        _appointController = [[BYOfficialCertGroupController alloc] init];
        _appointController.view.loginStatus = [AccountModel sharedAccountModel].hasShowLogin;
        [S_VC addChildViewController:_appointController];
    }
    return _appointController;
}

- (BYHomeVideoController *)videoController{
    if (!_videoController) {
        _videoController = [[BYHomeVideoController alloc] init];
        _videoController.view.loginStatus = [AccountModel sharedAccountModel].hasShowLogin;
        [S_VC addChildViewController:_videoController];
    }
    return _videoController;
}

- (ArticleInfoListViewController *)infoController{
    if (!_infoController) {
        _infoController = [[ArticleInfoListViewController alloc] init];
        _infoController.view.loginStatus = [AccountModel sharedAccountModel].hasShowLogin;
        [S_VC addChildViewController:_infoController];
    }
    return _infoController;
}
@end

static const char loginStatusKey;
@implementation UIView (loginStatus)

- (void)setLoginStatus:(BOOL)loginStatus{
    objc_setAssociatedObject(self, &loginStatusKey, @(loginStatus), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)loginStatus{
    NSNumber *status = objc_getAssociatedObject(self, &loginStatusKey);
    return [status boolValue];
}

- (UIViewController *)viewController{
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}
@end
