//
//  BYLiveHomeControllerV1.h
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYLiveHomeControllerV1 : BYCommonViewController


+ (instancetype)shareManager;

- (void)selectSegmentAtIndex:(NSInteger)index;

- (UIViewController *)getSegmentSubControllerAtIndex:(NSInteger)index;


-(void)reloadViewInterfaceManager;
@end

NS_ASSUME_NONNULL_END
