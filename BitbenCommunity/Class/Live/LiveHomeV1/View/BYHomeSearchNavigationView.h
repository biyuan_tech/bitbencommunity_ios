//
//  BYHomeSearchNavigationView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/9.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeSearchNavigationView : UIView

/** 刷新红点状态 */
- (void)reloadRedDot;

@end

NS_ASSUME_NONNULL_END
