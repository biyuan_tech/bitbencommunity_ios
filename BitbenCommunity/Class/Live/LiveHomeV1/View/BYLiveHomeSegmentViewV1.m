//
//  BYLiveHomeSegmentViewV1.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYLiveHomeSegmentViewV1.h"

static NSInteger count = 5;
@interface BYLiveHomeSegmentViewV1()<UIScrollViewDelegate>
{
    NSInteger _index;
    NSInteger _lastIndex;
    CGFloat _beginDragOffsetX;
}

/** 标题存储，也用于初始化子View */
@property (nonatomic ,strong) NSArray *titles;

/** 存储headerView按钮 */
@property (nonatomic ,strong) NSArray *buttons;

/** subViews */
@property (nonatomic ,strong) NSArray *subViews;

/** headerView */
@property (nonatomic ,strong) UIScrollView *headerView;

/** 选择项跟踪横线 */
@property (nonatomic ,strong) UIView *selLineView;

/** contentView,subView容器 */
@property (nonatomic ,strong) UIScrollView *scrollView;

@end

static NSInteger baseTag = 0x943;
static CGFloat headerHeight = 33.0f;
@implementation BYLiveHomeSegmentViewV1

- (instancetype)initWithTitles:(NSString *)titles, ... NS_REQUIRES_NIL_TERMINATION{
    NSMutableArray *arrays = [NSMutableArray array];
    va_list argList;
    if (titles.length){
        [arrays addObject:titles];
        va_start(argList, titles);
        id temp;
        while ((temp = va_arg(argList, id))){
            [arrays addObject:temp];
        }
    }
    BYLiveHomeSegmentViewV1 *segmentView = [[BYLiveHomeSegmentViewV1 alloc] init];
    segmentView.alignment = BYSegmentAlignmentCenter;
    segmentView.titles = arrays;
    segmentView.vernierHeight = 2.0f;
    [segmentView setContentView];
    segmentView.defultSelIndex = 0;
    segmentView->_lastIndex = 0;
    return segmentView;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    UIButton *button = [self viewWithTag:baseTag + _lastIndex];
    self.selLineView.center = CGPointMake(button.center.x, self.selLineView.center.y);
}

- (void)setSelectAttributed:(NSDictionary *)selectAttributed{
    _selectAttributed = selectAttributed;
    for (int i = 0; i < _titles.count; i ++) {
        NSString *title = _titles[i];
        UIButton *btn = [_headerView viewWithTag:baseTag + i];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic setDictionary:selectAttributed];
        [dic setObject:title forKey:@"title"];
        [btn setBy_attributedTitle:dic forState:UIControlStateSelected];
        CGSize size = [title getStringSizeWithFont:dic[NSFontAttributeName] maxWidth:MAXFLOAT];
        [btn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(ceil(size.width));
        }];
    }
}

- (void)setNormalAttributed:(NSDictionary *)normalAttributed{
    _normalAttributed = normalAttributed;
    for (int i = 0; i < _titles.count; i ++) {
        NSString *title = _titles[i];
        UIButton *btn = [_headerView viewWithTag:baseTag + i];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic setDictionary:normalAttributed];
        [dic setObject:title forKey:@"title"];
        [btn setBy_attributedTitle:dic forState:UIControlStateNormal];
    }
}

- (void)setVernierHeight:(CGFloat)vernierHeight{
    _vernierHeight = vernierHeight;
    CGRect frame = self.selLineView.frame;
    frame.size.height = vernierHeight;
    frame.origin.y = headerHeight - vernierHeight;
    _selLineView.frame = frame;
}

- (void)reloadHeader{
    for (int i = 0; i < _titles.count; i ++) {
        NSString *title = _titles[i];
        UIButton *btn = [_headerView viewWithTag:baseTag + i];
        UIButton *lastBtn = [_headerView viewWithTag:baseTag + i - 1];
        if (_alignment == BYSegmentAlignmentCenter) {
            CGFloat width = kCommonScreenWidth/_titles.count;
            [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(width);
                make.top.bottom.mas_equalTo(0);
                make.left.mas_equalTo(width*i);
            }];
        }else if (_alignment == BYSegmentAlignmentLeft){
            CGFloat width = 0;
            NSAttributedString *selectedAttributed = [btn attributedTitleForState:UIControlStateSelected];
            if (selectedAttributed) {
                NSRange range = NSMakeRange(0, title.length);
                NSDictionary *dic = [selectedAttributed attributesAtIndex:0 effectiveRange:&range];
                CGSize size = [title getStringSizeWithFont:dic[NSFontAttributeName] maxWidth:MAXFLOAT];
                width = ceil(size.width);
            }else{
                width = stringGetWidth(title, 14);
            }
            [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(width);
                make.top.bottom.mas_equalTo(0);
                if (i == 0) {
                    make.left.mas_equalTo(15);
                }else{
                    make.left.mas_equalTo(lastBtn.mas_right).mas_offset(20);
                }
            }];
        }
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        UIButton *button = [self viewWithTag:baseTag + _defultSelIndex];
        self.selLineView.center = CGPointMake(button.center.x, self.selLineView.center.y);
    });
}

- (void)setDelegate:(id<BYLiveHomeSegmentViewDelegateV1>)delegate{
    _delegate = delegate;
    UIView *view = [_scrollView viewWithTag:2*baseTag + _defultSelIndex];
    if (view.subviews.count == 0 && view) {
        if ([self.delegate respondsToSelector:@selector(by_segmentViewLoadSubView:)]) {
            NSMutableArray *array = [NSMutableArray array];
            [array addObjectsFromArray:_subViews];
            UIView *subView = [self.delegate by_segmentViewLoadSubView:_defultSelIndex];
            [view addSubview:subView];
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
            [array replaceObjectAtIndex:_defultSelIndex withObject:subView];
            self.subViews = [array copy];
        }
    }
    if ([self.delegate respondsToSelector:@selector(by_segmentViewDidScrollToIndex:)]) {
        [self.delegate by_segmentViewDidScrollToIndex:_defultSelIndex];
    }
}

#pragma mark - customMehtod
- (void)selectIndex:(NSInteger)index{
    if (index == _lastIndex) return;
    [_scrollView setContentOffset:CGPointMake(kCommonScreenWidth*index, 0) animated:YES];
}
- (void)didSelectSubViewIndex:(NSInteger)index{
    // 设置高亮
    _lastIndex = index;
    if (index <= self.subViews.count - 1) {
        self.currentSubView = self.subViews[index];
    }
    UIButton *button = [self viewWithTag:baseTag + index];
    for (UIButton *btn in self.buttons) {
        btn.selected = button == btn ? YES : NO;
        if (btn == button) {
            [UIView animateWithDuration:0.1 animations:^{
                self.selLineView.center = CGPointMake(btn.center.x, self.selLineView.center.y);
            }];
            
            if (btn.center.x > self.headerView.center.x) {
                if (self.headerView.contentSize.width > kCommonScreenWidth) {
                    if (self.headerView.contentSize.width - btn.center.x < self.headerView.center.x) {
                        [self.headerView setContentOffset:CGPointMake(self.headerView.contentSize.width - CGRectGetWidth(self.headerView.frame), 0) animated:YES];
                        continue;
                    }
                    CGFloat offsetX = btn.center.x - self.headerView.center.x;
                    [self.headerView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
                }else{
                    [self.headerView setContentOffset:CGPointMake(0, 0) animated:YES];

                }
            }
            else if (btn.center.x < self.headerView.center.x && self.headerView.contentOffset.x != 0) {
                [self.headerView setContentOffset:CGPointMake(0, 0) animated:YES];
            }
            [self layoutIfNeeded];
        }
    }
}


#pragma mark - buttonAction
- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - baseTag;
    // 如与上次选择一样则返回
    if (index == _lastIndex) return;
    [_scrollView setContentOffset:CGPointMake(kCommonScreenWidth*index, 0) animated:YES];
}

- (void)setDefultSelIndex:(NSInteger)defultSelIndex{
    _defultSelIndex = defultSelIndex;
//    [self didSelectSubViewIndex:defultSelIndex];
    [_scrollView setContentOffset:CGPointMake(kCommonScreenWidth*defultSelIndex, 0) animated:YES];
}

- (void)setAlignment:(BYSegmentAlignment)alignment{
    _alignment = alignment;
    [self reloadHeader];
}

#pragma mark - scrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat indexFloat = (CGFloat)scrollView.contentOffset.x/kCommonScreenWidth;
    NSInteger index = ceil(scrollView.contentOffset.x/kCommonScreenWidth);
    if (indexFloat < index) {
        index = round(scrollView.contentOffset.x/kCommonScreenWidth);
    }
    UIView *view = [_scrollView viewWithTag:2*baseTag + index];
    if (view.subviews.count == 0 && view) {
        if ([self.delegate respondsToSelector:@selector(by_segmentViewLoadSubView:)]) {
            NSMutableArray *array = [NSMutableArray array];
            [array addObjectsFromArray:_subViews];
            UIView *subView = [self.delegate by_segmentViewLoadSubView:index];
            [view addSubview:subView];
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
//            [array addObject:subView];
            [array replaceObjectAtIndex:index withObject:subView];
            self.currentSubView = subView;
            self.subViews = [array copy];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    BOOL scrollToScrollStop = !scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
    if (scrollToScrollStop) {
        [self scrollViewDidEndScroll:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        BOOL dragToDragStop = scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
        if (dragToDragStop) {
            [self scrollViewDidEndScroll:scrollView];
        }
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    BOOL scrollToScrollStop = !scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
    if (scrollToScrollStop) {
        [self scrollViewDidEndScroll:scrollView];
    }
}

- (void)scrollViewDidEndScroll:(UIScrollView *)scrollView{
    NSInteger index = ceil(scrollView.contentOffset.x/kCommonScreenWidth);
    if (_lastIndex == index) return;
    [self didSelectSubViewIndex:index];
    if ([self.delegate respondsToSelector:@selector(by_segmentViewDidScrollToIndex:)]) {
        [self.delegate by_segmentViewDidScrollToIndex:index];
    }
}

#pragma mark - configSubView Method

- (void)setContentView{
    UIScrollView *headerView = [self getHeaderView];
    [self addSubview:headerView];
    _headerView = headerView;
    CGFloat detal = _titles.count <= count ? 0 : 15;
    UIButton *button = [headerView viewWithTag:baseTag + _titles.count - 1];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.left.mas_equalTo(0);
        make.height.mas_equalTo(headerHeight);
        make.right.equalTo(button.mas_right).offset(detal).priorityLow();
    }];
    
    
    UIScrollView *scrollView = [self getScrollView];
    scrollView.delegate = self;
    [self addSubview:scrollView];
    _scrollView = scrollView;
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(headerHeight, 0, 0, 0));
    }];
    
}

// headerView
- (UIScrollView *)getHeaderView{
    UIScrollView *headerView = [UIScrollView by_init];
    headerView.showsHorizontalScrollIndicator = NO;
    NSMutableArray *arrays = [NSMutableArray array];
    NSMutableArray *tmpArr = [NSMutableArray array];
    for (int i = 0; i < _titles.count; i ++) {
        
        // 先使用临时view顶替所有未加载的subView
        [tmpArr addObject:[UIView new]];
        
        NSString *title = _titles[i];
        UIButton *button = [UIButton by_buttonWithCustomType];
        NSDictionary *normalDic = @{@"title":title,NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0x8f8f8f)};
        NSDictionary *highlight = @{@"title":title,NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0x323232)};
        [button setBy_attributedTitle:normalDic forState:UIControlStateNormal];
        [button setBy_attributedTitle:highlight forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        button.selected = i == 0 ? YES : NO;
        [headerView addSubview:button];
        [arrays addObject:button];
        UIButton *lastBtn = [headerView viewWithTag:baseTag + i - 1];
        button.tag = baseTag + i;
        if (_alignment == BYSegmentAlignmentCenter) {
            CGFloat width = kCommonScreenWidth/_titles.count;
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(width);
                make.top.bottom.mas_equalTo(0);
                make.left.mas_equalTo(width*i);
            }];
        }else if (_alignment == BYSegmentAlignmentLeft){
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(stringGetWidth(title, 14));
                make.top.bottom.mas_equalTo(0);
                if (i == 0) {
                    make.left.mas_equalTo(15);
                }else{
                    make.left.mas_equalTo(lastBtn.mas_right).mas_offset(20);
                }
            }];
        }
    }
    self.buttons = arrays;
    self.subViews = tmpArr;
    
    UIButton *button = [headerView viewWithTag:baseTag];
    UIView *selLineView = [UIView by_init];
    selLineView.backgroundColor = kColorRGBValue(0xea6441);
    selLineView.layer.cornerRadius = 1.0f;
    [headerView addSubview:selLineView];
    _selLineView = selLineView;
    selLineView.frame = CGRectMake(0, headerHeight - self.vernierHeight, 16, self.vernierHeight);
    selLineView.center = CGPointMake(button.center.x, selLineView.center.y);
    
//    UIView *lineView = [UIView by_init];
//    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
//    [headerView addSubview:lineView];
//    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(0);
//        make.width.mas_equalTo(kCommonScreenWidth);
//        make.top.mas_equalTo(headerHeight - 0.5);
//        make.height.mas_equalTo(0.5);
//    }];
    
    return headerView;
}

// contentView
- (UIScrollView *)getScrollView{
    UIScrollView *scrollView = [UIScrollView by_init];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    for (int i = 0; i < _titles.count; i ++) {
        UIView *view = [UIView by_init];
        [scrollView addSubview:view];
        view.tag = 2*baseTag + i;
        @weakify(self);
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.left.mas_equalTo(kCommonScreenWidth*i);
            make.top.mas_equalTo(0);
            make.height.equalTo(scrollView.mas_height).with.offset(0);
            make.width.equalTo(scrollView.mas_width).with.offset(0);
            if (i == self.titles.count - 1) {
                make.right.mas_equalTo(0);
            }
        }];
    }
    return scrollView;
}

@end
