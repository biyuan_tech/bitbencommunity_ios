//
//  BYHomeSearchNavigationView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/9.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeSearchNavigationView.h"
#import "BYHomeSearchController.h"
#import "BYMyActiviryController.h"
#import "BYDaySignInView.h"

@interface BYHomeSearchNavigationView ()

/** 红点 */
@property (nonatomic ,strong) UIView *redDot;

@end

@implementation BYHomeSearchNavigationView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)reloadRedDot{
    self.redDot.hidden = [AccountModel sharedAccountModel].unReadMessage > 0 ? NO : YES;
}

#pragma mark - action
- (void)searchViewAction{
    AbstractViewController *currentController = (AbstractViewController *)[BYTabbarViewController sharedController].currentController;
    BYHomeSearchController *searchController = [[BYHomeSearchController alloc] init];
    [currentController authorizeWithCompletionHandler:^(BOOL successed) {
        if (!successed) return ;
        dispatch_async(dispatch_get_main_queue(), ^{
            [searchController hidesTabBarWhenPushed];
            [currentController.navigationController pushViewController:searchController animated:YES];
        });
    }];
}

- (void)messageBtnAction{
//    [BYDaySignInView showAnimation:@"" bp:@""];
    AbstractViewController *currentController = (AbstractViewController *)[BYTabbarViewController sharedController].currentController;
    MessageNewRootViewController *messageController = [[MessageNewRootViewController alloc] init];
    [currentController authorizeWithCompletionHandler:^(BOOL successed) {
        if (!successed) return ;
        dispatch_async(dispatch_get_main_queue(), ^{
            [messageController hidesTabBarWhenPushed];
            [currentController.navigationController pushViewController:messageController animated:YES];
        });
    }];
}

#pragma mark - configUI

- (void)setContentView{
    UIView *searchView = [[UIView alloc] init];
    searchView.backgroundColor = [UIColor whiteColor];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchViewAction)];
    [searchView addGestureRecognizer:tapGesture];
    [self addSubview:searchView];
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.top.mas_equalTo(0);
//        make.height.mas_equalTo(kSafe_Mas_Top(60));
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    UIImageView *searchBgView = [UIImageView by_init];
    [searchBgView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    searchBgView.layer.cornerRadius = 4.0f;
    searchBgView.userInteractionEnabled = YES;
    [searchView addSubview:searchBgView];
    [searchBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.right.mas_equalTo(-45);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(35);
    }];
    
    UIImageView *searchImgView = [UIImageView by_init];
    [searchImgView setImage:[UIImage imageNamed:@"livehome_search"]];
    [searchBgView addSubview:searchImgView];
    [searchImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(searchImgView.image.size.width);
        make.height.mas_equalTo(searchImgView.image.size.height);
    }];
    
    UILabel *searchTitleLab = [UILabel by_init];
    [searchTitleLab setBy_font:14];
    [searchTitleLab setTextColor:kColorRGBValue(0xb6b6b7)];
    searchTitleLab.textAlignment = NSTextAlignmentLeft;
    searchTitleLab.text = @"搜索视频 / 用户 / 观点";
    [searchBgView addSubview:searchTitleLab];
    [searchTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(searchImgView.center_y).mas_offset(0);
        make.left.mas_equalTo(38);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(17);
    }];
    
    UIButton *messageBtn = [UIButton by_buttonWithCustomType];
    [messageBtn setBy_imageName:@"videohome_message" forState:UIControlStateNormal];
    [messageBtn addTarget:self action:@selector(messageBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [searchView addSubview:messageBtn];
    [messageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(searchBgView.mas_right).mas_offset(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(searchBgView.mas_height).mas_offset(0);
        make.centerY.mas_equalTo(searchBgView.mas_centerY).mas_offset(0);
    }];
    
    UIView *redDot = [UIView by_init];
    [redDot setBackgroundColor:kRedColor];
    redDot.layer.cornerRadius = 2.0f;
    redDot.hidden = YES;
    [searchView addSubview:redDot];
    self.redDot = redDot;
    [redDot mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(4);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(kSafe_Mas_Top(30));
    }];
}

@end
