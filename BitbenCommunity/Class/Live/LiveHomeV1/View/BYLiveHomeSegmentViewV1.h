//
//  BYLiveHomeSegmentViewV1.h
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger , BYSegmentAlignment) {
    BYSegmentAlignmentLeft,
    BYSegmentAlignmentCenter,
    BYSegmentAlignmentRight,
};

@protocol BYLiveHomeSegmentViewDelegateV1 <NSObject>

/** 根据index自定义添加View */
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index;

/** 当前滚动到的Index */
- (void)by_segmentViewDidScrollToIndex:(NSInteger)index;

@end

@interface BYLiveHomeSegmentViewV1 : UIView


/** 初始化选择的index (默认0) */
@property (nonatomic ,assign) NSInteger defultSelIndex;
@property (nonatomic ,weak) id<BYLiveHomeSegmentViewDelegateV1>delegate;
@property (nonatomic ,strong ,readonly) NSArray *titles;
@property (nonatomic ,strong ,readonly) NSArray *subViews;
/** currentSubView */
@property (nonatomic ,strong) UIView *currentSubView;
/** 分部方式 (左，中，右) */
@property (nonatomic ,assign) BYSegmentAlignment alignment;
/** 被选择的状态 */
@property (nonatomic ,strong) NSDictionary *selectAttributed;
/** 未选中状态 */
@property (nonatomic ,strong) NSDictionary *normalAttributed;
/** 浮标厚度 */
@property (nonatomic ,assign) CGFloat vernierHeight;

/** 点击分类回调 */
@property (nonatomic ,copy) void (^didLoadSubView)(NSInteger index,UIView *subView);

- (instancetype)initWithTitles:(NSString *)titles, ... NS_REQUIRES_NIL_TERMINATION;

- (void)selectIndex:(NSInteger)index;
@end

NS_ASSUME_NONNULL_END
