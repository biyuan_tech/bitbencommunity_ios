//
//  BYLiveHomeHeaderRecommendView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/3.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYLiveHomeHeaderRecommendView : UIView

/** 数据 */
@property (nonatomic ,strong) NSArray *data;

@property (nonatomic ,copy) void (^didSelectRecommendIndex)(NSInteger index);

- (void)reloadData;

/** 启动定时 */
- (void)startTimer;
/** 销毁定时 */
- (void)destoryTimer;

@end

NS_ASSUME_NONNULL_END
