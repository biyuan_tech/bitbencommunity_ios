//
//  BYLiveHomeHeaderRecommendView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/3.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYLiveHomeHeaderRecommendView.h"
#import "iCarousel.h"
#import "BYHomeBannerModel.h"

@interface BYLiveHomeHeaderRecommendView ()<iCarouselDelegate,iCarouselDataSource>

/** scrollView */
//@property (nonatomic ,strong) UIScrollView *scrollView;
/** 轮播 */
@property (nonatomic ,strong) iCarousel *scrollView;
@property (nonatomic ,strong) NSTimer *timer;

@end

@implementation BYLiveHomeHeaderRecommendView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)reloadData{
    [_scrollView reloadData];
    [self startTimer];
}

- (void)startTimer{
    if (!self.data.count) return;
    [self destoryTimer];
    if (!_timer) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(scrollAdBanner) userInfo:nil repeats:YES];
    }
}

- (void)destoryTimer{
    if (_timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)scrollAdBanner{
    NSInteger index;
    if (_scrollView.currentItemIndex < self.data.count - 1)
        index = _scrollView.currentItemIndex + 1;
    else
        index = 0;
    [_scrollView scrollToItemAtIndex:index duration:1.0];
}
#pragma mark - iCarouselDelegate/DataScoure
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return self.data.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view{
    if (view == nil) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 5, CGRectGetWidth(carousel.frame), 32)];
        view.clipsToBounds = YES;
        UILabel *titleLab = [UILabel by_init];
        titleLab.frame = CGRectMake(40, 5, CGRectGetWidth(carousel.frame) - 55, 22);
        [titleLab setBy_font:13];
        titleLab.textColor = kColorRGBValue(0x323232);
        [view addSubview:titleLab];
        titleLab.tag = 543;
    }
    UILabel *titleLab = [view viewWithTag:543];
    BYHomeBannerModel *model = self.data[index];
    titleLab.text = model.content;
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    switch (option) {
            // 设置滚动循环
        case iCarouselOptionWrap:
            return YES;
            break;
        default:
            break;
    }
    return value;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    if (self.didSelectRecommendIndex) {
        self.didSelectRecommendIndex(index);
    }
}

- (void)carouselWillBeginDragging:(iCarousel *)carousel{
    [self destoryTimer];
}

- (void)carouselDidEndDragging:(iCarousel *)carousel willDecelerate:(BOOL)decelerate{
    [self startTimer];
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{

}

- (void)setContentView{
    iCarousel *scrollView = [[iCarousel alloc] initWithFrame:self.bounds];
    scrollView.delegate = self;
    scrollView.dataSource = self;
    scrollView.pagingEnabled = YES;
    scrollView.vertical = YES;
    scrollView.clipsToBounds = YES;
    scrollView.scrollEnabled = NO;
    [self addSubview:scrollView];
    self.scrollView = scrollView;
}

@end
