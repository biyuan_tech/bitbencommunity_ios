//
//  BYHomeRecommendHeaderVIew.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYHomeRecommendHeaderView.h"
#import "iCarousel.h"
#import "BYPaomaView.h"
#import "BYLiveHomeHeaderRecommendView.h"
#import "AppointmentViewController.h"
#import "BYHomeBannerModel.h"

@interface BYHomeRecommendHeaderView ()<iCarouselDelegate,iCarouselDataSource>
// **广告轮播
@property (nonatomic ,strong) iCarousel *adScrollerView;

@property (nonatomic ,strong) NSTimer *timer;
//@property (nonatomic ,strong) UIPageControl *pageControl;
/** pageControl */
@property (nonatomic ,strong) BYPageControl *pageControl;
/** scrollview */
@property (nonatomic ,strong) BYLiveHomeHeaderRecommendView *headerRecommendView;



@end

static NSInteger kBaseTag = 0x442;
@implementation BYHomeRecommendHeaderView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setContentView];
    }
    return self;
}

- (void)setContentView{
    [self addSubview:self.adScrollerView];
    
    self.pageControl = [[BYPageControl alloc] init];
    [self addSubview:self.pageControl];
    [_pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(117);
        make.height.mas_equalTo(6);
    }];
//    if (![AccountModel sharedAccountModel].isShenhe){
        [self addSortView];
//    }
    [self addMessageView];
}

- (void)setAdvertData:(NSArray *)advertData{
    _advertData = advertData;
    if (advertData.count == 2) {
        self.recommendDatas = advertData[0];
        self.advertArr = advertData[1];
    }
    else{
        self.advertArr = advertData[0];
    }
}

- (void)reloadData{
    [_adScrollerView reloadData];
    self.pageControl.numberOfPages = self.advertArr.count;
    self.pageControl.currentPage = _adScrollerView.currentItemIndex;
    [self startTimer];
    
    self.headerRecommendView.data = _recommendDatas;
    [self.headerRecommendView reloadData];
    
}

- (void)startTimer{
    if (!self.advertArr.count) return;
    [self destoryTimer];
    if (!_timer) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(scrollAdBanner) userInfo:nil repeats:YES];
    }
    
    [self.headerRecommendView startTimer];
}

- (void)destoryTimer{
    if (_timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    [self.headerRecommendView destoryTimer];
}

- (void)scrollAdBanner{
    NSInteger index;
    if (_adScrollerView.currentItemIndex < self.advertArr.count - 1)
        index = _adScrollerView.currentItemIndex + 1;
    else
        index = 0;
    [_adScrollerView scrollToItemAtIndex:index duration:0.5];
}

#pragma mark - action
- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - kBaseTag;
    if (self.didSelectModuleIndex) {
        self.didSelectModuleIndex(index);
    }
}

- (void)paomaViewAction{
    
}

#pragma mark - iCarouselDelegate/DataScoure
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return self.advertArr.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view{
    if (view == nil) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, CGRectGetHeight(carousel.frame))];
        PDImageView *imageView = [[PDImageView alloc] initWithFrame:CGRectMake(15, 0, kCommonScreenWidth - 30, CGRectGetHeight(carousel.frame))];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        imageView.layer.cornerRadius = 5.0f;
        [view addSubview:imageView];
        imageView.tag = 315;
    }
    PDImageView *imageView = [view viewWithTag:315];
    BYHomeBannerModel *model = self.advertArr[index];
    [imageView uploadMainImageWithURL:model.content placeholder:nil imgType:PDImgTypeOriginal callback:nil];
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    switch (option) {
            // 设置滚动循环
        case iCarouselOptionWrap:
            return YES;
            break;
        default:
            break;
    }
    return value;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    if (self.didSelectAdvertIndex) {
        self.didSelectAdvertIndex(index);
    }
}

- (void)carouselWillBeginDragging:(iCarousel *)carousel{
    [self destoryTimer];
}

- (void)carouselDidEndDragging:(iCarousel *)carousel willDecelerate:(BOOL)decelerate{
    [self startTimer];
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
    _pageControl.currentPage = carousel.currentItemIndex;
}

#pragma makr - initMethod

- (void)addSortView{
    NSArray *imgNames = @[@"livehome_info",@"livehome_ course",@"livehome_activity",@"livehome_zhuanlan",@"livehome_youyue"];
    NSArray *titles = @[@"情报",@"课程",@"会议",@"专栏",@"币本有约"];
    CGFloat spaceW = (kCommonScreenWidth - 44*imgNames.count - 30)/(imgNames.count - 1);
    for (int i = 0; i < 5; i ++) {
        UIButton *sortBtn = [UIButton by_buttonWithCustomType];
        [sortBtn setBy_imageName:imgNames[i] forState:UIControlStateNormal];
        [sortBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        sortBtn.tag = kBaseTag + i;
        [self addSubview:sortBtn];
        [sortBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15 + (sortBtn.imageView.image.size.width + spaceW)*i);
            make.top.mas_equalTo(self.adScrollerView.mas_bottom).mas_offset(17);
            make.width.mas_equalTo(sortBtn.imageView.image.size.width);
            make.height.mas_equalTo(sortBtn.imageView.image.size.height);
        }];
        
        UILabel *titleLab = [UILabel by_init];
        titleLab.text = titles[i];
        titleLab.font = [UIFont systemFontOfSize:14];
        titleLab.textColor = kColorRGBValue(0x323232);
        [self addSubview:titleLab];
        [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(stringGetWidth(titles[i], 14));
            make.height.mas_equalTo(stringGetHeight(titles[i], 14));
            make.top.mas_equalTo(sortBtn.mas_bottom).mas_offset(12);
            make.centerX.mas_equalTo(sortBtn.mas_centerX).mas_offset(0);
        }];
        
//        if (i == 2) {
//            UIImageView *newImgView = [[UIImageView alloc] init];
//            [newImgView by_setImageName:@"common_new"];
//            [self addSubview:newImgView];
//            [newImgView mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.width.mas_equalTo(newImgView.image.size.width);
//                make.height.mas_equalTo(newImgView.image.size.height);
//                make.centerX.mas_equalTo(sortBtn).offset(sortBtn.imageView.image.size.width/2);
//                make.top.mas_equalTo(sortBtn).offset(-2);
//            }];
//        }
    }
}

- (void)addMessageView{
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(-42);
    }];
    
    UIImageView *msgImgView = [[UIImageView alloc] init];
    [msgImgView setImage:[UIImage imageNamed:@"livehome_msg"]];
    [self addSubview:msgImgView];
    [msgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.bottom.mas_equalTo(-15);
        make.width.mas_equalTo(msgImgView.image.size.width);
        make.height.mas_equalTo(msgImgView.image.size.height);
    }];
    
    BYLiveHomeHeaderRecommendView *headerRecommendView;
//    if ([AccountModel sharedAccountModel].isShenhe){
//        headerRecommendView = [[BYLiveHomeHeaderRecommendView alloc] initWithFrame:CGRectMake(0, 130, kCommonScreenWidth, 42)];
//    }else{
        headerRecommendView = [[BYLiveHomeHeaderRecommendView alloc] initWithFrame:CGRectMake(0, 233, kCommonScreenWidth, 42)];
//    }

    @weakify(self);
    headerRecommendView.didSelectRecommendIndex = ^(NSInteger index) {
        @strongify(self);
        if (self.didSelectRecommendIndex)
            self.didSelectRecommendIndex(index);
    };
    [self addSubview:headerRecommendView];
    self.headerRecommendView = headerRecommendView;
}

- (iCarousel *)adScrollerView{
    if (!_adScrollerView) {
        _adScrollerView = [[iCarousel alloc] initWithFrame:CGRectMake(0, 7, kCommonScreenWidth, 120)];
        _adScrollerView.delegate = self;
        _adScrollerView.dataSource = self;
        _adScrollerView.pagingEnabled = YES;
    }
    return _adScrollerView;
}

@end

@interface BYPageControl ()

/** pages */
@property (nonatomic ,strong) NSArray *pages;


@end

@implementation BYPageControl

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)setNumberOfPages:(NSInteger)numberOfPages{
    _numberOfPages = numberOfPages;
    [self setContentView];
}

- (void)setCurrentPage:(NSInteger)currentPage{
    for (UIButton *btn in self.pages) {
        NSInteger index = btn.tag - 823;
        btn.selected = index == currentPage ? YES : NO;
    }
}

- (void)setContentView{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < _numberOfPages; i ++) {
        UIButton *pageBtn = [UIButton by_buttonWithCustomType];
        [pageBtn setBackgroundImage:[UIImage imageWithColor:kColorRGB(255, 255, 255, 0.5)] forState:UIControlStateNormal];
        [pageBtn setBackgroundImage:[UIImage imageWithColor:kColorRGB(255, 255, 255, 1)] forState:UIControlStateSelected];
        pageBtn.selected = i == 0 ? YES : NO;
        pageBtn.tag = 823 + i;
        [self addSubview:pageBtn];
        [pageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12*i);
            make.width.mas_equalTo(6);
            make.height.mas_equalTo(1);
            make.top.mas_equalTo(0);
        }];
        [array addObject:pageBtn];
        if (i == 0) {
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(pageBtn.mas_left).mas_offset(0);
            }];
        }
        
        if (i == _numberOfPages - 1) {
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(pageBtn.mas_right).mas_offset(0);
            }];
        }
    }
    self.pages = [array copy];
}

@end
