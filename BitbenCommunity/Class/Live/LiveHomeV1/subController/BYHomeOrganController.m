//
//  BYHomeOrganController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/23.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeOrganController.h"
#import "BYHomeOrganSubController.h"

static NSInteger baseTag = 744;
@interface BYHomeOrganController ()

/** 媒体方 */
@property (nonatomic ,strong) BYHomeOrganSubController *mediaSubController;
/** 项目方 */
@property (nonatomic ,strong) BYHomeOrganSubController *projectSubController;
/** scrollView */
@property (nonatomic ,strong) UIScrollView *scrollView;
/** headerView */
@property (nonatomic ,strong) UIView *headerView;


@end

@implementation BYHomeOrganController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addHeaderView];
    [self addScrollView];
}

- (void)autoReload{
    if (self.scrollView.contentOffset.x >= kCommonScreenWidth) {
        [self.projectSubController autoReload];
    }else{
        [self.mediaSubController autoReload];
    }
}

- (void)reloadData{
    [self.mediaSubController reloadData];
    [self.projectSubController reloadData];
}

#pragma mark - action
- (void)btnAction:(UIButton *)sender{
    NSInteger index = sender.tag - baseTag;
    for (int i = 0; i < 2; i ++) {
        UIButton *btn = [self.headerView viewWithTag:baseTag + i];
        btn.selected = btn == sender ? YES : NO;
    }
    [self.scrollView setContentOffset:CGPointMake(kCommonScreenWidth*index, 0) animated:YES];
}

#pragma mark - configUI

- (void)addHeaderView{
    UIView *headerView = [UIView by_init];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:headerView];
    self.headerView = headerView;
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(53);
    }];
    
    NSArray *titles = @[@"媒体",@"项目"];
    for (int i = 0; i < 2; i ++) {
        UIButton *btn = [UIButton by_buttonWithCustomType];
        [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBy_attributedTitle:@{@"title":titles[i],
                                     NSFontAttributeName:[UIFont systemFontOfSize:15],
                                     NSForegroundColorAttributeName:kColorRGBValue(0x8f8f8f)
                                     } forState:UIControlStateNormal];
        [btn setBy_attributedTitle:@{@"title":titles[i],
                                     NSFontAttributeName:[UIFont systemFontOfSize:15],
                                     NSForegroundColorAttributeName:kColorRGBValue(0x323232)
                                     } forState:UIControlStateSelected];
        btn.selected = i == 0 ? YES : NO;
        btn.tag = baseTag + i;
        [headerView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(8);
            make.bottom.mas_equalTo(0);
            make.left.mas_equalTo((kCommonScreenWidth/2)*i);
            make.width.mas_equalTo(kCommonScreenWidth/2);
        }];
    }
    
    UIView *verticalView = [UIView by_init];
    verticalView.backgroundColor = kColorRGBValue(0xe7e7ea);
    [headerView addSubview:verticalView];
    [verticalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(13);
        make.width.mas_equalTo(0.5);
        make.height.mas_equalTo(35);
    }];
    
    UIView *horizontalView = [UIView by_init];
    horizontalView.backgroundColor = kColorRGBValue(0xe7e7ea);
    [headerView addSubview:horizontalView];
    [horizontalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(8);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)addScrollView{
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.scrollEnabled = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    [self.scrollView setContentSize:CGSizeMake(kCommonScreenWidth*2, 0)];
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(53, 0, 0, 0));
    }];
    
    [self addSubController];
}

- (void)addSubController{
//    UIViewController *currentController = [BYTabbarViewController sharedController].currentController;
    
    self.mediaSubController = [[BYHomeOrganSubController alloc] init];
    self.mediaSubController.type = BY_HOME_ARTICLE_TYPE_ORGAN_MEDIA;
    [self addChildViewController:self.mediaSubController];
    [self.scrollView addSubview:self.mediaSubController.view];
    [self.mediaSubController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(0);
        make.height.mas_equalTo(self.scrollView);
        make.width.mas_equalTo(kCommonScreenWidth);
    }];
    
    self.projectSubController = [[BYHomeOrganSubController alloc] init];
    self.projectSubController.type = BY_HOME_ARTICLE_TYPE_ORGAN_PROJECT;
    [self addChildViewController:self.projectSubController];
    [self.scrollView addSubview:self.projectSubController.view];
    [self.projectSubController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(self.scrollView);
        make.left.mas_equalTo(kCommonScreenWidth);
        make.width.mas_equalTo(kCommonScreenWidth);
    }];
    
    
}


@end
