//
//  BYHomeTopicController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"
#import "BYLiveHomeSegmentViewV1.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeTopicController : BYCommonViewController

- (void)autoReload;

@end

NS_ASSUME_NONNULL_END
