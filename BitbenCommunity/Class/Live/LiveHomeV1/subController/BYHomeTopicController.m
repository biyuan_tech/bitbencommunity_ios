//
//  BYHomeTopicController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeTopicController.h"
#import "BYTopicDetailController.h"

#import "BYHomeTopicModel.h"

@interface BYHomeTopicController ()<BYCommonTableViewDelegate>


/** BYCommonTableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;

@end

@implementation BYHomeTopicController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addTableView];
    [self loadRequestGetTopicList];
}

- (void)autoReload{
    [self.tableView beginRefreshing];
    [self reloadData];
}

- (void)reloadData{
    [self loadRequestGetTopicList];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BYHomeTopicModel *model = tableView.tableData[indexPath.row];
    BYTopicDetailController *topicDetailController = [[BYTopicDetailController alloc] init];
    BYCommonViewController *currentController =  (BYCommonViewController *)[BYTabbarViewController sharedController].currentController;
    topicDetailController.topic = model.content;
    [currentController authorizePush:topicDetailController animation:YES];
}

#pragma mark - request
- (void)loadRequestGetTopicList{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetTopicList:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        self.tableView.tableData = object;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

#pragma mark - configUI
- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    [self.tableView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.tableView addHeaderRefreshTarget:self action:@selector(reloadData)];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

@end
