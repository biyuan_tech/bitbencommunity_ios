//
//  BYHomeNewsletterRootCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/11/5.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeNewsletterRootCell.h"

static char actionClickWithShareManagerWithBlockKey;
static char actionClickWithAblumShowBlockKey;
@interface BYHomeNewsletterRootCell()
@property (nonatomic,strong)PDImageView *dotImgView;
@property (nonatomic,strong)PDImageView *lineView;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UIButton *dingButton;
@property (nonatomic,strong)UIButton *dowmButton;
@property (nonatomic,strong)UIButton *shareButton;
@property (nonatomic,strong)UIButton *tapGestureButton;
@end

@implementation BYHomeNewsletterRootCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.dotImgView = [[PDImageView alloc]init];
    self.dotImgView.backgroundColor = [UIColor clearColor];
    self.dotImgView.image = [UIImage imageNamed:@"icon_kuaixun_dot"];
    
    self.lineView = [[PDImageView alloc]init];
    self.lineView.backgroundColor = [UIColor hexChangeFloat:@"DFE2E4"];
    [self addSubview:self.lineView];
    [self addSubview:self.dotImgView];
    
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"959595"];
    [self addSubview:self.timeLabel];
    
    self.titleLabel = [GWViewTool createLabelFont:@"17" textColor:@"323232"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.numberOfLines = 0;
    [self addSubview:self.titleLabel];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"14" textColor:@"323232"];
    [self addSubview:self.dymicLabel];
    
    self.ablumImgView = [[PDImageView alloc]init];
    self.ablumImgView.backgroundColor = [UIColor clearColor];
    self.ablumImgView.contentMode = UIViewContentModeCenter;
    self.ablumImgView.clipsToBounds = YES;
    [self addSubview:self.ablumImgView];
    
    self.tapGestureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.tapGestureButton];
        
    __weak typeof(self)weakSelf = self;
    self.dingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.dingButton];
    
    self.dowmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.dowmButton];
    
    self.shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.shareButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithShareManagerWithBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.shareButton];
}

-(void)setTransferIndex:(NSInteger)transferIndex{
    _transferIndex = transferIndex;
}

-(void)setTransferModel:(BYHomeNewsletterModel *)transferModel{
    _transferModel = transferModel;
    
    // time
    self.timeLabel.text = [NSDate getCurrentTimeWithKuaixunShifen:transferModel.create_time / 1000.];
    self.timeLabel.frame = CGRectMake(LCFloat(31.5), LCFloat(20), kScreenBounds.size.width - LCFloat(100), [NSString contentofHeightWithFont:self.timeLabel.font]);
    
    if (self.transferIndex == 0){
        self.lineView.frame = CGRectMake(LCFloat(18), CGRectGetMaxY(self.dotImgView.frame) , .5f, [BYHomeNewsletterRootCell calculationCellHeightWithModel:self.transferModel] - CGRectGetMaxY(self.dotImgView.frame));
    } else {
        self.lineView.frame = CGRectMake(LCFloat(18), 0, .5f, [BYHomeNewsletterRootCell calculationCellHeightWithModel:self.transferModel] );
    }    
    self.dotImgView.frame = CGRectMake(self.timeLabel.orgin_x - LCFloat(10), self.timeLabel.orgin_y + (self.timeLabel.size_height - LCFloat(6)) / 2., LCFloat(6), LCFloat(6));
    self.dotImgView.center_x = self.lineView.center_x;
    
    // title
    CGFloat width = kScreenBounds.size.width - LCFloat(31.5) - LCFloat(15.5);
    self.titleLabel.text = transferModel.title;
    // 1. 添加行间距
    [self.titleLabel hangjianjusetLabelSpace];
    
    CGFloat titleSizeHeight = [UILabel hangjianjugetSpaceLabelHeight:transferModel.title withFont:[[UIFont fontWithCustomerSizeName:@"17"] boldFont] withWidth:width limitHeight:kScreenBounds.size.height];
    self.titleLabel.frame = CGRectMake(LCFloat(31.5), CGRectGetMaxY(self.timeLabel.frame) + LCFloat(13), width, titleSizeHeight);
    
    //
    self.dymicLabel.text = transferModel.content;
    CGSize dymicSize = [transferModel.content sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (transferModel.hasShowAll){
        [self.dymicLabel hangjianjusetLabelSpace];
        CGFloat dymicHangjianju = [UILabel hangjianjugetSpaceLabelHeight:transferModel.content withFont:[UIFont fontWithCustomerSizeName:@"14"] withWidth:width limitHeight:CGFLOAT_MAX];
        self.dymicLabel.numberOfLines = 0;
        self.dymicLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(13), width, dymicHangjianju + LCFloat(30));
    } else {
        [self.dymicLabel hangjianjusetLabelSpace];
        
        NSInteger realRows = dymicSize.height / [NSString contentofHeightWithFont:self.dymicLabel.font];
        self.dymicLabel.numberOfLines = MIN(4, realRows);
        
        CGFloat limitHeight =MIN(4, realRows) * ([NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]] + LCFloat(6));
        CGFloat dymicHangjianju = [UILabel hangjianjugetSpaceLabelHeight:transferModel.content withFont:[UIFont fontWithCustomerSizeName:@"14"] withWidth:width limitHeight: limitHeight];
        self.dymicLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(13), width,  MIN(limitHeight, dymicHangjianju));
    }

    
    // ablum
    if (transferModel.picture.count){
        self.ablumImgView.hidden = NO;
        self.tapGestureButton.hidden = NO;
        
        self.ablumImgView.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.dymicLabel.frame) + LCFloat(13), width, LCFloat(150));
        __weak typeof(self)weakSelf = self;
        [self.ablumImgView uploadHDImageWithURL:[transferModel.picture firstObject] callback:^(UIImage *image) {
            if (!weakSelf){
                return ;
            }
            transferModel.ablum = image;
        }];
    } else {
        self.ablumImgView.hidden = YES;
        self.tapGestureButton.hidden = YES;
    }

    //
    if (transferModel.picture.count){
        self.dingButton.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.ablumImgView.frame) + LCFloat(13), LCFloat(90), LCFloat(30));
    } else {
        self.dingButton.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.dymicLabel.frame) + LCFloat(13), LCFloat(90), LCFloat(30));
    }
    
    NSString *dingStr = [NSString stringWithFormat:@"%li",transferModel.count_support];
    NSString *caiStr =  [NSString stringWithFormat:@"%li",transferModel.count_tread];
    if (transferModel.count_support > 10000){
        dingStr = [NSString stringWithFormat:@"%.2f 万",transferModel.count_support * 1.0 / 10000];
    }
    
    if (transferModel.count_tread > 10000){
        caiStr = [NSString stringWithFormat:@"%.2f 万",transferModel.count_tread * 1.0 / 10000];
    }
    
    [self.dingButton setImage:[UIImage imageNamed:@"icon_liaoxun_up_nor"] forState:UIControlStateNormal];
    [self.dingButton setTitle:[NSString stringWithFormat:@"利好 %@",dingStr] forState:UIControlStateNormal];
    [self.dingButton setTitleColor:[UIColor hexChangeFloat:@"323232"] forState:UIControlStateNormal];
    self.dingButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"13"];
    [self.dingButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleLeft imageTitleSpace:7];
    
    [self.dowmButton setImage:[UIImage imageNamed:@"icon_liaoxun_down_nor"] forState:UIControlStateNormal];
    [self.dowmButton setTitle:[NSString stringWithFormat:@"利空 %@",caiStr] forState:UIControlStateNormal];
    [self.dowmButton setTitleColor:[UIColor hexChangeFloat:@"323232"] forState:UIControlStateNormal];
    self.dowmButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"13"];
    self.dowmButton.frame = CGRectMake(CGRectGetMaxX(self.dingButton.frame) + LCFloat(20), self.dingButton.orgin_y, self.dingButton.size_width, self.dingButton.size_height);
    [self.dowmButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleLeft imageTitleSpace:7];
    
    [self.shareButton setImage:[UIImage imageNamed:@"icon_liaoxun_share_nor"] forState:UIControlStateNormal];
    [self.shareButton setTitle:@"分享" forState:UIControlStateNormal];
    [self.shareButton setTitleColor:[UIColor hexChangeFloat:@"323232"] forState:UIControlStateNormal];
    self.shareButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"13"];
    self.shareButton.frame = CGRectMake(CGRectGetMaxX(self.dymicLabel.frame) - self.dingButton.size_width, self.dingButton.orgin_y, self.dingButton.size_width, self.dingButton.size_height);
    [self.shareButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleLeft imageTitleSpace:7];
    
    __weak typeof(self)weakSelf = self;
    [self.dingButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToLihaoLikong:strongSelf.dingButton hasLihao:YES];
    }];
    
    [self.dowmButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToLihaoLikong:strongSelf.dingButton hasLihao:NO];
    }];
    
    self.tapGestureButton.frame = self.ablumImgView.frame;
    
    [self.tapGestureButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(BYHomeNewsletterModel *model) = objc_getAssociatedObject(strongSelf, &actionClickWithAblumShowBlockKey);
        if (block){
            block(strongSelf.transferModel);
        }
    }];
    
    if (transferModel.bulletin_type == 1){
        self.titleLabel.textColor = [UIColor hexChangeFloat:@"DA1A00"];
        self.dymicLabel.textColor = [UIColor hexChangeFloat:@"DA1A00"];
    } else {
        self.titleLabel.textColor = [UIColor hexChangeFloat:@"323232"];
        self.dymicLabel.textColor = [UIColor hexChangeFloat:@"323232"];
    }
    
    if (transferModel.isSupport){
        [self.dingButton setTitleColor:[UIColor hexChangeFloat:@"4DCC86"] forState:UIControlStateNormal];
        [self.dingButton setImage:[UIImage imageNamed:@"icon_liaoxun_up_hlt"] forState:UIControlStateNormal];
    } else {
        [self.dingButton setTitleColor:[UIColor hexChangeFloat:@"8F8F8F"] forState:UIControlStateNormal];
        [self.dingButton setImage:[UIImage imageNamed:@"icon_liaoxun_up_nor"] forState:UIControlStateNormal];
    }
    
    if (transferModel.isTread){

        [self.dowmButton setTitleColor:[UIColor hexChangeFloat:@"CC5F4D"] forState:UIControlStateNormal];
        [self.dowmButton setImage:[UIImage imageNamed:@"icon_liaoxun_down_hlt"] forState:UIControlStateNormal];
    } else {
        [self.dowmButton setTitleColor:[UIColor hexChangeFloat:@"8F8F8F"] forState:UIControlStateNormal];
        [self.dowmButton setImage:[UIImage imageNamed:@"icon_liaoxun_down_nor"] forState:UIControlStateNormal];
    }
}

-(void)actionClickWithAblumShowBlock:(void(^)(BYHomeNewsletterModel *model))block{
    objc_setAssociatedObject(self, &actionClickWithAblumShowBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


-(void)actionClickWithShareManagerWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithShareManagerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeightWithModel:(BYHomeNewsletterModel *)model{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(20);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    cellHeight += LCFloat(13);
    
    // title
    CGFloat main_width = kScreenBounds.size.width - LCFloat(31.5) - LCFloat(15.5);
    CGFloat titleHeight = [UILabel hangjianjugetSpaceLabelHeight:model.title withFont:[[UIFont fontWithCustomerSizeName:@"17"] boldFont] withWidth:main_width limitHeight:CGFLOAT_MAX];
    
    cellHeight += titleHeight;
    
    // content
    cellHeight += LCFloat(13.5);
    CGSize contentSize = [model.content sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"14"] constrainedToSize:CGSizeMake(main_width, CGFLOAT_MAX)];
    
    if (model.hasShowAll){           //
        CGFloat dymicHangjianju = [UILabel hangjianjugetSpaceLabelHeight:model.content withFont:[UIFont fontWithCustomerSizeName:@"14"] withWidth:main_width limitHeight:kScreenBounds.size.height];
        cellHeight += dymicHangjianju;
        cellHeight += LCFloat(30);
    } else {
        NSInteger realRows = contentSize.height / [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]];
    
        CGFloat limitHeight = MIN(4, realRows) * ([NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]] + LCFloat(6));
        
        CGFloat dymicHangjianju = [UILabel hangjianjugetSpaceLabelHeight:model.content withFont:[UIFont fontWithCustomerSizeName:@"14"] withWidth:main_width limitHeight:limitHeight];
        
        
        cellHeight += MIN(limitHeight, dymicHangjianju) ;
    }
    
    // pic
    if (model.picture.count){
        cellHeight += LCFloat(13.5);
        cellHeight += LCFloat(150);
    }

    //
    cellHeight += LCFloat(20);
    cellHeight += LCFloat(30);
    cellHeight += LCFloat(11);
    return cellHeight;
}

#pragma mark - 利好利空
-(void)sendRequestToLihaoLikong:(UIButton *)button hasLihao:(BOOL)hasLihao{
    if (self.transferModel.isSupport || self.transferModel.isTread){
        return;
    }
    
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"bulletin_id":self.transferModel.bulletin_id,@"operate_type":@(hasLihao?0:1)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:create_bulletin_operate requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (hasLihao){
                strongSelf.transferModel.isSupport = YES;
                if (strongSelf.transferModel.isSupport){
                    strongSelf.transferModel.count_support += 1;
                    
                    NSString *dingStr = [NSString stringWithFormat:@"%li",strongSelf.transferModel.count_support];
                    if (strongSelf.transferModel.count_support > 10000){
                        dingStr = [NSString stringWithFormat:@"%.2f 万",strongSelf.transferModel.count_support * 1.0 / 10000];
                    }
                    
                    [self.dingButton setTitle:[NSString stringWithFormat:@"利好 %@",dingStr] forState:UIControlStateNormal];
                    [strongSelf.dingButton setTitleColor:[UIColor hexChangeFloat:@"4DCC86"] forState:UIControlStateNormal];
                    [strongSelf.dingButton setImage:[UIImage imageNamed:@"icon_liaoxun_up_hlt"] forState:UIControlStateNormal];
                } else {
                    [strongSelf.dingButton setTitleColor:[UIColor hexChangeFloat:@"8F8F8F"] forState:UIControlStateNormal];
                    [strongSelf.dingButton setImage:[UIImage imageNamed:@"icon_liaoxun_up_nor"] forState:UIControlStateNormal];
                }
            } else {
                strongSelf.transferModel.isTread = YES;
                strongSelf.transferModel.count_tread += 1;
                NSString *caiStr = [NSString stringWithFormat:@"%li",strongSelf.transferModel.count_tread];
                if (strongSelf.transferModel.count_tread > 10000){
                    caiStr = [NSString stringWithFormat:@"%.2f 万",strongSelf.transferModel.count_tread * 1.0 / 10000];
                }
                
                [self.dowmButton setTitle:[NSString stringWithFormat:@"利空 %@",caiStr] forState:UIControlStateNormal];
                if (strongSelf.transferModel.isTread){
                    [strongSelf.dowmButton setTitleColor:[UIColor hexChangeFloat:@"CC5F4D"] forState:UIControlStateNormal];
                    [strongSelf.dowmButton setImage:[UIImage imageNamed:@"icon_liaoxun_down_hlt"] forState:UIControlStateNormal];
                } else {
                    [strongSelf.dowmButton setTitleColor:[UIColor hexChangeFloat:@"8F8F8F"] forState:UIControlStateNormal];
                    [strongSelf.dowmButton setImage:[UIImage imageNamed:@"icon_liaoxun_down_nor"] forState:UIControlStateNormal];
                }
            }
        }
    }];
}






@end
