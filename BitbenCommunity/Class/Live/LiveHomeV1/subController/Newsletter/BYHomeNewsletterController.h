//
//  BYHomeNewsletterController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/5.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeNewsletterController : AbstractViewController

-(void)notificationDirectWithShowManager:(NSString *)infoId;

- (void)autoReload;

@end

NS_ASSUME_NONNULL_END
