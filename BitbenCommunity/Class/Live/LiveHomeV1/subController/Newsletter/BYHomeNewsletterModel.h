//
//  BYHomeNewsletterModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/11/5.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol BYHomeNewsletterModel <NSObject>

@end

@interface BYHomeNewsletterModel : FetchModel

@property (nonatomic,copy)NSString *_id;
@property (nonatomic,copy)NSString *bulletin_id;
@property (nonatomic,assign)NSInteger bulletin_type;

@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,assign)NSInteger count_support;
@property (nonatomic,assign)NSInteger count_tread;
@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,copy)NSString *datetime;
@property (nonatomic,assign)BOOL isSupport;
@property (nonatomic,assign)BOOL isTread;
@property (nonatomic,strong)NSArray *picture;
@property (nonatomic,strong)UIImage *ablum;

@property (nonatomic,assign)NSInteger status;
@property (nonatomic,copy)NSString *register_reward;
@property (nonatomic,assign)BOOL hasShowAll;                /**< 判断是否折叠,YES 不折叠 NO 折叠*/


@end


@interface BYHomeNewsletterListModel:FetchModel
@property (nonatomic,strong)NSArray <BYHomeNewsletterModel > *content;
@end


NS_ASSUME_NONNULL_END
