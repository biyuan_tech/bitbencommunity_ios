//
//  BYHomeNewsletterController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/5.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeNewsletterController.h"
#import "BYHomeNewsletterRootCell.h"
#import "BYHomeNewsletterModel.h"
#import "ShareRootViewController.h"
#import "CenterShareImgManager.h"
#import "CenterShareImgModel.h"
#import "GWAssetsImgSelectedViewController.h"
#import "NewsLetterShareRootViewController.h"
#import <Photos/Photos.h>
#import <MJRefresh.h>

@interface BYHomeNewsletterController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *newsTableView;
@property (nonatomic,strong)NSMutableArray *newsMutableArr;
@property (nonatomic,strong)UILabel *titleLabel;


@end

@implementation BYHomeNewsletterController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToUpload];
    
}

- (void)autoReload{
    [self.newsTableView.mj_header beginRefreshing];
//    [self reloadData];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"快讯";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.newsMutableArr = [NSMutableArray array];
    
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.newsTableView){
        self.newsTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.newsTableView.dataSource = self;
        self.newsTableView.delegate = self;
        [self.view addSubview:self.newsTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.newsTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToUpload];
    }];
    
    [self.newsTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToUpload];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.newsMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    BYHomeNewsletterRootCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[BYHomeNewsletterRootCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferIndex = indexPath.row;
    BYHomeNewsletterModel *newLetterModel = [self.newsMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferModel = newLetterModel;
    
    __weak typeof(self)weakSelf = self;
    [cellWithRowOne actionClickWithShareManagerWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf shareManagerWithModel:newLetterModel];
    }];
    
    [cellWithRowOne actionClickWithAblumShowBlock:^(BYHomeNewsletterModel * _Nonnull model) {
        if (!weakSelf){
              return ;
          }
        GWAssetsImgSelectedViewController *imgSelected = [[GWAssetsImgSelectedViewController alloc]init];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
        [imgSelected showInView:currentController imgArr:model.picture currentIndex:0 cell:cell];
    }];
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    BYHomeNewsletterModel *newLetterModel = [self.newsMutableArr objectAtIndex:indexPath.row];
    newLetterModel.hasShowAll = !newLetterModel.hasShowAll;
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    BYHomeNewsletterModel *newLetterModel = [self.newsMutableArr objectAtIndex:indexPath.row];
    return [BYHomeNewsletterRootCell calculationCellHeightWithModel:newLetterModel];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(30);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    if (section == 0){
        headerView.backgroundColor = [UIColor hexChangeFloat:@"F2F4F5"];
        UILabel *titleLabel = [GWViewTool createLabelFont:@"12" textColor:@"323232"];
        titleLabel.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(30));
        [headerView addSubview:titleLabel];
        self.titleLabel = titleLabel;
        if (!titleLabel.text.length && self.newsMutableArr.count){
            BYHomeNewsletterModel *newLetterModel = [self.newsMutableArr objectAtIndex:0];
            self.titleLabel.text = [NSDate getKuaixunHeaderString:newLetterModel.create_time / 1000.];
        }
    }
    return headerView;
}


// 界面刷新接口
- (void)reloadData{
    [self sendRequestToUpload];
}

-(void)sendRequestToUpload{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page_number":@(self.newsTableView.currentPage),@"page_size":@"10"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:page_bulletin requestParams:params responseObjectClass:[BYHomeNewsletterListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            BYHomeNewsletterListModel *model = (BYHomeNewsletterListModel *)responseObject;
            
            if (strongSelf.newsTableView.isXiaLa){
                [strongSelf.newsMutableArr removeAllObjects];
            }
            if (model.content.count){
                [strongSelf.newsMutableArr addObjectsFromArray:model.content];
            }
            
            // 如果是第一页的话。更新header
            if (strongSelf.newsTableView.currentPage == 0 && self.newsMutableArr.count){
                BYHomeNewsletterModel *newLetterModel = [self.newsMutableArr objectAtIndex:0];
                strongSelf.titleLabel.text = [NSDate getKuaixunHeaderString:newLetterModel.create_time / 1000.];
            }
            
            [strongSelf.newsTableView reloadData];
            
            if (strongSelf.newsMutableArr.count){
                [strongSelf.newsTableView dismissPrompt];
            } else {
                [strongSelf.newsTableView showPrompt:@"当前没有快讯" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
            [strongSelf.newsTableView stopPullToRefresh];
            [strongSelf.newsTableView stopFinishScrollingRefresh];
        }
    }];
}


-(void)shareManagerWithModel:(BYHomeNewsletterModel *)transferModel{
    CenterShareImgModel *share = [[CenterShareImgModel alloc]init];
    
    NewsLetterShareRootViewController *shareViewController = [[NewsLetterShareRootViewController alloc]init];
    
    shareViewController.showImgBackgroundImage = [share createKuaixunImgWithModelWithModel:transferModel];
    shareViewController.isHasGesture = YES;
    
    
   
    
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
       }
       __strong typeof(weakSelf)strongSelf = weakSelf;
       thirdLoginType shareType = thirdLoginTypeWechat;
       if ([type isEqualToString:@"wechat"]){
           shareType = thirdLoginTypeWechat;
           [ShareSDKManager shareManagerWithType:shareType title:transferModel.title desc:transferModel.content img:shareViewController.showImgBackgroundImage url:nil callBack:NULL];
       } else if ([type isEqualToString:@"friendsCircle"]){
           shareType = thirdLoginTypeWechatFirend;
           [ShareSDKManager shareManagerWithType:shareType title:transferModel.title desc:transferModel.content img:shareViewController.showImgBackgroundImage url:nil callBack:NULL];
       } else if ([type isEqualToString:@"sina"]){
           shareType = thirdLoginTypeWeibo;
           [ShareSDKManager shareManagerWithType:shareType title:transferModel.title desc:transferModel.content img:shareViewController.showImgBackgroundImage url:nil callBack:NULL];
       } else if ([type isEqualToString:@"save"]){
           [strongSelf savePhotoWithAssetWithImg:shareViewController.showImgBackgroundImage block:NULL];
           return;
       }
    }];
    [shareViewController showInView:self.parentViewController];
}

-(void)savePhotoWithAssetWithImg:(UIImage *)img block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
       if (status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted) {
          [StatusBarManager statusBarHidenWithText:@"保存成功"];
       } else {
           [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
               [PHAssetChangeRequest creationRequestForAssetFromImage:img];
           } completionHandler:^(BOOL success, NSError * _Nullable error) {
               dispatch_async(dispatch_get_main_queue(), ^{
                   if (!weakSelf){
                       return ;
                   }
                   if (error){
                       [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"保存失败,请重试:%@",error.localizedDescription]];
                   } else {
                       [StatusBarManager statusBarHidenWithText:@"保存成功"];
                   }
               });
           }];
       }
    }];
}

-(void)sendRequestToGetInfoManager{
   
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.newsTableView){
        NSArray * array = [self.newsTableView visibleCells];
        if (!array.count) return;
        NSIndexPath * indexPath = [self.newsTableView indexPathForCell:array.firstObject];
        BYHomeNewsletterModel *newLetterModel = [self.newsMutableArr objectAtIndex:indexPath.row];
        self.titleLabel.text = [NSDate getKuaixunHeaderString:newLetterModel.create_time / 1000.];
    }
}


#pragma mark - 从消息
-(void)notificationDirectWithShowManager:(NSString *)infoId{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"bulletin_id":infoId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_bulletin requestParams:params responseObjectClass:[BYHomeNewsletterModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            BYHomeNewsletterModel *infoModel = (BYHomeNewsletterModel *)responseObject;
            [strongSelf shareManagerWithModel:infoModel];
        }
    }];
}

@end
