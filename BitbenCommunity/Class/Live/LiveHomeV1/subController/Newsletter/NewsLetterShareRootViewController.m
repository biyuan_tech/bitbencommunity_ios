//
//  NewsLetterShareRootViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/11/8.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "NewsLetterShareRootViewController.h"
#import "UIImage+ImageEffects.h"
#import "NetworkAdapter+Task.h"

#define SheetViewHeight       LCFloat(235 )
@interface NewsLetterShareRootViewController ()
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)UIScrollView *mainScrollView;;
@property (nonatomic,strong)PDImageView *actionBgimgView;
//@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)UIView *shareView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图片
@property (nonatomic,strong)NSArray *shareButtonArray;
@end

static char actionClickWithShareBlockKey;
static char actionClickWithJubaoBlockKey;

@implementation NewsLetterShareRootViewController

-(void)dealloc{
    NSLog(@"qqqqqq 释放");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self arrayWithInit];
    [self createSheetView];                      // 加载Sheetview
}

#pragma mark - 创建sheetView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
        
    }];
    self.mainScrollView.backgroundColor = [UIColor whiteColor];
    self.mainScrollView.frame = kScreenBounds;
    [self.view addSubview:self.mainScrollView];
    
    self.actionBgimgView = [[PDImageView alloc]init];
    self.actionBgimgView.backgroundColor = [UIColor clearColor];
    self.actionBgimgView.image = self.showImgBackgroundImage;
    self.actionBgimgView.frame = self.view.bounds;
    self.actionBgimgView.alpha = 0;
    [self.mainScrollView addSubview:self.actionBgimgView];
    
    // 1. 换算比例
    CGFloat progress = kScreenBounds.size.width * 1.0 / self.showImgBackgroundImage.size.width;
    CGFloat height = progress * self.showImgBackgroundImage.size.height;
    CGFloat rootHeight = height + LCFloat(130);
    if (rootHeight > kScreenBounds.size.height){            // scr 大于屏幕
        self.actionBgimgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, height);
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width, rootHeight);
    } else {
        self.actionBgimgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, height);
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width, kScreenBounds.size.height);
    }
    
    [self createSharetView];
    
    // 创建背景色
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sheetViewDismiss)];
    [self.view addGestureRecognizer:tapGesture];
    
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
}


#pragma mark - 创建view
-(void)createSharetView{
    CGFloat viewHeight = SheetViewHeight;
    if (self.hasJubao){
        viewHeight += LCFloat(51);
    }
    
    CGRect screenRect = [[UIScreen mainScreen]bounds];
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, screenRect.size.height, screenRect.size.width, LCFloat(130))];
    _shareView.clipsToBounds = YES;
    _shareView.backgroundColor = [UIColor hexChangeFloat:@"F2F4F5"];
    _shareView.userInteractionEnabled = YES;
    _shareView.contentMode = UIViewContentModeBottom;
    [self.view addSubview:_shareView];


    // 创建图标
    [self createShareView];
}


-(void)createShareView{
    CGFloat width = LCFloat(70);
    CGFloat margin = (kScreenBounds.size.width - self.shareButtonArray.count * width) / (self.shareButtonArray.count + 1);
    CGFloat height = LCFloat(80);
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i < self.shareButtonArray.count;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        NSString *title = [[self.shareButtonArray objectAtIndex:i] objectAtIndex:0];
        NSString *img = [[self.shareButtonArray objectAtIndex:i]objectAtIndex:1];
        NSString *type = [[self.shareButtonArray objectAtIndex:i]objectAtIndex:2];
        [button setTitle:title forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithCustomerName:@"7F7F7F"] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithCustomerSizeName:@"12"];
        [button setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
        button.frame = CGRectMake(margin + (width + margin) * i, 0, width, height);
        
        [button layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleTop imageTitleSpace:LCFloat(15)];
        
        
        [button buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf dismissFromView:strongSelf.showViewController block:^{
                void(^block)(NSString *type) = objc_getAssociatedObject(strongSelf, &actionClickWithShareBlockKey);
                if (block){
                    block(type);
                }
            }];
        }];
        [_shareView addSubview:button];
    }
    
    UIButton *cancenButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancenButton.backgroundColor = [UIColor clearColor];
    cancenButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
    [cancenButton setTitle:@"取消" forState:UIControlStateNormal];
    [cancenButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cancenButton.frame = CGRectMake(0, LCFloat(80), kScreenBounds.size.width, LCFloat(40));
    [cancenButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sheetViewDismiss];
    }];
    [_shareView addSubview:cancenButton];
}


-(void)arrayWithInit{
    self.shareButtonArray = @[@[@"微信",@"icon_kuaixun_share_wechat",@"wechat"],@[@"朋友圈",@"icon_kuaixun_share_firend",@"friendsCircle"],@[@"微博",@"icon_kuaixun_share_sina",@"sina"],@[@"保存",@"icon_kuaixun_share_down",@"save"]];
}

#pragma mark - actionClick
-(void)sheetViewDismiss{
    __weak typeof(self)weakSelf = self;
    [self dismissFromView:self.showViewController block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSString *type) = objc_getAssociatedObject(strongSelf, &actionClickWithShareBlockKey);
        if (block){
            block(@"");
        }
    }];
}


#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.view.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.actionBgimgView.alpha = 1;
    
    [UIView animateWithDuration:0.2f animations:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.shareView.frame = CGRectMake(0, screenHeight, strongSelf.shareView.bounds.size.width, strongSelf.shareView.bounds.size.height);
        strongSelf.actionBgimgView.alpha = 0;
        strongSelf.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    } completion:^(BOOL finished) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        [strongSelf willMoveToParentViewController:nil];
        [strongSelf.view removeFromSuperview];
        [strongSelf removeFromParentViewController];
        if (block){
            block();
        }
    }];
    
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    __weak NewsLetterShareRootViewController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    
    [UIView animateWithDuration:.5f animations:^{
        if (!weakVC){
            return ;
        }
        __strong typeof(weakVC)strongSelf = weakVC;
        strongSelf.shareView.frame = CGRectMake(strongSelf.shareView.frame.origin.x, screenHeight- strongSelf.shareView.size_height, strongSelf.shareView.size_width, strongSelf.shareView.size_height);
        
        strongSelf.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
        strongSelf.actionBgimgView.alpha = 1;
    } completion:^(BOOL finished) {
        if (!weakVC){
            return ;
        }
        __strong typeof(weakVC)strongSelf = weakVC;
        strongSelf.actionBgimgView.alpha = 1;
    }];
}


- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}

-(void)actionClickWithShareBlock:(void(^)(NSString *type))block{
    objc_setAssociatedObject(self, &actionClickWithShareBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithJubaoBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithJubaoBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}



@end
