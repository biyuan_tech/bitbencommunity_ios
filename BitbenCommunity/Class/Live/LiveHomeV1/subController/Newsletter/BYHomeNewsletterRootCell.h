//
//  BYHomeNewsletterRootCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/11/5.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "BYHomeNewsletterModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeNewsletterRootCell : PDBaseTableViewCell

@property (nonatomic,assign)NSInteger transferIndex;
@property (nonatomic,strong)BYHomeNewsletterModel *transferModel;
@property (nonatomic,strong)PDImageView *ablumImgView;

-(void)actionClickWithShareManagerWithBlock:(void(^)())block;
-(void)actionClickWithAblumShowBlock:(void(^)(BYHomeNewsletterModel *model))block;

+(CGFloat)calculationCellHeightWithModel:(BYHomeNewsletterModel *)model;

@end

NS_ASSUME_NONNULL_END
