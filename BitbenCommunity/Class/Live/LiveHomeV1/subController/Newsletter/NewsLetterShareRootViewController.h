//
//  NewsLetterShareRootViewController.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/11/8.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewsLetterShareRootViewController : AbstractViewController

// 相关属性
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势
@property (nonatomic,strong)UIImage *showImgBackgroundImage;                // 背景图片
@property (nonatomic,assign)BOOL hasJubao;                              // 是否有举报


- (void)showInView:(UIViewController *)viewController;
- (void)dismissFromView:(UIViewController *)viewController block:(void(^)())block;
- (void)hideParentViewControllerTabbar:(UIViewController *)viewController;              // 毛玻璃效果

-(void)actionClickWithShareBlock:(void(^)(NSString *type))block;
-(void)actionClickWithJubaoBlock:(void(^)())block;
@end

NS_ASSUME_NONNULL_END
