//
//  BYHomeTopicModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeTopicModel.h"

@implementation BYHomeTopicModel

+ (NSArray *)getHomeTopicData:(NSArray *)respond{
    if (!respond) {
        return @[];
    }
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    
    NSMutableArray *data = [NSMutableArray array];
    for (NSDictionary *dic in respond) {
        BYHomeTopicModel *model = [BYHomeTopicModel mj_objectWithKeyValues:dic];
        model.cellString = @"BYHomeTopicCell";
        model.cellHeight = 60;
        [data addObject:model];
    }
    return data;
}

@end
