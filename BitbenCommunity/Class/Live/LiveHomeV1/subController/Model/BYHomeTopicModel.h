//
//  BYHomeTopicModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeTopicModel : BYCommonModel

/** 封面图 */
@property (nonatomic ,copy) NSString *img;
/** 话题标签 */
@property (nonatomic ,copy) NSString *content;
/** 标签简介 */
@property (nonatomic ,copy) NSString *remark;

+ (NSArray *)getHomeTopicData:(NSArray *)respond;

@end

NS_ASSUME_NONNULL_END
