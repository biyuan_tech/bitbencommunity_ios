//
//  BYHomeArticleModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeArticleModel.h"
#import "BYPersonHomeHeaderModel.h"
#import <NSAttributedString+YYText.h>
#import <YYLabel.h>


@implementation BYHomeArticleModel

+ (NSArray *)getNoAttentionCellData:(NSArray *)respondArr{
    NSMutableArray *data = [NSMutableArray array];
    BYHomeArticleModel *headerModel = [[BYHomeArticleModel alloc] init];
    headerModel.cellHeight = 119;
    headerModel.cellString = @"BYHomeNoAttentionHeaderCell";
    [data addObject:headerModel];
    for (NSDictionary *dic in respondArr) {
        BYPersonHomeHeaderModel *model = [BYPersonHomeHeaderModel mj_objectWithKeyValues:dic];
        model.cellString = @"BYHomeAttentionUserCell";
        model.cellHeight = 64;
        [data addObject:model];
    }
    return @[@{@"data":data}];
}

+ (NSArray *)getArticleData:(NSDictionary *)dic hiddenAttention:(BOOL)hiddenAttention{
    NSMutableArray *tmpArr = [NSMutableArray array];
    for (int i = 0; i < 3; i ++) {
        BYHomeArticleModel *model = [BYHomeArticleModel getSingleModel:dic row:i];
        model.home_article_type = BY_HOME_ARTICLE_TYPE_NORMAL;
        if (i == 0) {
            model.hiddenAttentionBtn = hiddenAttention;
        }
        [tmpArr addObject:model];
    }
    return tmpArr;
}

+ (BYHomeArticleModel *)getSingleModel:(NSDictionary *)dic row:(NSInteger)row{
    BYHomeArticleModel *model = [BYHomeArticleModel mj_objectWithKeyValues:dic];
    switch (row) {
        case 0: // 头部文章标题类
        {
            if (model.article_type == article_typeNormal) {
                model.subtitle = model.title;
                model.title = @"";
            }
            CGFloat defultTopH = 79;
            CGSize titleMaxSize = CGSizeMake(kCommonScreenWidth - 30, 55);
            CGSize titleSize = CGSizeZero;
            if (model.title.length) {
                titleSize = [model.title getStringSizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]} maxSize:titleMaxSize];
            }
            CGSize contentSize = CGSizeZero;
            if (nullToEmpty(model.subtitle).length) {
                NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.subtitle)];
                messageAttributed.yy_lineSpacing = 8.0f;
                messageAttributed.yy_font = [UIFont systemFontOfSize:15];
                messageAttributed.yy_color = kColorRGBValue(0x323232);
                YYTextContainer *textContainer = [YYTextContainer new];
                textContainer.size = CGSizeMake(kCommonScreenWidth - 60, 105);
                YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
                contentSize = layout.textBoundingSize;
            }
            model.contentSize = contentSize;
            model.showTime = [NSDate getTimeGap:model.create_time/1000];
            model.cellString = @"BYHomeArticleTitleCell";
            if (model.title.length && nullToEmpty(model.subtitle).length) {
                model.cellHeight = defultTopH + titleSize.height + 12 + contentSize.height;
            }else if (!model.title.length && nullToEmpty(model.subtitle).length){
                model.cellHeight = defultTopH + contentSize.height;
            }else if (model.title.length && !nullToEmpty(model.subtitle).length){
                model.cellHeight = defultTopH + titleSize.height;
            }
            
        }
            break;
        case 1: // 中部图片类
        {
            CGFloat imgW = (kCommonScreenWidth - 36)/3.0;
            NSInteger section = ceil(model.picture.count/3.0);
            model.cellString = @"BYHomeArticleImgCell";
            if (model.picture.count == 1) {
                NSString *url = model.picture[0];
                NSArray *imgArr = [url componentsSeparatedByString:@"!!"];
                CGFloat width = 0;
                CGFloat height = 0;
                if (imgArr.count == 3){
                    width = [[imgArr objectAtIndex:1] integerValue];
                    height = [[imgArr objectAtIndex:2] integerValue];
                    model.cellHeight = 113*height/width + 12;
                }else{
                    model.cellHeight = !model.picture.count ? 0 : imgW + (imgW + 3)*(section - 1) + 12;
                }
            }else{
                model.cellHeight = !model.picture.count ? 0 : imgW + (imgW + 3)*(section - 1) + 12;
            }
        }
            break;
        default: // 尾部标签点赞类
        {
            model.cellString = @"BYHomeArticleTagCell";
            model.cellHeight = 90;
        }
            break;
    }
    return model;
}

+ (NSArray *)getAttentionData:(NSArray *)respondArr topData:(NSArray *)topRespondArr pageNum:(NSInteger)pageNum{
    NSMutableArray *data = [NSMutableArray array];
    if (pageNum == 0) {
        if (topRespondArr.count) {
            BYHomeSectionModel *topModel = [[BYHomeSectionModel alloc] init];
            topModel.cellString = @"BYHomeLiveTopCell";
            topModel.cellHeight = 150;
            NSMutableArray *topCellData = [NSMutableArray array];
            for (NSDictionary *dic in topRespondArr) {
                BYCommonLiveModel *model = [BYCommonLiveModel mj_objectWithKeyValues:dic];
                [topCellData addObject:model];
            }
            topModel.cellData = [topCellData copy];
            [data addObject:@{@"data":@[topModel]}];
        }
    }
    for (NSDictionary *dic in respondArr) {
        if ([dic.allKeys indexOfObject:@"article_id"] != NSNotFound) { // 表明为文章
            
            NSArray *tmpArr = [BYHomeArticleModel getArticleData:dic hiddenAttention:NO];
            [data addObject:@{@"data":tmpArr}];
            
        }else{ // 直播数据解析
            NSMutableArray *tmpArr = [NSMutableArray array];
            for (int i = 0; i < 2; i ++) {
                BYCommonLiveModel *model = [BYCommonLiveModel mj_objectWithKeyValues:dic];
                switch (i) {
                    case 0: // 头部直播类信息
                    {
                        CGFloat defultTopH = 79;
                        CGFloat coverImgH = (kCommonScreenWidth - 30)*190.0/345.0;
                        CGSize titleMaxSize = CGSizeMake(kCommonScreenWidth - 30, 55);
                        CGSize titleSize;
                        if (model.live_title.length) {
                            titleSize = [model.live_title getStringSizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]} maxSize:titleMaxSize];
                        }
                        NSDate *date = [NSDate by_dateFromString:model.begin_time dateformatter:K_D_F];
                        NSTimeInterval time = [date timeIntervalSince1970];
                        model.begin_time = [NSDate timeIntervalFromLastTime:time];
                        model.cellString = @"BYHomeLiveCell";
                        model.cellHeight = defultTopH + titleSize.height + coverImgH + 12;
                    }
                        break;
                    default: // 尾部标签点赞类
                    {
                        model.shareMap = [BYCommonShareModel mj_objectWithKeyValues:dic[@"shareMap"]];
                        model.cellString = @"BYHomeArticleTagCell";
                        model.cellHeight = 90;
                    }
                        break;
                }
                [tmpArr addObject:model];
            }
            [data addObject:@{@"data":tmpArr}];
        }
    }
    
    return data;
}

+ (NSArray *)getTopicCellData:(NSArray *)respondArr topData:(NSArray *)topRespondArr pageNum:(NSInteger)pageNum{
    return [BYHomeArticleModel getAttentionData:respondArr topData:topRespondArr pageNum:pageNum];
}

+ (NSArray *)getPersonArticleData:(NSArray *)respondArr{
    if (![respondArr isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (!respondArr.count) {
        return @[];
    }
    NSMutableArray *data = [NSMutableArray array];
    for (NSDictionary *dic in respondArr) {
        NSArray *tmpArr = [BYHomeArticleModel getArticleData:dic hiddenAttention:YES];
        [data addObject:@{@"data":tmpArr}];
    }
    return data;
}

+ (NSArray *)getAnalystTableData:(NSArray *)respondArr{
    if (![respondArr isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (!respondArr.count) {
        return @[];
    }
    NSMutableArray *data = [NSMutableArray array];
    for (NSDictionary *dic in respondArr) {
        NSMutableArray *tmpArr = [NSMutableArray array];
        for (int i = 0; i < 3; i ++) {
            BYHomeArticleModel *model = [BYHomeArticleModel getSingleModel:dic row:i];
            model.home_article_type = BY_HOME_ARTICLE_TYPE_ANALYST;
            [tmpArr addObject:model];
        }
        [data addObject:@{@"data":tmpArr}];
    }
    return data;
}

@end

@implementation BYHomeSectionModel

@end
