//
//  BYHomeArticleModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"
#import "ArticleRootSingleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeArticleModel : BYCommonModel

/** 文章id */
@property (nonatomic ,copy) NSString *article_id;
/** 文章类型 */
@property (nonatomic ,assign)article_type article_type;
/** 头像 */
@property (nonatomic ,copy) NSString *head_img;
/** 用户id */
@property (nonatomic ,copy) NSString *author_id;
/** 关注状态 */
@property (nonatomic ,assign)BOOL isAttention;
/** 昵称 */
@property (nonatomic ,copy)NSString *nickname;
/** 图片组 */
@property (nonatomic ,strong) NSArray *picture;
/** 标题 */
//@property (nonatomic ,copy) NSString *title;
/** 内容 */
@property (nonatomic ,copy) NSString *content;
/** 副标题 */
@property (nonatomic ,copy) NSString *subtitle;
/** 话题组 */
@property (nonatomic ,strong) NSArray *topic_content;
/** 评论数 */
@property (nonatomic ,assign) NSInteger comment_count;
/** 支持数 */
@property (nonatomic ,assign) NSInteger count_support;
/** 支持状态 */
@property (nonatomic ,assign) BOOL isSupport;
/** 观看人次 */
@property (nonatomic ,assign) NSInteger watchNum;
/** 浏览次数 */
@property (nonatomic ,assign) NSInteger count_uv;
/** 创建时间 */
@property (nonatomic ,assign) NSTimeInterval create_time;
/** 显示时间 */
@property (nonatomic ,copy) NSString *showTime;
/** 分享链接 */
@property (nonatomic ,copy) NSString *article_url;
/** NSAttributedString */
@property (nonatomic ,strong) NSAttributedString *attributedString;
/** bbt奖励数 */
@property (nonatomic ,copy) NSString *reward;
/** vip认证 */
@property (nonatomic ,assign) NSInteger cert_badge;
/** 成就组 */
@property (nonatomic ,strong) NSArray <NSString *>*achievement_badge_list;
/** 隐藏关注按钮 */
@property (nonatomic ,assign) BOOL hiddenAttentionBtn;
/** 是否为原创(0 否 1 是) */
@property (nonatomic ,assign) BOOL is_author;
/** contentSize */
@property (nonatomic ,assign) CGSize contentSize;
/** 首页文章类型 */
@property (nonatomic ,assign) BY_HOME_ARTICLE_TYPE home_article_type;
/** 分析师名称 */
@property (nonatomic ,copy) NSString *analyst_remark;
/** 是否收藏 */
@property (nonatomic ,assign) BOOL collection_status;


/** 获取无关注人的用户列表 */
+ (NSArray *)getNoAttentionCellData:(NSArray *)respondArr;
/** 获取关注用户列表数据 */
+ (NSArray *)getAttentionData:(NSArray *)respondArr topData:(NSArray *)topRespondArr pageNum:(NSInteger)pageNum;
/** 获取话题列表数据 */
+ (NSArray *)getTopicCellData:(NSArray *)respondArr topData:(NSArray *)topRespondArr pageNum:(NSInteger)pageNum;
/** 获取他人个人主页文章数据 */
+ (NSArray *)getPersonArticleData:(NSArray *)respondArr;
/** 获取分析师列表数据 */
+ (NSArray *)getAnalystTableData:(NSArray *)respondArr;
@end

@interface BYHomeSectionModel : BYCommonModel

/** data */
@property (nonatomic ,strong) NSArray *cellData;

@end

NS_ASSUME_NONNULL_END
