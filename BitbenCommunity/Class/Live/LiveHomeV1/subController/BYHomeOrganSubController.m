//
//  BYHomeOrganSubController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeOrganSubController.h"
#import "BYTopicDetailController.h"
#import "GWAssetsImgSelectedViewController.h"
#import "BYPersonHomeController.h"
#import "ShareRootViewController.h"

#import "BYReportInCellView.h"
#import "BYHomeArticleTagCell.h"

#import "BYHomeArticleModel.h"
#import "BYPersonHomeHeaderModel.h"

@interface BYHomeOrganSubController ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;
/** 举报 */
@property (nonatomic ,strong) BYReportInCellView *reportView;


@end

@implementation BYHomeOrganSubController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTableView];
    [self reloadData];
}

- (void)autoReload{
    [self.tableView beginRefreshing];
    [self reloadData];
}

- (void)reloadData{
    self.pageNum = 0;
    [self loadRequestProject:self.type];
}

- (void)loadMoreData{
    [self loadRequestProject:self.type];
}

- (void)showReportView:(CGPoint)point model:(id)model{
    NSString *theme_id;
    BY_THEME_TYPE type = BY_THEME_TYPE_LIVE;
    
    BYHomeArticleModel *data = (BYHomeArticleModel *)model;
    theme_id = data.article_id;
    type = BY_THEME_TYPE_ARTICLE;
    
    self.reportView.theme_id = theme_id;
    self.reportView.theme_type = type;
    [self.reportView showAnimationWithPoint:point];
}

// 跳转话题详情
- (void)pushTopicDetailController:(NSString *)topic{
    if (!topic.length) return;
    BYCommonViewController *currentController = (BYCommonViewController *)[BYTabbarViewController sharedController].currentController;
    BYTopicDetailController *topicDetailController = [[BYTopicDetailController alloc] init];
    topicDetailController.topic = topic;
    [currentController authorizePush:topicDetailController animation:YES];
}

#pragma mark - action

- (void)shareAction:(id)model{
    NSString *shareTitle;
    NSString *shareContent;
    NSString *shareImgUrl;
    NSString *shareUrl;
    shareType sharetype = shareTypeLive;
    BYHomeArticleModel *data = (BYHomeArticleModel *)model;
    NSDictionary *params = @{@"首页文章分享":(data.article_id.length?data.article_id:@"找不到文章")};
    [MTAManager event:MTATypeShortShare params:params];
    id smartImgUrl;
    if (data.picture.count){
        smartImgUrl = [data.picture firstObject];
    } else {
        smartImgUrl = data.head_img;
    }
    
    id mainImgUrl;
    if ([smartImgUrl isKindOfClass:[NSString class]]){
        NSString *urlStr = (NSString *)smartImgUrl;
        NSString *baseURL = [PDImageView getUploadBucket:urlStr];
        NSString *nUrl = [[NSString stringWithFormat:@"%@%@",baseURL,urlStr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        mainImgUrl = nUrl;
    } else if ([smartImgUrl isKindOfClass:[UIImage class]]){
        mainImgUrl = smartImgUrl;
    }
    if (data.article_type == article_typeWeb){
        shareTitle = data.title;
        if (data.subtitle.length && ![data.subtitle isEqualToString:@"null"]){
            shareContent = data.subtitle;
        } else {
            shareContent = @"最新最热区块链内容，尽在币本社区";
        }
    } else if (data.article_type == article_typeNormal){
        shareTitle = data.title.length?data.title:data.content;
        shareContent = @"最新最热区块链内容，尽在币本社区";
    }
    shareUrl = data.article_url;
    shareImgUrl = mainImgUrl;
    sharetype = shareTypeArticle;
    
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.transferCopyUrl = shareUrl;
    shareViewController.collection_status = data.collection_status;
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        [ShareSDKManager shareManagerWithType:shareType title:shareTitle desc:shareContent img:shareImgUrl url:shareUrl callBack:NULL];
        [ShareSDKManager shareSuccessBack:sharetype block:NULL];
    }];
    
    [shareViewController actionClickWithCollectionBlock:^{
        data.collection_status = !data.collection_status;
    }];
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    [shareViewController showInView:currentController];
}

// 顶的操作
- (void)supportAction:(id)model indexPath:(NSIndexPath *)indexPath suc:(void(^)(void))suc{
    BYHomeArticleModel *data = (BYHomeArticleModel *)model;
    if (data.isSupport) {
        showToastView(@"你已经顶过该条内容", self.view);
        return;
    }
    [self loadRequestSupport:data.article_id user_id:data.author_id suc:^{
        data.isSupport = YES;
        data.count_support ++;
        if (suc) suc();
    }];
}

// 关注
- (void)attentionAction:(id)model{
    BYHomeArticleModel *data = (BYHomeArticleModel *)model;
    @weakify(self);
    [self loadRequestAttentionUser:data.author_id isAttention:!data.isAttention suc:^(BOOL attention) {
        @strongify(self);
        data.isAttention = attention;
        [self.tableView reloadData];
    }];
}

#pragma mark - BYCommonTableViewDelegate
- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [UIView by_init];
    [sectionView setBackgroundColor:[UIColor clearColor]];
    return sectionView;
}

- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForHeaderInSection:(NSInteger)section{
    return 8.0f;
}

- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = tableView.tableData[indexPath.section];
    id model = dic.allValues[0][indexPath.row];
    BYHomeArticleModel *data = (BYHomeArticleModel *)model;
    [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_ARTICLE
                                            liveType:0
                                             themeId:data.article_id];
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    BYCommonViewController *currentController = (BYCommonViewController *)[BYTabbarViewController sharedController].currentController;
    if ([actionName isEqualToString:@"shareAction"]) { // 分享
        NSDictionary *dic = tableView.tableData[indexPath.section];
        id model = dic.allValues[0][indexPath.row];
        [self shareAction:model];
    }else if ([actionName isEqualToString:@"likeAction"]) { // 顶
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYHomeArticleTagCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        id model = dic.allValues[0][indexPath.row];
        @weakify(self);
        [currentController authorizeWithCompletionHandler:^(BOOL successed) {
            @strongify(self);
            if (!successed) return ;
            [self supportAction:model indexPath:indexPath suc:^{
                [cell reloadAttentionAnimation];
            }];
        }];
    }else if ([actionName isEqualToString:@"attentionAction"]) { // 关注
        NSDictionary *dic = tableView.tableData[indexPath.section];
        id model = dic.allValues[0][indexPath.row];
        @weakify(self);
        [currentController authorizeWithCompletionHandler:^(BOOL successed) {
            @strongify(self);
            if (!successed) return ;
            [self attentionAction:model];
        }];
    }else if ([actionName isEqualToString:@"imgAction"]) { // 图片点击放大
        NSInteger index = [param[@"index"] integerValue];
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYHomeArticleModel *model = dic.allValues[0][indexPath.row];
        UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
        GWAssetsImgSelectedViewController *imgSelected = [[GWAssetsImgSelectedViewController alloc]init];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [imgSelected showInView:currentController imgArr:model.picture currentIndex:index cell:cell];
    }else if ([actionName isEqualToString:@"selectTagAction"]){ // 点击话题tag
        NSString *tag = param[@"tag"];
        [self pushTopicDetailController:tag];
    }else if ([actionName isEqualToString:@"reportAction"]) { // 举报
        NSDictionary *dic = tableView.tableData[indexPath.section];
        id model = dic.allValues[0][indexPath.row];
        NSValue *value = param[@"point"];
        CGPoint point = [value CGPointValue];
        [self showReportView:point model:model];
    }
}

#pragma mark - request
- (void)loadRequestProject:(BY_HOME_ARTICLE_TYPE)type{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestPageIndexArticle:self.type pageNum:self.pageNum successBlock:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        if (![object count]) {
            return ;
        }
        if (self.pageNum == 0) {
            self.tableView.tableData = object;
        }else{
            NSMutableArray *data = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [data addObjectsFromArray:object];
            self.tableView.tableData = data;
        }
        self.pageNum ++;
        
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"获取数据失败！", self.view);
    }];
}

// 关注
- (void)loadRequestAttentionUser:(NSString *)user_id isAttention:(BOOL)isAttention suc:(void(^)(BOOL attention))suc{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:user_id isAttention:isAttention successBlock:^(id object) {
        @strongify(self);
        showToastView(@"关注成功", self.view);
        if (suc) {
            suc(isAttention);
        }
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"关注失败", self.view);
    }];
}

// 顶
- (void)loadRequestSupport:(NSString *)theme_id user_id:(NSString *)user_id suc:(void(^)(void))suc{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestLiveGuestOpearType:BY_GUEST_OPERA_TYPE_UP theme_id:theme_id theme_type:BY_THEME_TYPE_ARTICLE receiver_user_id:user_id successBlock:^(id object) {
        if (suc) suc();
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"顶操作失败", self.view);
    }];
}

#pragma mark - configUI

- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    [self.tableView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.tableView addHeaderRefreshTarget:self action:@selector(reloadData)];
    [self.tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}


@end
