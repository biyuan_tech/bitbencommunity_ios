//
//  BYHomeVideoController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/31.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeVideoController.h"
#import "BYVideoHomeSubController.h"

static NSInteger baseTag = 865;
@interface BYHomeVideoController ()

/** 热门 */
@property (nonatomic ,strong) BYVideoHomeSubController *hotVideoController;
/** 新鲜 */
@property (nonatomic ,strong) BYVideoHomeSubController *freshVideoController;
/** scrollView */
@property (nonatomic ,strong) UIScrollView *scrollView;
/** headerView */
@property (nonatomic ,strong) UIView *headerView;

@end

@implementation BYHomeVideoController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addHeaderView];
    [self addScrollView];
    
}

- (void)autoReload{
    if (self.scrollView.contentOffset.x >= kCommonScreenWidth) {
        [self.freshVideoController autoReload];
    }else{
        [self.hotVideoController autoReload];
    }
}

- (void)reloadData{
    [self.hotVideoController reloadData];
    [self.freshVideoController reloadData];
}

#pragma mark - action
- (void)btnAction:(UIButton *)sender{
    NSInteger index = sender.tag - baseTag;
    for (int i = 0; i < 2; i ++) {
        UIButton *btn = [self.headerView viewWithTag:baseTag + i];
        btn.selected = btn == sender ? YES : NO;
        UIColor *bgColor = btn == sender ? kColorRGBValue(0xea6438) : [UIColor whiteColor];
        [btn setBackgroundColor:bgColor];
    }
    [self.scrollView setContentOffset:CGPointMake(kCommonScreenWidth*index, 0) animated:YES];
}

#pragma mark - configUI

- (void)addHeaderView{
    UIView *headerView = [UIView by_init];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:headerView];
    self.headerView = headerView;
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(48);
    }];
    
    NSArray *titles = @[@"热门",@"新鲜"];
    for (int i = 0; i < 2; i ++) {
        UIButton *btn = [UIButton by_buttonWithCustomType];
        [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBy_attributedTitle:@{@"title":titles[i],
                                     NSFontAttributeName:[UIFont systemFontOfSize:13],
                                     NSForegroundColorAttributeName:kColorRGBValue(0x8f8f8f)
                                     } forState:UIControlStateNormal];
        [btn setBy_attributedTitle:@{@"title":titles[i],
                                     NSFontAttributeName:[UIFont systemFontOfSize:13],
                                     NSForegroundColorAttributeName:[UIColor whiteColor]
                                     } forState:UIControlStateSelected];
        btn.selected = i == 0 ? YES : NO;
        [btn setBackgroundColor:i == 0 ? kColorRGBValue(0xea6438) : [UIColor whiteColor]];
        btn.tag = baseTag + i;
        [headerView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(22);
            make.top.mas_equalTo(18);
            make.left.mas_equalTo(15 + (45 + 10)*i);
            make.width.mas_equalTo(45);
        }];
    }
    
    UIView *horizontalView = [UIView by_init];
    horizontalView.backgroundColor = kColorRGBValue(0xe7e7ea);
    [headerView addSubview:horizontalView];
    [horizontalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(8);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)addScrollView{
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.scrollEnabled = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    [self.scrollView setContentSize:CGSizeMake(kCommonScreenWidth*2, 0)];
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(48, 0, 0, 0));
    }];
    
    [self addSubController];
}

- (void)addSubController{
    //    UIViewController *currentController = [BYTabbarViewController sharedController].currentController;
    
    self.hotVideoController = [[BYVideoHomeSubController alloc] init];
    self.hotVideoController.sort_field = @"score";
    [self addChildViewController:self.hotVideoController];
    [self.scrollView addSubview:self.hotVideoController.view];
    [self.hotVideoController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(0);
        make.height.mas_equalTo(self.scrollView);
        make.width.mas_equalTo(kCommonScreenWidth);
    }];
    
    self.freshVideoController = [[BYVideoHomeSubController alloc] init];
    self.freshVideoController.sort_field = @"datetime";
    [self addChildViewController:self.freshVideoController];
    [self.scrollView addSubview:self.freshVideoController.view];
    [self.freshVideoController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(self.scrollView);
        make.left.mas_equalTo(kCommonScreenWidth);
        make.width.mas_equalTo(kCommonScreenWidth);
    }];
    
    
}

@end
