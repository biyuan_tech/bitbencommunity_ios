//
//  BYHomeSearchController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeSearchController.h"
#import "BYHomeSearchViewModel.h"

@interface BYHomeSearchController ()

@end

@implementation BYHomeSearchController

- (Class)getViewModelClass{
    return [BYHomeSearchViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}


@end
