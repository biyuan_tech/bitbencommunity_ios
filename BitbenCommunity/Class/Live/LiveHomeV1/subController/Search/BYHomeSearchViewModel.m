//
//  BYHomeSearchViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeSearchViewModel.h"
#import "BYHomeSearchView.h"
#import "BYHomeSearchHistoryView.h"
#import "BYHomeSearchResultView.h"

#import "BYStorageUtil.h"


@interface BYHomeSearchViewModel ()

/** searchView */
@property (nonatomic ,strong) BYHomeSearchView *searchBarView;
/** 搜索记录 */
@property (nonatomic ,strong) BYHomeSearchHistoryView *historyView;
/** 搜索结果 */
@property (nonatomic ,strong) BYHomeSearchResultView *resultView;


@end

@implementation BYHomeSearchViewModel

- (void)setContentView{
    [self addSearchBarView];
    [self addHistoryView];
    [self addResultView];
}

// 写入搜索缓存
- (void)writeSearchData:(NSString *)title{
    if (!title.length) return;
    NSArray *searchCacheData = [BYStorageUtil readFileToPlist:kSearchCacheKey];
    NSArray *cacheData = [BYCommonModel mj_objectArrayWithKeyValuesArray:searchCacheData];
    NSMutableArray *data = [NSMutableArray arrayWithArray:cacheData];
    for (BYCommonModel *tmpModel in cacheData) {
        if ([tmpModel.title isEqualToString:title]) {
            [data removeObject:tmpModel];
        }
    }
    BYCommonModel *model = [[BYCommonModel alloc] init];
    model.cellString = @"BYSearchHistoryCell";
    model.cellHeight = 46;
    model.title = title;
    [data insertObject:model atIndex:0];
    if (data.count > 10) {
        [data removeLastObject];
    }
    NSArray *saveArr = [BYCommonModel mj_keyValuesArrayWithObjectArray:data];
    [BYStorageUtil writeFileToPlist:saveArr key:kSearchCacheKey];
    [self.historyView reloadCacheData];
}

#pragma mark - request
- (void)loadRequestGetSearchResult:(NSString *)title{
    [self writeSearchData:title];
    [self.resultView loadSearchKey:title];
    self.resultView.hidden = NO;
}

#pragma mark - configUI

- (void)addSearchBarView{
    self.searchBarView = [[BYHomeSearchView alloc] init];
    @weakify(self);
    self.searchBarView.reloadCacheDataHandle = ^{
        @strongify(self);
        [self.historyView reloadCacheData];
    };
    self.searchBarView.loadSearchTitleHandle = ^(NSString * _Nonnull title) {
        @strongify(self);
        [self loadRequestGetSearchResult:title];
    };
    self.searchBarView.didClearSearchHandle = ^{
        @strongify(self);
        self.resultView.hidden = YES;
    };
    [S_V_VIEW addSubview:self.searchBarView];
    [self.searchBarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(35);
        make.top.mas_equalTo(kSafe_Mas_Top(25));
    }];
}

- (void)addHistoryView{
    self.historyView = [[BYHomeSearchHistoryView alloc] init];
    @weakify(self);
    self.historyView.loadSearchTitleHandle = ^(NSString * _Nonnull title) {
        @strongify(self);
        self.searchBarView.textField.text = nullToEmpty(title);
        [self loadRequestGetSearchResult:title];
    };
    [S_V_VIEW addSubview:self.historyView];
    [self.historyView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.right.bottom.mas_equalTo(0);
        make.top.mas_equalTo(self.searchBarView.mas_bottom).mas_offset(0);
    }];
}

- (void)addResultView{
    self.resultView = [[BYHomeSearchResultView alloc] init];
    [S_V_VIEW addSubview:self.resultView];
    self.resultView.hidden = YES;
    @weakify(self);
    [self.resultView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.right.bottom.mas_equalTo(0);
        make.top.mas_equalTo(self.searchBarView.mas_bottom).mas_offset(0);
    }];
}


@end
