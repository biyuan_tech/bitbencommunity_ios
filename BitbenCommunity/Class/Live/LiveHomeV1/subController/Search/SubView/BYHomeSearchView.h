//
//  BYHomeSearchView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYHomeSearchTextField.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeSearchView : UIView

/** textField */
@property (nonatomic ,strong) BYHomeSearchTextField *textField;
/** 更新搜索记录 */
@property (nonatomic ,copy) void (^reloadCacheDataHandle)(void);
/** 搜索 */
@property (nonatomic ,copy) void (^loadSearchTitleHandle)(NSString *title);
/** 清除搜索内容 */
@property (nonatomic ,copy) void (^didClearSearchHandle)(void);


@end

NS_ASSUME_NONNULL_END
