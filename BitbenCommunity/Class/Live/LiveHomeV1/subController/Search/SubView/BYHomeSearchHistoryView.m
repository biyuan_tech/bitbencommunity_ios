//
//  BYHomeSearchHistoryView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeSearchHistoryView.h"
#import "BYStorageUtil.h"
#import "BYHomeSearchHotView.h"

@interface BYHomeSearchHistoryView ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** 热门搜索 */
@property (nonatomic ,strong) BYHomeSearchHotView *searchHotView;

@end

@implementation BYHomeSearchHistoryView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configUI];
        [self loadCacheData];
        [self loadRequestGetHotSearch];
    }
    return self;
}

// 读取搜索记录
- (void)loadCacheData{
    NSArray *searchCacheData = [BYStorageUtil readFileToPlist:kSearchCacheKey];
    NSArray *cacheData = [BYCommonModel mj_objectArrayWithKeyValuesArray:searchCacheData];
    [_searchHotView setTopViewHidden:cacheData.count ? NO : YES];
    if (!cacheData) return;
    NSMutableArray *data = [NSMutableArray arrayWithArray:cacheData];
    BYCommonModel *model = [[BYCommonModel alloc] init];
    model.cellString = @"BYSearchHistoryDelCell";
    model.cellHeight = 46;
    [data addObject:model];
    self.tableView.tableData = data;
}

- (void)reloadCacheData{
    [self loadCacheData];
}

#pragma mark - request
- (void)loadRequestGetHotSearch{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestHotSearch:^(id object) {
        @strongify(self);
        if (![object count]) return ;
        self.tableView.tableFooterView = self.searchHotView;
        [self.searchHotView loadData:object];
        if (!self.tableView.tableData.count) {
            [self.searchHotView setTopViewHidden:YES];
        }
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BYCommonModel *model = tableView.tableData[indexPath.row];
    if (self.loadSearchTitleHandle) {
        self.loadSearchTitleHandle(model.title);
    }
}
- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"deleteAction"]) { // 移除单条记录
        NSMutableArray *array = [NSMutableArray arrayWithArray:tableView.tableData];
        [array removeObjectAtIndex:indexPath.row];
        tableView.tableData = array.count == 1 ? @[] : [array copy];
        [array removeLastObject];
        NSArray *cacheData = [BYCommonModel mj_keyValuesArrayWithObjectArray:array];
        [BYStorageUtil writeFileToPlist:cacheData key:kSearchCacheKey];
        if (!array.count) {
            [self.searchHotView setTopViewHidden:YES];
        }
    }else if ([actionName isEqualToString:@"deleteAllAction"]){ // 移除所有记录
        UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
        [currentController.view endEditing:YES];
        BYAlertView *alertView = [BYAlertView initWithTitle:@"是否清空所有记录？" message:nil inView:currentController.view alertViewStyle:BYAlertViewStyleAlert];
        [alertView setMaskViewStyle:BYAlertViewMaskStyleBlack];
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"确定" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kColorRGBValue(0xea6441)}];
        @weakify(self);
        [alertView addActionCustomAttributedString:attributedString handle:^{
            @strongify(self);
            tableView.tableData = @[];
            [BYStorageUtil removeFileToPlist:kSearchCacheKey];
            [self.searchHotView setTopViewHidden:YES];
        }];
        [alertView showAnimation];
    }
}

#pragma mark - configUI

- (void)configUI{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (BYHomeSearchHotView *)searchHotView{
    if (!_searchHotView) {
        _searchHotView = [[BYHomeSearchHotView alloc] init];
        _searchHotView.frame = CGRectMake(0, 0, kCommonScreenWidth, 250);
        @weakify(self);
        _searchHotView.didSelectTagHandle = ^(NSString * _Nonnull title) {
            @strongify(self);
            if (self.loadSearchTitleHandle) {
                self.loadSearchTitleHandle(title);
            }
        };
    }
    return _searchHotView;
}


@end
