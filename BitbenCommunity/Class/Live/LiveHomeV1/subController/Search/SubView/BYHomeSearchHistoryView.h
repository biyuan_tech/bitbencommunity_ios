//
//  BYHomeSearchHistoryView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeSearchHistoryView : UIView

/** 搜索 */
@property (nonatomic ,copy) void (^loadSearchTitleHandle)(NSString *title);


- (void)reloadCacheData;

@end

NS_ASSUME_NONNULL_END
