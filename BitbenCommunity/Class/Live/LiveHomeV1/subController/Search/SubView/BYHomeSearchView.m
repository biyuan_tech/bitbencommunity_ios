//
//  BYHomeSearchView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeSearchView.h"
#import "BYHomeSearchTextField.h"
#import "BYStorageUtil.h"

@interface BYHomeSearchView ()<BYHomeSearchTextFieldDelegate,UITextFieldDelegate>

/** cancelBtn */
@property (nonatomic ,strong) UIButton *cancelBtn;
/** contentView */
@property (nonatomic ,strong) UIView *contentView;


@end

@implementation BYHomeSearchView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}


#pragma mark - action

- (void)cancelBtnAction{
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    [currentController.navigationController popViewControllerAnimated:YES];
}

#pragma mark - BYHomeSearchTextFieldDelegate
- (BOOL)textFieldDidClear:(UITextField *)textField{
    if (self.didClearSearchHandle) {
        self.didClearSearchHandle();
    }
    return YES;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (!textField.text.length) return YES;
    if (self.loadSearchTitleHandle) {
        self.loadSearchTitleHandle(textField.text);
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *rangString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (rangString.length == 0) {
        if (self.didClearSearchHandle) {
            self.didClearSearchHandle();
        }
    }
    if (rangString.length > 60) { // 最大字数限制60
        return NO;
    }
    return YES;
}

#pragma mark - configUI
- (void)setContentView{
    UIView *contentView = [[UIView alloc] init];
    contentView.layer.cornerRadius = 4.0f;
    [contentView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self addSubview:contentView];
    self.contentView = contentView;
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.bottom.mas_equalTo(0);
        make.right.mas_equalTo(-55);
    }];
    
    UIImageView *searchImgView = [[UIImageView alloc] init];
    [searchImgView by_setImageName:@"livehome_search"];
    [self.contentView addSubview:searchImgView];
    [searchImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(13);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(searchImgView.image.size.width);
        make.height.mas_equalTo(searchImgView.image.size.height);
    }];
    
    [self addTextFieldView];
    
    self.cancelBtn = [UIButton by_buttonWithCustomType];
    self.cancelBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [self.cancelBtn setTitleColor:kColorRGBValue(0x323232) forState:UIControlStateNormal];
    [self.cancelBtn addTarget:self action:@selector(cancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.cancelBtn];
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(55);
        make.top.bottom.mas_equalTo(0);
    }];
}

- (void)addTextFieldView{
    self.textField = [[BYHomeSearchTextField alloc] init];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"搜索视频/用户/观点" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0xb6b6b7)}];
    _textField.attributedPlaceholder = attributedString;
    _textField.layer.cornerRadius = 4;
    _textField.layer.masksToBounds = YES;
    _textField.returnKeyType = UIReturnKeySearch;
    _textField.backgroundColor= kColorRGBValue(0xf2f4f5);
    _textField.base_delegate = self;
    _textField.delegate = self;
    [self.contentView addSubview:self.textField];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(40);
        make.top.bottom.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
}

@end
