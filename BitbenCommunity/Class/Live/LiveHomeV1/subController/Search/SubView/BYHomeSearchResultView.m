//
//  BYHomeSearchResultView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeSearchResultView.h"
#import "BYLiveHomeSegmentViewV1.h"

#import "BYSearchHisModel.h"
#import "BYHomeArticleModel.h"
#import "BYRecommendUserModel.h"
#import "BYPersonHomeHeaderModel.h"

@interface BYCommonTableView (search)

/** 重新请求数据 */
@property (nonatomic ,assign) BOOL needReload;
/** 页码 */
@property (nonatomic ,assign) NSInteger pageNum;


@end

static NSInteger kBaseTag = 0x966;
@interface BYHomeSearchResultView ()<BYLiveHomeSegmentViewDelegateV1,BYCommonTableViewDelegate>

/** segment */
@property (nonatomic ,strong) BYLiveHomeSegmentViewV1 *segmentView;
/** 搜索内容 */
@property (nonatomic ,copy) NSString *key;

@end

@implementation BYHomeSearchResultView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setContentView];
    }
    return self;
}

- (void)reloadData:(BY_SEARCH_TYPE)type{
    BYCommonTableView *tableView = [self.segmentView viewWithTag:kBaseTag + type];
    tableView.pageNum = 0;
    [self loadRequestSearchType:type];
}

- (void)loadMoreData:(BY_SEARCH_TYPE)type{
    [self loadRequestSearchType:type];
}

- (void)loadSearchKey:(NSString *)key{
    if (!key.length) return;
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    [currentController.view endEditing:YES];
    self.key = key;
    for (int i = 0; i < 5; i ++) {
        BYCommonTableView *tableView = [self.segmentView viewWithTag:kBaseTag + i];
        tableView.tableData = @[];
        tableView.pageNum = 0;
        tableView.needReload = YES;
    }
    [self.segmentView selectIndex:0];
    [self by_segmentViewDidScrollToIndex:0];
}

#pragma mark - request
// 关注
- (void)loadRequestAttentionUser:(NSString *)user_id isAttention:(BOOL)isAttention suc:(void(^)(BOOL attention))suc{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:user_id isAttention:isAttention successBlock:^(id object) {
        if (suc) {
            suc(isAttention);
        }
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"关注失败", self);
    }];
}

- (void)loadRequestSearchType:(BY_SEARCH_TYPE)type{
    if (!self.key.length) return;
    BYCommonTableView *tableView = [self.segmentView viewWithTag:kBaseTag + type];
    [[BYLiveHomeRequest alloc] loadRequestSearchType:type key:self.key pageNum:tableView.pageNum successBlock:^(id object) {
        tableView.needReload = NO;
        [tableView endRefreshing];
        [tableView removeEmptyView];
        if (![object count] && tableView.pageNum == 0) {
            tableView.tableData = @[];
            [tableView addEmptyView:@"暂无内容"];
            return ;
        }
        if (![object count]) return;
        if (type == BY_SEARCH_TYPE_ALL) {
            tableView.tableData = object;
            return;
        }
        if (tableView.pageNum == 0) {
            tableView.tableData = @[@{@"data":object}];
        }else{
            NSDictionary *dic = tableView.tableData[0];
            NSMutableArray *array = [NSMutableArray arrayWithArray:dic.allValues[0]];
            [array addObjectsFromArray:object];
            tableView.tableData = @[@{@"data":array}];
        }
        tableView.pageNum ++;
    } faileBlock:^(NSError *error) {
        [tableView endRefreshing];
    }];
}

#pragma mark - BYLiveHomeSegmentViewDelegateV1
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    BYCommonTableView *tableView = [[BYCommonTableView alloc] init];
    tableView.backgroundColor = [UIColor whiteColor];
    tableView.group_delegate = self;
    tableView.needReload = YES;
    tableView.pageNum = 0;
    tableView.tag = kBaseTag + index;
    @weakify(self);
    [tableView addHeaderRefreshHandle:^{
        @strongify(self);
        [self reloadData:index];
    }];
    if (index > 0) {
        [tableView addFooterRefreshHandle:^{
            @strongify(self);
            [self loadMoreData:index];
        }];
    }
    
    UIView *headerView = [UIView by_init];
    [headerView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    headerView.frame = CGRectMake(0, 0, kCommonScreenWidth, 8);
    tableView.tableHeaderView = headerView;
    
    return tableView;
}

- (void)by_segmentViewDidScrollToIndex:(NSInteger)index{
    BYCommonTableView *tableView = [self.segmentView viewWithTag:kBaseTag + index];
    if (tableView && tableView.needReload) {
        [self loadRequestSearchType:index];
    }
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) return;
    NSDictionary *dic = tableView.tableData[indexPath.section];
    id data = dic.allValues[0][indexPath.row];
    if ([data isKindOfClass:[BYSearchHisModel class]]) {
        BYSearchHisModel *model = (BYSearchHisModel *)data;
        [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_LIVE liveType:model.live_type themeId:model.live_record_id];
    }
    else if ([data isKindOfClass:[BYHomeArticleModel class]]){
        BYHomeArticleModel *model = (BYHomeArticleModel *)data;
        [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_ARTICLE liveType:0 themeId:model.article_id];
    }
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"user_AttentionAction"]) {
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYPersonHomeHeaderModel *model = dic.allValues[0][indexPath.row];
        [self loadRequestAttentionUser:model.account_id isAttention:!model.isAttention suc:^(BOOL attention) {
            model.isAttention = attention;
            [tableView reloadData];
        }];
    }
    else if ([actionName isEqualToString:@"moreAction"]){
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYCommonModel *model = dic.allValues[0][indexPath.row];
        [self.segmentView selectIndex:model.index];
    }
}

- (void)setContentView{
    self.segmentView = [[BYLiveHomeSegmentViewV1 alloc] initWithTitles:@"推荐",@"直播",@"视频",@"观点",@"用户", nil];
    self.segmentView.delegate = self;
    [self addSubview:self.segmentView];
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(10, 0, 0, 0));
    }];
}


@end

static const char pageNumKey;
static const char needReloadKey;
@implementation BYCommonTableView (search)

- (void)setPageNum:(NSInteger)pageNum{
    objc_setAssociatedObject(self, &pageNumKey, @(pageNum), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSInteger)pageNum{
    NSNumber *number = objc_getAssociatedObject(self, &pageNumKey);
    return [number integerValue];
}

- (void)setNeedReload:(BOOL)needReload{
    objc_setAssociatedObject(self, &needReloadKey, @(needReload), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)needReload{
    NSNumber *reload = objc_getAssociatedObject(self, &needReloadKey);
    return [reload boolValue];
}

@end


