//
//  BYHomeSearchHotView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeSearchHotView : UIView

/** 热门搜索点击回调 */
@property (nonatomic ,copy) void (^didSelectTagHandle)(NSString *title);


- (void)loadData:(NSArray *)data;

- (void)setTopViewHidden:(BOOL)hidden;

@end

NS_ASSUME_NONNULL_END
