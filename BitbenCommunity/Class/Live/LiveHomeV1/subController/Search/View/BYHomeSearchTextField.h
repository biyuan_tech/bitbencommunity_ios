//
//  BYHomeSearchTextField.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol BYHomeSearchTextFieldDelegate <NSObject>

- (BOOL)textFieldDidClear:(UITextField *)textField;

@end


@interface BYHomeSearchTextField : UITextField


@property (nonatomic ,weak) id<BYHomeSearchTextFieldDelegate>base_delegate;

@end

NS_ASSUME_NONNULL_END
