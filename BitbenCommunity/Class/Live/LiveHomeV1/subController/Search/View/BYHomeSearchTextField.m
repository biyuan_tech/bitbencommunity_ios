
//
//  BYHomeSearchTextField.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeSearchTextField.h"

#define kClearBtnTag 1000
@interface BYHomeSearchTextField ()

@property (nonatomic ,strong) UIView *rightBtnView;
@property (nonatomic ,strong) UIButton *clearBtn;

@end

@implementation BYHomeSearchTextField

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addTarget:self action:@selector(textFieldDidChangeNSNOtific:) forControlEvents:UIControlEventEditingChanged];
        self.tintColor = [UIColor blackColor];
        self.textColor = kColorRGBValue(0x323232);
        self.font = [UIFont systemFontOfSize:14];
        self.rightView = self.rightBtnView;
        self.rightViewMode = UITextFieldViewModeWhileEditing;

    }
    return self;
}
         
 - (void)layoutSubviews{
     [super layoutSubviews];
     _rightBtnView.frame = CGRectMake(CGRectGetWidth(self.frame)- _clearBtn.imageView.image.size.width - 15, 0, _clearBtn.imageView.image.size.width + 15, self.frame.size.height);
     _clearBtn.frame = CGRectMake(0, 0, _clearBtn.imageView.image.size.width + 15, CGRectGetHeight(self.frame));
 }
         
#pragma mark - custonMethod
 
 - (void)clearTextField{
     [_clearBtn setImage:[UIImage imageNamed:@"livehome_search_cancel"] forState:UIControlStateNormal];
     if ([self.base_delegate respondsToSelector:@selector(textFieldDidClear:)]) {
         self.text = [self.base_delegate textFieldDidClear:self] ? @"" : self.text;
         return;
     }
     self.text = @"";
 }
 
 - (void)textFieldDidChangeNSNOtific:(id)sender{
     NSString *imageName = self.text.length ? @"livehome_search_cancel" : @"livehome_search_cancel";
     [_clearBtn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
 }
 
#pragma mark - initMethod
 
 - (UIView *)rightBtnView{
     if (!_rightBtnView) {
         _rightBtnView = [[UIView alloc] init];
         self.clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
         [_clearBtn setImage:[UIImage imageNamed:@"livehome_search_cancel"] forState:UIControlStateNormal];
         _clearBtn.frame = CGRectMake(0, 0, 40, CGRectGetHeight(self.frame));
         [_clearBtn addTarget:self action:@selector(clearTextField) forControlEvents:UIControlEventTouchUpInside];
         _clearBtn.tag = kClearBtnTag;
         [_rightBtnView addSubview:_clearBtn];
         _rightBtnView.frame = CGRectMake(0, 0, CGRectGetWidth(_clearBtn.frame), CGRectGetHeight(self.frame));
     }
     
     return _rightBtnView;
 }

@end
