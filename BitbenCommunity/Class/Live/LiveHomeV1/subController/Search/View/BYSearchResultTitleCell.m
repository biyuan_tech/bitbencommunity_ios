//
//  BYSearchResultTitleCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYSearchResultTitleCell.h"
#import "BYSearchHisModel.h"

@interface BYSearchResultTitleCell ()

/** titleLab */
@property (nonatomic ,strong) UILabel *titleLab;
/** rightView */
@property (nonatomic ,strong) UIView *rightView;

@end

@implementation BYSearchResultTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYCommonModel *model = dic.allValues[0][indexPath.row];
    self.titleLab.text = model.title;
    self.indexPath = indexPath;
    self.rightView.hidden = !model.isHasMore;
}

- (void)moreBtnAction:(UIButton *)sender{
    [self sendActionName:@"moreAction" param:@{@"btn":sender} indexPath:self.indexPath];
}

- (void)setContentView{
    self.titleLab = [[UILabel alloc] init];
    [self.titleLab setBy_font:16];
    self.titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(0);
        make.height.mas_equalTo(18);
        make.width.mas_equalTo(100);
    }];
    
    UIView *rightView = [UIView by_init];
    [self.contentView addSubview:rightView];
    self.rightView = rightView;
    [rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(200);
    }];
    
    UILabel *rightLab = [UILabel by_init];
    [rightLab setBy_font:12];
    rightLab.text = @"更多";
    rightLab.textColor = kColorRGBValue(0x323232);
    rightLab.textAlignment = NSTextAlignmentRight;
    [rightView addSubview:rightLab];
    [rightLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-25);
        make.width.mas_equalTo(stringGetWidth(rightLab.text, 12));
        make.height.mas_equalTo(stringGetHeight(rightLab.text, 12));
        make.centerY.mas_equalTo(0);
    }];
    
    UIImageView *rightIcon = [UIImageView by_init];
    [rightIcon by_setImageName:@"livehome_rightIcon"];
    [rightView addSubview:rightIcon];
    [rightIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(rightIcon.image.size.width);
        make.height.mas_equalTo(rightIcon.image.size.height);
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(0);
    }];
    
    UIButton *button = [UIButton by_buttonWithCustomType];
    [button addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [rightView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

@end
