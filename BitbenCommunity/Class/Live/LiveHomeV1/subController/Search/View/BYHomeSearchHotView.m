//
//  BYHomeSearchHotView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeSearchHotView.h"
#import "BYSearchHisModel.h"


static NSInteger kBaseTag = 0x821;

@interface BYHomeSearchHotView ()

/** data */
@property (nonatomic ,strong) NSArray *data;
/** topView */
@property (nonatomic ,strong) UIView *topView;

@end

@implementation BYHomeSearchHotView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setTopViewHidden:(BOOL)hidden{
    _topView.hidden = hidden;
}

- (void)loadData:(NSArray *)data{
    self.data = data;
    for (int i = 0; i < data.count; i ++) {
        BYSearchHotModel *model = data[i];
        UIButton *button = [UIButton by_buttonWithCustomType];
        UIButton *lastBtn = [self viewWithTag:kBaseTag + i - 1];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        [button setTitle:model._id forState:UIControlStateNormal];
        [button setTitleColor:kColorRGBValue(0x323232) forState:UIControlStateNormal];
        [button setBackgroundColor:kColorRGBValue(0xf5f6f7)];
        [button addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        if (i <= 2) {
            [button setImage:[UIImage imageNamed:@"live_search_tag_hot"] forState:UIControlStateNormal];
            button.titleEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
            button.imageEdgeInsets = UIEdgeInsetsMake(0, -3, 0, 0);
        }
        [self addSubview:button];
        CGFloat width;
        if (i <= 2) {
            width = stringGetWidth(model._id, 14) + 50;
        }else{
            width = stringGetWidth(model._id, 14) + 30;
        }
        CGFloat height = stringGetHeight(model._id, 14) + 20;
        CGFloat originX = 0;
        CGFloat originY = 0;
        if (i == 0) {
            originX = 15;
            originY = 67;
        }else{
            CGFloat detal = kCommonScreenWidth - CGRectGetMaxX(lastBtn.frame) - width - 9;
            if (detal < 15) { // 当前行无法显示全信息则自动换至下行显示
                originX = 15;
                originY = CGRectGetMaxY(lastBtn.frame) + 12;
            }else{
                originX = CGRectGetMaxX(lastBtn.frame) + 9;
                originY = CGRectGetMinY(lastBtn.frame);
            }
        }
        button.layer.cornerRadius = 16;
        button.tag = kBaseTag + i;
        button.frame = CGRectMake(originX, originY, width, height);
    }
}

#pragma mark - action
- (void)btnAction:(UIButton *)sender{
    NSInteger index = sender.tag - kBaseTag;
    BYSearchHotModel *model = self.data[index];
    if (self.didSelectTagHandle) {
        self.didSelectTagHandle(nullToEmpty(model._id));
    }
}

#pragma mark - configUI
- (void)setContentView{
    UIView *topView = [UIView by_init];
    [topView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self addSubview:topView];
    self.topView = topView;
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(8);
    }];
    
    UIImageView *hotImgView = [[UIImageView alloc] init];
    [hotImgView by_setImageName:@"live_search_hot"];
    [self addSubview:hotImgView];
    [hotImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(28);
        make.width.mas_equalTo(hotImgView.image.size.width);
        make.height.mas_equalTo(hotImgView.image.size.height);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    titleLab.text = @"热门搜索";
    [titleLab setBy_font:16];
    titleLab.textColor = kColorRGBValue(0x323232);
    [self addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(hotImgView.mas_centerY).mas_offset(0);
        make.left.mas_equalTo(hotImgView.mas_right).mas_offset(10);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(20);
    }];
    
}

@end
