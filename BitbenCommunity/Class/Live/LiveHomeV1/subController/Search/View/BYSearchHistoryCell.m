//
//  BYSearchHistoryCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYSearchHistoryCell.h"

@interface BYSearchHistoryCell ()

/** titleLab */
@property (nonatomic ,strong) UILabel *titlaLab;
/** delBtn */
@property (nonatomic ,strong) UIButton *deleteBtn;


@end

@implementation BYSearchHistoryCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYCommonModel *model = object[indexPath.row];
    self.indexPath = indexPath;
    self.titlaLab.text = model.title;
}

#pragma mark - action
- (void)deleteBtnAction{
    [self sendActionName:@"deleteAction" param:nil indexPath:self.indexPath];
}

#pragma mark - configUI

- (void)setContentView{
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView by_setImageName:@"live_search_his_icon"];
    [self.contentView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(imageView.image.size.width);
        make.height.mas_equalTo(imageView.image.size.height);
    }];
    
    
    self.titlaLab = [UILabel by_init];
    [self.titlaLab setBy_font:14];
    self.titlaLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:self.titlaLab];
    [self.titlaLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(40);
        make.centerY.mas_equalTo(0);
        make.height.mas_equalTo(18);
        make.right.mas_equalTo(-15);
    }];
    
    self.deleteBtn = [UIButton by_buttonWithCustomType];
    [self.deleteBtn addTarget:self action:@selector(deleteBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.deleteBtn setImage:[UIImage imageNamed:@"live_search_his_del"] forState:UIControlStateNormal];
    [self.contentView addSubview:self.deleteBtn];
    [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(39);
        make.top.bottom.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.centerX.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
}

@end

@implementation BYSearchHistoryDelCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)delBtnAction{
    [self sendActionName:@"deleteAllAction" param:nil indexPath:self.indexPath];
}

- (void)setContentView{
    UIButton *delBtn = [UIButton by_buttonWithCustomType];
    delBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [delBtn setTitle:@"清除所有记录" forState:UIControlStateNormal];
    [delBtn setTitleColor:kColorRGBValue(0x777777) forState:UIControlStateNormal];
    [delBtn addTarget:self action:@selector(delBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:delBtn];
    [delBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

@end
