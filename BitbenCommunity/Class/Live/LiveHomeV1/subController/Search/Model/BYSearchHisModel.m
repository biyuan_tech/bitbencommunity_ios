//
//  BYSearchHisModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYSearchHisModel.h"
#import "BYHomeArticleModel.h"
#import "BYRecommendUserModel.h"
#import "BYPersonHomeHeaderModel.h"

static NSInteger topMaxCount = 2;

@implementation BYSearchHisModel

/** 解析搜索结果的推荐数据 */
+ (NSArray *)getSearchRecommendData:(NSDictionary *)respond tag:(NSString *)tag{
    if (![respond isKindOfClass:[NSDictionary class]]) {
        return @[];
    }
    NSMutableArray *data = [NSMutableArray array];
    NSArray *liveRspData = respond[@"recommend_live"];
    NSArray *videoRspData = respond[@"recommend_video"];
    NSArray *articleRspData = respond[@"recommend_article"];
    NSArray *userRspData = respond[@"recommend_user"];
    if (liveRspData.count) {
        NSArray *liveData = [BYSearchHisModel getSearchLiveData:liveRspData tag:tag isHasMore:YES];
        if ([liveData count]) {
            [data addObject:@{@"data":liveData}];
        }
    }
    if (videoRspData.count) {
        NSArray *videoData = [BYSearchHisModel getSearchVideoData:videoRspData tag:tag isHasMore:YES];
        if ([videoData count]) {
            [data addObject:@{@"data":videoData}];
        }
    }
    if (articleRspData.count) {
        NSArray *articleData = [BYSearchHisModel getSearchArticleData:articleRspData tag:tag isHasMore:YES];
        if ([articleData count]) {
            [data addObject:@{@"data":articleData}];
        }
    }
    if (userRspData.count) {
        NSArray *userData = [BYSearchHisModel getSearchUserData:userRspData tag:tag isHasMore:YES];
        if ([userData count]) {
            [data addObject:@{@"data":userData}];
        }
    }
    return data;
}
/** 解析搜索结果的直播数据 */
+ (NSArray *)getSearchLiveData:(NSArray *)respond tag:(NSString *)tag isHasMore:(BOOL)isHasMore{
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (![respond count]) {
        return @[];
    }
    NSMutableArray *data = [NSMutableArray array];
    BYCommonModel *topModel = [[BYCommonModel alloc] init];
    topModel.cellString = @"BYSearchResultTitleCell";
    topModel.cellHeight = 55;
    topModel.title = @"直播";
    topModel.isHasMore = isHasMore;
    topModel.index = 1;
    [data addObject:topModel];
    for (NSDictionary *dic in respond) {
        BYSearchHisModel *model = [BYSearchHisModel mj_objectWithKeyValues:dic];
        model.cellString = @"BYRecommendNoticMoreCell";
//        model.cellHeight = 105;
        NSInteger index = [respond indexOfObject:dic];
        if (respond.count == topMaxCount) {
            model.cellHeight = index == respond.count - 1 ? 105 - 13 : 105;
        }else{
            model.cellHeight = 105;
        }
        NSAttributedString *attributedStr = [model.live_title tagKeyWordsStr:tag attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0xff0000)}];
        model.attributedString = attributedStr;
        model.isShowWatchNum = YES;
        [data addObject:model];
    }
    return data;
}
/** 解析搜索结果的视频数据 */
+ (NSArray *)getSearchVideoData:(NSArray *)respond tag:(NSString *)tag isHasMore:(BOOL)isHasMore{
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (![respond count]) {
        return @[];
    }
    NSMutableArray *data = [NSMutableArray array];
    BYCommonModel *topModel = [[BYCommonModel alloc] init];
    topModel.cellString = @"BYSearchResultTitleCell";
    topModel.cellHeight = 55;
    topModel.title = @"视频";
    topModel.index = 2;
    topModel.isHasMore = isHasMore;
    [data addObject:topModel];
    for (NSDictionary *dic in respond) {
        BYSearchHisModel *model = [BYSearchHisModel mj_objectWithKeyValues:dic];
        model.cellString = @"BYGuessLikeCell";
        NSInteger index = [respond indexOfObject:dic];
        if (respond.count == topMaxCount) {
            model.cellHeight = index == respond.count - 1 ? 92 - 13 : 92;
        }else{
            model.cellHeight = 92;
        }
        NSAttributedString *attributedStr = [model.live_title tagKeyWordsStr:tag attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0xff0000)}];
        model.attributedString = attributedStr;
        model.isShowWatchNum = YES;
        [data addObject:model];
    }
    return data;
}
/** 解析搜索结果的观点数据 */
+ (NSArray *)getSearchArticleData:(NSArray *)respond tag:(NSString *)tag isHasMore:(BOOL)isHasMore{
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (![respond count]) {
        return @[];
    }
    NSMutableArray *data = [NSMutableArray array];
    BYCommonModel *topModel = [[BYCommonModel alloc] init];
    topModel.cellString = @"BYSearchResultTitleCell";
    topModel.cellHeight = 55;
    topModel.title = @"观点";
    topModel.isHasMore = isHasMore;
    topModel.index = 3;
    [data addObject:topModel];
    for (NSDictionary *dic in respond) {
        BYHomeArticleModel *model = [BYHomeArticleModel mj_objectWithKeyValues:dic];
        model.cellString = @"BYSearchResultArticleCell";
        NSInteger index = [respond indexOfObject:dic];
        if (respond.count == topMaxCount) {
            model.cellHeight = index == respond.count - 1 ? 92 - 13 : 92;
        }else{
            model.cellHeight = 92;
        }
        NSAttributedString *attributedStr = [model.title tagKeyWordsStr:tag attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0xff0000)}];
        model.attributedString = attributedStr;
        [data addObject:model];
    }
    return data;
}
/** 解析搜索结果的用户数据 */
+ (NSArray *)getSearchUserData:(NSArray *)respond tag:(NSString *)tag isHasMore:(BOOL)isHasMore{
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (![respond count]) {
        return @[];
    }
    NSMutableArray *data = [NSMutableArray array];
    BYCommonModel *topModel = [[BYCommonModel alloc] init];
    topModel.cellString = @"BYSearchResultTitleCell";
    topModel.cellHeight = 55;
    topModel.title = @"用户";
    topModel.isHasMore = isHasMore;
    topModel.index = 4;
    [data addObject:topModel];
    for (NSDictionary *dic in respond) {
        BYPersonHomeHeaderModel *model = [BYPersonHomeHeaderModel mj_objectWithKeyValues:dic];
        model.showIntro = YES;
        model.cellString = @"BYHomeAttentionUserCell";
        model.cellHeight = 64;
        NSAttributedString *attributedStr = [model.title tagKeyWordsStr:tag attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kColorRGBValue(0xff0000)}];
        model.attributedString = attributedStr;
        [data addObject:model];
    }
    return data;
}

+ (NSArray *)getSearchLiveData:(NSArray *)respond tag:(NSString *)tag{
    return [BYSearchHisModel getSearchLiveData:respond tag:tag isHasMore:NO];
}

+ (NSArray *)getSearchVideoData:(NSArray *)respond tag:(NSString *)tag{
    return [BYSearchHisModel getSearchVideoData:respond tag:tag isHasMore:NO];
}

+ (NSArray *)getSearchArticleData:(NSArray *)respond tag:(NSString *)tag{
    return [BYSearchHisModel getSearchArticleData:respond tag:tag isHasMore:NO];
}

+ (NSArray *)getSearchUserData:(NSArray *)respond tag:(NSString *)tag{
    return [BYSearchHisModel getSearchUserData:respond tag:tag isHasMore:NO];
}

@end

@implementation BYSearchHotModel

+ (NSArray *)getHotData:(NSArray *)respondData{
    if (![respondData isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (![respondData count]) {
        return @[];
    }
    NSMutableArray *data = [NSMutableArray array];
    for (NSDictionary *dic in respondData) {
        BYSearchHotModel *model = [BYSearchHotModel mj_objectWithKeyValues:dic];
        [data addObject:model];
    }
    return data;
}

@end

static char isHasMoreKey;
static char indexKey;
@implementation BYCommonModel (isHasMore)

- (void)setIsHasMore:(BOOL)isHasMore{
    objc_setAssociatedObject(self, &isHasMoreKey, @(isHasMore), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)isHasMore{
    NSNumber *number = objc_getAssociatedObject(self, &isHasMoreKey);
    return [number boolValue];
}

- (void)setIndex:(NSInteger)index{
    objc_setAssociatedObject(self, &indexKey, @(index), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSInteger)index{
    NSNumber *number = objc_getAssociatedObject(self, &indexKey);
    return [number integerValue];
}
@end

