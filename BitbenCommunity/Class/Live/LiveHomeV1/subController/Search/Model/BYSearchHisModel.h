//
//  BYSearchHisModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonLiveModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYSearchHisModel : BYCommonLiveModel

/** NSAttributedString */
@property (nonatomic ,strong) NSAttributedString *attributedString;
/** 是否显示观看人次 */
@property (nonatomic ,assign) BOOL isShowWatchNum;


/** 解析搜索结果的推荐数据 */
+ (NSArray *)getSearchRecommendData:(NSDictionary *)respond tag:(NSString *)tag;
/** 解析搜索结果的直播数据 */
+ (NSArray *)getSearchLiveData:(NSArray *)respond tag:(NSString *)tag;
/** 解析搜索结果的视频数据 */
+ (NSArray *)getSearchVideoData:(NSArray *)respond tag:(NSString *)tag;
/** 解析搜索结果的观点数据 */
+ (NSArray *)getSearchArticleData:(NSArray *)respond tag:(NSString *)tag;
/** 解析搜索结果的用户数据 */
+ (NSArray *)getSearchUserData:(NSArray *)respond tag:(NSString *)tag;

@end

@interface BYSearchHotModel : BYCommonModel

/** 搜索内容 */
@property (nonatomic ,copy) NSString *_id;

+ (NSArray *)getHotData:(NSArray *)respondData;

@end

@interface BYCommonModel (isHasMore)

/** 是否有更多按钮 */
@property (nonatomic ,assign) BOOL isHasMore;
/** index */
@property (nonatomic ,assign) NSInteger index;

@end

NS_ASSUME_NONNULL_END
