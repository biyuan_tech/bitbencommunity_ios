//
//  BYHomeRecommendController.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYHomeRecommendController.h"
#import "BYVideoRoomController.h"
#import "BYILiveRoomController.h"
#import "BYPPTUploadController.h"
#import "BYVODPlayController.h"
#import "BYPPTRoomController.h"
#import "ArticleDetailRootViewController.h"
#import "BYPersonHomeController.h"
#import "BYVideoLiveController.h"
#import "BYRecommendNoticMoreController.h"
#import "BYRecommendMoreController.h"
#import "BYHomeCourseController.h"
#import "BYCourseDetailController.h"
#import "AppointmentViewController.h"
#import "IntelligenceRootViewController.h"                  // 币本情报
#import "SpecialColumnViewController.h"
#import "BYOfficialCertGroupController.h"
#import "BYLiveActivityController.h"

#import "BYHomeBannerModel.h"
#import "BYRecommendUserModel.h"
#import "BYHomeArticleModel.h"

#import "BYHomeRecommendHeaderView.h"
#import "BYFeaturedRecommendCell.h"
#import "BYRecommendLiveNoticeCell.h"


@interface BYHomeRecommendController ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** headerView */
@property (nonatomic ,strong) BYHomeRecommendHeaderView *headerView;
/** 精选推荐乱序数据 */
@property (nonatomic ,strong) NSArray *featuredData;

@end

@implementation BYHomeRecommendController

- (void)viewDidLoad{
    [super viewDidLoad];
    [self addTableView];
    
    [self setEmptyTableData];
    [self reloadData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self startTimer];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self destoryTimer];
}

#pragma mark - custom Method

- (void)autoReload{
    [self.tableView beginRefreshing];
    [self reloadData];
}

- (void)startTimer{
    [_headerView startTimer];
    [self startLiveTimer];
}

- (void)destoryTimer{
    [_headerView destoryTimer];
    [self destoryLiveTimer];
}

- (void)startLiveTimer{
    if (!_tableView) return;
    NSDictionary *dic = _tableView.tableData[0];
    BYCommonLiveModel *model = dic.allValues[0][0];
    if (model.status != BY_LIVE_STATUS_SOON) return;
    BYRecommendLiveNoticeCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
#pragma clang diagnostic ignored "-Wundeclared-selector"
    [cell performSelector:@selector(verifyLiveTime)];
}

- (void)destoryLiveTimer{
    [[BYCommonTool shareManager] stopTimer];
}

- (void)setEmptyTableData{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < 4; i ++) {
        BYCommonLiveModel *model = [[BYCommonLiveModel alloc] init];
        model.cellString = @"BYCommonTableViewCell";
        model.cellHeight = 0;
        [array addObject:@{@"data":@[model]}];
    }
    self.tableView.tableData = [array copy];
}

// 轮播图点击
- (void)headerViewDidSelectAdvertIndex:(NSInteger)index{
    BYHomeBannerModel *bannerModel = _headerView.advertArr[index];
    // banner点击统计
    [[BYLiveHomeRequest alloc] loadRequestClickBanner:bannerModel.banner_id];
    
    [DirectManager commonPushControllerWithThemeType:bannerModel.theme_type
                                            liveType:bannerModel.live_type
                                             themeId:bannerModel.theme_id];
}

// 公告滚动点击
- (void)headerViewDidSelectRecommendIndex:(NSInteger)index{
    BYHomeBannerModel *bannerModel = _headerView.recommendDatas[index];
    [DirectManager commonPushControllerWithThemeType:bannerModel.theme_type
                                            liveType:bannerModel.live_type
                                             themeId:bannerModel.theme_id];
}

// 课程等点击跳转
- (void)headerViewDidSelectMoudleAtIndex:(NSInteger)index{
    UIViewController *currentContoller = [BYTabbarViewController sharedController].currentController;
    if (![AccountModel sharedAccountModel].hasLoggedIn) {
        @weakify(self);
        [(AbstractViewController *)currentContoller authorizeWithCompletionHandler:^(BOOL successed) {
            @strongify(self);
            if (!successed) return ;
            [self headerViewDidSelectMoudleAtIndex:index];
        }];
        return;
    }
    switch (index) {
        case 1: // 课程
        {
            BYHomeCourseController *courseController = [[BYHomeCourseController alloc] init];
            [currentContoller.navigationController pushViewController:courseController animated:YES];
        }
            break;
        case 2: // 活动
        {
            BYLiveActivityController *activityController = [[BYLiveActivityController alloc] init];
            [currentContoller.navigationController pushViewController:activityController animated:YES];
//            AppointmentViewController *appointmentVC = [[AppointmentViewController alloc]init];
//            [currentContoller.navigationController pushViewController:appointmentVC animated:YES];

        }
            break;
        case 0: // 币本情报
        {
            IntelligenceRootViewController *intelligenceRootVC = [[IntelligenceRootViewController alloc]init];
            [currentContoller.navigationController pushViewController:intelligenceRootVC animated:YES];
        }
            break;
        case 3: // 专栏
        {
            SpecialColumnViewController *specialColumnVC = [[SpecialColumnViewController alloc]init];
            [currentContoller.navigationController pushViewController:specialColumnVC animated:YES];
        }
            break;
        case 4: // 加入社群
        {
            AppointmentViewController *appointController = [[AppointmentViewController alloc] init];
            [currentContoller.navigationController pushViewController:appointController animated:YES];
//            BYOfficialCertGroupController *groupController = [[BYOfficialCertGroupController alloc] init];
//            [currentContoller.navigationController pushViewController:groupController animated:YES];
        }
            break;
        default:
            break;
    }
}

- (void)reloadData{
    [self loadRequestGetRecommendLive];
    [self loadRequestGetFeature];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 ||
        (indexPath.section == 1 && indexPath.row > 0) ) {
        NSDictionary *dic = tableView.tableData[indexPath.section];
        id model = dic.allValues[0][indexPath.row];
        BY_THEME_TYPE type = 0;
        BY_NEWLIVE_TYPE liveType = 0;
        NSString *theme_id = @"";
        if ([model isKindOfClass:[BYCommonLiveModel class]]) {
            type = BY_THEME_TYPE_LIVE;
            theme_id = ((BYCommonLiveModel *)model).live_record_id;
            liveType = ((BYCommonLiveModel *)model).live_type;
        }else if ([model isKindOfClass:[BYHomeArticleModel class]]){
            type = BY_THEME_TYPE_ARTICLE;
            theme_id = ((BYHomeArticleModel *)model).article_id;
        }
        [DirectManager commonPushControllerWithThemeType:type
                                                liveType:liveType
                                                 themeId:theme_id];
    }
    else if (indexPath.section == 3  &&
             indexPath.row == 0){
        BYRecommendMoreController *recommendMoreController = [[BYRecommendMoreController alloc] init];
        recommendMoreController.data = self.featuredData;
        UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
        [currentController.navigationController pushViewController:recommendMoreController animated:YES];

    }else if (indexPath.section == 1 &&
              indexPath.row == 0){
        [self.tabBarController setSelectedIndex:2];
    }
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = tableView.tableData[indexPath.section];
    BYCommonLiveModel *model = dic.allValues[0][indexPath.row];
    if ([actionName isEqualToString:@"appointAction"]) { // 关注
        if (model.attention) {
            @weakify(self);
            [self authorizeWithCompletionHandler:^(BOOL successed) { // 判断登录状态
                @strongify(self);
                if (!successed) return ;
                [self loadRequestCancelAttentionLive:model tableView:tableView indexPath:indexPath];
            }];
            return;
        }
        @weakify(self);
        [self authorizeWithCompletionHandler:^(BOOL successed) { // 判断登录状态
            @strongify(self);
            if (!successed) return ;
            [self loadRequestAttentionLive:model tableView:tableView indexPath:indexPath];
        }];
    }else if ([actionName isEqualToString:@"noticMoreAction"]){
        BYRecommendNoticMoreController *noticMoreController = [[BYRecommendNoticMoreController alloc] init];
        UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
        [currentController.navigationController pushViewController:noticMoreController animated:YES];
    }else if ([actionName isEqualToString:@"reloadAction"]){
        [self reloadData];
    }else if ([actionName isEqualToString:@"refreshAction"]){ // 换一批
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row - 1 inSection:indexPath.section]];
        if ([cell isKindOfClass:[BYFeaturedRecommendCell class]]) {
            [(BYFeaturedRecommendCell *)cell refreshDataAction];
        }
    }
}

#pragma mark - request

// 直播预告，感兴趣用户
- (void)loadRequestGetRecommendLive{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestLiveHomeRecommendLive:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        
        // 刷新banner
        self.headerView.advertData = object[0];
        [self.headerView reloadData];
        
        NSMutableArray *tableData = [NSMutableArray arrayWithArray:self.tableView.tableData];
        [tableData replaceObjectAtIndex:0 withObject:object[1]];
        [tableData replaceObjectAtIndex:2 withObject:object[2]];
        self.tableView.tableData = tableData;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

// 精选推荐，猜你喜欢
- (void)loadRequestGetFeature{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestLiveHomeHotContent:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        NSMutableArray *tableData = [NSMutableArray arrayWithArray:self.tableView.tableData];
        NSDictionary *dic = object[1];
        if ([dic[@"data"] count]) {
            BYRecommendUserModel *model = dic.allValues[0][1];
            self.featuredData = model.featuredData;
        }
        [tableData replaceObjectAtIndex:1 withObject:object[0]];
        [tableData replaceObjectAtIndex:3 withObject:object[1]];
        self.tableView.tableData = tableData;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

- (void)loadRequestAttentionLive:(BYCommonLiveModel *)model tableView:(BYCommonTableView *)tableView indexPath:(NSIndexPath *)indexPath{
    [[BYLiveHomeRequest alloc] loadRequestAttentionLive:model.live_record_id successBlock:^(id object) {
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYCommonLiveModel *model = dic.allValues[0][indexPath.row];
        model.attention = [object boolValue];
        model.attention_count++;
        [tableView reloadData];
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)loadRequestCancelAttentionLive:(BYCommonLiveModel *)model tableView:(BYCommonTableView *)tableView indexPath:(NSIndexPath *)indexPath{
    [[BYLiveHomeRequest alloc] loadRequestCancelAttentionLive:model.live_record_id successBlock:^(id object) {
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYCommonLiveModel *model = dic.allValues[0][indexPath.row];
        model.attention = [object boolValue];
        model.attention_count--;
        [tableView reloadData];
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - configUI
- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.tableHeaderView = self.headerView;;
    self.tableView.backgroundColor = kColorRGBValue(0xf2f4f5);
    self.tableView.group_delegate = self;
    [self.tableView addHeaderRefreshTarget:self action:@selector(reloadData)];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (BYHomeRecommendHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[BYHomeRecommendHeaderView alloc] init];
//        if ([AccountModel sharedAccountModel].isShenhe){
//            _headerView.frame = CGRectMake(0, 0, kCommonScreenWidth, 172);
//        }else{
            _headerView.frame = CGRectMake(0, 0, kCommonScreenWidth, 275);
//        }
        @weakify(self);
        _headerView.didSelectAdvertIndex = ^(NSInteger index) {
            @strongify(self);
            [self headerViewDidSelectAdvertIndex:index];
        };
        _headerView.didSelectRecommendIndex = ^(NSInteger index) {
            @strongify(self);
            [self headerViewDidSelectRecommendIndex:index];
        };
        _headerView.didSelectModuleIndex = ^(NSInteger index) {
            @strongify(self);
            [self headerViewDidSelectMoudleAtIndex:index];
//            showToastView(@"敬请期待", self.view);
        };
    }
    return _headerView;
}


@end
