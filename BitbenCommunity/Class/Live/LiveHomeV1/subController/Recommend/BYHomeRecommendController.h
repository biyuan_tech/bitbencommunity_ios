//
//  BYHomeRecommendController.h
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeRecommendController : BYCommonViewController


- (void)startTimer;

- (void)destoryTimer;

- (void)autoReload;
@end

NS_ASSUME_NONNULL_END
