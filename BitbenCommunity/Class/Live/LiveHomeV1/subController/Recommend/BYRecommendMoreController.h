//
//  BYRecommendMoreController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/3.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYRecommendMoreController : BYCommonViewController

/** 精选推荐数据 */
@property (nonatomic ,strong) NSArray *data;


@end

NS_ASSUME_NONNULL_END
