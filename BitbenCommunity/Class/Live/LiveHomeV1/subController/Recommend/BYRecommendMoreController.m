//
//  BYRecommendMoreController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/3.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYRecommendMoreController.h"
#import "BYLiveHomeSegmentViewV1.h"

#import "BYRecommendUserModel.h"

static NSInteger kBaseTag = 0x555;
@interface BYRecommendMoreController ()<BYLiveHomeSegmentViewDelegateV1,BYCommonTableViewDelegate>
/** 分类器 */
@property (nonatomic ,strong) BYLiveHomeSegmentViewV1 *segmentView;
/** 综合数据源 */
@property (nonatomic ,strong) NSArray *allData;
/** 直播数据源 */
@property (nonatomic ,strong) NSArray *liveData;
/** 视频数据源 */
@property (nonatomic ,strong) NSArray *videoData;

@end

@implementation BYRecommendMoreController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"精选推荐";
    [self addSegmentView];
}

- (void)setData:(NSArray *)data{
    _data = data;
    NSMutableArray *liveData = [NSMutableArray array];
    NSMutableArray *videoData = [NSMutableArray array];
    for (BYCommonLiveModel *model in data) {
        if (model.status == BY_LIVE_STATUS_END) {
            [liveData addObject:model];
        }
        if (model.live_type == BY_NEWLIVE_TYPE_VOD_VIDEO ||
            model.live_type == BY_NEWLIVE_TYPE_VOD_AUDIO ) {
            [videoData addObject:model];
        }
    }
    self.allData = [BYRecommendUserModel getFeaturedRecommendAllData:data];
    self.liveData = [BYRecommendUserModel getFeaturedRecommendLiveEndData:liveData];
    self.videoData = [BYRecommendUserModel getFeaturedRecommendVideoData:videoData];
}

#pragma mark - BYLiveHomeSegmentViewDelegateV1
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    BYCommonTableView *tableView = [[BYCommonTableView alloc] init];
    tableView.group_delegate = self;
    tableView.tag = kBaseTag + index;
    UIView *tableViewHeaderView = [UIView by_init];
    tableViewHeaderView.frame = CGRectMake(0, 0, kCommonScreenWidth, 12);
    tableView.tableHeaderView = tableViewHeaderView;
    NSArray *data;
    switch (index) {
        case 0:
            data = self.allData;
            break;
        case 1:
            data = self.liveData;
            break;
        default:
            data = self.videoData;
            break;
    }
    if (!data.count) {
        [tableView addEmptyView:@"暂无内容"];
    }
    return tableView;
}

- (void)by_segmentViewDidScrollToIndex:(NSInteger)index{
    BYCommonTableView *tableView = (BYCommonTableView *)[self.segmentView viewWithTag:kBaseTag + index];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        switch (index) {
            case 0:
                tableView.tableData = self.allData;
                break;
            case 1:
                tableView.tableData = self.liveData;
                break;
            default:
                tableView.tableData = self.videoData;
                break;
        }
    });
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYCommonLiveModel *model = dic.allValues[0][indexPath.row];
        [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_LIVE
                                                liveType:model.live_type
                                                 themeId:model.live_record_id];
    }
}

#pragma mark - configUI

- (void)addSegmentView{
    
    self.segmentView = [[BYLiveHomeSegmentViewV1 alloc] initWithTitles:@"综合",@"直播回放",@"视频", nil];
    self.segmentView.delegate = self;
    self.segmentView.defultSelIndex = 0;
    [self.view addSubview:self.segmentView];
    [_segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(0);
    }];
    [self.segmentView layoutIfNeeded];
}
@end
