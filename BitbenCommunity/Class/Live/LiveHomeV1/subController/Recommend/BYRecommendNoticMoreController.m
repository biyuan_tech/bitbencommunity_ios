//
//  BYRecommendNoticMoreController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/3.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYRecommendNoticMoreController.h"

#import "BYLiveHomeSegmentViewV1.h"

static NSInteger kBaseTag = 0x239;
@interface BYRecommendNoticMoreController ()<BYLiveHomeSegmentViewDelegateV1,BYCommonTableViewDelegate>

/** 分类器 */
@property (nonatomic ,strong) BYLiveHomeSegmentViewV1 *segmentView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;

@end

@implementation BYRecommendNoticMoreController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"直播及预告";
    _pageNum = 0;
    [self addSegmentView];
}

- (void)reloadData:(NSInteger)index{
    if (index == 2) {
        _pageNum = 0;
    }
    [self loadRequestGetMoreLives:index];
}

- (void)loadMoreData:(NSInteger)index{
    [self loadRequestGetMoreLives:index];
}

#pragma mark - BYLiveHomeSegmentViewDelegateV1
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    BYCommonTableView *tableView = [[BYCommonTableView alloc] init];
    tableView.tag = kBaseTag + index;
    tableView.group_delegate = self;
    UIView *headerView = [UIView by_init];
    headerView.frame = CGRectMake(0, 0, kCommonScreenWidth, 13);
    tableView.tableHeaderView = headerView;
    @weakify(self);
    [tableView addHeaderRefreshHandle:^{
        @strongify(self);
        [self reloadData:index];
    }];
    if (index == 2) {
        [tableView addFooterRefreshHandle:^{
            @strongify(self);
            [self loadMoreData:index];
        }];
    }
    return tableView;
}

- (void)by_segmentViewDidScrollToIndex:(NSInteger)index{
    BYCommonTableView *tableView = [self.segmentView viewWithTag:kBaseTag + index];
    if (tableView && tableView.tableData.count) {
        return;
    }
    [self loadRequestGetMoreLives:index];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BYCommonLiveModel *model = tableView.tableData[indexPath.row];
    [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_LIVE
                                            liveType:model.live_type
                                             themeId:model.live_record_id];
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    BYCommonLiveModel *model = tableView.tableData[indexPath.row];
    if ([actionName isEqualToString:@"appointAction"]) { // 关注
        if (model.attention) {
            @weakify(self);
            [self authorizeWithCompletionHandler:^(BOOL successed) { // 判断登录状态
                @strongify(self);
                if (!successed) return ;
                [self loadRequestCancelAttentionLive:model tableView:tableView indexPath:indexPath];
            }];
            return;
        }
        @weakify(self);
        [self authorizeWithCompletionHandler:^(BOOL successed) { // 判断登录状态
            @strongify(self);
            if (!successed) return ;
            [self loadRequestAttentionLive:model tableView:tableView indexPath:indexPath];
        }];
    }
}

#pragma mark - request
- (void)loadRequestGetMoreLives:(NSInteger)index{
    BY_LIVE_STATUS status = index == 0 ? 2 : (index == 1 ? 1 : 3);
    NSInteger pageNum = index == 2 ? _pageNum : 0;
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetMoreLives:status pageNum:pageNum successBlock:^(id object) {
        @strongify(self);
        BYCommonTableView *tableView = [self.segmentView viewWithTag:kBaseTag + index];
        [tableView endRefreshing];
        [tableView removeEmptyView];
        if (![object count]) {
            tableView.tableData = object;
            if (pageNum == 0) {
                [tableView addEmptyView:@"暂无内容"];
            }
            return;
        }
        if (pageNum == 0) {
            tableView.tableData = object;
        }
        else{
            NSMutableArray *data = [NSMutableArray array];
            [data addObjectsFromArray:tableView.tableData];
            [data addObjectsFromArray:object];
            tableView.tableData = data;
        }
        if (index == 2) {
            self.pageNum ++;
        }
    } faileBlock:^(NSError *error) {
        @strongify(self);
        BYCommonTableView *tableView = [self.segmentView viewWithTag:kBaseTag + index];
        [tableView endRefreshing];
    }];
}

- (void)loadRequestAttentionLive:(BYCommonLiveModel *)model tableView:(BYCommonTableView *)tableView indexPath:(NSIndexPath *)indexPath{
    [[BYLiveHomeRequest alloc] loadRequestAttentionLive:model.live_record_id successBlock:^(id object) {
        BYCommonLiveModel *model = tableView.tableData[indexPath.row];
        model.attention = [object boolValue];
        model.attention_count++;
        [tableView reloadData];
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)loadRequestCancelAttentionLive:(BYCommonLiveModel *)model tableView:(BYCommonTableView *)tableView indexPath:(NSIndexPath *)indexPath{
    [[BYLiveHomeRequest alloc] loadRequestCancelAttentionLive:model.live_record_id successBlock:^(id object) {
        BYCommonLiveModel *model = tableView.tableData[indexPath.row];
        model.attention = [object boolValue];
        model.attention_count--;
        [tableView reloadData];
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - configUI

- (void)addSegmentView{
    
    self.segmentView = [[BYLiveHomeSegmentViewV1 alloc] initWithTitles:@"直播中",@"预告",@"直播回放", nil];
    self.segmentView.delegate = self;
    self.segmentView.defultSelIndex = 0;
    [self.view addSubview:self.segmentView];
    [_segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(0);
    }];
    [_segmentView layoutIfNeeded];
}




@end
