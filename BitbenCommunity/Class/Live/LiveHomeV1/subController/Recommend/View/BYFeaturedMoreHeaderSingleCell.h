//
//  BYFeaturedMoreHeaderSingleCell.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/4.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYFeaturedMoreHeaderSingleCell : UICollectionViewCell

- (void)setContentWithObject:(BYCommonLiveModel *)object indexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
