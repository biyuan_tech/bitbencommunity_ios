//
//  BYFeaturedMoreHeaderCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/4.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYFeaturedMoreHeaderCell.h"
#import "BYFeaturedMoreHeaderSingleCell.h"

#import "BYRecommendUserModel.h"

@interface BYFeaturedMoreHeaderCell ()<UICollectionViewDelegate,UICollectionViewDataSource>

/** collectionView */
@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) BYRecommendUserModel *model;


@end

@implementation BYFeaturedMoreHeaderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYRecommendUserModel *model = dic.allValues[0][indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    if (model.needReloadData) {
        [self.collectionView reloadData];
        self.model.needReloadData = NO;
    }
}

#pragma mark - UICollectionViewDelegate/DataScoure
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.model.featuredCellData.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BYFeaturedMoreHeaderSingleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell setContentWithObject:self.model.featuredCellData[indexPath.row] indexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    BYCommonLiveModel *model = self.model.featuredCellData[indexPath.row];
    [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_LIVE
                                            liveType:model.live_type
                                             themeId:model.live_record_id];
}

- (void)setContentView{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(207, 145);
    layout.minimumInteritemSpacing = 10.0f;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.headerReferenceSize = CGSizeMake(15, 145);
    layout.footerReferenceSize = CGSizeMake(15, 145);
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [collectionView registerClass:[BYFeaturedMoreHeaderSingleCell class] forCellWithReuseIdentifier:@"cell"];
    [collectionView setBackgroundColor:[UIColor whiteColor]];
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.delegate = self;
    collectionView.dataSource = self;
    [self.contentView addSubview:collectionView];
    [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];}

@end
