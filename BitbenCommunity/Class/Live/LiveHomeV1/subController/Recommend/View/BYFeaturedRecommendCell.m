//
//  BYFeaturedRecommendCell.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYFeaturedRecommendCell.h"
#import "BYFeaturedRecommendSingleCell.h"
#import "BYFeaturedSingleFooterView.h"

#import "BYRecommendUserModel.h"

@interface BYFeaturedRecommendCell ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

/** collection */
@property (nonatomic ,strong) UICollectionView *collection;
/** model */
@property (nonatomic ,strong) BYRecommendUserModel *model;


@end

@implementation BYFeaturedRecommendCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYRecommendUserModel *model = dic.allValues[0][indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    if (self.model.needReloadData) {
        [self.collection reloadData];
        self.model.needReloadData = NO;
    }
}

// 换一批
- (void)refreshDataAction{
    NSInteger maxNum = self.model.featuredCellData.count/6;
    if (self.model.loadPageNum == maxNum) {
        self.model.loadPageNum = 0;
        self.model.featuredLoadData = [self.model.featuredCellData subarrayWithRange:NSMakeRange(0, 6)];
    }
    else if (self.model.loadPageNum == maxNum - 1){
        self.model.loadPageNum = self.model.loadPageNum + 1;
        self.model.featuredLoadData = [self.model.featuredCellData subarrayWithRange:NSMakeRange(6*self.model.loadPageNum, self.model.featuredCellData.count - 6*self.model.loadPageNum)];
    }
    else{
        self.model.loadPageNum = self.model.loadPageNum + 1;
        self.model.featuredLoadData = [self.model.featuredCellData subarrayWithRange:NSMakeRange(6*self.model.loadPageNum, 6)];
    }
    [UIView transitionWithView:self.collection
                      duration:2.0
                       options:UIViewAnimationOptionCurveEaseOut
                    animations:^{
                        [self.collection reloadData];
                    } completion:nil];
    
}

#pragma mark - UICollectionViewDelegate/DataScoure
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.model.featuredLoadData.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BYFeaturedRecommendSingleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell setContentWithObject:self.model.featuredLoadData[indexPath.row] indexPath:indexPath];
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 15, 0, 15);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    BYCommonLiveModel *model = self.model.featuredLoadData[indexPath.row];
    [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_LIVE
                                            liveType:model.live_type
                                             themeId:model.live_record_id];
}

#pragma mark - configUI

- (void)setContentView{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    CGFloat width = (kCommonScreenWidth - 43)/2;
    layout.itemSize = CGSizeMake(width, 193);
    layout.minimumInteritemSpacing = 13.0f;
    layout.minimumLineSpacing = 0.0f;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    UICollectionView *collection = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [collection registerClass:[BYFeaturedRecommendSingleCell class] forCellWithReuseIdentifier:@"cell"];
    [collection setBackgroundColor:[UIColor whiteColor]];
    collection.delegate = self;
    collection.dataSource = self;
    collection.scrollEnabled = NO;
    [self.contentView addSubview:collection];
    self.collection = collection;
    [collection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
}

@end
