//
//  BYRecommendUserCell.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYRecommendUserCell.h"
#import "BYRecommendSingleUserCell.h"

#import "BYRecommendUserModel.h"

@interface BYRecommendUserCell ()<UICollectionViewDelegate,UICollectionViewDataSource>

/** colection */
@property (nonatomic ,strong) UICollectionView *collectionView;
/** model */
@property (nonatomic ,strong) BYRecommendUserModel *model;


@end

@implementation BYRecommendUserCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setConentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYRecommendUserModel *model = dic.allValues[0][indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    if (model.needReloadData) {
        [self.collectionView reloadData];
        self.model.needReloadData = NO;
    }
}

#pragma mark - UICollectionViewDelegate/DataScoure
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.model.cellData.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BYRecommendSingleUserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell setContentWithObject:self.model.cellData[indexPath.row] indexPath:indexPath];
    @weakify(self);
    @weakify(cell);
    cell.attentionAction = ^(NSIndexPath * _Nonnull indexPath) {
        @strongify(self);
        @strongify(cell);
        @weakify(cell);
        [self loadRequestAttention:indexPath suc:^{
            @strongify(cell);
            [cell reloadAttention];
        }];;
    };
    return cell;
}

#pragma mark - request
- (void)loadRequestAttention:(NSIndexPath *)indexPath suc:(void(^)(void))suc{
    BYRecommendUserCellModel *model = self.model.cellData[indexPath.row];
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    [(BYCommonViewController *)currentController authorizeWithCompletionHandler:^(BOOL successed) {
        if (!successed) return ;
        [[BYLiveHomeRequest alloc] loadRequestCreatAttention:model.account_id isAttention:!model.isAttention successBlock:^(id object) {
            model.isAttention = !model.isAttention;
            if (suc) suc();
        } faileBlock:^(NSError *error) {
            
        }];
    }];
}

#pragma mark - configUI

- (void)setConentView{
    UIView *spaceView = [UIView by_init];
    [spaceView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.contentView addSubview:spaceView];
    [spaceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.height.mas_equalTo(8);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    titleLab.font = [UIFont boldSystemFontOfSize:16];
    titleLab.textColor = kColorRGBValue(0x323232);
    titleLab.text = @"你可能感兴趣的用户";
    [self.contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(28);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
    }];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(120, 160);
    layout.minimumInteritemSpacing = 10.0f;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.headerReferenceSize = CGSizeMake(15, 160);
    layout.footerReferenceSize = CGSizeMake(15, 160);
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [collectionView registerClass:[BYRecommendSingleUserCell class] forCellWithReuseIdentifier:@"cell"];
    [collectionView setBackgroundColor:[UIColor whiteColor]];
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.delegate = self;
    collectionView.dataSource = self;
    [self.contentView addSubview:collectionView];
    self.collectionView = collectionView;
    [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(0);
    }];
}

@end
