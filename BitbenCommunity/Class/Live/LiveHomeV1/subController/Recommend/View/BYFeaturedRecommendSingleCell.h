//
//  BYFeaturedRecommendSingleCell.h
//  BibenCommunity
//
//  Created by 随风 on 2019/5/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYFeaturedRecommendSingleCell : UICollectionViewCell

- (void)setContentWithObject:(BYCommonLiveModel *)object indexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
