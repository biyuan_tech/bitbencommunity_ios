//
//  BYRecommendSingleUserCell.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/29.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYRecommendSingleUserCell.h"

#import "BYPersonHomeController.h"

@interface BYRecommendSingleUserCell ()

/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogo;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 关注状态 */
@property (nonatomic ,strong) UIButton *attentionBtn;
/** model */
@property (nonatomic ,strong) BYRecommendUserCellModel *model;
/** indexPath */
@property (nonatomic ,strong) NSIndexPath *indexPath;


@end

@implementation BYRecommendSingleUserCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = NO;
        self.contentView.clipsToBounds = NO;
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(BYRecommendUserCellModel *)object indexPath:(NSIndexPath *)indexPath{
    self.model = object;
    self.userlogo.style = object.cert_badge;
    self.indexPath = indexPath;
    [self.userlogo uploadHDImageWithURL:object.head_img callback:nil];
    self.userNameLab.text = object.nickname;
    [self reloadAttentionStatus:object.isAttention];
}

- (void)reloadAttention{
    [self reloadAttentionStatus:self.model.isAttention];
}

- (void)reloadAttentionStatus:(BOOL)isAttention{
    self.attentionBtn.selected = isAttention;
    self.attentionBtn.backgroundColor = isAttention ? kColorRGBValue(0xf2f2f2) : kColorRGBValue(0xea6441);
}

- (void)attentionBtnAction:(id)sender{
    if (self.attentionAction) {
        self.attentionAction(self.indexPath);
    }
}

- (void)userlogoAction{
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
    personHomeController.user_id = self.model.account_id;
    [(BYCommonViewController *)currentController authorizePush:personHomeController animation:YES];
}

- (void)setContentView{
    
    UIView *bgView = [UIView by_init];
    [bgView setBackgroundColor:[UIColor whiteColor]];
    bgView.layer.cornerRadius = 4.0f;
    bgView.layer.shadowColor = kColorRGB(0, 0, 0, 0.06).CGColor;
    bgView.layer.shadowOpacity = 1;
    bgView.layer.shadowOffset = CGSizeMake(0, 0);
    bgView.layer.shadowRadius = 10;
    [self.contentView addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    PDImageView *userlogo = [[PDImageView alloc] init];
    userlogo.contentMode = UIViewContentModeScaleAspectFill;
    userlogo.layer.cornerRadius = 25;
    userlogo.clipsToBounds = YES;
    userlogo.userInteractionEnabled = YES;
    @weakify(self);
    [userlogo addTapGestureRecognizer:^{
        @strongify(self);
        [self userlogoAction];
    }];
    [self.contentView addSubview:userlogo];
    self.userlogo = userlogo;
    [userlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.width.height.mas_equalTo(50);
        make.top.mas_equalTo(20);
    }];
    
    
    UILabel *userNameLab = [UILabel by_init];
    [userNameLab setBy_font:14];
    userNameLab.textAlignment = NSTextAlignmentCenter;
    userNameLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:userNameLab];
    self.userNameLab = userNameLab;
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(18);
        make.right.mas_equalTo(-18);
        make.top.mas_equalTo(userlogo.mas_bottom).mas_offset(20);
        make.height.mas_equalTo(16);
    }];
    
    UIButton *attentionBtn = [UIButton by_buttonWithCustomType];
    [attentionBtn setBy_attributedTitle:@{@"title":@"关注",
                                        NSForegroundColorAttributeName:kColorRGBValue(0xffffff),
                                        NSFontAttributeName:[UIFont systemFontOfSize:11],
                                        } forState:UIControlStateNormal];
    [attentionBtn setBy_attributedTitle:@{@"title":@"已关注",
                                        NSForegroundColorAttributeName:kColorRGBValue(0x7c7b7b),
                                        NSFontAttributeName:[UIFont systemFontOfSize:11],
                                        } forState:UIControlStateSelected];
    [attentionBtn addTarget:self
                   action:@selector(attentionBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];
    attentionBtn.layer.cornerRadius = 4.0f;
    [self.contentView addSubview:attentionBtn];
    self.attentionBtn = attentionBtn;
    [attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(55);
        make.height.mas_equalTo(22);
        make.top.mas_equalTo(118);
        make.centerX.mas_equalTo(0);
    }];
    [self reloadAttentionStatus:NO];
}

@end
