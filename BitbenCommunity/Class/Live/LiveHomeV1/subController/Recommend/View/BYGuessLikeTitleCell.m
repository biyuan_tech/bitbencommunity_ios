//
//  BYGuessLikeTitleCell.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYGuessLikeTitleCell.h"

#import "BYGuessLikeTitleCell.h"

@interface BYGuessLikeTitleCell ()

@end

@implementation BYGuessLikeTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setContentView{
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:16];
    titleLab.text = @"猜你喜欢";
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(4);
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(titleLab.font.pointSize);
    }];
}

@end
