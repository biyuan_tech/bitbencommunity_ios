//
//  BYGuessLikeCell.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYGuessLikeCell.h"
#import "BYSearchHisModel.h"

@interface BYGuessLikeCell ()
/** 直播封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 直播状态 */
@property (nonatomic ,strong) UILabel *liveStatusLab;
/** 直播状态背景 */
@property (nonatomic ,strong) UIView *liveStatusBg;
/** 标题 */
@property (nonatomic ,strong) UILabel *titleLab;
/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogo;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 观看次数 */
@property (nonatomic ,strong) UILabel *watchNumLab;
/** 热度标签 */
@property (nonatomic ,strong) UIImageView *hotImgView;

@end

@implementation BYGuessLikeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYCommonLiveModel *model;
    if ([object[0] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dic = object[indexPath.section];
        model = dic.allValues[0][indexPath.row];
    }else{
        model = object[indexPath.row];
    }
    if ([model isKindOfClass:[BYSearchHisModel class]]) {
        [self setSearchWithObject:(BYSearchHisModel *)model indexPath:indexPath];
        return;
    }
    self.userlogo.style = model.cert_badge;
    self.indexPath = indexPath;
    self.titleLab.text = model.live_title;
    self.userNameLab.text = model.nickname;
    self.watchNumLab.text = [NSString stringWithFormat:@"%@人看过",[NSString transformIntegerShow:model.watch_times]];
    [self.userlogo uploadHDImageWithURL:model.head_img callback:nil];
    [self.coverImgView uploadHDImageWithURL:model.live_cover_url callback:nil];
    [self reloadLiveStatus:model.status liveType:model.live_type];
    
    if (model.status == BY_LIVE_STATUS_LIVING) {
        self.hotImgView.hidden = model.watch_times >= 500 ? NO : YES;
    }else{
        self.hotImgView.hidden = model.watch_times >= 1000 ? NO : YES;
    }
}

- (void)setSearchWithObject:(BYSearchHisModel *)model indexPath:(NSIndexPath *)indexPath{
    self.indexPath = indexPath;
//    self.titleLab.text = model.live_title;
    self.userlogo.style = model.cert_badge;
    self.userNameLab.text = model.nickname;
    self.watchNumLab.text = [NSString stringWithFormat:@"%@人看过",[NSString transformIntegerShow:model.watch_times]];
    [self.userlogo uploadHDImageWithURL:model.head_img callback:nil];
    [self.coverImgView uploadHDImageWithURL:model.live_cover_url callback:nil];
    [self reloadLiveStatus:model.status liveType:model.live_type];
    
    if (model.status == BY_LIVE_STATUS_LIVING) {
        self.hotImgView.hidden = model.watch_times >= 500 ? NO : YES;
    }else{
        self.hotImgView.hidden = model.watch_times >= 1000 ? NO : YES;
    }
    self.titleLab.attributedText = model.attributedString;
}

- (void)reloadLiveStatus:(BY_LIVE_STATUS)status liveType:(BY_NEWLIVE_TYPE)liveType{
    if (liveType == BY_NEWLIVE_TYPE_VOD_AUDIO ||
        liveType == BY_NEWLIVE_TYPE_VOD_VIDEO) {
        _liveStatusBg.backgroundColor = kColorRGBValue(0x00cc18);
        _liveStatusLab.text = liveType == BY_NEWLIVE_TYPE_VOD_AUDIO ? @"音频" : @"视频";
        @weakify(self);
        [_liveStatusLab mas_updateConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.width.mas_equalTo(stringGetWidth(self.liveStatusLab.text, 11) + 10);
            make.height.mas_equalTo(stringGetHeight(self.liveStatusLab.text, 11) + 4);
        }];
        return;
    }
    switch (status) {
        case BY_LIVE_STATUS_LIVING:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xea6441);
            _liveStatusLab.text = @"直播中";
            break;
        case BY_LIVE_STATUS_SOON:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xffbe21);
            _liveStatusLab.text = @"直播预告";
            break;
        case BY_LIVE_STATUS_END:
        case BY_LIVE_STATUS_OVERTIME:
            _liveStatusBg.backgroundColor = kColorRGBValue(0x4165ea);
            _liveStatusLab.text = @"直播回放";
            break;
        default:
            break;
    }
    @weakify(self);
    [_liveStatusLab mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(stringGetWidth(self.liveStatusLab.text, 11) + 10);
        make.height.mas_equalTo(stringGetHeight(self.liveStatusLab.text, 11) + 4);
    }];
}

- (void)setContentView{
    // 封面图
    PDImageView *coverImgView = [PDImageView by_init];
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.layer.cornerRadius = 5.0f;
    coverImgView.clipsToBounds = YES;
    [self.contentView addSubview:coverImgView];
    self.coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(80);
    }];
    
    UIImageView *hotImgView = [[UIImageView alloc] init];
    [hotImgView by_setImageName:@"livehome_hotTag"];
    [self.contentView addSubview:hotImgView];
    self.hotImgView = hotImgView;
    [hotImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(coverImgView.mas_right).mas_offset(0);
        make.width.mas_equalTo(hotImgView.image.size.width);
        make.height.mas_equalTo(hotImgView.image.size.height);
    }];
    
    // 直播类型标签
    [self addLiveStatusView];
    
    // 直播标题
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    titleLab.numberOfLines = 2;
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(150);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    // 用户头像
    PDImageView *userlogo = [[PDImageView alloc] init];
    userlogo.contentMode = UIViewContentModeScaleAspectFill;
    userlogo.layer.cornerRadius = 12;
    userlogo.clipsToBounds = YES;
    [self.contentView addSubview:userlogo];
    self.userlogo = userlogo;
    [userlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(150);
        make.bottom.mas_equalTo(-12);
        make.width.mas_equalTo(24);
        make.height.mas_equalTo(24);
    }];
    
    // 用户昵称
    UILabel *userNameLab = [UILabel by_init];
    [userNameLab setBy_font:12];
    userNameLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:userNameLab];
    self.userNameLab = userNameLab;
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(8);
        make.centerY.mas_equalTo(userlogo.mas_centerY).mas_offset(0);
        make.height.mas_equalTo(userNameLab.font.pointSize);
        make.right.mas_equalTo(-85);
    }];
    
    UILabel *watchNumLab = [UILabel by_init];
    watchNumLab.textAlignment = NSTextAlignmentRight;
    [watchNumLab setBy_font:10];
    watchNumLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:watchNumLab];
    [self.contentView insertSubview:watchNumLab belowSubview:userNameLab];
    self.watchNumLab = watchNumLab;
    [watchNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(watchNumLab.font.pointSize);
        make.centerY.mas_equalTo(userlogo.mas_centerY).mas_offset(0);
        make.width.mas_equalTo(65);
    }];
}


- (void)addLiveStatusView{
    UILabel *liveStatusLab = [UILabel by_init];
    liveStatusLab.text = @"直播回放";
    [liveStatusLab setBy_font:11];
    [liveStatusLab setTextColor:[UIColor whiteColor]];
    liveStatusLab.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:liveStatusLab];
    _liveStatusLab = liveStatusLab;
    @weakify(self);
    [liveStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.coverImgView.mas_left).with.offset(8);
        make.top.equalTo(self.coverImgView.mas_top).with.offset(6);
        make.width.mas_equalTo(stringGetWidth(liveStatusLab.text, liveStatusLab.font.pointSize) + 10);
        make.height.mas_equalTo(stringGetHeight(liveStatusLab.text, liveStatusLab.font.pointSize) + 4);
    }];
    
    UIView *liveStatusBg = [UIView by_init];
    liveStatusBg.layer.cornerRadius = 2.0f;
    [self.contentView addSubview:liveStatusLab];
    [self.contentView insertSubview:liveStatusBg belowSubview:liveStatusLab];
    self.liveStatusBg = liveStatusBg;
    [liveStatusBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.coverImgView.mas_left).with.offset(8);
        make.top.equalTo(self.coverImgView.mas_top).with.offset(6);
        make.width.mas_equalTo(liveStatusLab.mas_width).mas_offset(0);
        make.height.mas_equalTo(liveStatusLab.mas_height).mas_offset(0);
    }];
}

@end
