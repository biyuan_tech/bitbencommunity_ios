//
//  BYFeaturedHeaderCell.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYFeaturedHeaderCell.h"

@interface BYFeaturedHeaderCell ()

/** 标题i */
@property (nonatomic ,strong) UILabel *titleLab;
/** rightLab */
@property (nonatomic ,strong) UILabel *rightLab;
/** rightIcon */
@property (nonatomic ,strong) UIImageView *rightIcon;
/** spaceView */
@property (nonatomic ,strong) UIView *spaceView;

@end

@implementation BYFeaturedHeaderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYCommonModel *model = dic.allValues[0][indexPath.row];
    self.titleLab.text = model.title;
    if ([model.title isEqualToString:@"视频推荐"]) {
        self.spaceView.hidden = YES;
        [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-12);
        }];
    }else{
        self.spaceView.hidden = NO;
        [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-5);
        }];
    }
}

- (void)setContentView{
    UIView *spaceView = [UIView by_init];
    [spaceView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.contentView addSubview:spaceView];
    self.spaceView = spaceView;
    [spaceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.height.mas_equalTo(8);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    titleLab.font = [UIFont boldSystemFontOfSize:16];
    titleLab.text = @"精选推荐";
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.bottom.mas_equalTo(-16);
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(titleLab.font.pointSize);
    }];
    
    UILabel *rightLab = [UILabel by_init];
    [rightLab setBy_font:12];
    rightLab.text = @"更多";
    rightLab.textColor = kColorRGBValue(0x323232);
    rightLab.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:rightLab];
    self.rightLab = rightLab;
    [rightLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-25);
        make.width.mas_equalTo(stringGetWidth(rightLab.text, 12));
        make.height.mas_equalTo(stringGetHeight(rightLab.text, 12));
        make.centerY.mas_equalTo(titleLab.mas_centerY).mas_offset(0);
    }];
    
    UIImageView *rightIcon = [UIImageView by_init];
    [rightIcon by_setImageName:@"livehome_rightIcon"];
    [self.contentView addSubview:rightIcon];
    self.rightIcon = rightIcon;
    [rightIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(rightIcon.image.size.width);
        make.height.mas_equalTo(rightIcon.image.size.height);
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(rightLab.mas_centerY).mas_offset(0);
    }];
    
}

@end
