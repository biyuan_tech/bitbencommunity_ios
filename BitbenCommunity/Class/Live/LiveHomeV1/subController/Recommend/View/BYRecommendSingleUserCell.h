//
//  BYRecommendSingleUserCell.h
//  BibenCommunity
//
//  Created by 随风 on 2019/5/29.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYRecommendUserModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYRecommendSingleUserCell : UICollectionViewCell

/** 关注按钮回调 */
@property (nonatomic ,copy) void (^attentionAction)(NSIndexPath *indexPath);


- (void)setContentWithObject:(BYRecommendUserCellModel *)object indexPath:(NSIndexPath *)indexPath;

- (void)reloadAttention;

@end

NS_ASSUME_NONNULL_END
