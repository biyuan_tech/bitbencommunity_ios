//
//  BYFeaturedSingleFooterView.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYFeaturedSingleFooterView.h"

@implementation BYFeaturedSingleFooterView

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)refreshBtnAction:(id)sender{
    [self sendActionName:@"refreshAction" param:nil indexPath:self.indexPath];
}

- (void)setContentView{
    UIButton *refreshBtn = [UIButton by_buttonWithCustomType];
    [refreshBtn setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    refreshBtn.layer.cornerRadius = 4.0f;
    [refreshBtn addTarget:self action:@selector(refreshBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:refreshBtn];
    [refreshBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(36);
        make.right.mas_equalTo(-15);
    }];
    
    UIImageView *refreshImg = [UIImageView by_init];
    [refreshImg by_setImageName:@"livehome_refresh"];
    [refreshBtn addSubview:refreshImg];
    [refreshImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.centerX.mas_equalTo(-26);
        make.width.mas_equalTo(refreshImg.image.size.width);
        make.height.mas_equalTo(refreshImg.image.size.height);
    }];
    
    UILabel *label = [UILabel by_init];
    [label setBy_font:14];
    label.textColor = kColorRGBValue(0x323232);
    label.text = @"换一批";
    [refreshBtn addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(refreshImg.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(stringGetWidth(label.text, 14));
        make.height.mas_equalTo(label.font.pointSize);
    }];
}

@end
