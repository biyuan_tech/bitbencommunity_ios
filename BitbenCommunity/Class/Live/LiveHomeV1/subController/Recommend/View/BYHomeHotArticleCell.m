//
//  BYHomeHotArticleCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeHotArticleCell.h"

#import "BYHomeArticleModel.h"

@interface BYHomeHotArticleCell ()

/** 标题 */
@property (nonatomic ,strong) UILabel *titleLab;
/** 发布时间 */
@property (nonatomic ,strong) UILabel *timeLab;
/** 封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 阅读数 */
@property (nonatomic ,strong) UILabel *watchNumLab;
/** 评论数 */
@property (nonatomic ,strong) UILabel *commentNumLab;
/** lineView */
@property (nonatomic ,strong) UIView *lineView;

@end

@implementation BYHomeHotArticleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYHomeArticleModel *model = dic.allValues[0][indexPath.row];
    self.indexPath = indexPath;
    
    self.titleLab.text = model.title.length ? model.title : nullToEmpty(model.content);
    self.timeLab.text = model.showTime;
    self.watchNumLab.text = [NSString transformIntegerShow:model.count_uv];
    self.commentNumLab.text = [NSString transformIntegerShow:model.comment_count];
    NSString *picurl = model.picture.count ? model.picture[0] : @"";
    UIImage *defultImg = [UIImage imageNamed:@"bg_article_detail_guandian_tuijian_nor"];
    [self.coverImgView uploadMainImageWithURL:picurl placeholder:defultImg imgType:PDImgTypeHD callback:nil];
    self.lineView.hidden = indexPath.row == dic.allValues.count - 1 ? YES : NO;
}

#pragma mark - configUI

- (void)setContentView{
    // 直播标题
    UILabel *titleLab = [UILabel by_init];
//    [titleLab setBy_font:14];
    titleLab.font = [UIFont boldSystemFontOfSize:14];
    titleLab.numberOfLines = 2;
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(12);
        make.right.mas_equalTo(-150);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    // 发布时间
    UILabel *timeLab  = [UILabel by_init];
    [timeLab setBy_font:12];
    timeLab.textColor = kColorRGBValue(0x8f8f8f);
    timeLab.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:timeLab];
    self.timeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.bottom.mas_equalTo(-12);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(13);
    }];
    
    // 封面图
    PDImageView *coverImgView = [PDImageView by_init];
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.layer.cornerRadius = 5.0f;
    coverImgView.clipsToBounds = YES;
    [self.contentView addSubview:coverImgView];
    self.coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(12);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(80);
    }];
    
    // 观看人数icon
    UIImageView *watchNumImg = [UIImageView by_init];
    [watchNumImg by_setImageName:@"livehome_article_watch"];
    [self.contentView addSubview:watchNumImg];
    [watchNumImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(watchNumImg.image.size.width);
        make.height.mas_equalTo(watchNumImg.image.size.height);
        make.centerY.mas_equalTo(timeLab);
        make.right.mas_equalTo(-241);
    }];
    
    // 观看人数
    UILabel *watchNumLab = [UILabel by_init];
    watchNumLab.textAlignment = NSTextAlignmentLeft;
    [watchNumLab setBy_font:12];
    watchNumLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:watchNumLab];
    self.watchNumLab = watchNumLab;
    [watchNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(watchNumImg.mas_right).mas_offset(5);
        make.height.mas_equalTo(watchNumLab.font.pointSize);
        make.centerY.mas_equalTo(timeLab);
        make.width.mas_equalTo(40);
    }];
    
    // 评论人数icon
    UIImageView *commentNumImg = [UIImageView by_init];
    [commentNumImg by_setImageName:@"livehome_article_comment"];
    [self.contentView addSubview:commentNumImg];
    [commentNumImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(commentNumImg.image.size.width);
        make.height.mas_equalTo(commentNumImg.image.size.height);
        make.centerY.mas_equalTo(timeLab);
        make.right.mas_equalTo(-182);
    }];
    
    // 评论人数
    UILabel *commentNumLab = [UILabel by_init];
//    commentNumLab.textAlignment = NSTextAlignmentRight;
    [commentNumLab setBy_font:12];
    commentNumLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:commentNumLab];
    self.commentNumLab = commentNumLab;
    [commentNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-150);
        make.height.mas_equalTo(commentNumLab.font.pointSize);
        make.centerY.mas_equalTo(timeLab);
        make.left.mas_equalTo(commentNumImg.mas_right).mas_offset(5);
    }];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    self.lineView = lineView;
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
}

@end
