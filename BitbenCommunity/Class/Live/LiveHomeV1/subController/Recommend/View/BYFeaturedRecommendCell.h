//
//  BYFeaturedRecommendCell.h
//  BibenCommunity
//
//  Created by 随风 on 2019/5/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYCommonTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYFeaturedRecommendCell : BYCommonTableViewCell

// 换一批
- (void)refreshDataAction;

@end

NS_ASSUME_NONNULL_END
