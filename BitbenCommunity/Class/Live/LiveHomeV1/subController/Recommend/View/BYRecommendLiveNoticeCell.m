//
//  BYRecommendLiveNoticeCell.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYRecommendLiveNoticeCell.h"

@interface BYRecommendLiveNoticeCell ()

/** 标题 */
@property (nonatomic ,strong) UILabel *titleLab;
/** 直播时间 */
@property (nonatomic ,strong) UILabel *timeLab;
/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogo;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 预约按钮 */
@property (nonatomic ,strong) UIButton *appointBtn;
/** 封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 直播状态 */
@property (nonatomic ,strong) UILabel *liveStatusLab;
/** 直播状态背景 */
@property (nonatomic ,strong) UIView *liveStatusBg;
/** 预约人数 */
@property (nonatomic ,strong) UILabel *peopleNum;
/** 倒计时view */
@property (nonatomic ,strong) UIView *timeView;
/** 倒计时 小时 */
@property (nonatomic ,strong) UILabel *timeHour;
/** 倒计时 分 */
@property (nonatomic ,strong) UILabel *timeMin;
/** 倒计时 秒 */
@property (nonatomic ,strong) UILabel *timeSecond;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;

@end

@implementation BYRecommendLiveNoticeCell

- (void)dealloc
{
    [[BYCommonTool shareManager] stopTimer];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYCommonLiveModel *model = dic.allValues[0][indexPath.row];
    self.userlogo.style = model.cert_badge;
    self.model = model;
    self.indexPath = indexPath;
    self.titleLab.text = model.live_title;
    self.userNameLab.text = model.nickname;
    self.timeLab.text = [NSString stringWithFormat:@"时间 ：%@",nullToEmpty(model.begin_time)];
    [self.userlogo uploadHDImageWithURL:model.head_img callback:nil];
    [self.coverImgView uploadHDImageWithURL:model.live_cover_url callback:nil];
    [self reloadLiveStatus:model.status liveType:model.live_type];
    self.timeView.hidden = model.status == BY_LIVE_STATUS_SOON ? NO : YES;
    self.appointBtn.hidden = model.status == BY_LIVE_STATUS_SOON ? NO : YES;
    
    [self reloadAppointStatus:model.attention];

    if (model.status == BY_LIVE_STATUS_SOON) {
        [self verifyLiveTime];
    }
}

- (void)verifyLiveTime{
    NSDate *date = [NSDate by_dateFromString:self.model.begin_time dateformatter:K_D_F];
    NSDate *nowDate = [NSDate by_date];
    NSComparisonResult result = [date compare:nowDate];
    if (result == NSOrderedAscending ||
        result == NSOrderedSame) {
        self.timeHour.text = @"00";
        self.timeMin.text = @"00";
        self.timeSecond.text = @"00";
        [self updateTimeMas];
        [self reloadListData];
    }
    else // 未到开播时间，倒计时自动开播
    {
        @weakify(self);
        [[BYCommonTool shareManager] timerCutdown:self.model.begin_time cb:^(NSDictionary * _Nonnull param, BOOL isFinish) {
            @strongify(self);
            if (isFinish) {
                self.timeHour.text = @"00";
                self.timeMin.text = @"00";
                self.timeSecond.text = @"00";
                [self updateTimeMas];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self reloadListData];
                });
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.timeHour.text = [NSString stringWithFormat:@"%02d",[param[@"hours"] intValue] + [param[@"days"] intValue]*24];
                    self.timeMin.text = [NSString stringWithFormat:@"%02d",[param[@"minute"] intValue]];
                    self.timeSecond.text = [NSString stringWithFormat:@"%02d",[param[@"second"] intValue]];
                    [self updateTimeMas];
                });
            }
        }];
    }
}

- (void)updateTimeMas{
    [self.timeSecond mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(self.timeSecond.text, 12));
    }];
    
    [self.timeMin mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(self.timeMin.text, 12));
    }];
    
    [self.timeHour mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(self.timeHour.text, 12));
    }];
}

- (void)reloadListData{
    [self sendActionName:@"reloadAction" param:nil indexPath:self.indexPath];
}

- (void)appointBtnAction:(UIButton *)sender{
    // 埋点
    NSDictionary *params = @{@"预约直播":self.titleLab.text.length?self.titleLab.text:@"找不到直播标题"};
    [MTAManager event:MTATypeHotLiveYuyue params:params];
    
    [self sendActionName:@"appointAction" param:@{@"btn":sender} indexPath:self.indexPath];
}

- (void)noticMoreBtnAction:(UIButton *)sender{
    
    [self sendActionName:@"noticMoreAction" param:@{@"btn":sender} indexPath:self.indexPath];
}

- (void)reloadAppointStatus:(BOOL)isAppoint{
    self.appointBtn.selected = isAppoint;
    self.appointBtn.backgroundColor = isAppoint ? kColorRGBValue(0xf2f2f2) : kColorRGB(234, 100, 65, 0.15);
}

- (void)reloadLiveStatus:(BY_LIVE_STATUS)status liveType:(BY_NEWLIVE_TYPE)liveType{
    if (liveType == BY_NEWLIVE_TYPE_VOD_AUDIO ||
        liveType == BY_NEWLIVE_TYPE_VOD_VIDEO) {
        _liveStatusBg.backgroundColor = kColorRGBValue(0x00cc18);
        _liveStatusLab.text = liveType == BY_NEWLIVE_TYPE_VOD_AUDIO ? @"音频" : @"视频";
        @weakify(self);
        [_liveStatusLab mas_updateConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.width.mas_equalTo(stringGetWidth(self.liveStatusLab.text, 11) + 10);
            make.height.mas_equalTo(stringGetHeight(self.liveStatusLab.text, 11) + 4);
        }];
        return;
    }
    switch (status) {
        case BY_LIVE_STATUS_LIVING:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xea6441);
            _liveStatusLab.text = @"直播中";
            _peopleNum.text = [NSString stringWithFormat:@"%@人在看",[NSString transformIntegerShow:self.model.watch_times]];
            break;
        case BY_LIVE_STATUS_SOON:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xffbe21);
            _liveStatusLab.text = @"直播预告";
            _peopleNum.text = [NSString stringWithFormat:@"%@人预约",[NSString transformIntegerShow:self.model.attention_count]];
            break;
        case BY_LIVE_STATUS_END:
        case BY_LIVE_STATUS_OVERTIME:
            _liveStatusBg.backgroundColor = kColorRGBValue(0x4165ea);
            _liveStatusLab.text = @"直播回放";
            _peopleNum.text = [NSString stringWithFormat:@"%@人在看",[NSString transformIntegerShow:self.model.watch_times]];
            break;
        case BY_LIVE_STATUS_NOTLIVE_ONTIME:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xf28d73);
            _liveStatusLab.text = @"暂未开播";
            _peopleNum.text = [NSString stringWithFormat:@"%@人预约",[NSString transformIntegerShow:self.model.attention_count]];
            break;
        default:
            break;
    }
    @weakify(self);
    [_liveStatusLab mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(stringGetWidth(self.liveStatusLab.text, 11) + 10);
        make.height.mas_equalTo(stringGetHeight(self.liveStatusLab.text, 11) + 4);
    }];
}

- (void)setContentView{
    UIView *spaceView = [UIView by_init];
    [spaceView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.contentView addSubview:spaceView];
    [spaceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.height.mas_equalTo(8);
    }];
    
    UILabel *label = [UILabel by_init];
    label.text = @"直播";
    label.font = [UIFont boldSystemFontOfSize:16];
    label.textColor = kColorRGBValue(0x323232);
    label.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(spaceView.mas_bottom).mas_offset(23);
        make.width.mas_equalTo(34);
        make.height.mas_equalTo(17);
    }];
    
    UIView *timeView = [self getTimeView];
    [self.contentView addSubview:timeView];
    self.timeView = timeView;
    [timeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-118);
        make.centerY.mas_equalTo(label.mas_centerY).mas_offset(0);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(20);
    }];
    
    UILabel *rightLab = [UILabel by_init];
    [rightLab setBy_font:12];
    rightLab.text = @"更多直播 · 预告";
    rightLab.textColor = kColorRGBValue(0x323232);
    rightLab.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:rightLab];
    [rightLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-25);
        make.width.mas_equalTo(stringGetWidth(rightLab.text, 12));
        make.height.mas_equalTo(stringGetHeight(rightLab.text, 12));
        make.centerY.mas_equalTo(label.mas_centerY).mas_offset(0);
    }];
    
    UIImageView *rightIcon = [UIImageView by_init];
    [rightIcon by_setImageName:@"livehome_rightIcon"];
    [self.contentView addSubview:rightIcon];
    [rightIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(rightIcon.image.size.width);
        make.height.mas_equalTo(rightIcon.image.size.height);
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(rightLab.mas_centerY).mas_offset(0);
    }];
    
    UIButton *button = [UIButton by_buttonWithCustomType];
    [button addTarget:self action:@selector(noticMoreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(8);
        make.height.mas_equalTo(40);
    }];
    
    // 直播标题
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:14];
    titleLab.numberOfLines = 2;
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(63);
        make.right.mas_equalTo(-130);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    // 直播时间
    UILabel *timeLab  = [UILabel by_init];
    [timeLab setBy_font:12];
    timeLab.textColor = kColorRGBValue(0x8f8f8f);
    timeLab.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:timeLab];
    self.timeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(112);
        make.width.mas_equalTo(188);
        make.height.mas_equalTo(13);
    }];
    
    // 用户头像
    PDImageView *userlogo = [[PDImageView alloc] init];
    userlogo.contentMode = UIViewContentModeScaleAspectFill;
    userlogo.layer.cornerRadius = 12;
    userlogo.clipsToBounds = YES;
    [self.contentView addSubview:userlogo];
    self.userlogo = userlogo;
    [userlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.bottom.mas_equalTo(-15);
        make.width.mas_equalTo(24);
        make.height.mas_equalTo(24);
    }];
    
    // 用户昵称
    UILabel *userNameLab = [UILabel by_init];
    [userNameLab setBy_font:12];
    userNameLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:userNameLab];
    self.userNameLab = userNameLab;
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(8);
        make.centerY.mas_equalTo(userlogo.mas_centerY).mas_offset(0);
        make.height.mas_equalTo(14);
        make.right.mas_equalTo(-188);
    }];
    
    // 关注按钮
    UIButton *appointBtn = [UIButton by_buttonWithCustomType];
    [appointBtn setBy_attributedTitle:@{@"title":@"立即预约",
                                                  NSForegroundColorAttributeName:kColorRGBValue(0xea6441),
                                                  NSFontAttributeName:[UIFont systemFontOfSize:11],
                                                  } forState:UIControlStateNormal];
    [appointBtn setBy_attributedTitle:@{@"title":@"已预约",
                                                  NSForegroundColorAttributeName:kColorRGBValue(0x7c7b7b),
                                                  NSFontAttributeName:[UIFont systemFontOfSize:11],
                                                  } forState:UIControlStateSelected];
    [appointBtn addTarget:self
                             action:@selector(appointBtnAction:)
                   forControlEvents:UIControlEventTouchUpInside];
    appointBtn.layer.cornerRadius = 4.0f;
    [self.contentView addSubview:appointBtn];
    self.appointBtn = appointBtn;
    [appointBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(55);
        make.height.mas_equalTo(22);
        make.right.mas_equalTo(-130);
        make.centerY.mas_equalTo(userlogo.mas_centerY).mas_offset(0);
    }];
    [self reloadAppointStatus:NO];
    
    // 封面图
    PDImageView *coverImgView = [PDImageView by_init];
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.layer.cornerRadius = 5.0f;
    coverImgView.clipsToBounds = YES;
    [self.contentView addSubview:coverImgView];
    self.coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(64);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(93);
    }];
   
    [self addLiveStatusView];
    
    UIView *maskView= [UIView by_init];
    [maskView setBackgroundColor:kColorRGB(47, 47, 47, 0.6)];
    [maskView layerCornerRadius:5 byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight size:CGSizeMake(100, 20)];
    [self.contentView addSubview:maskView];
    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(coverImgView.mas_bottom).mas_offset(0);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(20);
    }];
    
    UILabel *peopleNum = [UILabel by_init];
    peopleNum.textColor = kColorRGBValue(0xffffff);
    [peopleNum setBy_font:11];
    peopleNum.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:peopleNum];
    self.peopleNum = peopleNum;
    [peopleNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(coverImgView.mas_bottom).mas_offset(0);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(20);
    }];
}

// 倒计时View
- (UIView *)getTimeView{
    UIView *timeView = [UIView by_init];
    
    for (int i = 0; i < 3; i ++) {
        UILabel *timeLab = [UILabel by_init];
        timeLab.textColor = [UIColor whiteColor];
        [timeLab setBy_font:11];
        timeLab.backgroundColor = [UIColor clearColor];
        timeLab.textAlignment = NSTextAlignmentCenter;
        [timeView addSubview:timeLab];
        [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_greaterThanOrEqualTo(16);
            make.height.mas_equalTo(20);
            if (i == 0) {
                make.right.mas_equalTo(0);
            }else if (i == 1){
                make.right.mas_equalTo(self.timeSecond.mas_left).mas_offset(-8);
            }else{
                make.right.mas_equalTo(self.timeMin.mas_left).mas_offset(-8);
            }
//            make.left.mas_equalTo(24*i);
            make.top.mas_equalTo(0);
        }];
        
        UIImageView *timeBg = [UIImageView by_init];
        [timeBg setImage:[[UIImage imageNamed:@"livehome_timeBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(2, 2, 2, 2)]];
//        [timeBg by_setImageName:@"livehome_timeBg"];
        [timeView addSubview:timeBg];
        [timeView insertSubview:timeBg belowSubview:timeLab];
        [timeBg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(timeLab).mas_offset(4);
            make.height.mas_equalTo(timeBg.image.size.height);
            make.centerY.mas_equalTo(timeLab.mas_centerY).mas_offset(0);
            make.centerX.mas_equalTo(timeLab.mas_centerX).mas_offset(0);
        }];
        
        switch (i) {
            case 0:
                self.timeSecond = timeLab;
                break;
            case 1:
                self.timeMin = timeLab;
                break;
            default:
                self.timeHour = timeLab;
                break;
        }
        
        if (i != 2) {
            UILabel *commaLab = [UILabel by_init];
            commaLab.textColor = kColorRGBValue(0x323232);
            [commaLab setBy_font:12];
            commaLab.text = @":";
            commaLab.textAlignment = NSTextAlignmentCenter;
            [timeView addSubview:commaLab];
            [commaLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(8);
                make.height.mas_equalTo(20);
//                make.left.mas_equalTo(16 + 24*i);
                if (i == 0) {
                    make.right.mas_equalTo(self.timeSecond.mas_left).mas_offset(0);
                }else{
                    make.right.mas_equalTo(self.timeMin.mas_left).mas_offset(0);
                }
                make.top.mas_equalTo(0);
            }];
        }
    }
    
    for (int i = 0; i < 2; i ++) {
        
    }
    
    return timeView;
}

- (void)addLiveStatusView{
    UILabel *liveStatusLab = [UILabel by_init];
    liveStatusLab.text = @"直播回放";
    [liveStatusLab setBy_font:11];
    [liveStatusLab setTextColor:[UIColor whiteColor]];
    liveStatusLab.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:liveStatusLab];
    _liveStatusLab = liveStatusLab;
    @weakify(self);
    [liveStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.coverImgView.mas_left).with.offset(8);
        make.top.equalTo(self.coverImgView.mas_top).with.offset(6);
        make.width.mas_equalTo(stringGetWidth(liveStatusLab.text, liveStatusLab.font.pointSize) + 10);
        make.height.mas_equalTo(stringGetHeight(liveStatusLab.text, liveStatusLab.font.pointSize) + 4);
    }];
    
    UIView *liveStatusBg = [UIView by_init];
    liveStatusBg.layer.cornerRadius = 2.0f;
    [self.contentView addSubview:liveStatusLab];
    [self.contentView insertSubview:liveStatusBg belowSubview:liveStatusLab];
    self.liveStatusBg = liveStatusBg;
    [liveStatusBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.coverImgView.mas_left).with.offset(8);
        make.top.equalTo(self.coverImgView.mas_top).with.offset(6);
        make.width.mas_equalTo(liveStatusLab.mas_width).mas_offset(0);
        make.height.mas_equalTo(liveStatusLab.mas_height).mas_offset(0);
    }];
}

@end
