//
//  BYHomeLiveTopCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeLiveTopCell.h"
#import "BYFeaturedMoreHeaderSingleCell.h"
#import "BYHomeLiveTopView.h"

#import "BYHomeArticleModel.h"

@interface BYHomeLiveTopCell ()

@property (nonatomic ,strong) BYHomeSectionModel *model;
/** topView */
@property (nonatomic ,strong) BYHomeLiveTopView *topView;

@end

@implementation BYHomeLiveTopCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYHomeSectionModel *model = dic.allValues[0][indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    [self.topView reloadData:model.cellData];
}

#pragma mark - configUI
- (void)setContentView{
    BYHomeLiveTopView *topView = [[BYHomeLiveTopView alloc] init];
    [self.contentView addSubview:topView];
    self.topView = topView;
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

@end
