//
//  BYHomeLiveCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeLiveCell.h"

#import "BYPersonHomeController.h"


@interface BYHomeLiveCell ()
/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogo;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 标题 */
@property (nonatomic ,strong) UILabel *titleLab;
/** 内容 */
@property (nonatomic ,strong) UILabel *contentLab;
/** 发布时间 */
@property (nonatomic ,strong) UILabel *timeLab;
/** 热度标签 */
@property (nonatomic ,strong) UIImageView *hotImgView;
/** 关注状态 */
@property (nonatomic ,strong) UIButton *attentionBtn;
/** 直播封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 直播状态 */
@property (nonatomic ,strong) UILabel *liveStatusLab;
/** 直播状态背景 */
@property (nonatomic ,strong) UIView *liveStatusBg;
/** 观看人数 */
@property (nonatomic ,strong) UILabel *watchNumLab;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 承载成就图标 */
@property (nonatomic ,strong) UIView *achievementView;
@end

@implementation BYHomeLiveCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYCommonLiveModel *model = dic.allValues[0][indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    self.userlogo.style = model.cert_badge;
    self.userNameLab.text = model.nickname;
    self.timeLab.text = model.begin_time;
    self.titleLab.text = nullToEmpty(model.live_title);
    [self.userlogo uploadHDImageWithURL:model.head_img callback:nil];
    [self.coverImgView uploadHDImageWithURL:model.live_cover_url callback:nil];
    [self reloadLiveStatus:model.status liveType:model.live_type];
    [self reloadAttentionStatus:model.isAttention];
    
    // 添加成就
    
    for (UIView *subView in self.achievementView.subviews) {
        [subView removeFromSuperview];
    }
    
    NSArray *achievementData = self.model.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [self.achievementView addSubview:imageView];
    }
}

- (void)reloadLiveStatus:(BY_LIVE_STATUS)status liveType:(BY_NEWLIVE_TYPE)liveType{
    self.watchNumLab.text = [NSString stringWithFormat:@"%@人看过",[NSString transformIntegerShow:_model.watch_times]];
    if (liveType == BY_NEWLIVE_TYPE_VOD_AUDIO ||
        liveType == BY_NEWLIVE_TYPE_VOD_VIDEO) {
        _liveStatusBg.backgroundColor = kColorRGBValue(0x00cc18);
        _liveStatusLab.text = liveType == BY_NEWLIVE_TYPE_VOD_AUDIO ? @"音频" : @"视频";
        @weakify(self);
        [_liveStatusLab mas_updateConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.width.mas_equalTo(stringGetWidth(self.liveStatusLab.text, 11) + 10);
            make.height.mas_equalTo(stringGetHeight(self.liveStatusLab.text, 11) + 4);
        }];
        return;
    }
    switch (status) {
        case BY_LIVE_STATUS_LIVING:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xea6441);
            _liveStatusLab.text = @"直播中";
            break;
        case BY_LIVE_STATUS_SOON:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xffbe21);
            _liveStatusLab.text = @"直播预告";
            self.watchNumLab.text = [NSString stringWithFormat:@"%@人预约",[NSString transformIntegerShow:_model.attention_count]];
            break;
        case BY_LIVE_STATUS_END:
        case BY_LIVE_STATUS_OVERTIME:
            _liveStatusBg.backgroundColor = kColorRGBValue(0x4165ea);
            _liveStatusLab.text = @"直播回放";
            break;
        default:
            break;
    }
    @weakify(self);
    [_liveStatusLab mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(stringGetWidth(self.liveStatusLab.text, 11) + 10);
        make.height.mas_equalTo(stringGetHeight(self.liveStatusLab.text, 11) + 4);
    }];
}

#pragma mark - action
- (void)userlogoAction{
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
    personHomeController.user_id = self.model.user_id;
    [(BYCommonViewController *)currentController authorizePush:personHomeController animation:YES];
}

// 举报
- (void)reportBtnAction:(UIButton *)sender{
    CGPoint point = [self.contentView convertPoint:sender.center toView:kCommonWindow];
    NSValue *value = [NSValue valueWithCGPoint:CGPointMake(point.x + CGRectGetWidth(sender.frame)/2, point.y + CGRectGetHeight(sender.frame)/2)];
    [self sendActionName:@"reportAction" param:@{@"point":value} indexPath:self.indexPath];
}

- (void)reloadAttentionStatus:(BOOL)isAttention{
    self.attentionBtn.selected = isAttention;
    self.attentionBtn.hidden = isAttention;
    self.attentionBtn.backgroundColor = isAttention ? kColorRGBValue(0xf2f2f2) : kColorRGBValue(0xea6441);
}

- (void)attentionBtnAction:(id)sender{
    [self sendActionName:@"attentionAction" param:nil indexPath:self.indexPath];
}

#pragma mark - configUI

- (void)setContentView{
    PDImageView *userlogo = [[PDImageView alloc] init];
    userlogo.contentMode = UIViewContentModeScaleAspectFill;
    userlogo.layer.cornerRadius = 22;
    userlogo.clipsToBounds = YES;
    userlogo.userInteractionEnabled = YES;
    @weakify(self);
    [userlogo addTapGestureRecognizer:^{
        @strongify(self);
        [self userlogoAction];
    }];
    [self.contentView addSubview:userlogo];
    self.userlogo = userlogo;
    [userlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.height.mas_equalTo(44);
        make.top.mas_equalTo(20);
    }];
    
    // 用户昵称
    UILabel *userNameLab = [UILabel by_init];
    [userNameLab setBy_font:14];
    userNameLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:userNameLab];
    self.userNameLab = userNameLab;
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(12);
        make.top.mas_equalTo(userlogo.mas_top).mas_offset(5);
        make.height.mas_equalTo(userNameLab.font.pointSize);
        make.width.mas_lessThanOrEqualTo(kCommonScreenWidth - 247);
    }];
    
    // 直播时间
    UILabel *timeLab  = [UILabel by_init];
    [timeLab setBy_font:12];
    timeLab.textColor = kColorRGBValue(0x8f8f8f);
    timeLab.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:timeLab];
    self.timeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(12);
        make.bottom.mas_equalTo(userlogo.mas_bottom).mas_offset(-5);
        make.width.mas_equalTo(188);
        make.height.mas_equalTo(timeLab.font.pointSize);
    }];
    
    self.achievementView = [[UIView alloc] init];
    [self.contentView addSubview:self.achievementView];
    [self.achievementView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.userNameLab.mas_right).mas_offset(5);
        make.centerY.equalTo(self.userNameLab);
        make.width.mas_equalTo(58);
        make.height.mas_equalTo(16);
    }];
    
    UIButton *attentionBtn = [UIButton by_buttonWithCustomType];
    [attentionBtn setBy_attributedTitle:@{@"title":@"关注",
                                          NSForegroundColorAttributeName:kColorRGBValue(0xffffff),
                                          NSFontAttributeName:[UIFont systemFontOfSize:11],
                                          } forState:UIControlStateNormal];
    [attentionBtn setBy_attributedTitle:@{@"title":@"已关注",
                                          NSForegroundColorAttributeName:kColorRGBValue(0x7c7b7b),
                                          NSFontAttributeName:[UIFont systemFontOfSize:11],
                                          } forState:UIControlStateSelected];
    [attentionBtn addTarget:self
                     action:@selector(attentionBtnAction:)
           forControlEvents:UIControlEventTouchUpInside];
    attentionBtn.layer.cornerRadius = 4.0f;
    [self.contentView addSubview:attentionBtn];
    self.attentionBtn = attentionBtn;
    [attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(55);
        make.height.mas_equalTo(22);
        make.right.mas_equalTo(-48);
        make.centerY.mas_equalTo(userlogo.mas_centerY).mas_offset(0);
    }];
    [self reloadAttentionStatus:NO];
    
    // 更多(举报，收藏)
    UIButton *moreBtn = [UIButton by_buttonWithCustomType];
    [moreBtn setBy_imageName:@"icon_live_more" forState:UIControlStateNormal];
    [moreBtn addTarget:self action:@selector(reportBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:moreBtn];
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(48);
        make.centerY.mas_equalTo(userlogo.mas_centerY).mas_offset(0);
        make.height.mas_equalTo(28);
        make.right.mas_equalTo(0);
    }];
    
    // 直播标题
    UILabel *titleLab = [UILabel by_init];
//    [titleLab setBy_font:17];
    [titleLab setFont:[UIFont boldSystemFontOfSize:17]];
    titleLab.numberOfLines = 2;
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(userlogo.mas_bottom).mas_offset(15);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    // 封面图
    PDImageView *coverImgView = [PDImageView by_init];
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.clipsToBounds = YES;
    [self.contentView addSubview:coverImgView];
    self.coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(12);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(ceil((kCommonScreenWidth - 30)*190.0/345.0));
    }];
    
    UIView *coverBgView = [UIView by_init];
    [coverBgView setBackgroundColor:kColorRGB(0, 0, 0, 0.3)];
    [coverImgView addSubview:coverBgView];
    [coverBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    UILabel *watchNumLab = [UILabel by_init];
    watchNumLab.textAlignment = NSTextAlignmentRight;
    [watchNumLab setBy_font:10];
    watchNumLab.textColor = kColorRGBValue(0xffffff);
    [coverImgView addSubview:watchNumLab];
    self.watchNumLab = watchNumLab;
    [watchNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-9);
        make.height.mas_equalTo(watchNumLab.font.pointSize);
        make.top.mas_equalTo(14);
        make.width.mas_equalTo(80);
    }];
    
    // 直播类型标签
    [self addLiveStatusView];
    
    UIImageView *playImgView = [UIImageView by_init];
    [playImgView by_setImageName:@"livehome_play"];
    [coverImgView addSubview:playImgView];
    [playImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(0);
        make.width.mas_equalTo(playImgView.image.size.width);
        make.height.mas_equalTo(playImgView.image.size.height);
    }];
    
    UIImageView *hotImgView = [[UIImageView alloc] init];
    [hotImgView by_setImageName:@"livehome_hotTag"];
    [self.contentView addSubview:hotImgView];
    hotImgView.hidden = YES;
    self.hotImgView = hotImgView;
    [hotImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.mas_equalTo(0);
        make.width.mas_equalTo(hotImgView.image.size.width);
        make.height.mas_equalTo(hotImgView.image.size.height);
    }];
}

- (void)addLiveStatusView{
    UILabel *liveStatusLab = [UILabel by_init];
    liveStatusLab.text = @"直播回放";
    [liveStatusLab setBy_font:11];
    [liveStatusLab setTextColor:[UIColor whiteColor]];
    liveStatusLab.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:liveStatusLab];
    _liveStatusLab = liveStatusLab;
    @weakify(self);
    [liveStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.coverImgView.mas_left).with.offset(8);
        make.top.equalTo(self.coverImgView.mas_top).with.offset(6);
        make.width.mas_equalTo(stringGetWidth(liveStatusLab.text, liveStatusLab.font.pointSize) + 10);
        make.height.mas_equalTo(stringGetHeight(liveStatusLab.text, liveStatusLab.font.pointSize) + 4);
    }];
    
    UIView *liveStatusBg = [UIView by_init];
    liveStatusBg.layer.cornerRadius = 2.0f;
    [self.contentView addSubview:liveStatusLab];
    [self.contentView insertSubview:liveStatusBg belowSubview:liveStatusLab];
    self.liveStatusBg = liveStatusBg;
    [liveStatusBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.coverImgView.mas_left).with.offset(8);
        make.top.equalTo(self.coverImgView.mas_top).with.offset(6);
        make.width.mas_equalTo(liveStatusLab.mas_width).mas_offset(0);
        make.height.mas_equalTo(liveStatusLab.mas_height).mas_offset(0);
    }];

}

@end
