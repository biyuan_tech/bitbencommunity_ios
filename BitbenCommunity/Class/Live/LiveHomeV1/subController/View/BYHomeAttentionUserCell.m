//
//  BYHomeAttentionUserCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeAttentionUserCell.h"

#import "BYPersonHomeController.h"

#import "BYPersonHomeHeaderModel.h"

@interface BYHomeAttentionUserCell ()

/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogo;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 粉丝数 */
@property (nonatomic ,strong) UILabel *fansNumLab;
/** 关注状态 */
@property (nonatomic ,strong) UIButton *attentionBtn;
/** model */
@property (nonatomic ,strong) BYPersonHomeHeaderModel *model;
/** 简介 */
@property (nonatomic ,strong) UILabel *instroLab;
/** 承载成就图标 */
@property (nonatomic ,strong) UIView *achievementView;
@end

@implementation BYHomeAttentionUserCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYPersonHomeHeaderModel *model = dic.allValues[0][indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    self.userlogo.style = model.cert_badge;
    [self.userlogo uploadHDImageWithURL:model.head_img callback:nil];
    self.userNameLab.text = model.nickname;
    self.fansNumLab.text = [NSString stringWithFormat:@"%i粉丝",[model.fans intValue]];
    self.instroLab.text = [nullToEmpty(model.self_introduction) formatIntro];
    [self reloadAttentionStatus:model.isAttention];
    if (model.attributedString) {
        self.userNameLab.attributedText = model.attributedString;
    }
    self.fansNumLab.hidden = model.showIntro;
    self.instroLab.hidden = !model.showIntro;
    
    // 添加成就
    for (UIView *subView in self.achievementView.subviews) {
        [subView removeFromSuperview];
    }
    
    NSArray *achievementData = model.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [self.achievementView addSubview:imageView];
    }
}

#pragma mark - action
- (void)userlogoAction{
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
    personHomeController.user_id = self.model.user_id;
    [(BYCommonViewController *)currentController authorizePush:personHomeController animation:YES];
}

- (void)reloadAttentionStatus:(BOOL)isAttention{
    self.attentionBtn.selected = isAttention;
    self.attentionBtn.backgroundColor = isAttention ? kColorRGBValue(0xf2f2f2) : kColorRGBValue(0xea6441);
}

- (void)attentionBtnAction:(id)sender{
    [self sendActionName:@"user_AttentionAction" param:nil indexPath:self.indexPath];
}

#pragma mark - configUI

- (void)setContentView{
    PDImageView *userlogo = [[PDImageView alloc] init];
    userlogo.contentMode = UIViewContentModeScaleAspectFill;
    userlogo.layer.cornerRadius = 20;
    userlogo.clipsToBounds = YES;
    userlogo.userInteractionEnabled = YES;
    @weakify(self);
    [userlogo addTapGestureRecognizer:^{
        @strongify(self);
        [self userlogoAction];
    }];
    [self.contentView addSubview:userlogo];
    self.userlogo = userlogo;
    [userlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.height.mas_equalTo(40);
        make.top.mas_equalTo(12);
    }];
    
    // 用户昵称
    UILabel *userNameLab = [UILabel by_init];
    [userNameLab setBy_font:14];
    userNameLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:userNameLab];
    self.userNameLab = userNameLab;
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(10);
        make.top.mas_equalTo(userlogo.mas_top).mas_offset(5);
        make.height.mas_equalTo(userNameLab.font.pointSize);
        make.width.mas_lessThanOrEqualTo(kCommonScreenWidth - 193);
    }];
    
    // 承载成就view
    self.achievementView = [[UIView alloc] init];
    [self.contentView addSubview:self.achievementView];
    [self.achievementView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userNameLab.mas_right).mas_offset(5);
        make.centerY.equalTo(userNameLab);
        make.width.mas_equalTo(58);
        make.height.mas_equalTo(16);
    }];
    
    // 粉丝数
    UILabel *fansNumLab = [UILabel by_init];
    [fansNumLab setBy_font:12];
    fansNumLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:fansNumLab];
    self.fansNumLab = fansNumLab;
    [fansNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(10);
        make.bottom.mas_equalTo(userlogo.mas_bottom).mas_offset(-5);
        make.height.mas_equalTo(fansNumLab.font.pointSize);
        make.right.mas_equalTo(-110);
    }];
    
    // 简介
    UILabel *introlLab = [UILabel by_init];
    [introlLab setBy_font:12];
    introlLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:introlLab];
    self.instroLab = introlLab;
    [introlLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(10);
        make.bottom.mas_equalTo(userlogo.mas_bottom).mas_offset(-5);
        make.height.mas_equalTo(fansNumLab.font.pointSize);
        make.right.mas_equalTo(-110);
    }];
    
    // 关注按钮
    UIButton *attentionBtn = [UIButton by_buttonWithCustomType];
    [attentionBtn setBy_attributedTitle:@{@"title":@"关注",
                                          NSForegroundColorAttributeName:kColorRGBValue(0xffffff),
                                          NSFontAttributeName:[UIFont systemFontOfSize:11],
                                          } forState:UIControlStateNormal];
    [attentionBtn setBy_attributedTitle:@{@"title":@"已关注",
                                          NSForegroundColorAttributeName:kColorRGBValue(0x7c7b7b),
                                          NSFontAttributeName:[UIFont systemFontOfSize:11],
                                          } forState:UIControlStateSelected];
    [attentionBtn addTarget:self
                     action:@selector(attentionBtnAction:)
           forControlEvents:UIControlEventTouchUpInside];
    attentionBtn.layer.cornerRadius = 4.0f;
    [self.contentView addSubview:attentionBtn];
    self.attentionBtn = attentionBtn;
    [attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(55);
        make.height.mas_equalTo(22);
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(userlogo.mas_centerY).mas_offset(0);
    }];
    [self reloadAttentionStatus:NO];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(0);
    }];
}

@end
