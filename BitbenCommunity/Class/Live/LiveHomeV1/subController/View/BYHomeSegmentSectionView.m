//
//  BYHomeSegmentSectionView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeSegmentSectionView.h"

@implementation BYHomeSegmentSectionView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setContentView];
    }
    return self;
}

- (void)reloadSortType:(NSInteger)index{
    for (int i = 0; i < 2; i ++) {
        UIButton *btn = (UIButton *)[self viewWithTag:244 + i];
        btn.selected = i == index ? YES : NO;
    }
}

- (void)replaceBottomLine{
    UIView *lineView = [UIView new];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)btnAction:(UIButton *)sender{
    NSInteger index = sender.tag - 244;
    if (self.didSelectIndex) {
        self.didSelectIndex(index);
    }
}

- (void)setContentView{
    NSArray *titles = @[@"新鲜",@"热门"];
    for (int i = 0; i < 2; i ++) {
        UIButton *btn = [UIButton by_buttonWithCustomType];
        [btn setTitle:titles[i] forState:UIControlStateNormal];
        [btn setBackgroundColor:[UIColor whiteColor]];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn setTitleColor:kColorRGBValue(0x8f8f8f) forState:UIControlStateNormal];
        [btn setTitleColor:kColorRGBValue(0x323232) forState:UIControlStateSelected];
        btn.selected = i == 0 ? YES : NO;
        [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 244 + i;
        [self addSubview:btn];
        [self insertSubview:btn atIndex:0];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(kCommonScreenWidth/2);
            make.height.mas_equalTo(42);
            make.left.mas_equalTo(kCommonScreenWidth*i/2);
            make.top.mas_equalTo(0);
        }];
        
        
        UIView *lineView = [[UIView alloc] init];
        [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
        [self addSubview:lineView];
        if (i == 0) {
            [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.top.mas_equalTo(0);
                make.height.mas_equalTo(0.5);
            }];
        }else{
            [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.top.mas_equalTo(0);
                make.height.mas_equalTo(42);
                make.width.mas_equalTo(0.5);
            }];
        }
        
    }
}
@end
