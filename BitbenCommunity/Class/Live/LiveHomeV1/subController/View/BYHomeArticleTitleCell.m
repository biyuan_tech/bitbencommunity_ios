//
//  BYHomeArticleTitleCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeArticleTitleCell.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

#import "BYPersonHomeController.h"

#import "BYHomeArticleModel.h"

@interface BYHomeArticleTitleCell ()
/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogo;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 标题 */
@property (nonatomic ,strong) UILabel *titleLab;
/** 内容 */
@property (nonatomic ,strong) YYLabel *contentLab;
/** 发布时间 */
@property (nonatomic ,strong) UILabel *timeLab;
/** 热度标签 */
@property (nonatomic ,strong) UIImageView *hotImgView;
/** 关注状态 */
@property (nonatomic ,strong) UIButton *attentionBtn;
/** model */
@property (nonatomic ,strong) BYHomeArticleModel *model;
/** 承载成就图标 */
@property (nonatomic ,strong) UIView *achievementView;
/** 原创标签 */
@property (nonatomic ,strong) UIImageView *originalImgView;

@end

@implementation BYHomeArticleTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYHomeArticleModel *model = dic.allValues[0][indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    self.userlogo.style = model.cert_badge;
    [self.userlogo uploadHDImageWithURL:model.head_img callback:nil];
    self.userNameLab.text = model.nickname;
    if (model.home_article_type == BY_HOME_ARTICLE_TYPE_ANALYST) {
        if (model.analyst_remark.length) {
            self.timeLab.text = [NSString stringWithFormat:@"%@ · %@",model.showTime,model.analyst_remark];
        }else{
            self.timeLab.text = model.showTime;
        }
    }else{
        self.timeLab.text = model.showTime;
    }
    self.titleLab.text = nullToEmpty(model.title);
//    self.contentLab.text = nullToEmpty(model.subtitle);
    self.originalImgView.hidden = !model.is_author;
    
    NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.subtitle)];
    messageAttributed.yy_lineSpacing = 8.0f;
    messageAttributed.yy_font = [UIFont systemFontOfSize:15];
    messageAttributed.yy_color = kColorRGBValue(0x323232);
    self.contentLab.attributedText = messageAttributed;

    [self.contentLab mas_updateConstraints:^(MASConstraintMaker *make) {
        if (!model.title.length) {
            make.top.mas_equalTo(self.titleLab.mas_bottom).mas_offset(0);
        }else{
            make.top.mas_equalTo(self.titleLab.mas_bottom).mas_offset(12);
        }
        make.height.mas_equalTo(model.contentSize.height);
    }];
    
    // 添加成就
    
    for (UIView *subView in self.achievementView.subviews) {
        [subView removeFromSuperview];
    }
    
    NSArray *achievementData = self.model.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [self.achievementView addSubview:imageView];
    }
    
    [self reloadAttentionStatus:model.isAttention];
}


#pragma mark - action
- (void)userlogoAction{
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    if ([currentController isKindOfClass:[BYPersonHomeController class]]) {
        return;
    }
    BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
    personHomeController.user_id = self.model.author_id;
    [(AbstractViewController *)currentController authorizeWithCompletionHandler:^(BOOL successed) {
        if (!successed) return ;
        [currentController.navigationController pushViewController:personHomeController animated:YES];
    }];
}

- (void)reloadAttentionStatus:(BOOL)isAttention{
    self.attentionBtn.selected = isAttention;
    self.attentionBtn.hidden = isAttention;
    self.attentionBtn.backgroundColor = isAttention ? kColorRGBValue(0xf2f2f2) : kColorRGBValue(0xea6441);
    if (self.model.hiddenAttentionBtn) {
        self.attentionBtn.hidden = YES;
    }
}

- (void)attentionBtnAction:(id)sender{
    [self sendActionName:@"attentionAction" param:nil indexPath:self.indexPath];
}

// 举报
- (void)reportBtnAction:(UIButton *)sender{
    CGPoint point = [self.contentView convertPoint:sender.center toView:kCommonWindow];
    NSValue *value = [NSValue valueWithCGPoint:CGPointMake(point.x + CGRectGetWidth(sender.frame)/2, point.y + CGRectGetHeight(sender.frame)/2)];
    [self sendActionName:@"reportAction" param:@{@"point":value} indexPath:self.indexPath];
}

#pragma mark - configUI
- (void)setContentView{
    PDImageView *userlogo = [[PDImageView alloc] init];
    userlogo.contentMode = UIViewContentModeScaleAspectFill;
    userlogo.layer.cornerRadius = 22;
    userlogo.clipsToBounds = YES;
    userlogo.userInteractionEnabled = YES;
    @weakify(self);
    [userlogo addTapGestureRecognizer:^{
        @strongify(self);
        [self userlogoAction];
    }];
    [self.contentView addSubview:userlogo];
    self.userlogo = userlogo;
    [userlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.width.height.mas_equalTo(44);
        make.top.mas_equalTo(20);
    }];
    
    // 用户昵称
    UILabel *userNameLab = [UILabel by_init];
    [userNameLab setBy_font:14];
    userNameLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:userNameLab];
    self.userNameLab = userNameLab;
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(12);
        make.top.mas_equalTo(userlogo.mas_top).mas_offset(5);
        make.height.mas_equalTo(userNameLab.font.pointSize);
        make.width.mas_lessThanOrEqualTo(kCommonScreenWidth - 247);
    }];
    
    // 发布时间
    UILabel *timeLab  = [UILabel by_init];
    [timeLab setBy_font:12];
    timeLab.textColor = kColorRGBValue(0x8f8f8f);
    timeLab.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:timeLab];
    self.timeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(12);
        make.bottom.mas_equalTo(userlogo.mas_bottom).mas_offset(-5);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(timeLab.font.pointSize);
    }];
    
    // 原创标签
    self.originalImgView = [UIImageView by_init];
    [self.originalImgView by_setImageName:@"livehome_article_original"];
    [self.contentView addSubview:self.originalImgView];
    [self.originalImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(timeLab.mas_right).mas_offset(8);
        make.centerY.mas_equalTo(timeLab).mas_offset(0);
        make.width.mas_equalTo(24);
        make.height.mas_equalTo(14);
    }];
    
    // 承载成就
    self.achievementView = [[UIView alloc] init];
    [self.contentView addSubview:self.achievementView];
    [self.achievementView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.userNameLab.mas_right).mas_offset(5);
        make.centerY.equalTo(self.userNameLab);
        make.width.mas_equalTo(58);
        make.height.mas_equalTo(16);
    }];
    
    UIButton *attentionBtn = [UIButton by_buttonWithCustomType];
    [attentionBtn setBy_attributedTitle:@{@"title":@"关注",
                                          NSForegroundColorAttributeName:kColorRGBValue(0xffffff),
                                          NSFontAttributeName:[UIFont systemFontOfSize:11],
                                          } forState:UIControlStateNormal];
    [attentionBtn setBy_attributedTitle:@{@"title":@"已关注",
                                          NSForegroundColorAttributeName:kColorRGBValue(0x7c7b7b),
                                          NSFontAttributeName:[UIFont systemFontOfSize:11],
                                          } forState:UIControlStateSelected];
    [attentionBtn addTarget:self
                     action:@selector(attentionBtnAction:)
           forControlEvents:UIControlEventTouchUpInside];
    attentionBtn.layer.cornerRadius = 4.0f;
    [self.contentView addSubview:attentionBtn];
    self.attentionBtn = attentionBtn;
    [attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(55);
        make.height.mas_equalTo(22);
        make.right.mas_equalTo(-48);
        make.centerY.mas_equalTo(userlogo.mas_centerY).mas_offset(0);
    }];
    [self reloadAttentionStatus:NO];
    
    // 更多(举报，收藏)
    UIButton *moreBtn = [UIButton by_buttonWithCustomType];
    [moreBtn setBy_imageName:@"icon_live_more" forState:UIControlStateNormal];
    [moreBtn addTarget:self action:@selector(reportBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:moreBtn];
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(48);
        make.centerY.mas_equalTo(userlogo.mas_centerY).mas_offset(0);
        make.height.mas_equalTo(28);
        make.right.mas_equalTo(0);
    }];
    
    // 文章标题
    UILabel *titleLab = [UILabel by_init];
    [titleLab setFont:[UIFont boldSystemFontOfSize:17]];
    titleLab.numberOfLines = 2;
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(userlogo.mas_bottom).mas_offset(15);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    // 文章内容
    YYLabel *contentLab = [YYLabel by_init];
    contentLab.font = [UIFont systemFontOfSize:15];
    contentLab.numberOfLines = 4;
    contentLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:contentLab];
    self.contentLab = contentLab;
    [contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(12);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    UIImageView *hotImgView = [[UIImageView alloc] init];
    [hotImgView by_setImageName:@"livehome_hotTag"];
    [self.contentView addSubview:hotImgView];
    hotImgView.hidden = YES;
    self.hotImgView = hotImgView;
    [hotImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.mas_equalTo(0);
        make.width.mas_equalTo(hotImgView.image.size.width);
        make.height.mas_equalTo(hotImgView.image.size.height);
    }];
}


@end
