//
//  BYHomeSegmentSectionView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeSegmentSectionView : UIView

/** 最新最热回调 */
@property (nonatomic ,copy) void (^didSelectIndex)(NSInteger index);

- (void)reloadSortType:(NSInteger)index;

- (void)replaceBottomLine;

@end

NS_ASSUME_NONNULL_END
