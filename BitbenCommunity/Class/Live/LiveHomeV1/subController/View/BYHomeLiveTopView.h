//
//  BYHomeLiveTopView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface BYHomeLiveTopView : UIView

- (void)reloadData:(NSArray *)data;

@end

NS_ASSUME_NONNULL_END
