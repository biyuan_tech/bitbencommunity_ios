//
//  BYHomeNoAttentionHeaderCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeNoAttentionHeaderCell.h"

@implementation BYHomeNoAttentionHeaderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentView{
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:18];
    titleLab.textColor = kColorRGBValue(0x323232);
    titleLab.text = @"您还没有关注的人哦";
    titleLab.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(30);
        make.height.mas_equalTo(titleLab.font.pointSize);
    }];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(75);
    }];
    
    UILabel *subTitleLab = [UILabel by_init];
    [subTitleLab setBy_font:16];
    subTitleLab.textColor = kColorRGBValue(0x323232);
    subTitleLab.text = @"推荐一些您可能感兴趣的人吧";
    subTitleLab.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:subTitleLab];
    [subTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(95);
        make.height.mas_equalTo(subTitleLab.font.pointSize);
    }];
    
}

@end
