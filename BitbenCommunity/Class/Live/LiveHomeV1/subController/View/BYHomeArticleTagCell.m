//
//  BYHomeArticleTagCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeArticleTagCell.h"
#import "BYHomeArticleModel.h"

static NSInteger kBaseTag = 0x644;
@interface BYHomeArticleTagCell ()
/** model */
@property (nonatomic ,strong) BYHomeArticleModel *articleModel;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *liveModel;
/** bbt奖励数 */
@property (nonatomic ,strong) UILabel *bbtNumLab;
/** 点赞按钮 */
@property (nonatomic ,strong) UIButton *likeBtn;

@end

@implementation BYHomeArticleTagCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    id model = dic.allValues[0][indexPath.row];
    self.indexPath = indexPath;
    if ([model isKindOfClass:[BYCommonLiveModel class]]) {
        BYCommonLiveModel *data = (BYCommonLiveModel *)model;
        self.liveModel = data;
        self.articleModel = nil;
        self.bbtNumLab.text = [data.reward transBBTNum];
//        if (data.status == BY_LIVE_STATUS_SOON &&
//            (data.live_type != BY_NEWLIVE_TYPE_VOD_AUDIO ||
//             data.live_type != BY_NEWLIVE_TYPE_VOD_VIDEO)) {
//                self.watchNumLab.text = [NSString stringWithFormat:@"%i人预约",(int)data.attention_count];
//            }else{
//                self.watchNumLab.text = [NSString stringWithFormat:@"%i人看过",(int)data.watch_times];
//            }
        [self addTagsView];
    }
    else if ([model isKindOfClass:[BYHomeArticleModel class]]){
        BYHomeArticleModel *data = (BYHomeArticleModel *)model;
        self.likeBtn.selected = data.isSupport;
        self.bbtNumLab.text = [data.reward transBBTNum];
        self.articleModel = model;
        self.liveModel = nil;
        if (data.home_article_type != BY_HOME_ARTICLE_TYPE_ANALYST) {
            [self addTagsView];
        }
    }

    [self setBottomData];
}

- (void)reloadAttentionAnimation{
    UIButton *btn = (UIButton *)[self viewWithTag:2*kBaseTag + 2];
    btn.selected = _articleModel ? _articleModel.isSupport : _liveModel.isSupport;
    NSString *title = _articleModel ? stringFormatInteger(_articleModel.count_support) : stringFormatInteger(_liveModel.count_support);
    [btn setBy_attributedTitle:@{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                 NSForegroundColorAttributeName:kColorRGBValue(0x323232),
                                 @"title":title
                                 } forState:UIControlStateNormal];
    [btn setBy_attributedTitle:@{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                 NSForegroundColorAttributeName:kColorRGBValue(0xea6441),
                                 @"title":title
                                 } forState:UIControlStateSelected];
    btn.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView beginAnimations:@"animation" context:nil];
    [UIView setAnimationDuration:0.2];
    btn.layer.transform = CATransform3DMakeScale(1.2, 1.1, 1);
    [UIView setAnimationDelay:0.4];
    [UIView setAnimationDuration:0.2];
    btn.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView commitAnimations];
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
}

- (void)setBottomData{
    for (int i = 0; i < 3; i ++) {
        UIButton *btn = (UIButton *)[self viewWithTag:2*kBaseTag + i];
        NSString *title;
        if (i == 0) {
            title = @"分享";
        }else if (i == 1){
            title = self.articleModel ? stringFormatInteger(self.articleModel.comment_count) : stringFormatInteger(self.liveModel.comment_count);
        }else{
            btn.selected = self.articleModel ? self.articleModel.isSupport : self.liveModel.isSupport;
            title = self.articleModel ? stringFormatInteger(self.articleModel.count_support) : stringFormatInteger(self.liveModel.count_support);
        }
        [btn setBy_attributedTitle:@{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                     NSForegroundColorAttributeName:kColorRGBValue(0x323232),
                                     @"title":title
                                     } forState:UIControlStateNormal];
        [btn setBy_attributedTitle:@{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                     NSForegroundColorAttributeName:kColorRGBValue(0xea6441),
                                     @"title":title
                                     } forState:UIControlStateSelected];
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
        btn.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
    }
}

#pragma mark - action
- (void)buttomBtnAction:(UIButton *)sender{
    NSInteger index = sender.tag - 2*kBaseTag;
    if (index == 0) {
        [self sendActionName:@"shareAction" param:nil indexPath:self.indexPath];
    }else if (index == 2){
        [self sendActionName:@"likeAction" param:@{@"btn":sender} indexPath:self.indexPath];
    }
}

- (void)tagBtnAction:(UIButton *)sender{
    [self sendActionName:@"selectTagAction" param:@{@"tag":sender.titleLabel.text} indexPath:self.indexPath];
}

#pragma mark - configUI
- (void)setContentView{
//    UILabel *watchNumLab = [UILabel by_init];
//    watchNumLab.textAlignment = NSTextAlignmentRight;
//    [watchNumLab setBy_font:10];
//    watchNumLab.textColor = kColorRGBValue(0x8f8f8f);
//    [self.contentView addSubview:watchNumLab];
//    self.watchNumLab = watchNumLab;
//    [watchNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(-15);
//        make.height.mas_equalTo(watchNumLab.font.pointSize);
//        make.top.mas_equalTo(16);
//        make.width.mas_equalTo(65);
//    }];
    
    UILabel *bbtNumLab = [UILabel by_init];
    [bbtNumLab setBy_font:12];
    bbtNumLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:bbtNumLab];
    _bbtNumLab = bbtNumLab;
    [bbtNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(15);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(16);
    }];
    
    UIImageView *bbtIcon = [UIImageView by_init];
    [bbtIcon by_setImageName:@"common_bp"];
    [self.contentView addSubview:bbtIcon];
    [bbtIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bbtNumLab.mas_left).mas_offset(-7);
        make.top.mas_equalTo(15);
        make.width.mas_equalTo(16);
        make.height.mas_equalTo(16);
    }];
    
    NSArray *imageNames = @[@"livehome_share",@"livehome_comment",@"livehome_like_normal"];
    CGFloat btnW = kCommonScreenWidth/3.0;
    for (int i = 0; i < 3; i ++) {
        UIButton *btn = [UIButton by_buttonWithCustomType];
        [btn setImage:[UIImage imageNamed:imageNames[i]] forState:UIControlStateNormal];
        [btn setTitle:@"0" forState:UIControlStateNormal];
        btn.selected = NO;
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
        btn.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
        btn.tag = 2*kBaseTag + i;
        if (i == 2) {
            [btn setImage:[UIImage imageNamed:@"livehome_like_highlight"] forState:UIControlStateSelected];
            self.likeBtn = btn;
        }
        [btn addTarget:self action:@selector(buttomBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(btnW);
            make.left.mas_equalTo(btnW*i);
            make.bottom.mas_equalTo(0);
            make.height.mas_equalTo(44);
        }];
    }
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(45);
    }];
}

- (void)addTagsView{
    NSArray *topicArr = self.articleModel ? self.articleModel.topic_content : self.liveModel.topic_content;
    for (UIView *view in self.contentView.subviews) {
        if (view.tag >= kBaseTag && view.tag < kBaseTag + 20) {
            [view removeFromSuperview];
        }
    }
    
    for (int i = 0; i < topicArr.count; i ++) {
        UIButton *tagView = [UIButton by_buttonWithCustomType];
        NSString *title = self.articleModel ? self.articleModel.topic_content[i] : self.liveModel.topic_content[i];
        [tagView setTitle:title forState:UIControlStateNormal];
        tagView.titleLabel.font = [UIFont systemFontOfSize:11];
        [tagView setTitleColor:kColorRGBValue(0x4f86cb) forState:UIControlStateNormal];
        [tagView setBackgroundColor:kColorRGBValue(0xf7fbff)];
        CGFloat width = stringGetWidth(title, 11) + 12;
        CGFloat height = stringGetHeight(title, 11) + 10;
        tagView.layer.cornerRadius = 2;
        tagView.layer.borderWidth = 0.5;
        tagView.layer.borderColor = kColorRGBValue(0x92b8e8).CGColor;
        [tagView addTarget:self action:@selector(tagBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        UIButton *lastTagView = [self viewWithTag:kBaseTag + i - 1];
        [self.contentView addSubview:tagView];
        tagView.tag = kBaseTag + i;
        [tagView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            if (lastTagView) {
                make.left.mas_equalTo(lastTagView.mas_right).mas_offset(5);
            }else{
                make.left.mas_equalTo(15);
            }
            make.top.mas_equalTo(12);
        }];
    }
}

@end
