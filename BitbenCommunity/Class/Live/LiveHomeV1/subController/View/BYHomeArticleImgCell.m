//
//  BYHomeArticleImgCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeArticleImgCell.h"
#import "BYHomeArticleModel.h"

static NSInteger kBaseTag = 0x539;
#define kImgW (kCommonScreenWidth - 36)/3.0
@interface BYHomeArticleImgCell ()
/** model */
@property (nonatomic ,strong) BYHomeArticleModel *model;

@end

@implementation BYHomeArticleImgCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.clipsToBounds = YES;
    }
    return self;
}


- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYHomeArticleModel *model = dic.allValues[0][indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    [self addArticleImgView];
}

- (void)imageTapAction:(UIGestureRecognizer *)gesture{
    UIImageView *image = (UIImageView *)gesture.view;
    self.selectImgView = (PDImageView *)image;
    NSInteger index = image.tag - kBaseTag;
    if (!image.image) {
        showToastView(@"暂无图片可加载", kCommonWindow);
        return;
    }
    [self sendActionName:@"imgAction" param:@{@"view":image,@"index":@(index)} indexPath:self.indexPath];
}

- (void)addArticleImgView{
    for (UIView *subview in self.contentView.subviews) {
        [subview removeFromSuperview];
    }
    CGFloat spaceW = (kCommonScreenWidth - kImgW*3 - 30)/2;
    for (int i = 0; i < self.model.picture.count; i ++) {
        NSInteger section = i/3;
        NSInteger row = i%3;
        NSString *url = self.model.picture[i];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        imageView.userInteractionEnabled = YES;
        [imageView uploadHDImageWithURL:url callback:nil];
        imageView.tag = kBaseTag + i;
        [self.contentView addSubview:imageView];
        
        UITapGestureRecognizer *tapGuesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapAction:)];
        [imageView addGestureRecognizer:tapGuesture];
        
        if (self.model.picture.count == 1) {
            NSArray *imgArr = [url componentsSeparatedByString:@"!!"];
            CGFloat width = 0;
            CGFloat height = 0;
            if (imgArr.count == 3){
                width = [[imgArr objectAtIndex:1] integerValue];
                height = [[imgArr objectAtIndex:2] integerValue];
                [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(15);
                    make.top.mas_equalTo(12 + (kImgW + spaceW)*section);
                    make.width.mas_equalTo(113);
                    make.height.mas_equalTo(113*height/width);
                }];
                return;
            }
            
        }
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15 + (kImgW + spaceW)*row);
            make.top.mas_equalTo(12 + (kImgW + spaceW)*section);
            make.width.height.mas_equalTo(kImgW);
        }];
    }
}

@end
