//
//  BYHomeTopicCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYHomeTopicCell.h"
#import "BYHomeTopicModel.h"

@interface BYHomeTopicCell ()

/** 封面 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 标题 */
@property (nonatomic ,strong) UILabel *titleLab;
/** 简介 */
@property (nonatomic ,strong) UILabel *introLab;


@end

@implementation BYHomeTopicCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYHomeTopicModel *model = object[indexPath.row];
    [self.coverImgView uploadHDImageWithURL:model.img callback:nil];
    self.titleLab.text = [NSString stringWithFormat:@"#%@#",nullToEmpty(model.content)];
    self.introLab.text = nullToEmpty(model.remark);
}

#pragma mark - configUI
- (void)setContentView{
    // 封面图
    self.coverImgView = [[PDImageView alloc] init];
    self.coverImgView.layer.cornerRadius = 4.0f;
    self.coverImgView.clipsToBounds = YES;
    [self.contentView addSubview:self.coverImgView];
    [self.coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(15);
        make.width.height.mas_equalTo(40);
    }];
    
    // 话题标签
    self.titleLab = [UILabel by_init];
    self.titleLab.font = [UIFont boldSystemFontOfSize:14];
    self.titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:self.titleLab];
    @weakify(self);
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.coverImgView.mas_right).mas_offset(10);
        make.top.mas_equalTo(self.coverImgView).mas_offset(4);
        make.height.mas_equalTo(self.titleLab.font.pointSize);
        make.right.mas_equalTo(-15);
    }];
    
    // 标签简介
    self.introLab = [UILabel by_init];
    [self.introLab setBy_font:12];
    self.introLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:self.introLab];
    [self.introLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.coverImgView.mas_right).mas_offset(10);
        make.height.mas_equalTo(self.introLab.font.pointSize);
        make.bottom.mas_equalTo(-14);
        make.right.mas_equalTo(-15);
    }];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
}

@end
