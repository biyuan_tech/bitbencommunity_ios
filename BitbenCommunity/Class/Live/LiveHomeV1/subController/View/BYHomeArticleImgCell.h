//
//  BYHomeArticleImgCell.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/11.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeArticleImgCell : BYCommonTableViewCell

/** 选中的图片 */
@property (nonatomic ,strong) PDImageView *selectImgView;


@end

NS_ASSUME_NONNULL_END
