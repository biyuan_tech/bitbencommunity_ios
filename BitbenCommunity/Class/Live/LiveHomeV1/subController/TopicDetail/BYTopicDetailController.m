//
//  BYTopicDetailController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYTopicDetailController.h"
#import "BYTopicDetailViewModel.h"

@interface BYTopicDetailController ()

@end

@implementation BYTopicDetailController

- (Class)getViewModelClass{
    return [BYTopicDetailViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    for (UIViewController *controller in viewControllers) {
        if ([controller isKindOfClass:[BYTopicDetailController class]]) {
            if (controller != self) {
                [self.rt_navigationController removeViewController:controller];
            }
        }
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


@end
