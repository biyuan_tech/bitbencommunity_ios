//
//  BYTopicDetailSubController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "HGPageViewController.h"
#import "BYHomeTopicModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYTopicDetailSubController : HGPageViewController

/** 排序方式 */
@property (nonatomic ,assign) BY_SORT_TYPE sort_type;
/** 话题标签 */
@property (nonatomic ,copy) NSString *topic;
/** 顶部话题详情 */
@property (nonatomic ,copy) void (^topTopicDetailHandle)(BYHomeTopicModel *model);


@end

NS_ASSUME_NONNULL_END
