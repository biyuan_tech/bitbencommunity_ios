//
//  BYTopicDetailHeaderView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYTopicDetailHeaderView.h"

@interface BYTopicDetailHeaderView ()

/** 话题 */
@property (nonatomic ,strong) UILabel *topicLab;
/** 话题简介 */
@property (nonatomic ,strong) UILabel *topicIntroLab;
/** 话题封面 */
@property (nonatomic ,strong) PDImageView *coverImgView;


@end

@implementation BYTopicDetailHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)reloadHeaderData:(BYHomeTopicModel *)model{
    self.topicLab.text = [NSString stringWithFormat:@"#%@#",nullToEmpty(model.content)];
    self.topicIntroLab.text = nullToEmpty(model.remark);
    [self.coverImgView uploadHDImageWithURL:model.img callback:nil];
}

#pragma mark - configUI

- (void)setContentView{
    // 话题
    self.topicLab = [UILabel by_init];
    self.topicLab.font = [UIFont boldSystemFontOfSize:18];
    self.topicLab.textColor = kColorRGBValue(0x000000);
    [self addSubview:self.topicLab];
    @weakify(self);
    [self.topicLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(20);
        make.width.mas_equalTo(kCommonScreenWidth/2);
        make.height.mas_equalTo(self.topicLab.font.pointSize);
    }];
    
    // 话题简介
    self.topicIntroLab = [UILabel by_init];
    [self.topicIntroLab setBy_font:14];
    self.topicIntroLab.textColor = kColorRGBValue(0x8f8f8f);
    self.topicIntroLab.numberOfLines = 2.0f;
    [self addSubview:self.topicIntroLab];
    [self.topicIntroLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.topicLab.mas_bottom).mas_offset(15);
        make.height.mas_greaterThanOrEqualTo(0);
        make.right.mas_equalTo(-135);
    }];
    
    // 话题封面
    self.coverImgView = [[PDImageView alloc] init];
    self.coverImgView.layer.cornerRadius = 4.0f;
    self.coverImgView.clipsToBounds = YES;
    [self addSubview:self.coverImgView];
    [self.coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.mas_equalTo(-15);
        make.width.height.mas_equalTo(50);
        make.top.mas_equalTo(self.topicLab);
    }];
}

@end
