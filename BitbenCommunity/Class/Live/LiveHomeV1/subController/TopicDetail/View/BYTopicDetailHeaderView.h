//
//  BYTopicDetailHeaderView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYHomeTopicModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYTopicDetailHeaderView : UIView

- (void)reloadHeaderData:(BYHomeTopicModel *)model;

@end

NS_ASSUME_NONNULL_END
