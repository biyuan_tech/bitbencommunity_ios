//
//  BYTopicDetailController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYTopicDetailController : BYCommonViewController

/** 话题标签 */
@property (nonatomic ,copy) NSString *topic;


@end

NS_ASSUME_NONNULL_END
