//
//  BYTopicDetailViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYTopicDetailViewModel.h"

#import "HGSegmentedPageViewController.h"
#import "HGCenterBaseTableView.h"
#import "BYTopicDetailHeaderView.h"
#import "BYTopicDetailSubController.h"
#import "BYTopicDetailController.h"


#define K_VC ((BYTopicDetailController *)S_VC)

@interface BYTopicDetailViewModel ()<HGSegmentedPageViewControllerDelegate, HGPageViewControllerDelegate,UITableViewDelegate,UITableViewDataSource>

/** tableView */
@property (nonatomic ,strong) HGCenterBaseTableView *tableView;
/** tableHeaderView */
@property (nonatomic ,strong) BYTopicDetailHeaderView *tableHeaderView;
/** segmentControllView */
@property (nonatomic ,strong) HGSegmentedPageViewController *segmentedPageViewController;
/** tableFooterView */
@property (nonatomic ,strong) UIView *tableFooterView;
/** 新鲜 */
@property (nonatomic ,strong) BYTopicDetailSubController *newTopicSubController;
/** 热门 */
@property (nonatomic ,strong) BYTopicDetailSubController *hotTopicSubController;
/** 是否可滑动 */
@property (nonatomic ,assign) BOOL cannotScroll;
/** titleLab */
@property (nonatomic ,strong) UILabel *titleLab;
/** topDetalView */
@property (nonatomic ,strong) UIView *topDetailView;
/** topImgView */
@property (nonatomic ,strong) PDImageView *topImgView;
/** topTitleLab */
@property (nonatomic ,strong) UILabel *topTitleLab;


@end

@implementation BYTopicDetailViewModel

- (void)setContentView{
    [self addTableView];
    [self addTitleLab];
    [self addTopDetail];
}

- (void)reloadTopDetailView:(BYHomeTopicModel *)model{
    [self.topImgView uploadHDImageWithURL:model.img callback:nil];
    self.topTitleLab.text = nullToEmpty(model.content);
}

- (void)topDetailViewAnimationHidden:(BOOL)hidden{
    if (self.cannotScroll) {
        hidden = NO;
    }
    [UIView animateWithDuration:0.1 animations:^{
        self.topDetailView.alpha = !hidden;
    }];
}

#pragma mark - BYCommonTableViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat contentOffsetY = scrollView.contentOffset.y;
    CGFloat criticalPointOffsetY = 102;
    
    if (contentOffsetY >= 60) {
        self.titleLab.alpha = 0.0f;
    }else if (contentOffsetY <= 20) {
        self.titleLab.alpha = 1.0f;
    }else{
        self.titleLab.alpha = (1 - (contentOffsetY - 20)/40);
    }
    
    [self topDetailViewAnimationHidden:contentOffsetY >= 70 ? NO : YES];
    
    //利用contentOffset处理内外层scrollView的滑动冲突问题
    if (contentOffsetY >= criticalPointOffsetY) {
        
        self.cannotScroll = YES;
        scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        [self.segmentedPageViewController.currentPageViewController makePageViewControllerScroll:YES];
    } else {
        
        if (self.cannotScroll) {
            //“维持吸顶状态”
            scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        } else {
            /* 吸顶状态 -> 不吸顶状态
             * categoryView的子控制器的tableView或collectionView在竖直方向上的contentOffsetY小于等于0时，会通过代理的方式改变当前控制器self.canScroll的值；
             */
        }
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    [self.segmentedPageViewController.currentPageViewController makePageViewControllerScrollToTop];
    return YES;
}


#pragma mark - HGSegmentedPageViewControllerDelegate
- (void)segmentedPageViewControllerWillBeginDragging {
    self.tableView.scrollEnabled = NO;
}

- (void)segmentedPageViewControllerDidEndDragging {
    self.tableView.scrollEnabled = YES;
}

#pragma mark - HGPageViewControllerDelegate
- (void)pageViewControllerLeaveTop {
    self.cannotScroll = NO;
}

#pragma mark - requset

#pragma mark - configUI

- (void)addTableView{
    HGCenterBaseTableView *tableView = [[HGCenterBaseTableView alloc] init];
    tableView.categoryViewHeight = 43.0f;
    tableView.delegate = self;
    tableView.tableHeaderView = self.tableHeaderView;
    tableView.tableFooterView = self.tableFooterView;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.showsHorizontalScrollIndicator = NO;
    [S_V_VIEW addSubview:tableView];
    self.tableView = tableView;
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (void)addTitleLab{
    self.titleLab = [UILabel by_init];
    self.titleLab.font = [UIFont boldSystemFontOfSize:16];
    self.titleLab.text = [NSString stringWithFormat:@"#%@#",K_VC.topic];
    [S_V_NC.navigationBar addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(44);
        make.centerX.mas_equalTo(15);
        make.bottom.mas_equalTo(0);
    }];
}

- (void)addTopDetail{
    self.topDetailView = [UIView by_init];
    self.topDetailView.alpha = 0.0f;
    [S_V_NC.navigationBar addSubview:self.topDetailView];
    [self.topDetailView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(44);
        make.height.mas_equalTo(44);
        make.width.mas_equalTo(150);
        make.bottom.mas_equalTo(0);
    }];
    
    PDImageView *topImgView = [[PDImageView alloc] init];
    topImgView.layer.cornerRadius = 2.0f;
    topImgView.clipsToBounds = YES;
    [self.topDetailView addSubview:topImgView];
    self.topImgView = topImgView;
    [topImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(26);
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    
    UILabel *topTitleLab = [UILabel by_init];
    topTitleLab.font = [UIFont boldSystemFontOfSize:15];
    topTitleLab.textColor = kColorRGBValue(0x000000);
    [self.topDetailView addSubview:topTitleLab];
    self.topTitleLab = topTitleLab;
    [topTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topImgView.mas_right).mas_offset(10);
        make.height.mas_equalTo(44);
        make.centerY.mas_equalTo(0);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
}

- (BYTopicDetailHeaderView *)tableHeaderView{
    if (!_tableHeaderView) {
        _tableHeaderView = [[BYTopicDetailHeaderView alloc] init];
        _tableHeaderView.frame = CGRectMake(0, 0, kCommonScreenWidth, 102);
    }
    return _tableHeaderView;
}

- (UIView *)tableFooterView{
    if (!_tableFooterView) {
        CGFloat height = kNavigationHeight;
        _tableFooterView = [[UIView alloc] init];
        _tableFooterView.frame = CGRectMake(0, 0, kCommonScreenWidth, kCommonScreenHeight - height);
        [S_VC addChildViewController:self.segmentedPageViewController];
        [S_VC addChildViewController:self.newTopicSubController];
        [S_VC addChildViewController:self.hotTopicSubController];
        [_tableFooterView addSubview:self.segmentedPageViewController.view];
        [self.segmentedPageViewController didMoveToParentViewController:S_VC];
        @weakify(self);
        [self.segmentedPageViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.edges.equalTo(self.tableFooterView);
        }];
    }
    return _tableFooterView;
}

- (HGSegmentedPageViewController *)segmentedPageViewController {
    if (!_segmentedPageViewController) {
        NSMutableArray *controllers = [NSMutableArray array];
        NSArray *titles = @[@"新鲜", @"热门"];
        for (int i = 0; i < titles.count; i++) {
            HGPageViewController *controller;
            if (i == 0) {
                controller = self.newTopicSubController;
            } else {
                controller = self.hotTopicSubController;
            }
            controller.delegate = self;
            [controllers addObject:controller];
        }
        _segmentedPageViewController = [[HGSegmentedPageViewController alloc] init];
        _segmentedPageViewController.pageViewControllers = controllers;
        _segmentedPageViewController.categoryView.titles = titles;
        _segmentedPageViewController.categoryView.height = 43;
        _segmentedPageViewController.categoryView.alignment = HGCategoryViewAlignmentLeft;
        _segmentedPageViewController.categoryView.originalIndex = 0;
        _segmentedPageViewController.categoryView.itemSpacing = 25;
        _segmentedPageViewController.categoryView.leftAndRightMargin = 15;
        _segmentedPageViewController.categoryView.titleNomalFont = [UIFont systemFontOfSize:14];
        _segmentedPageViewController.categoryView.titleSelectedFont = [UIFont boldSystemFontOfSize:16];
        _segmentedPageViewController.categoryView.titleNormalColor = kColorRGBValue(0x8f8f8f);
        _segmentedPageViewController.categoryView.titleSelectedColor = kColorRGBValue(0x323232);
        _segmentedPageViewController.categoryView.backgroundColor = [UIColor whiteColor];
        _segmentedPageViewController.categoryView.vernierHeight = 3;
        _segmentedPageViewController.categoryView.vernierWidth = 20;
        _segmentedPageViewController.categoryView.vernierBottomSapce = 5;
        _segmentedPageViewController.categoryView.vernier.backgroundColor = kColorRGBValue(0xea6441);
        _segmentedPageViewController.categoryView.topBorder.hidden = YES;
        _segmentedPageViewController.categoryView.bottomBorder.hidden = YES;
        _segmentedPageViewController.delegate = self;
    }
    return _segmentedPageViewController;
}

- (BYTopicDetailSubController *)newTopicSubController{
    if (!_newTopicSubController) {
        _newTopicSubController = [[BYTopicDetailSubController alloc] init];
        _newTopicSubController.topic = K_VC.topic;
        _newTopicSubController.sort_type = BY_SORT_TYPE_TIME;
        @weakify(self);
        _newTopicSubController.topTopicDetailHandle = ^(BYHomeTopicModel * _Nonnull model) {
            @strongify(self);
            [self.tableHeaderView reloadHeaderData:model];
            [self reloadTopDetailView:model];
        };
    }
    return _newTopicSubController;
}

- (BYTopicDetailSubController *)hotTopicSubController{
    if (!_hotTopicSubController) {
        _hotTopicSubController = [[BYTopicDetailSubController alloc] init];
        _hotTopicSubController.topic = K_VC.topic;
        _hotTopicSubController.sort_type = BY_SORT_TYPE_HOT;
    }
    return _hotTopicSubController;
}


@end
