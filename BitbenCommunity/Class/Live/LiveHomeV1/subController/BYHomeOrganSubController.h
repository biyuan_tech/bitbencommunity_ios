//
//  BYHomeOrganSubController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYHomeOrganSubController : BYCommonViewController

/** type */
@property (nonatomic ,assign) BY_HOME_ARTICLE_TYPE type;

- (void)autoReload;

- (void)reloadData;

@end

NS_ASSUME_NONNULL_END
