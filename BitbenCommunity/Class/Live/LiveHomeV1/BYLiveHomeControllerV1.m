//
//  BYLiveHomeControllerV1.m
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYLiveHomeControllerV1.h"
#import "BYLiveHomeViewModelV1.h"
#import "BYDaySignInView.h"

@interface BYLiveHomeControllerV1 ()

@end

@implementation BYLiveHomeControllerV1

+ (instancetype)shareManager{
    static BYLiveHomeControllerV1 *homeController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        homeController = [[BYLiveHomeControllerV1 alloc] init];
    });
    return homeController;
}

- (Class)getViewModelClass{
    return [BYLiveHomeViewModelV1 class];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)reloadViewInterfaceManager{
    [(BYLiveHomeViewModelV1 *)self.viewModel configUi];
    @weakify(self);
    [self actionAutoLoginBlock:NULL];
    [self loginSuccessedNotif:^(BOOL isSuccessed) {
        @strongify(self);
        [self loadRequestDaySignIn];
        [(BYLiveHomeViewModelV1 *)self.viewModel reloadCurrentView];
        if (isSuccessed){
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:kiSHaveLiveRoomKey] isEqual:@YES]) return;
            // 校验直播间
            [self verifyLiveRoom];
        } else {
            
        }
    }];
}

// 设置直播间状态
- (void)verifyLiveRoom{
    [[BYLiveHomeRequest alloc] loadReuqestIsHaveLiveRoomSuccessBlock:^(id object) {
        [[NSUserDefaults standardUserDefaults] setObject:@([object boolValue]) forKey:kiSHaveLiveRoomKey];
    } faileBlock:^(NSError *error) {
        
    }];
}


- (void)selectSegmentAtIndex:(NSInteger)index{
    if (!((BYLiveHomeViewModelV1 *)self.viewModel).segmentView) {
        return;
    }
    [((BYLiveHomeViewModelV1 *)self.viewModel).segmentView selectIndex:index];
}

- (UIViewController *)getSegmentSubControllerAtIndex:(NSInteger)index{
    return [(BYLiveHomeViewModelV1 *)self.viewModel getSegmentSubControllerAtIndex:index];
}


- (void)autoReload{
    [(BYLiveHomeViewModelV1 *)self.viewModel autoReload];
}

#pragma mark - 首页签到
- (void)loadRequestDaySignIn{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestDaySignIn:^(id object) {
        if (![object isKindOfClass:[NSDictionary class]]) return ;
        if (![object[@"is_first"] boolValue]) return;
        NSString *bbtAmount = [NSString transformIntegerShow:[object[@"bbt_total"] integerValue]];
        NSString *bpAmount = [NSString transformIntergerShowDouble:[object[@"bp_total"] integerValue]];
        [BYDaySignInView showAnimation:bbtAmount bp:bpAmount];
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"每日签到获取失败！", self.view);
    }];
}
@end
