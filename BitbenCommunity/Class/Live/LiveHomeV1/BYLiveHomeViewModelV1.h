//
//  BYLiveHomeViewModelV1.h
//  BibenCommunity
//
//  Created by 随风 on 2019/5/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYCommonViewModel.h"
#import "BYLiveHomeSegmentViewV1.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYLiveHomeViewModelV1 : BYCommonViewModel

/** 分类器 */
@property (nonatomic ,strong ,readonly) BYLiveHomeSegmentViewV1 *segmentView;


- (void)configUi;

- (void)reloadCurrentView;

- (UIViewController *)getSegmentSubControllerAtIndex:(NSInteger)index;

- (void)autoReload;

@end

NS_ASSUME_NONNULL_END
