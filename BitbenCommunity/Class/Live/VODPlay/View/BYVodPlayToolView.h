//
//  BYVodPlayToolView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYIMMessageInputView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYVodPlayToolView : UIView


@property (nonatomic ,strong ,readonly) BYIMMessageTextView *inputView;
/** 消息发送回调 */
@property (nonatomic ,copy) void (^didSendMessageHandle)(NSString *string);
///** 举报回调 */
//@property (nonatomic ,copy) void (^didReportActionHandle)(void);
/** 分享回调 */
@property (nonatomic ,copy) void (^didShareActionHandle)(void);



- (void)reloadData:(BYCommonLiveModel *)model;

@end

NS_ASSUME_NONNULL_END
