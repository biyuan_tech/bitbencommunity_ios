//
//  BYVodPlayToolView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYVodPlayToolView.h"

static NSInteger defultH = 49;
static NSInteger baseTag = 0x898;
@interface BYVodPlayToolView ()<UITextViewDelegate>

/** 输入框 */
@property (nonatomic ,strong) BYIMMessageTextView *inputView;
/** 消息发送 */
@property (nonatomic ,strong) UIButton *sendBtn;
/** 评论图标 */
@property (nonatomic ,strong) UIImageView *placeholderImgView;
/** 评论文案 */
@property (nonatomic ,strong) UILabel *placeholderLab;
/** 举报按钮 */
@property (nonatomic ,strong) UIButton *reportBtn;
@property (nonatomic ,strong) UIControl *maskView;

/** 消息触发区 */
@property (nonatomic ,strong) UIControl *msgView;
/** 消息输入view */
@property (nonatomic ,strong) UIView *msgContentView;
/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;


@end

@implementation BYVodPlayToolView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setContentView];
        [self addNSNotification];
        self.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        self.layer.shadowOpacity = .2f;
        self.layer.shadowOffset = CGSizeMake(.2f, .2f);
    }
    return self;
}


- (void)didMoveToSuperview{
    if (self.superview) {
        [self configMsgContentView];
    }
}

- (BOOL)resignFirstResponder{
    [_inputView resignFirstResponder];
    return [super resignFirstResponder];
}

- (void)resetToolView{
    [_inputView resignFirstResponder];
    _inputView.text = @"";
    self.placeholderImgView.hidden  = NO;
    self.placeholderLab.hidden      = NO;
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(kSafe_Mas_Bottom(defultH));
    }];
}

- (CGFloat)calculateTextViewHeight:(UITextView *)textView text:(NSString *)text{
    CGSize constraninSize = CGSizeMake(textView.contentSize.width, MAXFLOAT);
    CGSize size = [textView sizeThatFits:constraninSize];
    return size.height + 14 + textView.textContainerInset.top;
}

- (void)reloadData:(BYCommonLiveModel *)model{
    self.model = model;
    for (int i = 1; i < 3; i ++) {
        UIButton *button = [self viewWithTag:baseTag + i];
        if (i == 1) {
            button.selected = model.isSupport;
        }else{
            button.selected = model.collection_status;
        }
    }
}

#pragma mark - action

- (void)sendMessageAction{
    if (self.didSendMessageHandle) {
        NSString *string = [self.inputView.text removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
        self.didSendMessageHandle(string);
    }
    [self resetToolView];
}

- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - baseTag;
    switch (index) {
        case 0:
            if (self.didShareActionHandle) {
                self.didShareActionHandle();
            }
            break;
        case 1:
            [self likeAction];
            break;
        default:
            [self collectionAction];
            break;
    }
  
}

- (void)triggerMsgTextView{
    [self.inputView becomeFirstResponder];
}


- (void)likeAction{
    if (!self.model.isSupport) {
        @weakify(self);
        [[BYLiveHomeRequest alloc] loadRequestLiveGuestOpearType:BY_GUEST_OPERA_TYPE_UP theme_id:self.model.live_record_id theme_type:BY_THEME_TYPE_LIVE receiver_user_id:self.model.user_id successBlock:^(id object) {
            @strongify(self);
            self.model.isSupport = YES;
            self.model.count_support++;
            UIButton *button = [self viewWithTag:baseTag + 1];
            button.selected = self.model.isSupport;
        } faileBlock:^(NSError *error) {
            showToastView(@"顶操作失败", kCommonWindow);
        }];
    }
}

- (void)collectionAction{
    @weakify(self);
    if (!self.model.collection_status) {
        [[BYLiveHomeRequest alloc] loadRequestLiveGuestOpearType:2 theme_id:self.model.live_record_id theme_type:BY_THEME_TYPE_LIVE receiver_user_id:self.model.user_id successBlock:^(id object) {
            @strongify(self);
            self.model.collection_status = YES;
            UIButton *button = [self viewWithTag:baseTag + 2];
            button.selected = self.model.collection_status;
        } faileBlock:^(NSError *error) {
            showToastView(@"收藏操作失败", kCommonWindow);
        }];
    }else
    {
        [[BYLiveHomeRequest alloc] loadRequestDeltOpearType:2 theme_id:self.model.live_record_id theme_type:BY_THEME_TYPE_LIVE successBlock:^(id object) {
            @strongify(self);
            self.model.collection_status = NO;
            UIButton *button = [self viewWithTag:baseTag + 2];
            button.selected = self.model.collection_status;
        } faileBlock:^(NSError *error) {
            showToastView(@"收藏操作失败", kCommonWindow);
        }];
    }
  
}

#pragma mark - UITextViewDelegate 文本输入代理

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"] && textView.text.length) {
        [textView resignFirstResponder];
        [self sendMessageAction];
        return YES;
    }else if ([text isEqualToString:@"\n"]) {
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length) {
        self.placeholderImgView.hidden  = YES;
        self.placeholderLab.hidden      = YES;
    }else{
        self.placeholderImgView.hidden  = NO;
        self.placeholderLab.hidden      = NO;
    }
    CGFloat maxHeight = 70;
    CGFloat height = [self calculateTextViewHeight:textView text:textView.text];
    textView.scrollEnabled = height > maxHeight ? YES : NO;
    if (height < maxHeight ) {
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(kSafe_Mas_Bottom(height) < kSafe_Mas_Bottom(defultH) ? kSafe_Mas_Bottom(defultH) : kSafe_Mas_Bottom(height));
        }];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length) {
        self.placeholderImgView.hidden  = YES;
        self.placeholderLab.hidden      = YES;
    }else{
        self.placeholderImgView.hidden  = NO;
        self.placeholderLab.hidden      = NO;
    }
}

#pragma mark - keyBoardNSNotification

- (void)keyBoardWillShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardDidShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardWillHidden:(NSNotification *)notic{
    if (self.superview && self.inputView.isFirstResponder) {
        [UIView animateWithDuration:0.1 animations:^{
            self.maskView.hidden = YES;
            [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(70);
                make.left.right.mas_equalTo(0);
            }];
            [kCommonWindow layoutIfNeeded];
        }];
    }
}

- (void)keyBoardShowAnimation:(NSValue *)value{
    if (self.superview && self.inputView.isFirstResponder) {
        CGFloat detalY = [value CGRectValue].size.height;
        [UIView animateWithDuration:0.1 animations:^{
            self.maskView.hidden = NO;
            [self.msgContentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.right.mas_equalTo(0);
                make.bottom.mas_equalTo(-detalY);
            }];
            [kCommonWindow layoutIfNeeded];
        }];
    }
}

#pragma mark - regisetNSNotic
- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView *view = [super hitTest:point withEvent:event];
    if (view != self && view != nil) {
        if (![AccountModel sharedAccountModel].hasLoggedIn) {
            [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
            return nil;
        }
    }
    return view;
}

#pragma mark - configUI

- (void)setContentView{
    
    // 消息触发区
    UIControl *msgView = [[UIControl alloc] init];
    [msgView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    msgView.layer.cornerRadius = 4.0f;
    [msgView addTarget:self action:@selector(triggerMsgTextView) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:msgView];
    self.msgView = msgView;
    [msgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 15, kSafe_Mas_Bottom(7), 132));
    }];
    
    UIImageView *placeholderImgView = [[UIImageView alloc] init];
    [placeholderImgView by_setImageName:@"icon_article_detail_draw_input"];
    [self addSubview:placeholderImgView];
    [placeholderImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(27);
        make.centerY.mas_equalTo(self.msgView);
        make.width.mas_equalTo(ceil(placeholderImgView.image.size.width));
        make.height.mas_equalTo(ceil(placeholderImgView.image.size.height));
    }];
    
    UILabel *placeholderLab = [UILabel by_init];
    [placeholderLab setBy_font:14];
    placeholderLab.textColor = kColorRGBValue(0xb6b6b7);
    placeholderLab.text = @"写评论…";
    [self addSubview:placeholderLab];
    @weakify(self);
    [placeholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(56);
        make.centerY.mas_equalTo(self.msgView).mas_offset(0);
        make.height.mas_equalTo(placeholderLab.font.pointSize);
        make.right.mas_equalTo(self.msgView);
    }];
    
    NSArray *imageNormals = @[@"video_share",@"video_like_normal",@"video_collect_normal"];
    NSArray *imageHighlights = @[@"video_share",@"video_like_highlight",@"video_collect_highlight"];
    CGFloat width = 132/3;
    for (int i = 0; i < 3; i ++) {
        UIButton *button = [UIButton by_buttonWithCustomType];
        [button setBy_imageName:imageNormals[i] forState:UIControlStateNormal];
        [button setBy_imageName:imageHighlights[i] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        button.tag = baseTag + i;
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.msgView.mas_right).offset(width*i);
            make.height.mas_equalTo(defultH);
            make.centerY.mas_equalTo(self.msgView);
            make.width.mas_equalTo(width);
        }];
    }
}

- (void)configMsgContentView{
    
    self.maskView = [[UIControl alloc] init];
    self.maskView.hidden = YES;
    [self.maskView addTarget:self action:@selector(resignFirstResponder) forControlEvents:UIControlEventTouchUpInside];
    [kCommonWindow addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    self.msgContentView = [UIView by_init];
    [self.msgContentView setBackgroundColor:[UIColor whiteColor]];
    [kCommonWindow addSubview:self.msgContentView];
    [self.msgContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(defultH);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(defultH);
    }];
    
    self.inputView = [[BYIMMessageTextView alloc] init];
    self.inputView.layer.cornerRadius = 4.0f;
    [self.inputView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    self.inputView.tintColor = kBgColor_238;
    self.inputView.font = [UIFont systemFontOfSize:16];
    self.inputView.textContainerInset = UIEdgeInsetsMake(7, 8, 0, 5);
    self.inputView.textContainer.lineFragmentPadding = 0;
    self.inputView.returnKeyType = UIReturnKeySend;
    self.inputView.delegate = self;
    [self.msgContentView addSubview:self.inputView];
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 15, 7, 58));
    }];
    
    self.placeholderImgView = [[UIImageView alloc] init];
    [self.placeholderImgView by_setImageName:@"icon_article_detail_draw_input"];
    [self.msgContentView addSubview:self.placeholderImgView];
    [self.placeholderImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(27);
        make.centerY.mas_equalTo(self.inputView);
        make.width.mas_equalTo(ceil(self.placeholderImgView.image.size.width));
        make.height.mas_equalTo(ceil(self.placeholderImgView.image.size.height));
    }];
    
    self.placeholderLab = [UILabel by_init];
    [self.placeholderLab setBy_font:14];
    self.placeholderLab.textColor = kColorRGBValue(0xb6b6b7);
    self.placeholderLab.text = @"写评论…";
    [self.msgContentView addSubview:self.placeholderLab];
    @weakify(self);
    [self.placeholderLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.inputView).mas_offset(40);
        make.centerY.mas_equalTo(self.inputView).mas_offset(0);
        make.height.mas_equalTo(self.placeholderLab.font.pointSize);
        make.right.mas_equalTo(self.inputView);
    }];
    
    UIButton *sendBtn = [UIButton by_buttonWithCustomType];
    [sendBtn setBy_attributedTitle:@{@"title":@"发送",
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                     } forState:UIControlStateNormal];
    [sendBtn setBy_attributedTitle:@{@"title":@"发送",
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                     } forState:UIControlStateDisabled];
    [sendBtn setBackgroundColor:[UIColor whiteColor]];
    [sendBtn addTarget:self action:@selector(sendMessageAction) forControlEvents:UIControlEventTouchUpInside];
    sendBtn.layer.cornerRadius = 4.0f;
    [self.msgContentView addSubview:sendBtn];
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(58);
        make.height.mas_equalTo(defultH);
        make.centerY.mas_equalTo(self.inputView).mas_offset(0);
    }];
}

@end
