//
//  BYVODPlaySubController.h
//  BibenCommunity
//
//  Created by 随风 on 2018/10/23.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYCommonViewController.h"
#import "BYCommonPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@class BYCommonLiveModel;
@interface BYVODPlaySubController : BYCommonViewController

@property (nonatomic, strong) BYCommonLiveModel *model;
@property (nonatomic, weak) UIViewController *viewController;
@property (nonatomic ,strong ,readonly) BYCommonPlayerView *playerView;
@property (nonatomic, copy) void (^scrollViewDidScrollView)(UIScrollView *scrollView);
/** 分享回调 */
@property (nonatomic ,copy) void (^didShareActionHandle)(void);

- (void)reset;

@end

NS_ASSUME_NONNULL_END
