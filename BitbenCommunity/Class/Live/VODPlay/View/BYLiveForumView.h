//
//  BYLiveForumView.h
//  BibenCommunity
//
//  Created by Belief on 2018/10/23.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveForumRootInputView.h"

@interface BYLiveForumView : UIViewController

@property (nonatomic ,strong) LiveForumRootInputView *inputView;
@property (nonatomic,copy)NSString *transferRoomId;
/** 是否需要适配底部 */
@property (nonatomic,assign) BOOL needAdaptiveBottom;

/** 是否含举报按钮 */
@property (nonatomic ,assign) BOOL isReport;

@property (nonatomic ,weak) UIViewController *viewController;

@property (nonatomic, copy) void (^scrollViewDidScrollView)(UIScrollView *scrollView);
@property (nonatomic, copy) void (^keyboardWillShow)(void);
@property (nonatomic, copy) void (^keyboardWillHide)(void);

- (void)sendComment:(NSString *)string;

- (instancetype)initWithFatureController:(UIViewController *)viewController isReport:(BOOL)isReport;
@end
