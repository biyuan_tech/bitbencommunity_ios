//
//  BYVODPlaySubController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/10/23.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYVODPlaySubController.h"
#import "BYPersonHomeController.h"
#import "BYLiveForumView.h"
#import "BYCommonLiveModel.h"
//#import "BYILiveOpeartionView.h"
#import "BYReportSheetView.h"
#import "BYLiveReviewController.h"
#import "BYVodPlayToolView.h"

@interface BYVODPlaySubController ()<SuperPlayerDelegate>

/** 播放器 */
@property (nonatomic ,strong) BYCommonPlayerView *playerView;
/** 播放器View的父视图*/
@property (nonatomic) UIView *playerFatherView;
/** 点评 */
@property (nonatomic ,strong) BYLiveForumView *forumView;
@property (nonatomic ,strong) BYLiveReviewController *reviewView;

//@property (nonatomic ,strong) BYILiveOpeartionView *opeartionView;
/** headerView */
@property (nonatomic ,strong) UIView *headerView;

///** 举报 */
//@property (nonatomic ,strong) UIImageView *reportMarkView;
///** maskView */
//@property (nonatomic ,strong) UIView *reportMaskView;
///** reportView */
//@property (nonatomic ,strong) BYReportSheetView *reportSheetView;
/** 关注按钮 */
@property (nonatomic ,strong) UIButton *attentionBtn;
/** toolbar */
@property (nonatomic ,strong) BYVodPlayToolView *toolView;


@end

@implementation BYVODPlaySubController

- (void)dealloc
{

}

- (void)reset{
    _model = nil;
    _viewController = nil;
    _scrollViewDidScrollView = NULL;
    [_playerView resetPlayer];
    [_playerView removeFromSuperview];
    _playerView = nil;
    _playerFatherView = nil;
    [_forumView removeFromParentViewController];
    [_forumView.view removeFromSuperview];
//    [_reportMarkView removeFromSuperview];
//    [_reportMaskView removeFromSuperview];
//    [_reportSheetView removeFromSuperview];
    [_toolView removeFromSuperview];
//    [_opeartionView removeFromSuperview];
    _forumView = nil;
//    _reportMaskView = nil;
//    _reportMarkView = nil;
//    _reportSheetView = nil;
    _toolView = nil;
//    _opeartionView = nil;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_playerView pause];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_playerView resume];
}

- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    if (!model) return;
    [self configUI];
    [self reloadAttentionStatus];
}


- (void)playVideo{
//    if (!_model.courseware.count) return;
//    NSString *videoUrl = _model.courseware[0];
    NSString *videoUrl = _model.play_url.length ? _model.play_url : (_model.courseware.count > 1 ? _model.courseware[0] : @"");
    if (!videoUrl.length) return;
    [self.playerView playVideoWithUrl:videoUrl title:@""];
    [self.playerView setWatchNum:_model.watch_times];
}

- (void)reloadAttentionStatus{
    self.attentionBtn.selected = self.model.isAttention;
    UIColor *color = !_model.isAttention ? kColorRGBValue(0xea6441) : kColorRGBValue(0xffffff);
    [_attentionBtn setBackgroundColor:color];
    _attentionBtn.layer.borderColor = !_model.isAttention ? kColorRGBValue(0xea6441).CGColor : kColorRGBValue(0x9b9b9b).CGColor;
}


//// 举报
//- (void)reportAction{
//    [self showReportMarkViewHidden:NO];
//}
//
//- (void)reportBtnAction:(UIButton *)sender{
//    [self showReportMarkViewHidden:YES];
//    [self.reportSheetView showAnimation];
//}

- (void)userlogoAction{
    if (![AccountModel sharedAccountModel].hasLoggedIn) {
        [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
        return;
    }
    BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
    personHomeController.user_id = self.model.user_id;
    [self.viewController.navigationController pushViewController:personHomeController animated:YES];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if ([_forumView.inputView.inputView isFirstResponder]) {
        [_forumView.inputView.inputView resignFirstResponder];
    }
}

#pragma mark - request
- (void)attentionBtnAction{
    if (![AccountModel sharedAccountModel].hasLoggedIn) {
        [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
        return;
    }
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:_model.user_id
                                             isAttention:!_attentionBtn.selected
                                            successBlock:^(id object) {
                                                @strongify(self);
                                                self.model.isAttention = !self.attentionBtn.selected;
                                                [self reloadAttentionStatus];
                                            } faileBlock:nil];
}

#pragma mark - SuperPlayerDelegate

- (void)superPlayerBackAction:(SuperPlayerView *)player{
    // player加到控制器上，只有一个player时候
    // 状态条的方向旋转的方向,来判断当前屏幕的方向
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    // 是竖屏时候响应关
    if (orientation == UIInterfaceOrientationPortrait &&
        (self.playerView.state == StatePlaying)) {

    } else {
        [self.playerView resetPlayer];  //非常重要
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)configUI{
    if (_headerView) return;
    self.headerView = [UIView by_init];
    [self.headerView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.headerView];
    BOOL isHost = [self.model.user_id isEqualToString:[AccountModel sharedAccountModel].account_id];
    CGFloat height = isHost ? kCommonScreenWidth*9.0f/16.0f : kCommonScreenWidth*9.0f/16.0f + 48;
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(height);
    }];

    
    self.playerFatherView = [[UIView alloc] init];
    self.playerFatherView.backgroundColor = [UIColor blackColor];
    [self.headerView addSubview:self.playerFatherView];
    [self.view bringSubviewToFront:self.headerView];
    [self.playerFatherView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.leading.trailing.mas_equalTo(0);
        // 这里宽高比16：9,可自定义宽高比
        make.height.mas_equalTo(self.playerFatherView.mas_width).multipliedBy(9.0f/16.0f);
    }];
    self.playerView.fatherView = self.playerFatherView;
    [self.playerView setShowWatchNum:YES];
    
    if (!isHost) {
        [self addHeaderBottomView];
    }
    
    [self.view addSubview:self.forumView.view];
    [self.view sendSubviewToBack:self.forumView.view];
    [self.view addSubview:self.toolView];
//    [self addOpeartionView];
    // 添加举报
//    [self addRepeortMarkView];
    @weakify(self);
    [_forumView.view mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.headerView.mas_bottom).mas_equalTo(0);
        make.bottom.mas_equalTo(-kSafe_Mas_Bottom(49));
    }];
    [_toolView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Bottom(49));
    }];
    [self playVideo];
}

- (void)addHeaderBottomView{
    PDImageView *userlogoView = [[PDImageView alloc] init];
    userlogoView.contentMode = UIViewContentModeScaleAspectFill;
    userlogoView.layer.cornerRadius = 16;
    userlogoView.clipsToBounds = YES;
    userlogoView.userInteractionEnabled = YES;
    [self.headerView addSubview:userlogoView];
    userlogoView.style = _model.cert_badge;
    [userlogoView uploadHDImageWithURL:_model.head_img callback:nil];
    [userlogoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(17);
        make.bottom.mas_equalTo(-8);
        make.width.height.mas_equalTo(32);
    }];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userlogoAction)];
    [userlogoView addGestureRecognizer:tapRecognizer];
    
    
    // 头像背景
    UIImageView *userlogoBg = [[UIImageView alloc] init];
    [userlogoBg by_setImageName:@"common_userlogo_bg"];
    [self.headerView addSubview:userlogoBg];
    [self.headerView insertSubview:userlogoBg belowSubview:userlogoView];
    [userlogoBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(userlogoBg.image.size.width + 2);
        make.height.mas_equalTo(userlogoBg.image.size.height + 2);
        make.centerX.equalTo(userlogoView.mas_centerX).with.offset(0);
        make.centerY.equalTo(userlogoView.mas_centerY).with.offset(0);
    }];
    
    UILabel *userNameLab = [[UILabel alloc] init];
    [userNameLab setBy_font:13];
    userNameLab.textColor = kColorRGBValue(0x2c2c2c);
    userNameLab.text = _model.nickname;
    [self.headerView addSubview:userNameLab];
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogoView.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(userlogoView.mas_centerY).mas_offset(0);
        make.width.mas_lessThanOrEqualTo(kCommonScreenWidth - 196);
        make.height.mas_equalTo(15);
    }];
    
    UIView *achievementView = [UIView by_init];
    [self.headerView addSubview:achievementView];
    [achievementView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userNameLab.mas_right).mas_offset(5);
        make.centerY.equalTo(userNameLab);
        make.width.mas_equalTo(58);
        make.height.mas_equalTo(16);
    }];
    
    NSArray *achievementData = _model.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [achievementView addSubview:imageView];
    }
    
    // 关注按钮
    UIButton *attentionBtn = [UIButton by_buttonWithCustomType];
    NSAttributedString *normalAtt = [[NSAttributedString alloc] initWithString:@"+ 关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xffffff),NSFontAttributeName:[UIFont systemFontOfSize:12]}];
    NSAttributedString *selectAtt = [[NSAttributedString alloc] initWithString:@"已关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xee4944),NSFontAttributeName:[UIFont systemFontOfSize:12]}];
    [attentionBtn setAttributedTitle:normalAtt forState:UIControlStateNormal];
    [attentionBtn setAttributedTitle:selectAtt forState:UIControlStateSelected];
    [attentionBtn addTarget:self action:@selector(attentionBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView addSubview:attentionBtn];
    self.attentionBtn = attentionBtn;
    attentionBtn.layer.cornerRadius = 2;
    attentionBtn.layer.borderWidth = 0.5;
    attentionBtn.selected = _model.isAttention;
    [attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(54);
        make.bottom.mas_equalTo(-11);
    }];
    [self reloadAttentionStatus];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xededed)];
    [self.headerView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
}

//- (void)addOpeartionView{
//    if (_opeartionView) return;
//    self.opeartionView = [[BYILiveOpeartionView alloc] initWithIsHost:NO target:self];
//    _opeartionView.liveId = _model.live_record_id;
//    _opeartionView.hostId = _model.user_id;
//    _opeartionView.hostName = _model.nickname;
//    [_opeartionView setSupperNum:_model.count_support];
//    [_opeartionView hiddenReward];
//    @weakify(self);
//    _opeartionView.didSuppertSucHandle = ^{
//        @strongify(self);
//        if (self.model.isSupport) return ;
//        self.model.isSupport = YES;
//        self.model.count_support++;
//        [self.opeartionView setSupperNum:self.model.count_support];
//    };
//    [self.view addSubview:_opeartionView];
//    [_opeartionView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(0);
//        make.width.mas_equalTo(53);
//        make.height.mas_equalTo(170);
//        make.bottom.mas_equalTo(-130);
//    }];
//}

//- (void)addRepeortMarkView{
//    if (_reportMarkView) return;
//
//    self.reportSheetView = [BYReportSheetView initReportSheetViewShowInView:kCommonWindow];
//    self.reportSheetView.theme_id = self.model.live_record_id;
//    self.reportSheetView.theme_type = BY_THEME_TYPE_LIVE;
//    self.reportSheetView.user_id = self.model.user_id;
//
//    self.reportMaskView = [UIView by_init];
//    self.reportMaskView.layer.opacity = 0.0f;
//    [self.view addSubview:self.reportMaskView];
//    [self.reportMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
//    }];
//    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reportMaskViewAction)];
//    [self.reportMaskView addGestureRecognizer:tapGestureRecognizer];
//
//    self.reportMarkView = [[UIImageView alloc] init];
//    [self.reportMarkView by_setImageName:@"common_report_single"];
//    [self.view addSubview:self.reportMarkView];
//    self.reportMarkView.userInteractionEnabled = YES;
//    self.reportMarkView.layer.shadowColor = kColorRGBValue(0x4e4e4e).CGColor;
//    self.reportMarkView.layer.shadowOpacity = 0.2;
//    self.reportMarkView.layer.shadowRadius = 25;
//    self.reportMarkView.layer.shadowOffset = CGSizeMake(0, -2);
//    self.reportMarkView.layer.opacity = 0.0f;
//    @weakify(self);
//    [self.reportMarkView mas_makeConstraints:^(MASConstraintMaker *make) {
//        @strongify(self);
//        make.right.mas_equalTo(-15);
//        make.bottom.mas_equalTo(-kSafe_Mas_Bottom(40));
//        make.width.mas_equalTo(self.reportMarkView.image.size.width);
//        make.height.mas_equalTo(self.reportMarkView.image.size.height);
//    }];
//
//    UIButton *reportBtn = [UIButton by_buttonWithCustomType];
//    [reportBtn setBy_attributedTitle:@{@"title":@"举报",NSForegroundColorAttributeName:kColorRGBValue(0x4f4f4f),NSFontAttributeName:[UIFont systemFontOfSize:13]} forState:UIControlStateNormal];
//    [reportBtn addTarget:self action:@selector(reportBtnAction:) forControlEvents:UIControlEventTouchUpInside];
//    [self.reportMarkView addSubview:reportBtn];
//    [reportBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.top.right.mas_equalTo(0);
//        make.height.mas_equalTo(50);
//    }];
//}
//
//- (void)reportMaskViewAction{
//    [self showReportMarkViewHidden:YES];
//}
//
//- (void)showReportMarkViewHidden:(BOOL)hidden{
//    [self.view bringSubviewToFront:self.reportMaskView];
//    [self.view bringSubviewToFront:self.reportMarkView];
//    [UIView animateWithDuration:0.1 animations:^{
//        self.reportMarkView.layer.opacity = !hidden;
//        self.reportMaskView.layer.opacity = !hidden;
//    }];
//}

- (BYCommonPlayerView *)playerView {
    if (!_playerView) {
        _playerView = [[BYCommonPlayerView alloc] init];
        _playerView.fatherView = _playerFatherView;
        _playerView.enableFloatWindow = NO;
        // 设置代理
        _playerView.delegate = self;
    }
    return _playerView;
}

- (BYLiveForumView *)forumView{
    if (!_forumView) {
        _forumView = [[BYLiveForumView alloc] initWithFatureController:self isReport:YES];
        _forumView.transferRoomId = _model.live_record_id;
        _forumView.scrollViewDidScrollView = _scrollViewDidScrollView;
        _forumView.viewController = _viewController;
        _forumView.needAdaptiveBottom = YES;
        [self addChildViewController:_forumView];
    }
    return _forumView;
}

- (BYVodPlayToolView *)toolView{
    if (!_toolView) {
        _toolView = [[BYVodPlayToolView alloc] init];
        [_toolView reloadData:self.model];
        @weakify(self);
        _toolView.didSendMessageHandle = ^(NSString * _Nonnull string) {
            @strongify(self);
            [self.forumView sendComment:string];
        };
//        _toolView.didReportActionHandle = ^{
//            @strongify(self);
//            [self reportAction];
//        };
        _toolView.didShareActionHandle = ^{
            @strongify(self);
            if (self.didShareActionHandle) {
                self.didShareActionHandle();
            }
        };
    }
    return _toolView;
}
@end
