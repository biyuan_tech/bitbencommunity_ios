//
//  BYLiveForumView.m
//  BibenCommunity
//
//  Created by Belief on 2018/10/23.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYLiveForumView.h"
#import "NetworkAdapter.h"
#import "LiveFourumRootListModel.h"
#import "LiveForumRootSingleTableViewCell.h"
#import "LiveForumRootSingleSubTableViewCell.h"
#import "LiveForumRootInputView.h"
#import "NetworkAdapter+PPTLive.h"
#import "LiveForumItemsTableViewCell.h"
#import "LiveForumDetailViewController.h"
#import "BYCommonPlayerView.h"
#import "BYPersonHomeController.h"

@interface BYLiveForumView ()<UITableViewDelegate,UITableViewDataSource>
{
    CGRect _newKeyboardRect;
    LiveFourumRootListSingleModel *_tempSingleModle;
}
@property (nonatomic,strong)BYCommonTableView *dianpingTableView;
@property (nonatomic,strong)NSMutableArray *dianpingMutableArr;
@property (nonatomic,strong)UIViewController *fathureController;
@property (nonatomic,strong)UIView *maskView;

@end
@implementation BYLiveForumView

- (void)dealloc
{
    [_dianpingTableView dismissPrompt];
    [_dianpingTableView removeFromSuperview];
    _dianpingTableView = nil;
}


- (instancetype)initWithFatureController:(UIViewController *)viewController isReport:(BOOL)isReport{
    self = [super init];
    if (self) {
        _isReport = isReport;
        _fathureController = viewController;
        [self setContentView];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_inputView.superview) {
        [self.view addSubview:self.inputView];
        [self.view bringSubviewToFront:self.inputView];
        [_inputView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.bottom.mas_equalTo(kSafe_Mas_Bottom(0));
            make.height.mas_equalTo([LiveForumRootInputView  calculationHeight:@""]);
        }];
    }
}

- (void)setTransferRoomId:(NSString *)transferRoomId{
    _transferRoomId = transferRoomId;
    [self sendRequestToGetInfoWithReReload:NO];
}

- (void)setContentView{
    [self arrayWithInit];
//    [self createInputView];
    [self createTableView];
    [self creatMaskView];
//    [self createNotifi];                // 添加键盘代理
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.dianpingMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.dianpingTableView){
        self.dianpingTableView = [[BYCommonTableView alloc] init];
        self.dianpingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.dianpingTableView.dataSource = self;
        self.dianpingTableView.delegate = self;
        [self.view addSubview:self.dianpingTableView];
        [self.view sendSubviewToBack:self.dianpingTableView];
        [_dianpingTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }
    __weak typeof(self)weakSelf = self;
    [self.dianpingTableView addHeaderRefreshHandle:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithReReload:YES];
    }];

    [self.dianpingTableView addFooterRefreshHandle:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithReReload:NO];
    }];

//    [self addSubview:self.inputView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dianpingMutableArr.count;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:section];
//    return singleModel.child.count + 1;
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        LiveForumRootSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[LiveForumRootSingleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
        cellWithRowOne.hasArticleDetail = YES;
        cellWithRowOne.transferInfoManager = singleModel;
        cellWithRowOne.transferItemId = self.transferRoomId;
        cellWithRowOne.isLive = YES;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickHeaderImgWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (![AccountModel sharedAccountModel].hasLoggedIn) {
                [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
                return;
            }
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = cellWithRowOne.transferInfoManager.user_id;
            if (strongSelf.viewController) {
                [strongSelf.viewController.navigationController pushViewController:personHomeController animated:YES];
            }else{
                [strongSelf.viewController.navigationController pushViewController:personHomeController animated:YES];
            }
        }];
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        LiveForumRootSingleSubTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[LiveForumRootSingleSubTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
        cellWithRowTwo.transferCount = singleModel.childCount <= 3 ? -1:singleModel.childCount;
        NSInteger index = indexPath.row - 1;
        if (singleModel.child.count > 1){
            if (index == 0){
                cellWithRowTwo.transferSubType = LiveForumRootSingleSubTableViewCellTypeTop;
            } else if (indexPath.row == singleModel.child.count){
                cellWithRowTwo.transferSubType = LiveForumRootSingleSubTableViewCellTypeBottom;
            } else {
                cellWithRowTwo.transferSubType = LiveForumRootSingleSubTableViewCellTypeNormal;
            }
        } else {
            cellWithRowTwo.transferSubType = LiveForumRootSingleSubTableViewCellTypeSingle;
        }
        cellWithRowTwo.transferInfoManager = [singleModel.child objectAtIndex:index];
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([_inputView.inputView isFirstResponder]) {
        [_inputView releaseKeyboard];
        return;
    }
    if (![AccountModel sharedAccountModel].hasLoggedIn) {
        [(BYCommonViewController *)CURRENT_VC actionAutoLoginBlock:nil];
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
    
//    if (indexPath.row == 0){
        LiveForumDetailViewController *controller = [[LiveForumDetailViewController alloc]init];
        controller.transferFourumRootModel = singleModel;
        controller.transferRoomId = self.transferRoomId;
        if (self.viewController) {
            [self.viewController.navigationController pushViewController:controller animated:YES];
            return;
        }
        [self.viewController.navigationController pushViewController:controller animated:YES];
//    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
    NSInteger rowCount = singleModel.child.count + 2;
    if (indexPath.row == 0){
        return [LiveForumRootSingleTableViewCell calculationCellHeightWithModel:singleModel];
    } else if (indexPath.row == rowCount - 1){
        return 0;
    } else {
        NSInteger index = indexPath.row - 1;
        if (singleModel.child.count > 1){
            if (index == 0){
                return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:singleModel type:LiveForumRootSingleSubTableViewCellTypeTop];
            } else if (indexPath.row == singleModel.child.count){
                return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:singleModel type:LiveForumRootSingleSubTableViewCellTypeBottom];
            } else {
                return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:singleModel type:LiveForumRootSingleSubTableViewCellTypeNormal];
            }
        } else {
            return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:singleModel type:LiveForumRootSingleSubTableViewCellTypeSingle];
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor hexChangeFloat:@"E4E4E4"];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else {
        return .5f;
    }
}

#pragma mark - 创建输入框

- (void)creatMaskView{
    if (!_maskView) {
        self.maskView = [UIView by_init];
        self.maskView.hidden = YES;
        self.maskView.backgroundColor = [UIColor clearColor];
        if (_fathureController) {
            [_fathureController.view addSubview:_maskView];
            [_fathureController.view insertSubview:_maskView belowSubview:_inputView];
        }
        else{
            [self.view addSubview:_maskView];
            [self.view insertSubview:_maskView belowSubview:_inputView];
        }
        [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(maskViewAction)];
        [self.maskView addGestureRecognizer:tapGestureRecognizer];
    }
}

//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    [self.inputView releaseKeyboard];
//}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (_scrollViewDidScrollView) {
        _scrollViewDidScrollView(scrollView);
    }
}

#pragma mark  - Interface
-(void)sendRequestToGetInfoWithReReload:(BOOL)rereload{
    NSInteger page = rereload ?0:self.dianpingTableView.currentPage;
    NSDictionary *params = @{@"theme_id":self.transferRoomId,@"theme_type":@"0",@"reply_comment_id":@"0",@"page_number":@(page),@"page_size":@"10"};
    __weak typeof(self)weakSelf = self;
    
    [[BYLiveHomeRequest alloc] loadRequestPageComment:params successBlock:^(id object) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
            self.dianpingTableView.currentPage ++;
            if (strongSelf.dianpingTableView.isXiaLa || rereload){
                [strongSelf.dianpingMutableArr removeAllObjects];
            }
        LiveFourumRootListModel *listModel = [[LiveFourumRootListModel alloc] init];
        if ([object[@"content"] isKindOfClass:[NSArray class]]) {
            if ([object[@"content"] count]) {
                listModel.content = [[LiveFourumRootListSingleModel mj_objectArrayWithKeyValuesArray:object[@"content"]] copy];
                for (LiveFourumRootListSingleModel *model in listModel.content) {
                    if (model.child.count) {
                        model.child = [[LiveFourumRootListSingleModel mj_objectArrayWithKeyValuesArray:model.child] copy];
                    }
                }
            }
        }
        [strongSelf.dianpingMutableArr addObjectsFromArray:listModel.content];
        
        for (int i = 0; i <strongSelf.dianpingMutableArr.count;i++){
            LiveFourumRootListSingleModel *singleModel = [strongSelf.dianpingMutableArr objectAtIndex:i];
            if (singleModel.childCount > singleModel.child.count){          // 表示有多余的内容
                LiveFourumRootListSingleModel *childModel = [[LiveFourumRootListSingleModel alloc]init];
                childModel.reply_comment_name = @"共同NNNNN条回复";
                childModel.childCount = singleModel.childCount;
                NSMutableArray *childRootArr = [NSMutableArray array];
                [childRootArr addObjectsFromArray:singleModel.child];
                [childRootArr addObject:childModel];
                singleModel.child = [childRootArr copy];
            }
        }
        
        [strongSelf.dianpingTableView reloadData];
        
        if (strongSelf.dianpingMutableArr.count){
            [strongSelf.dianpingTableView dismissPrompt];
        } else {
            [strongSelf.dianpingTableView showPrompt:@"当前没有信息" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
        }
            //            [strongSelf.dianpingTableView stopPullToRefresh];
            //            [strongSelf.dianpingTableView stopFinishScrollingRefresh];
        [strongSelf.dianpingTableView endRefreshing];
    } faileBlock:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.dianpingTableView endRefreshing];
    }];
//    [[NetworkAdapter sharedAdapter] fetchWithPath:@"page_comment" requestParams:params responseObjectClass:[LiveFourumRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSucceeded){
//            self.dianpingTableView.currentPage ++;
//            if (strongSelf.dianpingTableView.isXiaLa || rereload){
//                [strongSelf.dianpingMutableArr removeAllObjects];
//            }
//            LiveFourumRootListModel *listModel = (LiveFourumRootListModel *)responseObject;
//            [strongSelf.dianpingMutableArr addObjectsFromArray:listModel.content];
//
//            for (int i = 0; i <strongSelf.dianpingMutableArr.count;i++){
//                LiveFourumRootListSingleModel *singleModel = [strongSelf.dianpingMutableArr objectAtIndex:i];
//                if (singleModel.childCount > singleModel.child.count){          // 表示有多余的内容
//                    LiveFourumRootListSingleModel *childModel = [[LiveFourumRootListSingleModel alloc]init];
//                    childModel.reply_comment_name = @"共同NNNNN条回复";
//                    childModel.childCount = singleModel.childCount;
//                    NSMutableArray *childRootArr = [NSMutableArray array];
//                    [childRootArr addObjectsFromArray:singleModel.child];
//                    [childRootArr addObject:childModel];
//                    singleModel.child = [childRootArr copy];
//                }
//            }
//
//            [strongSelf.dianpingTableView reloadData];
//
//            if (strongSelf.dianpingMutableArr.count){
//                [strongSelf.dianpingTableView dismissPrompt];
//            } else {
//                [strongSelf.dianpingTableView showPrompt:@"当前没有信息" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
//            }
////            [strongSelf.dianpingTableView stopPullToRefresh];
////            [strongSelf.dianpingTableView stopFinishScrollingRefresh];
//        }
//        [strongSelf.dianpingTableView endRefreshing];
//    }];
}

#pragma mark - 发送信息

- (void)sendComment:(NSString *)string{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreateCommentWithThemeId:self.transferRoomId type:LiveForumDetailViewControllerTypeNormal themeInfo:self.inputView.transferListModel content:string successBlock:^(id object) {
        @strongify(self);
        [self sendRequestToGetInfoWithReReload:YES];
        [self.inputView cleanKeyboard];
    } faileBlock:^(NSError *error) {
        
    }];
}

-(void)sendRequestToSendInfo{
    NSString *string = [self.inputView.inputView.text removeBothEndsEmptyString:BYRemoveTypeAll];
    __weak typeof(self)weakSelf = self;
    if (!string.length) return;
    [[BYLiveHomeRequest alloc] loadRequestCreateCommentWithThemeId:self.transferRoomId type:LiveForumDetailViewControllerTypeNormal themeInfo:self.inputView.transferListModel content:string successBlock:^(id object) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.inputView.transferListModel = nil;
        [strongSelf sendRequestToGetInfoWithReReload:YES];
        [strongSelf.inputView cleanKeyboard];
    } faileBlock:^(NSError *error) {
        
    }];
//    [[NetworkAdapter sharedAdapter] liveCreateCommentWiththemeId:self.transferRoomId type:LiveForumDetailViewControllerTypeNormal ThemeInfo:self.inputView.transferListModel content:string block:^(BOOL isSuccessed) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSuccessed){
//            strongSelf.inputView.transferListModel = nil;
//            [strongSelf sendRequestToGetInfoWithReReload:YES];
//            [strongSelf.inputView cleanKeyboard];
//        }
//    }];
}


@end
