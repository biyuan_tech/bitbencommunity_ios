//
//  BYVODPlayController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/10/23.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYVODPlayController.h"
#import "BYVODPlayViewModel.h"

@interface BYVODPlayController ()


@end

@implementation BYVODPlayController

- (void)dealloc
{
    
}
- (Class)getViewModelClass{
    return [BYVODPlayViewModel class];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.navigationTitle = self.naTitle;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

@end
