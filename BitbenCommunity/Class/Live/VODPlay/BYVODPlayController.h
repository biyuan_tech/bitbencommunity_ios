//
//  BYVODPlayController.h
//  BibenCommunity
//
//  Created by 随风 on 2018/10/23.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYVODPlayController : BYCommonViewController

/** 直播记录id */
@property (nonatomic ,copy) NSString *live_record_id;

/** title */
@property (nonatomic ,copy) NSString *naTitle;


@end

NS_ASSUME_NONNULL_END
