//
//  BYCertGroupWorkController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCertGroupWorkController.h"

@interface BYCertGroupWorkController ()

/** scrollView */
@property (nonatomic ,strong) UIScrollView *scrollView;
/** imageView */
@property (nonatomic ,strong) PDImageView *imageView;


@end

@implementation BYCertGroupWorkController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"我要合作";
    [self setContentView];
}


- (void)setContentView{
    self.scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    self.imageView = [[PDImageView alloc] init];
    [self.scrollView addSubview:self.imageView];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.height.mas_greaterThanOrEqualTo(0);
        make.right.mas_equalTo(self.view);
    }];
    
    
    @weakify(self);
    [self.imageView uploadHDImageWithURL:nullToEmpty(self.picurl) callback:^(UIImage *image) {
        @strongify(self);
        [self.imageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(image ? kCommonScreenWidth*image.size.height/image.size.width : 0);
        }];
        @weakify(self);
        [self.scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.bottom.mas_equalTo(self.imageView.mas_bottom).offset(0).priorityLow();
            make.bottom.mas_greaterThanOrEqualTo(self.view);
        }];
    }];

}

@end
