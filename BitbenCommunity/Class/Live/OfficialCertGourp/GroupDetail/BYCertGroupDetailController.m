//
//  BYCertGroupDetailController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCertGroupDetailController.h"
#import "BYCertGroupDetailViewModel.h"

@interface BYCertGroupDetailController ()

@end

@implementation BYCertGroupDetailController

- (Class)getViewModelClass{
    return [BYCertGroupDetailViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:kColorRGBValue(0xea6438)] forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setBackgroundImage: forBarMetrics:UIBarMetricsDefault];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    self.navigationTitle = @"社群简介";
    self.navigationItem.leftBarButtonItem = nil;
    self.titleColor = [UIColor whiteColor];
    @weakify(self);
    UIButton *leftBtn = [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_rank_nav_back"] barHltImage:[UIImage imageNamed:@"icon_rank_nav_back"] action:^{
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 30);
    
}


@end
