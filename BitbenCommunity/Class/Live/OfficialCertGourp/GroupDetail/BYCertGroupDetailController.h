//
//  BYCertGroupDetailController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYCertGroupDetailController : BYCommonViewController

/** id */
@property (nonatomic ,copy) NSString *community_id;


@end

NS_ASSUME_NONNULL_END
