//
//  BYCertGroupDetailViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCertGroupDetailViewModel.h"
#import "BYCertGroupDetailController.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>
#import "BYOfficialCertGroupModel.h"

#define K_VC ((BYCertGroupDetailController *)S_VC)
@interface BYCertGroupDetailViewModel ()

/** scrollView */
@property (nonatomic ,strong) UIScrollView *scrollView;
/** logo */
@property (nonatomic ,strong) PDImageView *logoImgView;
/** 群名 */
@property (nonatomic ,strong) UILabel *nameLab;
/** 群主昵称 */
@property (nonatomic ,strong) UILabel *ownerNameLab;
/** 活跃度 */
@property (nonatomic ,strong) UILabel *activityNumLab;
/** 简介 */
@property (nonatomic ,strong) YYLabel *introLab;
/** 加入群聊 */
@property (nonatomic ,strong) UIButton *joinGroupBtn;
/** model */
@property (nonatomic ,strong) BYOfficialCertGroupModel *model;

@end


@implementation BYCertGroupDetailViewModel

- (void)setContentView{
    [self addScrolleView];
    [self loadRequest];
}

- (void)reloadData{
   
    [self.logoImgView uploadHDImageWithURL:self.model.owner_pic callback:nil];
    self.nameLab.text = self.model.name;
    self.ownerNameLab.text = [NSString stringWithFormat:@"群主昵称：%@",self.model.owner_nickname];
    self.activityNumLab.text = stringFormatInteger(self.model.activation);
    
    NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(self.model.intro)];
    messageAttributed.yy_lineSpacing = 8.0f;
    messageAttributed.yy_font = [UIFont systemFontOfSize:15];
    messageAttributed.yy_color = kColorRGBValue(0x323232);
    YYTextContainer *textContainer = [YYTextContainer new];
    textContainer.size = CGSizeMake(kCommonScreenWidth - 60, MAXFLOAT);
    YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
    [self.introLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(ceil(layout.textBoundingSize.height));
    }];
    self.introLab.attributedText = messageAttributed;
    
    @weakify(self);
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.bottom.mas_equalTo(self.introLab.mas_bottom).offset(70).priorityLow();
        make.bottom.mas_greaterThanOrEqualTo(S_V_VIEW);
    }];
}

#pragma mark - action
- (void)joinGroupBtnAction{
    if (!self.model ||
        !self.model.remind_data ||
        !self.model.remind_data.service_wechat.length) {
        showToastView(@"暂未获取到微信号", S_V_VIEW);
        return;
    }
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.model.remind_data.service_wechat;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:self.model.remind_data.community_remind preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addAction:cancelAction];
    [S_V_NC presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - request
- (void)loadRequest{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCommuntiyDetail:K_VC.community_id successBlock:^(id object) {
        @strongify(self);
        self.model = object;
        [self reloadData];
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"获取社群简介失败", S_V_VIEW);
    }];
}

#pragma mark - configUI

- (void)addScrolleView{
    
    self.scrollView = [[UIScrollView alloc] init];
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    [S_V_VIEW addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    UIView *maskView = [UIView by_init];
    maskView.backgroundColor = kColorRGBValue(0xea6438);
    [self.scrollView addSubview:maskView];
    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.scrollView.mas_top);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(S_V_VIEW);
        make.height.mas_equalTo(kCommonScreenWidth/2);
    }];
    
    UIView *contentView = [UIView by_init];
    [self.scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.right.mas_equalTo(S_V_VIEW);
        make.bottom.mas_equalTo(S_V_VIEW);
        
    }];
    
    UIView *topBgView = [UIView by_init];
    [topBgView setBackgroundColor:kColorRGBValue(0xea6438)];
    [contentView addSubview:topBgView];
    [topBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.width.mas_equalTo(self.scrollView);
        make.height.mas_equalTo(147);
    }];
    
    /** logo */
    self.logoImgView = [[PDImageView alloc] init];
    self.logoImgView.layer.cornerRadius = 4.0;
    self.logoImgView.contentMode = UIViewContentModeScaleAspectFill;
    self.logoImgView.clipsToBounds = YES;
    [contentView addSubview:self.logoImgView];
    [self.logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(20);
        make.width.height.mas_equalTo(50);
    }];

    // 群名
    self.nameLab = [UILabel by_init];
    self.nameLab.font = [UIFont boldSystemFontOfSize:18];
    self.nameLab.textColor = kColorRGBValue(0xffffff);
    [contentView addSubview:self.nameLab];
    @weakify(self);
    [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.mas_equalTo(self.logoImgView).offset(5);
        make.left.mas_equalTo(self.logoImgView.mas_right).offset(14);
        make.height.mas_equalTo(self.nameLab.font.pointSize);
        make.right.mas_equalTo(-100);
    }];

    // 群主昵称
    self.ownerNameLab = [UILabel by_init];
    [self.ownerNameLab setBy_font:14];
    self.ownerNameLab.textColor = kColorRGBValue(0xffffff);
    [contentView addSubview:self.ownerNameLab];
    [self.ownerNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.bottom.mas_equalTo(self.logoImgView).offset(-5);
        make.left.mas_equalTo(self.logoImgView.mas_right).offset(14);
        make.height.mas_equalTo(self.ownerNameLab.font.pointSize);
        make.right.mas_equalTo(-15);
    }];

    // 加入群聊
    self.joinGroupBtn = [UIButton by_buttonWithCustomType];
    [self.joinGroupBtn setBackgroundColor:[UIColor whiteColor]];
    [self.joinGroupBtn setBy_attributedTitle:@{@"title":@"加入本群",
                                               NSFontAttributeName:[UIFont systemFontOfSize:14],
                                               NSForegroundColorAttributeName:kColorRGBValue(0xea6438)
                                               } forState:UIControlStateNormal];
    [self.joinGroupBtn addTarget:self action:@selector(joinGroupBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:self.joinGroupBtn];
    self.joinGroupBtn.layer.cornerRadius = 4.0f;
    [self.joinGroupBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(28);
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(20);
    }];

    // 周活跃度
    UILabel *activityLab = [UILabel by_init];
    [activityLab setBy_font:12];
    activityLab.text = @"周活跃度:";
    activityLab.textColor = kColorRGBValue(0xffffff);
    [contentView addSubview:activityLab];
    [activityLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.logoImgView.mas_right).offset(14);
        make.height.mas_equalTo(activityLab.font.pointSize);
        make.width.mas_equalTo(stringGetWidth(activityLab.text, 12));
        make.top.mas_equalTo(self.logoImgView.mas_bottom).offset(12);
    }];

    self.activityNumLab = [UILabel by_init];
//    [self.activityNumLab setBy_font:19];
    self.activityNumLab.font = [UIFont boldSystemFontOfSize:19];
    self.activityNumLab.textColor = kColorRGBValue(0xffffff);
    [contentView addSubview:self.activityNumLab];
    [self.activityNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(activityLab.mas_right).offset(10);
        make.centerY.mas_equalTo(activityLab);
        make.height.mas_equalTo(self.activityNumLab.font.pointSize);
        make.width.mas_equalTo(100);
    }];

    // 社群简介
    UILabel *introTitleLab = [UILabel by_init];
    introTitleLab.font = [UIFont boldSystemFontOfSize:18];
    introTitleLab.text = @"社群介绍";
    introTitleLab.textColor = kColorRGBValue(0x323232);
    [contentView addSubview:introTitleLab];
    [introTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(activityLab.mas_bottom).offset(45);
        make.height.mas_equalTo(introTitleLab.font.pointSize);
        make.width.mas_equalTo(100);
    }];

    self.introLab = [[YYLabel alloc] init];
    self.introLab.font = [UIFont systemFontOfSize:15];
    self.introLab.textColor = kColorRGBValue(0x323232);
    self.introLab.numberOfLines = 0;
    [contentView addSubview:self.introLab];
    [self.introLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(introTitleLab.mas_bottom).offset(20);
        make.height.mas_greaterThanOrEqualTo(0);
        make.right.mas_equalTo(-30);
    }];

    // 简介背景
    UIImageView *introTopBgView = [[UIImageView alloc] init];
    UIImage *image = [UIImage imageNamed:@"group_intro_bg"];
    [introTopBgView setImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 14, 0, 14)]];
    [contentView insertSubview:introTopBgView belowSubview:introTitleLab];
    [introTopBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(topBgView);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(introTopBgView.image.size.width);
    }];

    UIView *introBgView = [UIView by_init];
    introBgView.backgroundColor = [UIColor whiteColor];
    [contentView insertSubview:introBgView belowSubview:introTitleLab];
    [introBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(topBgView.mas_bottom);
        make.bottom.mas_equalTo(S_V_VIEW);
    }];

}

@end
