//
//  BYOfficialCertGroupModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@class BYOfficialRemindData;
@interface BYOfficialCertGroupModel : BYCommonModel

/** id */
@property (nonatomic ,copy) NSString *community_id;
/** 创建时间 */
@property (nonatomic ,assign) NSTimeInterval create_time;
/** 简介 */
@property (nonatomic ,copy) NSString *intro;
/** 社群名 */
@property (nonatomic ,copy) NSString *name;
/** 群主昵称 */
@property (nonatomic ,copy) NSString *owner_nickname;
/** 群主头像 */
@property (nonatomic ,copy) NSString *owner_pic;
/** 活跃度 */
@property (nonatomic ,assign) NSInteger activation;
/** wxData */
@property (nonatomic ,strong) BYOfficialRemindData *remind_data;

/** 获取社群列表 */
+ (NSArray *)getTableData:(NSArray *)respond;
/** 获取社群详情 */
+ (BYOfficialCertGroupModel *)getGroupDetailData:(NSDictionary *)respond;

@end

@interface BYOfficialRemindData : BYCommonModel

/** 微信名 */
@property (nonatomic ,copy) NSString *service_wechat;
/** 展示文案 */
@property (nonatomic ,copy) NSString *community_remind;
/** 我要合作图片 */
@property (nonatomic ,copy) NSString *cooperation_pic;


/** 获取展示的微信联系方式 */
+ (BYOfficialRemindData *)getWechatData:(NSDictionary *)respond;

@end

NS_ASSUME_NONNULL_END
