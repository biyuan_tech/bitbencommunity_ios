//
//  BYOfficialCertGroupModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYOfficialCertGroupModel.h"

@implementation BYOfficialCertGroupModel

+ (NSArray *)getTableData:(NSArray *)respond{
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (!respond.count) {
        return @[];
    }
    NSMutableArray *data = [NSMutableArray array];
    for (NSDictionary *dic in respond) {
        BYOfficialCertGroupModel *model = [BYOfficialCertGroupModel mj_objectWithKeyValues:dic];
        model.cellHeight = 75;
        model.cellString = @"BYOfficialCertGroupCell";
        [data addObject:model];
    }
    return data;
}

+ (BYOfficialCertGroupModel *)getGroupDetailData:(NSDictionary *)respond{
    BYOfficialCertGroupModel *model = [BYOfficialCertGroupModel mj_objectWithKeyValues:respond];
    model.remind_data = [BYOfficialRemindData mj_objectWithKeyValues:respond[@"remind_data"]];
    return model;
}

@end

@implementation BYOfficialRemindData


+ (BYOfficialRemindData *)getWechatData:(NSDictionary *)respond{
    BYOfficialRemindData *data = [BYOfficialRemindData mj_objectWithKeyValues:respond];
    return data;
}

@end
