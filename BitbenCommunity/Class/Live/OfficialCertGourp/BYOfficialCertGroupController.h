//
//  BYOfficialCertGroupController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYOfficialCertGroupController : BYCommonViewController

- (void)autoReload;

@end

NS_ASSUME_NONNULL_END
