//
//  BYOfficialCertGroupCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYOfficialCertGroupCell.h"
#import "BYOfficialCertGroupModel.h"

@interface BYOfficialCertGroupCell ()

/** logo */
@property (nonatomic ,strong) PDImageView *logoImgView;
/** title */
@property (nonatomic ,strong) UILabel *titleLab;
/** 简介 */
@property (nonatomic ,strong) UILabel *introLab;
/** 活跃度 */
@property (nonatomic ,strong) UILabel *activityNumLab;


@end

@implementation BYOfficialCertGroupCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYOfficialCertGroupModel *model = object[indexPath.row];
    [_logoImgView uploadHDImageWithURL:model.owner_pic callback:nil];
    self.titleLab.text = model.name;
    self.introLab.text = model.intro;
    self.activityNumLab.text = stringFormatInteger(model.activation);
}

- (void)setContentView{
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    PDImageView *logoImgView = [[PDImageView alloc] init];
    logoImgView.contentMode = UIViewContentModeScaleAspectFill;
    logoImgView.layer.cornerRadius = 4.0f;
    logoImgView.clipsToBounds = YES;
    [self.contentView addSubview:logoImgView];
    self.logoImgView = logoImgView;
    [logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(0);
        make.width.height.mas_equalTo(50);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:16];
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(logoImgView.mas_right).offset(13);
        make.top.mas_equalTo(logoImgView).offset(5);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.right.mas_equalTo(-80);
    }];
    
    UILabel *introLab = [UILabel by_init];
    [introLab setBy_font:13];
    introLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:introLab];
    self.introLab = introLab;
    [introLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(logoImgView).offset(-5);
        make.left.mas_equalTo(logoImgView.mas_right).offset(13);
        make.height.mas_equalTo(introLab.font.pointSize);
        make.right.mas_equalTo(-80);
    }];
    
    UILabel *activityLab = [UILabel by_init];
    [activityLab setBy_font:12];
    activityLab.text = @"周活跃度";
    activityLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:activityLab];
    [activityLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(introLab);
        make.height.mas_equalTo(activityLab.font.pointSize);
        make.width.mas_equalTo(stringGetWidth(activityLab.text, 12));
    }];
    
    UILabel *activityNumLab = [UILabel by_init];
    [activityNumLab setBy_font:19];
    activityNumLab.textAlignment = NSTextAlignmentCenter;
    activityNumLab.textColor = kColorRGBValue(0xea6438);
    [self.contentView addSubview:activityNumLab];
    self.activityNumLab = activityNumLab;
    [activityNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(activityLab);
        make.centerY.mas_equalTo(titleLab);
        make.width.mas_equalTo(activityLab).offset(30);
        make.height.mas_equalTo(activityNumLab.font.pointSize);
    }];
}

@end
