//
//  BYOfficialCertGroupViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYOfficialCertGroupViewModel.h"
#import "BYCertGroupDetailController.h"
#import "BYCertGroupWorkController.h"

#import "BYOfficialCertGroupModel.h"

@interface BYOfficialCertGroupViewModel ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;
/** wx展示 */
@property (nonatomic ,strong) BYOfficialRemindData *wxData;


@end

@implementation BYOfficialCertGroupViewModel


- (void)setContentView{
    self.pageNum = 0;
//    [self addNavigationLeftBtn];
    [self addTableView];
    [self addJoinGroupBtn];
    [self addGotoCooperationBtn];
    [self loadRequest];
}

#pragma mark - custom
- (void)reloadData{
    self.pageNum = 0;
    [self loadRequest];
}

- (void)loadMoreData{
    [self loadRequest];
}

- (void)autoReload{
    [self.tableView beginRefreshing];
    [self reloadData];
}

#pragma makr - action

- (void)leftBtnAction{
    BYCertGroupWorkController *groupWorkController = [[BYCertGroupWorkController alloc] init];
    groupWorkController.picurl = self.wxData.cooperation_pic;
    [CURRENT_VC.navigationController pushViewController:groupWorkController animated:YES];
//    showToastView(@"瞎点个锤子", S_V_VIEW);
}

- (void)joinGroupBtnAction{
    if (!self.wxData ||
        !self.wxData.service_wechat.length) {
        showToastView(@"未获取到客服微信号", S_V_VIEW);
        return;
    }
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.wxData.service_wechat;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:self.wxData.community_remind preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addAction:cancelAction];
    [CURRENT_VC.navigationController presentViewController:alertController animated:YES completion:nil];
}

- (void)cooperationBtnAction{
    [self leftBtnAction];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BYOfficialCertGroupModel *model = tableView.tableData[indexPath.row];
    BYCertGroupDetailController *groupDetailController = [[BYCertGroupDetailController alloc] init];
    groupDetailController.community_id = model.community_id;
    [CURRENT_VC.navigationController pushViewController:groupDetailController animated:YES];
}

#pragma mark - request

- (void)loadRequest{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCommunityList:self.pageNum successBlock:^(id object, id wxData) {
        @strongify(self);
        [self.tableView endRefreshing];
        [self.tableView removeEmptyView];
        if (self.pageNum == 0) {
            self.wxData = wxData;
            if (![object count]) {
                [self.tableView addEmptyView:@"暂无内容"];
                return ;
            }
            self.tableView.tableData = object;
        }
        else{
            NSMutableArray *data = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [data addObjectsFromArray:object];
            self.tableView.tableData = data;
        }
        self.pageNum ++;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

#pragma mark - configUI

- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    [self.tableView addHeaderRefreshTarget:self action:@selector(reloadData)];
    [self.tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    [S_V_VIEW addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (void)addNavigationLeftBtn{
    UIButton *leftBtn = [UIButton by_buttonWithCustomType];
    [leftBtn setBy_attributedTitle:@{@"title":@"我要合作",
                                     NSFontAttributeName:[UIFont systemFontOfSize:16],
                                     NSForegroundColorAttributeName:kColorRGBValue(0x323232)
                                     } forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [S_V_NC.navigationBar addSubview:leftBtn];
    [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(stringGetWidth(leftBtn.titleLabel.text, 16));
        make.height.mas_equalTo(44);
    }];
}

- (void)addJoinGroupBtn{
    UIButton *joinGroupBtn = [UIButton by_buttonWithCustomType];
    [joinGroupBtn setBy_imageName:@"join_group" forState:UIControlStateNormal];
    [joinGroupBtn addTarget:self action:@selector(joinGroupBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [S_V_VIEW addSubview:joinGroupBtn];
    [joinGroupBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(joinGroupBtn.imageView.image.size.width);
        make.height.mas_equalTo(joinGroupBtn.imageView.image.size.height);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-130);
    }];
}

- (void)addGotoCooperationBtn{
    UIButton *cooperationBtn = [UIButton by_buttonWithCustomType];
    [cooperationBtn setBy_imageName:@"goto_cooperation" forState:UIControlStateNormal];
    [cooperationBtn addTarget:self action:@selector(cooperationBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [S_V_VIEW addSubview:cooperationBtn];
    [cooperationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(cooperationBtn.imageView.image.size.width);
        make.height.mas_equalTo(cooperationBtn.imageView.image.size.height);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-182);
    }];
}
@end
