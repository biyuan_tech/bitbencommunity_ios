//
//  BYOfficialCertGroupController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYOfficialCertGroupController.h"
#import "BYOfficialCertGroupViewModel.h"

@interface BYOfficialCertGroupController ()

@end

@implementation BYOfficialCertGroupController

- (Class)getViewModelClass{
    return [BYOfficialCertGroupViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"官方认证社群";
}

- (void)autoReload{
    [(BYOfficialCertGroupViewModel *)self.viewModel autoReload];
}


@end
