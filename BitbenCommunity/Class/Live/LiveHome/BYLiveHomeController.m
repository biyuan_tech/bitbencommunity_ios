//
//  BYLiveHomeController.m
//  BY
//
//  Created by 黄亮 on 2018/7/25.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLiveHomeController.h"
#import "BYLiveHomeViewModel.h"
#import "BYCreatLiveRoomController.h"
#import "BYLiveDefine.h"
#import "BYIMManager.h"

#import "BYLiveHomeControllerV1.h"

@interface BYLiveHomeController ()

@end

@implementation BYLiveHomeController

- (Class)getViewModelClass{
    return [BYLiveHomeViewModel class];
}

+(instancetype)sharedController{
    static BYLiveHomeController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[BYLiveHomeController alloc] init];
    });
    return _sharedAccountModel;
}


-(void)reloadViewInterfaceManager{
    //完成
    [(BYLiveHomeViewModel *)self.viewModel configUI];
    @weakify(self);
    [self actionAutoLoginBlock:nil];
    [self loginSuccessedNotif:^(BOOL isSuccessed) {
        @strongify(self);
        if (isSuccessed){
            [(BYLiveHomeViewModel *)self.viewModel loginReloadData];
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:kiSHaveLiveRoomKey] isEqual:@YES]) return;
            // 校验直播间
            [self verifyLiveRoom];
        } else {
            
        }
    }];
}


- (void)viewDidLoad {
    [super viewDidLoad];
//    @weakify(self);
//    [self rightBarButtonWithTitle:@"新版首页" barNorImage:nil barHltImage:nil action:^{
//        @strongify(self);
//        BYLiveHomeControllerV1 *home = [[BYLiveHomeControllerV1 alloc] init];
//        [self.navigationController pushViewController:home animated:YES];
//    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [(BYLiveHomeViewModel *)self.viewModel reloadlistData];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

// 设置直播间状态
- (void)verifyLiveRoom{
    [[BYLiveHomeRequest alloc] loadReuqestIsHaveLiveRoomSuccessBlock:^(id object) {
        [[NSUserDefaults standardUserDefaults] setObject:@([object boolValue]) forKey:kiSHaveLiveRoomKey];
    } faileBlock:^(NSError *error) {

    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
