//
//  BYHomeBannerModel.h
//  BibenCommunity
//
//  Created by 随风 on 2018/9/28.
//  Copyright © 2018 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BYHomeBannerModel : NSObject

/** 图片地址 */
@property (nonatomic ,copy) NSString *content;
@property (nonatomic ,copy) NSString *banner_id;
@property (nonatomic ,copy) NSString *sort_id;
/** 类型 */
@property (nonatomic ,assign) BY_THEME_TYPE theme_type;
/** 直播类型 */
@property (nonatomic ,assign) BY_NEWLIVE_TYPE live_type;
/** 直播、文章id */
@property (nonatomic ,copy) NSString *theme_id;

+ (NSArray *)getBannerData:(NSArray *)respondArr;

+ (NSArray *)getBannerData:(NSArray *)respondArr msg:(NSArray *)msgArr;

@end
