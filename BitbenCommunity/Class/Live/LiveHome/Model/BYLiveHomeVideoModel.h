//
//  BYLiveHomeVideoModel.h
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYCommonLiveModel.h"
#import "BYLiveDefine.h"

@interface BYLiveHomeVideoModel : BYCommonLiveModel
/** size */
@property (nonatomic ,assign) CGSize titleSize;


- (NSArray *)getRespondData:(NSDictionary *)data type:(BY_LIVE_HOME_LIST_TYPE)type pageNum:(NSInteger)pageNum;
+ (BYLiveHomeVideoModel *)getSingleData:(NSDictionary *)dic;
@end
