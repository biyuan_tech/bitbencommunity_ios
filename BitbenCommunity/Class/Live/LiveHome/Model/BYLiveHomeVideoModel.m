//
//  BYLiveHomeVideoModel.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLiveHomeVideoModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

@implementation BYLiveHomeVideoModel

- (NSArray *)getRespondData:(NSDictionary *)data type:(BY_LIVE_HOME_LIST_TYPE)type pageNum:(NSInteger)pageNum{
    switch (type) {
        case BY_LIVE_HOME_LIST_TYPE_HOT: // 热门推荐
            return [self getHotTypeData:data pageNum:pageNum];
            break;
        case BY_LIVE_HOME_LIST_TYPE_AUDIO: // 音频直播
            return [self getAudioTypeData:data];
            break;
        case BY_LIVE_HOME_LIST_TYPE_ILIVE: // 视频直播
            return [self getILiveTypeData:data];
            break;
        default:                            // 我关注的
            return [self getFollowTypeData:data];
            break;
    }
}

- (NSMutableArray *)getHotTypeData:(NSDictionary *)data pageNum:(NSInteger)pageNum{
    NSMutableArray *tableData = [NSMutableArray array];
    if ([data.allKeys indexOfObject:@"hotRecommend"] != NSNotFound) {
        __block NSMutableArray *hotData = [NSMutableArray array];
        NSArray *hotlist = data[@"hotRecommend"];
        [tableData addObject:@{@"cell":hotData}];
        [hotlist enumerateObjectsUsingBlock:^(NSDictionary *tmpDic, NSUInteger idx, BOOL * _Nonnull stop) {
            BYLiveHomeVideoModel *model = [BYLiveHomeVideoModel getSingleData:tmpDic];
            [hotData addObject:model];
        }];
    }
    if ([data.allKeys indexOfObject:@"historyList"] != NSNotFound) {
        __block NSMutableArray *vodData = [NSMutableArray array];
        NSArray *vodiolist = data[@"historyList"];
        [tableData addObject:@{@"cell":vodData}];
        if (pageNum == 0) {
            BYLiveHomeVideoModel *model = [[BYLiveHomeVideoModel alloc] init];
            model.cellString = @"BYLiveHomeTitleCell";
            model.title = @"音视频回看";
            model.cellHeight = 45;
            [vodData addObject:model];
        }
        [vodiolist enumerateObjectsUsingBlock:^(NSDictionary *tmpDic, NSUInteger idx, BOOL * _Nonnull stop) {
            BYLiveHomeVideoModel *model = [BYLiveHomeVideoModel getSingleData:tmpDic];
            [vodData addObject:model];
        }];
    }
    return tableData;
}

- (NSMutableArray *)getAudioTypeData:(NSDictionary *)data{
    NSMutableArray *tableData = [NSMutableArray array];
    if ([data.allKeys indexOfObject:@"content"] != NSNotFound) {
        __block NSMutableArray *audioData = [NSMutableArray array];
        NSArray *audiolist = data[@"content"];
        [tableData addObject:@{@"cell":audioData}];
        [audiolist enumerateObjectsUsingBlock:^(NSDictionary *tmpDic, NSUInteger idx, BOOL * _Nonnull stop) {
            BYLiveHomeVideoModel *model = [BYLiveHomeVideoModel getSingleData:tmpDic];
            [audioData addObject:model];
        }];
    }
    return tableData;
}

- (NSMutableArray *)getILiveTypeData:(NSDictionary *)data{
    NSMutableArray *tableData = [NSMutableArray array];
    if ([data.allKeys indexOfObject:@"content"] != NSNotFound) {
        __block NSMutableArray *iLiveData = [NSMutableArray array];
        NSArray *iLivelist = data[@"content"];
        [tableData addObject:@{@"cell":iLiveData}];
        [iLivelist enumerateObjectsUsingBlock:^(NSDictionary *tmpDic, NSUInteger idx, BOOL * _Nonnull stop) {
            BYLiveHomeVideoModel *model = [BYLiveHomeVideoModel getSingleData:tmpDic];
            [iLiveData addObject:model];
        }];
    }
    return tableData;
}

- (NSMutableArray *)getFollowTypeData:(NSDictionary *)data{
    NSMutableArray *tableData = [NSMutableArray array];
    if ([data.allKeys indexOfObject:@"content"] != NSNotFound) {
        __block NSMutableArray *followData = [NSMutableArray array];
        NSArray *followlist = data[@"content"];
        [tableData addObject:@{@"cell":followData}];
        [followlist enumerateObjectsUsingBlock:^(NSDictionary *tmpDic, NSUInteger idx, BOOL * _Nonnull stop) {
            BYLiveHomeVideoModel *model = [BYLiveHomeVideoModel getSingleData:tmpDic];
            if (model.status == BY_LIVE_STATUS_SOON) {
                [followData addObject:model];
            }
        }];
    }
    return tableData;
}

+ (BYLiveHomeVideoModel *)getSingleData:(NSDictionary *)dic{
    BYLiveHomeVideoModel *model = [BYLiveHomeVideoModel mj_objectWithKeyValues:dic];
    NSDate *begin_date = [NSDate by_dateFromString:model.begin_time dateformatter:K_D_F];
    model.begin_time = [NSDate by_stringFromDate:begin_date dateformatter:K_S_D_F];
    if (model.courseware.count && !model.video_url.length) {
        model.video_url = model.courseware[0];
    }
    NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.live_title)];
    messageAttributed.yy_firstLineHeadIndent = 33.0f;
    messageAttributed.yy_lineSpacing = 4.0f;
    messageAttributed.yy_font = [UIFont systemFontOfSize:15];
    YYTextContainer *textContainer = [YYTextContainer new];
    textContainer.size = CGSizeMake(kCommonScreenWidth - 172, 42);
    YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
    model.titleSize = CGSizeMake(layout.textBoundingSize.width, layout.textBoundingSize.height);
    
    model.cellString = @"BYLiveHomeVideoCell";
    model.cellHeight = 118;
    return model;
}

@end
