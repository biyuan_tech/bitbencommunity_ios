//
//  BYLiveHomeViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLiveHomeViewModel.h"
#import "BYLiveHomeHeaderView.h"
#import "BYLiveHomeSegmentView.h"
#import "BYLiveHomeSortHeaderView.h"
#import "BYLiveHomeModel.h"
#import "BYCommonLiveModel.h"
#import "BYHomeBannerModel.h"
#import "BYIMManager.h"

#import "BYVideoRoomController.h"
#import "BYILiveRoomController.h"
#import "BYPPTUploadController.h"
#import "BYVODPlayController.h"
#import "BYLiveHomeController.h"
#import "BYPPTRoomController.h"
#import "ArticleDetailRootViewController.h"
#import "BYPersonHomeController.h"
#import "BYVideoLiveController.h"
#import "BYLiveDetailController.h"


#import "DirectManager.h"
#define S_DM ((BYLiveHomeModel *)self.dataModel)
#define K_VC ((BYLiveHomeController *)self.S_VC)
@interface BYLiveHomeViewModel ()<BYCommonTableViewDelegate,BYLiveHomeSegmentViewDelegate>
{
    BY_SORT_TYPE _sort_type;
    //记录scrollView之前的y轴位置，用于判断滑动方向
    CGFloat _hotPosition;
    CGFloat _audioPosition;
    CGFloat _videoPositon;
    CGFloat _followPosition;
}
// 记录各个类型加载的页码
@property (nonatomic ,assign) NSInteger hotPageNum;
@property (nonatomic ,assign) NSInteger audioPageNum;
@property (nonatomic ,assign) NSInteger iLivePageNum;
@property (nonatomic ,assign) NSInteger followPageNum;

@property (nonatomic ,assign) NSInteger currentIndex;

/** 轮播 */
@property (nonatomic ,strong) BYLiveHomeHeaderView *headerView;
/** 分类器 */
@property (nonatomic ,strong) BYLiveHomeSegmentView *segmentView;
/** 音频排序按钮 */
@property (nonatomic ,strong) BYLiveHomeSortHeaderView *audioHeaderView;
/** 视频排序按钮 */
@property (nonatomic ,strong) BYLiveHomeSortHeaderView *videoHeaderView;
@end

static NSInteger baseTag = 0x731;
@implementation BYLiveHomeViewModel

#pragma mark - setUI

- (Class)getDataModelClass{
    return [BYLiveHomeModel class];
}

- (void)viewWillAppear{
    [_headerView startTimer];
}

- (void)viewWillDisappear{
    [_headerView destoryTimer];
}

- (void)configUI{
    _hotPageNum    = 0;
    _audioPageNum  = 0;
    _iLivePageNum  = 0;
    _followPageNum = 0;
    
    [S_V_VIEW addSubview:self.headerView];
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(185);
    }];
    
    [S_V_VIEW addSubview:self.segmentView];
    @weakify(self);
    [_segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.equalTo(self.headerView.mas_bottom).with.offset(0);
        make.left.right.bottom.mas_equalTo(0);
    }];
    
    [self loadRequestGetBanner];
}

- (void)setContentView{

}

- (void)loginReloadData{
    for (int i = 0; i < 3; i ++) {
        BYCommonTableView *tableView = [self.segmentView viewWithTag:baseTag + i];
        if (tableView && tableView.tableData.count) {
            [self loadRequestWithIndex:i isCurSort:NO isReload:YES];
        }
        if (!tableView.tableData && i == 0) {
            [self loadRequestWithIndex:i isCurSort:NO isReload:YES];
        }
    }
}

// viewwillappear 刷新整个列表
- (void)reloadlistData{
    for (int i = 0; i < 4; i ++) {
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                BYCommonTableView *tableView = [_segmentView viewWithTag:baseTag + i];
                if (tableView) {
                    [self loadRequestWithIndex:i isCurSort:NO isReload:YES];
                }
            });
        });
    }
}

#pragma mark - customMethod
// 数据刷新
- (void)refreshData{
    switch (self.currentIndex) {
        case 0:
            self.hotPageNum     = 0;
            break;
        case 1:
            self.audioPageNum   = 0;
            break;
        case 2:
            self.iLivePageNum   = 0;
            break;
        default:
            self.followPageNum  = 0;
            break;
    }
    if (self.currentIndex == 0) {
        [self loadRequestGetBanner];
    }
    [self loadRequestWithIndex:self.currentIndex isCurSort:NO isReload:YES];
}

// 数据加载更多
- (void)loadMoreData{
//    [_tableView endRefreshing];
    [self loadRequestWithIndex:self.currentIndex isCurSort:NO isReload:NO];
}

- (void)headerViewTitleSelectIndex:(NSInteger)index{
    
}

- (void)headerViewDidSelectAdvertIndex:(NSInteger)index{
    BYHomeBannerModel *bannerModel = _headerView.advertData[index];
    // banner点击统计
    [[BYLiveHomeRequest alloc] loadRequestClickBanner:bannerModel.banner_id];
    
    if (bannerModel.theme_type == BY_THEME_TYPE_ARTICLE) { // 跳文章
        ArticleDetailRootViewController *articleController = [[ArticleDetailRootViewController alloc] init];
        articleController.transferArticleId = bannerModel.theme_id;
        articleController.transferPageType = ArticleDetailRootViewControllerTypeBanner;
        [S_VC authorizePush:articleController animation:YES];
        
    }else{
        [DirectManager commonPushControllerWithThemeType:bannerModel.theme_type liveType:bannerModel.live_type themeId:bannerModel.theme_id];
    }
//    else if (bannerModel.theme_type == BY_THEME_TYPE_LIVE) { // 跳转直播
//        switch (bannerModel.live_type) {
//            case BY_NEWLIVE_TYPE_ILIVE:
//            {
//                BYLiveConfig *config = [[BYLiveConfig alloc] init];
//                config.isHost = NO;
//                BYILiveRoomController *ilivewRoomController = [[BYILiveRoomController alloc] initWithConfig:config];
//                ilivewRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
//                [S_VC authorizePush:ilivewRoomController animation:YES];
//            }
//                break;
//            case BY_NEWLIVE_TYPE_UPLOAD_VIDEO:
//            {
//                BYVideoRoomController *videoRoomController = [[BYVideoRoomController alloc] init];
//                videoRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
//                [S_VC authorizePush:videoRoomController animation:YES];
//                
//            }
//                break;
//            case BY_NEWLIVE_TYPE_AUDIO_PPT:
//            case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
//            {
////                BYPPTRoomController *pptRoomController = [[BYPPTRoomController alloc] init];
////                pptRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
////                pptRoomController.isHost = NO;
//                BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
//                liveDetailController.live_record_id = nullToEmpty(bannerModel.theme_id);
//                liveDetailController.isHost = NO;
//                [S_VC authorizePush:liveDetailController animation:YES];
//            }
//                break;
//            case BY_NEWLIVE_TYPE_VOD_AUDIO:
//            case BY_NEWLIVE_TYPE_VOD_VIDEO:
//            {
//                BYVODPlayController *vodPlayController = [[BYVODPlayController alloc] init];
//                vodPlayController.live_record_id = nullToEmpty(bannerModel.theme_id);
//                [S_VC authorizePush:vodPlayController animation:YES];
//            }
//                break;
//            case BY_NEWLIVE_TYPE_VIDEO:
//            {
//                BYVideoLiveController *videoLiveController = [[BYVideoLiveController alloc] init];
//                videoLiveController.live_record_id = nullToEmpty(bannerModel.theme_id);
//                videoLiveController.isHost = NO;
//                [S_VC authorizePush:videoLiveController animation:YES];
//            }
//                break;
//            default:
//                break;
//        }
//    }
//    else if (bannerModel.theme_type == BY_THEME_TYPE_APP_H5) { // 跳转app内部h5
//        PDWebViewController *webViewController = [[PDWebViewController alloc] init];
//        [webViewController webDirectedWebUrl:bannerModel.theme_id];
//        [S_VC authorizePush:webViewController animation:YES];
//    }
}

#pragma mark - requestMethod
- (void)loadRequestWithIndex:(NSInteger)index pageNum:(NSInteger)pageNum sortType:(BY_SORT_TYPE)sortType isCurSort:(BOOL)isCurSort{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestLiveHome:index pageNum:pageNum sortType:sortType successBlock:^(id object) {
        @strongify(self);
        BYCommonTableView *tableView = (BYCommonTableView *)[self.segmentView viewWithTag:baseTag + index];
        [tableView endRefreshing];
        [tableView dismissListAnimation];
//        if (index == 3) { // 暂不加载我的关注
//            return ;
//        }
        if (![object count]) return;
        if (pageNum == 0) {
            tableView.tableData = object;
        }
        else{
            NSMutableArray *tmpTableData = [NSMutableArray array];
            [tmpTableData addObjectsFromArray:tableView.tableData];
            [tmpTableData addObjectsFromArray:object];
            tableView.tableData = tmpTableData;
        }
        switch (index) {
            case 0:
                self.hotPageNum++;
                break;
            case 1:
                self.audioPageNum++;
                break;
            case 2:
                self.iLivePageNum++;
                break;
            default:
                self.followPageNum++;
                break;
        }

        if (index == 1 && isCurSort) {
            [self.audioHeaderView reloadSortStatus];
        }
        else if (index == 2 && isCurSort){
            [self.videoHeaderView reloadSortStatus];
        }
    } faileBlock:^(NSError *error) {
        @strongify(self);
        BYCommonTableView *tableView = (BYCommonTableView *)[self.segmentView viewWithTag:baseTag + index];
        [tableView endRefreshing];
        [tableView dismissListAnimation];
    }];
}

- (void)loadRequestWithIndex:(NSInteger)index isCurSort:(BOOL)isCurSort isReload:(BOOL)isReload{
    NSInteger pageNum = 0;
    BY_SORT_TYPE sortType = 0;
    switch (index) {
        case 0:
            _hotPageNum = isReload ? 0 : _hotPageNum;
            pageNum = _hotPageNum;
            break;
        case 1:
            _audioPageNum = isReload ? 0 : _audioPageNum;
            pageNum = _audioPageNum;
            if (isCurSort) { // 是否切换排序方式
                sortType = _audioHeaderView.sort_type == BY_SORT_TYPE_HOT ? BY_SORT_TYPE_TIME : BY_SORT_TYPE_HOT;
                break;
            }
            sortType = _audioHeaderView.sort_type;
            break;
        case 2:
            _iLivePageNum = isReload ? 0 : _iLivePageNum;
            pageNum = _iLivePageNum;
            if (isCurSort) { // 是否切换排序方式
                sortType = _videoHeaderView.sort_type == BY_SORT_TYPE_HOT ? BY_SORT_TYPE_TIME : BY_SORT_TYPE_HOT;
                break;
            }
            sortType = _videoHeaderView.sort_type;
            break;
        default:
            _followPageNum = isReload ? 0 : _followPageNum;
            pageNum = _followPageNum;
            break;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        BYCommonTableView *tableView = (BYCommonTableView *)[self.segmentView viewWithTag:baseTag + index];
        if (!tableView.tableData.count) {
            [tableView showListLoadingAnimation];
        }
    });
    [self loadRequestWithIndex:index pageNum:pageNum sortType:sortType isCurSort:isCurSort];
}

- (void)loadRequestAttentionLive:(BYLiveHomeVideoModel *)model tableView:(BYCommonTableView *)tableView indexPath:(NSIndexPath *)indexPath{
    [[BYLiveHomeRequest alloc] loadRequestAttentionLive:model.live_record_id successBlock:^(id object) {
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYLiveHomeVideoModel *model = dic.allValues[0][indexPath.row];
        model.attention = [object boolValue];
        [tableView reloadData];
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)loadRequestCancelAttentionLive:(BYLiveHomeVideoModel *)model tableView:(BYCommonTableView *)tableView indexPath:(NSIndexPath *)indexPath{
    [[BYLiveHomeRequest alloc] loadRequestCancelAttentionLive:model.live_record_id successBlock:^(id object) {
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYLiveHomeVideoModel *model = dic.allValues[0][indexPath.row];
        model.attention = [object boolValue];
        if (tableView.tag - baseTag == 3) { // 我关注的需移除该数据
            NSMutableArray *tmpArr = [NSMutableArray arrayWithArray:dic.allValues[0]];
            [tmpArr removeObject:model];
            tableView.tableData = @[@{@"cell":tmpArr}];
        }
        [tableView reloadData];
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)loadRequestGetBanner{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetHomeBannerSuccessBlock:^(id object) {
        @strongify(self);
        self.headerView.advertData = object;
        [self.headerView reloadData];
    } faileBlock:^(NSError *error) {
        PDLog(@"%@",error);
    }];
}

#pragma mark - BYLiveHomeSegmentViewDelegate

- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    BYCommonTableView *tableView = [[BYCommonTableView alloc] init];
    tableView.group_delegate = self;
    tableView.tag = baseTag + index;
    [tableView addHeaderRefreshTarget:self action:@selector(refreshData)];
    if (index != 0) {
        [tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    }
    // 添加排序按钮
    if (index == 1) {
        tableView.tableHeaderView = self.audioHeaderView;
    }
    else if (index == 2){
        tableView.tableHeaderView = self.videoHeaderView;
    }
    return tableView;
}

- (void)by_segmentViewDidScrollToIndex:(NSInteger)index{
    _currentIndex = index;
    BYCommonTableView *tableView = (BYCommonTableView *)[S_V_VIEW viewWithTag:baseTag + index];
    if (tableView.contentOffset.y < kCommonScreenHeight && index == 3) {
        [self showHeaderViewAnimation:YES];
    }
    if (!tableView.tableData.count) { // 当前分类数据为空时发起请求
        [self loadRequestWithIndex:index isCurSort:NO isReload:YES];
    }
    
    // 埋点
    
    if (index == 0){        // 首页热门推荐
        [MTAManager event:MTATypeHotLiveHomeHot params:nil];
    } else if (index == 1){ // 音频直播
        [MTAManager event:MTATypeHotLiveHomeJianjie params:nil];
    } else if (index == 2){ // 视频直播
        [MTAManager event:MTATypeHotLiveHomeLive params:nil];
    } else if (index == 3){ // 我关注的
        [MTAManager event:MTATypeHotLiveHomeDianping params:nil];
    }
    
    
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    NSDictionary *dic = tableView.tableData[indexPath.section];
    BYLiveHomeVideoModel *model = dic.allValues[0][indexPath.row];
    switch (model.live_type) {
        case BY_NEWLIVE_TYPE_ILIVE:
        {
            BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
            liveDetailController.live_record_id = model.live_record_id;
            liveDetailController.isHost = NO;
            liveDetailController.navigationTitle = @"个人互动直播";
            [S_VC authorizePush:liveDetailController animation:YES];
        }
            break;
        case BY_NEWLIVE_TYPE_UPLOAD_VIDEO:
        {
            BYVideoRoomController *videoRoomController = [[BYVideoRoomController alloc] init];
            videoRoomController.live_record_id = model.live_record_id;
            [S_VC authorizePush:videoRoomController animation:YES];
        }
            break;
        case BY_NEWLIVE_TYPE_AUDIO_PPT:
        case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
        {
            BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
            liveDetailController.live_record_id = model.live_record_id;
            liveDetailController.isHost = NO;
            liveDetailController.navigationTitle = @"微直播";
            [S_VC authorizePush:liveDetailController animation:YES];
        }
            break;
        case BY_NEWLIVE_TYPE_VOD_AUDIO:
        case BY_NEWLIVE_TYPE_VOD_VIDEO:
        {
            BYVODPlayController *vodPlayController = [[BYVODPlayController alloc] init];
            vodPlayController.live_record_id = model.live_record_id;
            vodPlayController.naTitle = model.live_title;
            [S_VC authorizePush:vodPlayController animation:YES];
        }
            break;
        case BY_NEWLIVE_TYPE_VIDEO:
        {
            BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
            liveDetailController.live_record_id = model.live_record_id;
            liveDetailController.isHost = NO;
            liveDetailController.navigationTitle = @"推流直播";
            [S_VC authorizePush:liveDetailController animation:YES];
        }
            break;
        default:
            break;
    }
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = tableView.tableData[indexPath.section];
    BYLiveHomeVideoModel *model = dic.allValues[0][indexPath.row];
    if ([actionName isEqualToString:@"appointAction"]) { // 关注
        if (model.attention) {
            @weakify(self);
            [S_VC authorizeWithCompletionHandler:^(BOOL successed) { // 判断登录状态
                @strongify(self);
                if (!successed) return ;
                [self loadRequestCancelAttentionLive:model tableView:tableView indexPath:indexPath];
            }];
            return;
        }
        @weakify(self);
        [S_VC authorizeWithCompletionHandler:^(BOOL successed) { // 判断登录状态
            @strongify(self);
            if (!successed) return ;
            [self loadRequestAttentionLive:model tableView:tableView indexPath:indexPath];
        }];
    }
    else if ([actionName isEqualToString:@"headImgAction"]){ // 头像点击
        BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
        personHomeController.user_id = model.user_id;
        [S_VC authorizePush:personHomeController animation:YES];
    }
}

- (void)by_tableViewDidScroll:(UIScrollView *)scrollView{
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    CGFloat maximumOffset = size.height;
    
    // 滑动区域小于展示y区域或滑动区域大于展示区域不超过30
    if (maximumOffset < (bounds.size.height + inset.bottom) || maximumOffset - (bounds.size.height + inset.bottom) < 30) {
        return;
    }
    
    CGFloat temOffsetY = 0;
    switch (scrollView.tag - baseTag) {
        case 0:
            temOffsetY = _hotPosition;
            _hotPosition = scrollView.contentOffset.y;
            break; 
        case 1:
            temOffsetY = _audioPosition;
            _audioPosition = scrollView.contentOffset.y;
            break;
        case 2:
            temOffsetY = _videoPositon;
            _videoPositon = scrollView.contentOffset.y;
            break;
        default:
            temOffsetY = _followPosition;
            _followPosition = scrollView.contentOffset.y;
            break;
    }
 
    if (temOffsetY >= 120) {
        if (scrollView.contentOffset.y - temOffsetY > 0 && scrollView.decelerating) {
            [self showHeaderViewAnimation:NO];
        }
    }
    else{
        if (scrollView.contentOffset.y - temOffsetY <= 0 && scrollView.decelerating) {
            [self showHeaderViewAnimation:YES];
        }
    }
        
}

- (void)showHeaderViewAnimation:(BOOL)isShow{
    [S_V_VIEW updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.3 animations:^{
        [self.headerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(isShow ? 0 : -195);
        }];
        [S_V_VIEW layoutIfNeeded];
    }];
}

#pragma make - initMethod
- (BYLiveHomeHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[BYLiveHomeHeaderView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, 195)];
        @weakify(self);
        _headerView.didSelectHeaderTitle = ^(NSInteger index) {
            @strongify(self);
            [self headerViewTitleSelectIndex:index];
        };
        _headerView.didSelectAdvertIndex = ^(NSInteger index) {
            @strongify(self);
            [self headerViewDidSelectAdvertIndex:index];
        };
    }
    return _headerView;
}

- (BYLiveHomeSegmentView *)segmentView{
    if (!_segmentView) {
        _segmentView = [[BYLiveHomeSegmentView alloc] initWithTitles:@"热门推荐",@"音频直播",@"视频直播",@"我关注的", nil];
        _segmentView.delegate = self;
    }
    return _segmentView;
}

- (BYLiveHomeSortHeaderView *)audioHeaderView{
    if (!_audioHeaderView) {
        _audioHeaderView = [[BYLiveHomeSortHeaderView alloc] init];
        _audioHeaderView.frame = CGRectMake(0, 0, kCommonScreenWidth, 36);
        
        @weakify(self);
        _audioHeaderView.didSelectSortBtnHandle = ^{
            @strongify(self);
            self.audioPageNum = 0;
            [self loadRequestWithIndex:self.currentIndex isCurSort:YES isReload:YES];
        };
    }
    return _audioHeaderView;
}

- (BYLiveHomeSortHeaderView *)videoHeaderView{
    if (!_videoHeaderView) {
        _videoHeaderView = [[BYLiveHomeSortHeaderView alloc] init];
        _videoHeaderView.frame = CGRectMake(0, 0, kCommonScreenWidth, 36);
        @weakify(self);
        _videoHeaderView.didSelectSortBtnHandle = ^{
            @strongify(self);
            self.iLivePageNum = 0;
            [self showHeaderViewAnimation:YES];
            [self loadRequestWithIndex:self.currentIndex isCurSort:YES isReload:YES];
        };
    }
    return _videoHeaderView;
}

@end
