//
//  BYLiveHomeHeaderView.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLiveHomeHeaderView.h"
#import "iCarousel.h"
#import "BYHomeBannerModel.h"

@interface BYLiveHomeHeaderView()<iCarouselDelegate,iCarouselDataSource>

// **广告轮播
@property (nonatomic ,strong) iCarousel *adScrollerView;

@property (nonatomic ,strong) NSTimer *timer;
@property (nonatomic ,strong) UIPageControl *pageControl;
@end

@implementation BYLiveHomeHeaderView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setContentView];
    }
    return self;
}

- (void)setContentView{
    [self addSubview:self.adScrollerView];
    [self addSubview:self.pageControl];
    [_pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(40);
    }];
}

- (void)reloadData{
    [_adScrollerView reloadData];
    self.pageControl.numberOfPages = _advertData.count;
    self.pageControl.currentPage = _adScrollerView.currentItemIndex;
    [self startTimer];
}

- (void)startTimer{
    if (!_advertData.count) return;
    [self destoryTimer];
    if (!_timer) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(scrollAdBanner) userInfo:nil repeats:YES];
    }
}

- (void)destoryTimer{
    if (_timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)scrollAdBanner{
    NSInteger index;
    if (_adScrollerView.currentItemIndex < _advertData.count - 1)
        index = _adScrollerView.currentItemIndex + 1;
    else
        index = 0;
    [_adScrollerView scrollToItemAtIndex:index duration:0.5];
}

#pragma mark - iCarouselDelegate/DataScoure
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return _advertData.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view{
    if (view == nil) {
        view = [[PDImageView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, CGRectGetHeight(carousel.frame))];
        ((PDImageView *)view).contentMode = UIViewContentModeScaleAspectFill;
        ((PDImageView *)view).clipsToBounds = YES;
    }
    BYHomeBannerModel *model = _advertData[index];
    [(PDImageView *)view uploadMainImageWithURL:model.content placeholder:nil imgType:PDImgTypeOriginal callback:nil];
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    switch (option) {
        // 设置滚动循环
        case iCarouselOptionWrap:
            return YES;
            break;
        default:
            break;
    }
    return value;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    if (self.didSelectAdvertIndex) {
        self.didSelectAdvertIndex(index);
    }
}

- (void)carouselWillBeginDragging:(iCarousel *)carousel{
    [self destoryTimer];
}

- (void)carouselDidEndDragging:(iCarousel *)carousel willDecelerate:(BOOL)decelerate{
    [self startTimer];
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
    _pageControl.currentPage = carousel.currentItemIndex;
}

#pragma makr - initMethod

- (iCarousel *)adScrollerView{
    if (!_adScrollerView) {
        _adScrollerView = [[iCarousel alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, 195)];
        _adScrollerView.delegate = self;
        _adScrollerView.dataSource = self;
        _adScrollerView.pagingEnabled = YES;
    }
    return _adScrollerView;
}

- (UIPageControl *)pageControl{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.pageIndicatorTintColor = kBgColor_237;
        _pageControl.currentPageIndicatorTintColor = kTextColor_238;
    }
    return _pageControl;
}

@end
