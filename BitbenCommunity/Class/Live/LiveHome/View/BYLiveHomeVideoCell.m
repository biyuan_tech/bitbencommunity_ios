//
//  BYLiveHomeVideoCell.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYLiveHomeVideoCell.h"
#import "BYLiveHomeVideoModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

@interface BYLiveHomeVideoCell()

/** 封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogoImgView;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 组织 */
@property (nonatomic ,strong) YYLabel *organLab;
/** 标题 */
@property (nonatomic ,strong) YYLabel *titleLab;

@property (nonatomic ,strong) UIView *timerView;
/** 时间 */
@property (nonatomic ,strong) UILabel *timeLab;

@property (nonatomic ,strong) UIView *watchNumView;
/** 观看人数 */
@property (nonatomic ,strong) UILabel *watchNumLab;
/** 直播状态 */
@property (nonatomic ,strong) UIView *liveStatus;
/** 直播状态 */
@property (nonatomic ,strong) UILabel *liveStatusLab;
/** 预约按钮 */
@property (nonatomic ,strong) UIButton *appointBtn;

@end

@implementation BYLiveHomeVideoCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYLiveHomeVideoModel *model;
    if ([object[indexPath.section] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dic = object[indexPath.section];
        model = dic.allValues[0][indexPath.row];
    }else{
        model = object[indexPath.row];
    }
    self.indexPath = indexPath;
//    @weakify(self);
//    [_coverImgView uploadImageWithURL:model.live_cover_url placeholder:nil callback:nil];
    [_coverImgView uploadHDImageWithURL:model.live_cover_url callback:nil];
    
    NSMutableAttributedString *titleAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.live_title)];
    titleAttributed.yy_firstLineHeadIndent = model.organ_type == BY_LIVE_ORGAN_TYPE_LECTURER ? 40 : 33.0f;
    titleAttributed.yy_lineSpacing = 4.0f;
    titleAttributed.yy_color = kColorRGBValue(0x303030);
    titleAttributed.yy_font = [UIFont systemFontOfSize:15];
    _titleLab.attributedText = titleAttributed;
    [_titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(model.titleSize.height);
    }];
    
    _timeLab.text = model.begin_time;
    _userlogoImgView.style = model.cert_badge;
    [_userlogoImgView uploadHDImageWithURL:model.head_img callback:nil];
    _userNameLab.text = model.nickname;
    [self reloadLiveStatus:model.status liveType:model.live_type];
    _organLab.text = model.organ_type == BY_LIVE_ORGAN_TYPE_PERSON ? @"个人" : (model.organ_type == BY_LIVE_ORGAN_TYPE_LECTURER ? @"产业人" : @"机构");
    _appointBtn.selected = model.attention;
    
    if (model.organ_type == BY_LIVE_ORGAN_TYPE_LECTURER) {
        [self.organLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(37);
        }];
    }
    
    if (model.status == BY_LIVE_STATUS_SOON) {
        if (model.live_type == 6 ||
            model.live_type == 7) {
            _appointBtn.hidden = YES;
        }
        else{
            _appointBtn.hidden = [[AccountModel sharedAccountModel] hasLoggedIn] ? NO : YES;
        }
    }
    else{
        _appointBtn.hidden = YES;
    }
//    _appointBtn.hidden = model.status == BY_LIVE_STATUS_SOON ? (model.live_type == 6 ? YES : NO) : YES;
    if (model.status == BY_LIVE_STATUS_SOON) {
        if (model.live_type == BY_NEWLIVE_TYPE_VOD_VIDEO ||
            model.live_type == BY_NEWLIVE_TYPE_VOD_AUDIO ) {
            _watchNumView.layer.opacity = 1.0;
        }
        else{
            _watchNumView.layer.opacity = 0.0;
        }
    }else{
        _watchNumView.layer.opacity = 1.0;
    }
    _watchNumLab.text = stringFormatInteger(model.watch_times);
}

- (void)appointBtnAction:(UIButton *)sender{
    // 埋点
    NSDictionary *params = @{@"预约直播":self.titleLab.text.length?self.titleLab.text:@"找不到直播标题"};
    [MTAManager event:MTATypeHotLiveYuyue params:params];
    
    [self sendActionName:@"appointAction" param:@{@"btn":sender} indexPath:self.indexPath];
}

- (void)headerImgAction{
    [self sendActionName:@"headImgAction" param:nil indexPath:self.indexPath];
}

- (void)reloadLiveStatus:(BY_LIVE_STATUS)status liveType:(BY_NEWLIVE_TYPE)liveType{
    if (liveType == BY_NEWLIVE_TYPE_VOD_AUDIO ||
        liveType == BY_NEWLIVE_TYPE_VOD_VIDEO) {
        _liveStatus.backgroundColor = kColorRGBValue(0xed4a45);
        _liveStatusLab.text = liveType == BY_NEWLIVE_TYPE_VOD_AUDIO ? @"音频" : @"视频";
        return;
    }
    switch (status) {
        case BY_LIVE_STATUS_LIVING:
            _liveStatus.backgroundColor = kColorRGBValue(0xed4a45);
            _liveStatusLab.text = @"正在直播";
            break;
        case BY_LIVE_STATUS_SOON:
            _liveStatus.backgroundColor = kColorRGBValue(0x00a43e);
            _liveStatusLab.text = @"直播预告";
            break;
        case BY_LIVE_STATUS_END:
        case BY_LIVE_STATUS_OVERTIME:
            _liveStatus.backgroundColor = kColorRGBValue(0xed9c45);
            _liveStatusLab.text = @"直播回放";
            break;
        default:
            break;
    }
}

- (NSDictionary *)getTitleAttributes{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    //首行缩进
    paragraphStyle.firstLineHeadIndent = 33;
    //富文本样式
    NSDictionary *attributeDic = @{
                                   NSFontAttributeName : [UIFont systemFontOfSize:15],
                                   NSParagraphStyleAttributeName : paragraphStyle,
                                   NSForegroundColorAttributeName : kTextColor_71
                                   };
    return attributeDic;
}

- (void)setContentView{
    // 封面图
    PDImageView *coverImgView = [[PDImageView alloc] init];
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.layer.cornerRadius = 5.0;
    coverImgView.clipsToBounds = YES;
    [self.contentView addSubview:coverImgView];
    _coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(132);
        make.height.mas_equalTo(97);
    }];
    
    UIView *maskView= [UIView by_init];
    [maskView setBackgroundColor:kColorRGB(47, 47, 47, 0.6)];
    [maskView layerCornerRadius:5 byRoundingCorners:UIRectCornerTopLeft size:CGSizeMake(60, 23)];
    [self.contentView addSubview:maskView];
    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(23);
    }];
    
    // 直播状态
    UIView *liveStatus = [UIView by_init];
    liveStatus.layer.cornerRadius = 2;
    [maskView addSubview:liveStatus];
    _liveStatus = liveStatus;
    [liveStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(4);
        make.left.mas_equalTo(5);
        make.centerY.mas_equalTo(0);
    }];
    
    // 直播状态文案
    UILabel *liveStatusLab = [UILabel by_init];
    liveStatusLab.text = @"直播回放";
    [liveStatusLab setBy_font:11];
    [liveStatusLab setTextColor:[UIColor whiteColor]];
    [maskView addSubview:liveStatusLab];
    _liveStatusLab = liveStatusLab;
    [liveStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(liveStatus.mas_right).with.offset(3);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(stringGetWidth(liveStatusLab.text, liveStatusLab.font.pointSize));
        make.height.mas_equalTo(stringGetHeight(liveStatusLab.text, liveStatusLab.font.pointSize));
    }];
    
    // 组织
    YYLabel *organLab = [[YYLabel alloc] init];
    organLab.font = [UIFont systemFontOfSize:10];
    organLab.textColor = [UIColor whiteColor];
    organLab.textAlignment = NSTextAlignmentCenter;
    organLab.backgroundColor = kTextColor_68;
    organLab.layer.cornerRadius = 3.0f;
//    [organLab layerCornerRadius:3 size:CGSizeMake(30, 16)];
    [self.contentView addSubview:organLab];
    _organLab = organLab;
    [organLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(coverImgView.mas_right).with.offset(10);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(16);
    }];
    
    // 标题
    YYLabel *titleLab = [[YYLabel alloc] init];
    titleLab.font = [UIFont systemFontOfSize:15];
    titleLab.textColor = kTextColor_48;
    titleLab.numberOfLines = 2;
    [self.contentView addSubview:titleLab];
    _titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(coverImgView.mas_right).with.offset(10);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(15);
    }];
    
    UIView *timerView = [self getTimerView];
    _timerView = timerView;
    [self.contentView addSubview:timerView];
    [timerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(coverImgView.mas_right).with.offset(10);
        make.top.equalTo(titleLab.mas_bottom).with.offset(10);
        make.right.mas_equalTo(-50);
        make.height.mas_equalTo(20);
    }];
    
    // 头像
    PDImageView *userlogoImgView = [PDImageView by_init];
    userlogoImgView.contentMode = UIViewContentModeScaleAspectFill;
    userlogoImgView.userInteractionEnabled = YES;
    [self.contentView addSubview:userlogoImgView];
    _userlogoImgView = userlogoImgView;
    [userlogoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(22);
        make.bottom.equalTo(coverImgView.mas_bottom).with.offset(-2);
        make.left.equalTo(coverImgView.mas_right).with.offset(10);
    }];
    [userlogoImgView addCornerRadius:11 size:CGSizeMake(22, 22)];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headerImgAction)];
    [userlogoImgView addGestureRecognizer:tapGestureRecognizer];
    
    UIImageView *userlogoImgBgView = [UIImageView by_init];
    [userlogoImgBgView by_setImageName:@"livehome_userlogo_bg"];
    [self.contentView addSubview:userlogoImgBgView];
    [self.contentView insertSubview:userlogoImgBgView belowSubview:userlogoImgView];
    [userlogoImgBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(userlogoImgBgView.image.size.width);
        make.height.mas_equalTo(userlogoImgBgView.image.size.height);
        make.centerX.mas_equalTo(userlogoImgView.mas_centerX);
        make.centerY.mas_equalTo(userlogoImgView.mas_centerY);
    }];
    
    // 昵称
    UILabel *userNameLab = [UILabel by_init];
    [userNameLab setBy_font:12];
    userNameLab.textColor = kTextColor_77;
    _userNameLab = userNameLab;
    [self.contentView addSubview:userNameLab];
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userlogoImgView.mas_right).with.offset(7);
        make.centerY.mas_equalTo(userlogoImgView.mas_centerY);
        make.right.mas_equalTo(-70);
        make.height.mas_equalTo(14);
    }];
    
    // 直播观看人数view
    UIView *watchNumView = [self getWatchNumView];
    _watchNumView = watchNumView;
    watchNumView.layer.opacity = 0.0;
    [self.contentView addSubview:watchNumView];
    [watchNumView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(13);
        make.centerY.mas_equalTo(userNameLab.mas_centerY);
        make.width.mas_equalTo(60);
    }];
    
    // 预约按钮
    UIButton *appointBtn = [UIButton by_buttonWithCustomType];
    [appointBtn setBy_imageName:@"livehome_btn_appoint" forState:UIControlStateNormal];
    [appointBtn setBy_imageName:@"livehome_btn_isappoint" forState:UIControlStateSelected];
    [appointBtn addTarget:self action:@selector(appointBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:appointBtn];
    _appointBtn = appointBtn;
    [appointBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(52);
        make.height.mas_equalTo(23);
        make.right.mas_equalTo(-kCellRightSpace);
        make.bottom.mas_equalTo(coverImgView.mas_bottom);
    }];
}

- (UIView *)getTimerView{
    UIView *timerView = [UIView by_init];
    // 时间图标
    UIImageView *timeImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"common_time_icon"]];
    [timerView addSubview:timeImgView];
    [timeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(timeImgView.image.size.width);
        make.height.mas_equalTo(timeImgView.image.size.height);
    }];
    
    // 时间
    UILabel *timeLab = [[UILabel alloc] init];
    timeLab.font = [UIFont systemFontOfSize:12];
    timeLab.textColor = kTextColor_154;
    [timerView addSubview:timeLab];
    _timeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeImgView.mas_right).with.offset(5);
        make.centerY.equalTo(timeImgView.mas_centerY).with.offset(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(14);
    }];
    return timerView;
}

- (UIView *)getWatchNumView{
    UIView *watchNumView = [UIView by_init];
    UIImageView *watchNumImgView = [UIImageView by_init];
    [watchNumImgView by_setImageName:@"livehome_watchNum"];
    [watchNumView addSubview:watchNumImgView];
    [watchNumImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.width.mas_equalTo(watchNumImgView.image.size.width);
        make.height.mas_equalTo(watchNumImgView.image.size.height);
    }];
    
    // 直播人数
    UILabel *watchNumLab = [UILabel by_init];
    [watchNumLab setBy_font:12];
    watchNumLab.textColor = kTextColor_154;
    [watchNumView addSubview:watchNumLab];
    self.watchNumLab = watchNumLab;
    [watchNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(watchNumImgView.mas_right).with.offset(5);
        make.centerY.mas_equalTo(watchNumImgView.mas_centerY);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(11);
    }];
    return watchNumView;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
