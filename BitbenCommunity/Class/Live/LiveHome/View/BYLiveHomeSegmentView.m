//
//  BYLiveHomeSegmentView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYLiveHomeSegmentView.h"
#import "UIScrollView+Gesture.h"
#import "BYLiveHomeVideoModel.h"

@interface BYLiveHomeSegmentView()<UIScrollViewDelegate>
{
    /** subView width */
    CGFloat _sub_width;
    NSInteger _index;
    NSInteger _lastIndex;
    CGFloat _beginDragOffsetX;
}

/** 标题存储，也用于初始化子View */
@property (nonatomic ,strong) NSArray *titles;

/** 存储headerView按钮 */
@property (nonatomic ,strong) NSArray *buttons;

/** headerView */
@property (nonatomic ,strong) UIView *headerView;

/** contentView,subView容器 */
@property (nonatomic ,strong) UIScrollView *scrollView;

@end

static NSInteger baseTag = 0X5730;
@implementation BYLiveHomeSegmentView
- (instancetype)initWithTitles:(NSString *)titles, ... NS_REQUIRES_NIL_TERMINATION{
    NSMutableArray *arrays = [NSMutableArray array];
    va_list argList;
    if (titles.length){
        [arrays addObject:titles];
        va_start(argList, titles);
        id temp;
        while ((temp = va_arg(argList, id))){
            [arrays addObject:temp];
        }
    }
    BYLiveHomeSegmentView *segmentView = [[BYLiveHomeSegmentView alloc] init];
    segmentView.titles = arrays;
    [segmentView setContentView];
    segmentView.defultSelIndex = 0;
    segmentView->_lastIndex = 0;
    return segmentView;
}

- (void)layoutSubviews{
    [super layoutSubviews];
}

- (void)setDelegate:(id<BYLiveHomeSegmentViewDelegate>)delegate{
    _delegate = delegate;
    if ([self.delegate respondsToSelector:@selector(by_segmentViewDidScrollToIndex:)]) {
        [self.delegate by_segmentViewDidScrollToIndex:_defultSelIndex];
    }
    UIView *view = [_scrollView viewWithTag:2*baseTag + _defultSelIndex];
    if (view.subviews.count == 0 && view) {
        if ([self.delegate respondsToSelector:@selector(by_segmentViewLoadSubView:)]) {
            UIView *subView = [self.delegate by_segmentViewLoadSubView:_defultSelIndex];
            [view addSubview:subView];
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
        }
    }
}

#pragma mark - customMehtod
- (void)didSelectSubViewIndex:(NSInteger)index{
    // 设置高亮
    UIButton *button = [self viewWithTag:baseTag + index];
    for (UIButton *btn in self.buttons) {
        [UIView animateWithDuration:0.1 animations:^{
            btn.selected = button == btn ? YES : NO;
        }];
    }
    _lastIndex = index;
}

#pragma mark - buttonAction
- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - baseTag;
    // 如与上次选择一样则返回
    if (index == _lastIndex) return;
    [_scrollView setContentOffset:CGPointMake(kCommonScreenWidth*index, 0) animated:YES];
}

- (void)setDefultSelIndex:(NSInteger)defultSelIndex{
    _defultSelIndex = defultSelIndex;
    [self didSelectSubViewIndex:defultSelIndex];
}

#pragma mark - scrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat indexFloat = (CGFloat)scrollView.contentOffset.x/kCommonScreenWidth;
    NSInteger index = ceil(scrollView.contentOffset.x/kCommonScreenWidth);
    if (indexFloat < index) {
        index = round(scrollView.contentOffset.x/kCommonScreenWidth);
    }
    UIView *view = [_scrollView viewWithTag:2*baseTag + index];
    if (view.subviews.count == 0 && view) {
        if ([self.delegate respondsToSelector:@selector(by_segmentViewLoadSubView:)]) {
            UIView *subView = [self.delegate by_segmentViewLoadSubView:index];
            [view addSubview:subView];
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    BOOL scrollToScrollStop = !scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
    if (scrollToScrollStop) {
        [self scrollViewDidEndScroll:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        BOOL dragToDragStop = scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
        if (dragToDragStop) {
            [self scrollViewDidEndScroll:scrollView];
        }
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    BOOL scrollToScrollStop = !scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
    if (scrollToScrollStop) {
        [self scrollViewDidEndScroll:scrollView];
    }
}

- (void)scrollViewDidEndScroll:(UIScrollView *)scrollView{
    NSInteger index = ceil(scrollView.contentOffset.x/kCommonScreenWidth);
    if (_lastIndex == index) return;
    [self didSelectSubViewIndex:index];
    if ([self.delegate respondsToSelector:@selector(by_segmentViewDidScrollToIndex:)]) {
        [self.delegate by_segmentViewDidScrollToIndex:index];
    }
}

#pragma mark - configSubView Method

- (void)setContentView{
    CGFloat headerHeight = 64;
    UIView *headerView = [self getHeaderView];
    [self addSubview:headerView];
    _headerView = headerView;
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.height.mas_equalTo(headerHeight);
    }];
    
    
    UIScrollView *scrollView = [self getScrollView];
    scrollView.delegate = self;
    [self addSubview:scrollView];
    _scrollView = scrollView;
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(headerHeight, 0, 0, 0));
    }];
    
}

// headerView
- (UIView *)getHeaderView{
    UIView *headerView = [UIView by_init];
    NSMutableArray *arrays = [NSMutableArray array];
    _sub_width = 93;
    for (int i = 0; i < _titles.count; i ++) {
        NSString *title = _titles[i];
        UIButton *button = [UIButton by_buttonWithCustomType];
        NSDictionary *normalDic = @{@"title":title,NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kTextColor_86};
        NSDictionary *highlight = @{@"title":title,NSFontAttributeName:[UIFont systemFontOfSize:17],NSForegroundColorAttributeName:kTextColor_48};
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 2.5, -15 , 0);
        [button setBy_attributedTitle:normalDic forState:UIControlStateNormal];
        [button setBy_attributedTitle:highlight forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:button];
        [arrays addObject:button];
        button.tag = baseTag + i;
        @weakify(self);
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.width.mas_equalTo(self ->_sub_width);
            make.top.bottom.mas_equalTo(0);
            make.left.mas_equalTo((self ->_sub_width)*i);
        }];
    }
    self.buttons = arrays;
    return headerView;
}

// contentView
- (UIScrollView *)getScrollView{
    UIScrollView *scrollView = [UIScrollView by_init];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    for (int i = 0; i < _titles.count; i ++) {
        UIView *view = [UIView by_init];
        [scrollView addSubview:view];
//        view.backgroundColor = kColorRGB(20*i, 23, 20*i, 1);
        view.tag = 2*baseTag + i;
        @weakify(self);
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.left.mas_equalTo(kCommonScreenWidth*i);
            make.top.mas_equalTo(0);
            make.height.equalTo(scrollView.mas_height).with.offset(0);
            make.width.equalTo(scrollView.mas_width).with.offset(0);
            if (i == self.titles.count - 1) {
                make.right.mas_equalTo(0);
            }
        }];
    }
    return scrollView;
}

@end
