//
//  BYLiveHomeController.h
//  BY
//
//  Created by 黄亮 on 2018/7/25.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYCommonViewController.h"

@interface BYLiveHomeController : BYCommonViewController

+(instancetype)sharedController;

-(void)reloadViewInterfaceManager;

@end
