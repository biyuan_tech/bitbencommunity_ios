//
//  BYPersonHomeViewModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/12/3.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPersonHomeViewModel.h"
#import "BYPersonHomeController.h"
#import "BYPersonHomeSegmentView.h"
#import "BYPersonHomeHeaderView.h"

// subController
#import "BYPersonArticleSubController.h"
#import "BYPersonLiveSubController.h"
#import "BYPersonAppraiseSubController.h"

#import "GWAssetsImgSelectedViewController.h"

#define K_VC ((BYPersonHomeController *)S_VC)
@interface BYPersonHomeViewModel ()<BYPersonHomeSegmentViewDelegate>

/** navigationBar */
@property (nonatomic ,strong) UIView *navigatoinBarView;

@property (nonatomic ,strong) BYPersonHomeSegmentView *segmentView;
/** headerView */
@property (nonatomic ,strong) BYPersonHomeHeaderView *headerView;
/** 文章 */
@property (nonatomic ,strong) BYPersonArticleSubController *articleController;
/** 直播 */
@property (nonatomic ,strong) BYPersonLiveSubController *liveController;
/** 评价 */
@property (nonatomic ,strong) BYPersonAppraiseSubController *appraiseController;

@end

@implementation BYPersonHomeViewModel

- (void)setContentView{
    [self configUI];
    [self addNavigatoinBarView];
    [self loadRequestHeader];
}

- (void)backBtnAction{
    [S_V_NC popViewControllerAnimated:YES];
}

#pragma mark - BYLiveRoomSegmentViewDelegate
- (void)by_segmentViewWillScrollToIndex:(NSInteger)index{
   
}

- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    if (index == 0) {
        return self.articleController.view;
    }else if (index == 1){
        return self.liveController.view;
    }else{
        return self.appraiseController.view;
    }
}

#pragma mark - request
- (void)loadRequestHeader{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetPersonData:K_VC.user_id successBlock:^(id object) {
        @strongify(self);
        [self.headerView reloadData:object];
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - configUI
- (void)addNavigatoinBarView{
    self.navigatoinBarView = [UIView by_init];
    self.navigatoinBarView.backgroundColor = kColorRGB(220, 87, 77, 0.0f);
    [S_V_VIEW addSubview:self.navigatoinBarView];
    [self.navigatoinBarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(kNavigationHeight);
    }];
    
    // 返回按钮
    UIButton *backBtn = [UIButton by_buttonWithCustomType];
    [backBtn setBy_imageName:@"icon_center_bbt_back" forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigatoinBarView addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.mas_equalTo(0);
        make.width.height.mas_equalTo(44);
    }];
}
- (void)configUI{
    self.headerView = [[BYPersonHomeHeaderView alloc] init];
    self.headerView.user_id = K_VC.user_id;
    
#pragma mark from Ares
    __weak typeof(self)weakSelf = self;
    [self.headerView actionClickAvatarManagerWithBlock:^(NSString * _Nonnull imgUrl) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        GWAssetsImgSelectedViewController *imgSelected = [[GWAssetsImgSelectedViewController alloc]init];
        [imgSelected showInView:K_VC.parentViewController imgArr:@[imgUrl] currentIndex:0 cell:strongSelf.headerView];
    }];
    
    
    [S_V_VIEW addSubview:self.headerView];
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Top(178));
    }];
    
    self.segmentView = [[BYPersonHomeSegmentView alloc] initWithTitles:@"文章",@"直播",@"评价", nil];
    _segmentView.delegate = self;
    _segmentView.defultSelIndex = 0;
    [S_V_VIEW addSubview:self.segmentView];
    [_segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headerView.mas_bottom).mas_offset(0);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
}

- (BYPersonArticleSubController *)articleController{
    if (!_articleController) {
        _articleController = [[BYPersonArticleSubController  alloc] init];
        _articleController.user_id = K_VC.user_id;
        _articleController.superController = S_VC;
        [K_VC addChildViewController:_articleController];
    }
    return _articleController;
}

- (BYPersonLiveSubController *)liveController{
    if (!_liveController) {
        _liveController = [[BYPersonLiveSubController  alloc] init];
        _liveController.user_id = K_VC.user_id;
        _liveController.superController = S_VC;
        [K_VC addChildViewController:_liveController];
    }
    return _liveController;
}

- (BYPersonAppraiseSubController *)appraiseController{
    if (!_appraiseController) {
        _appraiseController = [[BYPersonAppraiseSubController  alloc] init];
        _appraiseController.user_id = K_VC.user_id;
        _appraiseController.superController = S_VC;
        [K_VC addChildViewController:_appraiseController];
    }
    return _appraiseController;
}
@end
