//
//  BYPersonHomeSegmentView.h
//  BibenCommunity
//
//  Created by 随风 on 2018/12/4.
//  Copyright © 2018 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol BYPersonHomeSegmentViewDelegate <NSObject>


/**
 根据index自定义添加View
 
 @param index index
 @return return value description
 */
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index;

@optional
/**
 当前滚动到的Index
 
 @param index index
 */
- (void)by_segmentViewDidScrollToIndex:(NSInteger)index;
- (void)by_segmentViewWillScrollToIndex:(NSInteger)index;

@end

@interface BYPersonHomeSegmentView : UIView

/** 初始化选择的index (默认1) */
@property (nonatomic ,assign) NSInteger defultSelIndex;
@property (nonatomic ,weak) id<BYPersonHomeSegmentViewDelegate>delegate;

/**
 
 */
@property (nonatomic ,copy) void (^didLoadSubView)(NSInteger index,UIView *subView);

- (instancetype)initWithTitles:(NSString *)titles, ... NS_REQUIRES_NIL_TERMINATION;

@end

NS_ASSUME_NONNULL_END
