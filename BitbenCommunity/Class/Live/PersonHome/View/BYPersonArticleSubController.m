//
//  BYPersonArticleSubController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/12/4.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPersonArticleSubController.h"
#import "ShareRootViewController.h"
#import "GWAssetsImgSelectedViewController.h"

#import "BYHomeArticleTagCell.h"
#import "BYReportInCellView.h"

#import "BYHomeArticleModel.h"

@interface BYPersonArticleSubController ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;
/** 举报 */
@property (nonatomic ,strong) BYReportInCellView *reportView;
/** tableFooterView */
@property (nonatomic ,strong) UIView *tableFooterView;


@end

@implementation BYPersonArticleSubController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageNum = 0;
    [self setContentView];
    [self loadRequestArticle];
    // Do any additional setup after loading the view.
}

// 分享
- (void)shareAction:(id)model{
    NSString *shareTitle;
    NSString *shareContent;
    NSString *shareImgUrl;
    NSString *shareUrl;
    shareType sharetype = shareTypeLive;

    BYHomeArticleModel *data = (BYHomeArticleModel *)model;
    NSDictionary *params = @{@"个人主页文章分享":(data.article_id.length?data.article_id:@"找不到文章")};
    [MTAManager event:MTATypeShortShare params:params];
    id smartImgUrl;
    if (data.picture.count){
        smartImgUrl = [data.picture firstObject];
    } else {
        smartImgUrl = data.head_img;
    }
    
    id mainImgUrl;
    if ([smartImgUrl isKindOfClass:[NSString class]]){
        NSString *urlStr = (NSString *)smartImgUrl;
        NSString *baseURL = [PDImageView getUploadBucket:urlStr];
        NSString *nUrl = [[NSString stringWithFormat:@"%@%@",baseURL,urlStr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        mainImgUrl = nUrl;
    } else if ([smartImgUrl isKindOfClass:[UIImage class]]){
        mainImgUrl = smartImgUrl;
    }
    if (data.article_type == article_typeWeb){
        shareTitle = data.title;
        if (data.subtitle.length && ![data.subtitle isEqualToString:@"null"]){
            shareContent = data.subtitle;
        } else {
            shareContent = @"最新最热区块链内容，尽在币本社区";
        }
    } else if (data.article_type == article_typeNormal){
        shareTitle = data.title.length?data.title:data.content;
        shareContent = @"最新最热区块链内容，尽在币本社区";
    }
    shareUrl = data.article_url;
    shareImgUrl = mainImgUrl;
    sharetype = shareTypeArticle;
    
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.collection_status = data.collection_status;
    shareViewController.transferCopyUrl = shareUrl;
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        [ShareSDKManager shareManagerWithType:shareType title:shareTitle desc:shareContent img:shareImgUrl url:shareUrl callBack:NULL];
        [ShareSDKManager shareSuccessBack:sharetype block:NULL];
    }];
    
    [shareViewController actionClickWithCollectionBlock:^{
        data.collection_status = !data.collection_status;
    }];
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    [shareViewController showInView:currentController];
}

// 顶的操作
- (void)supportAction:(id)model indexPath:(NSIndexPath *)indexPath suc:(void(^)(void))suc{
    BYHomeArticleModel *data = (BYHomeArticleModel *)model;
    if (data.isSupport) {
        showToastView(@"你已经顶过该条内容", self.view);
        return;
    }
    [self loadRequestSupport:data.article_id user_id:data.author_id suc:^{
        data.isSupport = YES;
        data.count_support ++;
        if (suc) suc();
    }];
}

// 举报
- (void)showReportView:(CGPoint)point model:(id)model{
    NSString *theme_id;
    BY_THEME_TYPE type = BY_THEME_TYPE_LIVE;
  
    BYHomeArticleModel *data = (BYHomeArticleModel *)model;
    theme_id = data.article_id;
    type = BY_THEME_TYPE_ARTICLE;
    
    self.reportView.theme_id = theme_id;
    self.reportView.theme_type = type;
    [self.reportView showAnimationWithPoint:point];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = tableView.tableData[indexPath.section];
    id model = dic.allValues[0][indexPath.row];
    BYHomeArticleModel *data = (BYHomeArticleModel *)model;
    [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_ARTICLE
                                                    liveType:0
                                                     themeId:data.article_id];
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"shareAction"]) { // 分享
        NSDictionary *dic = tableView.tableData[indexPath.section];
        id model = dic.allValues[0][indexPath.row];
        [self shareAction:model];
    }else if ([actionName isEqualToString:@"likeAction"]) { // 顶
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYHomeArticleTagCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        id model = dic.allValues[0][indexPath.row];
        @weakify(self);
        [self authorizeWithCompletionHandler:^(BOOL successed) {
            @strongify(self);
            if (!successed) return ;
            [self supportAction:model indexPath:indexPath suc:^{
                [cell reloadAttentionAnimation];
            }];
        }];
    }else if ([actionName isEqualToString:@"reportAction"]) { // 举报
        NSDictionary *dic = tableView.tableData[indexPath.section];
        id model = dic.allValues[0][indexPath.row];
        NSValue *value = param[@"point"];
        CGPoint point = [value CGPointValue];
        [self showReportView:point model:model];
    }else if ([actionName isEqualToString:@"imgAction"]) { // 图片点击放大
        NSInteger index = [param[@"index"] integerValue];
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYHomeArticleModel *model = dic.allValues[0][indexPath.row];
        UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
        GWAssetsImgSelectedViewController *imgSelected = [[GWAssetsImgSelectedViewController alloc]init];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [imgSelected showInView:currentController imgArr:model.picture currentIndex:index cell:cell];
    }
}

- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [UIView by_init];
    [sectionView setBackgroundColor:[UIColor clearColor]];
    return sectionView;
}

- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForHeaderInSection:(NSInteger)section{
    return 8;
}


#pragma mark - request
- (void)loadRequestArticle{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetPersonArticle:self.user_id pageNum:self.pageNum successBlock:^(id object) {
        @strongify(self);
        [self.tableView endRefreshing];
        [self.tableView removeEmptyView];
        if (self.pageNum == 0) {
            self.pageNum = 0;
            self.tableView.tableData = object;
            if (![object count]) {
                self.tableView.tableFooterView = nil;
                [self.tableView addEmptyView:@"暂无内容"];
                return;
            }
            self.tableView.tableFooterView = self.tableFooterView;
            self.pageNum++;
        }
        else{
            if ([object count]) {
                NSMutableArray *tmpData = [NSMutableArray arrayWithArray:self.tableView.tableData];
                [tmpData addObjectsFromArray:object];
                self.tableView.tableData = tmpData;
                self.pageNum++;
            }
        }
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

// 顶
- (void)loadRequestSupport:(NSString *)theme_id user_id:(NSString *)user_id suc:(void(^)(void))suc{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestLiveGuestOpearType:BY_GUEST_OPERA_TYPE_UP theme_id:theme_id theme_type:BY_THEME_TYPE_ARTICLE receiver_user_id:user_id successBlock:^(id object) {
        if (suc) suc();
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"顶操作失败", self.view);
    }];
}

#pragma mark - configUI

- (void)setContentView{
    self.tableView = [BYCommonTableView by_init];
    self.tableView.group_delegate = self;
    @weakify(self);
    [self.tableView addHeaderRefreshHandle:^{
        @strongify(self);
        self.pageNum = 0;
        [self loadRequestArticle];
    }];
    [self.tableView addFooterRefreshHandle:^{
        @strongify(self);
        [self loadRequestArticle];
    }];
    [self.tableView setBackgroundColor:kColorRGBValue(0xf5f5f5)];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (BYReportInCellView *)reportView{
    if (!_reportView) {
        _reportView = [BYReportInCellView initReportViewShowInView:kCommonWindow];
    }
    return _reportView;
}

- (UIView *)tableFooterView{
    if (!_tableFooterView) {
        _tableFooterView = [UIView by_init];
        _tableFooterView.frame = CGRectMake(0, 0, kCommonScreenWidth, 40);
        _tableFooterView.backgroundColor = [UIColor whiteColor];
    }
    return _tableFooterView;
}

@end
