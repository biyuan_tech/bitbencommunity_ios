//
//  BYPersonAppraiseSubController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/12/4.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPersonAppraiseSubController.h"
#import "ArticleDetailRootViewController.h"
#import "BYILiveRoomController.h"
#import "BYVideoRoomController.h"
#import "BYPPTRoomController.h"
#import "BYVODPlayController.h"
#import "BYLiveDetailController.h"

#import "BYAppraiseModel.h"

@interface BYPersonAppraiseSubController ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;

@end

@implementation BYPersonAppraiseSubController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setContentView];
    [self loadRequestAppraise];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    BYAppraiseModel *model = tableView.tableData[indexPath.row];
    if ([actionName isEqualToString:@"themeAction"]) {
        NSString *theme_id = model.theme_type == BY_THEME_TYPE_ARTICLE ? model.articleModel._id : model.liveModel.live_record_id;
        [DirectManager commonPushControllerWithThemeType:model.theme_type liveType:model.liveModel.live_type themeId:theme_id];
    }
}

#pragma mark - request
- (void)loadRequestAppraise{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetMyComment:self.user_id pageNum:self.pageNum successBlock:^(id object) {
        @strongify(self);
        if (self.pageNum == 0) {
            self.tableView.tableData = object;
            if (![object count]) {
                [self.tableView addEmptyView:@"暂无评论"];
            }else{
                [self.tableView removeEmptyView];
            }
        }else{
            NSMutableArray *tmpArr = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [tmpArr addObjectsFromArray:object];
            self.tableView.tableData = [tmpArr copy];
        }
        self.pageNum ++;
        [self.tableView endRefreshing];
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

#pragma mark - configUI

- (void)setContentView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    @weakify(self);
    [self.tableView addHeaderRefreshHandle:^{
        @strongify(self);
        self.pageNum = 0;
        [self loadRequestAppraise];
    }];
    [self.tableView addFooterRefreshHandle:^{
        @strongify(self);
        [self loadRequestAppraise];
    }];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}


@end
