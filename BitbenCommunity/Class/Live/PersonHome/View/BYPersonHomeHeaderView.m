//
//  BYPersonHomeHeaderView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/12/3.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPersonHomeHeaderView.h"

static char actionClickAvatarManagerWithBlockKey;
@interface BYPersonHomeHeaderView ()


/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 签名 */
@property (nonatomic ,strong) UILabel *signLab;
/** 关注数 */
@property (nonatomic ,strong) UILabel *attentionNumLab;
/** 粉丝数 */
@property (nonatomic ,strong) UILabel *fansNumLab;
/** 关注状态 */
@property (nonatomic ,strong) UIButton *attentionStatus;
/** 头像按钮*/
@property (nonatomic ,strong) UIButton *actionAvatarBtn;
/** vip认证 */
@property (nonatomic ,strong) PDImageView *vipImgView;
/** vip认证名 */
@property (nonatomic ,strong) UILabel *vipNameLab;
/** 承载成就视图 */
@property (nonatomic ,strong) UIView *achievementView;
/** model */
@property (nonatomic ,strong) BYPersonHomeHeaderModel *model;


@end

@implementation BYPersonHomeHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)reloadData:(BYPersonHomeHeaderModel *)model{
    self.model = model;
    self.headerImgView.vipImg_WH = 18;
//    self.headerImgView.style = model.cert_badge;
    [self.headerImgView uploadHDImageWithURL:model.head_img callback:nil];
    self.userNameLab.text = nullToEmpty(model.nickname);
    self.signLab.text = nullToEmpty(model.self_introduction);
    self.attentionNumLab.text = nullToEmpty(model.attention);
    self.fansNumLab.text = nullToEmpty(model.fans);
    self.attentionStatus.selected = model.isAttention;
    [self reloadAttentionStatus:model.isAttention];
    
    if ([model.user_id isEqualToString:[AccountModel sharedAccountModel].account_id]) {
        self.attentionStatus.hidden = YES;
    }
    
    if (model.cert_badge != PDImgStyleNormal) {
        NSString *bedg_id = stringFormatInteger(model.cert_badge);
        ServerBadgeModel *badgeModel = [BYCommonTool getVipImageName:bedg_id];
        self.vipImgView.hidden = NO;
        self.vipNameLab.hidden = NO;
        [self.vipImgView uploadHDImageWithURL:badgeModel.picture callback:nil];
        self.vipNameLab.text = badgeModel.name;
        if (model.cert_badge != PDImgStyleAvatar_Vip_Person) {
            [self.vipImgView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.height.mas_equalTo(16);
            }];
        }
        @weakify(self);
        [self.vipNameLab mas_updateConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.width.mas_equalTo(stringGetWidth(badgeModel.name, self.vipNameLab.font.pointSize));
        }];
    }
    
    [self addAchievementSubView];
    
    [self layoutIfNeeded];
}

- (void)reloadAttentionStatus:(BOOL)isAttention{
    self.attentionStatus.selected = isAttention;
    self.attentionStatus.backgroundColor = isAttention ? kColorRGBValue(0xDD694F) : [UIColor whiteColor];
}

#pragma mark - buttonAction
- (void)attentionStatusAction:(UIButton *)sender{
    [self loadRequestAttention:!sender.selected];
}

#pragma makr - request
- (void)loadRequestAttention:(BOOL)isAttention{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:self.user_id isAttention:isAttention successBlock:^(id object) {
        @strongify(self);
        [self reloadAttentionStatus:isAttention];
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - configUI

- (void)addAchievementSubView{
    for (UIView *subView in self.achievementView.subviews) {
        [subView removeFromSuperview];
    }
    
    double maxRow = iPhone5 ? 2.0 : 3.0;
    NSInteger maxRowInt = maxRow;
    CGFloat width = 92;
    CGFloat height = 16;
    NSInteger sectionNum = ceil(self.model.achievement_badge_list.count/maxRow);
    CGFloat achievementH = sectionNum > 0 ? height*sectionNum + 8*(sectionNum - 1) + 25 : 0;
    
    CGFloat headerH = kSafe_Mas_Top(178) + achievementH;
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(headerH);
    }];
    
//    if (achievementH > 0) {
        [self reloadGradient:headerH];
//    }
    
    [self.achievementView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(achievementH);
    }];
    
    for (int i = 0; i < self.model.achievement_badge_list.count; i ++) {
        NSInteger rowInt = i%maxRowInt;
        NSInteger sectionInt = i/maxRowInt;
        
        NSString *bagde_id = self.model.achievement_badge_list[i];
        ServerBadgeModel *bagdeModel = [BYCommonTool getAchievementData:bagde_id];
        UIView *subView = [self getSingleAchievementSubView:bagdeModel];
        [self.achievementView addSubview:subView];
        [subView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(width*rowInt);
            make.top.mas_equalTo((height + 8)*sectionInt);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
        }];
    }
}

- (UIView *)getSingleAchievementSubView:(ServerBadgeModel *)model{
    UIView *view = [UIView by_init];
    PDImageView *imageView = [[PDImageView alloc] init];
    [imageView uploadHDImageWithURL:model.picture callback:nil];
    [view addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.mas_equalTo(0);
        make.width.height.mas_equalTo(16);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:10];
    titleLab.text = nullToEmpty(model.name);
    titleLab.textColor = [UIColor whiteColor];
    [view addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(0);
        make.centerY.equalTo(imageView);
        make.height.mas_equalTo(16);
    }];
    
    return view;
}

- (void)reloadGradient:(CGFloat)height{
    NSMutableArray *layers = [NSMutableArray arrayWithArray:self.layer.sublayers];
    for (CALayer *layer in layers) {
        if ([layer isKindOfClass:[CAGradientLayer class]]) {
            [layer removeFromSuperlayer];
        }
    }
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, kCommonScreenWidth, height);
    gradient.colors = @[(id)kColorRGBValue(0xed6145).CGColor,(id)kColorRGBValue(0xed4a45).CGColor];
    gradient.startPoint = CGPointMake(0.5, 0);
    gradient.endPoint = CGPointMake(0.5, 1);
    [self.layer insertSublayer:gradient atIndex:0];
}

- (void)setContentView{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, kCommonScreenWidth, 114 + kNavigationHeight);
    gradient.colors = @[(id)kColorRGBValue(0xed6145).CGColor,(id)kColorRGBValue(0xed4a45).CGColor];
    gradient.startPoint = CGPointMake(0.5, 0);
    gradient.endPoint = CGPointMake(0.5, 1);
    [self.layer addSublayer:gradient];
    
    // 关注按钮
    self.attentionStatus = [UIButton by_buttonWithCustomType];
    [self.attentionStatus setBy_attributedTitle:@{@"title":@"+ 关注",
                                                  NSForegroundColorAttributeName:kColorRGBValue(0xee4944),
                                                  NSFontAttributeName:[UIFont systemFontOfSize:12],
                                                  } forState:UIControlStateNormal];
    [self.attentionStatus setBy_attributedTitle:@{@"title":@"已关注",
                                                  NSForegroundColorAttributeName:kColorRGBValue(0xffffff),
                                                  NSFontAttributeName:[UIFont systemFontOfSize:12],
                                                  } forState:UIControlStateSelected];
    [self.attentionStatus addTarget:self
                             action:@selector(attentionStatusAction:)
                   forControlEvents:UIControlEventTouchUpInside];
    self.attentionStatus.layer.cornerRadius = 5.0f;
    self.attentionStatus.layer.borderColor = [UIColor whiteColor].CGColor;
    self.attentionStatus.layer.borderWidth = 1.0f;
    [self addSubview:self.attentionStatus];
    [self.attentionStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(54);
        make.height.mas_equalTo(26);
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(kSafe_Mas_Top(84));
    }];
    [self reloadAttentionStatus:NO];
    
    UIView *headerImgBgView = [UIView by_init];
    [headerImgBgView setBackgroundColor:[UIColor whiteColor]];
    headerImgBgView.layer.cornerRadius = 33.0f;
    [self addSubview:headerImgBgView];
    [headerImgBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(66.0f);
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(kSafe_Mas_Top(77));
    }];
    
    // 头像
    self.headerImgView = [[PDImageView alloc] init];
    self.headerImgView.contentMode = UIViewContentModeScaleAspectFill;
    self.headerImgView.backgroundColor = [UIColor clearColor];
    self.headerImgView.layer.cornerRadius = 29;
    self.headerImgView.clipsToBounds = YES;
    [self addSubview:self.headerImgView];
    [self.headerImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(19);
        make.width.height.mas_equalTo(58);
        make.top.mas_equalTo(kSafe_Mas_Top(81));
    }];
    
    self.actionAvatarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionAvatarBtn.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.actionAvatarBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (!strongSelf.headerImgView.imgUrl.length) return;
            
    
        void(^block)(NSString *imgUrl) = objc_getAssociatedObject(strongSelf, &actionClickAvatarManagerWithBlockKey);
        if (block){
            block(strongSelf.headerImgView.imgUrl);
        }
    }];
    [self addSubview:self.actionAvatarBtn];
    [self.actionAvatarBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(19);
        make.width.height.mas_equalTo(58);
        make.top.mas_equalTo(kSafe_Mas_Top(81));
    }];
    
    
    // 昵称
    self.userNameLab = [UILabel by_init];
    self.userNameLab.font = [UIFont systemFontOfSize:15];
    self.userNameLab.textColor = [UIColor whiteColor];
    [self addSubview:self.userNameLab];
    @weakify(self);
    [self.userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.headerImgView.mas_right).mas_offset(24);
//        make.right.mas_equalTo(self.attentionStatus.mas_left).mas_offset(-35);
        make.height.mas_equalTo(15);
        make.width.mas_lessThanOrEqualTo(kCommonScreenWidth - 260);
        make.top.mas_equalTo(self.headerImgView.mas_top).mas_offset(-5);
    }];
    
    // vip图标
    self.vipImgView = [[PDImageView alloc] init];
    self.vipImgView.hidden = YES;
    [self addSubview:self.vipImgView];
    [self.vipImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.userNameLab.mas_right).mas_offset(5);
        make.centerY.equalTo(self.userNameLab);
        make.width.mas_equalTo(24);
        make.height.mas_equalTo(17);
    }];
    
    // vip名
    self.vipNameLab = [[UILabel alloc] init];
    self.vipNameLab.hidden = YES;
    [self.vipNameLab setBy_font:10];
    self.vipNameLab.textColor = [UIColor whiteColor];
    [self addSubview:self.vipNameLab];
    [self.vipNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.vipImgView.mas_right).mas_offset(5);
        make.centerY.equalTo(self.userNameLab);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(self.vipNameLab.font.pointSize);
    }];

    
    // 签名
    self.signLab = [UILabel by_init];
    self.signLab.textColor = [UIColor whiteColor];
    self.signLab.numberOfLines = 2;
    [self.signLab setBy_font:12];
    [self addSubview:self.signLab];
    [self.signLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.headerImgView.mas_right).mas_offset(24);
        make.top.mas_equalTo(self.userNameLab.mas_bottom).mas_offset(5);
        make.right.mas_equalTo(self.attentionStatus.mas_left).mas_offset(-25);
        make.height.mas_greaterThanOrEqualTo(12);
    }];
    
    UILabel *attentionLab = [UILabel by_init];
    attentionLab.text = @"关注";
    attentionLab.textColor = [UIColor whiteColor];
    [attentionLab setBy_font:12];
    [self addSubview:attentionLab];
    [attentionLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.headerImgView.mas_right).mas_offset(24);
        make.top.mas_equalTo(self.signLab.mas_bottom).mas_offset(15);
        make.height.mas_equalTo(stringGetHeight(attentionLab.text, 12));
        make.width.mas_equalTo(stringGetWidth(attentionLab.text, 12));
    }];
    
    // 关注数
    self.attentionNumLab = [UILabel by_init];
    self.attentionNumLab.textColor = [UIColor whiteColor];
    [self.attentionNumLab setBy_font:13];
    [self addSubview:self.attentionNumLab];
    [self.attentionNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(attentionLab.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(attentionLab.mas_centerY).mas_offset(0);
        make.height.mas_equalTo(attentionLab.mas_height).mas_offset(0);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    UIView *lineView = [UIView by_init];
    lineView.backgroundColor = [UIColor whiteColor];
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(10);
        make.centerY.mas_equalTo(attentionLab.mas_centerY).mas_offset(0);
        make.left.mas_equalTo(self.attentionNumLab.mas_right).mas_offset(15);
    }];
    
    UILabel *fansLab = [UILabel by_init];
    fansLab.text = @"粉丝";
    fansLab.textColor = [UIColor whiteColor];
    [fansLab setBy_font:12];
    [self addSubview:fansLab];
    [fansLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(fansLab.text, 12));
        make.height.mas_equalTo(stringGetHeight(fansLab.text, 12));
        make.centerY.mas_equalTo(attentionLab.mas_centerY).mas_offset(0);
        make.left.mas_equalTo(lineView.mas_right).mas_offset(15);
    }];
    
    // 粉丝数
    self.fansNumLab = [UILabel by_init];
    self.fansNumLab.textColor = [UIColor whiteColor];
    [self.fansNumLab setBy_font:13];
    [self addSubview:self.fansNumLab];
    [self.fansNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(fansLab.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(attentionLab.mas_centerY).mas_offset(0);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(fansLab.mas_height).mas_offset(0);
    }];
    
    // 承载成就
    self.achievementView = [UIView by_init];
    [self addSubview:self.achievementView];
    [self.achievementView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.userNameLab);
        make.top.equalTo(fansLab.mas_bottom).mas_equalTo(15);
        make.right.mas_equalTo(0);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
}


#pragma mark - from Ares 头像点击
-(void)actionClickAvatarManagerWithBlock:(void(^)(NSString *imgUrl))block{
    objc_setAssociatedObject(self, &actionClickAvatarManagerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
