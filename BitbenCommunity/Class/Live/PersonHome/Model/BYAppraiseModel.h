//
//  BYAppraiseModel.h
//  BibenCommunity
//
//  Created by 随风 on 2018/12/5.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYCommonModel.h"
#import "ArticleRootSingleModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface BYAppraiseModel : BYCommonModel

/** 奖赏bbt量 */
@property (nonatomic ,copy) NSString *reward;
/** 评论对象 */
@property (nonatomic ,copy) NSString *comment_name;
/** 评论时间戳 */
@property (nonatomic ,assign) NSTimeInterval create_time;
/** 评论时间展示 */
@property (nonatomic ,copy) NSString *create_time_show;

/** 回复id */
@property (nonatomic ,copy) NSString *reply_user_id;
/** 评论id */
@property (nonatomic ,copy) NSString *comment_id;
/** 评论者头像 */
@property (nonatomic ,copy) NSString *comment_pic;
/** 评论内容 */
@property (nonatomic ,copy) NSString *content;
/** 回复评论的id */
@property (nonatomic ,copy) NSString *reply_comment_id;
/** 回复者的头像 */
@property (nonatomic ,copy) NSString *reply_comment_pic;
/** 评论者id */
@property (nonatomic ,copy) NSString *user_id;
/** 回复者昵称 */
@property (nonatomic ,copy) NSString *reply_comment_name;
/** 类型 */
@property (nonatomic ,assign) BY_THEME_TYPE theme_type;
/** 评论内容AttributedString */
@property (nonatomic ,strong) NSAttributedString *commentAttributedString;
/** 评论内容size */
@property (nonatomic ,assign) CGSize commentSize;
/** 主题内容AttributedString */
@property (nonatomic ,strong) NSAttributedString *themeAttributedString;
/** 主题内容size */
@property (nonatomic ,assign) CGSize themeSize;
/** 文章内容 */
@property (nonatomic ,strong) ArticleRootSingleModel *articleModel;
/** 直播内容 */
@property (nonatomic ,strong) BYCommonLiveModel *liveModel;
/** vip认证 */
@property (nonatomic ,assign) NSInteger cert_badge;
/** 成就组 */
@property (nonatomic ,strong) NSArray <NSString *>*achievement_badge_list;

+ (NSArray *)getTableData:(NSDictionary *)respondDic;
@end

NS_ASSUME_NONNULL_END
