//
//  BYPersonHomeHeaderModel.h
//  BibenCommunity
//
//  Created by 随风 on 2018/12/4.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYPersonHomeHeaderModel : BYCommonModel

/** 头像 */
@property (nonatomic ,copy) NSString *head_img;
/** 用户id */
@property (nonatomic ,copy) NSString *user_id;
@property (nonatomic ,copy) NSString *account_id;
/** 昵称 */
@property (nonatomic ,copy) NSString *nickname;
/** 会员 */
@property (nonatomic ,assign) BOOL vip;
/** 简介 */
@property (nonatomic ,copy) NSString *self_introduction;
/** 会员级别 */
@property (nonatomic ,copy) NSString *level;
/** 关注数 */
@property (nonatomic ,copy) NSString *attention;
/** 粉丝数 */
@property (nonatomic ,copy) NSString *fans;
/** 关注状态 */
@property (nonatomic ,assign) BOOL isAttention;
/** NSAttributedString */
@property (nonatomic ,strong) NSAttributedString *attributedString;
/** 是否显示简介 */
@property (nonatomic ,assign) BOOL showIntro;
/** vip认证 */
@property (nonatomic ,assign) NSInteger cert_badge;
/** 成就组 */
@property (nonatomic ,strong) NSArray <NSString *>*achievement_badge_list;

- (BYPersonHomeHeaderModel *)getHeaderData:(NSDictionary *)respondDic;

@end

NS_ASSUME_NONNULL_END
