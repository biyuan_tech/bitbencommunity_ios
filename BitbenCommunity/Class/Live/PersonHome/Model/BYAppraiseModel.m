//
//  BYAppraiseModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/12/5.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYAppraiseModel.h"

@implementation BYAppraiseModel

+ (NSArray *)getTableData:(NSDictionary *)respondDic{
    if ([respondDic.allKeys indexOfObject:@"content"] == NSNotFound) {
        return nil;
    }
    NSArray *content = respondDic[@"content"];
    if (!content.count) {
        return nil;
    }
    NSMutableArray *tableData = [NSMutableArray array];
    for (NSDictionary *dic in content) {
        BYAppraiseModel *model = [BYAppraiseModel mj_objectWithKeyValues:dic];
        if (model.theme_type == BY_THEME_TYPE_LIVE) {
            model.liveModel = [BYCommonLiveModel mj_objectWithKeyValues:dic[@"live_record"]];
        }
        else if (model.theme_type == BY_THEME_TYPE_ARTICLE){
            model.articleModel = [ArticleRootSingleModel mj_objectWithKeyValues:dic[@"article"]];
        }
        
        if ([model.comment_id isEqualToString:@"0"]) { // 回复他人状态
            model.content = [NSString stringWithFormat:@"回复%@：%@",model.reply_comment_name,model.content];
        }
        
        model.create_time_show = [NSDate timeIntervalFromLastTime:model.create_time/1000.0];
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 6;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:model.content];
        [attributedString addAttribute:NSParagraphStyleAttributeName  value:paragraphStyle range:NSMakeRange(0, model.content.length)];
        [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, model.content.length)];
        if ([model.comment_id isEqualToString:@"0"]) { // 回复他人状态
            NSRange range = [model.content rangeOfString:model.reply_comment_name options:NSCaseInsensitiveSearch];
            [attributedString addAttribute:NSForegroundColorAttributeName value:kColorRGBValue(0xee4944) range:range];
        }
        model.commentAttributedString = attributedString;

        CGSize size = [model.content getStringSizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],
                                                                   NSParagraphStyleAttributeName:paragraphStyle
                                                                   } maxSize:CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT)];
        model.commentSize = size;
        NSString *themeString;
        if (model.theme_type == BY_THEME_TYPE_LIVE) {
            themeString = model.liveModel.live_title;
        }else{
            themeString = model.articleModel.title;
        }
        CGSize themeSize = [themeString getStringSizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                                                   NSParagraphStyleAttributeName:paragraphStyle
                                                                   } maxSize:CGSizeMake(kCommonScreenWidth - 105, 44)];
        model.themeSize = themeSize;
        NSMutableAttributedString *themeAttributedString = [[NSMutableAttributedString alloc] initWithString:themeString];
        [themeAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, themeString.length)];
        model.themeAttributedString = themeAttributedString;

        model.cellString = @"BYAppraiseCell";
        model.cellHeight = 200 + size.height;
        [tableData addObject:model];
    }
    return tableData;
}

@end
