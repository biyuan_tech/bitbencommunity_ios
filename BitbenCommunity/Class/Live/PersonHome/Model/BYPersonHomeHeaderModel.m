//
//  BYPersonHomeHeaderModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/12/4.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPersonHomeHeaderModel.h"

@implementation BYPersonHomeHeaderModel
- (BYPersonHomeHeaderModel *)getHeaderData:(NSDictionary *)respondDic{
    if (![respondDic isKindOfClass:[NSDictionary class]]) return self;
    self.isAttention = [respondDic[@"isAttention"] boolValue];
    if (respondDic[@"countMap"]) {
        self.attention = stringFormatInteger([respondDic[@"countMap"][@"attention"] integerValue]);
        self.fans = stringFormatInteger([respondDic[@"countMap"][@"fans"] integerValue]);
    }
    if (respondDic[@"otherMap"]) {
        NSString *introduction = nullToEmpty(respondDic[@"otherMap"][@"self_introduction"]);
        self.self_introduction = introduction.length ? introduction : @"这个人很懒，什么都没留下";
        self.nickname = nullToEmpty(respondDic[@"otherMap"][@"nickname"]);
        self.user_id = nullToEmpty(respondDic[@"otherMap"][@"user_id"]);
    }
    if (respondDic[@"accountMap"]) {
        self.head_img = nullToEmpty(respondDic[@"accountMap"][@"head_img"]);
        self.cert_badge = [respondDic[@"accountMap"][@"cert_badge"] integerValue];
        self.achievement_badge_list = respondDic[@"accountMap"][@"achievement_badge_list"];
    }
    return self;
}
@end
