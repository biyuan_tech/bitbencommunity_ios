//
//  BYAppraiseCell.m
//  BibenCommunity
//
//  Created by 随风 on 2018/12/5.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYAppraiseCell.h"
#import "BYAppraiseModel.h"
#import "StringAttributeHelper.h"

@interface BYAppraiseCell ()

/** userlogo */
@property (nonatomic ,strong) PDImageView *userlogoImgView;
/** username */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 评论时间 */
@property (nonatomic ,strong) UILabel *timeLab;
/** comment */
@property (nonatomic ,strong) UILabel *commentLab;
/** 文章标题 */
@property (nonatomic ,strong) UILabel *themeTitleLab;
/** 文章封面 */
@property (nonatomic ,strong) PDImageView *themeImgView;
/** bbt */
@property (nonatomic ,strong) UILabel *bbtLab;
/** 高亮 */
@property (nonatomic ,strong) StringAttribute *attribute;


@end

@implementation BYAppraiseCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYAppraiseModel *model = object[indexPath.row];
    self.indexPath = indexPath;
    self.userlogoImgView.style = model.cert_badge;
    [self.userlogoImgView uploadHDImageWithURL:model.comment_pic callback:nil];
    self.userNameLab.text = model.comment_name;
    self.timeLab.text = model.create_time_show;
    self.commentLab.attributedText = model.commentAttributedString;
    self.themeTitleLab.attributedText = model.themeAttributedString;
    if (model.theme_type == BY_THEME_TYPE_ARTICLE) {
        if (model.articleModel.article_type == article_typeNormal) {
            [self.themeImgView by_setImageName:@"common_point"];
        }else{
            [self.themeImgView uploadImageWithURL:model.articleModel.picture[0] placeholder:nil callback:nil];
        }
    }
    else if (model.theme_type == BY_THEME_TYPE_LIVE){
        [self.themeImgView uploadImageWithURL:model.liveModel.live_cover_url placeholder:nil callback:nil];
    }
    self.bbtLab.text = model.reward;
    [self.commentLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(model.commentSize.width);
        make.height.mas_equalTo(ceil(model.commentSize.height));
    }];
    
    [self.themeTitleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(model.themeSize.width);
        make.height.mas_equalTo(ceil(model.themeSize.height));
    }];
}

#pragma mark - action
- (void)themeViewAction{
    [self sendActionName:@"themeAction" param:nil indexPath:self.indexPath];
}

#pragma mark - configUI

- (void)setContentView{
    self.attribute = [[StringAttribute alloc] init];
    
    self.userlogoImgView = [[PDImageView alloc] init];
    self.userlogoImgView.backgroundColor = [UIColor clearColor];
    self.userlogoImgView.layer.cornerRadius = 20;
    self.userlogoImgView.clipsToBounds = YES;
    self.userlogoImgView.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:self.userlogoImgView];
    [self.userlogoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(40);
        make.left.mas_equalTo(19);
        make.top.mas_equalTo(18);
    }];
    
    // 头像背景
    UIImageView *userlogoBg = [[UIImageView alloc] init];
    [userlogoBg by_setImageName:@"common_userlogo_bg"];
    [self.contentView addSubview:userlogoBg];
    [self.contentView insertSubview:userlogoBg belowSubview:self.userlogoImgView];
    @weakify(self);
    [userlogoBg mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(userlogoBg.image.size.width + 10);
        make.height.mas_equalTo(userlogoBg.image.size.height + 10);
        make.centerX.equalTo(self.userlogoImgView.mas_centerX).with.offset(0);
        make.centerY.equalTo(self.userlogoImgView.mas_centerY).with.offset(0);
    }];
    
    // 名字
    self.userNameLab = [UILabel by_init];
    self.userNameLab.text = @"name";
    self.userNameLab.textColor = kColorRGBValue(0x4c4c4c);
    self.userNameLab.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:self.userNameLab];
    [self.userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(73);
        make.top.mas_equalTo(24);
        make.height.mas_equalTo(stringGetHeight(self.userNameLab.text, self.userNameLab.font.pointSize));
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    // 评论时间
    self.timeLab = [UILabel by_init];
    self.timeLab.textColor = kColorRGBValue(0x999999);
    self.timeLab.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:self.timeLab];
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(73);
        make.top.mas_equalTo(self.userNameLab.mas_bottom).mas_offset(5);
        make.width.mas_offset(150);
        make.height.mas_equalTo(12);
    }];
    
    // 评论内容
    self.commentLab = [UILabel by_init];
    self.commentLab.textColor = kColorRGBValue(0x2e2e2e);
    self.commentLab.font = [UIFont systemFontOfSize:14];
    self.commentLab.numberOfLines = 0;
    [self.contentView addSubview:self.commentLab];
    [self.commentLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(75);
        make.right.mas_equalTo(-kCellRightSpace);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
    
    UIView *themeView = [UIView by_init];
    themeView.backgroundColor = kColorRGBValue(0xf3f3f3);
    themeView.layer.cornerRadius = 5.0f;
    [self.contentView addSubview:themeView];
    [themeView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(kCellLeftSpace);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.height.mas_equalTo(63);
        make.top.mas_equalTo(self.commentLab.mas_bottom).mas_offset(14);
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(themeViewAction)];
    [themeView addGestureRecognizer:tapGestureRecognizer];
    
    // 主题内容
    self.themeTitleLab = [UILabel by_init];
    self.themeTitleLab.textColor = kColorRGBValue(0x717070);
    self.themeTitleLab.font = [UIFont systemFontOfSize:13];
    self.themeTitleLab.numberOfLines = 2;
    [themeView addSubview:self.themeTitleLab];
    [self.themeTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(12);
        make.right.mas_equalTo(-65);
        make.height.mas_lessThanOrEqualTo(44);
    }];
    
    self.themeImgView = [[PDImageView alloc] init];
    self.themeImgView.clipsToBounds = YES;
    self.themeImgView.contentMode = UIViewContentModeScaleAspectFill;
    [themeView addSubview:self.themeImgView];
    [self.themeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(43);
        make.right.mas_equalTo(-10);
        make.centerY.mas_equalTo(0);
    }];
    
    UIImageView *bpImgView = [[UIImageView alloc] init];
    [bpImgView by_setImageName:@"common_bp"];
    [self.contentView addSubview:bpImgView];
    [bpImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(19);
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(themeView.mas_bottom).mas_offset(10);
    }];
    
    // bbt
    self.bbtLab = [UILabel by_init];
    self.bbtLab.textColor = kColorRGBValue(0x414141);
    self.bbtLab.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:self.bbtLab];
    [self.bbtLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(bpImgView.mas_centerY).mas_offset(0);
        make.left.mas_equalTo(bpImgView.mas_right).mas_offset(5);
        make.height.mas_equalTo(12);
        make.right.mas_equalTo(-20);
    }];
    
    UIView *bottomView = [UIView by_init];
    bottomView.backgroundColor = kColorRGBValue(0xf8f8f8);
    [self.contentView addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(10);
    }];
}

@end
