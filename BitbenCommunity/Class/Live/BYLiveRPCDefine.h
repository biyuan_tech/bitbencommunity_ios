//
//  BYLiveRPCDefine.h
//  BY
//
//  Created by 黄亮 on 2018/7/27.
//  Copyright © 2018年 Belief. All rights reserved.
//

#ifndef BYLiveRPCDefine_h
#define BYLiveRPCDefine_h

#import "URLConstance.h"

#pragma mark - 首页
/** 首页获取推荐直播+感兴趣用户+banner */
static NSString * RPC_recommend_live = @"live/recommend_live";

/** 首页获取直播及预告 */
static NSString * RPC_get_more_lives = @"live/get_more_lives";

/** 首页热门观点 + 视频推荐 */
static NSString * RPC_hot_content = @"live/hot_content";

/** 首页话题列表 */
static NSString * RPC_topic_list = @"live/topic_list";

/** 获取直播首页列表 */
//static NSString * RPC_get_all_live_list = @"get_all_live_list";
/** 获取直播首页列表 */
static NSString * RPC_get_homepage_list = @"live/get_homepage_list";

/** 获取首页关注列表 */
static NSString * RPC_attention_homepage = @"live/attention_homepage";

/** 首页话题 */
static NSString * RPC_topic_homepage = @"live/topic_homepage";

/** 首页banner点击统计 */
static NSString * RPC_click_banner = @"user/click_banner";

/** 分页查询首页文章（分析师，机构-媒体，机构-项目方） */
static NSString * RPC_page_index_article = @"article/page_index_article";

/** 首页签到 */
static NSString * RPC_attendance_home_page = @"account/attendance_home_page";

#pragma mark - 搜索

/** 首页热门搜索记录 */
static NSString * RPC_hot_search = @"live/hot_search";

/** 搜索结果 */
static NSString * RPC_search_list = @"live/search_list";

/** 获取上传凭证 */
static NSString * RPC_Get_upload_signature = @"live/get_upload_signature";

/** 上传视频信息成功后同步到服务端 */
static NSString * RPC_insert_vod_record = @"live/insert_vod_record";

/** 创建在直播间 */
static NSString * RPC_create_live_room = @"live/create_live_room";

/** 更新直播状态(上报直播状态，结束直播) */
static NSString * RPC_switch_live = @"live/switch_live";

/** 获取直播登录的sig */
//static NSString * RPC_get_user_sig = @"live/get_user_sig";

/** 新建直播记录 */
static NSString * RPC_create_live_record = @"live/create_live_record";
/** 创建直播 */
static NSString * RPC_create_live = @"create_live";

/** 校验账号id是否存在 */
static NSString * RPC_is_exist_account_id = @"live/is_exist_account_id";

/** 更新直播间信息 */
static NSString * RPC_update_live_room = @"live/update_live_room";

/** 更新直播信息 */
static NSString * RPC_update_live_record = @"live/update_live_record";

/** 获取直播间信息 */
static NSString * RPC_get_live_room = @"live/get_live_room";

/** 获取直播列表 */
static NSString * RPC_get_live_list = @"live/get_live_list";

/** 获取首页banner列表 */
static NSString * RPC_get_banner_list = @"user/get_banner_list";

/** 判断是否有直播间 */
static NSString * RPC_whether_has_room = @"live/whether_has_room";

/** 更新ppt直播课件 */
static NSString * RPC_update_courseware = @"live/update_courseware";

/** 直播顶踩 */
static NSString * RPC_create_interaction = @"user/create_interaction";

/** 取消顶踩，收藏 */
static NSString * RPC_delete_interaction = @"user/delete_interaction";

/** 直播打赏、付费提问 */
static NSString * RPC_create_reward = @"user/create_reward";

/** 获取当前打赏的剩余金额 */
static NSString * RPC_reward_window = @"user/reward_window";

/** 心跳包 */
static NSString * RPC_get_heartbeat = @"user/get_heartbeat";;

/** 获取直播详情数据 */
static NSString * RPC_get_live_details = @"live/get_live_details";

/** 关注预约直播 */
static NSString * RPC_do_attention_live = @"user/do_attention_live";

/** 取消预约直播 */
static NSString * RPC_delete_attention_live = @"user/delete_attention_live";

/** 插入聊天记录 */
static NSString * RPC_insert_chat_history = @"live/insert_chat_history";

/** 获取聊天记录 */
static NSString * RPC_get_chat_history = @"live/get_chat_history";

/** 举报 */
static NSString * RPC_create_report = @"user/create_report";

/** 获取他人主页 */
static NSString * RPC_get_his_data = @"user/get_his_data";

/** 获取指定作者文章 */
static NSString *RPC_page_author_article = @"article/page_author_article";

/** 查询我的点评 */
static NSString *RPC_page_my_comment = @"user/page_my_comment";

/** 连麦混流 */
static NSString *RPC_mix_stream = @"live/mix_stream";

/** 获取推流地址 */
static NSString *RPC_get_push_url = @"live/get_push_url";

/** 分页获取消息记录 */
static NSString *RPC_page_chat_history = @"live/page_chat_history";

/** 评论 */
static NSString *RPC_page_comment = @"user/page_comment";

/** 删除文章 */
static NSString *RPC_delete_article = @"article/delete_article";

/** 删除直播 */
static NSString *RPC_delete_live = @"live/delete_live";

#pragma mark - 课程

/** 课程首页 */
static NSString *RPC_course_homepage = @"live/course_homepage";

/** 课程简介 */
static NSString *RPC_course_brief_intro = @"live/course_brief_intro";

/** 课程目录 */
static NSString *RPC_course_catalogue = @"live/course_catalogue";

#pragma mark - 社群

/** 分页社群信息 */
static NSString *RPC_page_community = @"user/page_community";

/** 获取社群信息 */
static NSString *RPC_get_community = @"user/get_community";

#pragma mark - 活动

/** 活动创建 */
static NSString *RPC_create_activity = @"live/create_activity";

/** 分页查询我创建的活动 */
static NSString *RPC_page_my_activity = @"live/page_my_activity";

/** 分页查询活动 */
static NSString *RPC_page_activity = @"live/page_activity";

/** 获取单条活动 */
static NSString *RPC_get_activity = @"live/get_activity";

/** 删除单条活动 */
static NSString *RPC_delete_activity = @"live/delete_activity";

/** 分页查询我报名的活动 */
static NSString *RPC_page_my_enroll = @"live/page_my_enroll";

/** 审核活动报名 */
static NSString *RPC_handle_activity_enroll = @"live/handle_activity_enroll";

/** 分页查询活动报名 */
static NSString *RPC_page_activity_enroll = @"live/page_activity_enroll";

/** 新增活动报名 */
static NSString *RPC_create_activity_enroll = @"live/create_activity_enroll";
#endif /* BYLiveRPCDefine_h */
