//
//  BYPPTUploadViewModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/10/9.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPPTUploadViewModel.h"
#import "BYPPTUploadController.h"
#import "BYPPTImgModel.h"
#import "BYPPTImgEditView.h"

#define K_VC_PPT ((BYPPTUploadController *)S_VC)
static NSString *headerTitle = @"长按图片进入编辑状态，完成则退出编辑";
static NSString *headerEditTitle = @"长按图片拖动可调整顺序";
@interface BYPPTUploadViewModel ()

@property (nonatomic ,strong) UIButton *rightItemButton;
@property (nonatomic ,strong) UIScrollView *scrollView;
@property (nonatomic ,strong) UILabel *headerTitle;


@property (nonatomic ,strong) NSMutableArray *editViewArr;
@property (nonatomic ,strong) NSMutableArray *editViewDataArr;
@property (nonatomic ,assign) CGFloat editViewW;
@property (nonatomic ,assign) CGFloat editViewH;
/** 用于替换移动时的editView */
@property (nonatomic ,strong) BYPPTImgEditView *replaceEditView;
@property (nonatomic ,strong) UIButton *replaceBtn;
/** 用于替换移动时的editView的index */
@property (nonatomic ,assign) NSInteger replaceIndex;
/** 用于记录手势开始时手势位于editView的point */
@property (nonatomic ,assign) CGPoint currentGesturePoint;
/** 是否上传七牛成功 */
@property (nonatomic ,assign) BOOL isUploadQN;
/** 当前课件是否上传完成 */
@property (nonatomic ,assign) BOOL isAllUpload;
/** 当前是否处于编辑状态 */
@property (nonatomic ,assign) BOOL isEditStatus;

@end

@implementation BYPPTUploadViewModel

- (void)setContentView{
    self.editViewArr = [NSMutableArray array];
    self.editViewDataArr = [NSMutableArray array];
    [self addRightItemButton];
    [self configSubView];
}

#pragma mark - custom Method

- (void)verifyGesturePoint:(UIGestureRecognizer *)gestureRecognizer{
}

- (void)reloadEditStatus:(BOOL)isEdit{
    //    self.rightItemButton.selected = isEdit;
    self.isEditStatus = isEdit;
    self.rightItemButton.titleLabel.text = isEdit ? @"完成" : @"上传";
    self.headerTitle.text = isEdit ? headerEditTitle : headerTitle;
    [_editViewArr enumerateObjectsUsingBlock:^(BYPPTImgEditView *editView, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([editView isKindOfClass:[BYPPTImgEditView class]]) {
            [editView setEditStatus:isEdit];
        }
    }];
}

// 刷新界面布局
- (void)reloadLayoutEditView{
    [_editViewArr enumerateObjectsUsingBlock:^(BYPPTImgEditView *editView, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([editView isKindOfClass:[BYPPTImgEditView class]]) {
            editView.indexLab.text = [NSString stringWithFormat:@"%02d",(int)idx];
        }
        if ([self.scrollView.subviews indexOfObject:editView] != NSNotFound) {
            @weakify(self);
            [editView mas_updateConstraints:^(MASConstraintMaker *make) {
                @strongify(self);
                [self setMasConstraints:make editView:editView index:idx];
            }];
        }
        else{
            if ([editView isKindOfClass:[BYPPTImgEditView class]]) {
                [self.scrollView addSubview:editView];
                @weakify(self);
                [editView mas_makeConstraints:^(MASConstraintMaker *make) {
                    @strongify(self);
                    [self setMasConstraints:make editView:editView index:idx];
                }];
            }
        }
        
        if ([editView isKindOfClass:[BYPPTImgEditView class]]) {
            // 修改scrollView的contentSize
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (idx == self.editViewArr.count - 1) {
                    [self.scrollView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, IS_iPhoneX ? 110 : 85, 0));
                        make.bottom.mas_equalTo(editView.mas_bottom).offset(20).priorityLow();
                    }];
                }
            });
        }
    }];
}

// 设置布局
- (void)setMasConstraints:(MASConstraintMaker *)make editView:(BYPPTImgEditView *)editView index:(NSInteger)index{
    //竖排个数
    NSInteger rowInteger = index/2;
    //横排个数
    NSInteger sectionInteger = index%2;
    make.left.mas_equalTo(10 + (self.editViewW + 10)*sectionInteger);
    make.top.mas_equalTo(33 + (self.editViewH + 10)*rowInteger);
    make.width.mas_equalTo(self.editViewW);
    make.height.mas_equalTo(self.editViewH);
}

// 新增ppt课件数据
- (void)addNewPPTEditViewData:(NSArray *)imageData{
    @weakify(self);
    NSInteger nowNum = _editViewDataArr.count;
    [imageData enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * _Nonnull stop) {
        @strongify(self);
        BYPPTImgModel *model;
        if ([obj isKindOfClass:[BYPPTImgModel class]]) {
            model = obj;
            model.index = [NSString stringWithFormat:@"%.2d",(int)(nowNum + idx)];
        }
        else{
            model = [[BYPPTImgModel alloc] init];
            model.index = [NSString stringWithFormat:@"%.2d",(int)(nowNum + idx)];
            model.isSend = NO;
            if ([obj isKindOfClass:[UIImage class]]) {
                model.image = obj;
            }
            else{
                model.url = obj;
            }
        }
        @weakify(self);
        BYPPTImgEditView *editView = [[BYPPTImgEditView alloc] init];
        [editView setEditStatus:self.isEditStatus];
        editView.longGestureRecognizerBlock = ^(UIGestureRecognizer * _Nonnull gestureRecognizer) {
            @strongify(self);
            [self longGestureRecognizer:gestureRecognizer];
        };
        editView.didDelBtnActionHandle = ^(BYPPTImgEditView * _Nonnull editView) {
            @strongify(self);
            [self delBtnAction:editView];
        };
        editView.clickActionBlock = ^(BYPPTImgEditView * _Nonnull editView) {
            
        };
        [editView reloadData:model];
        [self.editViewDataArr addObject:model];
        [self.editViewArr addObject:editView];
    }];
}

#pragma mark - gestureRecognizer/手势长按
- (void)longGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer{
    [self reloadEditStatus:YES];
    BYPPTImgEditView *editView = (BYPPTImgEditView *)gestureRecognizer.view;
    CGPoint point = [gestureRecognizer locationInView:editView.superview];
    if (CGPointEqualToPoint(self.currentGesturePoint, CGPointZero)) {
        self.currentGesturePoint = [gestureRecognizer locationInView:editView];
    }
    @weakify(self);
    [editView mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(point.x - self.currentGesturePoint.x);
        make.top.mas_equalTo(point.y - self.currentGesturePoint.y);
    }];
    [editView.superview bringSubviewToFront:editView];
    // 手势开始
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        NSInteger index = [_editViewArr indexOfObject:editView];
        self.replaceEditView = editView;
        self.replaceIndex = index;
        [self.editViewArr replaceObjectAtIndex:index withObject:_replaceBtn];
    }
    [self verifyGesturePoint:gestureRecognizer];
    // 手势移动
    [self.editViewArr enumerateObjectsUsingBlock:^(UIView *view, NSUInteger idx, BOOL * _Nonnull stop) {
        if (CGRectContainsPoint(view.frame, point)) {
            [self.editViewArr removeObject:_replaceBtn];
            [self.editViewArr insertObject:_replaceBtn atIndex:idx];
            [self.scrollView updateConstraintsIfNeeded];
            [UIView animateWithDuration:0.5 animations:^{
                [self reloadLayoutEditView];
                [self.scrollView layoutIfNeeded];
            }];
        }
    }];
    
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        //将替换的btn与itemView换回，重新布局
        NSInteger index = [self.editViewArr indexOfObject:_replaceBtn];
        [self.editViewArr replaceObjectAtIndex:index withObject:_replaceEditView];
        BYPPTImgModel *model = [_editViewDataArr objectAtIndex:_replaceIndex];
        [self.editViewDataArr removeObject:model];
        [self.editViewDataArr insertObject:model atIndex:index];
        [self.scrollView updateConstraintsIfNeeded];
        [UIView animateWithDuration:0.3 animations:^{
            [self reloadLayoutEditView];
            [self.scrollView layoutIfNeeded];
        }completion:^(BOOL finished) {
            self.currentGesturePoint = CGPointZero;
        }];
    }
}

#pragma mark - action

// 添加课件
- (void)addBtnAction{
    if (K_VC_PPT.live_type == 0 && self.editViewDataArr.count >= 1) {
        showToastView(@"当前课件数达到上限", S_V_VIEW);
        return;
    }
    GWAssetsLibraryViewController *assetVC = [[GWAssetsLibraryViewController alloc] init];
    @weakify(self);
    [assetVC selectImageArrayFromImagePickerWithMaxSelected:K_VC_PPT.live_type == 0 ? 1 : 50 andBlock:^(NSArray *selectedImgArr) {
        @strongify(self);
        // 在新增ppt课件后重置七牛上传状态
        self.isUploadQN = NO;
        // 新增新ppt信息与创建的新editview
        [self addNewPPTEditViewData:selectedImgArr];
        // reload布局
        [self reloadLayoutEditView];
    }];
    [S_V_NC pushViewController:assetVC animated:YES];
}

// 删除ppt课件
- (void)delBtnAction:(BYPPTImgEditView *)editView{
    NSInteger index = [_editViewArr indexOfObject:editView];
    [_editViewArr removeObjectAtIndex:index];
    [_editViewDataArr removeObjectAtIndex:index];
    [editView removeFromSuperview];
    [self reloadLayoutEditView];
}

// 开始编辑课件
- (void)beginEditPPT{
    [self reloadEditStatus:YES];
}

// 开始上传课件
- (void)beginUploadPPT{
    [self reloadEditStatus:NO];
    if (!_editViewDataArr.count && !K_VC_PPT.pptImgs.count) {
        showToastView(@"请先添加课件", S_V_VIEW);
        return;
    }
    
    BYToastView *toastView = [BYToastView toastViewPresentLoadingInView:S_V_VIEW.window];
    
    if (!_editViewDataArr.count) {
        [self loadRequestEmptyPPt:toastView];
        return;
    }
    // 如当前七牛已上传成功则直接更新服务器
    if (_isUploadQN) {
        [self uploadPPTImgURL:toastView];
        return;
    }
    dispatch_group_t group = dispatch_group_create();
    // 控制最大并发数
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(_editViewDataArr.count > 10 ? 10 : _editViewDataArr.count);
    for (int i = 0; i < _editViewDataArr.count; i++) {
        BYPPTImgModel *model = _editViewDataArr[i];
        OSSFileModel *fileModel = [[OSSFileModel alloc] init];
        fileModel.objcImage = model.image;
        fileModel.objcName  = [NSString stringWithFormat:@"%@-%@-%d",[NSDate by_stringFromDate:[NSDate by_date] dateformatter:@"yyyy-MM-dd-HH:mm:ss"],@"PPTIMG",i];
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        // 用于监控请求线程在请求完成后回调
        // 多线程上传图片
        dispatch_group_async(group, dispatch_get_global_queue(0, 0), ^{
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            [[OSSManager sharedUploadManager] uploadImageManagerWithImageList:[@[fileModel] copy] withUrlBlock:^(NSArray *imgUrlArr) {
                model.url = imgUrlArr[0];
                model.isSend = YES;
                dispatch_semaphore_signal(semaphore);
                dispatch_semaphore_signal(sema);
            }];
            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        });
    }
    // 上传七牛全部回调
    dispatch_group_notify(group, dispatch_get_global_queue(0, 0), ^{
        self.isUploadQN = YES;
        [self uploadPPTImgURL:toastView];
    });
}

// 上传七牛回调的地址
- (void)uploadPPTImgURL:(BYToastView *)toastView{
    NSMutableArray *params = [NSMutableArray array];
    for (BYPPTImgModel *model in _editViewDataArr) {
        [params addObject:@{@"url":nullToEmpty(model.url),@"index":model.index}];
    }
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestUpdateCourseware:params live_record_id:nullToEmpty(K_VC_PPT.record_id) successBlock:^(id object) {
        @strongify(self);
        [self reloadEditStatus:NO];
        if (K_VC_PPT.didUploadPPTHandle) K_VC_PPT.didUploadPPTHandle(self.editViewDataArr);
        showToastView(@"上传成功", S_V_VIEW);
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"上传失败", S_V_VIEW);
    }];
}

- (void)loadRequestEmptyPPt:(BYToastView *)toastView{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestUpdateCourseware:@[] live_record_id:nullToEmpty(K_VC_PPT.record_id) successBlock:^(id object) {
        @strongify(self);
        [self reloadEditStatus:NO];
        if (K_VC_PPT.didUploadPPTHandle) K_VC_PPT.didUploadPPTHandle(self.editViewDataArr);
        showToastView(@"上传成功", S_V_VIEW);
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"上传失败", S_V_VIEW);
    }];
}

#pragma mark - configUI

- (void)configSubView{
    self.scrollView = [[UIScrollView alloc] init];
    [S_V_VIEW addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, IS_PhoneXAll ? 110 : 85, 0));
    }];
    
    self.headerTitle = [UILabel by_init];
    self.headerTitle.textColor = kTextColor_77;
    [self.headerTitle setBy_font:13];
    self.headerTitle.text = headerTitle;
    [_scrollView addSubview:self.headerTitle];
    [self.headerTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(13);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(15);
    }];
    
    self.editViewW = (kCommonScreenWidth - 35)/2;
    self.editViewH = 100*self.editViewW/172;
    self.replaceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addPPTImgEditView];
    
    // 添加课件按钮
    UIButton *addBtn = [UIButton by_buttonWithCustomType];
    [addBtn setBackgroundColor:kBgColor_238];
    [addBtn setBy_attributedTitle:@{@"title":@"添加课件",NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [addBtn addTarget:self action:@selector(addBtnAction) forControlEvents:UIControlEventTouchUpInside];
    addBtn.layer.cornerRadius = 10;
    [S_V_VIEW addSubview:addBtn];
    [addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.right.mas_equalTo(-kCellLeftSpace);
        make.bottom.mas_equalTo(kiPhoneX_Mas_buttom(23));
        make.height.mas_equalTo(46);
    }];
}

- (void)addRightItemButton{
    @weakify(self);
    self.rightItemButton = [S_VC rightBarButtonWithTitle:@"上传" barNorImage:nil barHltImage:nil action:^{
        @strongify(self);
        if (self.isEditStatus) {
            // 课件编辑
            [self reloadEditStatus:NO];
        }
        else {
            // 课件上传
            [self beginUploadPPT];
        }
    }];
    [self.rightItemButton setTitleColor:kTextColor_237 forState:UIControlStateNormal];
}

// 初始添加ppt课件
- (void)addPPTImgEditView{
    [self addNewPPTEditViewData:K_VC_PPT.pptImgs];
    [self reloadLayoutEditView];
}

@end
