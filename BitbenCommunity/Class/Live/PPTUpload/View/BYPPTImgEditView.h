//
//  BYPPTImgEditView.h
//  BibenCommunity
//
//  Created by 随风 on 2018/10/9.
//  Copyright © 2018 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYPPTImgModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYPPTImgEditView : UIView

/** 排序indx */
@property (nonatomic ,strong) UILabel *indexLab;

@property (nonatomic ,strong) BYPPTImgModel *model;
/** 删除回调 */
@property (nonatomic ,copy) void (^didDelBtnActionHandle)(BYPPTImgEditView *editView);
/** 长按手势响应 */
@property (nonatomic ,copy) void (^longGestureRecognizerBlock)(UIGestureRecognizer *gestureRecognizer);
/** 点击响应 */
@property (nonatomic ,copy) void (^clickActionBlock)(BYPPTImgEditView *editView);

- (void)reloadData:(BYPPTImgModel *)model;
- (void)setEditStatus:(BOOL)isEdit;
@end

NS_ASSUME_NONNULL_END
