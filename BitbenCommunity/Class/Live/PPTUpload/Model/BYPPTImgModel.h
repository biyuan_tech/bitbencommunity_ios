//
//  BYPPTImgModel.h
//  BibenCommunity
//
//  Created by 随风 on 2018/10/9.
//  Copyright © 2018 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYPPTImgModel : NSObject

/** 图片地址*/
@property (nonatomic ,copy) NSString *url;
/** 本地图片 */
@property (nonatomic ,strong) UIImage *image;
/** 图片index */
@property (nonatomic ,copy) NSString *index;
/** 是否已发送 */
@property (nonatomic ,assign) BOOL isSend;

@end

NS_ASSUME_NONNULL_END
