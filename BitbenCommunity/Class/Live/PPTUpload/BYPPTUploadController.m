//
//  BYPPTUploadController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/10/9.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYPPTUploadController.h"
#import "BYPPTUploadViewModel.h"

@interface BYPPTUploadController ()

@end

@implementation BYPPTUploadController

- (Class)getViewModelClass{
    return [BYPPTUploadViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"课件";
}


@end
