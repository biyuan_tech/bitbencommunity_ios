//
//  NetworkAdapter+PPTLive.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "NetworkAdapter+PPTLive.h" 
#import "LiveFourumOperateModel.h"
#import "ArticleSubHuifuSingleModel.h"

@implementation NetworkAdapter (PPTLive)

#pragma mark - 顶&踩
-(void)liveComment_OperateWithCommentID:(NSString *)commentId itemId:(NSString *)itemId theme_type:(LiveForumDetailViewControllerType)type hasUp:(BOOL)up block:(void(^)(BOOL isSuccessed ,BOOL isOperate))block{
    __weak typeof(self)weakSelf = self;

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:commentId forKey:@"comment_id"];
    [params setValue:itemId forKey:@"theme_id"];
    [params setValue:@(type) forKey:@"theme_type"];
    
    if (up){
        [params setValue:@"0" forKey:@"operate_type"];
    } else {
        [params setValue:@"1" forKey:@"operate_type"];
    }
    
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:create_comment_operate requestParams:params responseObjectClass:[LiveFourumOperateModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            LiveFourumOperateModel *model = (LiveFourumOperateModel *)responseObject;
            
            if (block){
                block(isSucceeded, model.isOperate);
            }
        }
    }];
}

#pragma mark - 添加回复
-(void)liveCreateCommentWiththemeId:(NSString *)themeId type:(NSInteger)type ThemeInfo:(LiveFourumRootListSingleModel *)themeinfo content:(NSString *)content block:(void(^)(BOOL isSuccessed,NSString *commentId))block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:themeId forKey:@"theme_id"];
    [params setObject:@(type) forKey:@"theme_type"];
    if (!themeinfo){
        [params setObject:@"0" forKey:@"reply_comment_id"];
    } else {
        if (themeinfo.transferTempCommentId.length){
            [params setObject:themeinfo.transferTempCommentId forKey:@"reply_comment_id"];
        } else {
            [params setObject:themeinfo._id forKey:@"reply_comment_id"];
        }
        
        [params setObject:themeinfo.user_id forKey:@"reply_user_id"];
    }
    
    [params setObject:content forKey:@"content"];
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:create_comment requestParams:params responseObjectClass:[ArticleSubHuifuSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ArticleSubHuifuSingleModel *singleModel = (ArticleSubHuifuSingleModel *)responseObject;
            if (block){
                block(YES,singleModel.comment_id);
            }
        }
    }];
}

#pragma mark - 添加次级回复
-(void)liveCreateSubCommentWiththemeId:(NSString *)themeId ThemeInfo:(LiveFourumRootListSingleModel *)themeinfo content:(NSString *)content block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:themeId forKey:@"theme_id"];
    [params setObject:@"0" forKey:@"theme_type"];
    if (!themeinfo){
        [params setObject:@"0" forKey:@"reply_comment_id"];
    } else {
        [params setObject:themeinfo.transferTempCommentId forKey:@"reply_comment_id"];
        [params setObject:themeinfo.reply_user_id forKey:@"reply_user_id"];
    }
    
    [params setObject:content forKey:@"content"];
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:create_comment requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block(YES);
            }
        }
    }];
}


#pragma mark - 获取当前直播信息
-(void)liveGetMainInfoWithBlock:(void(^)(PPTLiveMainInfoModel *model))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:live_ppt_main_info requestParams:nil responseObjectClass:[PPTLiveMainInfoModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            PPTLiveMainInfoModel *liveModel = (PPTLiveMainInfoModel *)responseObject;
            if (block){
                block(liveModel);
            }
        }
    }];
}

@end
