//
//  NetworkAdapter+PPTLive.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "NetworkAdapter.h"
#import "LiveFourumRootListModel.h"
#import "PPTLiveMainInfoModel.h"

typedef NS_ENUM(NSInteger,LiveForumDetailViewControllerType) {
    LiveForumDetailViewControllerTypeNormal = 0,                    /**< 视频评论*/
    LiveForumDetailViewControllerTypeArticle,                       /**< 文章内部评论*/
};


@interface NetworkAdapter (PPTLive)

#pragma mark - 顶 踩
-(void)liveComment_OperateWithCommentID:(NSString *)commentId itemId:(NSString *)itemId theme_type:(LiveForumDetailViewControllerType)type hasUp:(BOOL)up block:(void(^)(BOOL isSuccessed ,BOOL isOperate))block;
#pragma mark - 添加回复
-(void)liveCreateCommentWiththemeId:(NSString *)themeId type:(NSInteger)type ThemeInfo:(LiveFourumRootListSingleModel *)themeinfo content:(NSString *)content block:(void(^)(BOOL isSuccessed,NSString *commentId))block;
#pragma mark - 添加次级回复
-(void)liveCreateSubCommentWiththemeId:(NSString *)themeId ThemeInfo:(LiveFourumRootListSingleModel *)themeinfo content:(NSString *)content block:(void(^)(BOOL isSuccessed))block;
#pragma mark - 获取当前直播信息
-(void)liveGetMainInfoWithBlock:(void(^)(PPTLiveMainInfoModel *model))block;


@end
