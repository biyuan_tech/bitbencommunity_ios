//
//  LivePPTDescTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/20.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

@interface LivePPTDescTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;

+(CGFloat)calculationCellHeightWIthTitle:(NSString *)title;

@end
