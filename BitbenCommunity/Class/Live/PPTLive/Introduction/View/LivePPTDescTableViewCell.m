//
//  LivePPTDescTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/20.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LivePPTDescTableViewCell.h"

@interface LivePPTDescTableViewCell()
@property (nonatomic,strong)UILabel *descLabel;
@end

@implementation LivePPTDescTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.descLabel = [GWViewTool createLabelFont:@"14" textColor:@"6F6F6F"];
    self.descLabel.numberOfLines = 0;
    [self addSubview:self.descLabel];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    
    self.descLabel.text = transferTitle;
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(16);
    CGSize contentOfSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.descLabel.frame = CGRectMake(LCFloat(16), LCFloat(9), width, contentOfSize.height);
    
}

+(CGFloat)calculationCellHeightWIthTitle:(NSString *)title{
    CGFloat cellHeight = 0;
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(16);
    CGSize contentOfSize = [title sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"14"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += LCFloat(9);
    cellHeight += contentOfSize.height;
    cellHeight += LCFloat(14);
    return cellHeight;
}

@end
