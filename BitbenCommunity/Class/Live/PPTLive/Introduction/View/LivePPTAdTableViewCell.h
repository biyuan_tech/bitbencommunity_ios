//
//  LivePPTAdTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/20.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

@interface LivePPTAdTableViewCellModel : FetchModel

@property (nonatomic,copy)NSString *imgUrl;
@property (nonatomic,assign)BOOL hasLoad;
@property (nonatomic,assign)CGFloat imgHeight;
@property (nonatomic,strong)UIImage *adImg;

@end


@interface LivePPTAdTableViewCell : PDBaseTableViewCell
@property (nonatomic,strong) LivePPTAdTableViewCellModel*transferAdModel;

-(void)actionReloadImgHeightBlock:(void(^)())block;

-(CGFloat)calculationCellHeight:(LivePPTAdTableViewCellModel *)model;

@end




