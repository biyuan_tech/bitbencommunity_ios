//
//  LivePPTPlayerTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LivePPTPlayerTableViewCell.h"

@interface LivePPTPlayerTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)PDImageView *clockImgView;
@property (nonatomic,strong)UILabel *timeLabel;

@end

@implementation LivePPTPlayerTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView;
-(void)createView{
    // 标题
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"4C4B4B"];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
    
    // avatar
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame = CGRectMake(0, 0, LCFloat(23), LCFloat(23));
    self.avatarImgView.clipsToBounds = YES;
    self.avatarImgView.layer.cornerRadius = MIN(self.avatarImgView.size_width, self.avatarImgView.size_height) / 2.;
    [self addSubview:self.avatarImgView];
    
    self.nickNameLabel = [GWViewTool createLabelFont:@"13" textColor:@"878787"];
    [self addSubview:self.nickNameLabel];
    
    self.clockImgView = [[PDImageView alloc]init];
    self.clockImgView.image = [UIImage imageNamed:@"icon_live_clock"];
    self.clockImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.clockImgView];
    
    self.timeLabel = [GWViewTool createLabelFont:@"13" textColor:@"878787"];
    [self addSubview:self.timeLabel];
}

-(void)setTransferMainModel:(PPTLiveMainInfoModel *)transferMainModel{
    _transferMainModel = transferMainModel;
    self.titleLabel.text = transferMainModel.live_title;
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(15);
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(LCFloat(15), LCFloat(14), width, titleSize.height);
    
    // 创建头像
    self.avatarImgView.frame = CGRectMake(LCFloat(17), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(14), LCFloat(23), LCFloat(23));
    NSString *headimg = transferMainModel.speaker_head_img.length ? transferMainModel.speaker_head_img : transferMainModel.head_img;
    [self.avatarImgView uploadImageWithURL:headimg placeholder:nil callback:NULL];

    // 昵称
    self.nickNameLabel.text = transferMainModel.speaker.length ? transferMainModel.speaker : transferMainModel.nickname;
    CGSize nickSize = [Tool makeSizeWithLabel:self.nickNameLabel];
    self.nickNameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(5), 0, nickSize.width, nickSize.height);
    self.nickNameLabel.center_y = self.avatarImgView.center_y;
    
    self.clockImgView.backgroundColor = [UIColor clearColor];
    self.clockImgView.frame = CGRectMake(CGRectGetMaxX(self.nickNameLabel.frame) + LCFloat(30), 0, LCFloat(13), LCFloat(13));
    self.clockImgView.center_y = self.nickNameLabel.center_y;
    
    self.timeLabel.text = transferMainModel.datetime;
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(CGRectGetMaxX(self.clockImgView.frame) + LCFloat(6), 0, timeSize.width, timeSize.height);
    self.timeLabel.center_y = self.clockImgView.center_y;
}

+(CGFloat)calculationCellHeight:(PPTLiveMainInfoModel *)transferMainModel{
    CGFloat cellHeight = 0;
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(15);
    CGSize contentOfSize = [transferMainModel.live_title sizeWithCalcFont:[[UIFont fontWithCustomerSizeName:@"15"] boldFont] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += LCFloat(14);
    cellHeight += contentOfSize.height;
    cellHeight += LCFloat(14);
    cellHeight += LCFloat(23);
    cellHeight += LCFloat(15);
    return cellHeight;
}

@end
