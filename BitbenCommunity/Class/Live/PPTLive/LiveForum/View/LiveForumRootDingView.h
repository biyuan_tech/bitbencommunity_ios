//
//  LiveForumRootDingView.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveFourumRootListModel.h"
#import "ArticleRootSingleModel.h"
#import "NetworkAdapter+PPTLive.h"
#import "NetworkAdapter+Article.h"


typedef NS_ENUM(NSInteger,LiveForumRootDingViewType) {
    LiveForumRootDingViewTypeRootBi,                /**< 文章首页的币*/
    LiveForumRootDingViewTypeRootUp,                /**< 文章首页的顶*/
    LiveForumRootDingViewTypeRootMsg,               /**< 文章首页的消息*/
    LiveForumRootDingViewTypeRootShare,             /**< 分享*/
    
    
    LiveForumRootDingViewTypeUp,                    /**< 顶*/
    LiveForumRootDingViewTypeDown,                  /**< 踩*/
    LiveForumRootDingViewTypeReply,                 /**< 回复*/
    
    // 文章内使用
    LiveForumRootDingViewTypeArticleShare,      /**< 文章分享*/
    LiveForumRootDingViewTypeArticleDing,       /**< 文章顶*/
    LiveForumRootDingViewTypeArticleDown,       /**< 文章踩*/
    LiveForumRootDingViewTypeArticleMessage,       /**< 文章顶*/
    LiveForumRootDingViewTypeArticleMoney,      /**< 文章钱*/
};

@interface LiveForumRootDingView : UIView

@property (nonatomic,assign)LiveForumRootDingViewType transferType;

@property (nonatomic,strong)LiveFourumRootListSingleModel *transferInfoManager;             // 直播的model
@property (nonatomic,strong)ArticleRootSingleModel *transferArticleRootSingleModel;         // 文章的model

// 临时调用
@property (nonatomic,assign) LiveForumDetailViewControllerType transferTempType;            //文章还是视频/
@property (nonatomic,copy)NSString *transferItemId;

// 点击回复按钮
-(void)actionButtonWithReplyBlock:(void(^)())block;
// 点击分享
-(void)actionButtonWithShareBlock:(void(^)())block;

+(CGSize)calculationSize;
@end
