//
//  LiveForumRootInputView.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveFourumRootListModel.h"

#define kInputSendBtnWidth LCFloat(63)                              // 输入框按钮

@interface LiveForumRootInputView : UIView

@property (nonatomic,strong)LiveFourumRootListSingleModel *transferListModel;
@property (nonatomic,strong)UITextView *inputView;
/** 是否有举报按钮 */
@property (nonatomic ,assign) BOOL isReport;


+(CGFloat)calculationHeight:(NSString *)inputText;
+(CGSize)inputStrSize:(NSString *)inputText;                            // 计算输入内容的高度

-(void)actionClickWithSendInfoBlock:(void(^)())block;                   //  点击发送按钮
-(void)actionTextInputChangeBlock:(void(^)())block;
-(void)actionReportBlock:(void(^)())block;                              // 举报按钮

-(void)releaseKeyboard;                                                // 释放键盘
-(void)cleanKeyboard;                                                  // 释放键盘并且清空文字

- (instancetype)initWithFrame:(CGRect)frame isReport:(BOOL)isReport;

@end
