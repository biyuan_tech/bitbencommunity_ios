//
//  LiveForumRootInputView.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LiveForumRootInputView.h"

static char actionClickWithSendInfoBlockKey;
static char actionTextInputChangeBlockKey;
static char actionReportBlockKey;

#define kInputBgTopMargin LCFloat(8)                        // 输入背景距上部&下部
#define kInputBgTopLeftMargin LCFloat(15)                   // 输入背景距左边边上
#define kInputMargin (LCFloat(35) - [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]]) / 2.       // 输入框与输入背景的高度
#define kInputLeftMargin LCFloat(15)                        // 输入框与输入背景的距左



@interface LiveForumRootInputView(){
    CGRect originalRect;
    CGRect keyboardShowRect;
}
@property (nonatomic,strong)UIButton *sendButton;                   /**< 发送按钮*/
@property (nonatomic,strong)PDImageView *bgInputImgView;            /**< 背景图片*/
/** 举报按钮 */
@property (nonatomic ,strong) UIButton *reportBtn;

@end

@implementation LiveForumRootInputView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        originalRect = frame;
        self.backgroundColor = [UIColor hexChangeFloat:@"FFFFFF"];
        [self createView];
        
        //  添加观察者，监听键盘弹出
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardWillShowNotification object:nil];
        
        //  添加观察者，监听键盘收起
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame isReport:(BOOL)isReport{
    self = [super initWithFrame:frame];
    
    if(self){
        self.isReport = isReport;
        originalRect = frame;
        self.backgroundColor = [UIColor hexChangeFloat:@"FFFFFF"];
        [self createView];
        
        //  添加观察者，监听键盘弹出
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardWillShowNotification object:nil];
        
        //  添加观察者，监听键盘收起
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}


-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - createView
-(void)createView{
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 1.);
    [self addSubview:lineView];
    
    self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sendButton.backgroundColor = [UIColor hexChangeFloat:@"424242"];
    [self.sendButton setTitle:@"发送" forState:UIControlStateNormal];
    self.sendButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
    CGFloat detalX = _isReport ? 32 : 0;
    self.sendButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - kInputSendBtnWidth - detalX, LCFloat(9), kInputSendBtnWidth, LCFloat(32));
    self.sendButton.layer.cornerRadius = self.sendButton.size_height / 2.;
    self.sendButton.clipsToBounds = YES;
    [self addSubview:self.sendButton];
    __weak typeof(self)weakSelf = self;
    [self.sendButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithSendInfoBlockKey);
        if (block){
            block();
        }
    }];
    
    self.reportBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.reportBtn setImage:[UIImage imageNamed:@"talk_more"] forState:UIControlStateNormal];
    self.reportBtn.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - 27 , LCFloat(11), 27, 27);
    self.reportBtn.hidden = !_isReport;
    [self addSubview:self.reportBtn];
    [self.reportBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionReportBlockKey);
        if (block){
            block();
        }
    }];
    
    self.bgInputImgView = [[PDImageView alloc]init];
    self.bgInputImgView.backgroundColor = [UIColor hexChangeFloat:@"EEEEEE"];
    self.bgInputImgView.layer.cornerRadius = LCFloat(3);
    self.bgInputImgView.clipsToBounds = YES;
    self.bgInputImgView.userInteractionEnabled = YES;
    [self addSubview:self.bgInputImgView];
    
    self.inputView = [[UITextView alloc]init];
    self.inputView.backgroundColor = [UIColor clearColor];
    self.inputView.font = [UIFont fontWithCustomerSizeName:@"13"];
    self.inputView.userInteractionEnabled = YES;
    [self.bgInputImgView addSubview:self.inputView];

    [self.inputView textViewDidChangeWithBlock:^(NSInteger currentCount) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf calculationTextViewHeight];
    }];
    
    [self.inputView textViewSendActionBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithSendInfoBlockKey);
        if (block){
            block();
        }
    }];
    
    self.bgInputImgView.frame = CGRectMake(LCFloat(15), LCFloat(7), self.sendButton.orgin_x - LCFloat(14) - LCFloat(15), 2 * LCFloat(9) + [NSString contentofHeightWithFont:self.inputView.font]);
    self.inputView.frame = CGRectMake(LCFloat(3), 0, self.bgInputImgView.size_width - 2 * LCFloat(3), self.bgInputImgView.size_height);
    self.inputView.placeholder = @"写评论";
    self.inputView.limitMax = 300;
}



-(void)setTransferListModel:(LiveFourumRootListSingleModel *)transferListModel{
    _transferListModel = transferListModel;
    
    self.inputView.placeholder = [NSString stringWithFormat:@"回复%@:",transferListModel.comment_name];
}

-(void)actionClickWithSendInfoBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithSendInfoBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionReportBlock:(void (^)())block{
    objc_setAssociatedObject(self, &actionReportBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);

}

-(void)actionTextInputChangeBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionTextInputChangeBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 计算文字高度
+(CGFloat)calculationHeight:(NSString *)inputText{
    // 2. 获取输入内容的高度
    CGSize fontSize = [LiveForumRootInputView inputStrSize:inputText];

    // 3. 获取输入内容的高度
    CGFloat cellHeight = 0;
    cellHeight += kInputBgTopMargin * 2;            // 输入背景距上部
    cellHeight += kInputMargin * 2;                 // 输入框
    cellHeight += MAX(fontSize.height,[NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]]);
    
    // 如果是iphoneX 就加入底部的段落。否则就不加
    if (IS_PhoneXAll){
        cellHeight += LCFloat(20);
    }
    
    return cellHeight;
}

+(CGSize)inputStrSize:(NSString *)inputText{
    if ([inputText isEqualToString:@""]){
        inputText = @"1";
    }
    
    // 1. 计算输入框实际宽度
    CGFloat inputWidth = kScreenBounds.size.width;
    inputWidth -= kInputBgTopLeftMargin - kInputSendBtnWidth - kInputBgTopLeftMargin * 2;
    inputWidth -= kInputLeftMargin * 2;
    
    // 2. 获取输入内容的高度
    CGSize fontSize = [inputText sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"13"] constrainedToSize:CGSizeMake(inputWidth, CGFLOAT_MAX)];
    
    // 2.1 限制输入内容的高度，最高不超过3行
    CGFloat masonrySizeHeight = MIN(3 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]], fontSize.height);
    
    CGSize newSize = CGSizeMake(fontSize.width, masonrySizeHeight);
    return newSize;
}

-(void)calculationTextViewHeight{
    // 1. 进行block 输出
    void(^block)() = objc_getAssociatedObject(self, &actionTextInputChangeBlockKey);
    if (block){
        block();
    }
    
    // 2. 计算位置
    // 2.1 计算输入内容高度
    CGFloat inputSizeHeight = [LiveForumRootInputView inputStrSize:self.inputView.text].height;
    
    // 2. 计算 输入框背景内容
    self.bgInputImgView.frame = CGRectMake(kInputLeftMargin, kInputBgTopMargin, self.bgInputImgView.size_width,inputSizeHeight + 2 * kInputMargin);

    self.inputView.size_height = self.bgInputImgView.size_height;
    
    // 3. 计算位置
    self.size_height = self.bgInputImgView.size_height + 2 * kInputBgTopMargin;
    
    self.orgin_y = keyboardShowRect.origin.y - self.size_height;

    // 如果是iphoneX 就加入底部的段落。否则就不加
    if (IS_PhoneXAll){
        self.size_height += LCFloat(20);
    }
}


-(void)releaseKeyboard{
    if (self.inputView.isFirstResponder){
        [self.inputView resignFirstResponder];
    }
}

-(void)cleanKeyboard{
    self.inputView.text = @"";
    self.inputView.placeholder = @"写评论";
    [self releaseKeyboard];
}


- (void)keyBoardDidShow:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.superview convertRect:keyboardRect fromView:nil];
    keyboardShowRect = keyboardRect;
    
    if (IS_PhoneXAll){
        self.orgin_y = keyboardRect.origin.y - self.size_height + LCFloat(20);
    } else {
        self.orgin_y = keyboardRect.origin.y - self.size_height;
    }
}

- (void)keyBoardDidHide:(NSNotification *)notification {
    self.frame = CGRectMake(0, originalRect.origin.y + originalRect.size.height - self.size_height, kScreenBounds.size.width, self.size_height);
}


@end
