//
//  LiveForumRootSingleSubTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LiveForumRootSingleSubTableViewCell.h"

@interface LiveForumRootSingleSubTableViewCell()
@property (nonatomic,strong)PDImageView *bgView;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *titleLabel;
@end

@implementation LiveForumRootSingleSubTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = RGB(255, 255, 255, 1);
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[PDImageView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgView];
    
    self.nickNameLabel = [GWViewTool createLabelFont:@"12" textColor:@"5A7FAC"];
    self.nickNameLabel.font = [self.nickNameLabel.font boldFont];
    [self.bgView addSubview:self.nickNameLabel];
    
    self.titleLabel = [GWViewTool createLabelFont:@"12" textColor:@"474747"];
    self.titleLabel.numberOfLines = 0;
    [self.bgView addSubview:self.titleLabel];
}

-(void)setTransferCount:(NSInteger)transferCount{
    _transferCount = transferCount;
}

-(void)setTransferSubType:(LiveForumRootSingleSubTableViewCellType)transferSubType{
    _transferSubType = transferSubType;
}

-(void)setTransferInfoManager:(LiveFourumRootListSingleModel *)transferInfoManager{
    _transferInfoManager = transferInfoManager;
    
    self.bgView.frame = CGRectMake(LCFloat(67), 0, kScreenBounds.size.width - LCFloat(67) - LCFloat(16), [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:transferInfoManager type:self.transferSubType]);
    
    self.nickNameLabel.text = [NSString stringWithFormat:@"%@ : ",transferInfoManager.comment_name];
    // 如果是最后的【共多少条回复】

    if ([transferInfoManager.reply_comment_name isEqualToString:@"共同NNNNN条回复"]){
        self.nickNameLabel.text = [NSString stringWithFormat:@"共%li条回复 >",(long)self.transferCount];
    }
    
    CGSize nickSize = [Tool makeSizeWithLabel:self.nickNameLabel];
    self.nickNameLabel.frame = CGRectMake(LCFloat(15), LCFloat(7), nickSize.width, nickSize.height);
    
    // dymic
    CGFloat width = kScreenBounds.size.width - LCFloat(67) - LCFloat(16);
    CGFloat dymicWidth = width - 2 * LCFloat(15) - nickSize.width;
    self.titleLabel.text = transferInfoManager.content;
    CGSize dymicSize = [transferInfoManager.content sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(dymicWidth, CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.nickNameLabel.frame), self.nickNameLabel.orgin_y, dymicWidth, dymicSize.height);
    
    if (self.transferSubType == LiveForumRootSingleSubTableViewCellTypeTop){
        self.bgView.image = [Tool stretchImageWithName:@"icon_live_sub_top"];
        self.nickNameLabel.orgin_y = LCFloat(15);
        self.titleLabel.orgin_y = LCFloat(15);
    } else if (self.transferSubType == LiveForumRootSingleSubTableViewCellTypeNormal){
        self.bgView.image = [Tool stretchImageWithName:@"icon_live_sub_normal"];
        self.nickNameLabel.orgin_y = LCFloat(5.5);
        self.titleLabel.orgin_y = LCFloat(5.5);
    } else if (self.transferSubType == LiveForumRootSingleSubTableViewCellTypeBottom){
        self.bgView.image = [Tool stretchImageWithName:@"icon_live_sub_bottom"];
        self.nickNameLabel.orgin_y = LCFloat(5.5);
        self.titleLabel.orgin_y = LCFloat(5.5);
    } else if (self.transferSubType == LiveForumRootSingleSubTableViewCellTypeSingle){
        self.bgView.image = [Tool stretchImageWithName:@"icon_live_sub_single"];
        self.nickNameLabel.orgin_y = LCFloat(15);
        self.titleLabel.orgin_y = LCFloat(15);
    }
}

+(CGFloat)calculationCellHeightWithModel:(LiveFourumRootListSingleModel *)model type:(LiveForumRootSingleSubTableViewCellType)type{
    CGFloat cellHeight = 0;
    CGFloat mainWidth = kScreenBounds.size.width - LCFloat(67) - LCFloat(16);
    NSString *nickName =  [NSString stringWithFormat:@"%@ : ",model.reply_comment_name];
    CGSize nickSize = [nickName sizeWithCalcFont:[[UIFont fontWithCustomerSizeName:@"12"]boldFont] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[[UIFont fontWithCustomerSizeName:@"12"]boldFont]])];
    CGFloat dymicWidth = mainWidth - 2 * LCFloat(15) - nickSize.width;
    
    CGSize dymicSize = [model.content sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"12"] constrainedToSize:CGSizeMake(dymicWidth, CGFLOAT_MAX)];
    
    CGFloat contentHeight = MAX(dymicSize.height, [NSString contentofHeightWithFont:[[UIFont fontWithCustomerSizeName:@"12"] boldFont]]) ;
    
    cellHeight += contentHeight;
    if (type == LiveForumRootSingleSubTableViewCellTypeTop){
        cellHeight += LCFloat(15) + LCFloat(5.5);
    } else if (type == LiveForumRootSingleSubTableViewCellTypeNormal){
        cellHeight += LCFloat(11);
    } else if (type == LiveForumRootSingleSubTableViewCellTypeBottom){
        cellHeight += LCFloat(15) + LCFloat(5.5);
    } else if (type == LiveForumRootSingleSubTableViewCellTypeSingle){
        cellHeight += 2 * LCFloat(15);
    }
    
    return cellHeight;
}

@end
