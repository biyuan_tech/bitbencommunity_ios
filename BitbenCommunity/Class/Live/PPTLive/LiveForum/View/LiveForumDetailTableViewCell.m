//
//  LiveForumDetailTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LiveForumDetailTableViewCell.h"
#import "NSAttributedString_Encapsulation.h"

@interface LiveForumDetailTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)PDImageView *convertView;
@property (nonatomic,strong)UIButton *moreButton;
/** 承载成就图标 */
@property (nonatomic ,strong) UIView *achievementView;
@end

static char actionClickHeaderImgWithBlockKey;
@implementation LiveForumDetailTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.avatarImgView addTapGestureRecognizer:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickHeaderImgWithBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.avatarImgView];
    
    self.convertView = [[PDImageView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.image = [UIImage imageNamed:@"icon_live_avatar_convert"];
    self.convertView.frame = CGRectMake(0, 0, LCFloat(67), LCFloat(67));
    [self addSubview:self.convertView];
    
    self.nickNameLabel = [GWViewTool createLabelFont:@"15" textColor:@"4C4C4C"];
    [self addSubview:self.nickNameLabel];
    
    // 承载成就view
    self.achievementView = [[UIView alloc] init];
    [self.contentView addSubview:self.achievementView];
    
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"7B7B7B"];
    [self addSubview:self.timeLabel];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"13" textColor:@"292929"];
    self.dymicLabel.numberOfLines = 0;
    [self addSubview:self.dymicLabel];
    
    self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.moreButton.backgroundColor = [UIColor clearColor];
    [self.moreButton setImage:[UIImage imageNamed:@"icon_live_more"] forState:UIControlStateNormal];
    [self addSubview:self.moreButton];
}

-(void)setTransferInfoManager:(LiveFourumRootListSingleModel *)transferInfoManager{
    _transferInfoManager = transferInfoManager;
    
    self.avatarImgView.style = transferInfoManager.cert_badge;
    self.avatarImgView.frame = CGRectMake(LCFloat(17), LCFloat(8), LCFloat(37), LCFloat(37));
    [self.avatarImgView uploadImageWithURL:transferInfoManager.comment_pic placeholder:nil callback:NULL];
    
    self.convertView.center = self.avatarImgView.center;
    
    // title
    self.nickNameLabel.text = transferInfoManager.comment_name;
    CGSize nickNameSize = [Tool makeSizeWithLabel:self.nickNameLabel];
    CGFloat originX = CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(13);
    CGFloat maxWidth = kCommonScreenWidth - originX - 68 - LCFloat(15) - LCFloat(30);
    CGFloat nickWidth = nickNameSize.width > maxWidth ? maxWidth : nickNameSize.width;
    self.nickNameLabel.frame = CGRectMake(originX, self.avatarImgView.orgin_y + LCFloat(11), nickWidth, nickNameSize.height);
    
    // 添加成就
    self.achievementView.frame = CGRectMake(CGRectGetMaxX(_nickNameLabel.frame) + 5,
                                            0, 58, 16);
    self.achievementView.center = CGPointMake(self.achievementView.center_x, self.nickNameLabel.center_y);
    
    for (UIView *subView in self.achievementView.subviews) {
        [subView removeFromSuperview];
    }
    
    NSArray *achievementData = transferInfoManager.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [self.achievementView addSubview:imageView];
    }
    
    // time
    self.timeLabel.text = [NSDate getTimeGap:transferInfoManager.create_time];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(self.nickNameLabel.orgin_x, CGRectGetMaxY(self.nickNameLabel.frame) + LCFloat(6), timeSize.width, timeSize.height);
    
    // more
    self.moreButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(30), 0, LCFloat(30), LCFloat(30));
    self.moreButton.center_y = self.nickNameLabel.center_y;
    
    NSString *replayInfo = [NSString stringWithFormat:@"回复%@ : %@",transferInfoManager.reply_comment_name,transferInfoManager.content];;
    self.dymicLabel.attributedText = [NSAttributedString_Encapsulation changeTextColorWithColor:[UIColor colorWithCustomerName:@"5A7FAC"] string:replayInfo andSubString:@[transferInfoManager.reply_comment_name]];
    
    CGFloat width = kScreenBounds.size.width - LCFloat(67) - LCFloat(16);
    CGSize dymicSize = [transferInfoManager.content sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.dymicLabel.frame = CGRectMake(LCFloat(67), CGRectGetMaxY(self.timeLabel.frame) + LCFloat(18), width, dymicSize.height);
}

+(CGFloat)calculationCellHeightWithModel:(LiveFourumRootListSingleModel *)model{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(21);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"15"]];
    cellHeight += LCFloat(6);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    cellHeight += LCFloat(18);
    CGFloat width = kScreenBounds.size.width - LCFloat(67) - LCFloat(15);
    NSString *newString = [NSString stringWithFormat:@"回复%@ : %@",model.reply_comment_name,model.content];
    CGSize contentOfSize = [newString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"13"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += contentOfSize.height;
    cellHeight += LCFloat(15);
    return cellHeight;
}

- (void)actionClickHeaderImgWithBlock:(void (^)(void))block{
    objc_setAssociatedObject(self, &actionClickHeaderImgWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
