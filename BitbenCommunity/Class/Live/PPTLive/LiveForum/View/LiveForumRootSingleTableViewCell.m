//
//  LiveForumRootSingleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LiveForumRootSingleTableViewCell.h"
#import "NSAttributedString_Encapsulation.h"

static char actionClickMoreBlockKey;
static char actionClickHeaderImgWithBlockKey;
static char actionClickZanManagerBlockKey;
static char actionClickReplaceManagerBlockKey;
@interface LiveForumRootSingleTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;                 /**< 头像*/
@property (nonatomic,strong)PDImageView *convertView;                   /**< 头像mask*/
@property (nonatomic,strong)UILabel *nickNameLabel;                     /**< 昵称*/
@property (nonatomic,strong)UIButton *zanButton;                        /**< 攒按钮*/
@property (nonatomic,strong)UILabel *dymicLabel;                        /**< 内容label*/
@property (nonatomic,strong)UILabel *timeLabel;                         /**< 时间label*/
@property (nonatomic,strong)UIButton *replaceButton;                    /**< 回复按钮*/
@property (nonatomic,strong)PDImageView *bpImgView;                     /**< bp图片*/
@property (nonatomic,strong)UILabel *bpLabel;                           /**< bpLabel*/
/** 承载成就图标 */
@property (nonatomic ,strong) UIView *achievementView;

@end

@implementation LiveForumRootSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.avatarImgView addTapGestureRecognizer:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickHeaderImgWithBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.avatarImgView];
    
    // 2. 头像遮罩
    self.convertView = [[PDImageView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.image = [UIImage imageNamed:@"icon_live_avatar_convert"];
    self.convertView.frame = CGRectMake(0, 0, LCFloat(67), LCFloat(67));
    [self addSubview:self.convertView];
    
    // 3. 昵称
    self.nickNameLabel = [GWViewTool createLabelFont:@"15" textColor:@"323232"];
    self.nickNameLabel.font = [self.nickNameLabel.font boldFont];
    [self addSubview:self.nickNameLabel];
    
    // 承载成就view
    self.achievementView = [[UIView alloc] init];
    [self.contentView addSubview:self.achievementView];
    
    // 4. 攒按钮
    self.zanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.zanButton setImage:[UIImage imageNamed:@"icon_article_detail_bottom_zan_nor"] forState:UIControlStateNormal];
    [self.zanButton setTitle:@"赞" forState:UIControlStateNormal];
    self.zanButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"12"];
    [self.zanButton setTitleColor:[UIColor hexChangeFloat:@"323232"] forState:UIControlStateNormal];
    [self.zanButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleLeft imageTitleSpace:LCFloat(3)];
    [self.zanButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf actionClickManagerWithInterface];
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickZanManagerBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.zanButton];

    // 5. 评论内容
    self.dymicLabel = [GWViewTool createLabelFont:@"15" textColor:@"323232"];
    self.dymicLabel.numberOfLines = 0;
    [self addSubview:self.dymicLabel];
    
    // 6. 时间
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"A2A2A2"];
    [self addSubview:self.timeLabel];
    
    // 7. 回复按钮
    self.replaceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.replaceButton setTitle:@"回复" forState:UIControlStateNormal];
    self.replaceButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"12"];
    [self.replaceButton setTitleColor:[UIColor hexChangeFloat:@"323232"] forState:UIControlStateNormal];
    [self.replaceButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickReplaceManagerBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.replaceButton];

    // 币图片
    self.bpImgView = [[PDImageView alloc]init];
    self.bpImgView.backgroundColor = [UIColor clearColor];
    self.bpImgView.image = [UIImage imageNamed:@"icon_article_detail_items_bi"];
    [self addSubview:self.bpImgView];

    // 币名称
    self.bpLabel = [GWViewTool createLabelFont:@"12" textColor:@"323232"];
    [self addSubview:self.bpLabel];
}

-(void)setHasHuifu:(BOOL)hasHuifu{
    _hasHuifu = hasHuifu;
}

-(void)setHasArticleDetail:(BOOL)hasArticleDetail{
    _hasArticleDetail = hasArticleDetail;
}

-(void)setTransferInfoManager:(LiveFourumRootListSingleModel *)transferInfoManager{
    _transferInfoManager = transferInfoManager;
    
    self.avatarImgView.style = transferInfoManager.cert_badge;
    self.avatarImgView.frame = CGRectMake(LCFloat(20), LCFloat(20), LCFloat(40), LCFloat(40));
    [self.avatarImgView uploadImageWithURL:transferInfoManager.comment_pic placeholder:nil callback:NULL];
    
    self.convertView.center = self.avatarImgView.center;
    
    // title
    self.nickNameLabel.text = transferInfoManager.comment_name;
    CGSize nickNameSize = [Tool makeSizeWithLabel:self.nickNameLabel];
    CGFloat originX = CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(13);
    CGFloat maxWidth = kCommonScreenWidth - originX - 68 - LCFloat(40) - LCFloat(20);
    CGFloat nickWidth = nickNameSize.width > maxWidth ? maxWidth : nickNameSize.width;
    self.nickNameLabel.frame = CGRectMake(originX, LCFloat(20), nickWidth, nickNameSize.height);
    
    // 赞按钮
    self.zanButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(40) - LCFloat(20), 0, LCFloat(40), LCFloat(40));
    self.zanButton.center_y = self.nickNameLabel.center_y;
    if (self.transferInfoManager.isSupport){
        [self.zanButton setTitle:[NSString stringWithFormat:@"%li",self.transferInfoManager.count_support] forState:UIControlStateNormal];
        [self.zanButton setImage:[UIImage imageNamed:@"icon_article_detail_tags_zan_hlt"] forState:UIControlStateNormal];
    } else {
        if (self.transferInfoManager.count_support <= 0){
            [self.zanButton setTitle:@"赞" forState:UIControlStateNormal];
        } else {
            [self.zanButton setTitle:[NSString stringWithFormat:@"%li",self.transferInfoManager.count_support] forState:UIControlStateNormal];
        }
        [self.zanButton setImage:[UIImage imageNamed:@"icon_article_detail_bottom_zan_nor"] forState:UIControlStateNormal];
    }
    
    // 添加成就
    
    self.achievementView.frame = CGRectMake(CGRectGetMaxX(_nickNameLabel.frame) + 5,
                                            0, 58, 16);
    self.achievementView.center = CGPointMake(self.achievementView.center_x, self.nickNameLabel.center_y);
    
    for (UIView *subView in self.achievementView.subviews) {
        [subView removeFromSuperview];
    }
    
    NSArray *achievementData = transferInfoManager.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [self.achievementView addSubview:imageView];
    }

    
    // 详情
    NSString *replayInfo = @"";
    if (self.hasArticleDetail){
        replayInfo = [NSString stringWithFormat:@"%@",transferInfoManager.content];;
        self.dymicLabel.text = replayInfo;
    } else {
        replayInfo = [NSString stringWithFormat:@"回复%@ : %@",transferInfoManager.reply_comment_name,transferInfoManager.content];;
        self.dymicLabel.attributedText = [NSAttributedString_Encapsulation changeTextColorWithColor:[UIColor colorWithCustomerName:@"5A7FAC"] string:replayInfo andSubString:@[transferInfoManager.reply_comment_name]];
    }
    

    
    CGFloat width = kScreenBounds.size.width - LCFloat(72) - LCFloat(21);
    CGSize dymicSize = [replayInfo sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.dymicLabel.frame = CGRectMake(LCFloat(72), CGRectGetMaxY(self.nickNameLabel.frame) + LCFloat(11), width, dymicSize.height);

    // time
    self.timeLabel.text = [NSString stringWithFormat:@"%@ · ",[NSDate getTimeGap:transferInfoManager.create_time / 1000.]];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(self.nickNameLabel.orgin_x, CGRectGetMaxY(self.dymicLabel.frame) + LCFloat(13), timeSize.width, timeSize.height);
    
    // 回复按钮
    NSString *replayStr = @"";
    if (self.hasHuifu){
        replayStr = @"回复";
        [self.replaceButton setBackgroundImage:nil forState:UIControlStateNormal];
    } else {
        if (transferInfoManager.count_reply != 0){
            replayStr = [NSString stringWithFormat:@"%li回复",(long)transferInfoManager.count_reply];
            [self.replaceButton setBackgroundImage:[Tool stretchImageWithName:@"icon_article_replay_btn_bg"] forState:UIControlStateNormal];
        } else {
            replayStr = @"回复";
            [self.replaceButton setBackgroundImage:nil forState:UIControlStateNormal];
        }
    }
    
    
    CGSize replaceSize = [replayStr sizeWithCalcFont:self.replaceButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.replaceButton.titleLabel.font])];
    self.replaceButton.frame = CGRectMake(CGRectGetMaxX(self.timeLabel.frame), 0, replaceSize.width + 2 * LCFloat(6), replaceSize.height + 2 * LCFloat(3));
    [self.replaceButton setTitle:replayStr forState:UIControlStateNormal];
    self.replaceButton.center_y = self.timeLabel.center_y;
    
    // 币按钮
    self.bpImgView.frame = CGRectMake(CGRectGetMaxX(self.replaceButton.frame) + LCFloat(30), 0, LCFloat(16), LCFloat(16));
    self.bpImgView.center_y = self.replaceButton.center_y;
    
    self.bpLabel.frame = CGRectMake(CGRectGetMaxX(self.bpImgView.frame) + LCFloat(9), 0, 100, [NSString contentofHeightWithFont:self.bpLabel.font]);
    self.bpLabel.text = transferInfoManager.reward;
    self.bpLabel.center_y = self.bpImgView.center_y;
}

+(CGFloat)calculationCellHeightWithModel:(LiveFourumRootListSingleModel *)model{
    CGFloat cellHeight = 0;
    // 1. 头像
    cellHeight += LCFloat(26);
    cellHeight += [NSString contentofHeightWithFont:[[UIFont fontWithCustomerSizeName:@"15"] boldFont]];
    cellHeight += LCFloat(11);
    
    CGFloat width = kScreenBounds.size.width - LCFloat(73) - LCFloat(20);
    NSString *replayInfo = [NSString stringWithFormat:@"回复%@ : %@",model.reply_comment_name,model.content];;
    CGSize contentOfSize = [replayInfo sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"15"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += contentOfSize.height;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    cellHeight += LCFloat(11);
    return cellHeight;
}

+(CGFloat)calculationCellHeightWithModelWithHuifu:(BOOL)hasHuifu model:(LiveFourumRootListSingleModel *)model{
    CGFloat cellHeight = 0;
    // 1. 头像
    cellHeight += LCFloat(26);
    cellHeight += [NSString contentofHeightWithFont:[[UIFont fontWithCustomerSizeName:@"15"] boldFont]];
    cellHeight += LCFloat(11);
    
    CGFloat width = kScreenBounds.size.width - LCFloat(73) - LCFloat(20);
    NSString *replayInfo = @"";
    if (hasHuifu){
        replayInfo = [NSString stringWithFormat:@"%@",model.content];;
    } else {
        replayInfo = [NSString stringWithFormat:@"回复%@ : %@",model.reply_comment_name,model.content];;
    }
    
    CGSize contentOfSize = [replayInfo sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"15"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += contentOfSize.height;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    cellHeight += LCFloat(11);
    return cellHeight;
}



-(void)actionClickMoreBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickMoreBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)actionClickHeaderImgWithBlock:(void (^)(void))block{
    objc_setAssociatedObject(self, &actionClickHeaderImgWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickZanManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickZanManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickReplaceManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickReplaceManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


#pragma mark - Interface
-(void)actionClickManagerWithInterface{
    __weak typeof(self)weakSelf = self;
    BOOL hasUp = NO;
    if (!self.transferInfoManager.isSupport){
        hasUp = YES;
    } else {
        hasUp = NO;
    }
    LiveForumDetailViewControllerType type = self.isLive ? LiveForumDetailViewControllerTypeNormal : LiveForumDetailViewControllerTypeArticle;
    
    [[NetworkAdapter sharedAdapter] liveComment_OperateWithCommentID:self.transferInfoManager._id itemId:self.transferItemId theme_type:type hasUp:YES block:^(BOOL isSuccessed, BOOL isOperate) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (!isOperate){
                strongSelf.transferInfoManager.isSupport = YES;
                self.transferInfoManager.count_support +=1;
                [strongSelf.zanButton setTitle:[NSString stringWithFormat:@"%li",strongSelf.transferInfoManager.count_support] forState:UIControlStateNormal];
                [strongSelf.zanButton setImage:[UIImage imageNamed:@"icon_article_detail_tags_zan_hlt"] forState:UIControlStateNormal];
            } else {
//                if (strongSelf.transferInfoManager.count_support - 1 <= 0){
//                    [strongSelf.zanButton setTitle:@"赞" forState:UIControlStateNormal];
//                } else {
//                    [strongSelf.zanButton setTitle:[NSString stringWithFormat:@"%li",strongSelf.transferInfoManager.count_support - 1] forState:UIControlStateNormal];
//                }
//                [strongSelf.zanButton setImage:[UIImage imageNamed:@"icon_article_detail_bottom_zan_nor"] forState:UIControlStateNormal];
            }
        }
    }];
}



#pragma mark - 直播时隐藏中间部分内容
-(void)setLiveHasHidden:(BOOL)liveHasHidden{
    _liveHasHidden = liveHasHidden;
    if(liveHasHidden){
        self.replaceButton.hidden = YES;
        self.bpImgView.hidden = YES;
        self.bpLabel.hidden = YES;
        self.timeLabel.text = [NSString stringWithFormat:@"%@",[NSDate getTimeGap:self.transferInfoManager.create_time / 1000.]];
    }
}

@end
