//
//  LiveForumDetailTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "LiveFourumRootListModel.h"
#import "LiveForumRootDingView.h"


@interface LiveForumDetailTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)LiveFourumRootListSingleModel *transferInfoManager;

+(CGFloat)calculationCellHeightWithModel:(LiveFourumRootListSingleModel *)model;
-(void)actionClickHeaderImgWithBlock:(void(^)(void))block;


@end
