//
//  LiveForumRootSingleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "LiveFourumRootListModel.h"
#import "LiveForumRootDingView.h"


@interface LiveForumRootSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)LiveFourumRootListSingleModel *transferInfoManager;
@property (nonatomic,copy)NSString *transferItemId;
@property (nonatomic,assign)BOOL hasHuifu;
@property (nonatomic,assign)BOOL hasArticleDetail;

// 直播的时候隐藏中间内容
@property (nonatomic,assign)BOOL liveHasHidden;
/** 是直播评论点赞 */
@property (nonatomic ,assign) BOOL isLive;


-(void)createView;

+(CGFloat)calculationCellHeightWithModel:(LiveFourumRootListSingleModel *)model;


-(void)actionClickZanManagerBlock:(void(^)())block;
-(void)actionClickMoreBlock:(void(^)())block;
-(void)actionClickHeaderImgWithBlock:(void(^)(void))block;
-(void)actionClickReplaceManagerBlock:(void(^)())block;

@end
