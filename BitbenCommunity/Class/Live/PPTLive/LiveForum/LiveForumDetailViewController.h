//
//  LiveForumDetailViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "LiveFourumRootListModel.h"
#import "LiveForumRootViewController.h"
#import "NetworkAdapter+PPTLive.h"

@class LiveForumRootViewController;
@interface LiveForumDetailViewController : AbstractViewController

@property (nonatomic,strong)LiveFourumRootListSingleModel *transferFourumRootModel;
@property (nonatomic,assign)LiveForumDetailViewControllerType transferPageType;
@property (nonatomic,copy)NSString *transferRoomId;
@end
