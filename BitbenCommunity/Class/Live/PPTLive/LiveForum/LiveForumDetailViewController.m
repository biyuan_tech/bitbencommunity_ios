//
//  LiveForumDetailViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "LiveForumDetailViewController.h"
#import "LiveForumRootSingleTableViewCell.h"
#import "LiveForumItemsTableViewCell.h"
#import "LiveForumDetailTableViewCell.h"
#import "NetworkAdapter+PPTLive.h"
#import "LiveForumRootInputView.h"
#import "BYPersonHomeController.h"

@interface LiveForumDetailViewController ()<UITableViewDelegate,UITableViewDataSource>{
    CGRect newKeyboardRect;
}
@property (nonatomic,strong)LiveForumRootSingleTableViewCell *topView1;
@property (nonatomic,strong)LiveForumItemsTableViewCell *topView2;
@property (nonatomic,strong)UITableView *dianpingDetailTableView;
@property (nonatomic,strong)NSMutableArray *dianpingMutableArr;
@property (nonatomic,strong)LiveForumRootInputView *inputView;
@end

@implementation LiveForumDetailViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTopView];
    [self createTableView];
    [self createInputView];
    [self sendRequestToGetInfoWithReReload:NO];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"点评详情";
    self.view.backgroundColor = [UIColor hexChangeFloat:@"F4F4F4"];
}

#pragma mark - 创建顶部View
-(void)createTopView{
    self.topView1 = [[LiveForumRootSingleTableViewCell alloc]init];
    self.topView1.backgroundColor = [UIColor whiteColor];
    [self.topView1 createView];
    self.topView1.frame = CGRectMake(0, 0, kScreenBounds.size.width, [LiveForumRootSingleTableViewCell calculationCellHeightWithModel:self.transferFourumRootModel]);
    self.topView1.transferInfoManager = self.transferFourumRootModel;
    __weak typeof(self)weakSelf = self;
    [self.topView1 actionClickHeaderImgWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
        personHomeController.user_id = strongSelf.transferFourumRootModel.user_id;
        [strongSelf.navigationController pushViewController:personHomeController animated:YES];
    }];
    [self.view addSubview:self.topView1];
    
    self.topView2 = [[LiveForumItemsTableViewCell alloc]init];
    self.topView2.backgroundColor = [UIColor whiteColor];
    [self.topView2 createView];
    self.topView2.frame = CGRectMake(0, CGRectGetMaxY(self.topView1.frame), kScreenBounds.size.width, [LiveForumItemsTableViewCell calculationCellHeight]);
    [self.view addSubview:self.topView2];
    self.topView2.transferItemId = self.transferRoomId;
    self.topView2.transferType = LiveForumDetailViewControllerTypeNormal;
    self.topView2.transferInfoManager = self.transferFourumRootModel;
    [self.topView2 itemsReplyManagerWithModel:^(LiveFourumRootListSingleModel *singleModle) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.inputView.transferListModel = singleModle;
        
        if (![strongSelf.inputView.inputView isFirstResponder]){
            [strongSelf.inputView.inputView becomeFirstResponder];
        }
        strongSelf.inputView.inputView.text = @"";

        strongSelf.inputView.inputView.placeholder = [NSString stringWithFormat:@"回复：%@",singleModle.comment_name];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.dianpingMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.dianpingDetailTableView){
        self.dianpingDetailTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.dianpingDetailTableView.dataSource = self;
        self.dianpingDetailTableView.orgin_y = CGRectGetMaxY(self.topView2.frame);
        self.dianpingDetailTableView.size_height = self.view.size_height - CGRectGetMaxY(self.topView2.frame) - [LiveForumRootInputView calculationHeight:@""];
        self.dianpingDetailTableView.delegate = self;
        [self.view addSubview:self.dianpingDetailTableView];
    }
    __weak typeof(self)weakSelf = self;

    [self.dianpingDetailTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithReReload:NO];
    }];
    [self.view addSubview:self.inputView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dianpingMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        LiveForumItemsTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[LiveForumItemsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
        cellWithRowTwo.transferItemId = self.transferRoomId;
        cellWithRowTwo.transferType = LiveForumDetailViewControllerTypeNormal;
        cellWithRowTwo.transferInfoManager = singleModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo itemsReplyManagerWithModel:^(LiveFourumRootListSingleModel *singleModle) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.inputView.transferListModel = singleModel;
            strongSelf.inputView.transferListModel.transferTempCommentId = strongSelf.transferFourumRootModel._id;
            
            if (![strongSelf.inputView.inputView isFirstResponder]){
                [strongSelf.inputView.inputView becomeFirstResponder];
            }
            strongSelf.inputView.inputView.text = @"";
            
            strongSelf.inputView.inputView.placeholder = [NSString stringWithFormat:@"回复：%@",singleModel.comment_name];
        }];
        
        return cellWithRowTwo;
    } else {
//        if (indexPath.section == 0){
//            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
//            LiveForumRootSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
//            if (!cellWithRowOne){
//                cellWithRowOne = [[LiveForumRootSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
//            }
//            LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
//            cellWithRowOne.transferInfoManager = singleModel;
//
//            return cellWithRowOne;
//        } else {
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            LiveForumDetailTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[LiveForumDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            }
            LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
            cellWithRowThr.transferInfoManager = singleModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr actionClickHeaderImgWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = cellWithRowThr.transferInfoManager.user_id;
            [strongSelf.navigationController pushViewController:personHomeController animated:YES];
        }];
            return cellWithRowThr;
        }
//    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1){
        return [LiveForumItemsTableViewCell calculationCellHeight];
    } else {
        if (indexPath.section == 0){
            LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
            return [LiveForumRootSingleTableViewCell calculationCellHeightWithModel:singleModel];
        } else {
            LiveFourumRootListSingleModel *singleModel = [self.dianpingMutableArr objectAtIndex:indexPath.section];
            return [LiveForumDetailTableViewCell calculationCellHeightWithModel:singleModel];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return LCFloat(13);
    } else {
        return .5f;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor hexChangeFloat:@"F4F4F4"];
    return headerView;
}

#pragma mark - 创建输入框
-(void)createInputView{
    if (!self.inputView){
        CGRect inputFrame = CGRectMake(0, self.view.size_height - [LiveForumRootInputView calculationHeight:@""] - [BYTabbarViewController sharedController].navBarHeight, kScreenBounds.size.width, [LiveForumRootInputView  calculationHeight:@""]);
        self.inputView = [[LiveForumRootInputView alloc]initWithFrame:inputFrame];
        self.inputView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        self.inputView.layer.shadowOpacity = .2f;
        self.inputView.layer.shadowOffset = CGSizeMake(.2f, .2f);
        
        [self.view addSubview:self.inputView];
    }
    __weak typeof(self)weakSelf = self;
    [self.inputView actionClickWithSendInfoBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToSendInfo];
    }];
    [self.inputView actionTextInputChangeBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.inputView.orgin_y = strongSelf.view.size_height - strongSelf->newKeyboardRect.size.height - [LiveForumRootInputView calculationHeight:strongSelf.inputView.inputView.text] ;
    }];
}


#pragma mark - Interface
-(void)sendRequestToGetInfoWithReReload:(BOOL)rereload{
    if (!self.inputView.transferListModel){
        self.inputView.transferListModel = self.transferFourumRootModel;
    }
    NSInteger pageNum = rereload ? 0 : self.dianpingDetailTableView.currentPage;
    
    NSDictionary *params = @{@"theme_id":self.transferRoomId,@"theme_type":@(self.transferPageType),@"reply_comment_id":self.inputView.transferListModel.comment_id,@"page_number":@(pageNum),@"page_size":@"10"};
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:page_comment requestParams:params responseObjectClass:[LiveFourumRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            LiveFourumRootListModel *singleModel = (LiveFourumRootListModel *)responseObject;
            if (strongSelf.dianpingDetailTableView.isXiaLa || rereload){
                [strongSelf.dianpingMutableArr removeAllObjects];
            }
            
            [strongSelf.dianpingMutableArr addObjectsFromArray:singleModel.content];
            
            [strongSelf.dianpingDetailTableView reloadData];

            if (strongSelf.dianpingDetailTableView.isXiaLa){
                [strongSelf.dianpingDetailTableView stopPullToRefresh];
            } else {
                [strongSelf.dianpingDetailTableView stopFinishScrollingRefresh];
            }
            
            
            if (strongSelf.dianpingMutableArr.count){
                [strongSelf.dianpingDetailTableView dismissPrompt];
            } else {
                [strongSelf.dianpingDetailTableView showPrompt:@"当前没有数据哦" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
        }
    }];
}

#pragma mark - 发送信息
-(void)sendRequestToSendInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] liveCreateCommentWiththemeId:self.transferRoomId type:self.transferPageType ThemeInfo:self.inputView.transferListModel content:self.inputView.inputView.text block:^(BOOL isSuccessed,NSString *commentId) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            strongSelf.inputView.transferListModel = strongSelf.transferFourumRootModel;
            [strongSelf sendRequestToGetInfoWithReReload:YES];
            [strongSelf.inputView cleanKeyboard];
        }
    }];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.inputView releaseKeyboard];
}

#pragma mark - 输入框释放
-(void)releaseKeyboard {
    [self.inputView cleanKeyboard];
}

@end
