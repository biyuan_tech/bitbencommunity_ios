//
//  LiveFourumRootListModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/17.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"
#import "ArticleDetailRootViewController.h"
@class ArticleDetailRootViewController;
@protocol LiveFourumRootListSingleModel <NSObject>
@end


@interface LiveFourumRootListSingleModel : FetchModel

@property (nonatomic,copy)NSString *_id;
@property (nonatomic,strong)NSArray<LiveFourumRootListSingleModel> *child;
@property (nonatomic,copy)NSString *comment_id;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *comment_pic;
@property (nonatomic,copy)NSString *comment_name;

@property (nonatomic,assign)NSInteger count_support;            /**< 顶的数量*/
@property (nonatomic,assign)NSInteger count_tread;              /**< 踩的数量*/
@property (nonatomic,assign)NSTimeInterval create_time;         /**< 创建时间*/
@property (nonatomic,copy)NSString *reply_comment_id;
@property (nonatomic,copy)NSString *reply_comment_name;
@property (nonatomic,copy)NSString *reply_comment_pic;
@property (nonatomic,copy)NSString *reply_user_id;
@property (nonatomic,copy)NSString *theme_id;
@property (nonatomic,copy)NSString *theme_type;
@property (nonatomic,copy)NSString *user_id;

@property (nonatomic,assign)NSInteger childCount;
@property (nonatomic,assign)BOOL isSupport;
@property (nonatomic,assign)BOOL isTread;
@property (nonatomic,copy)NSString *reward;
@property (nonatomic,assign)NSInteger count_reply;
@property (nonatomic,assign)BOOL isAttention;

//
@property (nonatomic,copy)NSString *transferTempCommentId;

@property (nonatomic,assign)ArticleDetailRootViewControllerType transferViewType;
/** vip认证 */
@property (nonatomic ,assign) NSInteger cert_badge;
/** 成就组 */
@property (nonatomic ,strong) NSArray <NSString *>*achievement_badge_list;
@end


@interface LiveFourumRootListModel : FetchModel

@property (nonatomic,strong)NSArray <LiveFourumRootListSingleModel> *content;

@end
