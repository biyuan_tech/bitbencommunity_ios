//
//  BYVideoPlayViewModel.h
//  BY
//
//  Created by 黄亮 on 2018/7/30.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYCommonViewModel.h"

@class BYLiveHomeVideoModel;
@interface BYVideoPlayViewModel : BYCommonViewModel

- (void)setPlayerModel:(BYLiveHomeVideoModel *)model;

@end
