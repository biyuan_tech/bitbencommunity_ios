//
//  BYVideoPlayViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/7/30.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYVideoPlayViewModel.h"
#import "BYLiveHomeVideoModel.h"
#import "BYVideoPlayModel.h"
#import "BYCommonPlayerView.h"
#import "BYAlertView.h"

@interface BYVideoPlayViewModel()<SuperPlayerDelegate>

// ** 播放器
@property (nonatomic ,strong) BYCommonPlayerView *playerView;

// ** pop返回
@property (nonatomic ,strong) UIButton *backBtn;

// ** 分享
@property (nonatomic ,strong) UIButton *shareBtn;

/** 播放器View的父视图*/
@property (nonatomic) UIView *playerFatherView;

/** 是否播放默认宣传视频 */
@property (nonatomic, assign) BOOL isPlayDefaultVideo;


@end

@implementation BYVideoPlayViewModel

- (void)dealloc
{
    [_playerView resetPlayer];
}

- (Class)getDataModelClass{
    return [BYVideoPlayModel class];
}

- (void)setPlayerModel:(BYLiveHomeVideoModel *)model{
//    if (!model.livehome_videoURL.length && !model.livehome_videoId.length) {
//        BYAlertView *alertView = [BYAlertView initWithTitle:nil message:@"视频链接错误" inView:[UIApplication sharedApplication].keyWindow alertViewStyle:BYAlertViewStyleAlert];
//        @weakify(self);
//        [alertView addActionCancleTitle:@"确定" handle:^{
//            @strongify(self);
//            [S_V_NC popViewControllerAnimated:YES];
//        }];
//        [alertView showAnimation];
//        return;
//    }
//    if (model.livehome_videoURL.length)
//        [_playerView playVideoWithUrl:model.livehome_videoURL title:model.livehome_title];
//    else
//        [_playerView playVideoWithVideoId:model.livehome_videoId title:model.livehome_title];
}

// 加载Ui
- (void)setContentView{

    self.isPlayDefaultVideo = YES;
    self.playerFatherView = [[UIView alloc] init];
    self.playerFatherView.backgroundColor = [UIColor blackColor];
    [S_V_VIEW addSubview:self.playerFatherView];
    [self.playerFatherView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(IS_PhoneXAll ? 44 : 0);
        make.leading.trailing.mas_equalTo(0);
        // 这里宽高比16：9,可自定义宽高比
        make.height.mas_equalTo(self.playerFatherView.mas_width).multipliedBy(9.0f/16.0f);
    }];
    self.playerView.fatherView = self.playerFatherView;
    
    // 布局ui
    [self configUI];
}

- (void)viewControllerPopAction{
//    if (![SuperPlayerWindow sharedInstance].isShowing) {
//        [self.playerView resetPlayer];
//    }
}

- (void)backBtnAction{
//    [self.playerView resetPlayer];  //非常重要
    [S_V_NC popViewControllerAnimated:YES];
}

- (void)shareBtnAction{
    
}

#pragma mark - SuperPlayerDelegate

- (void)superPlayerBackAction:(SuperPlayerView *)player{
    
    // player加到控制器上，只有一个player时候
    // 状态条的方向旋转的方向,来判断当前屏幕的方向
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    // 是竖屏时候响应关
    if (orientation == UIInterfaceOrientationPortrait &&
        (self.playerView.state == StatePlaying)) {
//        [SuperPlayerWindowShared setSuperPlayer:self.playerView];
//        [SuperPlayerWindowShared show];
//        SuperPlayerWindowShared.backController = self.viewController;
    } else {
        [self.playerView resetPlayer];  //非常重要
        SuperPlayerWindowShared.backController = nil;
    }
    [S_V_NC popViewControllerAnimated:YES];
}

#pragma mark - initMethod/setUI
- (void)configUI{
    
    [S_V_VIEW addSubview:self.backBtn];
    [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(59);
        make.height.mas_equalTo(59);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(IS_PhoneXAll ? 56 : 12);
    }];
    
    [S_V_VIEW addSubview:self.shareBtn];
    [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(59);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(IS_PhoneXAll ? 56 : 12);
    }];
}

- (BYCommonPlayerView *)playerView {
    if (!_playerView) {
        _playerView = [[BYCommonPlayerView alloc] init];
        _playerView.fatherView = _playerFatherView;
        _playerView.enableFloatWindow = NO;
        // 设置代理
        _playerView.delegate = self;
    }
    return _playerView;
}

- (UIButton *)backBtn{
    if (!_backBtn) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"videoPlay_item_back"] forState:UIControlStateNormal];
        [_backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backBtn;
}

- (UIButton *)shareBtn{
    if (!_shareBtn) {
        _shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_shareBtn setImage:[UIImage imageNamed:@"videoPlay_item_share"] forState:UIControlStateNormal];
        [_shareBtn addTarget:self action:@selector(shareBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareBtn;
}

@end
