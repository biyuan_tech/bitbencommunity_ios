//
//  BYVideoPlayController.m
//  BY
//
//  Created by 黄亮 on 2018/7/30.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYVideoPlayController.h"
#import "BYCommonPlayerView.h"
#import "BYVideoPlayViewModel.h"
#import "BYLiveHomeVideoModel.h"

@interface BYVideoPlayController ()

@end

@implementation BYVideoPlayController

- (Class)getViewModelClass{
    return [BYVideoPlayViewModel class];
}

- (instancetype)init
{
    if (SuperPlayerWindowShared.backController) {
        [SuperPlayerWindowShared hide];
        return (BYVideoPlayController *)SuperPlayerWindowShared.backController;
    } else {
        return [super init];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    [(BYVideoPlayViewModel *)self.viewModel setPlayerModel:_videoModel];
    // Do any additional setup after loading the view.
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
