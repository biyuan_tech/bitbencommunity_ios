//
//  BYLiveRoomHomeHeaderView.m
//  BY
//
//  Created by 黄亮 on 2018/9/12.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveRoomHomeHeaderView.h"
#import "BYCommonLiveModel.h"
#import "BYButton.h"


@interface BYLiveRoomHomeHeaderView()
{
    NSArray *_icons;
    NSArray *_signs;
}
/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogo;
/** 直播间标题 */
@property (nonatomic ,strong) UILabel *roomTitleLab;
/** 直播间封面图 */
@property (nonatomic ,strong) PDImageView *coverImageView;

@end

static NSInteger baseTag = 421;
@implementation BYLiveRoomHomeHeaderView

- (instancetype)init
{
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

#pragma mark - custom Mehtod
- (void)reloadData:(BYCommonLiveModel *)model{
    self.userlogo.style = model.cert_badge;
    [_userlogo uploadHDImageWithURL:model.head_img callback:nil];
    _roomTitleLab.text = model.room_title;
    [_coverImageView uploadImageWithURL:model.cover_url placeholder:nil callback:nil];
    
    for (int i = 0; i < _signs.count; i ++) {
        UILabel *numLab = [self viewWithTag:2*baseTag + i];
        numLab.text = i == 0 ? stringFormatInteger(model.fans) : (i == 1 ? model.yesterday_live_income : model.total_live_income);
        CGFloat numLabW = stringGetWidth(numLab.text, numLab.font.pointSize);
        [numLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(numLabW);
        }];
    }
}

#pragma mark - btn Action Method
// 直播间设置x
- (void)roomSetBtnAction{
    if (_didRoomSetBtnHandle) {
        _didRoomSetBtnHandle();
    }
    
    // 设置 - 编辑介绍页
    [MTAManager event:MTATypeLiveEdit params:nil];
}

- (void)newLiveBtnAction{
    if (_didNewLiveBtnHandle) {
        _didNewLiveBtnHandle();
    }
    
    // 进入直播
    [MTAManager event:MTATypeLiveEdit params:nil];
}

#pragma mark - configSubView Method

- (void)setContentView{
    
    _icons = @[@"liveroomhome_user_icon",@"liveroomhome_award_icon"];
    _signs = @[@"粉丝",@"昨日奖励",@"累计奖励"];
    
    // 头像
    PDImageView *userlogo = [[PDImageView alloc] init];
    userlogo.contentMode = UIViewContentModeScaleAspectFill;
    userlogo.layer.cornerRadius = 15.0f;
    userlogo.clipsToBounds = YES;
    [self addSubview:userlogo];
    _userlogo = userlogo;
    [userlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(30);
        make.left.mas_equalTo(17);
        make.top.mas_equalTo(11);
    }];
    
    // 头像背景
    UIImageView *userlogoBg = [[UIImageView alloc] init];
    [userlogoBg by_setImageName:@"common_userlogo_bg"];
    [self addSubview:userlogoBg];
    [self insertSubview:userlogoBg belowSubview:userlogo];
    [userlogoBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(userlogoBg.image.size.width);
        make.height.mas_equalTo(userlogoBg.image.size.height);
        make.centerX.equalTo(userlogo.mas_centerX).with.offset(0);
        make.centerY.equalTo(userlogo.mas_centerY).with.offset(0);
    }];
    
    // 直播间主题
    UILabel *roomTitleLab = [UILabel by_init];
    [roomTitleLab setBy_font:14];
    roomTitleLab.textColor = kTextColor_60;
    [self addSubview:roomTitleLab];
    _roomTitleLab = roomTitleLab;
    [roomTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userlogo.mas_right).with.offset(8);
        make.centerY.equalTo(userlogo.mas_centerY).with.offset(0);
        make.height.mas_equalTo(15);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    // 认证图标
    UIImageView *authIcon = [[UIImageView alloc] init];
    [authIcon by_setImageName:@"liveroomhome_auth_icon"];
    [self addSubview:authIcon];
    [authIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(authIcon.image.size.width);
        make.height.mas_equalTo(authIcon.image.size.height);
        make.centerY.equalTo(roomTitleLab.mas_centerY).with.offset(0);
        make.left.equalTo(roomTitleLab.mas_right).with.offset(10);
    }];
    
    // 直播间封面图
    PDImageView *coverImageView = [[PDImageView alloc] init];
    coverImageView.contentMode = UIViewContentModeScaleAspectFill;
    coverImageView.clipsToBounds = YES;
    [self addSubview:coverImageView];
    _coverImageView = coverImageView;
    [coverImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(54);
        make.height.mas_equalTo(195);
    }];
    
    for (int i = 0; i < _signs.count; i ++ ) {
        UIView *view = [self getSingleView:i];
        [self addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(coverImageView.mas_bottom).with.offset(0);
            make.width.mas_equalTo(kCommonScreenWidth/3);
            make.left.mas_equalTo(kCommonScreenWidth*i/3);
            make.height.mas_equalTo(60);
        }];
    }
    
    for (int i = 0; i < 2; i ++) {
        // 竖线
        UIView *verticalLineView = [UIView by_init];
        verticalLineView.backgroundColor = kBgColor_237;
        [self addSubview:verticalLineView];
        [verticalLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kCommonScreenWidth*(i + 1)/3);
            make.top.equalTo(coverImageView.mas_bottom).with.offset(14);
            make.height.mas_equalTo(31);
            make.width.mas_equalTo(0.5);
        }];
    }
    
    UIView *spaceView = [UIView by_init];
    [spaceView setBackgroundColor:kColorRGBValue(0xf5f5f5)];
    [self addSubview:spaceView];
    [spaceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth);
        make.centerX.mas_equalTo(0);
        make.top.equalTo(coverImageView.mas_bottom).with.offset(60);
        make.height.mas_equalTo(10);
    }];

    
    // 直播间设置
    UIButton *roomSetBtn = [UIButton by_buttonWithCustomType];
    [roomSetBtn setBy_attributedTitle:@{@"title":@"直播间设置",NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kTextColor_237} forState:UIControlStateNormal];
    [roomSetBtn addTarget:self action:@selector(roomSetBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [roomSetBtn layerCornerRadius:10 borderWidth:1 borderColor:kTextColor_237 size:CGSizeMake(148, 42)];
    [self addSubview:roomSetBtn];
    [roomSetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(148);
        make.height.mas_equalTo(42);
        make.top.equalTo(coverImageView.mas_bottom).with.offset(96);
        make.left.mas_equalTo((kCommonScreenWidth/2 - 148)/2);
    }];
    
    // 新建直播
    UIButton *newLiveBtn = [UIButton by_buttonWithCustomType];
    [newLiveBtn setBy_attributedTitle:@{@"title":@"新建直播",NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [newLiveBtn setBackgroundColor:kNaTitleColor_237];
    [newLiveBtn addTarget:self action:@selector(newLiveBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [newLiveBtn layerCornerRadius:10 size:CGSizeMake(148, 42)];
//    newLiveBtn.layer.shadowColor = kNaTitleColor_237.CGColor;
//    newLiveBtn.layer.shadowOpacity = 0.5f;
//    newLiveBtn.layer.shadowRadius = 14.0f;
//    newLiveBtn.layer.shadowOffset = CGSizeMake(13, 13);
    [self addSubview:newLiveBtn];
    [newLiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(148);
        make.height.mas_equalTo(42);
        make.top.equalTo(coverImageView.mas_bottom).with.offset(96);
        make.left.mas_equalTo((kCommonScreenWidth/2 - 148)/2 + kCommonScreenWidth/2);
    }];
}

- (UIView *)getSingleView:(NSInteger)index{
    
    UIView *subView = [UIView by_init];
    subView.backgroundColor = [UIColor whiteColor];
    
    // 标识
    UILabel *signLab = [UILabel by_init];
    signLab.text = _signs[index];
    signLab.textColor = kColorRGBValue(0xa6a6a6);
    [signLab setBy_font:11];
    [subView addSubview:signLab];
    [signLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(stringGetHeight(signLab.text, 11));
        make.width.mas_equalTo(stringGetWidth(signLab.text, signLab.font.pointSize));
        make.bottom.mas_equalTo(-15);
    }];
    
    // 数字
    UILabel *numLab = [UILabel by_init];
    numLab.text = @"00";
    [numLab setBy_font:18];
    [numLab setTextColor:kColorRGBValue(0x3b3b3b)];
    [subView addSubview:numLab];
    numLab.tag = 2*baseTag + index;
    [numLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(stringGetHeight(numLab.text, 18));
        make.width.mas_greaterThanOrEqualTo(5);
        make.bottom.mas_equalTo(signLab.mas_top).mas_offset(0);
    }];
    
    return subView;
}
@end
