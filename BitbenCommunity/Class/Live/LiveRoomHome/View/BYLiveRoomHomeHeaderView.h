//
//  BYLiveRoomHomeHeaderView.h
//  BY
//
//  Created by 黄亮 on 2018/9/12.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BYCommonLiveModel;
@interface BYLiveRoomHomeHeaderView : UIView

@property (nonatomic ,copy) void (^didRoomSetBtnHandle)(void);
@property (nonatomic ,copy) void (^didNewLiveBtnHandle)(void);

/** 刷新界面数据 */
- (void)reloadData:(BYCommonLiveModel *)model;

@end
