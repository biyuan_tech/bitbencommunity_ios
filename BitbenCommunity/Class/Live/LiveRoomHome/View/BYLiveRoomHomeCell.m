//
//  BYLiveRoomHomeCell.m
//  BY
//
//  Created by 黄亮 on 2018/9/12.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveRoomHomeCell.h"
#import "BYCommonLiveModel.h"

@interface BYLiveRoomHomeCell()

@property (nonatomic ,strong) PDImageView *coverImgView;
@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UILabel *timeLab;
@property (nonatomic ,strong) UIView *liveStatus;
@property (nonatomic ,strong) UILabel *liveStatusLab;
/** bp */
@property (nonatomic ,strong) UILabel *bpNumLab;
/** 状态 */
@property (nonatomic ,strong) UILabel *statusLab;


@end;

@implementation BYLiveRoomHomeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    self.indexPath = indexPath;
    BYCommonLiveModel *model = object[indexPath.row];
//    @weakify(self);
//    [_coverImgView uploadImageWithURL:model.live_cover_url placeholder:nil callback:^(UIImage *image) {
//        @strongify(self);
//        self.coverImgView.image = [image addCornerRadius:5 size:CGSizeMake(132, 97)];
//
//    }];
    [_coverImgView uploadHDImageWithURL:model.live_cover_url callback:nil];
    _titleLab.text = model.live_title;
    _timeLab.text = model.show_begin_time;
    _bpNumLab.text = model.reward;
    [self reloadLiveStatus:model.status liveType:model.live_type];
    
    [_bpNumLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(model.reward, 11));
    }];
}

- (void)moreAction{
    [self sendActionName:@"moreAction" param:nil indexPath:self.indexPath];
}

- (void)reloadLiveStatus:(BY_LIVE_STATUS)status liveType:(BY_NEWLIVE_TYPE)liveType{
    if (liveType == BY_NEWLIVE_TYPE_VOD_AUDIO ||
        liveType == BY_NEWLIVE_TYPE_VOD_VIDEO) {
        _liveStatus.backgroundColor = kColorRGBValue(0xed4a45);
        _liveStatusLab.text = liveType == BY_NEWLIVE_TYPE_VOD_AUDIO ? @"音频" : @"视频";
        return;
    }
    self.statusLab.hidden = NO;
    self.statusLab.textColor = kColorRGBValue(0xea6438);
    switch (status) {
        case BY_LIVE_STATUS_LIVING:
            _liveStatus.backgroundColor = kColorRGBValue(0xed4a45);
            _liveStatusLab.text = @"正在直播";
            _statusLab.textColor = kColorRGBValue(0x6384ff);
            _statusLab.text = @"审核已通过";
            break;
        case BY_LIVE_STATUS_SOON:
            _liveStatus.backgroundColor = kColorRGBValue(0x00a43e);
            _liveStatusLab.text = @"直播预告";
            _statusLab.textColor = kColorRGBValue(0x6384ff);
            _statusLab.text = @"审核已通过";
            break;
        case BY_LIVE_STATUS_END:
            _liveStatus.backgroundColor = kColorRGBValue(0xed9c45);
            _liveStatusLab.text = @"直播回放";
            _statusLab.textColor = kColorRGBValue(0x6384ff);
            _statusLab.text = @"审核已通过";
            break;
        case BY_LIVE_STATUS_OVERTIME:
            _liveStatus.backgroundColor = kColorRGBValue(0xed9c45);
            _liveStatusLab.text = @"直播回放";
            _statusLab.text = @"超时未播";
            break;
        case BY_LIVE_STATUS_REVIEW_WAITING:
            _liveStatus.backgroundColor = kColorRGBValue(0x00a43e);
            _liveStatusLab.text = @"直播预告";
            _statusLab.text = @"审核中";
            break;
        case BY_LIVE_STATUS_REVIEW_FAIL:
            _liveStatus.backgroundColor = kColorRGBValue(0xed9c45);
            _liveStatusLab.text = @"直播回放";
            _statusLab.text = @"审核失败";
            break;
        default:
            break;
    }
}

- (void)setContentView{
    // 封面图
    PDImageView *coverImgView = [[PDImageView alloc] init];
    coverImgView.layer.cornerRadius = 5.0f;
    coverImgView.clipsToBounds = YES;
    [self.contentView addSubview:coverImgView];
    _coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(132);
        make.height.mas_equalTo(97);
    }];
    
    UIView *maskView= [UIView by_init];
    [maskView setBackgroundColor:kColorRGB(47, 47, 47, 0.6)];
    [maskView layerCornerRadius:5 byRoundingCorners:UIRectCornerTopLeft size:CGSizeMake(60, 23)];
    [self.contentView addSubview:maskView];
    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(kCellLeftSpace);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(23);
    }];
    
    // 直播状态
    UIView *liveStatus = [UIView by_init];
    liveStatus.layer.cornerRadius = 2;
    [maskView addSubview:liveStatus];
    _liveStatus = liveStatus;
    [liveStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(4);
        make.left.mas_equalTo(5);
        make.centerY.mas_equalTo(0);
    }];
    
    // 直播状态文案
    UILabel *liveStatusLab = [UILabel by_init];
    liveStatusLab.text = @"直播回放";
    [liveStatusLab setBy_font:11];
    [liveStatusLab setTextColor:[UIColor whiteColor]];
    [maskView addSubview:liveStatusLab];
    _liveStatusLab = liveStatusLab;
    [liveStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(liveStatus.mas_right).with.offset(3);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(stringGetWidth(liveStatusLab.text, liveStatusLab.font.pointSize));
        make.height.mas_equalTo(stringGetHeight(liveStatusLab.text, liveStatusLab.font.pointSize));
    }];
    
    
    // 标题
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.font = [UIFont systemFontOfSize:14];
    titleLab.textColor = kColorRGBValue(0x2c2c2c);
    titleLab.numberOfLines = 2;
    [self.contentView addSubview:titleLab];
    _titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(coverImgView.mas_right).with.offset(10);
        make.top.mas_equalTo(2);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(14);
    }];
    
    // 时间图标
    UIImageView *timeImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"common_time_icon"]];
    [self.contentView addSubview:timeImgView];
    [timeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(coverImgView.mas_right).with.offset(10);
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(timeImgView.image.size.width);
        make.height.mas_equalTo(timeImgView.image.size.height);
    }];
    
    // 时间
    UILabel *timeLab = [[UILabel alloc] init];
    timeLab.font = [UIFont systemFontOfSize:12];
    timeLab.textColor = kTextColor_154;
    [self.contentView addSubview:timeLab];
    _timeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeImgView.mas_right).with.offset(5);
        make.centerY.equalTo(timeImgView.mas_centerY).with.offset(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(18);
    }];
    
    self.statusLab = [UILabel by_init];
    [self.statusLab setBy_font:13];
    self.statusLab.textColor = kColorRGBValue(0xea6438);
    self.statusLab.textAlignment = NSTextAlignmentRight;
    self.statusLab.hidden = YES;
    [self.contentView addSubview:self.statusLab];
    [self.statusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(self.statusLab.font.pointSize);
        make.centerY.mas_equalTo(timeLab);
        make.width.mas_equalTo(70);
    }];
    
    UIImageView *bpImgView = [[UIImageView alloc] init];
    [bpImgView by_setImageName:@"icon_article_bi"];
    [self.contentView addSubview:bpImgView];
    [bpImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(18);
        make.bottom.mas_equalTo(coverImgView.mas_bottom).mas_offset(0);
        make.left.mas_equalTo(coverImgView.mas_right).mas_offset(10);
    }];
    
    self.bpNumLab = [UILabel by_init];
    self.bpNumLab.text = @"0.00";
    [self.bpNumLab setBy_font:11];
    self.bpNumLab.textColor = kColorRGBValue(0x5b5b5b);
    [self.contentView addSubview:self.bpNumLab];
    @weakify(self);
    [self.bpNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(bpImgView.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(bpImgView.mas_centerY).mas_offset(0);
        make.width.mas_equalTo(stringGetWidth(self.bpNumLab.text, 11));
        make.height.mas_equalTo(stringGetHeight(self.bpNumLab.text, 11));
    }];
    
    // 进入按钮
    UIButton *enterBtn = [UIButton by_buttonWithCustomType];
    [enterBtn setBackgroundColor:[UIColor whiteColor]];
    [enterBtn setBy_imageName:@"icon_live_more" forState:UIControlStateNormal];
    [enterBtn addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    enterBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -(52 - enterBtn.imageView.image.size.width)/2);
    //    [enterBtn layerCornerRadius:4 size:CGSizeMake(52, 23)];
    [self.contentView addSubview:enterBtn];
    [enterBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(52);
        make.height.mas_equalTo(23);
        make.right.mas_equalTo(-kCellRightSpace);
        make.centerY.mas_equalTo(bpImgView.mas_centerY).mas_offset(0);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
