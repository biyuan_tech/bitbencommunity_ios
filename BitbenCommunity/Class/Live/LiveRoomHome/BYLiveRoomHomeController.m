//
//  BYLiveRoomHomeController.m
//  BY
//
//  Created by 黄亮 on 2018/9/12.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveRoomHomeController.h"
#import "BYLiveRoomHomeViewModel.h"

@interface BYLiveRoomHomeController ()

@end

@implementation BYLiveRoomHomeController

- (void)dealloc
{
    
}

- (Class)getViewModelClass{
    return [BYLiveRoomHomeViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.popGestureRecognizerEnale = NO;
    self.navigationTitle = @"直播间";
//    for (UIViewController *viewController in self.navigationController.viewControllers) {
//        // 移除直播创建界面
//        if ([NSStringFromClass([viewController class]) isEqualToString:@"BYCreatLiveRoomController"]) {
//            NSMutableArray *tmpViewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
//            [tmpViewControllers removeObject:viewController];
//            self.navigationController.viewControllers = tmpViewControllers;
//        }
//    }
}

//- (void)reloadData{
//    // 延迟0.2秒刷新数据，避免后端未更新数据
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [(BYLiveRoomHomeViewModel *)self.viewModel reloadData];
//    });
//}

@end
