//
//  BYLiveRoomHomeViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/12.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveRoomHomeViewModel.h"
// Controller
#import "BYNewLiveController.h"
#import "BYLiveRoomSetController.h"
#import "BYLiveHostController.h"
#import "BYLiveController.h"
#import "BYNewLiveControllerV1.h"
#import "BYLiveDetailController.h"
#import "BYVODPlayController.h"
// View
#import "BYLiveRoomHomeHeaderView.h"

#import "BYCommonModel.h"
#import "BYCommonLiveModel.h"
// Request
#import "BYIMCommon.h"

#import "BYSIMController.h"


@interface BYLiveRoomHomeViewModel()<BYCommonTableViewDelegate>

@property (nonatomic ,strong) BYCommonTableView *tableView;
@property (nonatomic ,strong) BYLiveRoomHomeHeaderView *headerView;
@property (nonatomic ,assign) NSInteger pageNum;
@property (nonatomic ,strong) BYCommonLiveModel *model;

@end

@implementation BYLiveRoomHomeViewModel

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setContentView{
    _pageNum = 0;
    [S_V_VIEW addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kSafeAreaInsetsBottom, 0));
    }];
    [self addNSNotification];
//    [self reloadData];
}

- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveStopPushStream_notic) name:NSNotification_stopPushStream object:nil];
}

- (void)reloadData{
    self.pageNum = 0;
    [self loadRequestGetLiveRoomInfo];
    [self loadRequestGetLiveList];
}

- (void)loadMoreData{
    [self loadRequestGetLiveList];
}

- (void)viewWillAppear{
    [self reloadData];
//    [self.tableView reloadData];
}

#pragma mark - notic
// 接收关闭直播消息刷新界面
- (void)receiveStopPushStream_notic{
    [self reloadData];
}

#pragma mark - buttonAction
// 新建直播
- (void)didNewLiveBtnAction{
//    BYNewLiveController *newLiveController = [[BYNewLiveController alloc] init];
    BYNewLiveControllerV1 *newLiveController = [[BYNewLiveControllerV1 alloc] init];
    [S_V_NC pushViewController:newLiveController animated:YES];
}
// 直播间设置
- (void)didRoomSetBtnAction{
    BYLiveRoomSetController *liveRoomSetController = [[BYLiveRoomSetController alloc] init];
    liveRoomSetController.model = self.model;
    @weakify(self);
    liveRoomSetController.reloadLiveRoomHandel = ^{
        @strongify(self);
        [self.headerView reloadData:self.model];
    };
    [S_V_NC pushViewController:liveRoomSetController animated:YES];
}

#pragma mark - request
- (void)loadRequestGetLiveRoomInfo{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLiveRoomInfoSuccessBlock:^(id object) {
        @strongify(self);
        self.model = object;
        [self.headerView reloadData:self.model];
    } faileBlock:^(NSError *error) {
        
    }];
}

- (void)loadRequestGetLiveList{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLivelist:[AccountModel sharedAccountModel].account_id pageNum:_pageNum successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        [self.tableView endRefreshing];
        NSMutableArray *tmpArr = [NSMutableArray array];
        if (self.pageNum == 0) {
            self.pageNum ++;
            BYCommonModel *model = [[BYCommonModel alloc] init];
            model.cellString = @"BYLiveRoomHomeTitleCell";
            model.cellHeight = 58;
            model.title = @"直播列表";
            [tmpArr addObject:model];
            [tmpArr addObjectsFromArray:object];
            self.tableView.tableData = tmpArr;
            return ;
        }
        self.pageNum ++;
        [tmpArr addObjectsFromArray:self.tableView.tableData];
        [tmpArr addObjectsFromArray:object];
        self.tableView.tableData = tmpArr;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}
#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) return;
    BYCommonLiveModel *model = tableView.tableData[indexPath.row];
    
    if (model.live_type == BY_NEWLIVE_TYPE_VOD_AUDIO ||
        model.live_type == BY_NEWLIVE_TYPE_VOD_VIDEO) {
        BYVODPlayController *vodPlayController = [[BYVODPlayController alloc] init];
        vodPlayController.live_record_id = nullToEmpty(model.live_record_id);
        [S_V_NC pushViewController:vodPlayController animated:YES];
        return;
    }
    
    NSString *title = @"";
    switch (model.live_type) {
        case BY_NEWLIVE_TYPE_VIDEO:
            title = @"推流直播";
            break;
        case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
        case BY_NEWLIVE_TYPE_AUDIO_PPT:
            title = @"微直播";
            break;
        default:
            title = @"个人互动直播";
            break;
    }
    
    BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
    liveDetailController.live_record_id = model.live_record_id;
//    liveDetailController.model = model;
    liveDetailController.isHost = YES;
    liveDetailController.navigationTitle = title;
    [S_V_NC pushViewController:liveDetailController animated:YES];

}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"moreAction"]) {
        BYCommonLiveModel *model = tableView.tableData[indexPath.row];
        @weakify(self);
        [BYCommonTool showDelSheetViewInCurrentView:model.live_record_id theme_type:BY_THEME_TYPE_LIVE cb:^{
            @strongify(self);
            NSMutableArray *tableData = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [tableData removeObject:model];
            self.tableView.tableData = tableData;
        }];
    }
}

#pragma mark - initMethod

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        _tableView.tableHeaderView = self.headerView;
        [_tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    }
    return _tableView;
}

- (BYLiveRoomHomeHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[BYLiveRoomHomeHeaderView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, 395)];
        @weakify(self);
        _headerView.didNewLiveBtnHandle = ^{
            @strongify(self);
            [self didNewLiveBtnAction];
        };
        _headerView.didRoomSetBtnHandle = ^{
            @strongify(self);
            [self didRoomSetBtnAction];
        };
    }
    return _headerView;
}



@end
