//
//  BYLiveDefine.h
//  BY
//
//  Created by 黄亮 on 2018/8/31.
//  Copyright © 2018年 BY. All rights reserved.
//

#ifndef BYLiveDefine_h
#define BYLiveDefine_h

typedef NS_ENUM(NSInteger, BY_LIVE_ORGAN_TYPE) {
    BY_LIVE_ORGAN_TYPE_PERSON = 0, // 个人
    BY_LIVE_ORGAN_TYPE_LECTURER, // 讲师
    BY_LIVE_ORGAN_TYPE_ORGAN // 机构
};

typedef NS_ENUM(NSInteger, BY_LIVE_TYPE) {
    BY_LIVE_TYPE_LIVE = 0, // 互动直播
    BY_LIVE_TYPE_PPT, // PPT幻灯片形式
    BY_LIVE_UPLOAD_VIDEO, // 音视频录播形式
    BY_LIVE_TYPE_VIDEO // 视频形式
};

typedef NS_ENUM(NSInteger, BY_LIVE_STATUS) {
    BY_LIVE_STATUS_NOTLIVE_ONTIME = -10, // 未准时开播
    BY_LIVE_STATUS_REVIEW_FAIL = -2,   // 审核失败
    BY_LIVE_STATUS_OVERTIME = -1, // 直播超时
//    BY_LIVE_STATUS_WAITING = 0, // 申请直播
    BY_LIVE_STATUS_REVIEW_WAITING = 0,   // 待审核
    BY_LIVE_STATUS_SOON, // 预约直播
    BY_LIVE_STATUS_LIVING, // 正在直播
    BY_LIVE_STATUS_END // 结束直播
};

// 新建直播时的选择直播形式
typedef NS_ENUM(NSInteger, BY_NEWLIVE_TYPE) {
    BY_NEWLIVE_TYPE_OTHER = -1, // (用于活动种类的 不同步线上活动)
    BY_NEWLIVE_TYPE_AUDIO_LECTURE = 0, // 音频讲座形式
    BY_NEWLIVE_TYPE_AUDIO_PPT = 1, // 音频PPT形式
    BY_NEWLIVE_TYPE_ILIVE = 2, // 个人互动直播形式
    BY_NEWLIVE_TYPE_VIDEO = 3, // 视频直播形式（OBS）
    BY_NEWLIVE_TYPE_RECORD_SCREEN = 4, // 录屏直播形式
    BY_NEWLIVE_TYPE_UPLOAD_VIDEO = 5, // 音视频录播形式
    BY_NEWLIVE_TYPE_VOD_VIDEO = 6 , // 视频点播(视频)
    BY_NEWLIVE_TYPE_VOD_AUDIO = 7 , // 视频点播(音频)
};

// 微直播类型
typedef NS_ENUM(NSInteger, BY_NEWLIVE_MICRO_TYPE) {
    BY_NEWLIVE_MICRO_TYPE_APP = 0, // app内直播
    BY_NEWLIVE_MICRO_TYPE_WX  // 微信群直播
};

// 身份类型
typedef NS_ENUM(NSInteger, BY_IDENTITY_TYPE) {
    BY_IDENTITY_TYPE_NORMAL = 0,    // 普通观众
    BY_IDENTITY_TYPE_CREATOR,       // 创建者
    BY_IDENTITY_TYPE_HOST,          // 主持人
    BY_IDENTITY_TYPE_GUEST,         // 嘉宾
};

// 首页列表分类
typedef NS_ENUM(NSInteger, BY_LIVE_HOME_LIST_TYPE) {
    BY_LIVE_HOME_LIST_TYPE_HOT = 0, // 热门推荐
    BY_LIVE_HOME_LIST_TYPE_AUDIO, // 音频类
    BY_LIVE_HOME_LIST_TYPE_ILIVE, // 视频类
    BY_LIVE_HOME_LIST_TYPE_FOLLOW // 我的关注
};

// 排序类型
typedef NS_ENUM(NSInteger, BY_SORT_TYPE) {
    BY_SORT_TYPE_HOT = 0, // 热度
    BY_SORT_TYPE_TIME, // 时间
    BY_SORT_TYPE_HOT_TIME // 热度与时间
};

// 用户操作类型
typedef NS_ENUM(NSInteger, BY_GUEST_OPERA_TYPE) {
    BY_GUEST_OPERA_TYPE_UP = 0, // 顶
    BY_GUEST_OPERA_TYPE_STAMP = 1, // 踩
    BY_GUEST_OPERA_TYPE_REWARD = 2, // 打赏
    BY_GUEST_OPERA_TYPE_RECEIVE_STREAM = 3, // 接收推流(开播)
    BY_GUEST_OPERA_TYPE_UP_VIDEO = 4, // 上麦
    BY_GUEST_OPERA_TYPE_AGREE_UP_VIDEO = 5, // 同意上麦
    BY_GUEST_OPERA_TYPE_DOWN_VIDEO = 6, // 下麦
    BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM = 7, // 停止推流（下播）
};

// 主题类型
typedef NS_ENUM(NSInteger, BY_THEME_TYPE){
    BY_THEME_TYPE_LIVE = 0, // 直播
    BY_THEME_TYPE_ARTICLE = 1,// 文章
    BY_THEME_TYPE_LIVE_COMMENT = 2, // 直播评论
    BY_THEME_TYPE_ARTICLE_COMMENT = 3,// 文章评论
    BY_THEME_TYPE_APP_H5 = 9, // 内部h5链接
    BY_THEME_TYPE_APP_LIVE_HOME = 11,/**< 直播失效首页*/
    
};

typedef NS_ENUM(NSInteger, BY_SEARCH_TYPE) {
    BY_SEARCH_TYPE_ALL = 0, // 推荐
    BY_SEARCH_TYPE_LIVE ,   // 直播
    BY_SEARCH_TYPE_VIDEO,   // 视频
    BY_SEARCH_TYPE_ARTICLE, // 观点
    BY_SEARCH_TYPE_USER,    // 用户
};

typedef NS_ENUM(NSInteger, BY_COURSE_TYPE) {
    BY_COURSE_TYPE_DF = 0,  // 默认
    BY_COURSE_TYPE_LEARN_MOST , // 最多学习
    BY_COURSE_TYPE_NEW   // 最新上架
};

// 首页文章查询类型
typedef NS_ENUM(NSInteger ,BY_HOME_ARTICLE_TYPE) {
    BY_HOME_ARTICLE_TYPE_NORMAL = -1,            // 普通状态
    BY_HOME_ARTICLE_TYPE_ANALYST = 0,           // 分析师
    BY_HOME_ARTICLE_TYPE_ORGAN_MEDIA,       // 机构-媒体
    BY_HOME_ARTICLE_TYPE_ORGAN_PROJECT,     // 机构-项目方
};

//// 活动类型
//typedef NS_ENUM(NSInteger ,BY_ACTIVITY_TYPE) {
//    BY_ACTIVITY_TYPE_NORMAL = -1, // 不同步线上活动
//    BY_ACTIVITY_TYPE_SYNC_ILIVE = 0,  // 同步互动直播
//    BY_ACTIVITY_TYPE_SYNC_VIDEO = 1,  // 同步推流直播
//};

// 活动状态
typedef NS_ENUM(NSInteger ,BY_ACTIVITY_STATUS) {
    BY_ACTIVITY_STATUS_NOT_ENROLL       = -4, // 未报名
    BY_ACTIVITY_STATUS_OVERTIME         = -3, // 审核超时
    BY_ACTIVITY_STATUS_REVIEW_FAIL      = -2, // 被驳回
    BY_ACTIVITY_STATUS_REVIEW_WAITING   = 0,   // 待审核
    BY_ACTIVITY_STATUS_REVIEW_PASS      = 1,   // 已通过
    BY_ACTIVITY_STATUS_END              = 3,   // 已结束
    BY_ACTIVITY_STATUS_DELETE           = 9,   // 已删除
};


typedef NS_ENUM(NSInteger ,BY_HOME_SEGMENT_SUB) {
    BY_HOME_SEGMENT_SUB_ATTENTION,
    BY_HOME_SEGMENT_SUB_RECOMMENT,
    BY_HOME_SEGMENT_SUB_INFO,
    BY_HOME_SEGMENT_SUB_KUAIXUN = 3,
    BY_HOME_SEGMENT_SUB_ANALYST,
};

// 操作指南

static NSString * const review_waiting = @"创建直播后工作人员会及时审核，审核完成后会向您推送消息，请您密切关注本APP，如有问题请添加微信";
//static NSString * const review_fail = @"审核未通过可能有以下原因：直播涉嫌违法内容、账户存在异常、配图与内容无关、审核超时，修改后您可再次提交";


#define K_S_D_F @"yyyy-MM-dd HH:mm" // show dateformatter 展示的时间格式

#define kiSHaveLiveRoomKey [NSString stringWithFormat:@"%@-kiSHaveLiveRoom",[AccountModel sharedAccountModel].account_id]

// 本地存储设置的美颜值
#define kBeautyValue @"kBeautyValue"
#define kWhiteValue  @"kWhiteValue"

#define kSearchCacheKey @"kSearchCacheKey"

#pragma 通知

// 互动直播回放的滑块滑动
#define KNSNotification_ProgressTime @"KNSNotification_ProgressTime"

#endif /* BYLiveDefine_h */
