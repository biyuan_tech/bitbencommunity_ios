//
//  BYLiveHostTimerHeaderView.m
//  BibenCommunity
//
//  Created by 黄亮 on 2018/9/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYLiveHostTimerHeaderView.h"
#import "BYCommonLiveModel.h"

@interface BYLiveHostTimerHeaderView()

@property (nonatomic ,strong) UILabel *timeLab;
@property (nonatomic ,strong) dispatch_source_t timer;

@end

@implementation BYLiveHostTimerHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)setContentView{
    // 头部
    switch ([BYCommonLiveModel shareManager].live_type) {
        case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
        case BY_NEWLIVE_TYPE_AUDIO_PPT:
            [self addPPLiveTHeaderView];
            break;
        default:
            [self addVideoLiveHeaderView];
            break;
    }
    
    // 倒计时
    [self timerCountDown];
    
    // 操作指南
    UIView *operationGuideView = [self getOperationGuideView];
    [self addSubview:operationGuideView];
}

- (void )addPPLiveTHeaderView{
    UIView *bgView = [UIView by_init];
    bgView.backgroundColor = [UIColor clearColor];
}

- (void)addVideoLiveHeaderView{
    PDImageView *imageView = [[PDImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(195);
    }];
    [imageView uploadImageWithURL:[BYCommonLiveModel shareManager].live_cover_url placeholder:nil callback:nil];
    
    UIView *maskView = [UIView by_init];
    maskView.backgroundColor = kColorRGB(50, 50, 50, 0.47);
    [imageView addSubview:maskView];
    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(40);
    }];
    
    UILabel *timeLab = [UILabel by_init];
    [timeLab setBy_font:13];
    timeLab.textColor = [UIColor whiteColor];
    [imageView addSubview:timeLab];
    _timeLab = timeLab;
}

- (UIView *)getOperationGuideView{
    UIView *view = [UIView by_init];
    // 操作指南
    UILabel *guideLab = [UILabel by_init];
    [guideLab setBy_font:15];
    guideLab.textColor = kColorRGBValue(0x353535);
    guideLab.text = @"操作指南";
    [view addSubview:guideLab];
    [guideLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(25);
        make.width.mas_equalTo(stringGetWidth(guideLab.text, guideLab.font.pointSize));
        make.height.mas_equalTo(stringGetHeight(guideLab.text, guideLab.font.pointSize));
    }];
    
    // 操作指南详细
    UILabel *message = [UILabel by_init];
    [message setBy_font:14];
    message.textColor = kTextColor_107;
    message.numberOfLines = 0;
    [view addSubview:message];
    [message mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
    }];
    
    
    return view;
}

// 倒计时
- (void)timerCountDown{
    NSDate *endDate = [NSDate by_dateFromString:[BYCommonLiveModel shareManager].begin_time dateformatter:K_D_F];
    NSInteger timeDif = [NSDate by_startTime:[NSDate by_date] endTime:endDate];
    __weak __typeof(self) weakSelf = self;
    
    if (_timer == nil) {
        __block NSInteger timeout = timeDif; // 倒计时时间
        
        if (timeout!=0) {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
            dispatch_source_set_timer(self.timer, dispatch_walltime(NULL, 0), 1.0*NSEC_PER_SEC,  0); //每秒执行
            dispatch_source_set_event_handler(self.timer, ^{
                if(timeout <= 0){ //  当倒计时结束时做需要的操作: 关闭 活动到期不能提交
                    dispatch_source_cancel(self.timer);
                    self.timer = nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (weakSelf.didTimerBeginLive) {
                            weakSelf.didTimerBeginLive();
                        }
                    });
                } else { // 倒计时重新计算 时/分/秒
                    NSInteger days = (int)(timeout/(3600*24));
                    NSInteger hours = (int)((timeout-days*24*3600)/3600);
                    NSInteger minute = (int)(timeout-days*24*3600-hours*3600)/60;
                    NSInteger second = timeout - days*24*3600 - hours*3600 - minute*60;
                    NSString *strTime = [NSString stringWithFormat:@"直播倒计时 ： %02ld天%02ld时%02ld分%02ld秒", (long)days, (long)hours, (long)minute, (long)second];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (days == 0) {
                            weakSelf.timeLab.text = strTime;
                        } else {
                            weakSelf.timeLab.text = [NSString stringWithFormat:@"直播倒计时 ： %02ld天%02ld时%02ld分%02ld秒", (long)days, (long)hours, (long)minute, (long)second];
                        }
                    });
                    timeout--; // 递减 倒计时-1(总时间以秒来计算)
                }
            });
            dispatch_resume(self.timer);
        }
    }
}

@end
