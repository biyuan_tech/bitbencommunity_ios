//
//  BYLiveHostViewModel.m
//  BibenCommunity
//
//  Created by 黄亮 on 2018/9/17.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYLiveHostViewModel.h"
#import "BYCommonLiveModel.h"
#import "BYLiveHostTimerHeaderView.h"

@interface BYLiveHostViewModel()<BYCommonTableViewDelegate>

@property (nonatomic ,strong) BYCommonTableView *tableView;


@end

@implementation BYLiveHostViewModel

- (void)setContentView{
    
    self.tableView = [[BYCommonTableView alloc] init];
    _tableView.group_delegate = self;
    [S_V_VIEW addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kiPhoneX_Mas_buttom(0), 0));
    }];
    if ([self verifyLiveTime]) { // 已到开播时间
        
    }
    else{
        BYLiveHostTimerHeaderView *headerView = [[BYLiveHostTimerHeaderView alloc] init];
        headerView.frame = CGRectMake(0, 0, kCommonScreenWidth, 425);
        _tableView.tableHeaderView = headerView;
    }
    
}

// 开播时间与当前时间比对
- (BOOL)verifyLiveTime{
    NSDate *date = [NSDate by_dateFromString:[BYCommonLiveModel shareManager].begin_time dateformatter:K_D_F];
    NSDate *nowDate = [NSDate by_date];
    NSComparisonResult result = [date compare:nowDate];
    if (result == NSOrderedAscending ||
        result == NSOrderedSame) {
        return YES;
    }
    else // 未到开播时间，倒计时自动开播
    {
        return NO;
    }
}



@end
