//
//  BYCreatLiveRoomIdentityCell.m
//  BY
//
//  Created by 黄亮 on 2018/8/30.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCreatLiveRoomIdentityCell.h"
#import "BYLiveConfig.h"

@interface BYCreatLiveRoomIdentityCell()

/** 记录button组 */
@property (nonatomic ,strong) NSMutableArray *buttonArr;

@end

static NSInteger baseTag = 0x2314;
@implementation BYCreatLiveRoomIdentityCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.buttonArr = [NSMutableArray array];
        [self setContentView];
    }
    return self;
}

- (void)setContentView{
    CGFloat viewW = 80.f;
    CGFloat spaceLR = 31.f;
    CGFloat spaceW = (kCommonScreenWidth - viewW*3 - spaceLR*2)/3;
    for (int i = 0; i < 3; i++) {
        BY_LIVE_ORGAN_TYPE type = i == 0 ? BY_LIVE_ORGAN_TYPE_PERSON : (i == 1 ? BY_LIVE_ORGAN_TYPE_LECTURER : BY_LIVE_ORGAN_TYPE_ORGAN);
        UIView *view = [self getSubView:type];
        [self.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(viewW);
            make.top.bottom.mas_equalTo(0);
            make.left.mas_equalTo(spaceLR + (viewW + spaceW)*i);
        }];
    }
}

- (void)buttonAction:(UIButton *)sender{
    UIButton *btn = [self getSelectBtn];
    if (btn == sender) return; // 如果与之前被选择的为同一项则return
    sender.selected = !sender.selected;
    btn.selected = NO;
    BY_LIVE_ORGAN_TYPE type = sender.tag - baseTag;
    [self sendActionName:@"creatroom_identity" param:@{@"identity":@(type)} indexPath:self.indexPath];
}

// 找到当前被选择的类型
- (UIButton *)getSelectBtn{
    for (UIButton *btn in _buttonArr) {
        if (btn.selected) {
            return btn;
        }
    }
    return nil;
}

- (UIView *)getSubView:(BY_LIVE_ORGAN_TYPE)type{
    UIView *view = [UIView by_init];
    NSString *imageName;
    NSString *title;
    switch (type) {
        case BY_LIVE_ORGAN_TYPE_PERSON:
            imageName = @"creatroom_person";
            title     = @"个人";
            break;
        case BY_LIVE_ORGAN_TYPE_LECTURER:
            imageName = @"creatroom_lecturer";
            title     = @"产业人";
            break;
        case BY_LIVE_ORGAN_TYPE_ORGAN:
            imageName = @"creatroom_organ";
            title     = @"机构";
            break;
        default:
            break;
    }
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView by_setImageName:imageName];
    [view addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(imageView.image.size.width);
        make.height.mas_equalTo(imageView.image.size.height);
        make.top.mas_equalTo(18);
        make.centerX.mas_equalTo(0);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:13];
    [titleLab setTextColor:kColorRGBValue(0x4c4c4c)];
    titleLab.text = title;
    [titleLab sizeToFit];
    [view addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(CGRectGetWidth(titleLab.frame));
        make.height.mas_equalTo(CGRectGetHeight(titleLab.frame));
        make.top.equalTo(imageView.mas_bottom).with.offset(8);
        make.centerX.mas_equalTo(0);
    }];
    
    UIButton *button = [UIButton by_buttonWithCustomType];
    [button setBy_imageName:@"creatroom_select" forState:UIControlStateSelected];
    [button setBy_imageName:@"creatroom_unselect" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    button.tag = baseTag + type;
    [_buttonArr addObject:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(33);
        make.top.equalTo(titleLab.mas_bottom).with.offset(10);
        make.centerX.mas_equalTo(0);
    }];
    return view;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
