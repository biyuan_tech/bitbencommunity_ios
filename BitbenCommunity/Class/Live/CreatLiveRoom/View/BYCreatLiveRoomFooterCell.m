//
//  BYCreatLiveRoomFooterCell.m
//  BY
//
//  Created by 黄亮 on 2018/8/31.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCreatLiveRoomFooterCell.h"

static NSInteger baseTag = 0x3231;
@implementation BYCreatLiveRoomFooterCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:kBgColor_248];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    self.indexPath = indexPath;
}

- (void)buttonAction:(UIButton *)sender{
    if (sender.tag == baseTag + 2) {
        sender.selected = !sender.selected;
        [self sendActionName:@"creatroom_protocol" param:@{@"isAgree":@(sender.selected)} indexPath:self.indexPath];
    }
    else if (sender.tag == baseTag){
        sender.selected = !sender.selected;
    }
}

- (void)setContentView{
    for (int i = 0; i < 3; i ++) {
        
        UILabel *messageLab = [UILabel by_init];
        messageLab.textColor = kTextColor_117;
        [messageLab setBy_font:13];
        messageLab.text = @"不定期给直播间运营者提供功能使用和运营技巧讲解或培训。";
        messageLab.numberOfLines = 0;
        [self.contentView addSubview:messageLab];
        [messageLab sizeToFit];
        [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(35);
            make.left.mas_equalTo(37);
            make.right.mas_equalTo(-15);
            make.height.mas_greaterThanOrEqualTo(13);
        }];
        
        UIButton *btn = [UIButton by_buttonWithCustomType];
        [btn setBy_imageName:@"creatroom_select" forState:UIControlStateNormal];
//        if (i != 1) {
//            [btn setBy_imageName:@"creatroom_select" forState:UIControlStateSelected];
//        }
        [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn];
        btn.tag = baseTag + i;
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(7.5);
            if (i == 0)
                make.top.mas_equalTo(7.5);
            else
                make.top.equalTo(messageLab.mas_bottom).with.offset(i == 1 ? 8.5 : 45.5);
            make.width.mas_equalTo(btn.imageView.image.size.width);
            make.height.mas_equalTo(btn.imageView.image.size.height);
        }];
        
        UILabel *title = [UILabel by_init];
        [title setBy_font:14];
        [title setTextColor:kTextColor_102];
        switch (i) {
            case 0:
                title.text = @"添加微信好友进群";
                break;
            case 1:
                title.text = @"身份仅为标识，不影响后续直播类型选择";
                break;
            case 2:
                title.text = @"勾选表示同意《币本平台产业人协议》";
                break;
            default:
                break;
        }
        [self.contentView addSubview:title];
        [title sizeToFit];
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(37);
            make.centerY.equalTo(btn.mas_centerY).with.offset(-5);
            make.width.mas_equalTo(CGRectGetWidth(title.frame));
            make.height.mas_equalTo(CGRectGetHeight(title.frame));
        }];
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
