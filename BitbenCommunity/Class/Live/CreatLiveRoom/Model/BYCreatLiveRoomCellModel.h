//
//  BYCreatLiveRoomCellModel.h
//  BY
//
//  Created by 黄亮 on 2018/8/30.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonModel.h"

@interface BYCreatLiveRoomCellModel : BYCommonModel


/**
 按提示
 */
@property (nonatomic ,strong) NSString *placeholder;

@end
