//
//  BYCreatLiveRoomModel.h
//  BY
//
//  Created by 黄亮 on 2018/8/30.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonModel.h"
#import "BYLiveConfig.h"

@interface BYCreatLiveRoomModel : BYCommonModel

/**
 直播间主题
 */
@property (nonatomic ,strong) NSString *roomTitle;

/**
 可联系的微信
 */
@property (nonatomic ,strong) NSString *wxId;

/**
 身份
 */
@property (nonatomic ,assign) BY_LIVE_ORGAN_TYPE organ_type;

/**
 是否同意协议
 */
@property (nonatomic ,assign) BOOL isAgree;

@property (nonatomic ,strong) NSArray *tableData;

@end
