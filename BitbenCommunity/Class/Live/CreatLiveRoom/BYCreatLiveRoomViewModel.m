//
//  BYCreatLiveRoomViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/8/30.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCreatLiveRoomViewModel.h"
#import "BYLiveRoomHomeController.h"

#import "BYCreatLiveRoomModel.h"
#import "BYCreatLiveRoomCellModel.h"
#import "BYIMManager.h"
#import "BYTabbarViewController.h"

// request

@interface BYCreatLiveRoomViewModel()<BYCommonTableViewDelegate>

@property (nonatomic ,strong) BYCommonTableView *tableView;

@end

#define S_DM ((BYCreatLiveRoomModel *)self.dataModel)

@implementation BYCreatLiveRoomViewModel

- (Class)getDataModelClass{
    return [BYCreatLiveRoomModel class];
}

- (void)setContentView{
    
    [self addRightBarButtonItem];
    
    [S_V_VIEW addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    S_DM.organ_type = -1;
    S_DM.isAgree = 1;
    self.tableView.tableData = S_DM.tableData;
}

- (void)addRightBarButtonItem{
    UIBarButtonItem *rightBarButtonItem = [UIBarButtonItem initWithTitle:@"提交" target:self action:@selector(rightBarButtonItemAction) type:BYBarButtonItemTypeRight];
    S_VC.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

- (void)rightBarButtonItemAction{

    if (!S_DM.roomTitle.length) {
        showToastView(@"直播间主题未填写", S_V_VIEW);
        return;
    }
    if (!S_DM.wxId.length) {
        showToastView(@"联系微信未填写", S_V_VIEW);
        return;
    }
    if ([@(S_DM.organ_type) integerValue] == -1) {
        showToastView(@"请选择您的身份", S_V_VIEW);
        return;
    }
    if (!S_DM.isAgree) {
        showToastView(@"未同意协议", S_V_VIEW);
        return;
    }
    [self loadRequestCreatLiveRoom];
    
    // 埋点 提交直播间
    [MTAManager event:MTATypeLiveRelease params:nil];
}

#pragma mark - BYCommonTableViewDelegate

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    BYCreatLiveRoomCellModel *model = tableView.tableData[indexPath.row];
    if ([actionName isEqualToString:@"textField_change"]) { // 内容输入回调
        model.title = param[@"text"];
        if (indexPath.row == 1){
            S_DM.roomTitle = param[@"text"];
        }
        else
        {
            S_DM.wxId = param[@"text"];
        }
        
    }
    else if ([actionName isEqualToString:@"creatroom_identity"])
    {
        S_DM.organ_type = [param[@"identity"] integerValue];
    }
//    else if ([actionName isEqualToString:@"creatroom_protocol"])
//    {
//        S_DM.isAgree = [param[@"isAgree"] boolValue];
//    }
    
}

#pragma mark - requestMehtod

- (void)loadRequestCreatLiveRoom{
    BYToastView *toastView = [BYToastView toastViewPresentLoading];
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatLiveRoom:S_DM.roomTitle weChat:S_DM.wxId organType:S_DM.organ_type successBlock:^(id object) {
        @strongify(self);
        [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:kiSHaveLiveRoomKey];
        BYLiveRoomHomeController *liveRoomHomeController = [[BYLiveRoomHomeController alloc] init];
        [S_VC dismissViewControllerAnimated:NO completion:nil];
        UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
        [currentController.navigationController pushViewController:liveRoomHomeController animated:YES];        [toastView dissmissToastView];
    } faileBlock:^(NSError *error) {
        [toastView dissmissToastView];
    }];
}

#pragma mark - initMethod

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [BYCommonTableView by_init];
        _tableView.group_delegate = self;
        _tableView.backgroundColor = kColorRGBValue(0xf8f8f8);
    }
    return _tableView;
}

@end
