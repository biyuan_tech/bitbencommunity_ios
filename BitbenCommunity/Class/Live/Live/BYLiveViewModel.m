//
//  BYLiveViewModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYLiveViewModel.h"
// controller
#import "BYILiveRoomController.h"
#import "BYLiveSetController.h"
#import "BYLiveController.h"
#import "BYPPTRoomController.h"
#import "BYVODPlayController.h"
#import "BYVideoLiveController.h"
#import "BYLiveDetailController.h"
// view
#import "BYLiveHeaderView.h"
// request
// model
#import "BYCommonModel.h"
#import "BYCommonLiveModel.h"
#import "BYLiveHeaderModel.h"

@interface BYLiveViewModel()<BYCommonTableViewDelegate>
{
    NSInteger _pageNum;
}
@property (nonatomic ,assign) BOOL isHave;
@property (nonatomic ,strong) BYCommonTableView *tableView;
@property (nonatomic ,strong) BYLiveHeaderView *headerView;
@property (nonatomic ,strong) UIButton *editBtn;
@property (nonatomic ,strong) UIButton *enterBtn;
@end;

#define S_LIVE_VC ((BYLiveController *)S_VC)
@implementation BYLiveViewModel

- (void)setContentView{
    _pageNum = 0;
    _isHave = S_LIVE_VC.model ? YES : NO;
    [S_V_VIEW addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, -kiPhoneX_Mas_buttom(50), 0));
    }];
    
    [self addBottomView];
    
    if (S_LIVE_VC.model) {
        [self.headerView reloadData:S_LIVE_VC.model];
        [self reloadBottomStatus];
    }
    [self loadRequestGetLiveList];
}

- (void)refreshData{
    _pageNum = 0;
    [self loadRequestGetLiveList];
}

- (void)loadMoreData{
    [self loadRequestGetLiveList];
}

- (void)reloadBottomStatus{
    if (self.headerView.model.status == BY_LIVE_STATUS_END ||
        self.headerView.model.status == BY_LIVE_STATUS_OVERTIME) {
        self.editBtn.enabled = NO;
        self.editBtn.layer.borderColor = kBgColor_180.CGColor;
    }
    else{
        self.editBtn.enabled = YES;
        self.editBtn.layer.borderColor = kBgColor_238.CGColor;
    }
    
    
    // 点播时修改底部按钮文案
    NSString *enterBtnTitle;
    if (self.headerView.model.live_type == BY_NEWLIVE_TYPE_VOD_VIDEO ||
        self.headerView.model.live_type == BY_NEWLIVE_TYPE_VOD_AUDIO) {
        enterBtnTitle = @"进入观看";
    }
    else{
        enterBtnTitle = @"进入直播";
    }
    [self.enterBtn setBy_attributedTitle:@{@"title":enterBtnTitle,NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];

}

// 在第一页数据中删除与头部相同的数据
- (NSArray *)removeHeaderViewSameData:(NSArray *)arr{
    if (!arr.count) return arr;
    NSMutableArray *tmpArr = [NSMutableArray arrayWithArray:arr];
    NSMutableArray *data = [NSMutableArray arrayWithArray:arr];
    for (id model in tmpArr) {
        if ([model isKindOfClass:[BYCommonLiveModel class]]) {
            BYCommonLiveModel *liveModel = (BYCommonLiveModel *)model;
            if ([liveModel.live_record_id isEqualToString:self.headerView.model.live_record_id]) {
                [self.headerView reloadData:liveModel];
                S_LIVE_VC.model = liveModel;
                [data removeObject:liveModel];
            }
        }
    }
    return data;
}

// 跳转直播设置
- (void)editBtnAction{
    BYLiveSetController *liveSetController = [[BYLiveSetController alloc] init];
    liveSetController.model = self.headerView.model;
    @weakify(self);
    liveSetController.reloadLiveRoomHandel = ^{
        @strongify(self);
        [self.headerView reloadData:self.headerView.model];
    };
    [S_V_NC pushViewController:liveSetController animated:YES];
}

// 进入开播
- (void)enterLiveBtnAction{
    [self pushControllerByLiveType:self.headerView.model.live_type];
}

- (void)pushControllerByLiveType:(BY_NEWLIVE_TYPE)liveType{
    if (liveType == BY_NEWLIVE_TYPE_ILIVE) {
        BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
        liveDetailController.live_record_id = self.headerView.model.live_record_id;
        liveDetailController.isHost = NO;
        liveDetailController.navigationTitle = @"个人互动直播";
        [S_VC authorizePush:liveDetailController animation:YES];
    }
    else if (liveType == BY_NEWLIVE_TYPE_AUDIO_PPT ||
             liveType == BY_NEWLIVE_TYPE_AUDIO_LECTURE){ // ppt直播
        
        BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
        liveDetailController.live_record_id = self.headerView.model.live_record_id;
        liveDetailController.isHost = NO;
        liveDetailController.navigationTitle = @"微直播";
        [S_VC authorizePush:liveDetailController animation:YES];
    }
    else if (liveType == BY_NEWLIVE_TYPE_VOD_AUDIO ||
             liveType == BY_NEWLIVE_TYPE_VOD_VIDEO){ // 视频上传
        BYVODPlayController *vodPlayController = [[BYVODPlayController alloc] init];
        vodPlayController.live_record_id = self.headerView.model.live_record_id;
        vodPlayController.navigationTitle = self.headerView.model.live_title;
        [S_V_NC pushViewController:vodPlayController animated:YES];
    }
    else if (liveType == BY_NEWLIVE_TYPE_VIDEO){ // 视频直播
        BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
        liveDetailController.live_record_id = self.headerView.model.live_record_id;
        liveDetailController.isHost = NO;
        liveDetailController.navigationTitle = @"推流直播";
        [S_VC authorizePush:liveDetailController animation:YES];
    }
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) return;
    BYCommonLiveModel *model = tableView.tableData[indexPath.row];
    BYLiveController *livecontroller = [[BYLiveController alloc] init];
    livecontroller.model = model;
    [S_V_NC pushViewController:livecontroller animated:YES];
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"moreAction"]) {
        BYCommonLiveModel *model = tableView.tableData[indexPath.row];
        @weakify(self);
        [BYCommonTool showDelSheetViewInCurrentView:model.live_record_id theme_type:BY_THEME_TYPE_LIVE cb:^{
            @strongify(self);
            NSMutableArray *tableData = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [tableData removeObject:model];
            self.tableView.tableData = tableData;
        }];
    }
}

#pragma mark - request method
- (void)loadRequestGetLiveList{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLivelist:[AccountModel sharedAccountModel].account_id pageNum:_pageNum successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        [self.tableView endRefreshing];
        NSMutableArray *tmpArr = [NSMutableArray array];
        if (self->_pageNum == 0) {
            self->_pageNum ++;
            BYCommonModel *model = [[BYCommonModel alloc] init];
            model.cellString = @"BYLiveRoomHomeTitleCell";
            model.cellHeight = 58;
            model.title = @"其他直播";
            [tmpArr addObject:model];
            if ([object count] >= 1) {
                NSMutableArray *arr = [NSMutableArray arrayWithArray:object];
                if (!self.isHave) {
                    [self.headerView reloadData:object[0]];
                    [self reloadBottomStatus];
//                    [arr removeObjectAtIndex:0];
                }
                arr = [[self removeHeaderViewSameData:arr] copy];
                [tmpArr addObjectsFromArray:arr];
            }
            self.tableView.tableData = tmpArr;
            return ;
        }
        self->_pageNum ++;
        [tmpArr addObjectsFromArray:self.tableView.tableData];
        [tmpArr addObjectsFromArray:object];
        self.tableView.tableData = tmpArr;
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}
#pragma mark - initMethod

- (void)addBottomView{
    
    UIView *bottomView = [UIView by_init];
    [bottomView setBackgroundColor:[UIColor whiteColor]];
    [S_V_VIEW addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(kiPhoneX_Mas_buttom(0));
        make.height.mas_equalTo(50);
    }];
    
    UIView *lineView = [UIView by_init];
    lineView.backgroundColor = kTextColor_229;
    [bottomView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    CGFloat spaceW = (kCommonScreenWidth - 132*2)/3;
    
    // 编辑介绍页
    UIButton *editBtn = [UIButton by_buttonWithCustomType];
    [editBtn setBy_attributedTitle:@{@"title":@"编辑介绍页",NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kTextColor_237} forState:UIControlStateNormal];
    [editBtn setBy_attributedTitle:@{@"title":@"编辑介绍页",NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kTextColor_180} forState:UIControlStateDisabled];
    [editBtn addTarget:self action:@selector(editBtnAction) forControlEvents:UIControlEventTouchUpInside];
    editBtn.layer.cornerRadius = 8;
    editBtn.layer.borderWidth = 1;
    editBtn.layer.borderColor = kBgColor_238.CGColor;
    [bottomView addSubview:editBtn];
    self.editBtn = editBtn;
    [editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(spaceW);
        make.width.mas_equalTo(132);
        make.height.mas_equalTo(36);
        make.centerY.mas_equalTo(0);
    }];
    
    // 进入直播
    UIButton *enterLiveBtn = [UIButton by_buttonWithCustomType];
    [enterLiveBtn setBackgroundColor:kBgColor_238];
    [enterLiveBtn setBy_attributedTitle:@{@"title":@"进入直播",NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [enterLiveBtn addTarget:self action:@selector(enterLiveBtnAction) forControlEvents:UIControlEventTouchUpInside];
    enterLiveBtn.layer.cornerRadius = 8;
    [bottomView addSubview:enterLiveBtn];
    _enterBtn = enterLiveBtn;
    [enterLiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-spaceW);
        make.width.mas_equalTo(132);
        make.height.mas_equalTo(36);
        make.centerY.mas_equalTo(0);
    }];
    
}

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        _tableView.tableHeaderView = self.headerView;
        [_tableView addHeaderRefreshTarget:self action:@selector(refreshData)];
        [_tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    }
    return _tableView;
}

- (BYLiveHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[BYLiveHeaderView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, 360)];
    }
    return _headerView;
}

@end
