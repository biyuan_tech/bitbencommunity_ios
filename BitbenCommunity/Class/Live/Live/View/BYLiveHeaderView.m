//
//  BYLiveHeaderView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYLiveHeaderView.h"
#import "BYLiveHeaderModel.h"

@interface BYLiveHeaderView()

/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogo;
/** 直播间标题 */
@property (nonatomic ,strong) UILabel *roomTitleLab;
/** 直播间封面图 */
@property (nonatomic ,strong) PDImageView *coverImageView;
/** 直播标题 */
@property (nonatomic ,strong) UILabel *liveTitleLab;
/** 开播时间 */
@property (nonatomic ,strong) UILabel *timeLab;

@property (nonatomic ,strong) UIView *watchNumView;
/** 观看人数 */
@property (nonatomic ,strong) UILabel *watchNumLab;
@property (nonatomic ,strong) BYCommonLiveModel *model;
@end

@implementation BYLiveHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}


#pragma mark - custom Mehtod
- (void)reloadData:(BYCommonLiveModel *)model{
    self.model = model;
    self.userlogo.style = model.cert_badge;
    [_userlogo uploadHDImageWithURL:model.speaker_head_img.length ? model.speaker_head_img : model.head_img callback:nil];
    
    _roomTitleLab.text = [NSString stringWithFormat:@"%@的直播 %d",nullToEmpty(model.nickname),(int)model.room_id];
    [_coverImageView uploadImageWithURL:model.live_cover_url placeholder:nil callback:nil];
    
    [_liveTitleLab sizeToFit];
    _liveTitleLab.text = model.live_title;
//    _timeLab.text = model.begin_time;
    _timeLab.text = model.show_begin_time;
    _watchNumLab.text = stringFormatInteger(model.watch_times);
    
    _watchNumView.layer.opacity = model.status == BY_LIVE_STATUS_LIVING ? 1.0 : 0.0;

}

- (void)setContentView{
    // 头像
    PDImageView *userlogo = [[PDImageView alloc] init];
    userlogo.contentMode = UIViewContentModeScaleAspectFill;
    userlogo.layer.cornerRadius = 15.0f;
    userlogo.clipsToBounds = YES;
    [self addSubview:userlogo];
    _userlogo = userlogo;
    [userlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(30);
        make.left.mas_equalTo(17);
        make.top.mas_equalTo(11);
    }];
    
    // 头像背景
    UIImageView *userlogoBg = [[UIImageView alloc] init];
    [userlogoBg by_setImageName:@"common_userlogo_bg"];
    [self addSubview:userlogoBg];
    [self insertSubview:userlogoBg belowSubview:userlogo];
    [userlogoBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(userlogoBg.image.size.width);
        make.height.mas_equalTo(userlogoBg.image.size.height);
        make.centerX.equalTo(userlogo.mas_centerX).with.offset(0);
        make.centerY.equalTo(userlogo.mas_centerY).with.offset(0);
    }];
    
    // 直播间主题
    UILabel *roomTitleLab = [UILabel by_init];
    [roomTitleLab setBy_font:14];
    roomTitleLab.textColor = kTextColor_60;
    [self addSubview:roomTitleLab];
    _roomTitleLab = roomTitleLab;
    [roomTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userlogo.mas_right).with.offset(8);
        make.centerY.equalTo(userlogo.mas_centerY).with.offset(0);
        make.height.mas_equalTo(15);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    // 直播间封面图
    PDImageView *coverImageView = [[PDImageView alloc] init];
    coverImageView.contentMode = UIViewContentModeScaleAspectFill;
    coverImageView.clipsToBounds = YES;
    [self addSubview:coverImageView];
    _coverImageView = coverImageView;
    [coverImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(54);
        make.height.mas_equalTo(195);
    }];
    
    // 直播主题
    UILabel *liveTitleLab = [UILabel by_init];
    [liveTitleLab setBy_font:15];
    liveTitleLab.textColor = kTextColor_60;
    liveTitleLab.numberOfLines = 0;
    [self addSubview:liveTitleLab];
    _liveTitleLab = liveTitleLab;
    [liveTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(coverImageView.mas_bottom).with.offset(20);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.centerX.mas_equalTo(0);
        make.height.mas_greaterThanOrEqualTo(15);
    }];
    
    // 时间图标
    UIImageView *timeImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"common_time_icon"]];
    [self addSubview:timeImgView];
    [timeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.equalTo(coverImageView.mas_bottom).with.offset(70);
        make.width.mas_equalTo(timeImgView.image.size.width);
        make.height.mas_equalTo(timeImgView.image.size.height);
    }];
    
    // 时间
    UILabel *timeLab = [[UILabel alloc] init];
    timeLab.font = [UIFont systemFontOfSize:12];
    timeLab.textColor = kTextColor_154;
    [self addSubview:timeLab];
    _timeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeImgView.mas_right).with.offset(5);
        make.centerY.equalTo(timeImgView.mas_centerY).with.offset(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(18);
    }];
    
    // 观看人数
    UIView *watchNumView = [UIView by_init];
    [self addSubview:watchNumView];
    _watchNumView = watchNumView;
    [watchNumView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(13);
        make.width.mas_equalTo(60);
        make.centerY.mas_equalTo(timeImgView.mas_centerY);
    }];
    
    UIView *bottomView = [UIView by_init];
    bottomView.backgroundColor = kBgColor_248;
    [self addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(0);
        make.height.mas_equalTo(9);
    }];
}

- (UIView *)getWatchNumView{
    UIView *watchNumView = [UIView by_init];
    UIImageView *watchNumImgView = [UIImageView by_init];
    [watchNumImgView by_setImageName:@"livehome_watchnum"];
    [watchNumView addSubview:watchNumImgView];
    [watchNumImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.width.mas_equalTo(watchNumImgView.image.size.width);
        make.height.mas_equalTo(watchNumImgView.image.size.height);
    }];
    
    // 直播人数
    UILabel *watchNumLab = [UILabel by_init];
    [watchNumLab setBy_font:12];
    watchNumLab.textColor = kTextColor_154;
    [watchNumView addSubview:watchNumLab];
    _watchNumLab = watchNumLab;
    [watchNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(watchNumImgView.mas_right).with.offset(5);
        make.centerY.mas_equalTo(watchNumLab.mas_centerY);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(11);
    }];
    return watchNumView;
}

@end
