//
//  BYLiveEditBgImgViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/11.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveEditBgImgViewModel.h"
#import "BYLiveEditBgImgController.h"
#import "GWAssetsImgSelectedViewController.h"
#import "BYCommonLiveModel.h"

#define K_VC ((BYLiveEditBgImgController *)self.S_VC)
static NSInteger baseTag = 0x534;
@interface BYLiveEditBgImgViewModel()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    CGFloat _imageW;
    CGFloat _imageH;
    CGFloat _headViewH;
}
/** contentView */
@property (nonatomic ,strong) UIScrollView *scrollView;

@property (nonatomic ,strong) UIImageView *imageView;
@property (nonatomic ,strong) NSArray *defultImgs;
@property (nonatomic ,strong) UIImage *selectImage;
@property (nonatomic ,copy) NSString *selectImageUrl;
@property (nonatomic ,strong) NSMutableArray *dfSelectImageViewArr;
/** 是否选择的默认图 */
@property (nonatomic ,assign) BOOL isSelectDF;
@property (nonatomic ,copy) NSString *updateKey;
@end

@implementation BYLiveEditBgImgViewModel


#pragma mark - action Method

// 图片点击
- (void)imageViewTapAction{
    if (_imageView.image) {
//        GWAssetsImgSelectedViewController *imgSelectVC = [[GWAssetsImgSelectedViewController alloc] init];
//        [imgSelectVC showInView:S_VC imgArr:@[_imageView.image] currentIndex:0 cell:_imageView];
    }
}

// 添加图片
- (void)addImageAction{
    [self openPhotoLibrary];
}

- (void)delBtnAction{
    self.selectImage = nil;
    self.imageView.image = nil;
    self.selectImageUrl = nil;
    self.isSelectDF = NO;
    [self reloadImageViewMas];
    for (int i = 0; i < 6; i ++) {
        UIButton *btn = [_scrollView viewWithTag:2*baseTag + i];
        UIImageView *selImgView = [_scrollView viewWithTag:3*baseTag + i];
        btn.selected = NO;
        selImgView.hidden = YES;
    }
}

// 选择默认图
- (void)selectDFImgViewAction:(UIButton *)sender{
    self.isSelectDF = YES;
    NSInteger index = sender.tag - 2*baseTag;
    UIImageView *imageView = [_scrollView viewWithTag:baseTag + index];
    self.imageView.image = imageView.image;
    self.selectImage = imageView.image;
    self.selectImageUrl = self.defultImgs[index];
    for (int i = 0; i < 6; i ++) {
        UIButton *btn = [_scrollView viewWithTag:2*baseTag + i];
        UIImageView *selImgView = [_scrollView viewWithTag:3*baseTag + i];
        btn.selected = btn == sender ? YES : NO;
        selImgView.hidden = btn == sender ? NO : YES;
    }
    [self reloadImageViewMas];
}

// 图片上传
- (void)rightBarButtonItemAction{
    if (!_selectImage) {
        showToastView(@"请先选择上传图片", S_V_VIEW);
        return;
    }
    // 判断当前图片是否修改过
    if (_selectImageUrl.length && [_selectImageUrl isEqualToString:[self getInitImgeViewUrl]]) {
        showToastView(@"图片未修改", S_V_VIEW);
        return;
    }

    BYToastView *toastView = [BYToastView toastViewPresentLoading];
    // 如选择的是默认图则直接上传
    if (_isSelectDF) {
        if (K_VC.edit_type == EDIT_IMG_TYPE_LIVEROOM) {
            [self loadRequestUpdateLiveRoom:@{self.updateKey:_selectImageUrl} toastView:toastView];
        }
        else
        {
            [self loadRequestUpdateLive:@{self.updateKey:_selectImageUrl} toastView:toastView];
        }
        return;
    }
    OSSFileModel *model = [OSSFileModel new];
    model.objcImage = _selectImage;
    model.objcName  = [NSString stringWithFormat:@"%@-%@",[NSDate by_stringFromDate:[NSDate by_date] dateformatter:@"yyyy-MM-dd-HH:mm:ss"],self.updateKey];
    @weakify(self);
    [[OSSManager sharedUploadManager] uploadImageManagerWithImageList:[@[model] copy] withUrlBlock:^(NSArray *imgUrlArr) {
        @strongify(self);
        if (K_VC.edit_type == EDIT_IMG_TYPE_PUBLICITY ||
            K_VC.edit_type == EDIT_IMG_TYPE_LIVE) {
            [self loadRequestUpdateLive:@{self.updateKey:imgUrlArr[0]} toastView:toastView];
        }
        else
        {
            [self loadRequestUpdateLiveRoom:@{self.updateKey:imgUrlArr[0]} toastView:toastView];
        }
    }];
}

// 更新展示imagwView的布局
- (void)reloadImageViewMas{
    _imageView.hidden = _imageView.image ? NO : YES;

    CGFloat imageViewH = self.imageView.image ? (kCommonScreenWidth-30)*self.selectImage.size.height/self.selectImage.size.width : _headViewH;
    @weakify(self);
    [_imageView mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        if (!self) return ;
        make.height.mas_equalTo(imageViewH);
    }];
    // 如有image则重新给imageview绘制圆角
    if (self.imageView.image) {
        [_imageView addCornerRadius:10 size:CGSizeMake(kCommonScreenWidth-30, imageViewH)];
    }
    [_scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        if (K_VC.edit_type == EDIT_IMG_TYPE_LIVEROOM) {
            UIImageView *imageView = [self.scrollView viewWithTag:baseTag + 5];
            if (!imageView) return ;
            make.bottom.mas_equalTo(imageView.mas_bottom).offset(20).priorityLow();
        }
        else
        {
            make.bottom.mas_equalTo(self.imageView.mas_bottom).offset(20).priorityLow();
        }
        make.bottom.mas_greaterThanOrEqualTo(S_V_VIEW);
    }];
}

// 获取初始imageView加载的url
- (NSString *)getInitImgeViewUrl{
    NSString *imageUrl = nil;
    switch (K_VC.edit_type) {
        case EDIT_IMG_TYPE_LIVEROOM:
            imageUrl = K_VC.model.cover_url;
            break;
        case EDIT_IMG_TYPE_LIVE:
            imageUrl = K_VC.model.live_cover_url;
            break;
        default:
            imageUrl = K_VC.model.ad_img;
            break;
    }
    return imageUrl;
}

#pragma mark - request method

// 更新直播间信息
- (void)loadRequestUpdateLiveRoom:(NSDictionary *)param toastView:(BYToastView *)toastView{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadReuqestUpdateLiveRoom:param successBlock:^(id object) {
        @strongify(self);
        K_VC.model.cover_url = param[self.updateKey];
        if (K_VC.reloadLiveRoomHandel) {
            K_VC.reloadLiveRoomHandel();
        }
        [toastView dissmissToastView];
        showToastView(@"上传成功", S_V_VIEW);
    } faileBlock:^(NSError *error) {
        @strongify(self);
        [toastView dissmissToastView];
        showToastView(@"上传失败", S_V_VIEW);
    }];
}

// 更新直播信息
- (void)loadRequestUpdateLive:(NSDictionary *)param toastView:(BYToastView *)toastView{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveSet:K_VC.model.live_record_id param:param successBlock:^(id object) {
        @strongify(self);
        if (K_VC.edit_type == EDIT_IMG_TYPE_LIVE) {
            K_VC.model.live_cover_url = param[self.updateKey];
        }
        else{
            K_VC.model.ad_img = param[self.updateKey];
        }
        [toastView dissmissToastView];
        if (K_VC.reloadLiveRoomHandel) {
            K_VC.reloadLiveRoomHandel();
        }
        showToastView(@"修改成功", S_V_VIEW);
    } faileBlock:^(NSError *error) {
        [toastView dissmissToastView];
    }];
}

// 打开相册
- (void)openPhotoLibrary{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.navigationBar.translucent=NO;
        [self.viewController presentViewController:imagePicker animated:YES completion:nil];
        return;
    }
}

// 图片尺寸校验
- (BOOL)verifyImgSize:(UIImage *)image{
    switch (K_VC.edit_type) {
        case EDIT_IMG_TYPE_LIVE:
        {
            if (image.size.width < image.size.height) {
                showToastView(@"图片尺寸超出范围", S_V_VIEW);
                return NO;
            }
            return YES;
        }
            break;
        default:
        {
            if (image.size.width > image.size.height) {
                showToastView(@"图片尺寸超出范围", S_V_VIEW);
                return NO;
            }
            return YES;
        }
            break;
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{

    UIImage *image = info[UIImagePickerControllerOriginalImage];
//    // 校验图片尺寸
//    if (![self verifyImgSize:image]) {
//        [picker dismissViewControllerAnimated:YES completion:nil];
//        return;
//    }
    // 图片尺寸限制
    UIImage *newImage = [image resizeImageToMaxWidthOrHeight:_imageW > _imageH ? _imageW : _imageH];
    // 图片大小限制
    NSData *data = [newImage compressWithMaxLength:1024*1024*2];
    UIImage *tmpImg  = [UIImage imageWithData:data];
    _imageView.image = [tmpImg addCornerRadius:8 size:tmpImg.size];
    _selectImage = tmpImg;
    [self reloadImageViewMas];
    
    //如果是相机拍照，则保存到相册
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - initMethod/configSubView

- (void)setContentView{
    [self addRightBarButtonItem];
    
    [S_V_VIEW addSubview:self.scrollView];
    [_scrollView setContentSize:CGSizeMake(kCommonScreenWidth, kCommonScreenHeight)];
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self addHeaderView];
    
    PDImageView *imageView = [[PDImageView alloc] init];
    imageView.userInteractionEnabled = YES;
    imageView.hidden = YES;
    _imageView = imageView;
    [_scrollView addSubview:imageView];
    @weakify(self);
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        if (!self) return ;
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.height.mas_equalTo(self->_headViewH);
        make.centerX.mas_equalTo(0);
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewTapAction)];
    [imageView addGestureRecognizer:tapGestureRecognizer];
    
    UIButton *delBtn = [UIButton by_buttonWithCustomType];
    [delBtn setBy_imageName:@"liveedit_del" forState:UIControlStateNormal];
    [delBtn addTarget:self action:@selector(delBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:delBtn];
    [delBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.mas_equalTo(0);
        make.width.height.mas_equalTo(40);
    }];
    
    // 加载默认图选择图
    if (K_VC.edit_type == EDIT_IMG_TYPE_LIVEROOM) {
        NSMutableArray *tmpArr = [NSMutableArray array];
        NSString *defultHeaderStr = @"defult_liveroom_bg_";
        for (int i = 0; i < 6; i ++) {
            [tmpArr addObject:[NSString stringWithFormat:@"%@%02d.jpg",defultHeaderStr,i]];
        }
        self.defultImgs = [tmpArr copy];
        // 添加默认选择背景图
        [self addDefultImgBg];
    }
    
    NSString *imageUrl = [self getInitImgeViewUrl];
    _selectImageUrl = imageUrl;
    if (imageUrl) {
        [imageView uploadMainImageWithURL:imageUrl placeholder:nil imgType:PDImgTypeOriginal callback:^(UIImage *image) {
            @strongify(self);
            self.selectImage = image;
            self.imageView.image = [image addCornerRadius:8 size:image.size];
            [self reloadImageViewMas];
        }];
    }
}

- (void)addHeaderView{
    _headViewH = 0;
    CGFloat top = 0;
    NSString *messageStr = @"";
    switch (K_VC.edit_type) {
        case EDIT_IMG_TYPE_LIVEROOM:
            _headViewH = 171;
            top = 30;
            _imageW = 750;
            _imageH = 370;
            messageStr = @"JPG、BMP、JPEG最大2M，370*750）";
            self.updateKey = @"cover_url";
            break;
        case EDIT_IMG_TYPE_LIVE:
            _headViewH = 153;
            top = 24;
            _imageW = 360;
            _imageH = 240;
            messageStr = @"（JPG、BMP、JPEG最大2M，360*240）";
            self.updateKey = @"live_cover_url";
            break;
        default:
            _headViewH = 230;
            top = 60;
            _imageW = 750;
            _imageH = 370;
            messageStr = @"（JPG、BMP、JPEG最大2M，370*750）";
            self.updateKey = @"ad_img";
            break;
    }
    UIView *bgView = [UIView by_init];
    bgView.backgroundColor = kBgColor_237;
    bgView.layer.cornerRadius = 8;
    //    [bgView layerCornerRadius:8 size:CGSizeMake(kCommonScreenWidth-30, height)];
    [_scrollView addSubview:bgView];
    @weakify(self);
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        if (!self) return ;
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(self->_headViewH);
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addImageAction)];
    [bgView addGestureRecognizer:tapGestureRecognizer];
    
    UIImageView *addimgView = [[UIImageView alloc] init];
    [addimgView by_setImageName:@"liveedit_bgimg_addimg"];
    [bgView addSubview:addimgView];
    [addimgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(top);
        make.width.mas_equalTo(addimgView.image.size.width);
        make.height.mas_equalTo(addimgView.image.size.height);
        make.centerX.mas_equalTo(0);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:13];
    titleLab.textColor = kTextColor_60;
    titleLab.text = @"从相册上传背景";
    [bgView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(addimgView.mas_bottom).with.offset(17);
        make.width.mas_equalTo(stringGetWidth(titleLab.text, titleLab.font.pointSize));
        make.height.mas_equalTo(stringGetHeight(titleLab.text, titleLab.font.pointSize));
    }];
    
    UILabel *messageLab = [UILabel by_init];
    [messageLab setBy_font:12];
    messageLab.textColor = kTextColor_117;
    messageLab.text = messageStr;
    [bgView addSubview:messageLab];
    [messageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(titleLab.mas_bottom).with.offset(5);
        make.width.mas_equalTo(stringGetWidth(messageLab.text, messageLab.font.pointSize));
        make.height.mas_equalTo(stringGetHeight(messageLab.text, messageLab.font.pointSize));
    }];
}

- (void)addRightBarButtonItem{
    UIBarButtonItem *rightBarButtonItem = [UIBarButtonItem initWithTitle:@"提交" target:self action:@selector(rightBarButtonItemAction) type:BYBarButtonItemTypeRight];
    S_VC.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

#pragma mark - 直播间设置默认背景图

- (void)addDefultImgBg{
    UILabel *label = [UILabel by_init];
    label.text = @"自定义背景图";
    [label setBy_font:13];
    label.textColor = kTextColor_77;
    [_scrollView addSubview:label];
    @weakify(self);
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.equalTo(self.imageView.mas_bottom).with.offset(15);
        make.left.mas_equalTo(15);
        make.height.mas_equalTo(stringGetHeight(label.text, 13));
        make.width.mas_equalTo(100);
    }];
    CGFloat imgW = (kCommonScreenWidth - 40)/2;
    CGFloat imgH = imgW*370/750;
    for (int i = 0; i < 3; i ++) {
        for (int j = 0; j < 2; j ++) {
            UIImageView *imageView = [self getSingleDFImageView:2*i + j];
            [_scrollView addSubview:imageView];
            [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(kCellLeftSpace + (imgW + 10)*j);
                make.top.equalTo(label.mas_bottom).with.offset(12 + (imgH + 13)*i);
                make.width.mas_equalTo(imgW);
                make.height.mas_equalTo(imgH);
            }];
            [imageView addCornerRadius:10 size:CGSizeMake(imgW, imgH)];
        }
    }
}

- (UIImageView *)getSingleDFImageView:(NSInteger)index{
    UIImageView *imageView = [[UIImageView alloc] init];
    NSString *imageName = _defultImgs[index];
    imageView.userInteractionEnabled = YES;
    [imageView by_setImageName:imageName];
    
    // 用于点击按钮
    UIButton *customBtn = [UIButton by_buttonWithCustomType];
    customBtn.selected = [K_VC.model.cover_url isEqualToString:imageName] ? YES : NO;
    [customBtn addTarget:self action:@selector(selectDFImgViewAction:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:customBtn];
    [customBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    // 选中的图标
    UIImageView *selectImgView = [[UIImageView alloc] init];
    [selectImgView by_setImageName:@"creatroom_select"];
    selectImgView.hidden = [K_VC.model.cover_url isEqualToString:imageName] ? NO : YES;
    [imageView addSubview:selectImgView];
    [selectImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(3);
        make.right.mas_equalTo(-4);
        make.width.mas_equalTo(selectImgView.image.size.width);
        make.height.mas_equalTo(selectImgView.image.size.height);
    }];
    
    imageView.tag = baseTag + index;
    customBtn.tag = 2*baseTag + index;
    selectImgView.tag = 3*baseTag + index;
    
    return imageView;
}

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
    }
    return _scrollView;
}

@end
