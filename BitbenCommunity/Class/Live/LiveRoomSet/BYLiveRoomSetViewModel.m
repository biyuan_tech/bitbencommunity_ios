//
//  BYLiveRoomSetViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/10.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveRoomSetViewModel.h"
// controller
#import "BYLiveEditBgImgController.h"
#import "BYLiveRoomSetController.h"
// model
#import "BYLiveRoomSetModel.h"
#import "BYCommonLiveModel.h"

#import "BYEditingTextView.h"
// request

#define S_DM ((BYLiveRoomSetModel *)self.dataModel)
#define K_VC ((BYLiveRoomSetController *)self.S_VC)
@interface BYLiveRoomSetViewModel()<BYCommonTableViewDelegate>

@property (nonatomic ,strong) BYCommonTableView *tableView;

@property (nonatomic ,strong) BYEditingTextView *roomTitleEditingView;

@property (nonatomic ,strong) BYEditingTextView *introEditingView;

@end

@implementation BYLiveRoomSetViewModel

- (Class)getDataModelClass{
    return [BYLiveRoomSetModel class];
}

- (void)setContentView{
    [S_V_VIEW addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    _tableView.tableData = [S_DM getTableData:K_VC.model];
}

- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForHeaderInSection:(NSInteger)section{
    return 12;
}

- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [UIView by_init];
    sectionView.backgroundColor = [UIColor clearColor];
    return sectionView;
}

- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0: // 修改直播主题
            {
                [self.roomTitleEditingView showAnimation];
            }
                break;
            case 1: // 修改直播简介
            {
                [self.introEditingView showAnimation];
            }
                break;
            case 2: // 直播间背景图
            {
                BYLiveEditBgImgController *editBgImgController = [[BYLiveEditBgImgController alloc] init];
                editBgImgController.edit_type = EDIT_IMG_TYPE_LIVEROOM;
                editBgImgController.model = K_VC.model;
                editBgImgController.reloadLiveRoomHandel = K_VC.reloadLiveRoomHandel;
                [S_V_NC pushViewController:editBgImgController animated:YES];
            }
                break;
            default:
                break;
        }
    }
}

#pragma mark - requestMethod
- (void)loadRequestUpdateLiveRoom:(NSDictionary *)param cb:(void(^)(void))cb{
    @weakify(self);
    BYToastView *toastView = [BYToastView toastViewPresentLoading];
    [[BYLiveHomeRequest alloc] loadReuqestUpdateLiveRoom:param successBlock:^(id object) {
        @strongify(self);
        [toastView dissmissToastView];
        showToastView(@"修改成功", S_V_VIEW);
        if (cb) {
            cb();
        }
        if (K_VC.reloadLiveRoomHandel) {
            K_VC.reloadLiveRoomHandel();
        }
        self.tableView.tableData = [S_DM getTableData:K_VC.model];
    } faileBlock:^(NSError *error) {
        [toastView dissmissToastView];
        showToastView(@"修改失败", S_V_VIEW);
    }];
}

#pragma mark - initMethod

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.backgroundColor = kBgColor_248;
        _tableView.group_delegate = self;
    }
    return _tableView;
}

- (BYEditingTextView *)roomTitleEditingView{
    if (!_roomTitleEditingView) {
        _roomTitleEditingView = [[BYEditingTextView alloc] initWithFathureView:kCommonWindow editingType:BYEditingTextViewTypeNormal];
        _roomTitleEditingView.textView.text = nullToEmpty(K_VC.model.room_title);
        @weakify(self);
        _roomTitleEditingView.didConfirmHandle = ^(NSString *text) {
            @strongify(self);
            @weakify(self);
            [self loadRequestUpdateLiveRoom:@{@"room_title":text} cb:^{
                @strongify(self);
                K_VC.model.room_title = text;
            }];;
        };
    }
    return _roomTitleEditingView;
}

- (BYEditingTextView *)introEditingView{
    if (!_introEditingView) {
        _introEditingView = [[BYEditingTextView alloc] initWithFathureView:kCommonWindow editingType:BYEditingTextViewTypeIntro];
        _introEditingView.textView.text = nullToEmpty(K_VC.model.room_intro);
        @weakify(self);
        _introEditingView.didConfirmHandle = ^(NSString *text) {
            @strongify(self);
            @weakify(self);
            [self loadRequestUpdateLiveRoom:@{@"room_intro":text} cb:^{
                @strongify(self);
                K_VC.model.room_intro = text;
            }];
        };
    }
    return _introEditingView;
}
@end
