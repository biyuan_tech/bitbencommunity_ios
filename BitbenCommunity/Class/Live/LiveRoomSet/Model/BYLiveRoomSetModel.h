//
//  BYLiveRoomSetModel.h
//  BY
//
//  Created by 黄亮 on 2018/9/10.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonModel.h"

@class BYCommonLiveModel;
@interface BYLiveRoomSetModel : BYCommonModel

- (NSArray *)getTableData:(BYCommonLiveModel *)liveModel;

@end

@interface BYLiveRoomSetCellModel : BYCommonModel

@property (nonatomic ,strong) NSString *rightTitle;

@property (nonatomic ,assign) BOOL showBottomLine;


@end

