//
//  BYNewLiveController.m
//  BY
//
//  Created by 黄亮 on 2018/9/7.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYNewLiveController.h"
#import "BYNewLiveViewModel.h"
#import "BYSIMManager.h"
#import "NetworkAdapter.h"
#import "NetworkAdapter+MemberVIP.h"

@interface BYNewLiveController ()<NetworkAdapterSocketDelegate>

@end

@implementation BYNewLiveController

- (void)dealloc
{
    [[BYSIMManager shareManager] resetScoketStatus];
}

- (Class)getViewModelClass{
    return [BYNewLiveViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    self.hasCancelSocket = NO;
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"新建直播";
    [self.view setBackgroundColor:kBgColor_248];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data{
    [[BYSIMManager shareManager] webSocketReceiveData:type data:data];
}

-(void)webSocketDidConnectedWithSocket:(WebSocketConnection *)webSocketConnection connectedStatus:(SocketConnectType)connectedStatus{
    if (webSocketConnection == [NetworkAdapter sharedAdapter].mainRootSocketConnection) {
        if (connectedStatus == SocketConnectTypeOpen) {
            [[BYSIMManager shareManager] webSocketDidConnectedWithSocket:webSocketConnection];
        }
    }
}

@end
