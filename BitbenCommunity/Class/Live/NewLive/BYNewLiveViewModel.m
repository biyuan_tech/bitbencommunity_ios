//
//  BYNewLiveViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/7.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYNewLiveViewModel.h"
#import "BYILiveRoomController.h"
#import "BYLiveController.h"
#import "BYLiveRoomHomeController.h"
#import "ArticleDetailRootViewController.h"

// view
#import "BYNewLiveFooterView.h"
#import "BYSelectTimePickerView.h"

// model
#import "BYNewLiveModel.h"
#import "BYNewLiveCellModel.h"
#import "BYCommonLiveModel.h"

// request
#import "BYIMManager.h"
#import "BYSIMManager.h"


#define S_DM ((BYNewLiveModel *)self.dataModel)
@interface BYNewLiveViewModel()<BYCommonTableViewDelegate>

@property (nonatomic ,strong) BYCommonTableView *tableView;

@property (nonatomic ,strong) BYNewLiveFooterView *footerView;

/** 时间选择器 */
@property (nonatomic ,strong) BYSelectTimePickerView *timePickerView;

@end

@implementation BYNewLiveViewModel

- (Class)getDataModelClass{
    return [BYNewLiveModel class];
}

- (void)setContentView{
    [S_V_VIEW addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, -kiPhoneX_Mas_buttom(0), 0));
    }];
    // 设置默认不存的直播形式
    S_DM.liveType = -1;
}

#pragma mark - buttonAction
- (void)confirmAction{
    if (!S_DM.liveTheme.length) {
        [S_VC showToastView:@"请填写直播主题"];
        return;
    }
    if (!S_DM.beginTime.length) {
        [S_VC showToastView:@"请选择开始时间"];
        return;
    }
    if ([@(S_DM.liveType) integerValue] == -1) {
        [S_VC showToastView:@"请选择直播形式"];
        return;
    }
    if (S_DM.liveType == BY_NEWLIVE_TYPE_UPLOAD_VIDEO) {
        if (S_VC.tabBarController) {
            ArticleDetailRootViewController *articleVC = [[ArticleDetailRootViewController alloc]init];
            articleVC.transferPageType = ArticleDetailRootViewControllerTypeLive;
            [S_V_NC pushViewController:articleVC animated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                for (UIViewController *viewController in articleVC.navigationController.viewControllers) {
                    // 移除直播创建界面
                    if ([NSStringFromClass([viewController class]) isEqualToString:@"BYNewLiveController"]) {
                        NSMutableArray *tmpViewControllers = [NSMutableArray arrayWithArray:articleVC.navigationController.viewControllers];
                        [tmpViewControllers removeObject:viewController];
                        articleVC.navigationController.viewControllers = tmpViewControllers;
                    }
                }
            });
        }
        else
        {
            [S_VC dismissViewControllerAnimated:NO completion:nil];
            ArticleDetailRootViewController *articleVC = [[ArticleDetailRootViewController alloc]init];
            articleVC.transferPageType = ArticleDetailRootViewControllerTypeLive;
            UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
            [currentController.navigationController pushViewController:articleVC animated:YES];
        }
       
        return;
    }
    // 发起新建直播请求
    [self loadRequestNewLive];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"textField_change"]) {
        S_DM.liveTheme = param[@"text"];
    }
}

- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        [self.timePickerView showAnimation];
    }
}

#pragma mark - requestMethod
- (void)loadRequestNewLive{
//    if (S_DM.liveType == BY_NEWLIVE_TYPE_RECORD_SCREEN ||
//        S_DM.liveType == BY_NEWLIVE_TYPE_UPLOAD_VIDEO) { // S_DM.liveType == BY_NEWLIVE_TYPE_VIDEO ||
//        showToastView(@"暂不支持此类型直播创建", S_V_VIEW);
//        return;
//    }
//    BYToastView *toastView = [BYToastView toastViewPresentLoading];
//    @weakify(self);
//    [[BYLiveHomeRequest alloc] loadRequestNewLive:S_DM.liveTheme beginTime:S_DM.beginTime liveType:S_DM.liveType successBlock:^(id object) {
//        @strongify(self);
//        if (S_DM.liveType == BY_NEWLIVE_TYPE_ILIVE ||
//            S_DM.liveType == BY_NEWLIVE_TYPE_VIDEO ||
//            S_DM.liveType == BY_NEWLIVE_TYPE_AUDIO_PPT ||
//            S_DM.liveType == BY_NEWLIVE_TYPE_AUDIO_LECTURE) {
//            @weakify(self);
//            [self createSIMGroup:object[@"live_record_id"] cb:^(BOOL isSuccess) { // 创建互动直播群聊id
//                @strongify(self);
//                [toastView dissmissToastView];
//                if (isSuccess) {
//                    [self pushLiveController];
//                }
//                else{
//                    showToastView(@"群聊创建失败", S_V_VIEW);
//                }
//            }];
//        }
//    } faileBlock:^(NSError *error) {
//        [toastView dissmissToastView];
//    }];
}

- (void)pushLiveController{
    // push 出来的界面
    if (S_VC.tabBarController) {
        [BYCommonLiveModel shareManager].begin_time = S_DM.beginTime;
        [BYCommonLiveModel shareManager].live_title = S_DM.liveTheme;
        [BYCommonLiveModel shareManager].live_type  = S_DM.liveType;
        BYLiveController *liveController = [[BYLiveController alloc] init];
        [S_V_NC pushViewController:liveController animated:YES];
        return;
    }
    // present 出来的界面
    [S_VC dismissViewControllerAnimated:NO completion:nil];
    BYLiveRoomHomeController *liveRoomHomeController = [[BYLiveRoomHomeController alloc] init];
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    [currentController.navigationController pushViewController:liveRoomHomeController animated:YES];
    [BYCommonLiveModel shareManager].begin_time = S_DM.beginTime;
    [BYCommonLiveModel shareManager].live_title = S_DM.liveTheme;
    [BYCommonLiveModel shareManager].live_type  = S_DM.liveType;
    BYLiveController *liveController = [[BYLiveController alloc] init];
    [liveRoomHomeController.navigationController pushViewController:liveController animated:YES];
}

// 创建互动直播群聊id
//- (void)creatIMGroup:(NSString *)groupId cb:(void(^)())cb{
//    PDLog(@"互动直播聊天室创建groupId:%@",groupId);
//    @weakify(self);
//    [[BYIMManager sharedInstance] createAVChatRoomGroupId:groupId succ:^{
//        @strongify(self);
//        PDLog(@"互动直播群聊id创建成功");
//        [self updateGroupId:groupId recordId:groupId cb:cb];
//    } fail:nil];
//}


- (void)createSIMGroup:(NSString *)record_id cb:(void(^)(BOOL isSuccess))cb{
    @weakify(self);
    [[BYSIMManager shareManager] createRoom:^(NSString * _Nonnull roomId) {
        @strongify(self);
        if (roomId.length) {
            PDLog(@"视频音频直播群聊id创建成功");
            [self updateGroupId:roomId recordId:record_id cb:^{
                if (cb) cb(YES);
            }];
        }
    } fail:^{
        if (cb) cb(NO);
    }];
}

- (void)updateGroupId:(NSString *)group_id recordId:(NSString *)recordId cb:(void(^)())cb{
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveSet:recordId param:@{@"group_id":group_id} successBlock:^(id object) {
        PDLog(@"同步群聊id成功");
        if (cb) cb();
    } faileBlock:nil];
}

#pragma mark - initMethod

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        _tableView.tableFooterView = self.footerView;
        _tableView.tableData = S_DM.tableData;
    }
    return _tableView;
}

- (BYNewLiveFooterView *)footerView{
    if (!_footerView) {
        _footerView = [[BYNewLiveFooterView alloc] initWithFrame:CGRectMake(0, 0, kCommonScreenWidth, 421)];//497 346  421
        @weakify(self);
        _footerView.didSelectNewLiveType = ^(BY_NEWLIVE_TYPE type) {
            @strongify(self);
            S_DM.liveType = type;
        };
        _footerView.didConfirmHandle = ^{
            @strongify(self);
            [self confirmAction];
        };
    }
    return _footerView;
}

- (BYSelectTimePickerView *)timePickerView{
    if (!_timePickerView) {
        _timePickerView = [[BYSelectTimePickerView alloc] initWithFathureView:kCommonWindow];
        @weakify(self);
        _timePickerView.didSelectTimeString = ^(NSString *data,NSString *showTimeData) {
            @strongify(self);
            S_DM.beginTime = data;
            BYNewLiveCellModel *model = self.tableView.tableData[3];
            model.textField_text = showTimeData;
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        };
    }
    return _timePickerView;
}
@end
