//
//  BYSelectTimePickerView.m
//  BY
//
//  Created by 黄亮 on 2018/9/8.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYSelectTimePickerView.h"

@interface BYSelectTimePickerView()<UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic ,weak) UIView *fathureView;

/** 时间选择器 */
@property (nonatomic ,strong) UIPickerView *pickerView;

@property (nonatomic ,strong) UIView *contentView;

/** 遮罩层 */
@property (nonatomic ,strong) UIView *maskView;

@end

static CGFloat conteneViewH = 305;
@implementation BYSelectTimePickerView

- (instancetype)initWithFathureView:(UIView *)fathureView{
    self = [super init];
    if (self) {
        self.fathureView = fathureView;
        self.frame = fathureView.bounds;
        [self setContentView];
    }
    return self;
}

#pragma mark - customMethod
- (void)showAnimation{
    NSAssert(self.fathureView, @"父视图不能为nil");
    // 自动选择到当月的日,时，分上
    [_pickerView selectRow:[NSDate getCurrentDateComponents].day - 1 inComponent:1 animated:NO];
    [_pickerView selectRow:[NSDate getCurrentDateComponents].hour inComponent:2 animated:NO];
    [_pickerView selectRow:[NSDate getCurrentDateComponents].minute inComponent:3 animated:NO];

    [_fathureView addSubview:self];
    self.center = CGPointMake(_fathureView.center.x, _fathureView.center.y);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self setNeedsUpdateConstraints];
        self.maskView.opaque = 1.0f;
        [UIView animateWithDuration:0.2 animations:^{
            [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
            }];
            [self layoutIfNeeded];
        }];
    });
}

- (void)hiddenAnimation{
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.2 animations:^{
        self.maskView.opaque = 0.0f;
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(conteneViewH);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (NSAttributedString *)getPickerAttributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSArray *units = @[@"月",@"日",@"时",@"分"];
    NSString *string;
    NSString *hightlighString;
    if (component == 0) {
        string = [NSString stringWithFormat:@"%02d%@",[[NSDate getSurplusMonth][row] intValue],units[component]];
        hightlighString = [NSString stringWithFormat:@"%02d",[[NSDate getSurplusMonth][row] intValue]];
    }
    else if (component == 1){
        string = [NSString stringWithFormat:@"%02ld%@",(long)row + 1,units[component]];
        hightlighString = [NSString stringWithFormat:@"%02ld",(long)row + 1];
    }
    else{
        string = [NSString stringWithFormat:@"%02ld%@",(long)row,units[component]];
        hightlighString = [NSString stringWithFormat:@"%02ld",(long)row];
    }
    NSAttributedString *highlightAttributedString = [[NSAttributedString alloc] initWithString:hightlighString attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:20],NSForegroundColorAttributeName:kTextColor_39}];
    NSRange rang = [string rangeOfString:highlightAttributedString.string];
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:kTextColor_168}];
    NSRange tagRang = NSMakeRange(0, highlightAttributedString.string.length);
    NSDictionary *tagAttributedStringKey = [highlightAttributedString attributesAtIndex:0 effectiveRange:&tagRang];
    [mutableAttributedString addAttributes:tagAttributedStringKey range:rang];
    return mutableAttributedString;
}

- (NSDate *)setMonth:(NSInteger)month day:(NSInteger)day hour:(NSInteger)hour minute:(NSInteger)minute{
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *components = [calender components:NSCalendarUnitYear fromDate:[NSDate by_date]];
    NSDate *date = [NSDate by_dateFromString:[NSString stringWithFormat:@"%d-%02d-%02d %02d:%02d:00",(int)components.year,(int)month,(int)day,(int)hour,(int)minute] dateformatter:@"yyyy-MM-dd HH:mm:ss"];
    return date;
}

#pragma mark - buttonAction

- (void)cancelBtnAction{
    [self hiddenAnimation];
}

- (void)confirmBtnAction{
    NSInteger month = [[[NSDate getSurplusMonth] objectAtIndex:[_pickerView selectedRowInComponent:0]] integerValue];
    NSInteger day   = [_pickerView selectedRowInComponent:1] + 1;
    NSInteger hour  = [_pickerView selectedRowInComponent:2];
    NSInteger min   = [_pickerView selectedRowInComponent:3];
    NSDate *date = [self setMonth:month day:day hour:hour minute:min];
    NSComparisonResult result = [date compare:[NSDate date]];
    if (result == NSOrderedAscending ||
        result == NSOrderedSame) {
        showToastView(@"选择时间不能低于当前时间", self.fathureView);
        return;
    }
    if (_didSelectTimeString) {
        NSString *string = [NSDate by_stringFromDate:date dateformatter:@"yyyy-MM-dd HH:mm:ss"];
        NSString *showTimeStr = [NSDate by_stringFromDate:date dateformatter:@"yyyy-MM-dd HH:mm"];
//        NSString *showTimeStr = [NSString stringWithFormat:@"%02d-%02d %02d:%02d",(int)month,(int)day,(int)hour,(int)min];
        _didSelectTimeString(string,showTimeStr);
    }
    [self hiddenAnimation];
}

#pragma mark - UIPickerViewDelegate/DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 4;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    switch (component) {
        case 0:
            return [NSDate getSurplusMonth].count;
            break;
        case 1: // 根据月份获取天数
        {
            NSInteger selectMonthIndex = [pickerView selectedRowInComponent:0];
            NSInteger month = [[NSDate  getSurplusMonth][selectMonthIndex] integerValue];
            return [NSDate getDayNumInMonth:month];
        }
            break;
        case 2:
            return 24;
            break;
        case 3:
            return 60;
            break;
            
        default:
            break;
    }
    return 0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component __TVOS_PROHIBITED{
    return (kCommonScreenWidth - 30)/4;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component __TVOS_PROHIBITED{
    return 45;
}

- (nullable NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component NS_AVAILABLE_IOS(6_0) __TVOS_PROHIBITED{
    return [self getPickerAttributedTitleForRow:row forComponent:component];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component __TVOS_PROHIBITED{
    if (component == 0) {
        // 月份更新后需要对应刷新每月显示天数
        [pickerView reloadComponent:1];
    }
}

#pragma mark - initMehtod/configSubView

- (void)setContentView{
    
    [self addSubview:self.maskView];
    [_maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self addSubview:self.contentView];
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth - 30);
        make.height.mas_equalTo(conteneViewH);
        make.centerX.mas_equalTo(0);
        make.bottom.mas_equalTo(conteneViewH);
    }];
    // 绘制半圆角
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, kCommonScreenWidth - 30, conteneViewH) byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = CGRectMake(0, 0, kCommonScreenWidth - 30, conteneViewH);
    maskLayer.path = maskPath.CGPath;
    _contentView.layer.mask = maskLayer;
    
    [_contentView addSubview:self.pickerView];
    [_pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(250);
    }];
    
    // 取消按钮
    UIButton *cancelBtn = [UIButton by_buttonWithCustomType];
    [cancelBtn setBy_attributedTitle:@{@"title":@"取消",NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kTextColor_168} forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(34);
        make.left.mas_equalTo(27);
        make.top.mas_equalTo(11);
        
    }];
    
    UILabel *title = [UILabel by_init];
    title.text = @"选择时间";
    title.textColor = kTextColor_39;
    [title setBy_font:15];
    [_contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(title.text, title.font.pointSize));
        make.height.mas_equalTo(stringGetHeight(title.text, title.font.pointSize));
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(20);
    }];
    
    // 确定按钮
    UIButton *confirmBtn = [UIButton by_buttonWithCustomType];
    [confirmBtn setBy_attributedTitle:@{@"title":@"确定",NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kTextColor_237} forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(confirmBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:confirmBtn];
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(34);
        make.right.mas_equalTo(-27);
        make.top.mas_equalTo(11);
    }];
    
    UIView *lineView = [UIView by_init];
    lineView.backgroundColor = kBgColor_237;
    [_contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.mas_equalTo(0);
        make.top.mas_equalTo(55);
        make.height.mas_equalTo(0.5);
    }];
    
}

- (UIPickerView *)pickerView{
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc] init];
        _pickerView.dataSource = self;
        _pickerView.delegate = self;
    }
    return _pickerView;
}

- (UIView *)contentView{
    if (!_contentView) {
        _contentView = [UIView by_init];
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    return _contentView;
}

- (UIView *)maskView{
    if (!_maskView) {
        _maskView = [UIView by_init];
        _maskView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.47];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenAnimation)];
        [_maskView addGestureRecognizer:tapGestureRecognizer];
    }
    return _maskView;
}

@end
