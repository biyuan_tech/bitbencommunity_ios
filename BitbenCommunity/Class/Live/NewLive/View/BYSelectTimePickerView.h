//
//  BYSelectTimePickerView.h
//  BY
//
//  Created by 黄亮 on 2018/9/8.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BYSelectTimePickerView : UIView

/** 时间选择回调 (data用于计算的data showTimeData用于展示的data) */
@property (nonatomic ,copy) void (^didSelectTimeString)(NSString *data,NSString *showTimeData);

- (instancetype)initWithFathureView:(UIView *)fathureView;

- (void)showAnimation;
@end
