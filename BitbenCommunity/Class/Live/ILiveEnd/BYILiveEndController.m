//
//  BYILiveEndController.m
//  BY
//
//  Created by 黄亮 on 2018/9/15.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveEndController.h"
#import "BYILiveEndViewModel.h"
#import "BYILiveController.h"

@interface BYILiveEndController ()

@end

@implementation BYILiveEndController

- (Class)getViewModelClass{
    return [BYILiveEndViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    self.popGestureRecognizerEnale = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSMutableArray *array = [NSMutableArray arrayWithArray:CURRENT_VC.navigationController.viewControllers];
    [array enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[BYILiveController class]]) {
            [CURRENT_VC.rt_navigationController removeViewController:obj];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end
