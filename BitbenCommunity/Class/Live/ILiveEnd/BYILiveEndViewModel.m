//
//  BYILiveEndViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/15.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveEndViewModel.h"
#import "BYILiveEndController.h"
#import "BYLiveRoomHomeController.h"
#import "BYCommonLiveModel.h"
#import "BYPersonHomeController.h"

#define S_END_VC ((BYILiveEndController *)S_VC)
@interface BYILiveEndViewModel()

@property (nonatomic ,strong) UILabel *watchNumLab;
/** maskImgView */
@property (nonatomic ,strong) PDImageView *maskImgView;

@end

@implementation BYILiveEndViewModel

- (void)backBtnAction{
    if (S_END_VC.isHost) {
        [S_V_NC popToRootViewControllerAnimated:YES];
        return;
    }
    [S_V_NC popViewControllerAnimated:YES];
}

- (void)personHomeAction{
    BYPersonHomeController *personHomController = [[BYPersonHomeController alloc] init];
    personHomController.user_id = S_END_VC.model.user_id;
    [S_V_NC pushViewController:personHomController animated:YES];
}

- (void)setContentView{
    
    [self addBackMaskView];
    [self addHostInfoView];
    if (S_END_VC.isHost) {
        [self addHostShowData];
    }else{
        [self addAudienceShowData];
    }
}

- (void)addBackMaskView{
    [S_V_VIEW addSubview:self.maskImgView];
    [self.maskImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    effectView.frame = CGRectMake(0, 0, S_V_VIEW.bounds.size.width, S_V_VIEW.bounds.size.height);
    effectView.layer.opacity = 0.7;
    [S_V_VIEW addSubview:effectView];
    
    @weakify(self);
    [self.maskImgView uploadHDImageWithURL:S_END_VC.model.head_img callback:^(UIImage *image) {
        @strongify(self);
        self.maskImgView.image = [UIImage boxblurImage:image withBlurNumber:1.0];
    }];
}

- (void)addHostInfoView{
    BYCommonLiveModel *model = S_END_VC.model;
    
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.font = [UIFont systemFontOfSize:22];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"直播已结束";
    [S_V_VIEW addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(titleLab.font.pointSize);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(kSafe_Mas_Top(82));
    }];
    
    // 直播时长
    NSDate *beginDate = [NSDate by_dateFromString:model.real_begin_time dateformatter:K_D_F];
    NSInteger timeDif = [NSDate by_startTime:beginDate endTime:[NSDate by_date]];
    NSInteger days = (int)(timeDif/(3600*24));
    NSInteger hours = (int)((timeDif-days*24*3600)/3600);
    NSInteger minute = (int)(timeDif-days*24*3600-hours*3600)/60;
    NSInteger second = timeDif - days*24*3600 - hours*3600 - minute*60;
    UILabel *liveTimeLab = [UILabel by_init];
    [liveTimeLab setBy_font:12];
    liveTimeLab.textColor = [UIColor colorWithWhite:1 alpha:0.5];
    liveTimeLab.text = [NSString stringWithFormat:@"直播时长：%02ld:%02ld:%02ld", (long)hours, (long)minute, (long)second];;
    [S_V_VIEW addSubview:liveTimeLab];
    [liveTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(liveTimeLab.text, 12));
        make.height.mas_equalTo(stringGetHeight(liveTimeLab.text, 12));
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(kSafe_Mas_Top(116));
    }];
    
    // 头像
    PDImageView *logoImgView = [[PDImageView alloc] init];
    logoImgView.contentMode = UIViewContentModeScaleAspectFill;
    logoImgView.layer.cornerRadius = 38;
    logoImgView.layer.borderColor = [UIColor whiteColor].CGColor;
    logoImgView.layer.borderWidth = 1.0f;
    logoImgView.clipsToBounds = YES;
    [logoImgView uploadHDImageWithURL:model.head_img callback:nil];
    [S_V_VIEW addSubview:logoImgView];
    [logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(76);
        make.top.mas_equalTo(kSafe_Mas_Top(160));
        make.centerX.mas_equalTo(0);
    }];
    
    UILabel *userNameLab = [UILabel by_init];
    [userNameLab setBy_font:16];
    userNameLab.textColor = [UIColor whiteColor];
    userNameLab.text = model.nickname;
    [S_V_VIEW addSubview:userNameLab];
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(model.nickname, 16));
        make.height.mas_equalTo(stringGetHeight(model.nickname, 16));
        make.centerX.mas_equalTo(0);
        make.top.equalTo(logoImgView.mas_bottom).with.offset(18);
    }];
    
    UILabel *idLab = [UILabel by_init];
    [idLab setBy_font:12];
    idLab.textColor = [UIColor colorWithWhite:1 alpha:0.5];
    idLab.text = [NSString stringWithFormat:@"币本直播ID : %@",model.user_id];
    [S_V_VIEW addSubview:idLab];
    [idLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(idLab.text, 12));
        make.height.mas_equalTo(stringGetHeight(idLab.text, 12));
        make.centerX.mas_equalTo(0);
        make.top.equalTo(userNameLab.mas_bottom).with.offset(12);
    }];
    
}

- (void)addHostShowData{
    NSArray *titles = @[@"人气值",@"获得打赏",@"新增粉丝"];
    for (int i = 0; i < 3; i ++) {
        UILabel *titleLab = [UILabel by_init];
        titleLab.font = [UIFont systemFontOfSize:12];
        titleLab.textColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        titleLab.text = titles[i];
        [S_V_VIEW addSubview:titleLab];
        CGFloat centerX = 0;
        switch (i) {
            case 0:
                centerX = -kCommonScreenWidth/4;
                break;
            case 1:
                centerX = 0;
                break;
            default:
                centerX = kCommonScreenWidth/4;
                break;
        }
        [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(402);
            make.width.mas_equalTo(stringGetWidth(titles[i], 12));
            make.height.mas_equalTo(stringGetHeight(titles[i], 12));
            make.centerX.mas_equalTo(centerX);
        }];
        
        NSString *dataStr = @"";
        switch (i) {
            case 0:
                dataStr = [NSString transformIntegerShow:S_END_VC.watchNum];
                break;
            case 1:
                dataStr = [NSString stringWithFormat:@"%.2fBP",S_END_VC.rewardAmount];
                break;
            default:
                dataStr = [NSString transformIntegerShow:S_END_VC.newFansNum];
                break;
        }
        
        UILabel *dataLab = [UILabel by_init];
        dataLab.font = [UIFont systemFontOfSize:17];
        dataLab.textColor = [UIColor whiteColor];
        dataLab.text = dataStr;
        [S_V_VIEW addSubview:dataLab];
        [dataLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(titleLab.mas_top).offset(-10);
            make.width.mas_equalTo(stringGetWidth(dataStr, 17));
            make.height.mas_equalTo(stringGetHeight(dataStr, 17));
            make.centerX.mas_equalTo(titleLab);
        }];
    }
    
    CGFloat bottom = iPhone5 ? - 60 : -110;
    UIButton *backBtn = [UIButton  by_buttonWithCustomType];
    [backBtn setBackgroundColor:[UIColor clearColor]];
    [backBtn setBy_attributedTitle:@{@"title":@"返回",NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    backBtn.layer.cornerRadius = 20.0f;
    backBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    backBtn.layer.borderWidth = 0.5f;
    [S_V_VIEW addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(165);
        make.centerX.mas_equalTo(0);
        make.bottom.mas_equalTo(bottom);
        make.height.mas_equalTo(40);
    }];
    
}

- (void)addAudienceShowData{
    UILabel *titleLab = [UILabel by_init];
    titleLab.font = [UIFont systemFontOfSize:12];
    titleLab.textColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    titleLab.text = @"人气值";
    [S_V_VIEW addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(402);
        make.width.mas_equalTo(stringGetWidth(titleLab.text, 12));
        make.height.mas_equalTo(stringGetHeight(titleLab.text, 12));
        make.centerX.mas_equalTo(0);
    }];
    
    NSString *dataStr = [NSString transformIntegerShow:S_END_VC.watchNum];
    
    UILabel *dataLab = [UILabel by_init];
    dataLab.font = [UIFont systemFontOfSize:17];
    dataLab.textColor = [UIColor whiteColor];
    dataLab.text = dataStr;
    [S_V_VIEW addSubview:dataLab];
    [dataLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(titleLab.mas_top).offset(-10);
        make.width.mas_equalTo(stringGetWidth(dataStr, 17));
        make.height.mas_equalTo(stringGetHeight(dataStr, 17));
        make.centerX.mas_equalTo(titleLab);
    }];
    
    CGFloat width = iPhone5 ? 130 : 150;
    CGFloat bottom = iPhone5 ? - 60 : -110;
    UIButton *homeBtn = [UIButton  by_buttonWithCustomType];
    [homeBtn setBackgroundColor:[UIColor clearColor]];
    [homeBtn setBy_attributedTitle:@{@"title":@"去他首页",NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [homeBtn addTarget:self action:@selector(personHomeAction) forControlEvents:UIControlEventTouchUpInside];
    homeBtn.layer.cornerRadius = 20.0f;
    homeBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    homeBtn.layer.borderWidth = 0.5f;
    [S_V_VIEW addSubview:homeBtn];
    [homeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.centerX.mas_equalTo(-(10 + width/2));
        make.bottom.mas_equalTo(bottom);
        make.height.mas_equalTo(40);
    }];
    
    
    UIButton *backBtn = [UIButton  by_buttonWithCustomType];
    [backBtn setBackgroundColor:[UIColor clearColor]];
    [backBtn setBy_attributedTitle:@{@"title":@"返回",NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    backBtn.layer.cornerRadius = 20.0f;
    backBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    backBtn.layer.borderWidth = 0.5f;
    [S_V_VIEW addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.centerX.mas_equalTo(10 + width/2);
        make.bottom.mas_equalTo(bottom);
        make.height.mas_equalTo(40);
    }];
}

- (PDImageView *)maskImgView{
    if (!_maskImgView) {
        _maskImgView = [[PDImageView alloc] init];
        _maskImgView.clipsToBounds = YES;
        _maskImgView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _maskImgView;
}

@end
