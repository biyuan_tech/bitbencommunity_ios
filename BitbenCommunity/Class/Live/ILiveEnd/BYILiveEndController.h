//
//  BYILiveEndController.h
//  BY
//
//  Created by 黄亮 on 2018/9/15.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonViewController.h"

@class BYCommonLiveModel;
@interface BYILiveEndController : BYCommonViewController

@property (nonatomic ,strong) BYCommonLiveModel *model;
/** isHost */
@property (nonatomic ,assign) BOOL isHost;
/** 获得打赏 */
@property (nonatomic ,assign) CGFloat rewardAmount;
/** 新增粉丝 */
@property (nonatomic ,assign) NSInteger newFansNum;
/** 人气值 */
@property (nonatomic ,assign) NSInteger watchNum;



@end
