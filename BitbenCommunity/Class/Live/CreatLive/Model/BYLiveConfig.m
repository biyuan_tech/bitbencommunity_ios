//
//  BYLiveConfig.m
//  BY
//
//  Created by 黄亮 on 2018/8/14.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveConfig.h"
#import "BYAVMediaUtil.h"
//#import <ILiveSDK/ILiveQualityData.h>
#import <TXLiteAVSDK_Professional/TXLiteAVSDK.h>

@implementation BYLiveConfig


//+ (ILivePushOption *)getHostPushOption{
//    ILiveChannelInfo *info = [[ILiveChannelInfo alloc] init];
//    info.channelName = [NSString stringWithFormat:@"互动直播_%@",[[ILiveLoginManager getInstance] getLoginId]];
//    info.channelDesc = [NSString stringWithFormat:@"互动直播推流"];
//    
//    ILivePushOption *option = [[ILivePushOption alloc] init];
//    option.channelInfo = info;
//    option.encodeType = ILive_ENCODE_HLS;
//    option.recrodFileType = ILive_RECORD_FILE_TYPE_HLS;
//    return option;
//}

+ (void)changeCameraPos{
//    [[ILiveRoomManager getInstance] switchCamera:nil failed:^(NSString *module, int errId, NSString *errMsg) {
//        NSString *errInfo = [NSString stringWithFormat:@"switch camera fail.module=%@,errid=%d,errmsg=%@",module,errId,errMsg];
//        kShowToastView(errInfo, kCommonWindow);
//    }];
    [[TRTCCloud sharedInstance] switchCamera];
}

+ (BOOL)enableMic{
//    ILiveRoomManager *manager = [ILiveRoomManager getInstance];
//    __block BOOL micState = [manager getCurMicState];
//    [manager enableMic:![manager getCurMicState] succ:^{
//        micState = ![manager getCurMicState];
//    } failed:^(NSString *module, int errId, NSString *errMsg) {
//        micState = [manager getCurMicState];
//    }];
//    return micState;
    return NO;
}

+ (BOOL)enableFlashlight{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    BOOL flashlightState = [device isTorchActive];
    if (![device hasTorch]) return NO;
    if (!flashlightState) {
        [device lockForConfiguration:nil];
        [device setTorchMode:AVCaptureTorchModeOn];
        [device unlockForConfiguration];
        return [device isTorchActive];
    }
    else
    {
        [device lockForConfiguration:nil];
        [device setTorchMode:AVCaptureTorchModeOff];
        [device unlockForConfiguration];
        return [device isTorchActive];
    }
    return NO;
}

+ (void)setBeautyLevel:(float)beautyLevel whiteLevel:(float)whiteLevel{
    [[TRTCCloud sharedInstance] setBeautyStyle:TRTCBeautyStyleSmooth beautyLevel:beautyLevel whitenessLevel:whiteLevel ruddinessLevel:6];
}

+ (void)setBeautyLevel:(float)value{
    CGFloat whiteValue = [[[NSUserDefaults standardUserDefaults] objectForKey:kWhiteValue] floatValue];
    [[TRTCCloud sharedInstance] setBeautyStyle:TRTCBeautyStyleSmooth beautyLevel:value whitenessLevel:whiteValue ruddinessLevel:6];
}

+ (void)setWhiteLevel:(float)value{
    CGFloat beantyValue = [[[NSUserDefaults standardUserDefaults] objectForKey:kBeautyValue] floatValue];
    [[TRTCCloud sharedInstance] setBeautyStyle:TRTCBeautyStyleSmooth beautyLevel:beantyValue whitenessLevel:value ruddinessLevel:6];
}

//+ (BOOL)setBeauty:(float)value{
//    if ([BYAVMediaUtil verifyAVAuthorization:BYAVMedioTypeVideo]) {
//        return [[ILiveRoomManager getInstance] setBeauty:value];
//    }
//    return NO;
//}
//
//+ (BOOL)setWhite:(float)value{
//    if ([BYAVMediaUtil verifyAVAuthorization:BYAVMedioTypeVideo]) {
//        return [[ILiveRoomManager getInstance] setWhite:value];
//    }
//    return NO;
//}



- (NSString *)onLogTimer
{
//    QAVContext *context = [[ILiveSDK getInstance] getAVContext];
//    if (context.videoCtrl && context.audioCtrl && context.room)
//    {
//        ILiveQualityData *qualityData = [[ILiveRoomManager getInstance] getQualityData];
//        NSMutableString *paramString = [NSMutableString string];
//        //房间号
//        int roomid = [[ILiveRoomManager getInstance] getRoomId];
//        [paramString appendString:[NSString stringWithFormat:@"直播间号:    %d\n",roomid]];
//
//        //角色
//        NSString *roleStr = self.isHost ? @"主播" : @"非主播";
//        [paramString appendString:[NSString stringWithFormat:@"个人角色:    %@\n",roleStr]];
//
//        //FPS
//        [paramString appendString:[NSString stringWithFormat:@"编码帧率:    %li\n",(long)qualityData.interactiveSceneFPS/10]];
//        //Send Recv
//        [paramString appendString:[NSString stringWithFormat:@"发送码率:    %likbps \n",(long)qualityData.sendRate]];
//        [paramString appendString:[NSString stringWithFormat:@"接收码率:    %likbps\n",(long)qualityData.recvRate]];
//
//        //sendLossRate recvLossRate
//        CGFloat sendLossRate = (CGFloat)qualityData.sendLossRate / (CGFloat)100;
//        CGFloat recvLossRate = (CGFloat)qualityData.recvLossRate / (CGFloat)100;
//        NSString *per = @"%";
//        [paramString appendString:[NSString stringWithFormat:@"发送丢包率:  %.2f%@\n",sendLossRate,per]];
//        [paramString appendString:[NSString stringWithFormat:@"接收丢包率:  %.2f%@.\n",recvLossRate,per]];
//
//        //麦克风
//        NSString *isOpen = [[ILiveRoomManager getInstance] getCurMicState] ? @"打开" : @"关闭";
//        [paramString appendString:[NSString stringWithFormat:@"麦克风状态:  %@\n",isOpen]];
//
//        //扬声器
//        NSString *isOpenSpeaker = [[ILiveRoomManager getInstance] getCurSpeakerState] ? @"打开" : @"关闭";
//        [paramString appendString:[NSString stringWithFormat:@"扬声器状态:  %@\n",isOpenSpeaker]];
//
//        //appcpu syscpu
//        CGFloat appCpuRate = (CGFloat)qualityData.appCPURate / (CGFloat)100;
//        CGFloat sysCpuRate = (CGFloat)qualityData.sysCPURate / (CGFloat)100;
//        [paramString appendString:[NSString stringWithFormat:@"应用CPU占用率:  %.2f%@\n",appCpuRate,per]];
//        [paramString appendString:[NSString stringWithFormat:@"系统CPU占用率:  %.2f%@\n",sysCpuRate,per]];
//
//        [paramString appendString:[NSString stringWithFormat:@"主播视频分辨率:  "]];
//        //分别角色的分辨率
//        NSArray *keys = [_resolutionDic allKeys];
//        for (NSString *key in keys)
//        {
//            QAVFrameDesc *desc = _resolutionDic[key];
//            [paramString appendString:[NSString stringWithFormat:@"%@:%li * %li\n",key,(long)desc.width,(long)desc.height]];
//        }
//
//        //        //采集信息
//        //        NSString *videoParam = [context.videoCtrl getQualityTips];
//        //        NSArray *array = [videoParam componentsSeparatedByString:@"\n"]; //从字符A中分隔成2个元素的数组
//        //        if (array.count > 3)
//        //        {
//        //            NSString *resolution = [array objectAtIndex:2];
//        //            [paramString appendString:[NSString stringWithFormat:@"%@\n",resolution]];
//        //        }
//
//        NSMutableString *versionInfo = [NSMutableString string];
//        NSString *iliveSDKVer = [NSString stringWithFormat:@"ILiveSDK: %@\n",[[ILiveSDK getInstance] getVersion]];
//        [versionInfo appendString:iliveSDKVer];
//        //TXCVideoPreprocessor
////        NSString *filterSDKVer = [NSString stringWithFormat:@"TXCVP:    %@\n",[TXCVideoPreprocessor getVersion]];
////        [versionInfo appendString:filterSDKVer];
//        NSString *imSDKVer = [NSString stringWithFormat:@"IMSDK:    %@\n",[[TIMManager sharedInstance] GetVersion]];
//        [versionInfo appendString:imSDKVer];
//        NSString *avSDKVer = [NSString stringWithFormat:@"AVSDK:    %@\n",[QAVContext getVersion]];
//        [versionInfo appendString:avSDKVer];
//
//
//        [paramString appendString:versionInfo];
//        return paramString;
////        dispatch_async(dispatch_get_main_queue(), ^{
////            ws.paramTextView.text = paramString;
////        });
//    }
    return @"";
}

- (NSMutableDictionary *)resolutionDic{
    if (!_resolutionDic) {
        _resolutionDic = [NSMutableDictionary dictionary];
    }
    return _resolutionDic;
}

@end
