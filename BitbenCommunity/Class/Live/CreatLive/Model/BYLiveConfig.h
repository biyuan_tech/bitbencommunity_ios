//
//  BYLiveConfig.h
//  BY
//
//  Created by 黄亮 on 2018/8/14.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <TILLiveSDK/TILLiveSDK.h>
#import "BYLiveDefine.h"

#define kRoomTimerDuration 30

#define kRole_HostUSD @"USD"  // 超清
#define kRole_HostHD  @"HD"   // 高清
#define kRole_HostSD  @"SD"   // 标清


@interface BYLiveConfig : NSObject

/**
 直播画质 (HD 高清 SD 标清 LD标清)
 */
@property (nonatomic ,copy) NSString *liveQuality;

/**
 房间id
 */
@property (nonatomic ,copy) NSString *roomId;

/**
 聊天室id
 */
@property (nonatomic ,copy) NSString *groupId;

/**
 是否为主播（YES 主播，NO 观众）
 */
@property (nonatomic ,assign) BOOL isHost;

/** 当前是否为点播状态 */
//@property (nonatomic ,assign) BOOL isVOD;

@property (nonatomic ,copy) NSString *video_url;

@property (nonatomic ,strong) NSMutableDictionary *resolutionDic;

// -- 互动直播 ---

/**
 获取主播推流配置

 @return return value description
 */
//+ (ILivePushOption *)getHostPushOption;

/**
 修改摄像头方向
 */
+ (void)changeCameraPos;

/**
 打开/关闭麦克风

 @return YES 打开 NO 关闭
 */
+ (BOOL)enableMic;

/**
 打开/关闭闪光灯

 @return YES 打开 NO 关闭
 */
+ (BOOL)enableFlashlight;

+ (void)setBeautyLevel:(float)beautyLevel whiteLevel:(float)whiteLevel;
/**
 设置美颜（设备支持美颜并摄像头打开）
 
 @param  value 取值范围在0-9之间，0代表关闭美颜
 @return BOOL  设置是否成功
 */
+ (BOOL)setBeauty:(float)value;
+ (void)setBeautyLevel:(float)value;
/**
 设置美白（设备支持美颜并摄像头打开）
 
 @param  value  取值范围在0-9之间，0代表关闭美白
 @return BOOL   设置是否成功
 */
//+ (BOOL)setWhite:(float)value;
//+ (void)setWhiteLevel:(float)value;

/**
 获取直播推拉流的系统信息(debug用)

 @return 系统信息
 */
- (NSString *)onLogTimer;
@end







