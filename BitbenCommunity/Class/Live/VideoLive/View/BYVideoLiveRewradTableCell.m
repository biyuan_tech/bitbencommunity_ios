//
//  BYVideoLiveRewradTableCell.m
//  BibenCommunity
//
//  Created by 随风 on 2019/4/17.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYVideoLiveRewradTableCell.h"

@interface BYVideoLiveRewradTableCell ()

/** title */
@property (nonatomic ,strong) UILabel *rewardLab;
/** maskView */
@property (nonatomic ,strong) UIView *maskView;


@end

@implementation BYVideoLiveRewradTableCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIView *maskView = [UIView by_init];
        maskView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
        maskView.layer.cornerRadius = 4.0f;
        [self.contentView addSubview:maskView];
        self.maskView = maskView;
        [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.centerX.mas_equalTo(0);
            make.width.mas_greaterThanOrEqualTo(0);
            make.height.mas_greaterThanOrEqualTo(0);
        }];
        
        UILabel *rewardLab = [[UILabel alloc] init];
        rewardLab.font = [UIFont systemFontOfSize:12];
        rewardLab.textColor = [UIColor whiteColor];
        rewardLab.textAlignment = NSTextAlignmentCenter;
        rewardLab.numberOfLines = 2;
        [self.contentView addSubview:rewardLab];
        self.rewardLab = rewardLab;
        [rewardLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(maskView).mas_offset(33);
            make.top.mas_equalTo(maskView).mas_offset(7);
            make.width.mas_greaterThanOrEqualTo(0);
            make.height.mas_greaterThanOrEqualTo(0);
        }];
        
        UIImageView *rewardImgView = [[UIImageView alloc] init];
        [rewardImgView by_setImageName:@"microlive_reward_icon"];
        [self.contentView addSubview:rewardImgView];
        [rewardImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(maskView).mas_offset(10);
            make.centerY.mas_equalTo(rewardLab);
            make.width.mas_equalTo(rewardImgView.image.size.width);
            make.height.mas_equalTo(rewardImgView.image.size.height);
        }];
       
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configCellByData:(CDChatMessage)data table:(CDChatListView *)table{
    self.backgroundColor = data.chatConfig.msgContentBackGroundColor;
    
    self.rewardLab.text = data.customData[@"text"];
    NSString *tagStr = [NSString stringWithFormat:@"%.2fBP",[data.customData[@"amount"] floatValue]];
    NSAttributedString *attributedString = [self.rewardLab.text tagKeyWordsStr:tagStr attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:kColorRGBValue(0xffc64d)}];
    self.rewardLab.attributedText = attributedString;
    CGFloat maxWidth = iPhone5 ? kCommonScreenWidth - 70*2 : kCommonScreenWidth - 80*2;
    CGSize size = [self.rewardLab.text getStringSizeWithFont:[UIFont systemFontOfSize:12] maxWidth:maxWidth];
    [self.rewardLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceil(size.width));
        make.height.mas_equalTo(ceil(size.height));
    }];
    
    [self.maskView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceil(size.width) + 43);
        make.height.mas_equalTo(ceil(size.height) + 14);
    }];
    
}

@end
