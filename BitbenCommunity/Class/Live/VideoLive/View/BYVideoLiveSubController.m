//
//  BYVideoLiveSubController.m
//  BibenCommunity
//
//  Created by 随风 on 2019/1/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYVideoLiveSubController.h"

// controller
#import "BYVideoLivePlayController.h" // 直播
#import "BYSIMController.h" // 聊天
#import "BYLiveRoomHomeController.h"
#import "BYHistotyIMViewController.h"
#import "BYPersonHomeController.h"

// view
#import "BYILiveOpeartionView.h"
#import "BYCommonPlayerView.h"
// model
#import "BYCommonLiveModel.h"
// request
#import "BYCommonTool.h"
#import "BYSIMManager.h"


@interface BYVideoLiveSubController ()
{
    // 当前已到开播时间
    BOOL _isTimeLive;
    // 当前是否接收到直播信号
    BOOL _isReceiveLiveSign;
    BOOL _isViewDidLoad;
}
/** 聊天区 */
@property (nonatomic ,strong) BYSIMController *imController;
/** 历史消息 */
@property (nonatomic ,strong) BYHistotyIMViewController *historyImController;
/** headerView */
@property (nonatomic ,strong) UIView *headerView;
/** 直播 */
@property (nonatomic ,strong) BYVideoLivePlayController *livePlayView;
/** 倒计时View */
@property (nonatomic ,strong) UIView *timeHeaderView;
/** 倒计时 */
@property (nonatomic ,strong) UILabel *timerLab;
/** 用户操作 */
@property (nonatomic,strong) BYILiveOpeartionView *opeartionView;
/** 回放播放 */
@property (nonatomic ,strong) BYCommonPlayerView *playerView;
/** 心跳计时 */
@property (nonatomic ,strong) NSTimer *roomTimer;
/** 虚拟pv数 */
@property (nonatomic ,assign) NSInteger virtual_pv;
/** 关注按钮 */
@property (nonatomic ,strong) UIButton *attentionBtn;


@end

@implementation BYVideoLiveSubController

- (void)dealloc
{
    [_playerView resetPlayer];
    [_imController.view removeFromSuperview];
    [_imController removeFromParentViewController];
    [_livePlayView.view removeFromSuperview];
    [_livePlayView removeFromParentViewController];
    _imController = nil;
    _livePlayView = nil;
    _playerView = nil;
}

- (void)destoryTimer{
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
    [[BYCommonTool shareManager] stopTimer];
}

- (void)destorySubView{
    [_playerView resetPlayer];
    [_imController.view removeFromSuperview];
    [_imController removeFromParentViewController];
    [_livePlayView.view removeFromSuperview];
    [_livePlayView removeFromParentViewController];
    _imController = nil;
    _livePlayView = nil;
    _playerView = nil;
}

- (void)viewDidDisappear:(BOOL)animated{
    
}

- (void)setIsHost:(BOOL)isHost{
    _isHost = isHost;
    self.virtual_pv = _isHost ? 0 : -1;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hasCancelSocket = YES;
    _isViewDidLoad = YES;
    if (_model) {
        [self configUI];
    }
    // Do any additional setup after loading the view.
}

- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    if (!model) return;
    if (!_isViewDidLoad) return;
    [self configUI];
}

- (void)configUI{
    // 延迟加载避免界面跳动
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self addHeaderView];
        if (self.model.status == BY_LIVE_STATUS_END ||
            self.model.status == BY_LIVE_STATUS_OVERTIME) {
            [self creatHistoryIM];
        }
        else{
            [self createIMWithNUmber:self.model.group_id];
        }
        if (!_isHost) {
            [self addOpeartionView];
        }
    });
}

- (void)playerViewResume{
    [_playerView resume];
    [_livePlayView.playerView resume];
}

- (void)playerViewPause{
    [_playerView pause];
    [_livePlayView.playerView pause];
}


// 加载当前观看人次（virtual_pv 虚拟人次）
- (void)loadCurrentWatchNum{
    self.model.watch_times = self.virtual_pv;
    [self.livePlayView.playerView setWatchNum:self.virtual_pv];
}

// 开播时间与当前时间比对
- (void)verifyLiveTime{
    NSDate *date = [NSDate by_dateFromString:self.model.begin_time dateformatter:K_D_F];
    NSDate *nowDate = [NSDate by_date];
    NSComparisonResult result = [date compare:nowDate];
    if (result == NSOrderedAscending ||
        result == NSOrderedSame) {
        _isTimeLive = YES;
        [self verifyLiveStatus];
    }
    else // 未到开播时间，倒计时自动开播
    {
        // 直播倒计时
        @weakify(self);
        [[BYCommonTool shareManager] timerCutdown:self.model.begin_time cb:^(NSDictionary * _Nonnull param, BOOL isFinish) {
            @strongify(self);
            if (isFinish) {
                self->_isTimeLive = YES;
                [self verifyLiveStatus];
            }
            else{
                self.timerLab.text = [NSString stringWithFormat:@"直播倒计时 : %02d天 %02d时 %02d分 %02d秒", [param[@"days"] intValue], [param[@"hours"] intValue], [param[@"minute"] intValue], [param[@"second"] intValue]];
                
            }
        }];
    }
}

// 检测当前开播状态
- (void)verifyLiveStatus{
    // 加入直播播放视图
    [self addLivePlayHeaderView];
    // 未到开播时间或未接收到直播信号源则返回
    if (!_isTimeLive || !_isReceiveLiveSign) return;
    if (_timeHeaderView && _isHost) {
        // 移除倒计时
        [_timeHeaderView removeFromSuperview];
        _timeHeaderView = nil;
    }
    if (self.model.status != BY_LIVE_STATUS_LIVING) {
        @weakify(self);
        // 发起心跳包
        [self startHeartBeatTimer];
        // 更新直播状态
        [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_LIVING cb:^{
            @strongify(self);
            self.model.status = BY_LIVE_STATUS_LIVING;
            // 发送开播消息
            [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_RECEIVE_STREAM data:nil suc:nil fail:nil];
            
        }];
    }
}

- (void)reloadAttentionStatus{
    self.attentionBtn.selected = self.model.isAttention;
    UIColor *color = !_model.isAttention ? kColorRGBValue(0xea6441) : kColorRGBValue(0xffffff);
    [_attentionBtn setBackgroundColor:color];
    _attentionBtn.layer.borderColor = !_model.isAttention ? kColorRGBValue(0xea6441).CGColor : kColorRGBValue(0x9b9b9b).CGColor;
}

// 结束直播
- (void)finishLiveAction{
    @weakify(self);
    [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_END cb:^{
        @strongify(self);
        @weakify(self);
        [[BYSIMManager shareManager] sendCustomMessage:BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM data:nil suc:^{
            @strongify(self);
            @weakify(self);
            [[BYSIMManager shareManager] closeRoom:self.model.group_id suc:^{
                @strongify(self);
                showDebugToastView(@"关闭聊天室成功",self.view);
            } fail:^{
                @strongify(self);
                showDebugToastView(@"关闭聊天室失败",self.view);
            }];
        } fail:nil];
        self.model.status = BY_LIVE_STATUS_END;
        UIViewController *viewController = self.viewController.navigationController.viewControllers[1];
        [self.viewController.navigationController popToViewController:viewController animated:YES];
    }];
}

- (void)attentionBtnAction{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:_model.user_id
                                             isAttention:!_attentionBtn.selected
                                            successBlock:^(id object) {
                                                @strongify(self);
                                                self.model.isAttention = !self.attentionBtn.selected;
                                                [self reloadAttentionStatus];
                                            } faileBlock:nil];
}

- (void)userlogoAction{
    BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
    personHomeController.user_id = self.model.user_id;
    [self.viewController.navigationController pushViewController:personHomeController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - request
// 更新直播间状态
- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status cb:(void(^)(void))cb{
    if (!self.model.real_begin_time.length) {
        self.model.real_begin_time = [NSDate by_stringFromDate:[NSDate by_date] dateformatter:K_D_F];
    }
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveStatus:status live_record_id:_model.live_record_id stream_id:nil real_begin_time:self.model.real_begin_time successBlock:^(id object) {
        if (cb) cb();
        PDLog(@"更新直播间状态成功");
    } faileBlock:^(NSError *error) {
        PDLog(@"更新直播间状态失败");
    }];
}

// 开始发送心跳
- (void)startHeartBeatTimer{
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
    _roomTimer = [NSTimer scheduledTimerWithTimeInterval:kRoomTimerDuration target:self selector:@selector(postHeartBeat) userInfo:nil repeats:YES];
    [self postHeartBeat];
}

- (void)postHeartBeat{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestPostHeartBeat:_model.live_record_id
                                             virtual_pv:_isHost ? _virtual_pv : -1
                                           successBlock:^(id object) {
                                               @strongify(self);
                                               if (object && [object[@"content"] isEqualToString:@"success"]) {
                                                   self.virtual_pv = [object[@"virtualPV"] integerValue];
                                                   self.model.count_support = [object[@"count_support"] integerValue];
                                                   [self.opeartionView setSupperNum:self.model.count_support];
                                                   [self loadCurrentWatchNum];
                                               }
                                           } faileBlock:nil];
}

#pragma mark - 创建IM
- (void)addHeaderView{
    if (_headerView) return;
    
    self.headerView = [[UIView alloc] init];
    [self.view addSubview:self.headerView];
    CGFloat height = kCommonScreenWidth*9.0f/16.0f;
    CGFloat headerH = _isHost ? height : height + 48;
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(headerH);
    }];

    if (_model.status == BY_LIVE_STATUS_SOON){
        [self addTimeHeaderView];           // 倒计时
    }else if (_model.status == BY_LIVE_STATUS_LIVING) {
        [self addLivePlayHeaderView];       // 直播
        [self startHeartBeatTimer];
    }else if (_model.status == BY_LIVE_STATUS_END ||
              _model.status == BY_LIVE_STATUS_OVERTIME){
        [self addVideoPlayerHeaderView];    // 回放
    }
    if (!_isHost) {
        [self addHeaderBottomView];
    }
}

// 直播播放
- (void)addLivePlayHeaderView{
    if (_livePlayView) return;
    if (_timeHeaderView) {
        // 移除倒计时
        [_timeHeaderView removeFromSuperview];
        _timeHeaderView = nil;
    }
    self.livePlayView = [[BYVideoLivePlayController alloc] init];
    @weakify(self);
    self.livePlayView.receiveLiveSignHandle = ^(BOOL hasSign) {
        @strongify(self);
        if (!self) return ;
        if (!self.isHost) return;
        self->_isReceiveLiveSign = YES;
        // 校验直播状态
        [self verifyLiveStatus];
    };
    [self.headerView addSubview:self.livePlayView.view];
    [self addChildViewController:self.livePlayView];
    CGFloat height = kCommonScreenWidth*9.0f/16.0f;
    [self.livePlayView.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(height);
    }];
    self.livePlayView.isHost = self.isHost;
    self.livePlayView.model = self.model;
}

- (void)addHeaderBottomView{
    PDImageView *userlogoView = [[PDImageView alloc] init];
    userlogoView.contentMode = UIViewContentModeScaleAspectFill;
    userlogoView.layer.cornerRadius = 16;
    userlogoView.clipsToBounds = YES;
    userlogoView.userInteractionEnabled = YES;
    [self.headerView addSubview:userlogoView];
    userlogoView.style = _model.cert_badge;
    [userlogoView uploadHDImageWithURL:_model.head_img callback:nil];
    [userlogoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(17);
        make.bottom.mas_equalTo(-8);
        make.width.height.mas_equalTo(32);
    }];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userlogoAction)];
    [userlogoView addGestureRecognizer:tapRecognizer];

    
    // 头像背景
    UIImageView *userlogoBg = [[UIImageView alloc] init];
    [userlogoBg by_setImageName:@"common_userlogo_bg"];
    [self.headerView addSubview:userlogoBg];
    [self.headerView insertSubview:userlogoBg belowSubview:userlogoView];
    [userlogoBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(userlogoBg.image.size.width + 2);
        make.height.mas_equalTo(userlogoBg.image.size.height + 2);
        make.centerX.equalTo(userlogoView.mas_centerX).with.offset(0);
        make.centerY.equalTo(userlogoView.mas_centerY).with.offset(0);
    }];
    
    UILabel *userNameLab = [[UILabel alloc] init];
    [userNameLab setBy_font:13];
    userNameLab.textColor = kColorRGBValue(0x2c2c2c);
    userNameLab.text = _model.nickname;
    [self.headerView addSubview:userNameLab];
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogoView.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(userlogoView.mas_centerY).mas_offset(0);
        make.width.mas_lessThanOrEqualTo(kCommonScreenWidth - 196);
        make.height.mas_equalTo(15);
    }];
    
    UIView *achievementView = [UIView by_init];
    [self.headerView addSubview:achievementView];
    [achievementView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userNameLab.mas_right).mas_offset(5);
        make.centerY.equalTo(userNameLab);
        make.width.mas_equalTo(58);
        make.height.mas_equalTo(16);
    }];
    
    NSArray *achievementData = _model.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [achievementView addSubview:imageView];
    }
    
    // 关注按钮
    UIButton *attentionBtn = [UIButton by_buttonWithCustomType];
    NSAttributedString *normalAtt = [[NSAttributedString alloc] initWithString:@"+ 关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xffffff),NSFontAttributeName:[UIFont systemFontOfSize:12]}];
    NSAttributedString *selectAtt = [[NSAttributedString alloc] initWithString:@"已关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xee4944),NSFontAttributeName:[UIFont systemFontOfSize:12]}];
    [attentionBtn setAttributedTitle:normalAtt forState:UIControlStateNormal];
    [attentionBtn setAttributedTitle:selectAtt forState:UIControlStateSelected];
    [attentionBtn addTarget:self action:@selector(attentionBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView addSubview:attentionBtn];
    self.attentionBtn = attentionBtn;
    attentionBtn.layer.cornerRadius = 2;
    attentionBtn.layer.borderWidth = 0.5;
    attentionBtn.selected = _model.isAttention;
    [attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(54);
        make.bottom.mas_equalTo(-11);
    }];
    [self reloadAttentionStatus];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xededed)];
    [self.headerView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
}

// 回放播放
- (void)addVideoPlayerHeaderView{
    if (_playerView) return;
    UIView *fatherView = [UIView by_init];
    [self.headerView addSubview:fatherView];
    CGFloat height = kCommonScreenWidth*9.0f/16.0f;
    [fatherView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(height);
    }];
    self.playerView = [[BYCommonPlayerView alloc] init];
    self.playerView.enableFloatWindow = NO;
    [self.playerView setShowWatchNum:YES];
    self.playerView.fatherView = fatherView;
    [self.playerView playVideoWithUrl:_model.video_url title:@""];
    [self.playerView setWatchNum:_model.watch_times];
//    self.playerView.delegate = self;
}

// 倒计时
- (void)addTimeHeaderView{
    if (_timeHeaderView) return;
    self.timeHeaderView = [[UIView alloc] init];
    [self.headerView addSubview:self.timeHeaderView];
    CGFloat height = kCommonScreenWidth*9.0f/16.0f;
//    CGFloat headerH = _isHost ? height : height + 48;
    [self.timeHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(height);
    }];
    
    PDImageView *coverImgView = [[PDImageView alloc] init];
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.clipsToBounds = YES;
    [coverImgView uploadHDImageWithURL:_model.live_cover_url callback:nil];
    [self.timeHeaderView addSubview:coverImgView];
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    UIView *bottomView = [UIView by_init];
    bottomView.backgroundColor = kColorRGB(50, 50, 50, 0.47);
    [self.timeHeaderView addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(40);
    }];
    
    self.timerLab = [UILabel by_init];
    [self.timerLab setBy_font:13];
    self.timerLab.textColor = kColorRGBValue(0xfefefe);
    [bottomView addSubview:self.timerLab];
    [_timerLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(0);
        make.top.bottom.mas_equalTo(0);
    }];
    [self verifyLiveTime];
}

-(void)createIMWithNUmber:(NSString *)number{
    if (!self.imController) {
        self.imController = [[BYSIMController alloc] init];
        self.imController.isHost = self.isHost;
        self.imController.model = self.model;
        self.imController.superController = self;
        [self.view addSubview:self.imController.view];
        [self addChildViewController:self.imController];
//        CGFloat top = kCommonScreenWidth*9.0f/16.0f;
        @weakify(self);
        [self.imController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.left.bottom.right.mas_equalTo(0);
            make.top.mas_equalTo(self.headerView.mas_bottom).mas_offset(0);
        }];

        [self.view bringSubviewToFront:self.opeartionView];
        self.opeartionView.aConversationController = self.imController;
        
        // 结束直播
        self.imController.didFinishLiveHandle = ^{
            @strongify(self);
            [self finishLiveAction];
        };

        // 接收到开播消息开始接收信号源
        self.imController.didBeginLiveHandle = ^{
            @strongify(self);
            [self.livePlayView playLive];
        };
        
        self.imController.didReceiveLiveEndMsgHandle = ^{
            @strongify(self);
            [self.livePlayView setLiveEndStatus];
        };
        // 聊天室滑动
        self.imController.scrollViewDidScroll = ^(UIScrollView *scrollView) {
            @strongify(self);
            if (self.scrollViewDidScrollView) self.scrollViewDidScrollView(scrollView);
        };
    }
}

- (void)creatHistoryIM{
    if (_historyImController) return;
    self.historyImController = [[BYHistotyIMViewController alloc] init];
    self.historyImController.model = self.model;
    self.historyImController.superController = self;
    @weakify(self);
    self.historyImController.scrollViewDidScrollView = ^(UIScrollView *scrollView) {
        @strongify(self);
        if (self.scrollViewDidScrollView) self.scrollViewDidScrollView(scrollView);
    };
    [self.view addSubview:self.historyImController.view];
    [self addChildViewController:self.historyImController];
//    CGFloat top = kCommonScreenWidth*9.0f/16.0f;
    [self.historyImController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.bottom.right.mas_equalTo(0);
//        make.top.mas_equalTo(top);
        make.top.mas_equalTo(self.headerView.mas_bottom).mas_offset(0);
    }];
}

- (void)addOpeartionView{
    if (_opeartionView) return;
    self.opeartionView = [[BYILiveOpeartionView alloc] initWithIsHost:_isHost target:self];
    _opeartionView.liveId = _model.live_record_id;
    _opeartionView.hostId = _model.user_id;
    _opeartionView.hostName = _model.nickname;
    _opeartionView.aConversationController = self.imController;
    [_opeartionView setSupperNum:_model.count_support];
    [self.view addSubview:_opeartionView];
    [self.view bringSubviewToFront:_opeartionView];
    @weakify(self);
    _opeartionView.didSuppertSucHandle = ^{
        @strongify(self);
        if (self.model.isSupport) return ;
        self.model.isSupport = YES;
        self.model.count_support++;
        [self.opeartionView setSupperNum:self.model.count_support];
    };
    [_opeartionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(53);
        make.height.mas_equalTo(170);
        make.bottom.mas_equalTo(-130);
    }];
}

@end
