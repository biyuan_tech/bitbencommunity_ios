//
//  BYVideoLiveSubController.h
//  BibenCommunity
//
//  Created by 随风 on 2019/1/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYCommonViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class BYCommonLiveModel;
@interface BYVideoLiveSubController : BYCommonViewController

@property (nonatomic,strong,readonly)UIView *headerView;

@property (nonatomic, strong) BYCommonLiveModel *model;
@property (nonatomic, weak) UIViewController *viewController;
@property (nonatomic, assign) BOOL isHost;
@property (nonatomic, copy) void (^scrollViewDidScrollView)(UIScrollView *scrollView);

/** 销毁定时器 */
- (void)destoryTimer;
- (void)destorySubView;
/** 播放器暂停、播放 */
- (void)playerViewResume;
- (void)playerViewPause;

@end

NS_ASSUME_NONNULL_END
