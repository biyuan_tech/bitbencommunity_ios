//
//  BYVideoLivePlayController.m
//  BibenCommunity
//
//  Created by 随风 on 2019/1/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYVideoLivePlayController.h"

@interface BYVideoLivePlayController ()<SuperPlayerDelegate>

/** 播放器View的父视图*/
@property (nonatomic) UIView *playerFatherView;

/** 直播结束标识 */
@property (nonatomic ,strong) UILabel *titleLab;


@end

@implementation BYVideoLivePlayController

- (void)dealloc
{
    [_playerView resetPlayer];
    [_playerView removeFromSuperview];
    _playerView = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hasCancelSocket = YES;
    [self configUI];
    // Do any additional setup after loading the view.
}

- (void)setModel:(BYCommonLiveModel *)model{
    _model = model;
    if (!model) return;
    if (_isHost) { // 主播播放接收信号
        [self playLive];
    }
    else{
        if (model.status != BY_LIVE_STATUS_LIVING) {
            self.titleLab.hidden = NO;
        }
        else{ // 直播状态播放直播
            [self playLive];
        }
    }
}

// 直播播放
- (void)playLive{
    if (_model.play_url.length) {
        self.titleLab.hidden = YES;
        [self.playerView playLiveWithRtmp:_model.play_url];
        [_playerView setShowWatchNum:YES];
        [self.playerView setWatchNum:_model.watch_times];
    }
}

- (void)setLiveEndStatus{
    [_playerView resetPlayer];
    NSString *title = @"直播已经结束";
    self.titleLab.text = title;
    self.titleLab.hidden = NO;
    [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(title, 17));
        make.height.mas_equalTo(stringGetHeight(title, 17));
    }];

}

#pragma mark - SuperPlayerDelegate

- (void)superPlayerBackAction:(SuperPlayerView *)player{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    // 是竖屏时候响应关
    if (orientation == UIInterfaceOrientationPortrait &&
        (self.playerView.state == StatePlaying)) {

    } else {
        [self.playerView resetPlayer];  //非常重要
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)superPlayerDidStart:(SuperPlayerView *)player{
    PDLog(@"开播");
    if (self.receiveLiveSignHandle) {
        self.receiveLiveSignHandle(YES);
    }
}

- (void)superPlayerDidEnd:(SuperPlayerView *)player{
    PDLog(@"%s",__func__);
}

- (void)superPlayerError:(SuperPlayerView *)player errCode:(int)code errMessage:(NSString *)why{
    showDebugToastView(why, kCommonWindow);
    PDLog(@"%s",__func__);
}

#pragma mark - UI

- (void)configUI{
    self.playerFatherView = [[UIView alloc] init];
    self.playerFatherView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.playerFatherView];
    [self.view bringSubviewToFront:self.playerFatherView];
    [self.playerFatherView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(0);
//        make.leading.trailing.mas_equalTo(0);
//        // 这里宽高比16：9,可自定义宽高比
//        make.height.mas_equalTo(self.playerFatherView.mas_width).multipliedBy(9.0f/16.0f);
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    self.playerView.fatherView = self.playerFatherView;
    
    
    NSString *title = @"等待信号源接入...";
    self.titleLab = [UILabel by_init];
    self.titleLab.text = title;
    self.titleLab.hidden = YES;
    self.titleLab.textColor = [UIColor whiteColor];
    [self.titleLab setBy_font:17];
    [self.playerView addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(stringGetWidth(title, 17));
        make.height.mas_equalTo(stringGetHeight(title, 17));
    }];
}

- (BYCommonPlayerView *)playerView {
    if (!_playerView) {
        _playerView = [[BYCommonPlayerView alloc] init];
        _playerView.fatherView = _playerFatherView;
        _playerView.enableFloatWindow = NO;
        _playerView.playerConfig.type = PLAYER_TYPE_LIVE;
        
        // 设置代理
        _playerView.delegate = self;
    }
    return _playerView;
}

@end
