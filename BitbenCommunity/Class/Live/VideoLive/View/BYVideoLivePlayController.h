//
//  BYVideoLivePlayController.h
//  BibenCommunity
//
//  Created by 随风 on 2019/1/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYCommonViewController.h"
#import "BYCommonPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYVideoLivePlayController : BYCommonViewController

@property (nonatomic, strong) BYCommonLiveModel *model;
@property (nonatomic, weak) UIViewController *viewController;
/** 播放器 */
@property (nonatomic ,strong) BYCommonPlayerView *playerView;
/** isHost */
@property (nonatomic ,assign) BOOL isHost;
/** 接收到直播源回调 */
@property (nonatomic ,copy) void (^receiveLiveSignHandle)(BOOL hasSign);

// 结束直播
- (void)setLiveEndStatus;

- (void)playLive;

@end

NS_ASSUME_NONNULL_END
