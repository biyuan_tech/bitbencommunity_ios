//
//  BYVideoLiveViewModel.m
//  BibenCommunity
//
//  Created by 随风 on 2019/1/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYVideoLiveViewModel.h"
#import "ShareRootViewController.h"
#import "BYVideoLiveSubController.h"
#import "BYVideoLiveController.h"
#import "BYPersonHomeController.h"

#import "BYLiveRoomSegmentView.h"
#import "BYILiveIntroView.h"
#import "BYLiveForumView.h"
#import "BYLiveReviewController.h"

@interface BYVideoLiveViewModel ()<BYLiveRoomSegmentViewDelegate>
{
    CGFloat _lastOffsetY;
    BOOL _isRefreshMas;
}

@property (nonatomic ,strong) BYLiveRoomSegmentView *segmentView;
/** 简介 */
@property (nonatomic ,strong) BYILiveIntroView *introView;
/** 直播模块 */
@property (nonatomic,strong) BYVideoLiveSubController *subController;
/** 数据源 */
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 点评 */
//@property (nonatomic ,strong) BYLiveForumView *forumView;
/** 点评 */
@property (nonatomic ,strong) BYLiveReviewController *reviewView;


@end

#define K_VC ((BYVideoLiveController *)self.S_VC)
@implementation BYVideoLiveViewModel

- (void)dealloc
{
    
}

- (void)viewWillAppear{
    [S_V_NC setNavigationBarHidden:NO];
    [_subController playerViewResume];
}

- (void)viewWillDisappear{
    [_subController playerViewPause];
}

- (void)viewControllerPopAction{
    [[BYSIMManager shareManager] leaveRoom:self.model.group_id suc:^{
        showDebugToastView(@"离开房间成功", kCommonWindow);
    } fail:^{
        showDebugToastView(@"离开房间失败", kCommonWindow);
    }];
    [self.subController destoryTimer];
    [self.subController destorySubView];
}

- (void)setContentView{
    @weakify(self);
    [K_VC rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_live_share"] barHltImage:nil action:^{
        @strongify(self);
        [self shareAction];
    }];
    [self loadRequestGetDetail];
}

// 分享
- (void)shareAction{
    if (!self.model.shareMap) return;
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.collection_status = self.model.collection_status;
    shareViewController.transferCopyUrl = self.model.shareMap.share_url;
    @weakify(self);
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        @strongify(self);
        thirdLoginType shareType;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        NSString *mainImgUrl = [PDImageView appendingImgUrl:self.model.shareMap.share_img];
        NSString *imgurl = [mainImgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [ShareSDKManager shareManagerWithType:shareType title:self.model.shareMap.share_title desc:self.model.shareMap.share_intro img:imgurl url:self.model.shareMap.share_url callBack:NULL];
        [ShareSDKManager shareSuccessBack:shareTypeLive block:NULL];
    }];
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithCollectionBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf =weakSelf;
        strongSelf.model.collection_status = !strongSelf.model.collection_status;
    }];
    [shareViewController showInView:S_VC];
}

- (void)showHeaderViewAnimation:(BOOL)isShow{
    _isRefreshMas = YES;
    [S_V_NC setNavigationBarHidden:!isShow animated:YES];
    
    [S_V_VIEW updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        [self.segmentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(isShow ? 0 : -64 + kSafeAreaInsetsTop);
        }];
        [S_V_VIEW layoutIfNeeded];
    }];
}

- (void)subScrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint offset = scrollView.contentOffset;
    
    CGFloat temOffsetY = _lastOffsetY;
    _lastOffsetY = offset.y;
    
    // 在刚刷新布局时不做滑动判断
    if (_isRefreshMas) {
        _isRefreshMas = NO;
        return;
    }
    
    if (temOffsetY >= 80 && self.segmentView.frame.origin.y > (-64 + kSafeAreaInsetsTop)) {
        [self showHeaderViewAnimation:NO];
    }
    else if (offset.y <= 30 && self.segmentView.frame.origin.y <= (-64 + kSafeAreaInsetsTop)){
        [self showHeaderViewAnimation:YES];
    }
}

#pragma mark - BYLiveRoomSegmentViewDelegate
- (void)by_segmentViewWillScrollToIndex:(NSInteger)index{
    [self.subController.view endEditing:YES];
    [self.reviewView.view endEditing:YES];
    [self showHeaderViewAnimation:YES];
}

- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    if (index == 0) {
        return self.introView;
    }
    else if (index == 1){
        return self.subController.view;
    }
//    return self.forumView.view;
    return self.reviewView.view;
}

#pragma mark - request
- (void)loadRequestGetDetail{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLiveDetail:K_VC.live_record_id successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        if (!object) return ;
        self.model = object;
        K_VC.navigationTitle = self.model.live_title;
        self.subController.model = self.model;
        [self.introView reloadData:object];
        if (self.model.status == BY_LIVE_STATUS_END ||
            self.model.status == BY_LIVE_STATUS_OVERTIME) {
            [self addSegmentView];
        }
        else{
            @weakify(self);
            [[BYSIMManager shareManager] joinRoom:self.model.group_id suc:^{
                @strongify(self);
                [self addSegmentView];
                showDebugToastView(@"进入房间成功", kCommonWindow);
            } fail:^{
                showDebugToastView(@"进入房间失败", kCommonWindow);
            }];
        }
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - configUI
- (void)addSegmentView{
    self.segmentView = [[BYLiveRoomSegmentView alloc] initWithTitles:@"简介",@"直播",@"点评", nil];
    _segmentView.delegate = self;
    _segmentView.defultSelIndex = 1;
    [S_V_VIEW addSubview:self.segmentView];
    [_segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (BYILiveIntroView *)introView{
    if (!_introView) {
        _introView = [[BYILiveIntroView alloc] init];
        @weakify(self);
        _introView.tapHeaderImgHandle = ^{
            @strongify(self);
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = self.model.user_id;
            [S_V_NC pushViewController:personHomeController animated:YES];
        };
    }
    return _introView;
}

- (BYVideoLiveSubController *)subController{
    if (!_subController) {
        _subController = [[BYVideoLiveSubController alloc] init];
        _subController.view.backgroundColor = K_VC.isHost ? kColorRGBValue(0xfafafa) : [UIColor whiteColor];
        _subController.viewController = S_VC;
        _subController.isHost = K_VC.isHost;
        [self.viewController addChildViewController:_subController];
        @weakify(self);
        _subController.scrollViewDidScrollView = ^(UIScrollView * _Nonnull scrollView) {
            @strongify(self);
            [self subScrollViewDidScroll:scrollView];
        };
    }
    return _subController;
}

//- (BYLiveForumView *)forumView{
//    if (!_forumView) {
//        _forumView = [[BYLiveForumView alloc] initWithFatureController:nil isReport:NO];
//        _forumView.transferRoomId = K_VC.live_record_id;
//        _forumView.viewController = S_VC;
//        _forumView.needAdaptiveBottom = YES;
//        [S_VC addChildViewController:_forumView];
//    }
//    return _forumView;
//}

- (BYLiveReviewController *)reviewView{
    if (!_reviewView) {
        _reviewView = [[BYLiveReviewController alloc] init];
        _reviewView.theme_id = K_VC.live_record_id;
        [S_VC addChildViewController:_reviewView];
    }
    return _reviewView;
}

@end
