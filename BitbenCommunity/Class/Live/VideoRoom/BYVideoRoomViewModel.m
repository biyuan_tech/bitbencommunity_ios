//
//  BYVideoRoomViewModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYVideoRoomViewModel.h"

#import "BYVideoRoomController.h"
#import "BYVideoPlaySubController.h"
#import "BYPersonHomeController.h"
// View
#import "BYLiveRoomSegmentView.h"
#import "BYVideoRoomHeaderView.h"
#import "BYILiveIntroView.h"

#import "BYCommonLiveModel.h"
#import "BYLiveHomeVideoModel.h"


#define S_VIDEO_VC ((BYVideoRoomController *)S_VC)
@interface BYVideoRoomViewModel()<BYLiveRoomSegmentViewDelegate>

/** 分类选择 */
@property (nonatomic ,strong) BYLiveRoomSegmentView *segmentView;
/** 直播简介 */
@property (nonatomic ,strong) BYILiveIntroView *introView;

@property (nonatomic ,strong) BYVideoPlaySubController *videoPlayController;

/** model */
@property (nonatomic ,strong) BYCommonLiveModel *model;


@end

@implementation BYVideoRoomViewModel

- (void)setContentView{
    [S_V_VIEW addSubview:self.segmentView];
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    [self loadRequestGetDetail];
}

- (void)viewControllerPopAction{
    if (![SuperPlayerWindow sharedInstance].isShowing) {
        [self.videoPlayController.playerView resetPlayer];
    }
}

#pragma mark - BYLiveRoomSegmentViewDelegate

- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    if (index == 0) {
        return self.introView;
    }

    UIView *view = [UIView by_init];
    view.backgroundColor = index == 0 ? [UIColor yellowColor] : (index == 1 ? [UIColor whiteColor] : [UIColor blackColor]);
    if (index == 1) {
        self.videoPlayController = [[BYVideoPlaySubController alloc] init];
        _videoPlayController.superController = S_VC;
        return _videoPlayController.view;
    }
    return view;
}

- (void)by_segmentViewDidScrollToIndex:(NSInteger)index{
    
}

#pragma mark - request
- (void)loadRequestGetDetail{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLiveDetail:S_VIDEO_VC.live_record_id successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        if (!object) return ;
        self.model = object;
        [self.introView reloadData:object];
        self.videoPlayController.videourl = self.model.video_url;
    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - initMethod
- (BYLiveRoomSegmentView *)segmentView{
    if (!_segmentView) {
        _segmentView = [[BYLiveRoomSegmentView alloc] initWithTitles:@"简介",@"直播",@"点评", nil];
        _segmentView.delegate = self;
    }
    return _segmentView;
}

- (BYILiveIntroView *)introView{
    if (!_introView) {
        _introView = [[BYILiveIntroView alloc] init];
        _introView.orginY = 0;
//        [_introView reloadData:S_VIDEO_VC.introModel];
        @weakify(self);
        _introView.tapHeaderImgHandle = ^{
            @strongify(self);
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = S_VIDEO_VC.introModel.user_id;
            [S_V_NC pushViewController:personHomeController animated:YES];
        };
    }
    return _introView;
}
@end
