//
//  BYVideoPlaySubController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYVideoPlaySubController.h"

@interface BYVideoPlaySubController ()<SuperPlayerDelegate>


/** 播放器View的父视图*/
@property (nonatomic) UIView *playerFatherView;

/** 是否播放默认宣传视频 */
@property (nonatomic, assign) BOOL isPlayDefaultVideo;

@end

@implementation BYVideoPlaySubController

- (void)dealloc
{
//    [_playerView resetPlayer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isPlayDefaultVideo = NO;
    self.playerFatherView = [[UIView alloc] init];
    self.playerFatherView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.playerFatherView];
    [self.playerFatherView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(IS_iPhoneX ? 44 : 0);
        make.leading.trailing.mas_equalTo(0);
        // 这里宽高比16：9,可自定义宽高比
        make.height.mas_equalTo(self.playerFatherView.mas_width).multipliedBy(9.0f/16.0f);
    }];
    self.playerView.fatherView = self.playerFatherView;
    
}

- (void)setVideourl:(NSString *)videourl{
    _videourl = videourl;
    [_playerView playVideoWithUrl:_videourl title:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SuperPlayerDelegate
- (void)superPlayerBackAction:(SuperPlayerView *)player{    
    // player加到控制器上，只有一个player时候
    // 状态条的方向旋转的方向,来判断当前屏幕的方向
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    // 是竖屏时候响应关
    if (orientation == UIInterfaceOrientationPortrait &&
        (self.playerView.state == StatePlaying)) {
//        [SuperPlayerWindowShared setSuperPlayer:self.playerView];
//        [SuperPlayerWindowShared show];
//        SuperPlayerWindowShared.backController = self;
    } else {
        [self.playerView resetPlayer];  //非常重要
        SuperPlayerWindowShared.backController = nil;
    }
    [self.superController.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - initMethod/setUI
- (BYCommonPlayerView *)playerView {
    if (!_playerView) {
        _playerView = [[BYCommonPlayerView alloc] init];
        _playerView.fatherView = _playerFatherView;
        _playerView.enableFloatWindow = NO;
        // 设置代理
        _playerView.delegate = self;
    }
    return _playerView;
}

@end
