//
//  BYLiveRoomSegmentView.m
//  BY
//
//  Created by 黄亮 on 2018/9/3.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveRoomSegmentView.h"
#import "BYPPTRoomViewModel.h"

@interface BYLiveRoomSegmentView()<UIScrollViewDelegate>
{
    NSInteger _index;
    CGFloat _beginDragOffsetX;
    CGFloat _loadDF;
}

/** 标题存储，也用于初始化子View */
@property (nonatomic ,strong) NSArray *titles;

/** 存储headerView按钮 */
@property (nonatomic ,strong) NSArray *buttons;

/** headerView */
@property (nonatomic ,strong) UIView *headerView;

/** 选择项跟踪横线 */
@property (nonatomic ,strong) UIView *selLineView;

/** subView width */
@property (nonatomic ,assign) CGFloat sub_width;
@property (nonatomic ,assign) NSInteger lastIndex;

/** contentView,subView容器 */
@property (nonatomic ,strong) UIScrollView *scrollView;

@end

static NSInteger baseTag = 0X530;
@implementation BYLiveRoomSegmentView
- (instancetype)initWithTitles:(NSString *)titles, ... NS_REQUIRES_NIL_TERMINATION{
    NSMutableArray *arrays = [NSMutableArray array];
    va_list argList;
    if (titles.length){
        [arrays addObject:titles];
        va_start(argList, titles);
        id temp;
        while ((temp = va_arg(argList, id))){
            [arrays addObject:temp];
        }
    }
    BYLiveRoomSegmentView *segmentView = [[BYLiveRoomSegmentView alloc] init];
    segmentView.titles = arrays;
    [segmentView setContentView];
    segmentView.defultSelIndex = 1;
    segmentView->_lastIndex = 1;
    return segmentView;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self setNeedsUpdateConstraints];
    @weakify(self);
    [UIView animateWithDuration:0.2 animations:^{
        @strongify(self);
        @weakify(self);
        [self.selLineView mas_updateConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.centerX.equalTo(self.headerView.mas_left).with.offset((self.sub_width)*self.lastIndex + (self.sub_width)/2);
        }];
        [self layoutIfNeeded];
    }];
    if (_scrollView.contentSize.width > 0 && !_loadDF) {
        _loadDF = YES;
        [_scrollView setContentOffset:CGPointMake(kCommonScreenWidth*_defultSelIndex, 0) animated:NO];
    }
}

- (void)setDelegate:(id<BYLiveRoomSegmentViewDelegate>)delegate{
    _delegate = delegate;
    if ([self.delegate respondsToSelector:@selector(by_segmentViewDidScrollToIndex:)]) {
        [self.delegate by_segmentViewDidScrollToIndex:_defultSelIndex];
    }
    UIView *view = [_scrollView viewWithTag:2*baseTag + _defultSelIndex];
    if (view.subviews.count == 0 && view) {
        if ([self.delegate respondsToSelector:@selector(by_segmentViewLoadSubView:)]) {
            UIView *subView = [self.delegate by_segmentViewLoadSubView:_defultSelIndex];
            [view addSubview:subView];
            [view setBackgroundColor:subView.backgroundColor];
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
        }
    }
}

#pragma mark - customMehtod
- (void)didSelectSubViewIndex:(NSInteger)index{
    // 设置高亮
    UIButton *button = [self viewWithTag:baseTag + index];
    for (UIButton *btn in self.buttons) {
        btn.selected = button == btn ? YES : NO;
    }
    [self setNeedsLayout];
    self.lastIndex = index;
}

#pragma mark - buttonAction
- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - baseTag;
    // 如与上次选择一样则返回
    if (index == _lastIndex) return;
    [_scrollView setContentOffset:CGPointMake(kCommonScreenWidth*index, 0) animated:YES];
//    [self didSelectSubViewIndex:index];
}

- (void)setDefultSelIndex:(NSInteger)defultSelIndex{
    _defultSelIndex = defultSelIndex;
    [_scrollView setContentOffset:CGPointMake(kCommonScreenWidth*defultSelIndex, 0) animated:NO];
    [self didSelectSubViewIndex:defultSelIndex];
}

#pragma mark - scrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat indexFloat = (CGFloat)scrollView.contentOffset.x/kCommonScreenWidth;
    NSInteger index = ceil(scrollView.contentOffset.x/kCommonScreenWidth);
    if (indexFloat < index) {
        index = round(scrollView.contentOffset.x/kCommonScreenWidth);
    }
    UIView *view = [_scrollView viewWithTag:2*baseTag + index];
    if (view.subviews.count == 0 && view) {
        if ([self.delegate respondsToSelector:@selector(by_segmentViewLoadSubView:)]) {
            UIView *subView = [self.delegate by_segmentViewLoadSubView:index];
            [view addSubview:subView];
            [view setBackgroundColor:subView.backgroundColor];
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, kSafeAreaInsetsBottom, 0));
            }];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    BOOL scrollToScrollStop = !scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
    if (scrollToScrollStop) {
        [self scrollViewDidEndScroll:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        BOOL dragToDragStop = scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
        if (dragToDragStop) {
            [self scrollViewDidEndScroll:scrollView];
        }
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    BOOL scrollToScrollStop = !scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
    if (scrollToScrollStop) {
        [self scrollViewDidEndScroll:scrollView];
    }
}

- (void)scrollViewDidEndScroll:(UIScrollView *)scrollView{
    NSInteger index = ceil(scrollView.contentOffset.x/kCommonScreenWidth);
    if (_lastIndex == index) return;
    if ([self.delegate respondsToSelector:@selector(by_segmentViewWillScrollToIndex:)]) {
        [self.delegate by_segmentViewWillScrollToIndex:index];
    }
    [self didSelectSubViewIndex:index];
    if ([self.delegate respondsToSelector:@selector(by_segmentViewDidScrollToIndex:)]) {
        [self.delegate by_segmentViewDidScrollToIndex:index];
    }
}

#pragma mark - configSubView Method

- (void)setContentView{
    CGFloat headerHeight = 64;
    UIView *headerView = [self getHeaderView];
    [self addSubview:headerView];
    _headerView = headerView;
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.height.mas_equalTo(headerHeight);
    }];
    
    
    UIScrollView *scrollView = [self getScrollView];
    scrollView.delegate = self;
    [self addSubview:scrollView];
    _scrollView = scrollView;
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(headerHeight, 0, 0, 0));
    }];
    
}

// headerView
- (UIView *)getHeaderView{
    UIView *headerView = [UIView by_init];
    NSMutableArray *arrays = [NSMutableArray array];
    _sub_width = (CGFloat)kCommonScreenWidth/_titles.count;
    for (int i = 0; i < _titles.count; i ++) {
        NSString *title = _titles[i];
        UIButton *button = [UIButton by_buttonWithCustomType];
        NSDictionary *normalDic = @{@"title":title,NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kTextColor_102};
        NSDictionary *highlight = @{@"title":title,NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kTextColor_238};
        [button setBy_attributedTitle:normalDic forState:UIControlStateNormal];
        [button setBy_attributedTitle:highlight forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:button];
        [arrays addObject:button];
        button.tag = baseTag + i;
        @weakify(self);
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.width.mas_equalTo(self ->_sub_width);
            make.top.bottom.mas_equalTo(0);
            make.left.mas_equalTo((self ->_sub_width)*i);
        }];
    }
    self.buttons = arrays;
    
    UIView *selLineView = [UIView by_init];
    selLineView.backgroundColor = kTextColor_238;
    [selLineView layerCornerRadius:1 size:CGSizeMake(16, 2)];
    [headerView addSubview:selLineView];
    _selLineView = selLineView;
    [selLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-2);
        make.height.mas_equalTo(2);
        make.width.mas_equalTo(16);
        make.centerX.equalTo(headerView.mas_left).with.offset(kCommonScreenWidth/2);
    }];
    
    return headerView;
}

// contentView
- (UIScrollView *)getScrollView{
    UIScrollView *scrollView = [UIScrollView by_init];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    for (int i = 0; i < _titles.count; i ++) {
        UIView *view = [UIView by_init];
        [scrollView addSubview:view];
        //        view.backgroundColor = kColorRGB(20*i, 23, 20*i, 1);
        view.tag = 2*baseTag + i;
        @weakify(self);
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.left.mas_equalTo(kCommonScreenWidth*i);
            make.top.mas_equalTo(0);
            make.height.equalTo(scrollView.mas_height).with.offset(0);
            make.width.equalTo(scrollView.mas_width).with.offset(0);
            if (i == self.titles.count - 1) {
                make.right.mas_equalTo(0);
            }
        }];
    }
    return scrollView;
}

@end
