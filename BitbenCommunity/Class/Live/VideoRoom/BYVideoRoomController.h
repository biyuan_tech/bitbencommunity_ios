//
//  BYVideoRoomController.h
//  BibenCommunity
//
//  Created by 随风 on 2018/9/21.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYCommonViewController.h"

@class BYCommonLiveModel;
@interface BYVideoRoomController : BYCommonViewController

/** 简介数据 */
@property (nonatomic ,strong) BYCommonLiveModel *introModel;

/** id */
@property (nonatomic ,copy) NSString *live_record_id;

@end
