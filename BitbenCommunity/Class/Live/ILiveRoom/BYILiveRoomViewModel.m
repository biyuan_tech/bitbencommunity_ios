//
//  BYILiveRoomViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/8/14.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveRoomViewModel.h"
#import "BYILiveRoomSegmentView.h"
//#import "BYILiveRoomSubController.h"
#import "BYILiveVODSubController.h"
#import "BYILiveRoomController.h"
#import "LiveForumRootViewController.h"
#import "BYILiveIntroView.h"
#import "ShareRootViewController.h"
#import "BYPersonHomeController.h"
#import "BYILiveRoomSubControllerV1.h"
#import "BYLiveReviewController.h"


#import "BYCommonLiveModel.h"

#define S_ILIVE_VC ((BYILiveRoomController *)S_VC)
@interface BYILiveRoomViewModel()<BYILiveRoomSegmentViewDelegate>

/** 分类选择器 */
@property (nonatomic ,strong) BYILiveRoomSegmentView *segmentView;
/** 直播显示的view */
//@property (nonatomic ,strong) BYILiveRoomSubController *liveSubController;
/** 直播显示的View */
@property (nonatomic ,strong) BYILiveRoomSubControllerV1 *liveSubController;
/** 回房显示的view */
@property (nonatomic ,strong) BYILiveVODSubController *liveVODSubController;
/** 简介 */
@property (nonatomic ,strong) BYILiveIntroView *introView;
/** 点评 */
@property (nonatomic ,strong) LiveForumRootViewController *forumController;
@property (nonatomic ,strong) BYLiveReviewController *reviewView;

@property (nonatomic ,strong) BYCommonLiveModel *model;
@end

@implementation BYILiveRoomViewModel

- (void)dealloc
{
    [_liveVODSubController removeFromParentViewController];
    [_liveSubController removeFromParentViewController];
    [_liveVODSubController.view removeFromSuperview];
    [_liveSubController.view removeFromSuperview];
    _liveVODSubController = nil;
    _liveSubController = nil;
}

- (void)setContentView{
    
    [self loadRequestGetDetail];
    
    // 设置美颜默认值
    [self setDefultBeautyValue];
    
    
    // 简介
    self.introView = [[BYILiveIntroView alloc] init];
    self.introView.orginY = kNavigationHeight;
    @weakify(self);
    _introView.tapHeaderImgHandle = ^{
        @strongify(self);
        BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
        personHomeController.user_id = self.model.user_id;
        [S_V_NC pushViewController:personHomeController animated:YES];
    };
    
    // 分类选择器
    self.segmentView = [[BYILiveRoomSegmentView alloc] initWithIsHost:_config.isHost titles:@"简介",@"直播",@"点评", nil];
    self.segmentView.defultSelIndex = 1;
    self.segmentView.delegate = self;
    self.segmentView.didBackActionHandle = ^{
        @strongify(self);
        if (self.model.status == BY_LIVE_STATUS_END) {
            [self.liveVODSubController backBtnAction];
        }else{
            [self.liveSubController quitRoom];
        }
    };
    self.segmentView.didShareActionHandle = ^{
        @strongify(self);
        [self shareAction];
    };
    [S_V_VIEW addSubview:self.segmentView];
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
//    _segmentView.scrollView.scrollEnabled = _config.isVOD ? NO : YES;
 
    __weak typeof(self)weakSelf = self;
    [self.segmentView actionSegmentListIndexBlock:^(NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.viewController.view endEditing:YES];
        if (!strongSelf.model) return;
        NSDictionary *params = @{@"直播详情":strongSelf.model.room_id};
        if(index == 0){         // 简介
            [MTAManager event:MTATypeHotLiveJianjie params:params];
        } else if (index == 1){     // 直播
            [MTAManager event:MTATypeHotLiveLive params:params];
        } else if (index == 2){     // 点评
            [MTAManager event:MTATypeHotLiveDianping params:params];
        }
    }];
    
}

// 设置美颜默认值
- (void)setDefultBeautyValue{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kBeautyValue] floatValue] == 0 ||
        [[[NSUserDefaults standardUserDefaults] objectForKey:kWhiteValue] floatValue] == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:@(7) forKey:kBeautyValue];
        [[NSUserDefaults standardUserDefaults] setObject:@(7) forKey:kWhiteValue];
    }
}

// 分享
- (void)shareAction{
    if (!self.model.shareMap) return;
    // 埋点
    NSDictionary *params = @{@"直播详情分享":(self.model.room_id.length?self.model.room_id:@"找不到房间号")};
    [MTAManager event:MTATypeHotLiveShare params:params];
    
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.transferCopyUrl = self.model.shareMap.share_url;
    shareViewController.collection_status = self.model.collection_status;
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithCollectionBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.model.collection_status = !strongSelf.model.collection_status;
    }];
    
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
        } else if ([type isEqualToString:@"sina"]){
                          shareType = thirdLoginTypeWeibo;

        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        NSString *mainImgUrl = [PDImageView appendingImgUrl:weakSelf.model.shareMap.share_img];
        NSString *imgurl = [mainImgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [ShareSDKManager shareManagerWithType:shareType title:weakSelf.model.shareMap.share_title desc:weakSelf.model.shareMap.share_intro img:imgurl url:weakSelf.model.shareMap.share_url callBack:NULL];
        [ShareSDKManager shareSuccessBack:shareTypeLive block:NULL];
    }];
    [shareViewController showInView:S_VC];
}

#pragma mark - BYILiveRoomSegmentViewDelegate
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index{
    switch (index) {
        case 0:
            return self.introView; // 简介
            break;
        case 1:
            return [[UIView alloc] init];
            break;
        default:
//            return self.forumController.view; // 点评
            return self.reviewView.view;
            break;
    }
}

- (void)by_segmentViewDidScrollToIndex:(NSInteger)index{
    [self.reviewView.view endEditing:YES];
    // 埋点
    if (!self.model) return;
    NSDictionary *params = @{@"直播详情":self.model.room_id};
    if(index == 0){         // 简介
        [MTAManager event:MTATypeHotLiveJianjie params:params];
    } else if (index == 1){     // 直播
        [MTAManager event:MTATypeHotLiveLive params:params];
    } else if (index == 2){     // 点评
        [MTAManager event:MTATypeHotLiveDianping params:params];
    }
}


#pragma mark - request
- (void)loadRequestGetDetail{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetLiveDetail:S_ILIVE_VC.live_record_id successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        if (!object) return ;
        self.model = object;
        [self.introView reloadData:self.model];
        if (self.model.status == BY_LIVE_STATUS_END ||
            self.model.status == BY_LIVE_STATUS_OVERTIME) {
            self.liveVODSubController.model = self.model;
            self.segmentView.scrollView.scrollEnabled = NO;
            [self.segmentView replaceLiveSubView:self.liveVODSubController.view bgColor:[UIColor blackColor]];
        }
        else{
            @weakify(self);
            [[BYSIMManager shareManager] joinRoom:self.model.group_id suc:^{
                @strongify(self);
                if (!self) return ;
                TRTCParams *params = [[TRTCParams alloc] init];
                params.sdkAppId = kTRTCSDKAppId;
                params.userId = [AccountModel sharedAccountModel].account_id;
                params.userSig = self.model.userSig.length ?  self.model.userSig : [AccountModel sharedAccountModel].userSig;
                NSMutableString *string = [NSMutableString stringWithString:self.model.live_record_id];
                params.roomId = [[string substringWithRange:NSMakeRange(string.length - 8, 8)] integerValue];
                params.role = self.config.isHost ? TRTCRoleAnchor : TRTCRoleAudience;
                self.liveSubController.param = params;
                self.liveSubController.model = self.model;
                [self.segmentView replaceLiveSubView:self.liveSubController.view bgColor:kColorRGBValue(0xd9d9d9)];
                showDebugToastView(@"进入房间成功", kCommonWindow);
            } fail:^{
                @strongify(self);
                if (!self) return ;
                self.liveSubController.model = self.model;
                [self.segmentView replaceLiveSubView:self.liveSubController.view bgColor:kColorRGBValue(0xd9d9d9)];
                showDebugToastView(@"进入房间失败", kCommonWindow);
            }];
        }

    } faileBlock:^(NSError *error) {
        
    }];
}

#pragma mark - initMethod

- (BYILiveRoomSubControllerV1 *)liveSubController{
    if (!_liveSubController) {
        // 直播界面
        self.liveSubController = [[BYILiveRoomSubControllerV1 alloc] init];
        self.liveSubController.superController = S_VC;
        self.liveSubController.config = self.config;
        self.liveSubController.segmentView = self.segmentView;
//        CGFloat height = kCommonScreenHeight - kiPhoneX_Mas_top(0) - (-kiPhoneX_Mas_buttom(0));
//        self.liveSubController.view.frame = CGRectMake(0, 0, kCommonScreenWidth, kCommonScreenHeight);
        @weakify(self);
        self.liveSubController.didShareActionHandle = ^{
            @strongify(self);
            [self shareAction];
        };
    }
    return _liveSubController;
}

- (BYILiveVODSubController *)liveVODSubController{
    if (!_liveVODSubController) {
        self.liveVODSubController = [[BYILiveVODSubController alloc] init];
        self.liveVODSubController.superController = S_VC;
        self.liveVODSubController.config = self.config;
    }
    return _liveVODSubController;
}

- (LiveForumRootViewController *)forumController{
    if (!_forumController) {
        _forumController = [[LiveForumRootViewController alloc] init];
        _forumController.superController = S_VC;
        _forumController.transferRoomId = S_ILIVE_VC.live_record_id;
        CGFloat height = kCommonScreenHeight - kNavigationHeight;
        _forumController.viewSizeHeight = height;
        _forumController.view.frame = CGRectMake(0, kNavigationHeight, kCommonScreenWidth, height);
    }
    return _forumController;
}

- (BYLiveReviewController *)reviewView{
    if (!_reviewView) {
        _reviewView = [[BYLiveReviewController alloc] init];
        _reviewView.theme_id = S_ILIVE_VC.live_record_id;
        _reviewView.needSafeBottom = YES;
        _reviewView.safeTopY = kNavigationHeight + 23;
        [S_VC addChildViewController:_reviewView];
    }
    return _reviewView;
}
@end
