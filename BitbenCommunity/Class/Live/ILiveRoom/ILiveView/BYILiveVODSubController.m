//
//  BYILiveVODSubController.m
//  BibenCommunity
//
//  Created by 随风 on 2018/9/26.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "BYILiveVODSubController.h"

#import "BYIMDefine.h"
#import "BYLiveConfig.h"
// View
#import "BYILiveChatView.h"
#import "BYILiveRoomBottomView.h"
#import "BYILiveOpeartionView.h"
#import "BYCommonPlayerView.h"
#import "BYCommonLiveModel.h"
#import "BYReportSheetView.h"


@interface BYILiveVODSubController ()<SuperPlayerDelegate>
{
    BOOL _firstLoad;
}
/** 聊天室展示 */
@property (nonatomic ,strong) BYILiveChatView *chatView;
/** 底部footerView */
//@property (nonatomic ,strong) BYILiveRoomBottomView *bottomView;
/** 主播、用户操作台 */
@property (nonatomic ,strong) BYILiveOpeartionView *opeartionView;

@property (nonatomic ,strong) BYToastView *endToastView;

/** 播放器View的父视图*/
@property (nonatomic) UIView *playerFatherView;
/** 是否播放默认宣传视频 */
@property (nonatomic, assign) BOOL isPlayDefaultVideo;
/** 播放器 */
@property (nonatomic ,strong) BYCommonPlayerView *playerView;
/** 主播直播信息 */
@property (nonatomic ,strong) UIView *hostInfoView;
/** 主播头像 */
@property (nonatomic ,strong) PDImageView *logoImgView;
/** 直播时长 */
@property (nonatomic ,strong) UILabel *liveTimerLab;
/** 直播观看人次 */
@property (nonatomic ,strong) UILabel *liveNumLab;
/** 举报 */
@property (nonatomic ,strong) UIImageView *reportMarkView;
/** maskView */
@property (nonatomic ,strong) UIView *reportMaskView;
/** reportView */
@property (nonatomic ,strong) BYReportSheetView *reportSheetView;

@end

@implementation BYILiveVODSubController

- (void)dealloc
{
    [_playerView resetPlayer];
    self.playerView = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)setModel:(BYCommonLiveModel *)model{
    if (!_model) {
        _firstLoad = YES;
    }
    _model = model;
    if (_firstLoad) {
        [self configUI];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reportMaskViewAction{
    [self showReportMarkViewHidden:YES];
}

- (void)showReportMarkViewHidden:(BOOL)hidden{
    [self.view bringSubviewToFront:self.reportMaskView];
    [self.view bringSubviewToFront:self.reportMarkView];
    [UIView animateWithDuration:0.1 animations:^{
        self.reportMarkView.layer.opacity = !hidden;
        self.reportMaskView.layer.opacity = !hidden;
    }];
}

#pragma mark - SuperPlayerDelegate
- (void)superPlayerBackAction:(SuperPlayerView *)player{
    
    // player加到控制器上，只有一个player时候
    // 状态条的方向旋转的方向,来判断当前屏幕的方向
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    // 是竖屏时候响应关
    if (orientation == UIInterfaceOrientationPortrait &&
        (self.playerView.state == StatePlaying)) {
//        [SuperPlayerWindowShared setSuperPlayer:self.playerView];
//        [SuperPlayerWindowShared show];
//        SuperPlayerWindowShared.backController = self;
    } else {
        [self.playerView resetPlayer];  //非常重要
        SuperPlayerWindowShared.backController = nil;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)superPlayerDidStart:(SuperPlayerView *)player{
    NSInteger durMin = player.playDuration / 60;//总秒
    NSInteger durSec = (int)player.playDuration % 60;//总分钟
    self.liveTimerLab.text = [NSString stringWithFormat:@"%02zd:%02zd", durMin, durSec];
}

- (void)superPlayerError:(SuperPlayerView *)player errCode:(int)code errMessage:(NSString *)why{
    
}

- (void)superPlayerReportAction:(SuperPlayerView *)player{
    [self showReportMarkViewHidden:NO];
}

#pragma mark - buttonAction Method
// 退出
- (void)backBtnAction{
    showDebugToastView(@"退出房间成功", kCommonWindow);
    [self.superController.navigationController popViewControllerAnimated:YES];
}

- (void)reportBtnAction:(UIButton *)sender{
    [self showReportMarkViewHidden:YES];
    [self.reportSheetView showAnimation];
}

- (void)configUI{
    
    
    self.isPlayDefaultVideo = NO;
    self.playerFatherView = [[UIView alloc] init];
    self.playerFatherView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.playerFatherView];
    [self.playerFatherView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    self.playerView.fatherView = self.playerFatherView;
    [self.playerView setShowWatchNum:NO];
    [_playerView playVideoWithUrl:_model.video_url];
    [_playerView setWatchNum:_model.watch_times];
//    [_playerView.controlView playerShowControlView];
//    self.bottomView = [BYILiveRoomBottomView initWithIsHost:_config.isHost inView:self.view];
    @weakify(self);

    
    self.chatView = [[BYILiveChatView alloc] initWithModel:self.model];
    [self.view addSubview:self.chatView];
    [_chatView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(0);
        if (self.config.isHost) {
            make.bottom.mas_equalTo(-kSafe_Mas_Bottom(30));
        }
        else {
            make.bottom.mas_equalTo(-kSafe_Mas_Bottom(60));
        }
        make.right.mas_equalTo(-80);
        make.height.mas_equalTo(200);
    }];
    
    self.opeartionView = [[BYILiveOpeartionView alloc] initWithIsHost:_config.isHost target:self];
    _opeartionView.liveId = _model.live_record_id;
    _opeartionView.hostId = _model.user_id;
    _opeartionView.hostName = _model.nickname;
    [_opeartionView setSupperNum:_model.count_support];
    [_opeartionView hiddenReward];
    _opeartionView.didExitLiveBtnHandle = ^{
        @strongify(self);
        [self backBtnAction];
    };
    _opeartionView.didSuppertSucHandle = ^{
        @strongify(self);
        if (self.model.isSupport) return ;
        self.model.isSupport = YES;
        self.model.count_support++;
        [self.opeartionView setSupperNum:self.model.count_support];
    };
    [self.view addSubview:_opeartionView];
    [_opeartionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(53);
        make.height.mas_equalTo(170);
//        make.bottom.mas_equalTo(-130);
        make.bottom.mas_equalTo(-kSafe_Mas_Bottom(215));
    }];

    [self addHostLiveInfoView];
    // 添加举报
    [self addRepeortMarkView];

}

- (void)addHostLiveInfoView{
    if (_hostInfoView) return;
    UIView *infoMaskView = [UIView by_init];
    infoMaskView.layer.cornerRadius = 16;
    infoMaskView.backgroundColor = kColorRGB(75, 75, 75, 0.4);
    [self.view addSubview:infoMaskView];
    _hostInfoView = infoMaskView;
    [infoMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(kSafe_Mas_Top(75));
        make.width.mas_equalTo(96);
        make.height.mas_equalTo(32);
    }];
    // 主播头像
    PDImageView *logoImgView = [PDImageView by_init];
    logoImgView.contentMode = UIViewContentModeScaleAspectFill;
    logoImgView.layer.cornerRadius = 14;
    logoImgView.clipsToBounds = YES;
    [infoMaskView addSubview:logoImgView];
    _logoImgView = logoImgView;
    [logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(28);
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(3);
    }];
//    [logoImgView addCornerRadius:14 size:CGSizeMake(28, 28)];
    [logoImgView uploadHDImageWithURL:_model.head_img callback:nil];
    
    // 主播时长
    UILabel *liveTimerLab = [UILabel by_init];
    [liveTimerLab setBy_font:10];
    liveTimerLab.textColor = [UIColor whiteColor];
    liveTimerLab.text = @"00:00";
    [infoMaskView addSubview:liveTimerLab];
    _liveTimerLab = liveTimerLab;
    [liveTimerLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(4);
        make.left.mas_equalTo(35);
        make.right.mas_equalTo(-5);
        make.height.mas_equalTo(10);
    }];
    
    // 观看人次
    UILabel *liveNumLab = [UILabel by_init];
    [liveNumLab setBy_font:10];
    liveNumLab.textColor = [UIColor whiteColor];
    liveNumLab.text = [NSString stringWithFormat:@"%i人次",(int)_model.watch_times];
    [infoMaskView addSubview:liveNumLab];
    _liveNumLab = liveNumLab;
    [liveNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(liveTimerLab.mas_bottom).with.offset(2);
        make.left.mas_equalTo(35);
        make.height.mas_equalTo(10);
        make.right.mas_equalTo(-5);
    }];
}

- (void)addRepeortMarkView{
    if (_reportMarkView) return;
    
    self.reportSheetView = [BYReportSheetView initReportSheetViewShowInView:kCommonWindow];
    self.reportSheetView.theme_id = self.model.live_record_id;
    self.reportSheetView.theme_type = BY_THEME_TYPE_LIVE;
    self.reportSheetView.user_id = self.model.user_id;
    
    self.reportMaskView = [UIView by_init];
    self.reportMaskView.layer.opacity = 0.0f;
    [self.view addSubview:self.reportMaskView];
    [self.reportMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reportMaskViewAction)];
    [self.reportMaskView addGestureRecognizer:tapGestureRecognizer];
    
    self.reportMarkView = [[UIImageView alloc] init];
    [self.reportMarkView by_setImageName:@"common_report_single"];
    [self.view addSubview:self.reportMarkView];
    self.reportMarkView.userInteractionEnabled = YES;
    self.reportMarkView.layer.shadowColor = kColorRGBValue(0x4e4e4e).CGColor;
    self.reportMarkView.layer.shadowOpacity = 0.2;
    self.reportMarkView.layer.shadowRadius = 25;
    self.reportMarkView.layer.shadowOffset = CGSizeMake(0, -2);
    self.reportMarkView.layer.opacity = 0.0f;
    @weakify(self);
    [self.reportMarkView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(-kSafe_Mas_Bottom(40));
        make.width.mas_equalTo(self.reportMarkView.image.size.width);
        make.height.mas_equalTo(self.reportMarkView.image.size.height);
    }];
    
    UIButton *reportBtn = [UIButton by_buttonWithCustomType];
    [reportBtn setBy_attributedTitle:@{@"title":@"举报",NSForegroundColorAttributeName:kColorRGBValue(0x4f4f4f),NSFontAttributeName:[UIFont systemFontOfSize:13]} forState:UIControlStateNormal];
    [reportBtn addTarget:self action:@selector(reportBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.reportMarkView addSubview:reportBtn];
    [reportBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
}

- (BYCommonPlayerView *)playerView {
    if (!_playerView) {
        _playerView = [[BYCommonPlayerView alloc] init];
        _playerView.fatherView = _playerFatherView;
        _playerView.enableFloatWindow = NO;
        _playerView.playerConfig.type = PLAYER_TYPE_REPORT;
        _playerView.playerConfig.renderMode =  RENDER_MODE_FILL_EDGE;
        // 设置代理
        _playerView.delegate = self;
    }
    return _playerView;
}

@end
