//
//  BYILiveRoomSubControllerV1+TRTCCloudDelegate.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/7/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveRoomSubControllerV1.h"
#import <TXLiteAVSDK_Professional/TXLiteAVSDK.h>
#import "TRTCVideoView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYILiveRoomSubControllerV1 (TRTCCloudDelegate) <TRTCCloudDelegate,TRTCVideoViewDelegate>

@end

NS_ASSUME_NONNULL_END
