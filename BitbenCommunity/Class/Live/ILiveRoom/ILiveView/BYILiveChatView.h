//
//  BYILiveChatView.h
//  BY
//
//  Created by 黄亮 on 2018/8/30.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BYILiveChatView : UIView

/** 在接收到打赏后返回最新的bbt余额 */
@property (nonatomic ,copy) void (^didSurplusBBTHandle)(void);
/** 接收到主播开播通知 */
@property (nonatomic ,copy) void (^didReceiveOpenLiveHandle)(void);
/** 观众接收到上麦回调 */
@property (nonatomic ,copy) void (^didReceiveUpToVideoHandle)(BOOL isUpToVideo);


@property (nonatomic ,assign) BOOL isHost;
@property (nonatomic ,strong) BYCommonLiveModel *model;

- (id)initWithModel:(BYCommonLiveModel *)model;

@end
