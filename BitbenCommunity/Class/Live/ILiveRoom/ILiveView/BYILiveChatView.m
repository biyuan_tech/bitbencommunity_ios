//
//  BYILiveChatView.m
//  BY
//
//  Created by 黄亮 on 2018/8/30.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveChatView.h"
#import "BYIMDefine.h"
#import "BYILiveChatViewCellModel.h"
#import "BYUpVideoCellModel.h"

@interface BYILiveChatView()<BYCommonTableViewDelegate>

@property (nonatomic ,strong) BYCommonTableView *tableView;
/** 页码 */
@property (nonatomic ,assign) NSInteger pageNum;


@end;

@implementation BYILiveChatView

//- (instancetype)init
//{
//    self = [super init];
//    if (self) {
//        self.pageNum = 0;
//        [self setContentView];
//        [self addMessageListener];
//    }
//    return self;
//}

- (id)initWithModel:(BYCommonLiveModel *)model{
    self = [super init];
    if (self) {
        self.model = model;
        self.pageNum = 0;
        [self setContentView];
        [self addMessageListener];
    }
    return self;
}

- (void)addMessageListener{
    // 设置消息回调
    @weakify(self);
    [[BYSIMManager shareManager] setMessageListener:^(NSArray * _Nonnull msgs) {
        @strongify(self);
        [self receiveMessage:msgs];
    }];
}

- (void)verifyMsgData:(NSDictionary *)msgData{
    if ([msgData[@"content_type"] integerValue] == BY_SIM_MSG_TYPE_CUSTOME ||
        [msgData[@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
        NSDictionary *customData;
        if ([msgData[@"content"] isKindOfClass:[NSString class]]) {
            // 现返回的为纯json字符串，需要转两次
            NSData *data = [msgData[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
            customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        }
        else{
            customData = msgData[@"content"];
        }
        
        BY_GUEST_OPERA_TYPE opearType = [customData[kBYCustomMessageType] integerValue];
        switch (opearType) {
            case BY_GUEST_OPERA_TYPE_REWARD: // 打赏消息
            {
                if (_isHost) {
                    // 校验打赏消息并刷新当前bbt数额
                    if (self.didSurplusBBTHandle) self.didSurplusBBTHandle();
                }
            }
                break;
            case BY_GUEST_OPERA_TYPE_RECEIVE_STREAM: // 开播消息
                if (self.didReceiveOpenLiveHandle) self.didReceiveOpenLiveHandle();
                break;
            case BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM: // 关播消息
                [[NSNotificationCenter defaultCenter] postNotificationName:NSNotification_stopPushStream object:nil];
                break;
            case BY_GUEST_OPERA_TYPE_UP_VIDEO: // 请求上麦消息
            {
                if (_isHost) {
                    BYUpVideoCellModel *model = [[BYUpVideoCellModel alloc] init];
                    model.userlogoUrl = nullToEmpty(msgData[@"head_img"]);
                    model.userName = nullToEmpty(msgData[@"nickname"]);
                    model.liveRole = LIVE_GUEST;
                    model.userId = msgData[@"user_id"];
                    model.cellString = @"BYUpVideoCell";
                    model.cellHeight = 57.0f;
                    // 接收上麦请求
                    [[BYIMManager sharedInstance] receiveUpToVideoRequest:model];
                }
            }
                break;
            case BY_GUEST_OPERA_TYPE_AGREE_UP_VIDEO: // 同意上麦消息
            {
                if (!_isHost) {
                    if ([[AccountModel sharedAccountModel].account_id isEqualToString:customData[@"user_id"]]) {
                        // 上麦
                        [self upToVideo];
                    }
                }
            }
                break;
            case BY_GUEST_OPERA_TYPE_DOWN_VIDEO: // 请求下麦消息
            {
                if (!_isHost) {
                    if ([[AccountModel sharedAccountModel].account_id isEqualToString:customData[@"user_id"]]) {
                        // 下麦
                        [self downToVideo];
                    }
                }
            }
                break;
            default:
                break;
        }
    }
}

// 上麦
- (void)upToVideo{
    if (self.didReceiveUpToVideoHandle) {
        self.didReceiveUpToVideoHandle(YES);
    }
}

// 下麦
- (void)downToVideo{
    if (self.didReceiveUpToVideoHandle) {
        self.didReceiveUpToVideoHandle(NO);
    }
}


#pragma mark - MessageListener
- (void)receiveMessage:(NSArray *)msgs{
    if (!msgs.count) return;
    NSDictionary *dic = msgs[0];
    if (!dic) return;
    [self verifyMsgData:dic];
//    [self verifyRewardMsg:msgs];
    NSMutableArray *tableData = [NSMutableArray arrayWithArray:self.tableView.tableData];
    // 消息转换为model
    NSArray *msgArr = [BYILiveChatViewCellModel transformSIMTableData:msgs];
    [tableData addObjectsFromArray:msgArr];
    self.tableView.tableData = tableData;
    if (self.tableView.tableData.count) {
        NSIndexPath *indexPath=[NSIndexPath indexPathForRow:self.tableView.tableData.count-1 inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

#pragma mark - BYCommonTableViewDelegate

#pragma mark - request
- (void)loadRequestGetHistoryIM{
    NSString *sendTime;
    if (self.model.status == BY_LIVE_STATUS_END ||
        self.model.status == BY_LIVE_STATUS_OVERTIME) {
        sendTime = [NSDate getCurrentTimeStr];
    }else{
        sendTime = [BYSIMManager shareManager].joinTime;
    }
    
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetPageChatHistory:self.model.group_id pageNum:self.pageNum sendTime:sendTime successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        [self.tableView endRefreshing];
        if (![object[@"content"] isKindOfClass:[NSArray class]]) return ;
        if (![object[@"content"] count]) return;
        if (self.pageNum == 0) {
            NSArray *array = [BYILiveChatViewCellModel transformSIMTableData:object[@"content"]];
            // 数据倒序
            array = [[array reverseObjectEnumerator] allObjects];
            self.tableView.tableData = array;
            [self.tableView reloadData];
            if (self.tableView.tableData.count) {
                NSIndexPath *indexPath=[NSIndexPath indexPathForRow:self.tableView.tableData.count-1 inSection:0];
                [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            }
        }
        else{
            NSMutableArray *data = [NSMutableArray array];
            NSArray *array = [BYILiveChatViewCellModel transformSIMTableData:object[@"content"]];
            array = [[array reverseObjectEnumerator] allObjects];
            [data addObjectsFromArray:array];
            [data addObjectsFromArray:self.tableView.tableData];
            self.tableView.tableData = data;
            [self.tableView reloadData];
        }
        self.pageNum ++;
        
    } faileBlock:^(NSError *error) {
        @strongify(self);
        if (!self) return ;
        [self.tableView endRefreshing];
    }];
}

#pragma mark - configUI Method
- (void)setContentView{
    [self addSubview:self.tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self loadRequestGetHistoryIM];
}

- (BYCommonTableView *)tableView{
    if (!_tableView) {
        _tableView = [[BYCommonTableView alloc] init];
        _tableView.group_delegate = self;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView addHeaderRefreshTarget:self action:@selector(loadRequestGetHistoryIM)];
    }
    return _tableView;
}
@end
