//
//  BYILiveRoomSubControllerV1.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/7/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYILiveRoomSubControllerV1.h"
#import "BYIMDefine.h"
// Controller
#import "BYILiveEndController.h"

// View
#import "BYILiveChatView.h"
#import "BYILiveRoomBottomView.h"
#import "BYILiveOpeartionView.h"
#import "BYReportSheetView.h"
#import "TRTCVideoView.h"
#import "TRTCVideoViewLayout.h"

// model、request
#import "BYCommonLiveModel.h"

@interface BYILiveRoomSubControllerV1 ()<TRTCCloudDelegate,TRTCVideoViewDelegate>
{
    BOOL _firstLoad;
    BOOL _isReadyLive;
}

@property (nonatomic, retain) TRTCCloud *trtc;               //TRTC SDK 实例对象
/** 聊天室展示 */
@property (nonatomic ,strong) BYILiveChatView *chatView;
/** 心跳计时 */
@property (nonatomic ,strong) NSTimer *roomTimer;
/** 底部footerView */
@property (nonatomic ,strong) BYILiveRoomBottomView *bottomView;

@property (nonatomic ,strong) BYToastView *toastView;
@property (nonatomic ,strong) BYToastView *endToastView;
/** 直播倒计时view */
@property (nonatomic ,strong) UIView *timerView;
@property (nonatomic ,strong) UILabel *timerLab;
@property (nonatomic ,strong) NSTimer *logTimer;
@property (nonatomic ,strong) UITextView *textView;
/** 主播头像 */
@property (nonatomic ,strong) PDImageView *logoImgView;
/** 直播时长 */
@property (nonatomic ,strong) UILabel *liveTimerLab;
/** 直播观看人次 */
@property (nonatomic ,strong) UILabel *liveNumLab;
/** bbt数量 */
@property (nonatomic ,strong) UILabel *bbtNumLab;
/** 虚拟pv数 */
@property (nonatomic ,assign) NSInteger virtual_pv;
/** 主播直播信息 */
@property (nonatomic ,strong) UIView *hostInfoView;
@property (nonatomic ,strong) UIView *bbtMaskView;
/** 举报 */
@property (nonatomic ,strong) UIImageView *reportMarkView;
/** maskView */
@property (nonatomic ,strong) UIView *reportMaskView;
/** reportView */
@property (nonatomic ,strong) BYReportSheetView *reportSheetView;
/** 关注按钮 */
@property (nonatomic ,strong) UIButton *attentionBtn;
/** 本地画面的View */
@property (nonatomic, strong) TRTCVideoView *localView;
@property (nonatomic ,strong) UIView *holderView;
@property (nonatomic ,strong) TRTCVideoViewLayout *layoutEngine;

@end

@implementation BYILiveRoomSubControllerV1

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_bottomView removeFromSuperview];
    [_chatView removeFromSuperview];
    [_opeartionView removeFromSuperview];
    [_reportSheetView removeFromSuperview];
    self.bottomView = nil;
    self.chatView = nil;
    self.opeartionView = nil;
    self.reportSheetView = nil;
    [[BYCommonTool shareManager] stopTimer];
    [TRTCCloud destroySharedIntance];
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)setModel:(BYCommonLiveModel *)model{
    if (!_model) {
        _firstLoad = YES;
    }
    _model = model;
    if (_firstLoad) {
        _firstLoad = NO;
        [self configAll];
    }
}
/**
 * 防止iOS锁屏：如果视频通话进行中，则方式iPhone进入锁屏状态
 */
- (void)setRoomStatus:(TRTCStatus)roomStatus {
    _roomStatus = roomStatus;
    switch (_roomStatus) {
        case TRTC_IDLE:
            [self exitRoomSuc];
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
            break;
        case TRTC_ENTERED:
            [self enterRoomSuc];
            [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
            break;
        default:
            break;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hasCancelSocket = YES;
    _isReadyLive = NO;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configAll{
    if (!_trtc) {
        _trtc = [TRTCCloud sharedInstance];
        [_trtc setDelegate:self];
    }
    [self configUI];
//    [self enterRoom];
    [self verifyLiveTime];
    [self addNSNotification];
    if (_config.isHost) { // 开播时间校验
        _virtual_pv = 0;
        [self loadRequestGetSurplusBBT];
    }
    else{
        // 虚拟pv设-1,后端区分主播，观众端
        _virtual_pv = -1;
    }
}

- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveStopPushStream_notic) name:NSNotification_stopPushStream object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidEnterBackGround:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

// 当前观众观看的主播状态为直播时刷新
- (void)reloadGuestLivingStatus:(BOOL)isLiving{
    if (!isLiving) {
        [_hostInfoView removeFromSuperview];
        if (_roomTimer) {
            [_roomTimer invalidate];
            _roomTimer = nil;
        }
        [[BYCommonTool shareManager] stopTimer];
        return;
    }
    if (self.model.status == BY_LIVE_STATUS_LIVING) {
        [self addHostLiveInfoView];
        // 开始直播计时
        [self startLiveTimer];
        // 开始发送心跳包
        [self startHeartBeatTimer];
    }
}

// 开播时间与当前时间比对
- (void)verifyLiveTime{
    NSDate *date = [NSDate by_dateFromString:self.model.begin_time dateformatter:K_D_F];
    NSDate *nowDate = [NSDate by_date];
    NSComparisonResult result = [date compare:nowDate];
    if (result == NSOrderedAscending ||
        result == NSOrderedSame) {
        [self openLiveAction];
        [self.view setBackgroundColor:[UIColor whiteColor]];
        [self.segmentView setLiveSubViewBgColor:[UIColor blackColor]];
    }
    else // 未到开播时间，倒计时自动开播
    {
        self.timerView.layer.opacity = 1.0f;
        [self.view setBackgroundColor:[UIColor whiteColor]];
        @weakify(self);
        [[BYCommonTool shareManager] timerCutdown:self.model.begin_time cb:^(NSDictionary * _Nonnull param, BOOL isFinish) {
            @strongify(self);
            if (isFinish) {
                [self openLiveAction];
                self.timerView.layer.opacity = 0.0f;
                [self.segmentView setLiveSubViewBgColor:[UIColor blackColor]];
            }
            else{
                self.timerLab.text = [NSString stringWithFormat:@"%02d : %02d : %02d : %02d", [param[@"days"] intValue], [param[@"hours"] intValue], [param[@"minute"] intValue], [param[@"second"] intValue]];
            }
        }];
    }
}

// 直播时长
- (void)startLiveTimer{
    @weakify(self);
    [[BYCommonTool shareManager] timerBeginTime:self.model.real_begin_time cb:^(NSDictionary * _Nonnull param) {
        @strongify(self);
        int hour = [param[@"hours"] intValue];
        int min = [param[@"minute"] intValue];
        int second = [param[@"second"] intValue];
        self.liveTimerLab.text = [NSString stringWithFormat:@"%02d:%02d",min + hour*60,second];
    }];
}

#pragma mark - 心跳(房间保活)

// 开始发送心跳
- (void)startHeartBeatTimer{
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
    _roomTimer = [NSTimer scheduledTimerWithTimeInterval:kRoomTimerDuration target:self selector:@selector(postHeartBeat) userInfo:nil repeats:YES];
    [self postHeartBeat];
}

#pragma mark - customMethod

// 获取直播测试信息
- (void)setLogInfo{
    NSString *string = [self.config onLogTimer];
    NSLog(@"%@",string);
    dispatch_async(dispatch_get_main_queue(), ^{
        self.textView.text = string;
    });
}

// 进入直播房间
- (void)enterRoom{
    // 大画面的编码器参数设置
    // 设置视频编码参数，包括分辨率、帧率、码率等等，这些编码参数来自于 TRTCSettingViewController 的设置
    // 注意（1）：不要在码率很低的情况下设置很高的分辨率，会出现较大的马赛克
    // 注意（2）：不要设置超过25FPS以上的帧率，因为电影才使用24FPS，我们一般推荐15FPS，这样能将更多的码率分配给画质
    TRTCVideoEncParam* encParam = [TRTCVideoEncParam new];
    encParam.videoResolution = VIDEO_RESOLUTION_720_1280;
    encParam.videoBitrate = 1150;
    encParam.videoFps = 15;
    encParam.resMode = TRTCVideoResolutionModePortrait;
    
    TRTCNetworkQosParam * qosParam = [TRTCNetworkQosParam new];
    qosParam.preference = TRTCVideoQosPreferenceClear;
    qosParam.controlMode = TRTCQosControlModeServer;
    [_trtc setNetworkQosParam:qosParam];
    
    //小画面的编码器参数设置
    //TRTC SDK 支持大小两路画面的同时编码和传输，这样网速不理想的用户可以选择观看小画面
    //注意：iPhone & Android 不要开启大小双路画面，非常浪费流量，大小路画面适合 Windows 和 MAC 这样的有线网络环境
    TRTCVideoEncParam* smallVideoConfig = [TRTCVideoEncParam new];
    smallVideoConfig.videoResolution = TRTCVideoResolution_160_90;
    smallVideoConfig.videoFps = 15;
    smallVideoConfig.videoBitrate = 100;
    
    [_trtc setLocalViewFillMode:TRTCVideoFillMode_Fill];
    [_trtc setGSensorMode:TRTCGSensorMode_UIFixLayout];
    
    [_trtc setPriorRemoteVideoStreamType:TRTCVideoStreamTypeBig];
    [_trtc setAudioRoute:TRTCAudioModeSpeakerphone];
    [_trtc enableAudioVolumeEvaluation:300];
    
//    if (_param.role == TRTCRoleAudience) {
//        [_trtc stopLocalAudio];
//    } else {
//        [_trtc startLocalAudio];
//    }
    
    [self startPreview];
    
    [_trtc setVideoEncoderParam:encParam];
    [_trtc enableEncSmallVideoStream:NO withQuality:smallVideoConfig];
    // 进入音频房间
    [_trtc enterRoom:_param appScene:TRTCAppSceneLIVE];
}
// 成功进入直播房间
- (void)enterRoomSuc{
    if (_config.isHost) {
        if (!_isReadyLive) return;
        CGFloat beautyValue = [[[NSUserDefaults standardUserDefaults] objectForKey:kBeautyValue] floatValue];
        CGFloat whiteValue = [[[NSUserDefaults standardUserDefaults] objectForKey:kWhiteValue] floatValue];
        [BYLiveConfig setBeautyLevel:beautyValue whiteLevel:whiteValue];
        if (self.model.status != 2) {
            // 重置播放起始时间
            [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_LIVING cb:NULL];
            
        }
        // 开始直播计时
        [self startLiveTimer];
        // 开始发送心跳包
        [self startHeartBeatTimer];
        // 发送开播通知
        [self sendOpenLiveNotic];
        showDebugToastView(@"创建互动房间成功", self.view);
    }else{
        [self reloadGuestLivingStatus:YES];
    }
}

- (void)exitRoomSuc{
    // 观众退出房间
    if (!_config.isHost) {
        showDebugToastView(@"退出房间成功", kCommonWindow);
        [self.superController.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    // 主播退出房间
    // 角色为主播未开播退出时
    if (_model.status != BY_LIVE_STATUS_LIVING) {
        [self.superController.navigationController popViewControllerAnimated:YES];
        return;
    }
    // 向当前还存在的连麦观众发送下麦消息
    [[BYIMManager sharedInstance] sendStopPushStreamMessage:^{
        // 清空上麦成员
        [[BYIMManager sharedInstance] removeAllUpToVideoMember];
    } fail:nil];
    
    // 计时器置空
    [self.roomTimer invalidate];
    self.roomTimer = nil;
    [self loadRequestUpdateLiveStatus:BY_LIVE_STATUS_END cb:^{
      
    }];
    @weakify(self);
    [[BYSIMManager shareManager] closeRoom:self.model.group_id suc:^{
        @strongify(self);
        showDebugToastView(@"关闭聊天室成功",self.view);
    } fail:^{
        @strongify(self);
        showDebugToastView(@"关闭聊天室失败",self.view);
    }];
    
    BYILiveEndController *liveEndController = [[BYILiveEndController alloc] init];
    liveEndController.model = self.model;
    [self.superController.navigationController pushViewController:liveEndController animated:YES];
}

// 退出直播间
- (void)quitRoom{
    [[TRTCCloud sharedInstance] stopAllRemoteView];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_bottomView removeFromSuperview];
    [_chatView removeFromSuperview];
    [_opeartionView removeFromSuperview];
    [_reportSheetView removeFromSuperview];
    self.bottomView = nil;
    self.chatView = nil;
    self.opeartionView = nil;
    self.reportSheetView = nil;
    [[BYCommonTool shareManager] stopTimer];
    if (_roomTimer) {
        [_roomTimer invalidate];
        _roomTimer = nil;
    }
    [self backBtnAction];
}

- (void)sendOpenLiveNotic{
    // 向当前聊天室发送开播通知
    [[BYIMManager sharedInstance] sendOpenLiveMessage:nil fail:nil];
}

- (void)reportMaskViewAction{
    [self showReportMarkViewHidden:YES];
}

- (void)showReportMarkViewHidden:(BOOL)hidden{
    [self.superController.view bringSubviewToFront:self.reportMaskView];
    [self.superController.view bringSubviewToFront:self.reportMarkView];
    [UIView animateWithDuration:0.1 animations:^{
        self.reportMarkView.layer.opacity = !hidden;
        self.reportMaskView.layer.opacity = !hidden;
    }];
}

- (void)reloadAttentionStatus{
    self.attentionBtn.selected = self.model.isAttention;
    UIColor *color = !_model.isAttention ? kColorRGBValue(0xea6441) : kColorRGBValue(0xffffff);
    [_attentionBtn setBackgroundColor:color];
}

#pragma mark - Notification

- (void)receiveStopPushStream_notic{
    [self showAlertView:@"直播已经结束" message:nil];
    [_trtc stopAllRemoteView];
    [self reloadGuestLivingStatus:NO];
}

- (void)onAppWillResignActive:(NSNotification *)notification {
    if (_trtc != nil) {
        ;
    }
}

- (void)onAppDidBecomeActive:(NSNotification *)notification {
    if (_trtc != nil) {
        ;
    }
}

- (void)onAppDidEnterBackGround:(NSNotification *)notification {
    if (_trtc != nil) {
        ;
    }
}

- (void)onAppWillEnterForeground:(NSNotification *)notification {
    if (_trtc != nil) {
        ;
    }
}

#pragma mark - buttonAction Method

// 退出
- (void)backBtnAction{
    if (_model.status == BY_LIVE_STATUS_LIVING) {
        [_trtc exitRoom];
    }else{
        [self exitRoomSuc];
    }
}

// 切换摄像头
- (void)swichCameraBtnAction{
    // 切换摄像头
    [BYLiveConfig changeCameraPos];
}

// 查看直播信息log
- (void)infoBtnAction:(UIButton *)sender{
    if (!sender.selected) {
        self.textView = [[UITextView alloc] init];
        self.textView.editable = NO;
        [self.textView setFont:[UIFont systemFontOfSize:17]];
        [self.textView setTextColor:kColorRGBValue(0xed4a45)];
        [self.textView setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:self.textView];
        [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(kCommonScreenWidth);
            make.centerX.bottom.mas_equalTo(0);
            make.top.mas_equalTo(50);
        }];
        
        self.logTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(setLogInfo) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.logTimer forMode:NSDefaultRunLoopMode];
    }
    else
    {
        [self.textView removeFromSuperview];
        self.textView = nil;
        if (self.logTimer) {
            [self.logTimer invalidate];
            self.logTimer = nil;
        }
    }
    sender.selected = !sender.selected;
}

// 举报
- (void)reportBtnAction:(UIButton *)sender{
    [self showReportMarkViewHidden:YES];
    [self.reportSheetView showAnimation];
}

// 申请连麦
- (void)upVideoBtnAction:(UIButton *)sender{
    // 发送上麦请求
    [self showReportMarkViewHidden:YES];
    [[BYIMManager sharedInstance] sendUpVideoMessage:nil fail:nil];
}

- (void)receiveAgreeUpToVideo:(BOOL)isUpToVideo{
    if (isUpToVideo) { // 上麦
        _localView = [TRTCVideoView newVideoViewWithType:VideoViewType_Remote userId:_param.userId];
        _localView.delegate = self;
        [_trtc switchRole:TRTCRoleAnchor];
        _param.role = TRTCRoleAnchor;
        [self startPreview];
    }else{             // 下麦
        _localView = nil;
        [_trtc switchRole:TRTCRoleAudience];
        _param.role = TRTCRoleAudience;
        [self stopPreview];
    }
}

// 开始直播
- (void)openLiveAction{
    _isReadyLive = YES;
    [self enterRoom];
}

- (void)attentionBtnAction{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:_model.user_id
                                             isAttention:!_attentionBtn.selected
                                            successBlock:^(id object) {
                                                @strongify(self);
                                                self.model.isAttention = !self.attentionBtn.selected;
                                                [self reloadAttentionStatus];
                                            } faileBlock:nil];
}

// 加载当前观看人次（virtual_pv 虚拟人次）
- (void)loadCurrentWatchNum:(NSInteger)memberCount{
//    if (self.virtual_pv >= 500) {
//        // 获取当前直播真实人数
//        self.model.watch_times = 500 + memberCount;
//        self.liveNumLab.text = [NSString stringWithFormat:@"%d人次",(int)(500 + memberCount)];
//    }
//    else
//    {
        self.model.watch_times = self.virtual_pv;
        self.liveNumLab.text = [NSString stringWithFormat:@"%d人次",(int)self.virtual_pv];
//    }
}

- (void)updateRenderViewAnimation{
    
}

- (void)updateCloudMixtureParams
{
    if (!_config.isHost) return;

    int videoWidth  = 720;
    int videoHeight = 1280;

    // 小画面宽高
    int subWidth  = 90;
    int subHeight = 137;

    int offsetX = 15;
    int offsetY = 63;

    int bitrate = 200;

    videoWidth  = 720;
    videoHeight = 1280;
    subWidth    = 138;
    subHeight   = 240;
    bitrate     = 1500;

    TRTCTranscodingConfig* config = [TRTCTranscodingConfig new];
    config.appId = kTRTCAppId;
    config.bizId = kTRTCBizid;
    config.videoWidth = videoWidth;
    config.videoHeight = videoHeight;
    config.videoGOP = 2;
    config.videoFramerate = 15;
    config.videoBitrate = bitrate;
    config.audioSampleRate = 48000;
    config.audioBitrate = 64;
    config.audioChannels = 2;

    // 设置混流后主播的画面位置
    TRTCMixUser* broadCaster = [TRTCMixUser new];
    broadCaster.userId = _model.user_id; // 以主播uid为broadcaster为例
    broadCaster.zOrder = 0;
    broadCaster.rect = CGRectMake(0, 0, videoWidth, videoHeight);
    broadCaster.roomID = nil;
    broadCaster.streamType = TRTCVideoStreamTypeBig;
    
    NSMutableArray* mixUsers = [NSMutableArray new];
    [mixUsers addObject:broadCaster];

    // 设置混流后各个小画面的位置
    int index = 0;
    NSMutableDictionary *upToVideoDic = [NSMutableDictionary dictionary];
    [upToVideoDic setValuesForKeysWithDictionary:_remoteViewDic];
    [upToVideoDic removeObjectForKey:_model.user_id];
    for (NSString* userId in upToVideoDic.allKeys) {
        TRTCMixUser* audience = [TRTCMixUser new];
        audience.userId = userId;
        audience.zOrder = index + 1;
        audience.streamType = TRTCVideoStreamTypeSmall;
        //辅流判断：辅流的Id为原userId + "-sub"
        if ([userId hasSuffix:@"-sub"]) {
            NSArray* spritStrs = [userId componentsSeparatedByString:@"-"];
            if (spritStrs.count < 2)
                continue;
            NSString* realUserId = spritStrs[0];
            if (![_remoteViewDic.allKeys containsObject:realUserId])
                return;
            audience.userId = realUserId;
            audience.streamType = TRTCVideoStreamTypeSub;
        }
        if (index < 3) {
            // 前三个小画面靠右从下往上铺
            audience.rect = CGRectMake(videoWidth - offsetX - subWidth, videoHeight - offsetY - index * subHeight - subHeight, subWidth, subHeight);
        } else if (index < 6) {
            // 后三个小画面靠左从下往上铺
            audience.rect = CGRectMake(offsetX, videoHeight - offsetY - (index - 3) * subHeight - subHeight, subWidth, subHeight);
        } else {
            // 最多只叠加六个小画面
        }
        
        [mixUsers addObject:audience];
        ++index;
    }
    config.mixUsers = mixUsers;
    [_trtc setMixTranscodingConfig:config];
}

/**
 * 视频窗口排布函数，此处代码用于调整界面上数个视频画面的大小和位置
 */
- (void)relayout {
    NSMutableArray *views = @[].mutableCopy;
    TRTCVideoView *playerView = [_remoteViewDic objectForKey:_mainPreviewId];
    if (playerView) { // 为主播端画面
        [views addObject:playerView];
    }

    for (id userID in _remoteViewDic) {
        TRTCVideoView *playerView = [_remoteViewDic objectForKey:userID];
//        if ([_mainPreviewId isEqualToString:userID]) {
//            _localView = playerView;
//        }
        if (![userID isEqualToString:_mainPreviewId]) {
            playerView.enableMove = NO;
            [views addObject:playerView];
        }
    }
    
    [_layoutEngine relayout:views];
    
    // 刷新顶踩赞位置
    if (!self.config.isHost) {
        CGFloat bottom = views.count <= 1 ? -kSafe_Mas_Bottom(130) : (views.count >= 2 ? -kSafe_Mas_Bottom(215) : -kSafe_Mas_Bottom(364));
        [UIView animateWithDuration:0.2 animations:^{
            [self.opeartionView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(bottom);
            }];
            [self.view layoutIfNeeded];
        }];
    }
}

#pragma mark - requestMethod
// 心跳包请求
- (void)postHeartBeat{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestPostHeartBeat:_model.live_record_id
                                             virtual_pv:_config.isHost ? _virtual_pv : -1
                                           successBlock:^(id object) {
                                               @strongify(self);
                                               if (object && [object[@"content"] isEqualToString:@"success"]) {
                                                   self.virtual_pv = [object[@"virtualPV"] integerValue];
                                                   self.model.count_support = [object[@"count_support"] integerValue];
                                                   [self.opeartionView setSupperNum:self.model.count_support];
                                                   [self loadCurrentWatchNum:0];
                                               }
                                           } faileBlock:nil];
}

// 更新直播间状态
- (void)loadRequestUpdateLiveStatus:(BY_LIVE_STATUS)status cb:(void(^)(void))cb{
    //    NSString *stream_id = status == BY_LIVE_STATUS_END ? self.stream_id : nil;
    if (!self.model.real_begin_time.length) {
        self.model.real_begin_time = [NSDate by_stringFromDate:[NSDate by_date] dateformatter:K_D_F];
        //        [[BYLiveHomeRequest alloc] loadRequestUpdateLiveSet:self.model.live_record_id param:@{@"real_begin_time":self.model.real_begin_time} successBlock:nil faileBlock:nil];
    }
    NSString *roomId = stringFormatInteger(_param.roomId);
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestUpdateLiveStatus:status live_record_id:self.model.live_record_id stream_id:roomId real_begin_time:self.model.real_begin_time successBlock:^(id object) {
        @strongify(self);
        if (cb) cb();
        self.model.status = status;
        PDLog(@"更新直播间状态成功");
    } faileBlock:^(NSError *error) {
        if (cb) cb();
        PDLog(@"更新直播间状态失败");
    }];
}

// 获取bbt余额
- (void)loadRequestGetSurplusBBT{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestSurplusBBTSuccessBlock:^(id object) {
        @strongify(self);
        self.bbtNumLab.text = [NSString stringWithFormat:@"%d",(int)[object[@"balance"] floatValue]];
    } faileBlock:nil];
}

#pragma mark - startPreview
- (void)startPreview
{
    //开摄像头
    //视频通话默认开摄像头。直播模式主播才开摄像头
    if (_param.role == TRTCRoleAnchor) {
        // 开启屏幕分享
        [_trtc startLocalPreview:YES view:_localView];
        // 开启音频上行
        [_trtc startLocalAudio];
        [_remoteViewDic setObject:_localView forKey:_param.userId];
    }
    [self relayout];
}

- (void)stopPreview
{
    // 关闭屏幕分享
    [_trtc stopLocalPreview];
    // 关闭音频上行
    [_trtc stopLocalAudio];
    [_remoteViewDic removeObjectForKey:_param.userId];
    [self relayout];
}

- (void)stopLocalPreview{
    [self receiveAgreeUpToVideo:NO];
}

#pragma mark - initMethod/configUI

// 初始化ui
- (void)configUI{
    _remoteViewDic = [[NSMutableDictionary alloc] init];
    _mainPreviewId = @"";
    
    [self addLocalPreView];
    
    if (_config.isHost) {
        [self addHostLiveInfoView];
    }
    else{
        if (_model.status == BY_LIVE_STATUS_LIVING) {
            [self addHostLiveInfoView];
        }
    }
    
    if (!_config.isHost) {
        [self.view addSubview:self.bottomView];
        @weakify(self);
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.height.mas_equalTo(54);
            make.left.right.mas_equalTo(0);
            if (self.model.status == BY_LIVE_STATUS_END ||
                self.model.status == BY_LIVE_STATUS_OVERTIME) {
                make.bottom.mas_equalTo(-kSafe_Mas_Bottom(45));
            }else{
                make.bottom.mas_equalTo(-kSafe_Mas_Bottom(5));
            }
        }];
        [self addRepeortMarkView];
    }
    
    [self.view addSubview:self.chatView];
    @weakify(self);
    [_chatView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(0);
        if (self.config.isHost) {
            make.bottom.mas_equalTo(-kSafe_Mas_Bottom(30));
        }
        else {
            make.bottom.equalTo(self.bottomView.mas_top).with.offset(-20);
        }
        make.right.mas_equalTo(-80);
        make.height.mas_equalTo(250);
    }];
    
    // 添加倒计时view
    [self addTimerView];
    
    self.opeartionView = [[BYILiveOpeartionView alloc] initWithIsHost:_config.isHost target:self];
    _opeartionView.liveId = _model.live_record_id;
    _opeartionView.hostId = _model.user_id;
    _opeartionView.hostName = _model.nickname;
    _opeartionView.model = _model;
    [_opeartionView setSupperNum:_model.count_support];
    _opeartionView.didExitLiveBtnHandle = ^{
        @strongify(self);
        [self quitRoom];
        //        [self backBtnAction];
    };
    _opeartionView.didShareActionHandle = ^{
        @strongify(self);
        if (self.didShareActionHandle) {
            self.didShareActionHandle();
        }
    };
    _opeartionView.didSuppertSucHandle = ^{
        @strongify(self);
        if (self.model.isSupport) return ;
        self.model.isSupport = YES;
        self.model.count_support++;
        [self.opeartionView setSupperNum:self.model.count_support];
    };
    _opeartionView.didChangeBeautyHandle = ^(CGFloat beauty, CGFloat white) {
        [BYLiveConfig setBeautyLevel:beauty*9 whiteLevel:white*9];
        [[NSUserDefaults standardUserDefaults] setObject:@(beauty*9) forKey:kBeautyValue];
        [[NSUserDefaults standardUserDefaults] setObject:@(white*9) forKey:kWhiteValue];
    };
    [self.view addSubview:_opeartionView];
    [_opeartionView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        if (self.config.isHost) {
            make.width.mas_equalTo(33);
            make.height.mas_equalTo(268);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(kSafe_Mas_Top(0));
        }
        else
        {
            make.right.mas_equalTo(0);
            make.width.mas_equalTo(53);
            make.height.mas_equalTo(170);
            make.bottom.mas_equalTo(-kSafe_Mas_Bottom(130));
        }
    }];
}

- (void)addLocalPreView{
    // 本地预览view
    if (_config.isHost) {
        _localView = [TRTCVideoView newVideoViewWithType:VideoViewType_Local userId:_model.user_id];
        _localView.delegate = self;
        [_localView setBackgroundColor:[UIColor whiteColor]];
    }

    // 默认大画面未主播端
    _mainPreviewId = _model.user_id;
    
    _holderView = [[UIView alloc] initWithFrame:self.view.bounds];
    [_holderView setBackgroundColor:[UIColor whiteColor]];
    [self.view insertSubview:_holderView atIndex:0];
    
    _layoutEngine = [[TRTCVideoViewLayout alloc] init];
    _layoutEngine.view = _holderView;
    
//    [self relayout];
}

// 添加倒计时view
- (void)addTimerView{
    if (_timerLab) return;
    self.timerView = [UIView by_init];
    self.timerView.backgroundColor = [UIColor clearColor];
    self.timerView.layer.opacity = 0.0f;
    [self.view addSubview:self.timerView];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setTextColor:kColorRGBValue(0x292929)];
    [titleLab setBy_font:15];
    titleLab.text = @"直播倒计时";
    [self.timerView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(titleLab.text, 15));
        make.height.mas_equalTo(stringGetHeight(titleLab.text, 15));
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    
    _timerLab = [UILabel by_init];
    _timerLab.font = [UIFont systemFontOfSize:30];
    _timerLab.textColor = kColorRGBValue(0x292929);
    _timerLab.text = @"00 : 00 : 00 : 00";
    _timerLab.textAlignment = NSTextAlignmentCenter;
    //    self.timerLab.hidden = YES;
    [self.timerView addSubview:_timerLab];
    @weakify(self);
    [_timerLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(kCommonScreenWidth);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(stringGetHeight(self.timerLab.text, 30));
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(25);
    }];
    
    UILabel *unitLab = [UILabel by_init];
    unitLab.textColor = kColorRGBValue(0x292929);
    [unitLab setBy_font:11];
    unitLab.text = @"天               时               分               秒";
    [self.timerView addSubview:unitLab];
    [unitLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(stringGetWidth(unitLab.text, 11));
        make.height.mas_equalTo(stringGetHeight(unitLab.text, 11));
        make.top.mas_equalTo(self.timerLab.mas_bottom).mas_offset(0);
        make.centerX.mas_equalTo(0);
    }];
    
    [self.timerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth);
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(-50);
        make.top.mas_equalTo(titleLab.mas_top).mas_offset(0);
        make.bottom.mas_equalTo(unitLab.mas_bottom).mas_offset(0);
    }];
}

- (void)addHostLiveInfoView{
    if (_hostInfoView) return;
    UIView *infoMaskView = [UIView by_init];
    infoMaskView.layer.cornerRadius = 16;
    infoMaskView.backgroundColor = kColorRGB(75, 75, 75, 0.4);
    [self.view addSubview:infoMaskView];
    _hostInfoView = infoMaskView;
    CGFloat width = _config.isHost ? 96 : 140;
    [infoMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(kSafe_Mas_Top(75));
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(32);
    }];
    // 主播头像
    PDImageView *logoImgView = [PDImageView by_init];
    logoImgView.contentMode = UIViewContentModeScaleAspectFill;
    logoImgView.clipsToBounds = YES;
    [infoMaskView addSubview:logoImgView];
    _logoImgView = logoImgView;
    [logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(28);
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(3);
    }];
    [logoImgView addCornerRadius:14 size:CGSizeMake(28, 28)];
    [logoImgView uploadHDImageWithURL:_model.head_img callback:nil];
    
    // 主播时长
    UILabel *liveTimerLab = [UILabel by_init];
    [liveTimerLab setBy_font:10];
    liveTimerLab.textColor = [UIColor whiteColor];
    liveTimerLab.text = @"00:00";
    [infoMaskView addSubview:liveTimerLab];
    _liveTimerLab = liveTimerLab;
    [liveTimerLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(4);
        make.left.mas_equalTo(35);
        make.right.mas_equalTo(-5);
        make.height.mas_equalTo(10);
    }];
    
    // 观看人次
    UILabel *liveNumLab = [UILabel by_init];
    [liveNumLab setBy_font:10];
    liveNumLab.textColor = [UIColor whiteColor];
    liveNumLab.text = @"0人次";
    [infoMaskView addSubview:liveNumLab];
    _liveNumLab = liveNumLab;
    [liveNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(liveTimerLab.mas_bottom).with.offset(2);
        make.left.mas_equalTo(35);
        make.height.mas_equalTo(10);
        make.right.mas_equalTo(-5);
    }];
    
    if (!_config.isHost) {
        // 关注按钮
        UIButton *attentionBtn = [UIButton by_buttonWithCustomType];
        NSAttributedString *normalAtt = [[NSAttributedString alloc] initWithString:@"+ 关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xffffff),NSFontAttributeName:[UIFont systemFontOfSize:12]}];
        NSAttributedString *selectAtt = [[NSAttributedString alloc] initWithString:@"已关注" attributes:@{NSForegroundColorAttributeName:kColorRGBValue(0xee4944),NSFontAttributeName:[UIFont systemFontOfSize:12]}];
        [attentionBtn setAttributedTitle:normalAtt forState:UIControlStateNormal];
        [attentionBtn setAttributedTitle:selectAtt forState:UIControlStateSelected];
        [attentionBtn addTarget:self action:@selector(attentionBtnAction) forControlEvents:UIControlEventTouchUpInside];
        [infoMaskView addSubview:attentionBtn];
        self.attentionBtn = attentionBtn;
        attentionBtn.layer.cornerRadius = 12;
        attentionBtn.selected = _model.isAttention;
        [attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-5);
            make.height.mas_equalTo(24);
            make.width.mas_equalTo(48);
            make.centerY.mas_equalTo(0);
        }];
    }
    [self reloadAttentionStatus];
}

- (void)addBBTMaskView{
    if (_bbtMaskView) return;
    UIView *bbtMaskView = [UIView by_init];
    bbtMaskView.layer.cornerRadius = 11;
    bbtMaskView.backgroundColor = kColorRGB(75, 75, 75, 0.4);
    [self.view addSubview:bbtMaskView];
    _bbtMaskView = bbtMaskView;
    [bbtMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(kSafe_Mas_Top(120));
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(22);
    }];
    
    // app图标
    UIImageView *iconImgView = [UIImageView by_init];
    [iconImgView setImage:[[UIImage imageNamed:@"common_icon_17"] addCornerRadius:8 size:CGSizeMake(16, 16)]];
    [bbtMaskView addSubview:iconImgView];
    [iconImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(16);
        make.left.mas_equalTo(3);
        make.centerY.mas_equalTo(0);
    }];
    
    // bbt数量
    UILabel *bbtNumLab = [UILabel by_init];
    [bbtNumLab setBy_font:12];
    bbtNumLab.textColor = [UIColor whiteColor];
    bbtNumLab.text = @"0";
    [bbtMaskView addSubview:bbtNumLab];
    _bbtNumLab = bbtNumLab;
    [bbtNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(26);
        make.right.mas_equalTo(-5);
        make.height.mas_equalTo(14);
    }];
}

// 举报
- (void)addRepeortMarkView{
    if (_reportMarkView) return;
    
    // 举报
    self.reportSheetView = [BYReportSheetView initReportSheetViewShowInView:kCommonWindow];
    self.reportSheetView.theme_id = _model.live_record_id;
    self.reportSheetView.theme_type = BY_THEME_TYPE_LIVE;
    self.reportSheetView.user_id = self.model.user_id;
    
    self.reportMaskView = [UIView by_init];
    self.reportMaskView.layer.opacity = 0.0f;
    [self.superController.view addSubview:self.reportMaskView];
    [self.reportMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reportMaskViewAction)];
    [self.reportMaskView addGestureRecognizer:tapGestureRecognizer];
    
    self.reportMarkView = [[UIImageView alloc] init];
    [self.reportMarkView by_setImageName:@"common_report_double"];
    [self.superController.view addSubview:self.reportMarkView];
    self.reportMarkView.userInteractionEnabled = YES;
    self.reportMarkView.layer.shadowColor = kColorRGBValue(0x4e4e4e).CGColor;
    self.reportMarkView.layer.shadowOpacity = 0.2;
    self.reportMarkView.layer.shadowRadius = 25;
    self.reportMarkView.layer.shadowOffset = CGSizeMake(0, -2);
    self.reportMarkView.layer.opacity = 0.0f;
    @weakify(self);
    [self.reportMarkView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(-45);
        make.width.mas_equalTo(self.reportMarkView.image.size.width);
        make.height.mas_equalTo(self.reportMarkView.image.size.height);
    }];
    
    UIButton *reportBtn = [UIButton by_buttonWithCustomType];
    [reportBtn setBy_attributedTitle:@{@"title":@"举报",NSForegroundColorAttributeName:kColorRGBValue(0x4f4f4f),NSFontAttributeName:[UIFont systemFontOfSize:13]} forState:UIControlStateNormal];
    [reportBtn addTarget:self action:@selector(reportBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.reportMarkView addSubview:reportBtn];
    [reportBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    
    UIButton *upVideoBtn = [UIButton by_buttonWithCustomType];
    [upVideoBtn setBy_attributedTitle:@{@"title":@"申请连麦",NSForegroundColorAttributeName:kColorRGBValue(0x4f4f4f),NSFontAttributeName:[UIFont systemFontOfSize:13]} forState:UIControlStateNormal];
    [upVideoBtn addTarget:self action:@selector(upVideoBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.reportMarkView addSubview:upVideoBtn];
    [upVideoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(50);
        make.height.mas_equalTo(50);
    }];
    
    UIView *lineView = [UIView by_init];
    lineView.backgroundColor = kColorRGBValue(0xededed);
    [self.reportMarkView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(50);
        make.height.mas_equalTo(0.5);
    }];
}

- (BYILiveRoomBottomView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[BYILiveRoomBottomView alloc] initWithFathureView:self.view isHost:_config.isHost];
        @weakify(self);
        _bottomView.openLiveBtnHandle = ^{
            @strongify(self);
            [self openLiveAction];
        };
        _bottomView.moreBtnHandle = ^{
            @strongify(self);
            [self showReportMarkViewHidden:NO];
        };
    }
    return _bottomView;
}

- (BYILiveChatView *)chatView{
    if (!_chatView) {
        _chatView = [[BYILiveChatView alloc] initWithModel:self.model];
        _chatView.isHost = _config.isHost;
        @weakify(self);
        _chatView.didSurplusBBTHandle = ^(void) {
            @strongify(self);
            [self loadRequestGetSurplusBBT];
        };
        _chatView.didReceiveOpenLiveHandle = ^{
            @strongify(self);
            if (!self.config.isHost) {
                self.model.status = BY_LIVE_STATUS_LIVING;
                [self enterRoom];
            }
        };
        // 观众接收到同意上麦请求操作
        _chatView.didReceiveUpToVideoHandle = ^(BOOL isUpToVideo) {
            @strongify(self);
            [self receiveAgreeUpToVideo:isUpToVideo];
        };
    }
    return _chatView;
}
@end
