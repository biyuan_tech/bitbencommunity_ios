//
//  BYILiveRoomSubControllerV1.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/7/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewController.h"
#import "BYLiveConfig.h"
#import "BYILiveOpeartionView.h"
#import "BYILiveRoomSegmentView.h"
#import <TXLiteAVSDK_Professional/TXLiteAVSDK.h>

@class BYCommonLiveModel;
NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    TRTC_IDLE,       // SDK 没有进入视频通话状态
    TRTC_ENTERED,    // SDK 视频通话进行中
} TRTCStatus;

@interface BYILiveRoomSubControllerV1 : BYCommonViewController
{
    NSString *_mainPreviewId;
}
/** 配置 */
@property (nonatomic ,strong) BYLiveConfig *config;
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 主播、用户操作台 */
@property (nonatomic ,strong) BYILiveOpeartionView *opeartionView;

/** 父Controller */
@property (nonatomic ,weak) UIViewController *superController;
@property (nonatomic ,copy) void(^didShareActionHandle)(void);
/** segment */
@property (nonatomic ,weak) BYILiveRoomSegmentView *segmentView;

/** 直播类型状态 */
@property (nonatomic ,assign) TRTCStatus roomStatus;
/** 参数 */
@property (nonatomic ,strong) TRTCParams *param;
/** 记录所有用户画面 */
@property (nonatomic ,strong) NSMutableDictionary *remoteViewDic;


// 成功进入音视频房间
- (void)enterRoomSuc;
// 成功推出音视频房间
- (void)exitRoomSuc;

// 混流
- (void)updateCloudMixtureParams;

- (void)relayout;
- (void)quitRoom;
- (void)backBtnAction;
- (void)reloadGuestLivingStatus:(BOOL)isLiving;

- (void)stopLocalPreview;

@end

NS_ASSUME_NONNULL_END
