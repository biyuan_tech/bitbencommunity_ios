//
//  BYKeyBoardTextView.m
//  BY
//
//  Created by 黄亮 on 2018/8/20.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYKeyBoardTextView.h"
#import "BYIMDefine.h"
#import "BYTextField.h"
#import "BYLiveDefine.h"

@interface BYKeyBoardTextView()<UITextFieldDelegate>
{
    /** 输入的内容 */
    NSString *_inputText;
}
// ** 消息发送
@property (nonatomic ,strong) UIView *keyBoardView;

// ** superView
@property (nonatomic ,strong) UIView *fathureView;

// ** 输入控件
@property (nonatomic ,strong) UITextField *textField;

@end

/** 输入控件高度 */
static CGFloat keyBoardViewH = 50;

@implementation BYKeyBoardTextView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFathureView:(UIView *)fathureView
{
    self = [super init];
    if (self) {
        _fathureView = fathureView;
//        self.frame = fathureView.bounds;
        [self addNSNotification];
        [self setContentView];
    }
    return self;
}

//+ (instancetype)keyBoardTextViewWithFathureView:(UIView *)fathureView{
//    return [[self alloc] initWithFathureView:fathureView];
//}


- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)sendBtnAction:(UIButton *)sender{
    @weakify(self);
    [[BYIMManager sharedInstance] sendTextMessage:_inputText succ:^{
        @strongify(self);
        [self clearInputText];
        showToastView(@"消息发送成功", self.superview);
    } fail:^ {
        @strongify(self);
        showToastView(@"消息发送失败", self.fathureView);
    }];
    
    // 埋点
//    直播发送
    [MTAManager event:MTATypeLiveSend params:nil];
}

// 清除输入内容
- (void)clearInputText{
    self.textField.text = @"";
    _inputText = nil;
}

- (void)showAnimation{
    [_fathureView addSubview:self];
    [self mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    [_textField becomeFirstResponder];
}

- (void)endExit{
    [_textField resignFirstResponder];
}

#pragma mark - show/hiddenAnimation

- (void)layoutKeyBoardWillShowMas:(CGFloat)keyBoardH{
    @weakify(self);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        @strongify(self);
        [self setNeedsUpdateConstraints];
        @weakify(self);
        [UIView animateWithDuration:0.2 animations:^{
            @strongify(self);
            [self.keyBoardView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(-keyBoardH);
            }];
            [self layoutIfNeeded];
        }];
    });
}

- (void)layoutKeyBoardWillHiddenMas{
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.2 animations:^{
        [self.keyBoardView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(keyBoardViewH);
        }];
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - NSNotification
- (void)keyBoardWillShow:(NSNotification *)notic{
    NSDictionary *userInfo = notic.userInfo;
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyBoardRect = [value CGRectValue];
    CGFloat keyBoardH = keyBoardRect.size.height;
    [self layoutKeyBoardWillShowMas:keyBoardH];
}

- (void)keyBoardWillHidden:(NSNotification *)notic{
    [self layoutKeyBoardWillHiddenMas];
}

//- (void)liveStatusDidChange:(NSNotification *)notic{
//    _liveStatus = [notic.object boolValue];
//}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    _inputText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return YES;
}

- (void)setContentView{

    [self addTarget:self action:@selector(endExit) forControlEvents:UIControlEventTouchUpInside];
    
    self.keyBoardView = [self getKeyBoardTextView];
    [self addSubview:self.keyBoardView];
    [_keyBoardView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth);
        make.height.mas_equalTo(50);
        make.centerX.mas_equalTo(0);
        make.bottom.mas_equalTo(keyBoardViewH);
    }];
}

- (UIView *)getKeyBoardTextView{
    UIView *view = [UIView by_init];
    view.backgroundColor = [UIColor whiteColor];
    
    // 消息发送
    UIButton *sendBtn = [UIButton by_buttonWithCustomType];
    [sendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [sendBtn setBackgroundColor:kColorRGBValue(0x424242)];
    [sendBtn addTarget:self action:@selector(sendBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [sendBtn layerCornerRadius:16 size:CGSizeMake(63, 32)];
    [view addSubview:sendBtn];
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(63);
        make.height.mas_equalTo(32);
        make.centerY.mas_equalTo(0);
        make.right.mas_equalTo(-15);
    }];
    
    // 消息填写
    BYTextField *textField = [[BYTextField alloc] init];
//    textField.borderStyle = UITextBorderStyleLine;
    textField.background = [UIImage imageNamed:@"ilive_chat_bg"];
    textField.delegate = self;
    [view addSubview:textField];
    _textField = textField;
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(6);
        make.bottom.mas_equalTo(-6);
        make.right.equalTo(sendBtn.mas_left).with.offset(-15);
    }];
    
    return view;
}

@end
