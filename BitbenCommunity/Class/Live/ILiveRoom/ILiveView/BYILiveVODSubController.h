//
//  BYILiveVODSubController.h
//  BibenCommunity
//
//  Created by 随风 on 2018/9/26.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BYCommonLiveModel,BYLiveConfig;
@interface BYILiveVODSubController : UIViewController


/** 配置 */
@property (nonatomic ,strong) BYLiveConfig *config;
@property (nonatomic ,strong) BYCommonLiveModel *model;
/** 父Controller */
@property (nonatomic ,weak) UIViewController *superController;

- (void)backBtnAction;

@end
