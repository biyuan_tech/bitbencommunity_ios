//
//  BYILivewChatViewTextCell.m
//  BY
//
//  Created by 黄亮 on 2018/8/30.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILivewChatViewTextCell.h"
#import "BYILiveChatViewCellModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

@interface BYILivewChatViewTextCell()

@property (nonatomic ,strong) BYILiveChatViewCellModel *model;

@property (nonatomic ,strong) UIView *maskBgView;

/** 用户头像 */
@property (nonatomic ,strong) PDImageView *userlogo;

/** 用户昵称 */
//@property (nonatomic ,strong) UILabel *userName;

/** 发表的信息 */
@property (nonatomic ,strong) YYLabel *message;

@end

@implementation BYILivewChatViewTextCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setContentView];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews{
    [super layoutSubviews];
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYILiveChatViewCellModel *model = object[indexPath.row];
    _model = model;
//    @weakify(self);
//    [_userlogo sd_setImageWithURL:[NSURL URLWithString:model.userlogoUrl] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
//        @strongify(self);
//        self.userlogo.image = [image addCornerRadius:10 size:CGSizeMake(20, 20)];
//    }];
    [self.userlogo uploadHDImageWithURL:model.userlogoUrl callback:nil];

    NSString *message = [NSString stringWithFormat:@"%@ : %@",model.userName,model.message];
    NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:message];
    messageAttributed.yy_color = [UIColor whiteColor];
    messageAttributed.yy_lineSpacing = 4;
    messageAttributed.yy_font = [UIFont boldSystemFontOfSize:12];
    [messageAttributed yy_setColor:kColorRGBValue(0xffd050) range:NSMakeRange(0, [NSString stringWithFormat:@"%@ :",model.userName].length)];

    self.message.attributedText = messageAttributed;
    [_message mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(ceil(model.messageSize.height));
    }];
}

- (void)setContentView{
    
    UIView *maskBgView = [UIView by_init];
    [maskBgView setBackgroundColor:kColorRGB(98, 98, 98, 0.4)];
    maskBgView.layer.cornerRadius = 12;
    [self.contentView addSubview:maskBgView];
    _maskBgView = maskBgView;
    [maskBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.top.mas_equalTo(0);
        make.right.mas_lessThanOrEqualTo(-10);
    }];
    
    // 头像
    self.userlogo = [PDImageView by_init];
    _userlogo.contentMode = UIViewContentModeScaleAspectFill;
    [maskBgView addSubview:self.userlogo];
    [_userlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(20);
        make.left.mas_equalTo(2);
        make.top.mas_equalTo(2);
    }];
    [self.userlogo addCornerRadius:10 size:CGSizeMake(20, 20)];
    
    // 消息
    self.message = [YYLabel by_init];
    _message.font = [UIFont systemFontOfSize:12];
    _message.numberOfLines = 0;
    [maskBgView addSubview:_message];
    @weakify(self);
    [_message mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.userlogo.mas_right).with.offset(8);
        make.top.mas_equalTo(4);
        make.bottom.mas_equalTo(-4);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(12);
    }];
}
@end
