//
//  BYILiveRoomController.m
//  BY
//
//  Created by 黄亮 on 2018/8/14.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveRoomController.h"
#import "BYILiveRoomViewModel.h"

#define ViewModel ((BYILiveRoomViewModel *)self.viewModel)
@interface BYILiveRoomController ()<NetworkAdapterSocketDelegate>

@property (nonatomic ,strong) BYLiveConfig *config;

@end

@implementation BYILiveRoomController

- (void)dealloc
{
    [[BYSIMManager shareManager] resetScoketStatus];
}

- (Class)getViewModelClass{
    return [BYILiveRoomViewModel class];
}

- (id)initWithConfig:(BYLiveConfig *)config{
    self = [super init];
    if (self) {
        _config = config;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    self.hasCancelSocket = NO;
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [NetworkAdapter sharedAdapter].socketDelegate = self;
    [[BYSIMManager shareManager] connectionSocket];
    // 关闭手势返回
    self.popGestureRecognizerEnale = NO;

}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    // 埋点
    NSDictionary *params = @{@"直播详情":(self.model.room_id.length?self.model.room_id:@"找不到房间号")};
    [MTAManager event:MTATypeHotLiveBack params:params];
    [[BYSIMManager shareManager] closeSocket];
}


- (void)viewDidLoad {
    ViewModel.config = self.config;
    [super viewDidLoad];
    
    
    
    // 埋点
    NSDictionary *params = @{@"直播详情":(self.model.room_id.length?self.model.room_id:@"找不到房间号")};
    [MTAManager event:MTATypeHotLiveDetail params:params];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data{
    [[BYSIMManager shareManager] webSocketReceiveData:type data:data];
}

-(void)webSocketDidConnectedWithSocket:(WebSocketConnection *)webSocketConnection connectedStatus:(SocketConnectType)connectedStatus{
    if (webSocketConnection == [NetworkAdapter sharedAdapter].mainRootSocketConnection) {
        if (connectedStatus == SocketConnectTypeOpen) {
            [[BYSIMManager shareManager] webSocketDidConnectedWithSocket:webSocketConnection];
        }
    }
}


@end
