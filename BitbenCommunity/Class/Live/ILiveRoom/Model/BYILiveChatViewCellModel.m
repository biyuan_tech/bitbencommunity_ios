//
//  BYILiveChatViewCellModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/13.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveChatViewCellModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

@implementation BYILiveChatViewCellModel

//+ (NSArray *)transMessageArr:(NSArray *)arr{
//    NSMutableArray *tableData = [NSMutableArray array];
//    for (TIMMessage *msg in arr) {
//        if ([[msg getElem:0] isMemberOfClass:[TIMGroupSystemElem class]]) break;
//         if ([[msg getElem:0] isMemberOfClass:[TIMTextElem class]]) {
//            BYILiveChatViewCellModel *model = [[BYILiveChatViewCellModel alloc] getTextElemCellModel:msg];
//            [tableData addObject:model];
//        }
//        else if ([[msg getElem:0] isMemberOfClass:[TIMCustomElem class]]){
//            BYILiveChatViewCellModel *model = [[BYILiveChatViewCellModel alloc] getCustomElemCellModel:msg];
//            if (model) {
//                [tableData addObject:model];
//            }
//        }
//    }
//    return tableData;
//}

// 获取文本消息的cell model
//- (BYILiveChatViewCellModel *)getTextElemCellModel:(TIMMessage *)msg{
//    TIMTextElem *elem = (TIMTextElem *)[msg getElem:0];
//    BYILiveChatViewCellModel *model = self;
//    model.cellString = @"BYILivewChatViewTextCell";
//    model.userName = msg.sender;
//    if (msg.isSelf) {
//        model.userName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
//        model.userlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
//    }
//    else
//    {
//        TIMUserProfile *user = [msg GetSenderProfile];
//        model.userName = user.nickname;
//        model.userlogoUrl = user.faceURL;
//    }
//    model.message = elem.text;
//    NSString *message = [NSString stringWithFormat:@"%@ : %@",model.userName,model.message];
//    NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:message];
//    messageAttributed.yy_color = [UIColor whiteColor];
//    messageAttributed.yy_lineSpacing = 4;
//    messageAttributed.yy_font = [UIFont systemFontOfSize:12];
//    YYTextContainer *textContainer = [YYTextContainer new];
//    textContainer.size = CGSizeMake(kCommonScreenWidth - 150, MAXFLOAT);
//    YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
//    model.cellHeight = layout.textBoundingSize.height + 8 + 10;
//    model.messageSize = CGSizeMake(layout.textBoundingSize.width, layout.textBoundingSize.height);
//    return model;
//}
//
//- (BYILiveChatViewCellModel *)getCustomElemCellModel:(TIMMessage *)msg{
//    TIMCustomElem *elem = (TIMCustomElem *)[msg getElem:0];
//    BYILiveChatViewCellModel *model = self;
//    model.cellString = @"BYILiveChatViewOperaCell";
//    model.userName = msg.sender;
//    NSString *receiveStr = [[NSString alloc]initWithData:elem.data encoding:NSUTF8StringEncoding];
//    NSData * data = [receiveStr dataUsingEncoding:NSUTF8StringEncoding];
//    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//    if ([dic[@"opera_type"] integerValue] == BY_GUEST_OPERA_TYPE_REWARD) {
//        model.message = dic[@"text"];
//    }
//    else
//    {
//        return nil;
//    }
//    CGFloat width = kCommonScreenWidth - 80 - 50;
//    model.cellHeight = [model.message getStringSizeWithFont:[UIFont systemFontOfSize:12] maxWidth:width].height + 8 + 10;
//    return model;
//}

+ (NSArray *)transformSIMTableData:(NSArray *)array{
    if (!array.count) return @[];
    NSMutableArray *tableData = [NSMutableArray array];
    for (NSDictionary *dic in array) {
        if (!dic) break;
        BY_SIM_MSG_TYPE msgType = [dic[@"content_type"] integerValue];
        if (msgType == BY_SIM_MSG_TYPE_TEXT) { // 文本消息
            BYILiveChatViewCellModel *model = [[BYILiveChatViewCellModel alloc] getSingleTextCellModel:dic];
            [tableData addObject:model];
        }
        else if (msgType == BY_SIM_MSG_TYPE_CUSTOME ||
                 msgType == BY_SIM_MSG_TYPE_SYSTEM){
            if ([dic[@"content"] length]) {
                NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                BY_GUEST_OPERA_TYPE opearType = [customData[kBYCustomMessageType] integerValue];
                if (opearType == BY_GUEST_OPERA_TYPE_REWARD) { // 打赏消息
                    BYILiveChatViewCellModel *model = [[BYILiveChatViewCellModel alloc] getSingleCellModel:dic];
                    [tableData addObject:model];
                }
            }
        }
        else if (msgType == BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW){
            BYILiveChatViewCellModel *model = [[BYILiveChatViewCellModel alloc] getSystemCellModel:dic];
            [tableData addObject:model];
        }
    }
    return tableData;
}

- (BYILiveChatViewCellModel *)getSingleTextCellModel:(NSDictionary *)dic{
    BYILiveChatViewCellModel *model = self;
    model.cellString = @"BYILivewChatViewTextCell";
    model.userName = dic[@"nickname"];
    model.userlogoUrl = dic[@"head_img"];
    model.message = [nullToEmpty(dic[@"content"]) removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
    NSString *message = [NSString stringWithFormat:@"%@ : %@",model.userName,model.message];
    NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:message];
    messageAttributed.yy_color = [UIColor whiteColor];
    messageAttributed.yy_lineSpacing = 4;
    messageAttributed.yy_font = [UIFont systemFontOfSize:12];
    YYTextContainer *textContainer = [YYTextContainer new];
    textContainer.size = CGSizeMake(kCommonScreenWidth - 150, MAXFLOAT);
    YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
    model.cellHeight = layout.textBoundingSize.height + 8 + 10;
    model.messageSize = CGSizeMake(layout.textBoundingSize.width, layout.textBoundingSize.height);
    return model;
}

- (BYILiveChatViewCellModel *)getSingleCellModel:(NSDictionary *)dic{
    BYILiveChatViewCellModel *model = self;
    model.cellString = @"BYILiveChatViewOperaCell";
    model.userName = dic[@"nickname"];
    NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    if ([customData[@"opera_type"] integerValue] == BY_GUEST_OPERA_TYPE_REWARD) {
        model.message = customData[@"text"];
    }
    else
    {
        return nil;
    }
    CGFloat width = kCommonScreenWidth - 80 - 50;
    model.cellHeight = [model.message getStringSizeWithFont:[UIFont systemFontOfSize:12] maxWidth:width].height + 8 + 10;
    return model;
}

- (BYILiveChatViewCellModel *)getSystemCellModel:(NSDictionary *)dic{
    BYILiveChatViewCellModel *model = self;
    model.cellString = @"BYILiveChatViewSystemCell";
    model.message = dic[@"content"];
    CGFloat width = kCommonScreenWidth - 80 - 50;
    model.cellHeight = [model.message getStringSizeWithFont:[UIFont systemFontOfSize:12] maxWidth:width].height + 8 + 10;
    return model;
}
@end
