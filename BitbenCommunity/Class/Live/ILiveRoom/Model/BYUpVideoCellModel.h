//
//  BYUpVideoCellModel.h
//  BibenCommunity
//
//  Created by 随风 on 2018/11/20.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYUpVideoCellModel : BYCommonModel

/** 头像 */
@property (nonatomic ,copy) NSString *userlogoUrl;
/** 昵称 */
@property (nonatomic ,copy) NSString *userName;
/** id */
@property (nonatomic ,copy) NSString *userId;
/** 连麦状态 */
@property (nonatomic ,assign) BOOL isUpVideo;
/** 连麦身份 */
@property (nonatomic ,copy) NSString *liveRole;


+ (NSArray *)getForbidMembersData:(NSArray *)data;

@end

NS_ASSUME_NONNULL_END
