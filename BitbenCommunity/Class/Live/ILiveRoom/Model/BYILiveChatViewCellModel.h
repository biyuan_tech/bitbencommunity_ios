//
//  BYILiveChatViewCellModel.h
//  BY
//
//  Created by 黄亮 on 2018/9/13.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonModel.h"

@interface BYILiveChatViewCellModel : BYCommonModel

@property (nonatomic ,copy) NSString *userlogoUrl;
@property (nonatomic ,copy) NSString *userName;
@property (nonatomic ,copy) NSString *message;
@property (nonatomic ,assign) CGSize messageSize;

//+ (NSArray *)transMessageArr:(NSArray *)arr;
+ (NSArray *)transformSIMTableData:(NSArray *)array;
@end
