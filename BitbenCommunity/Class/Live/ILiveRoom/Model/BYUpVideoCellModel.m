//
//  BYUpVideoCellModel.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/20.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYUpVideoCellModel.h"

@implementation BYUpVideoCellModel

+ (NSArray *)getForbidMembersData:(NSArray *)data{
    if (![data isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (!data.count) {
        return @[];
    }
    __block NSMutableArray *result = [NSMutableArray array];
    [data enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BYUpVideoCellModel *model = [[BYUpVideoCellModel alloc] init];
        model.cellString = @"BYILiveForbidMemberCell";
        model.cellHeight = 56;
        model.userlogoUrl = obj[@"head_img"];
        model.userName = obj[@"nickname"];
        model.userId = obj[@"user_id"];
        [result addObject:model];
    }];
    return [result copy];
}

@end
