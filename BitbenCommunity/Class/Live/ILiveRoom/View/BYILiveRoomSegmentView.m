//
//  BYILiveRoomSegmentView.m
//  BY
//
//  Created by 黄亮 on 2018/9/3.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveRoomSegmentView.h"
#import "UIScrollView+Gesture.h"
#import "BYPPTRoomViewModel.h"

static char actionSegmentListIndexBlockKey;

@interface BYILiveRoomSegmentView()<UIScrollViewDelegate>
{
    NSInteger _index;
    CGFloat _beginDragOffsetX;
}
/** 标题存储，也用于初始化子View */
@property (nonatomic ,strong) NSArray *titles;

/** 存储headerView按钮 */
@property (nonatomic ,strong) NSArray *buttons;

/** headerView */
@property (nonatomic ,strong) UIView *headerView;


/** 选择项跟踪横线 */
@property (nonatomic ,strong) UIView *selLineView;

/** subView width */
@property (nonatomic ,assign) CGFloat sub_width;
@property (nonatomic ,assign) CGFloat spaceLeft;
@property (nonatomic ,assign) NSInteger lastIndex;
/** 当前是否为主播端 */
@property (nonatomic ,assign) BOOL isHost;
@end

static NSInteger baseTag = 0X599;
@implementation BYILiveRoomSegmentView

- (instancetype)initWithIsHost:(BOOL)isHost titles:(NSString *)titles, ... NS_REQUIRES_NIL_TERMINATION {
    NSMutableArray *arrays = [NSMutableArray array];
    va_list argList;
    if (titles.length){
        [arrays addObject:titles];
        va_start(argList, titles);
        id temp;
        while ((temp = va_arg(argList, id))){
            [arrays addObject:temp];
        }
    }
    BYILiveRoomSegmentView *segmentView = [[BYILiveRoomSegmentView alloc] init];
    segmentView.titles = arrays;
    segmentView.isHost = isHost;
    [segmentView setContentView];
    segmentView.defultSelIndex = 1;
    segmentView.lastIndex = 1;
    return segmentView;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self setNeedsUpdateConstraints];
    @weakify(self);
    [UIView animateWithDuration:0.2 animations:^{
        @strongify(self);
        @weakify(self);
        [self.selLineView mas_updateConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.centerX.equalTo(self.headerView.mas_left).with.offset(self.spaceLeft + (self.sub_width)*self.lastIndex + (self.sub_width)/2);
        }];
        [self layoutIfNeeded];
    }];
    [_scrollView setContentOffset:CGPointMake(kCommonScreenWidth*self.lastIndex, 0) animated:NO];
}

#pragma mark - customMehtod
- (void)didSelectSubViewIndex:(NSInteger)index{
    // 设置高亮
    UIButton *button = [self viewWithTag:baseTag + index];
    for (UIButton *btn in self.buttons) {
        btn.selected = button == btn ? YES : NO;
    }
    [self setNeedsLayout];
    self.lastIndex = index;
}

#pragma mark - buttonAction
- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - baseTag;
    // 如与上次选择一样则返回
    if (index == self.lastIndex) return;
    [_scrollView setContentOffset:CGPointMake(kCommonScreenWidth*index, 0) animated:YES];
    [self didSelectSubViewIndex:index];
    
    void(^block)(NSInteger index) = objc_getAssociatedObject(self, &actionSegmentListIndexBlockKey);
    if (block){
        block(index);
    }
}

-(void)actionSegmentListIndexBlock:(void(^)(NSInteger index))block{
    objc_setAssociatedObject(self, &actionSegmentListIndexBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

//- (void)reloadChangeThemeType:(BOOL)isWhite{
//    UIButton *backBtn = [self viewWithTag:3*baseTag];
//    UIButton *shareBtn = [self viewWithTag:3*baseTag + 1];
//    if (isWhite) {
//        [backBtn setBy_imageName:@"icon_center_bbt_back" forState:UIControlStateNormal];
//        [shareBtn setBy_imageName:@"ilive_share_white" forState:UIControlStateNormal];
//    }
//    for (int i = 0; i < _titles.count; i ++) {
//        NSString *title = _titles[i];
//        UIButton *titleBtn = [self viewWithTag:baseTag + i];
//        NSDictionary *normalDic = @{@"title":title,NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:isWhite ? [UIColor whiteColor] : kTextColor_70};
//        [titleBtn setBy_attributedTitle:normalDic forState:UIControlStateNormal];
//    }
//}

- (void)replaceLiveSubView:(UIView *)view bgColor:(UIColor *)color{
    UIView *subView = [_scrollView viewWithTag:2*baseTag + 1];
    UIView *liveView = [subView viewWithTag:4*baseTag];
    if (!view) return;
    if (liveView == view) return;
    [liveView removeFromSuperview];
    [subView addSubview:view];
    [subView setBackgroundColor:color];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (void)setLiveSubViewBgColor:(UIColor *)color{
    UIView *subView = [_scrollView viewWithTag:2*baseTag + 1];
    [subView setBackgroundColor:color];
}

- (void)setDefultSelIndex:(NSInteger)defultSelIndex{
    _defultSelIndex = defultSelIndex;
    [_scrollView setContentOffset:CGPointMake(kCommonScreenWidth*defultSelIndex, 0) animated:NO];
    [self didSelectSubViewIndex:defultSelIndex];
}

- (void)guestBtnAction:(UIButton *)sender{
    if (sender.tag == 3*baseTag) { // 返回
        if (self.didBackActionHandle) self.didBackActionHandle();
    }
    else if (sender.tag == 3*baseTag + 1) { // 分享
        if (self.didShareActionHandle) self.didShareActionHandle();
    }
}

#pragma mark - scrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat indexFloat = (CGFloat)scrollView.contentOffset.x/kCommonScreenWidth;
    NSInteger index = ceil(scrollView.contentOffset.x/kCommonScreenWidth);
    if (indexFloat < index) {
        index = round(scrollView.contentOffset.x/kCommonScreenWidth);
    }
    UIView *view = [_scrollView viewWithTag:2*baseTag + index];
    if (view.subviews.count == 0 && view) {
        if ([self.delegate respondsToSelector:@selector(by_segmentViewLoadSubView:)]) {
            UIView *subView = [self.delegate by_segmentViewLoadSubView:index];
            subView.tag = 4*baseTag;
            [view addSubview:subView];
            if (index != 2) {
                [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.edges.mas_equalTo(UIEdgeInsetsMake(kiPhoneX_Mas_top(0), 0, -kiPhoneX_Mas_buttom(0), 0));
                }];
            }
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    BOOL scrollToScrollStop = !scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
    if (scrollToScrollStop) {
        [self scrollViewDidEndScroll:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        BOOL dragToDragStop = scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
        if (dragToDragStop) {
            [self scrollViewDidEndScroll:scrollView];
        }
    }
}

- (void)scrollViewDidEndScroll:(UIScrollView *)scrollView{
    NSInteger index = ceil(scrollView.contentOffset.x/kCommonScreenWidth);
    if (self.lastIndex == index) return;
    [self didSelectSubViewIndex:index];
    if ([self.delegate respondsToSelector:@selector(by_segmentViewDidScrollToIndex:)]) {
        [self.delegate by_segmentViewDidScrollToIndex:index];
    }
}

#pragma mark - configSubView Method

- (void)setContentView{
    UIView *headerView = [self getHeaderView];
    headerView.backgroundColor = [UIColor clearColor];
    [self addSubview:headerView];
    _headerView = headerView;
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kiPhoneX_Mas_top(20));
        make.left.mas_equalTo(33);
        make.right.mas_equalTo(-33);
        make.height.mas_equalTo(47);
    }];
    
    if (!_isHost) {
        NSArray *imageNames = @[@"icon_main_back",@"ilive_share"];
        for (int i = 0; i < 2; i ++) {
            UIButton *button = [UIButton by_buttonWithCustomType];
            [button setBy_imageName:imageNames[i] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(guestBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:button];
            button.tag = 3*baseTag + i;
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.height.mas_equalTo(32);
                make.centerY.mas_equalTo(headerView.mas_centerY);
                if (i == 0) {
                    make.left.mas_equalTo(0);
                }
                else{
                    make.right.mas_equalTo(0);
                }
            }];
        }
    }
    
    UIScrollView *scrollView = [self getScrollView];
    scrollView.delegate = self;
    [self addSubview:scrollView];
    [self insertSubview:scrollView belowSubview:headerView];
    _scrollView = scrollView;
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

// headerView
- (UIView *)getHeaderView{
    UIView *headerView = [UIView by_init];
    NSMutableArray *arrays = [NSMutableArray array];
//    self.sub_width = (CGFloat)kCommonScreenWidth/_titles.count;
    self.sub_width = _isHost ? 102 : 70;
    self.spaceLeft = (kCommonScreenWidth - _titles.count*_sub_width - 66)/2;
    for (int i = 0; i < _titles.count; i ++) {
        NSString *title = _titles[i];
        UIButton *button = [UIButton by_buttonWithCustomType];
        button.backgroundColor = [UIColor clearColor];
        NSDictionary *normalDic = @{@"title":title,NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kTextColor_70};
        NSDictionary *highlight = @{@"title":title,NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kTextColor_238};
        [button setBy_attributedTitle:normalDic forState:UIControlStateNormal];
        [button setBy_attributedTitle:highlight forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:button];
        [arrays addObject:button];
        button.tag = baseTag + i;
        @weakify(self);
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.width.mas_equalTo(self.sub_width);
            make.top.bottom.mas_equalTo(0);
            make.left.mas_equalTo(self.spaceLeft + (self.sub_width)*i);
        }];
    }
    self.buttons = arrays;
    
    UIView *selLineView = [UIView by_init];
    selLineView.backgroundColor = kTextColor_238;
    [selLineView layerCornerRadius:1 size:CGSizeMake(16, 2)];
    [headerView addSubview:selLineView];
    _selLineView = selLineView;
    [selLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-2);
        make.height.mas_equalTo(2);
        make.width.mas_equalTo(16);
        make.centerX.equalTo(headerView.mas_left).with.offset(kCommonScreenWidth/2);
    }];
    
    for (int i = 0; i < 2; i ++) {
        UIView *verticalLineView = [UIView by_init];
        verticalLineView.backgroundColor = kTextColor_70;
        [headerView addSubview:verticalLineView];
        @weakify(self);
        [verticalLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.width.mas_equalTo(1);
            make.height.mas_equalTo(15);
            make.left.mas_equalTo(self.spaceLeft + (self.sub_width)*(i + 1));
            make.top.mas_equalTo(17);
        }];
    }
    return headerView;
}

// contentView
- (UIScrollView *)getScrollView{
    UIScrollView *scrollView = [UIScrollView by_init];
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    for (int i = 0; i < _titles.count; i ++) {
        UIView *view = [UIView by_init];
        [scrollView addSubview:view];
        view.tag = 2*baseTag + i;
       @weakify(self);
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.left.mas_equalTo(kCommonScreenWidth*i);
            make.top.mas_equalTo(0);
            make.height.equalTo(scrollView.mas_height).with.offset(0);
            make.width.equalTo(scrollView.mas_width).with.offset(0);
            if (i == self.titles.count - 1) {
                make.right.mas_equalTo(0);
            }
        }];
    }
    return scrollView;
}

@end
