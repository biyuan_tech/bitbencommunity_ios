//
//  BYILiveRoomSegmentView.h
//  BY
//
//  Created by 黄亮 on 2018/9/3.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BYILiveRoomSegmentViewDelegate <NSObject>

/**
 根据index自定义添加View

 @param index index
 @return return value description
 */
- (UIView *)by_segmentViewLoadSubView:(NSInteger)index;

/**
 当前滚动到的Index

 @param index index
 */
- (void)by_segmentViewDidScrollToIndex:(NSInteger)index;

@end

@class BYPPTRoomViewModel;
@interface BYILiveRoomSegmentView : UIView

/** 初始化选择的index (默认1) */
@property (nonatomic ,assign) NSInteger defultSelIndex;
/** contentView,subView容器 */
@property (nonatomic ,strong) UIScrollView *scrollView;

@property (nonatomic ,weak) id<BYILiveRoomSegmentViewDelegate>delegate;
/** 返回 */
@property (nonatomic ,copy) void (^didBackActionHandle)(void);
/** 分享 */
@property (nonatomic ,copy) void (^didShareActionHandle)(void);
/**
 
 */
@property (nonatomic ,copy) void (^didLoadSubView)(NSInteger index,UIView *subView);

- (instancetype)initWithIsHost:(BOOL)isHost titles:(NSString *)titles, ... NS_REQUIRES_NIL_TERMINATION ;

-(void)actionSegmentListIndexBlock:(void(^)(NSInteger index))block;

- (void)replaceLiveSubView:(UIView *)view bgColor:(UIColor *)color;

- (void)setLiveSubViewBgColor:(UIColor *)color;

//- (void)reloadChangeThemeType:(BOOL)isWhite;

@end
