//
//  BYUPVideoListView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/20.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYUPVideoListView.h"
#import "BYIMDefine.h"
//#import <TILLiveSDK/TILLiveSDK.h>
#import "BYUpVideoCellModel.h"

static NSInteger const contentH = 287.f;
@interface BYUPVideoListView ()<BYCommonTableViewDelegate>

/**  superView */
@property (nonatomic ,strong) UIView *fathuerView;
/** contentView */
@property (nonatomic ,strong) UIView *contentView;
/** maskView */
@property (nonatomic ,strong) UIView *maskView;
/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** 当前hidden状态 */
@property (nonatomic ,assign) BOOL isHidden;


@end

@implementation BYUPVideoListView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)initVideoListViewShowInView:(UIView *)view{
    BYUPVideoListView *listView = [[BYUPVideoListView alloc] init];
    listView.fathuerView = view ? view : kCommonWindow;
    listView.frame = listView.fathuerView.bounds;
    listView.hidden = YES;
    [listView addNSNotification];
    [listView configView];
    return listView;
}

- (void)showAnimation{
    self.hidden = NO;
    [self.fathuerView addSubview:self];
    self.tableView.tableData = [BYIMManager sharedInstance].upToVideoMembers;
    self.maskView.layer.opacity = 1.0f;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.2 animations:^{
            [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
            }];
            [self layoutIfNeeded];
        }];
    });
}

- (void)hiddenAnimation{
    self.maskView.layer.opacity = 0.0f;
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.2 animations:^{
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(contentH);
        }];
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
        self.hidden = YES;
        [self removeFromSuperview];
    }];
}

// 观众退出连麦通知
- (void)downVideo_NSNotification:(NSNotification *)notic{
    NSString *user = notic.object;
    NSMutableArray *tmpArr = [NSMutableArray arrayWithArray:[BYIMManager sharedInstance].upToVideoMembers];
    for (BYUpVideoCellModel *model in tmpArr) {
        if ([model.userId isEqualToString:user]) {
            NSMutableArray *mubArray = [NSMutableArray arrayWithArray:[BYIMManager sharedInstance].upToVideoMembers];
            [mubArray removeObject:model];
            [BYIMManager sharedInstance].upToVideoMembers = [mubArray copy];
            self.tableView.tableData = [mubArray copy];
            break;
        }
    }
}

- (void)upToVideoMembersValueDidChangle{
    if (!self.hidden) {
        self.tableView.tableData = [[BYIMManager sharedInstance].upToVideoMembers copy];
    }
}

#pragma mark - BYCommonTableViewDelegate

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    BYUpVideoCellModel *model = tableView.tableData[indexPath.row];
    if ([actionName isEqualToString:@"ignore"]) { // 忽略
        NSMutableArray *mubArray = [NSMutableArray arrayWithArray:[BYIMManager sharedInstance].upToVideoMembers];
        [mubArray removeObjectAtIndex:indexPath.row];
        [BYIMManager sharedInstance].upToVideoMembers = [mubArray copy];
        self.tableView.tableData = [mubArray copy];
    }
    else if ([actionName isEqualToString:@"agree"]) { // 同意
        // 向用户发送同意上麦消息
        @weakify(self);
        [[BYIMManager sharedInstance] sendAgreeUpVideoMessage:model.userId stream_id:_stream_id succ:^{
            @strongify(self);
            model.isUpVideo = YES;
//            [[BYIMManager sharedInstance] setUpVideoMemberStatus:model.userId isUp:YES];
            [self.tableView reloadData];
        } fail:nil];
    }
    else if ([actionName isEqualToString:@"finish"]) { // 结束
        [self showFinishSheetView:model indexPath:indexPath];
    }
}

- (void)showFinishSheetView:(BYUpVideoCellModel *)model indexPath:(NSIndexPath *)indexPath{
    NSString *title = [NSString stringWithFormat:@"确定要结束与%@连麦吗？",model.userName];
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    @weakify(self);
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        @weakify(self);
        [[BYIMManager sharedInstance] sendDownVideoMessage:model.userId role:model.liveRole succ:^{
            @strongify(self);
            NSMutableArray *mubArray = [NSMutableArray arrayWithArray:[BYIMManager sharedInstance].upToVideoMembers];
            if (mubArray.count - 1 < indexPath.row) return ;
            [mubArray removeObjectAtIndex:indexPath.row];
            [BYIMManager sharedInstance].upToVideoMembers = [mubArray copy];
            self.tableView.tableData = [mubArray copy];
        } fail:nil];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [sheetController addAction:confirmAction];
    [sheetController addAction:cancelAction];
    [self.superViewController presentViewController:sheetController animated:YES completion:nil];
}

#pragma mark - configUI
- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downVideo_NSNotification:) name:NSNotification_downVideo object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upToVideoMembersValueDidChangle) name:NSNotification_upToVideoMembersValueDidChangle object:nil];
}

- (void)configView{
    self.maskView = [UIView by_init];
    self.maskView.layer.opacity = 0.0f;
    [self addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenAnimation)];
    [self.maskView addGestureRecognizer:tapGestureRecognizer];
    
    self.contentView = [UIView by_init];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    self.contentView.layer.shadowColor = kColorRGBValue(0x4e4e4e).CGColor;
    self.contentView.layer.shadowOpacity = 0.2;
    self.contentView.layer.shadowRadius = 25;
    [self addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(contentH);
        make.height.mas_equalTo(contentH);
    }];
    
    UIButton *button = [UIButton by_buttonWithCustomType];
    UIColor *color = kColorRGBValue(0x353535);
    UIFont *font = [UIFont systemFontOfSize:15];
    NSDictionary *attribut = @{@"title":@"连麦列表",NSForegroundColorAttributeName:color,NSFontAttributeName:font};
    [button setBy_attributedTitle:attribut forState:UIControlStateNormal];
    [self.contentView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7e7)];
    [button addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(-0.5);
    }];
    
    UIImageView *arrowImgView = [[UIImageView alloc] init];
    [arrowImgView by_setImageName:@"common_arrow_down"];
    [button addSubview:arrowImgView];
    [arrowImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(arrowImgView.image.size.width);
        make.height.mas_equalTo(arrowImgView.image.size.height);
    }];
    
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    [self.contentView addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(50, 0, 0, 0));
    }];
    
}


@end
