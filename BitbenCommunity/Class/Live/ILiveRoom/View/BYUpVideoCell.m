//
//  BYUpVideoCell.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/20.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYUpVideoCell.h"
#import "BYUpVideoCellModel.h"

static NSInteger const baseTag = 0x254;
@interface BYUpVideoCell ()

/** userlogo */
@property (nonatomic ,strong) PDImageView *userlogoImgView;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 未连麦按钮 */
@property (nonatomic ,strong) UIView *notUpVideoView;
/** 已连麦按钮 */
@property (nonatomic ,strong) UIView *isUpVideoView;


@end

@implementation BYUpVideoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYUpVideoCellModel *model = object[indexPath.row];
    self.indexPath = indexPath;
    @weakify(self);
    [self.userlogoImgView uploadMainImageWithURL:model.userlogoUrl placeholder:nil imgType:PDImgTypeOriginal callback:^(UIImage *image) {
        @strongify(self);
        self.userlogoImgView.image = [image addCornerRadius:18 size:CGSizeMake(36, 36)];
    }];
    
    self.userNameLab.text = model.userName;
    if (model.isUpVideo) {
        self.isUpVideoView.layer.opacity = 1.0f;
        self.notUpVideoView.layer.opacity = 0.0f;
    }
    else{
        self.isUpVideoView.layer.opacity = 0.0f;
        self.notUpVideoView.layer.opacity = 1.0f;
    }
}

- (void)buttonAction:(UIButton *)sender{
    switch (sender.tag) {
        case baseTag: // 连麦中
            
            break;
        case baseTag + 1: // 结束
            [self sendActionName:@"finish" param:nil indexPath:self.indexPath];
            break;
        case 2*baseTag: // 同意
            [self sendActionName:@"agree" param:nil indexPath:self.indexPath];
            break;
        case 2*baseTag + 1: // 忽略
            [self sendActionName:@"ignore" param:nil indexPath:self.indexPath];
            break;
        default:
            break;
    }
}

- (void)setContentView{
    self.userlogoImgView = [[PDImageView alloc] init];
    self.userlogoImgView.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:self.userlogoImgView];
    [self.userlogoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(17);
        make.width.mas_equalTo(36);
        make.height.mas_equalTo(36);
    }];
    
    self.userNameLab = [UILabel by_init];
    self.userNameLab.textColor = kColorRGBValue(0x353535);
    self.userNameLab.textAlignment = NSTextAlignmentLeft;
    [self.userNameLab setBy_font:15];
    [self.contentView addSubview:self.userNameLab];
    @weakify(self);
    [self.userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(self.userlogoImgView.mas_right).mas_offset(20);
        make.centerY.mas_equalTo(0);
        make.height.mas_equalTo(stringGetHeight(@"1", 15));
        make.right.mas_equalTo(-140);
    }];
    
    self.isUpVideoView = [UIView by_init];
    [self.contentView addSubview:self.isUpVideoView];
    [self.isUpVideoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(130);
    }];
    
    NSArray *isUpVideoTitls = @[@"连麦中",@"结束"];
    NSArray *isUpVideoBgColor = @[kColorRGBValue(0x6A9C58),kColorRGBValue(0xDC564D)];
    for (int i = 0; i < 2; i ++) {
        UIButton *btn = [UIButton by_buttonWithCustomType];
        [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBy_attributedTitle:@{@"title":isUpVideoTitls[i],NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:12]} forState:UIControlStateNormal];
        btn.backgroundColor = isUpVideoBgColor[i];
        btn.layer.cornerRadius = 5;
        [self.isUpVideoView addSubview:btn];
        btn.tag = baseTag + i;
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.right.mas_equalTo(i == 0 ? -80 : -15);
            make.width.mas_equalTo(48);
            make.height.mas_equalTo(28);
        }];
    }
    
    self.notUpVideoView = [UIView by_init];
    [self.contentView addSubview:self.notUpVideoView];
    [self.notUpVideoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(130);
    }];
    
    NSArray *notUpVideoTitls = @[@"同意",@"忽略"];
    NSArray *notUpVideoBgColor = @[kColorRGBValue(0xE6BD5B),kColorRGBValue(0xF1F1F1)];
    NSArray *textColor = @[[UIColor whiteColor],kColorRGBValue(0x2a2a2a)];
    for (int i = 0; i < 2; i ++) {
        UIButton *btn = [UIButton by_buttonWithCustomType];
        [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBy_attributedTitle:@{@"title":notUpVideoTitls[i],NSForegroundColorAttributeName:textColor[i],NSFontAttributeName:[UIFont systemFontOfSize:12]} forState:UIControlStateNormal];
        btn.backgroundColor = notUpVideoBgColor[i];
        btn.layer.cornerRadius = 5;
        [self.notUpVideoView addSubview:btn];
        btn.tag = 2*baseTag + i;
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.right.mas_equalTo(i == 0 ? -80 : -15);
            make.width.mas_equalTo(48);
            make.height.mas_equalTo(28);
        }];
    }
    
}

@end
