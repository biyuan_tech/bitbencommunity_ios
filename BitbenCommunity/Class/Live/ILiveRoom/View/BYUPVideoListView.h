//
//  BYUPVideoListView.h
//  BibenCommunity
//
//  Created by 随风 on 2018/11/20.
//  Copyright © 2018 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYUPVideoListView : UIView

/** superController */
@property (nonatomic ,weak) UIViewController *superViewController;
/** stream_id 直播码 */
@property (nonatomic ,copy) NSString *stream_id;


+ (instancetype)initVideoListViewShowInView:(UIView *)view;

- (void)showAnimation;

@end

NS_ASSUME_NONNULL_END
