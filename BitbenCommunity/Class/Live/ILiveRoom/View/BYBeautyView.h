//
//  BYBeautyView.h
//  BibenCommunity
//
//  Created by 随风 on 2019/2/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYBeautyView : UIView

/** superController */
@property (nonatomic ,weak) UIViewController *superViewController;
/** 美颜回调 */
@property (nonatomic ,copy) void (^didChangeBeautyHandle)(CGFloat beauty,CGFloat white);

+ (instancetype)initViewShowInView:(UIView *)view;

- (void)showAnimation;

- (void)setBeautyValue:(CGFloat)value;
- (void)setWhiteValue:(CGFloat)value;
@end

NS_ASSUME_NONNULL_END
