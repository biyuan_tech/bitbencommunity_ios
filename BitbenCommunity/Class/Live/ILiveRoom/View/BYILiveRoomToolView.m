//
//  BYILiveRoomToolView.m
//  BY
//
//  Created by 黄亮 on 2018/8/17.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveRoomToolView.h"
#import "BYLiveConfig.h"

@interface BYILiveRoomToolView()

@property (nonatomic ,weak) UIView *fatherView;

@property (nonatomic ,strong) UIView *contentView;

// ** 麦克风 */
@property (nonatomic ,strong) UIButton *micBtn;

// ** 美颜 */
@property (nonatomic ,strong) UIButton *beautyBtn;

// ** 摄像头 */
@property (nonatomic ,strong) UIButton *cameraBtn;

// ** 闪光灯 */
@property (nonatomic ,strong) UIButton *flashlightBtn;

@end

static CGFloat contentViewH = 157.0;
static NSInteger btnTag = 0x5821;
@implementation BYILiveRoomToolView

- (instancetype)initInView:(UIView *)view{
    self = [super init];
    if (self) {
        self.fatherView = view;
        self.frame = view.bounds;
        [self setContentView];
    }
    return self;
}

- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - btnTag;
    switch (index) {
        case 0: // 麦克风
            sender.selected = [BYLiveConfig enableMic];
            break;
        case 1: // 美颜
        {
//            if (![BYLiveConfig setBeauty:6]) {
//                [BYLiveConfig setBeauty:6];
//                [BYLiveConfig setWhite:6];
//            }
//            else
//            {
//                [BYLiveConfig setBeauty:0];
//                [BYLiveConfig setWhite:0];
//            }
        }
            break;
        case 2: // 切换摄像
            [BYLiveConfig changeCameraPos];
            [self hiddenAnimation];
            break;
        case 3: // 闪光灯
            sender.selected = [BYLiveConfig enableFlashlight];
            [self hiddenAnimation];
            break;
        default:
            break;
    }
}

- (void)showAnimation{
    [self.fatherView addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-17);
        }];
        [self layoutIfNeeded];
    }];
}

- (void)hiddenAnimation{
    [UIView animateWithDuration:0.2 animations:^{
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(contentViewH);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if (touch.view == self)
        [self hiddenAnimation];
}

#pragma mark - setUI Method

- (void)setContentView{

    UIView *contentView = [UIView by_init];
    contentView.backgroundColor = kColorRGB(54, 54, 54, 0.88);
    [contentView layerCornerRadius:5 size:CGSizeMake(kCommonScreenWidth - 30, 157)];
    [self addSubview:contentView];
    _contentView = contentView;
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(contentViewH);
        make.height.mas_equalTo(contentViewH);
    }];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.contentView.frame = CGRectMake(0, 0, kCommonScreenWidth - 30, contentViewH);
    [contentView addSubview:visualEffectView];
    
    // 基础工具
    UILabel *titleLab = [UILabel by_init];
    titleLab.text = @"基础工具";
    [titleLab setBy_font:16];
    titleLab.textColor = kColorRGBValue(0xfefefe);
    [contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(titleLab.text, 16));
        make.height.mas_equalTo(17);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(23);
    }];
    NSArray *images = @[@"mic",@"beauty",@"camera",@"flashlight"];
    NSArray *titles  = @[@"麦克风",@"美颜",@"切换摄像",@"闪光灯"];
    CGFloat leftRightSpace = 36;
    CGFloat buttonWH = 45;
    CGFloat buttonSpace = (kCommonScreenWidth - 2*15 - 2*leftRightSpace - 4*buttonWH)/3;
    for (int i = 0; i < 4; i ++) {
        // 麦克风等功能按钮
        NSString *imageName_normal     = [NSString stringWithFormat:@"ilive_%@_normal",images[i]];
        NSString *imageName_hightlight = [NSString stringWithFormat:@"ilive_%@_highlight",images[i]];
        UIButton *button = [UIButton by_buttonWithCustomType];
        button.selected = NO;
        [button setBy_imageName:imageName_normal forState:UIControlStateNormal];
        [button setBy_imageName:imageName_hightlight forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:button];
        button.tag = btnTag + i;
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftRightSpace + (buttonWH + buttonSpace)*i);
            make.width.height.mas_equalTo(buttonWH);
            make.top.equalTo(titleLab.mas_bottom).with.offset(24);
        }];
        
        // 麦克风的备注
        UILabel *label = [UILabel by_init];
        [label setBy_font:12];
        label.textColor = kColorRGBValue(0xfefefe);
        label.text = titles[i];
        [contentView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(stringGetWidth(titles[i], 12));
            make.height.mas_equalTo(13);
            make.centerX.equalTo(button.mas_centerX).with.offset(0);
            make.top.equalTo(button.mas_bottom).with.offset(10);
        }];
    }
    
    [self layoutIfNeeded];
}



@end
