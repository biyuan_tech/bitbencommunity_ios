//
//  BYILiveOpeartionView.m
//  BY
//
//  Created by 黄亮 on 2018/9/13.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYILiveOpeartionView.h"
#import "BYLiveConfig.h"
#import "BYRewardView.h"
#import "BYIMManager+SendMsg.h"
#import "BYLikeAnimationView.h"
#import "BYUPVideoListView.h"
#import "BYBeautyView.h"
#import "BYSIMManager.h"
#import "BYLabel.h"

static NSInteger baseTag = 0x422;
@interface BYILiveOpeartionView()

/** 当前是否为主播 */
@property (nonatomic ,assign) BOOL isHost;

@property (nonatomic ,weak) UIViewController *target;
/** 打赏 */
@property (nonatomic ,strong) BYRewardView *rewardView;

@property (nonatomic ,strong) BYLikeAnimationView *animationView;
/** listView */
@property (nonatomic ,strong) BYUPVideoListView *listView;
/** beautyView美颜 */
@property (nonatomic ,strong) BYBeautyView *beautyView;
/** 是否已顶 */
@property (nonatomic ,assign) BOOL isUp;
/** 是否已踩 */
@property (nonatomic ,assign) BOOL isDown;


@end

@implementation BYILiveOpeartionView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.listView removeFromSuperview];
    [self.animationView removeFromSuperview];
    [self.rewardView removeFromSuperview];
    self.listView = nil;
    self.animationView = nil;
    self.rewardView = nil;
}

- (instancetype)initWithIsHost:(BOOL)isHost target:(UIViewController *)target{
    self = [super init];
    if (self) {
        self.isHost = isHost;
        self.target = target;
        self.isUp = NO;
        self.isDown = NO;
        [self setContentView];
    }
    return self;
}

- (void)setStream_id:(NSString *)stream_id{
    _stream_id = stream_id;
    _listView.stream_id = stream_id;
}

- (void)setSupperNum:(NSInteger)num{
    UILabel *supertNumLab = [self viewWithTag:3*baseTag];
    supertNumLab.text = [NSString transformIntegerShow:num];
}

- (void)setContentView{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upToVideoMembersValueDidChangle) name:NSNotification_upToVideoMembersValueDidChangle object:nil];
    if (_isHost)
        [self configHostView];
    else
        [self configGuestView];
    
}

- (void)upToVideoMembersValueDidChangle{
    if (!_isHost) return;
    UIView *bgView = [self viewWithTag:2*baseTag];
    UILabel *numLab = [self viewWithTag:2*baseTag + 1];
    if (![BYIMManager sharedInstance].upToVideoMembers.count) {
        bgView.layer.opacity = 0.0f;
        numLab.layer.opacity = 0.0f;
    }
    else{
        numLab.text = stringFormatInteger([BYIMManager sharedInstance].upToVideoMembers.count);
        bgView.layer.opacity = 1.0f;
        numLab.layer.opacity = 1.0f;
    }
}

- (void)setHostName:(NSString *)hostName{
    _hostName = hostName;
    self.rewardView.receicer_name = _hostName;
}

// 配置主播操作界面
- (void)configHostView{
    NSArray *imageNames = @[@"ilive_exit",@"ilive_share",@"ilive_switch_camera",@"ilive_up_mic",@"ilive_ beauty"];
    for (int i = 0; i < 5; i ++) {
        UIButton *button = [UIButton by_buttonWithCustomType];
        [button setBy_imageName:imageNames[i] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(hostButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -15/2, 0, 0);
        [self addSubview:button];
        button.tag = baseTag + i;
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.top.mas_equalTo(20 + 43*i);
            make.height.mas_equalTo(43);
        }];
        
        if (i == 3) {
            
            UIImageView *bgView = [[UIImageView alloc] init];
            [bgView by_setImageName:@"ilive_num_bg"];
            bgView.layer.opacity = 0.0f;
            [button addSubview:bgView];
            bgView.tag = 2*baseTag;
            [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.height.mas_equalTo(18);
                make.centerX.mas_equalTo(0);
                make.top.mas_equalTo(button.mas_bottom).mas_offset(-19);
            }];
        
            UILabel *numLab = [UILabel by_init];
            [numLab setBy_font:11];
            numLab.textColor = kTextColor_53;
            numLab.textAlignment = NSTextAlignmentCenter;
            numLab.backgroundColor = [UIColor clearColor];
            numLab.layer.opacity = 0.0f;
            [button addSubview:numLab];
            numLab.tag = 2*baseTag + 1;
            [numLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.height.mas_equalTo(18);
                make.centerX.mas_equalTo(0);
                make.top.mas_equalTo(button.mas_bottom).mas_offset(-19);
            }];
        }
    }
    
    self.listView = [BYUPVideoListView initVideoListViewShowInView:kCommonWindow];
    self.listView.superViewController = self.target;
    
    self.beautyView = [BYBeautyView initViewShowInView:kCommonWindow];
    self.beautyView.superViewController = self.target;
    @weakify(self);
    self.beautyView.didChangeBeautyHandle = ^(CGFloat beauty, CGFloat white) {
        @strongify(self);
        if (self.didChangeBeautyHandle) {
            self.didChangeBeautyHandle(beauty, white);
        }
    };
    
    CGFloat beautyValue = [[[NSUserDefaults standardUserDefaults] objectForKey:kBeautyValue] floatValue];
    CGFloat whiteValue = [[[NSUserDefaults standardUserDefaults] objectForKey:kWhiteValue] floatValue];
    [self.beautyView setBeautyValue:beautyValue/9.0];
    [self.beautyView setWhiteValue:whiteValue/9.0];
}

// 配置用户操作界面
- (void)configGuestView{
    self.rewardView = [[BYRewardView alloc] initWithFathureView:self.target.view];
    @weakify(self);
    _rewardView.confirmBtnHandle = ^(CGFloat amount) {
        @strongify(self);
        [self loadRequestReward:amount];
    };
    
    NSArray *imageNames = @[@"iliveroom_reward",@"iliveroom_top",@"iliveroom_trample"];
    for (int i = 0; i < 3; i ++) {
        UIButton *button = [UIButton by_buttonWithCustomType];
        [button setBy_imageName:imageNames[i] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(guestButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        button.tag = baseTag + i;
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-15);
            switch (i) {
                case 0:
                    make.top.mas_equalTo(0);
                    break;
                case 1:
                    make.top.mas_equalTo(60);
                    break;
                default:
                    make.top.mas_equalTo(137);
                    break;
            }
//            make.top.mas_equalTo(48*i);
            make.width.height.mas_equalTo(32);
        }];
        
        if (i == 1) {
            BYLabel *suppertNumLab = [BYLabel by_init];
            [suppertNumLab setBy_font:13];
            suppertNumLab.textAlignment = NSTextAlignmentCenter;
            suppertNumLab.labelColor = [UIColor whiteColor];
            suppertNumLab.fontStrokeColor = [UIColor colorWithWhite:0 alpha:0.6];
            suppertNumLab.fontStrokeWidth = 0.5;
            suppertNumLab.text = @"0";
            suppertNumLab.tag = 3*baseTag;
            [self addSubview:suppertNumLab];
            [suppertNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(button);
                make.top.mas_equalTo(button.mas_bottom).mas_offset(8);
                make.height.mas_equalTo(suppertNumLab.font.pointSize);
                make.centerX.mas_equalTo(button);
            }];
        };
    }

    self.animationView = [[BYLikeAnimationView alloc] init];
}

- (void)hiddenReward{
    UIButton *rewardBtn = [self viewWithTag:baseTag];
    rewardBtn.hidden = YES;
}

- (void)hostButtonAction:(UIButton *)sender{
    switch (sender.tag - baseTag) {
        case 0: // 关播
            [self showExitSheetView];
            break;
        case 1: // 分享
        {
            if (_didShareActionHandle) {
                _didShareActionHandle();
            }
        }
            break;
        case 2: // 切换摄像头
            [BYLiveConfig changeCameraPos];
            break;
        case 3: // 显示连麦列表
            [self.listView showAnimation];
            break;
        case 4: // 美颜
            [self.beautyView showAnimation];
            break;
        default:
            break;
    }
}

- (void)guestButtonAction:(UIButton *)sender{
    if (![AccountModel sharedAccountModel].hasLoggedIn) {
        [(AbstractViewController *)CURRENT_VC authorizeWithCompletionHandler:nil];
        return;
    }
    switch (sender.tag - baseTag) {
        case 0: // 打赏
            [self.rewardView showAnimation];
            break;
        case 1: // 顶
            [self loadRequestLiveGuestOpearType:BY_GUEST_OPERA_TYPE_UP];
            break;
        default: // 踩
            [self loadRequestLiveGuestOpearType:BY_GUEST_OPERA_TYPE_STAMP];
            break;
    }
    
    // 埋点
    NSInteger index = sender.tag - baseTag ;
    NSDictionary *params = @{@"房间号":self.liveId};
    if (index == 0){        // 直播打赏
        [MTAManager event:MTATypeLiveShang params:params];
    } else if (index == 1){ // 顶
        [MTAManager event:MTATypeLiveDing params:params];
    } else if (index == 2){ // 踩
        [MTAManager event:MTATypeLiveCai params:params];
    }
}

// 结束直播弹框
- (void)showExitSheetView{
//    NSString *title = @"";
    NSString *title = _model.status == BY_LIVE_STATUS_LIVING ? @"确定要结束直播吗？" : @"确定现在退出吗？";
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    @weakify(self);
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        if (self.didExitLiveBtnHandle) self.didExitLiveBtnHandle();
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [sheetController addAction:confirmAction];
    [sheetController addAction:cancelAction];
    [self.target presentViewController:sheetController animated:YES completion:nil];
}

#pragma mark - request

- (void)loadRequestLiveGuestOpearType:(BY_GUEST_OPERA_TYPE)type{
    CGPoint point = CGPointZero;
    if (type == BY_GUEST_OPERA_TYPE_UP) {
        UIButton *btn = [self viewWithTag:baseTag + 1];
        CGRect rect = [btn convertRect:btn.bounds toView:self.target.view];
        point = CGPointMake(rect.origin.x, rect.origin.y);
    }
    else{
        UIButton *btn = [self viewWithTag:baseTag + 2];
        CGRect rect = [btn convertRect:btn.bounds toView:self.target.view];
        point = CGPointMake(rect.origin.x, rect.origin.y);
    }
    
    if (type == 0 && _isUp) return; // 已经顶过
    if (type == 1 && _isDown) return; // 已经踩过
    
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestLiveGuestOpearType:type theme_id:_liveId theme_type:BY_THEME_TYPE_LIVE receiver_user_id:_hostId successBlock:^(id object) {
        @strongify(self);
        if (self.didSuppertSucHandle) {
            self.didSuppertSucHandle();
        }
        [self.animationView beginUpAniamtion:self.target.view point:point type:[@(type) integerValue]];
    } faileBlock:^(NSError *error) {
        if (type == 0)
            showToastView(@"顶操作失败", kCommonWindow);
        else
            showToastView(@"踩操作失败", kCommonWindow);
    }];
}

- (void)loadRequestReward:(CGFloat)amount{
    if ([_hostId isEqualToString:[AccountModel sharedAccountModel].account_id]) {
        showToastView(@"不能给自己打赏", self.target.view);
        return;
    }
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestReward:_hostId reward_amount:amount successBlock:^(id object) {
        @strongify(self);
        showToastView(@"打赏成功", self.target.view);
        [self.rewardView reloadSurplusBBT:amount];
        // 发送打赏消息
        [[BYIMManager sharedInstance] sendRewardMessage:amount receicer_user_Name:self.hostName receicer_user_Id:self.hostId text:nil succ:^{
        } fail:nil];
    
    } faileBlock:^(NSError *error) {
    }];
}
@end
