//
//  LaunchImageViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/27.
//  Copyright © 2018 币本. All rights reserved.
//

#import "LaunchImageViewController.h"
#import "NetworkAdapter.h"

@interface LaunchImageViewController ()
@property (nonatomic,strong)PDImageView *launchImageView;
@property (nonatomic,strong)PDImageView *logoImageView;
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UILabel *moonLabel;
@property (nonatomic,strong)UILabel *contentLabel;
@property (nonatomic,strong)UITapGestureRecognizer *tapGesture;

@end

@implementation LaunchImageViewController


-(void)dealloc{
    NSLog(@"释放");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.launchImageView];
    [self.view bringSubviewToFront:self.launchImageView];
    [self createView];
    __weak typeof(self)waekSelf = self;
    [waekSelf sendRequestGetLaunchImageInfo];
}

#pragma mark - createView
-(void)createView{
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.textColor = [UIColor whiteColor];
    self.numberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:120];
    self.numberLabel.autoresizesSubviews = YES;
    self.numberLabel.text = [NSDate getCurrentTimeWithDay];
    self.numberLabel.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:self.numberLabel];
    self.numberLabel.hidden = YES;
    
    // Moon
    self.moonLabel = [[UILabel alloc] init];
    self.moonLabel.textColor = [UIColor whiteColor];
    self.moonLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:30];
    self.moonLabel.hidden = YES;
    NSString *moon =  [NSDate getCurrentTimeWithMoon];
    NSString *realMoon = @"";
    if ([moon isEqualToString:@"1"]){
        realMoon = @"Jan.";
    } else if ([moon isEqualToString:@"2"]){
        realMoon = @"Feb.";
    } else if ([moon isEqualToString:@"3"]){
        realMoon = @"Mar.";
    } else if ([moon isEqualToString:@"4"]){
        realMoon = @"Apr.";
    } else if ([moon isEqualToString:@"5"]){
        realMoon = @"May.";
    } else if ([moon isEqualToString:@"6"]){
        realMoon = @"Jun.";
    } else if ([moon isEqualToString:@"7"]){
        realMoon = @"Jul.";
    } else if ([moon isEqualToString:@"8"]){
        realMoon = @"Aug.";
    } else if ([moon isEqualToString:@"9"]){
        realMoon = @"Sep.";
    } else if ([moon isEqualToString:@"10"]){
        realMoon = @"Oct.";
    } else if ([moon isEqualToString:@"11"]){
        realMoon = @"Nov.";
    } else if ([moon isEqualToString:@"12"]){
        realMoon = @"Dec.";
    }
    
    self.moonLabel.text = realMoon;
    [self.view addSubview:self.moonLabel];
    
    // Moon
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.textColor = [UIColor whiteColor];
    self.contentLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:20];
    self.contentLabel.numberOfLines = 0;
    [self.view addSubview:self.contentLabel];
    
    // 4. 创建手势
    self.tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(animationStopTapManager)];
    [self.view addGestureRecognizer:self.tapGesture];
}

#pragma mark - updateImageView
-(void)sendRequestGetLaunchImageInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] actionGetServerInterfaceBlock:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            // 通知首页进行刷新数据
            [[BYLiveHomeControllerV1 shareManager] reloadViewInterfaceManager];
            
            
            NSString *laungch = [AccountModel sharedAccountModel].serverModel.config.launch_image;
            [strongSelf.launchImageView uploadMainImageWithURL:laungch placeholder:[UIImage imageNamed:@"LaunchImage"] imgType:PDImgTypeTask callback:^(UIImage *image) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:3 animations:^{
                        strongSelf.launchImageView.transform = CGAffineTransformMakeScale(1.1, 1.1);
                        strongSelf.launchImageView.alpha = 0;
                        strongSelf.view.alpha = 0;
                    } completion:^(BOOL finished) {
                        [strongSelf.view removeFromSuperview];
                    }];
                });
            }];
        } else {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:3 animations:^{
                    strongSelf.launchImageView.transform = CGAffineTransformMakeScale(1.1, 1.1);
                    strongSelf.launchImageView.alpha = 0;
                    strongSelf.view.alpha = 0;
                } completion:^(BOOL finished) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.view removeFromSuperview];
                        self.view = nil;
                    });
                }];
            });
        }
    }];
}


#pragma mark - successSetting


#pragma mark - getter and setter

- (PDImageView *)launchImageView{
    
    if (_launchImageView == nil) {
        _launchImageView = [[PDImageView alloc] initWithFrame:kScreenBounds];
        _launchImageView.image = [UIImage imageNamed:@"LaunchImage"];
        _launchImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    
    return _launchImageView;
}

- (PDImageView *)logoImageView{
    
    if (_logoImageView == nil) {
        _logoImageView = [[PDImageView alloc] initWithFrame:CGRectMake(95, 458, 128, 100)];
        _logoImageView.image = [UIImage imageNamed:@"bg1.jpg"];
    }
    
    return _logoImageView;
}

#pragma mark - 手势点击页面消失
-(void)animationStopTapManager{
    [self dismissAnimationManager];
}


#pragma mark - dismissManager
-(void)dismissAnimationManager{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [UIView mdDeflateTransitionFromView:self.view toView:keywindow.rootViewController.view originalPoint:keywindow.center duration:.9f completion:^{
        
    }];
}



@end
