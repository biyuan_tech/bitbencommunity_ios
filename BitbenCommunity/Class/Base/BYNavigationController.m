//
//  BYNavigationController.m
//  BY
//
//  Created by 黄亮 on 2018/7/25.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYNavigationController.h"
#import <RTRootNavigationController.h>
#import "UIScrollView+Gesture.h"

@interface BYNavigationController ()<UIGestureRecognizerDelegate>


// ** 返回手势
@property (nonatomic ,strong) UIPanGestureRecognizer *panGestureRecognizer;

@property (nonatomic ,weak) UIViewController *popController;


@end
#pragma clang diagnostic ignored "-Wundeclared-selector"
@implementation BYNavigationController

- (void)setPopGestureRecognizer:(BOOL)popGestureRecognizer{
    objc_setAssociatedObject(self, @selector(handleNavigationTransition:), @(popGestureRecognizer), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)popGestureRecognizer{
    return [objc_getAssociatedObject(self, @selector(handleNavigationTransition:)) boolValue];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.delegate = self;
    // 取消navigationBar半透明
    [[UINavigationBar appearance] setTranslucent:NO];
    // 修改标题的属性
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica-Light" size:15],NSFontAttributeName,kNaTitleColor_53,NSForegroundColorAttributeName, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:attributes];
    // 设置全屏手势返回
    id target = self.interactivePopGestureRecognizer.delegate;
    self.interactivePopGestureRecognizer.enabled = NO;
    SEL handler = NSSelectorFromString(@"handleNavigationTransition:");
    self.panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:target action:handler];
    _panGestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:_panGestureRecognizer];
    self.modalPresentationStyle = UIModalPresentationFullScreen;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    viewController.hidesBottomBarWhenPushed = self.viewControllers.count >= 1 ? YES : NO;
    [super pushViewController:viewController animated:animated];
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated{
    self.popController = self.navigationController.topViewController;
    return [super popViewControllerAnimated:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
//    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]){
//        CGPoint point = [touch locationInView:gestureRecognizer.view];
//        if (point.x < kCommonScreenWidth/2) {
//            return YES;
//        } else {
//            return NO;
//        }
//    }
//    
//    return YES;
//}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if (!self.popGestureRecognizer) {
        return NO;
    }
    if (self.rt_viewControllers.count <= 1) {
        return NO;
    }
//    if (self.interactivePopGestureRecognizer &&
//        [[self.interactivePopGestureRecognizer.view gestureRecognizers] containsObject:gestureRecognizer]) {
//        
//        CGPoint tPoint = [(UIPanGestureRecognizer *)gestureRecognizer translationInView:gestureRecognizer.view];
//        
//        if (tPoint.x >= 0) {
//            CGFloat y = fabs(tPoint.y);
//            CGFloat x = fabs(tPoint.x);
//            CGFloat af = 30.0f/180.0f * M_PI;
//            CGFloat tf = tanf(af);
//            if ((y/x) <= tf) {
//                return YES;
//            }
//            return NO;
//            
//        }else{
//            return NO;
//        }
//    }
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(nonnull UIGestureRecognizer *)otherGestureRecognizer{
    if (gestureRecognizer == self.panGestureRecognizer) {
        return NO;
    }
    return YES;
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated{
    // 判断手势返回是否成功
    __block BOOL popGestureRecognizeFaile = YES;
    __block BOOL isPopGestureRecognizer = NO;
      id <UIViewControllerTransitionCoordinator>transitionCoordinator = navigationController.topViewController.transitionCoordinator;
    if (@available(iOS 10.0, *)) {
        [transitionCoordinator notifyWhenInteractionChangesUsingBlock:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
            popGestureRecognizeFaile = [context isCancelled];
            isPopGestureRecognizer = YES;
            // 手势返回成功调用popGestureRecognizerAction
            if (!popGestureRecognizeFaile && isPopGestureRecognizer) {
                [self notifyPopGestureRecognizerSuccess:YES];
            }
        }];
    } else {
        [transitionCoordinator notifyWhenInteractionEndsUsingBlock:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
            popGestureRecognizeFaile = [context isCancelled];
            isPopGestureRecognizer = YES;
            // 手势返回成功调用popGestureRecognizerAction
            if (!popGestureRecognizeFaile && isPopGestureRecognizer) {
                [self notifyPopGestureRecognizerSuccess:YES];
            }
        }];
    }
}

- (void)notifyPopGestureRecognizerSuccess:(BOOL)success{
    if (!_popController) {
        return;
    }
    if ([self.popController respondsToSelector:@selector(popGestureRecognizerAction)]) {
        [self.popController performSelector:@selector(popGestureRecognizerAction)];
    }
}

- (BOOL)shouldAutorotate {
    if ([self.topViewController isKindOfClass:NSClassFromString(@"RTContainerController")]) {
        RTContainerController *viewController = (RTContainerController *)self.topViewController;
        return viewController.contentViewController.shouldAutorotate;
    }
    return self.topViewController.shouldAutorotate;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if ([self.topViewController isKindOfClass:NSClassFromString(@"RTContainerController")]) {
        RTContainerController *viewController = (RTContainerController *)self.topViewController;
        return viewController.contentViewController.supportedInterfaceOrientations;
    }
    return self.topViewController.supportedInterfaceOrientations;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    if ([self.topViewController isKindOfClass:NSClassFromString(@"RTContainerController")]) {
        RTContainerController *viewController = (RTContainerController *)self.topViewController;
        return viewController.contentViewController.preferredInterfaceOrientationForPresentation;
    }
    return self.topViewController.preferredInterfaceOrientationForPresentation;
}



@end
