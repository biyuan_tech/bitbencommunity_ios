//
//  BYBaseViewController.h
//  BY
//
//  Created by 黄亮 on 2018/7/25.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"

@interface BYBaseViewController : AbstractViewController

/**
 标题色值
 */
@property (nonatomic, strong) UIColor *titleColor;

@property (nonatomic , copy) NSString *navigationTitle;

/**
 是否启动手势返回（默认YES）
 */
@property (nonatomic ,assign) BOOL popGestureRecognizerEnale;

/**
 是否显示navigationBar的阴影横线
 */
//@property (nonatomic ,assign) BOOL showNavigationBarShadow;

/**
 重写返回按钮点击事件
 */
- (void)backAction;

/**
 监听返回事件，包括手势返回（无法重写返回事件）
 */
- (void)popAction;

@end
