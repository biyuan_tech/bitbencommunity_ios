//
//  BYBaseViewController.m
//  BY
//
//  Created by 黄亮 on 2018/7/25.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYBaseViewController.h"
#import "UIBarButtonItem+BYBarButtonItem.h"
#import "UIImage+BYExtension.h"

@interface BYBaseViewController ()

@end

#pragma clang diagnostic ignored "-Wundeclared-selector"
@implementation BYBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.barMainTitle = self.navigationTitle;
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
//    self.popGestureRecognizerEnale = YES;
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (@available(iOS 11.0, *)) {
        if (IS_PhoneXAll) {
            self.additionalSafeAreaInsets = UIEdgeInsetsMake(kSafeAreaInsetsTop, 0, -kSafeAreaInsetsBottom, 0);
        }
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.popGestureRecognizerEnale = YES;

//    if (_showNavigationBarShadow) {
//        // 设置顶部灰线
//        [[UINavigationBar appearance] setShadowImage:[UIImage imageWithColor:[UIColor colorWithWhite:0 alpha:0.8]]];
//    }
//    else
//    {
//        [[UINavigationBar appearance] setShadowImage:[UIImage new]];
//    }
//
//    if (self == self.rt_navigationController.rt_viewControllers[0]) {
//        [self hiddenBackBarButtonItem:YES];
//    }
//    else
//    {
//        [self hiddenBackBarButtonItem:NO];
//    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)addBackBarButtonItem{
    UIBarButtonItem *leftBarButtonItem = [UIBarButtonItem initWithImageName:@"common_barItem_back" target:self action:@selector(backAction) type:BYBarButtonItemTypeLeft];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    self.navigationItem.backBarButtonItem = nil;
}

- (void)hiddenBackBarButtonItem:(BOOL)hidden{
    hidden ? self.navigationItem.leftBarButtonItem = nil : [self addBackBarButtonItem];
}

- (void)setTitleColor:(UIColor *)titleColor{
    _titleColor = titleColor;
    self.barMainTitleLabel.textColor = titleColor;
}

- (void)setNavigationTitle:(NSString *)navigationTitle{
    _navigationTitle = navigationTitle;
    self.barMainTitle = navigationTitle;
}

// 按钮点击返回执行此方法
- (void)backAction{
    [self popAction];
}

// 手势滑动返回执行此方法
- (void)popGestureRecognizerAction{
    [self popAction];
}

- (void)popAction{
    
}

- (void)setPopGestureRecognizerEnale:(BOOL)popGestureRecognizerEnale{
    _popGestureRecognizerEnale = popGestureRecognizerEnale;
    @synchronized(self){
        if (!self.rt_navigationController) return;
        objc_setAssociatedObject(self.rt_navigationController, @selector(handleNavigationTransition:), @(popGestureRecognizerEnale), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}

//- (void)setShowNavigationBarShadow:(BOOL)showNavigationBarShadow{
//    _showNavigationBarShadow = showNavigationBarShadow;
//}


- (BOOL)prefersHomeIndicatorAutoHidden{
    return YES;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

@end
