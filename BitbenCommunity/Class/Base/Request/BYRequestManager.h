//
//  BYRequestManager.h
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,BYOperationType) {
    BYOperationTypeGet,
    BYOperationTypePost
};

@interface BYRequestManager : NSObject

/**
 单例创建

 @return 单例
 */
+ (id)shareManager;

/**
 发起请求

 @param URLString 请求URL地址
 @param parameters 传入参数
 @param operationType 操作类型（get/post）
 @param successBlock 成功回调
 @param failureBlock 失败回调
 */
+ (void)ansyRequestWithURLString:(NSString *)URLString
                      parameters:(NSDictionary *)parameters
                   operationType:(BYOperationType)operationType
                    successBlock:(void(^)(id result))successBlock
                    failureBlock:(void(^)(NSError *error))failureBlock;

@end
