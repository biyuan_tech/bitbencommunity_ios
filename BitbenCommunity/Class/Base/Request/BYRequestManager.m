//
//  BYRequestManager.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYRequestManager.h"
#import <AFNetworking.h>
#import "NetworkAdapter.h"

@interface BYRequestManager()

@end

// 网络超时时间
static double const networkDefultTimeoutInterval = 30.0f;
// 手机网络情况下超时时间
//static double networkWLANTimeoutInterval = 30.0f;
// wifi情况下超时时间
//static double networkWifiTimeoutInterval = 15.0f;

@implementation BYRequestManager

+ (AFHTTPSessionManager *)requestSessionManager{
    static AFHTTPSessionManager *staticRequestSessionManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        staticRequestSessionManager = [[AFHTTPSessionManager alloc] init];
        staticRequestSessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        staticRequestSessionManager.requestSerializer.HTTPShouldHandleCookies = YES;
//        staticRequestSessionManager.responseSerializer.acceptableContentTypes =[NSSet setWithObjects:@"application/json",@"text/json",@"text/javascript",@"text/plain",nil];
        staticRequestSessionManager.requestSerializer.timeoutInterval = networkDefultTimeoutInterval;
        // 允许https校验
        [staticRequestSessionManager.securityPolicy setAllowInvalidCertificates:YES];
        
    });
    return staticRequestSessionManager;
}

//+ (instancetype)allocWithZone:(struct _NSZone *)zone{
//    static BYRequestManager *requestManager;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        if (!requestManager) {
//            requestManager = [super allocWithZone:zone];
//        }
//    });
//    return requestManager;
//}
//
//- (id)copyWithZone:(NSZone *)zone{
//    return [BYRequestManager shareManager];
//}

+ (id)shareManager{
    static BYRequestManager *requestManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!requestManager) {
            requestManager = [[BYRequestManager alloc] init];
        }
    });
    return requestManager;
}

+ (void)ansyRequestWithURLString:(NSString *)URLString
                      parameters:(NSDictionary *)parameters
                   operationType:(BYOperationType)operationType
                    successBlock:(void (^)(id))successBlock
                    failureBlock:(void (^)(NSError *))failureBlock{
    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
//    PDLog(@"manager.dataTasks--%@",manager.dataTasks);
//    PDLog(@"tasks--%@",manager.tasks);
//    PDLog(@"--%@",manager);
//
    [[BYRequestManager shareManager] ansyRequestWithURLString:URLString parameters:parameters operationType:operationType successBlock:successBlock failureBlock:failureBlock];
}

// 创建请求
- (void)ansyRequestWithURLString:(NSString *)URLString
                      parameters:(NSDictionary *)parameters
                   operationType:(BYOperationType)operationType
                    successBlock:(void (^)(id))successBlock
                    failureBlock:(void (^)(NSError *))failureBlock{
    
//    AFHTTPSessionManager *sessionManager = [BYRequestManager requestSessionManager];
//    NSString *requestURLString = [NSString stringWithFormat:@"%@%@",@"http://192.168.31.62:8080",URLString]; //62
//    NSString *URLStr = [[NSURL URLWithString:requestURLString relativeToURL:sessionManager.baseURL] absoluteString];
//    NSString *opeartion = operationType == BYOperationTypeGet ? @"GET" : @"POST";
//    // 采用json序列化格式请求
//    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:opeartion URLString:URLStr parameters:parameters error:nil];
//    NSURLSessionDataTask *dataTask = [sessionManager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//        if (!error) {
//            // 现返回的为纯json字符串，需要转两次
//            NSString *jsonStr =  [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
//            NSData *data = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
//            NSDictionary *respondDic =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
//            PDLog(@"respondObject -- \n %@",respondDic);
//            if (successBlock) successBlock(respondDic);
//        }
//        else
//        {
//            if (failureBlock) failureBlock(error);
//        }
//    }];
//    [dataTask resume];
    
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:URLString requestParams:parameters responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        PDLog(@"responseObject: %@",responseObject);
        if (isSucceeded) {
            if (successBlock) {
                successBlock(responseObject);
            }
        }
        else
        {
            if (failureBlock) {
                failureBlock(error);
            }
        }
    }];
    
}





@end
