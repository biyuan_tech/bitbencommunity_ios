//
//  PushNotifManager.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/25.
//  Copyright © 2018 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PushNotifManager : FetchModel

@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *type;
@property (nonatomic,copy)NSString *identify;

+(void)showMessageWithTitle:(NSString *)title desc:(NSString *)desc backBlock:(void(^)())block;

@end

NS_ASSUME_NONNULL_END
