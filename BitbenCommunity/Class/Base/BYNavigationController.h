//
//  BYNavigationController.h
//  BY
//
//  Created by 黄亮 on 2018/7/25.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <RTRootNavigationController.h>
@interface BYNavigationController : RTRootNavigationController

// ** 返回手势开启
@property (nonatomic, assign) BOOL popGestureRecognizer;
@end
