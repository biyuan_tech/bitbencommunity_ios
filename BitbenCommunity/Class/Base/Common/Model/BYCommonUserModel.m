//
//  BYCommonUserModel.m
//  BY
//
//  Created by 黄亮 on 2018/8/10.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYCommonUserModel.h"

@implementation BYCommonUserModel

+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    return [BYCommonUserModel shareObject];
}

+ (instancetype)shareObject{
    static BYCommonUserModel *userModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        userModel = [[super allocWithZone:NULL] init];
    });
    return userModel;
}

- (id)copyWithZone:(NSZone *)zone{
    return [BYCommonUserModel shareObject];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.user_id = 7001;
        self.user_roomId = @"7001";
        self.user_headImgURL = @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1532606738980&di=c5bef9564963b43f6a2394fc822e810a&imgtype=0&src=http%3A%2F%2Fimgtu.5011.net%2Fuploads%2Fcontent%2F20170209%2F4934501486627131.jpg";
        self.user_name = @"Belief";
    }
    return self;
}

@end
