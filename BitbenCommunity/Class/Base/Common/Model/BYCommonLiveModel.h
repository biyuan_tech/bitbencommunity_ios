//
//  BYCommonLiveModel.h
//  BY
//
//  Created by 黄亮 on 2018/9/12.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYLiveDefine.h"
#import "BYCommonModel.h"
#import "BYCommonShareModel.h"

//@protocol BYCommonLiveGuestModel <NSObject>
//
//
//@end

@interface BYCommonLiveGuestModel : NSObject

/** 用户id */
@property (nonatomic ,copy) NSString *user_id;
/** 用户头像 */
@property (nonatomic ,copy) NSString *head_img;
/** 用户简介 */
@property (nonatomic ,copy) NSString *intro;
/** 用户昵称 */
@property (nonatomic ,copy) NSString *nickname;
/** 关注状态 */
@property (nonatomic ,assign) BOOL isAttention;
/** 绑定的直播id */
@property (nonatomic ,copy) NSString *live_recoed_id;
/** 用户类型 */
@property (nonatomic ,assign) BY_NEWLIVE_MICRO_TYPE user_type;
/** 身份类型 */
@property (nonatomic ,assign) BY_IDENTITY_TYPE identity_type;

@end


@interface BYCommonLiveModel : BYCommonModel

+ (instancetype)shareManager;

@property (nonatomic ,copy) NSString *user_id;
@property (nonatomic ,copy) NSString *room_id;
@property (nonatomic ,copy) NSString *group_id;
/** 直播记录id */
@property (nonatomic ,copy) NSString *live_record_id;
/** 头像 */
@property (nonatomic ,copy) NSString *head_img;
@property (nonatomic ,copy) NSString *nickname;
/** 组织形式 */
@property (nonatomic ,assign) BY_LIVE_ORGAN_TYPE organ_type;
/** 微信号 */
@property (nonatomic, copy) NSString *wechat;
/** 直播时间 */
@property (nonatomic ,copy) NSString *begin_time;
@property (nonatomic ,copy) NSString *show_begin_time;
/** 直播简介显示的直播时间 */
@property (nonatomic ,copy) NSString *intro_begin_time;
/** 实际开播时间 */
@property (nonatomic ,copy) NSString *real_begin_time;
/** 直播形式 */
@property (nonatomic ,assign) BY_NEWLIVE_TYPE live_type;
/** 直播间标题 */
@property (nonatomic ,copy) NSString *room_title;
/** 直播间封面图 */
@property (nonatomic ,copy) NSString *cover_url;
/** 直播间简介 */
@property (nonatomic ,copy) NSString *room_intro;
/** 粉丝数 */
@property (nonatomic ,assign) NSInteger fans;
/** 直播标题 */
@property (nonatomic ,copy) NSString *live_title;
/** 直播收入 */
@property (nonatomic ,assign) NSInteger live_income;
/** 昨日收入 */
@property (nonatomic ,copy) NSString *yesterday_live_income;
/** 总收入 */
@property (nonatomic ,copy) NSString *total_live_income;
/** 直播封面图 */
@property (nonatomic ,copy) NSString *live_cover_url;
/** 直播宣传图 */
@property (nonatomic ,copy) NSString *ad_img;
/** 直播简介 */
@property (nonatomic ,copy) NSString *live_intro;
/** 直播状态 */
@property (nonatomic ,assign) BY_LIVE_STATUS status;
/** 主讲人 */
@property (nonatomic ,copy) NSString *speaker;
/** 主讲人头像 */
@property (nonatomic ,copy) NSString *speaker_head_img;

@property (nonatomic ,strong) NSArray *courseware;
/** 互动直播回放url */
@property (nonatomic ,copy) NSString *video_url;
/** 直播播放地址 */
@property (nonatomic ,copy) NSString *play_url;
/** 直播播放地址组 */
@property (nonatomic ,strong) NSDictionary *play_urls;

@property (nonatomic ,assign) NSInteger watch_times;
/** 浏览数 */
@property (nonatomic ,assign) NSInteger count_uv;

@property (nonatomic ,strong) BYCommonShareModel *shareMap;
/** 奖励的bp */
@property (nonatomic ,copy) NSString *reward;
/** 话题 */
@property (nonatomic ,strong) NSArray *topic_content;
/** 关注状态(直播的关注) */
@property (nonatomic ,assign) BOOL attention;
/** 对人的关注状态 */
@property (nonatomic ,assign) BOOL isAttention;
/** 预约人次 */
@property (nonatomic ,assign) NSInteger attention_count;
/** 是否顶 */
@property (nonatomic ,assign) BOOL isSupport;
/** 评论数 */
@property (nonatomic ,assign) NSInteger comment_count;
/** 支持数 */
@property (nonatomic ,assign) NSInteger count_support;
/** 收藏状态 */
@property (nonatomic ,assign) BOOL collection_status;
/** usersig */
@property (nonatomic ,copy) NSString *userSig;
@property (nonatomic ,assign) NSInteger cert_badge;
/** 成就组 */
@property (nonatomic ,strong) NSArray <NSString *>*achievement_badge_list;
/** 微直播类型 */
@property (nonatomic ,assign) BY_NEWLIVE_MICRO_TYPE scene_type;
/** 主持人 */
@property (nonatomic ,strong) NSArray <BYCommonLiveGuestModel *> *host_list;
/** 嘉宾 */
@property (nonatomic ,strong) NSArray <BYCommonLiveGuestModel *> *interviewee_list;
/** 提示 */
@property (nonatomic ,copy) NSString *remark;
/** 工作人员微信 */
@property (nonatomic ,copy) NSString *assistant_wechat;


@end
