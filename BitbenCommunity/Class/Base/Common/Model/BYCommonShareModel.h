//
//  BYCommonShareModel.h
//  BibenCommunity
//
//  Created by 随风 on 2018/10/20.
//  Copyright © 2018 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYCommonShareModel : FetchModel

/** 分享图片 */
@property (nonatomic ,copy) NSString *share_img;
/** 分享简介 */
@property (nonatomic ,copy) NSString *share_intro;
/** 分享参数 */
@property (nonatomic ,copy) NSString *share_param;
/** 分享标题 */
@property (nonatomic ,copy) NSString *share_title;
/** 分享url */
@property (nonatomic ,copy) NSString *share_url;

@end

NS_ASSUME_NONNULL_END
