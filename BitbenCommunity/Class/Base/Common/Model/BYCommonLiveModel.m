//
//  BYCommonLiveModel.m
//  BY
//
//  Created by 黄亮 on 2018/9/12.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonLiveModel.h"

@implementation BYCommonLiveGuestModel


@end

static BYCommonLiveModel *_liveModel;
@implementation BYCommonLiveModel

//+ (instancetype)shareManager{
//    return [[BYCommonLiveModel alloc] init];
//}
//
//+ (instancetype)allocWithZone:(struct _NSZone *)zone{
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        liveModel = [super allocWithZone:zone];
//    });
//    return liveModel;
//}

+ (instancetype)mj_objectWithKeyValues:(id)keyValues{
    BYCommonLiveModel *model = [super mj_objectWithKeyValues:keyValues];
    if ([keyValues[@"host_list"] count]) {
        model.host_list = [[BYCommonLiveGuestModel mj_objectArrayWithKeyValuesArray:keyValues[@"host_list"]] copy];
    }
    if ([keyValues[@"interviewee_list"] count]) {
        model.interviewee_list = [[BYCommonLiveGuestModel mj_objectArrayWithKeyValuesArray:keyValues[@"interviewee_list"]] copy];
    }
    model.shareMap = [BYCommonShareModel mj_objectWithKeyValues:keyValues[@"shareMap"]];
    return model;
}

+ (instancetype)shareManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _liveModel = [[BYCommonLiveModel alloc] init];
    });
    return _liveModel;
}

@end
