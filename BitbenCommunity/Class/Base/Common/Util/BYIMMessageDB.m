//
//  BYIMMessageDB.m
//  BibenCommunity
//
//  Created by 随风 on 2018/12/25.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYIMMessageDB.h"
#import <FMDB.h>
#import "BYHistoryIMModel.h"

@interface BYIMMessageDB ()

/** db */
@property (nonatomic ,strong) FMDatabase *db;


@end

@implementation BYIMMessageDB

+ (id)shareManager{
    static BYIMMessageDB *dbManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dbManager = [[BYIMMessageDB alloc] init];
    });
    return dbManager;
}

- (FMDatabase *)getBaseDb{
    NSString *doc = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *name = [NSString stringWithFormat:@"voiceMsg.sqlite"];
    NSString *file = [doc stringByAppendingPathComponent:name];
    FMDatabase *db = [FMDatabase databaseWithPath:file];
    if ([db open]) {
        BOOL result = [db executeUpdate:@"CREATE TABLE IF NOT EXISTS msgCache (id text NOT NULL , isRead integer NOT NULL , storeTime datetime NOT NULL)"];
        if (!result) {
            showDebugToastView(@"消息表创建失败！", kCommonWindow);
        }
    }
    return db;
}

- (void)addIMMessages:(BYHistoryIMModel *)model{
    FMResultSet *resultSet = [self.db executeQuery:@"select * from msgCache where id=?;",model.messageId];
    if ([resultSet next]) {

        return;
    }
    NSDate *date = [NSDate by_date];
    [self.db executeUpdate:@"INSERT INTO msgCache(id,isRead,storeTime) VALUES (?,?,?)",model.messageId,@(0),date];
//    NSString *error = [self.db lastErrorMessage];
//    int errorCode = [self.db lastErrorCode];
}

- (void)modifyIMMessageIsRead:(NSString *)msg_id{
    if (!msg_id.length) return;
    FMResultSet *resultSet = [self.db executeQuery:@"select * from msgCache where id=?;",msg_id];
    if ([resultSet next]) {
        [self.db executeUpdate:@"UPDATE msgCache SET isRead = ? WHERE id = ?;",@(1),msg_id];
        return;
    }
    showDebugToastView(@"未找到该语音消息", kCommonWindow);
}

- (id)selectMsgId:(NSString *)msgId msg_id:(NSString *)msg_id{
    return nil;
}

- (BOOL)selectIsReadWhereMsgId:(NSString *)msg_id{
    if (!msg_id.length) return NO;
    FMResultSet *resultSet = [self.db executeQuery:@"select * from msgCache where id=?;",msg_id];
    if ([resultSet next]) {
        BOOL isRead = [resultSet boolForColumn:@"isRead"];
        return isRead;
    }
    return NO;
}


- (void)closeDB{
    if ([self.db open]) {
        [self.db close];
    }
}

- (FMDatabase *)db{
    if (!_db) {
        _db = [self getBaseDb];
    }
    return _db;
}
@end
