//
//  BYIMMessageDB.h
//  BibenCommunity
//
//  Created by 随风 on 2018/12/25.
//  Copyright © 2018 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class BYHistoryIMModel;
@interface BYIMMessageDB : NSObject

+ (id)shareManager;

//- (void)saveIMMessages:(NSArray *)msgs record_id:(NSString *)record_id;
/** 聊天消息的存储（尾部追加） */
- (void)addIMMessages:(BYHistoryIMModel *)model;
/** 修改聊天消息的已读状态 */
- (void)modifyIMMessageIsRead:(NSString *)msg_id;
/** 获取单条消息 */
- (id)selectMsgId:(NSString *)msgId msg_id:(NSString *)msg_id;
/**  查询已读状态 */
- (BOOL)selectIsReadWhereMsgId:(NSString *)msg_id;

- (void)closeDB;

@end

NS_ASSUME_NONNULL_END
