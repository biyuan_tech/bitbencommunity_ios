//
//  BYServiceManager.m
//  BibenCommunity
//
//  Created by 随风 on 2018/12/14.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYServiceManager.h"


@implementation BYServiceManager

/** 注册获取默认值 */
+ (void)registerSettingDefultValue{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    //读取Settings.bundle里面的配置信息
    NSDictionary *settingsDict = [NSDictionary dictionaryWithContentsOfFile:[path stringByAppendingPathComponent:@"Root.plist"]];
    
    NSArray *preferences = [settingsDict objectForKey:@"PreferenceSpecifiers"];
    
    
    NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
    
    for(NSDictionary *prefSpecification in preferences){
        NSString *key = [prefSpecification objectForKey:@"Key"];
        
        if(key.length){
            //加载默认值
            [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
        }
        if ([key isEqualToString:@"kServicePort"]) {
            NSArray *titles = [prefSpecification objectForKey:@"Titles"];
            NSArray *values = [prefSpecification objectForKey:@"Values"];
            if (titles.count) {
                for (int i = 0; i < titles.count ; i ++) {
                    [defaultsToRegister setObject:values[i] forKey:titles[i]];
                }
            }
        }
    }
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
/** 获取网关 */
+ (NSString *)getServiceHost{
#if DEBUG
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    if ([[NSFileManager defaultManager] fileExistsAtPath: path])
    {
        NSString *serviceType = [userDefaults objectForKey:@"kServiceHost"];
        if ([serviceType isEqualToString:@"本地1"])
        {
            return [userDefaults objectForKey:@"kServiceLocal1"];
        }
        else if([serviceType isEqualToString:@"本地2"])
        {
            return [userDefaults objectForKey:@"kServiceLocal2"];
        }
        else if ([serviceType isEqualToString:@"测试"])
        {
            return [userDefaults objectForKey:@"kServiceTest"];
        }
        else if ([serviceType isEqualToString:@"线上"])
        {
            return [userDefaults objectForKey:@"kServiceOnline"];
        }
        else if([serviceType isEqualToString:@"自定义"])
        {
            return [userDefaults objectForKey:@"kDefine"];
        }
    }
    return nil;
#else
    return @"47.244.112.202";
#endif
}

/** 获取端口 */
+ (NSString *)getServicePort{
#if DEBUG
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    if ([[NSFileManager defaultManager] fileExistsAtPath: path])
    {
        NSString *serviceType = [userDefaults objectForKey:@"kServicePort"];
        if ([serviceType isEqualToString:@"9000"])
        {
            return [userDefaults objectForKey:@"9000"];
        }
        else if([serviceType isEqualToString:@"8080"])
        {
            return [userDefaults objectForKey:@"8080"];
        }
    }
    return nil;
#else
    return @"9000";
#endif
}

// 默认线上socket端口与ip
static NSString * const socketHost = @"47.244.112.202";
static NSInteger  const socketPort = 9710;

/** 获取socket网关 */
+ (NSString *)getSocketHost{
#if DEBUG
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    if ([[NSFileManager defaultManager] fileExistsAtPath: path])
    {
        NSString *serviceType = [userDefaults objectForKey:@"kServiceHost"];
        if ([serviceType isEqualToString:@"本地1"])
        {
            return [userDefaults objectForKey:@"kServiceLocal1"];
        }
        else if([serviceType isEqualToString:@"本地2"])
        {
            return [userDefaults objectForKey:@"kServiceLocal2"];
        }
        else if ([serviceType isEqualToString:@"测试"])
        {
//            return @"47.99.209.76"; // 直连
            return @"47.97.211.170"; // nginx转发
        }
        else if ([serviceType isEqualToString:@"线上"])
        {
//            return @"47.244.112.55";
//            return @"47.244.112.202";
            return socketHost;
        }
        else if([serviceType isEqualToString:@"自定义"])
        {
            return socketHost;
        }
    }
    return nil;
#else
    return socketHost;
#endif
}
/** 获取socket端口 */
+ (NSInteger )getSocketPort{
#if DEBUG
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    if ([[NSFileManager defaultManager] fileExistsAtPath: path])
    {
        NSString *serviceType = [userDefaults objectForKey:@"kServiceHost"];
        if ([serviceType isEqualToString:@"本地1"] ||
            [serviceType isEqualToString:@"本地2"])
        {
            return socketPort;
        }
        else if ([serviceType isEqualToString:@"测试"])
        {
            return socketPort;
        }
        else if ([serviceType isEqualToString:@"线上"])
        {
            return socketPort;
        }
    }
    return socketPort;
#else
    return socketPort;
#endif
}


@end
