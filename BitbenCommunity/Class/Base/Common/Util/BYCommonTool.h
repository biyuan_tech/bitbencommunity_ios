//
//  BYCommonTool.h
//  BibenCommunity
//
//  Created by 随风 on 2018/10/15.
//  Copyright © 2018 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYCommonTool : NSObject

/** 单例创建 */
+ (instancetype)shareManager;

/** 倒计时(param: days:天 hours:时 minute:分 second:秒) */
- (void)timerCutdown:(NSString *)beginTime cb:(void(^)(NSDictionary *param,BOOL isFinish))cb;

/** 计时器 */
- (void)timerBeginTime:(NSString *)beginTime cb:(void(^)(NSDictionary *param))cb;
- (void)stopTimer;

#pragma mark - 美颜设置

/** 设置默认的美颜值 */
- (void)setDefultBeautyValue;

/** 弹出删除视图 */
+ (void)showDelSheetViewInCurrentView:(NSString *)theme_id theme_type:(BY_THEME_TYPE)theme_type cb:(void(^)(void))cb;

/** 获取成就参数 */
+ (ServerBadgeModel *)getAchievementData:(NSString *)bagdId;

/** 获取vip参数 */
+ (ServerBadgeModel *)getVipImageName:(NSString *)bagdId;

/** 跳转三方地图 */
+ (void)openMapWithLocation:(NSString *)location;

/** 绘制文章分享长图*/
+ (void)drawAricleShareImage:(NSString *)content time:(NSTimeInterval)time articleId:(NSString *)articleId;

@end

NS_ASSUME_NONNULL_END
