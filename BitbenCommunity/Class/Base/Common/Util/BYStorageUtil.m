//
//  BYStorageUtil.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYStorageUtil.h"

@implementation BYStorageUtil

+ (id)shareObject{
    static BYStorageUtil *storageUtil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        storageUtil = [[BYStorageUtil alloc] init];
    });
    return storageUtil;
}

+ (void)writeFileToPlist:(id)object key:(NSString *)key{
    NSString *filename=[[self shareObject] getPlistFilePath:key];   //获取路径
    NSDictionary *dic = [NSDictionary dictionaryWithObject:object forKey:key];
    //写到plist文件里
    [dic writeToFile:filename atomically:YES];
}

+ (id)readFileToPlist:(NSString *)key{
    NSString *filename=[[self shareObject] getPlistFilePath:key];   //获取路径
    //读取数据
    NSDictionary * dic = [NSDictionary dictionaryWithContentsOfFile:filename];
    return dic[key];
}

+ (void)removeFileToPlist:(NSString *)key{
    NSString *filename=[[self shareObject] getPlistFilePath:key];   //获取路径
    //读取数据
    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithContentsOfFile:filename];
    [dic removeObjectForKey:key];
    [dic writeToFile:filename atomically:YES];
}

- (NSString *)getPlistFilePath:(NSString *)fileKey{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *path=[paths    objectAtIndex:0];
    NSString *filename=[path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",fileKey]];   //获取路径
    return filename;
}

@end
