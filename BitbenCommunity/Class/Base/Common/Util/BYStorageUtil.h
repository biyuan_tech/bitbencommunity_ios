//
//  BYStorageUtil.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYStorageUtil : NSObject

/**
 *  存储数据到plist中
 *
 *  @param object 存储对象
 *  @param key    key
 */
+ (void)writeFileToPlist:(id)object key:(NSString *)key;

/**
 *  从plist中读取数据
 *
 *  @param key key
 *
 *  @return id
 */
+ (id)readFileToPlist:(NSString *)key;

/**
 *  从plist中移除某数据
 *
 *  @param key key description
 */
+ (void)removeFileToPlist:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
