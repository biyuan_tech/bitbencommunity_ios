//
//  BYCommonTool.m
//  BibenCommunity
//
//  Created by 随风 on 2018/10/15.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYCommonTool.h"
#import "BYLiveRPCDefine.h"
#import <MapKit/MapKit.h>

@interface BYCommonTool ()<WKUIDelegate,
WKNavigationDelegate,
WKScriptMessageHandler>

@property (nonatomic ,strong) dispatch_source_t timer;

@end

@implementation BYCommonTool

+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    return [BYCommonTool shareManager];
}

+ (instancetype)shareManager{
    static BYCommonTool *userModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        userModel = [[super allocWithZone:NULL] init];
    });
    return userModel;
}

- (id)copyWithZone:(NSZone *)zone{
    return [BYCommonTool shareManager];
}

- (void)timerCutdown:(NSString *)beginTime cb:(void(^)(NSDictionary *param,BOOL isFinish))cb{
    NSDate *endDate = [NSDate by_dateFromString:beginTime dateformatter:K_D_F];
    NSInteger timeDif = [NSDate by_startTime:[NSDate by_date] endTime:endDate];
    if (_timer) {
        [self stopTimer];
    }
    if (_timer == nil) {
        __block NSInteger timeout = timeDif; // 倒计时时间
        if (timeout!=0) {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            self.timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
            dispatch_source_set_timer(self.timer, dispatch_walltime(NULL, 0), 1.0*NSEC_PER_SEC,  0); //每秒执行
            dispatch_source_set_event_handler(self.timer, ^{
                if(timeout <= 0){ //  当倒计时结束时做需要的操作: 关闭 活动到期不能提交
                    dispatch_source_cancel(self.timer);
                    self.timer = nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cb(nil,YES);
                    });
                } else { // 倒计时重新计算 时/分/秒
                    NSInteger days = (int)(timeout/(3600*24));
                    NSInteger hours = (int)((timeout-days*24*3600)/3600);
                    NSInteger minute = (int)(timeout-days*24*3600-hours*3600)/60;
                    NSInteger second = timeout - days*24*3600 - hours*3600 - minute*60;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cb(@{@"days":@(days),@"hours":@(hours),@"minute":@(minute),@"second":@(second)},NO);
                    });
                    timeout--; // 递减 倒计时-1(总时间以秒来计算)
                }
            });
            dispatch_resume(self.timer);
        }
        else
        {
            cb(nil,YES);
        }
    }
}

- (void)timerBeginTime:(NSString *)beginTime cb:(void (^)(NSDictionary * _Nonnull))cb{
    NSDate *beginDate = [NSDate by_dateFromString:beginTime dateformatter:K_D_F];
    NSInteger timeDif = [NSDate by_startTime:beginDate endTime:[NSDate by_date]];
    if (_timer) {
        [self stopTimer];
    }
    if (_timer == nil) {
        __block NSInteger timeout = timeDif; // 倒计时时间
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        self.timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
        dispatch_source_set_timer(self.timer, dispatch_walltime(NULL, 0), 1.0*NSEC_PER_SEC,  0); //每秒执行
        dispatch_source_set_event_handler(self.timer, ^{
            // 计时计算 时/分/秒
            NSInteger days = (int)(timeout/(3600*24));
            NSInteger hours = (int)((timeout-days*24*3600)/3600);
            NSInteger minute = (int)(timeout-days*24*3600-hours*3600)/60;
            NSInteger second = timeout - days*24*3600 - hours*3600 - minute*60;
            dispatch_async(dispatch_get_main_queue(), ^{
                cb(@{@"hours":@(hours),@"minute":@(minute),@"second":@(second)});
            });
            timeout++; // 递增 倒计时+1(总时间以秒来计算)
        });
        dispatch_resume(self.timer);
    }
}

- (void)stopTimer{
    if (_timer) {
        dispatch_source_cancel(self.timer);
        self.timer = nil;
    }
}

- (void)setDefultBeautyValue{
//    [[NSUserDefaults standardUserDefaults] setValue:6.0 forKey:<#(nonnull NSString *)#>]
}

+ (void)showDelSheetViewInCurrentView:(NSString *)theme_id theme_type:(BY_THEME_TYPE)theme_type cb:(void(^)(void))cb{
    UIViewController *currentController = [BYTabbarViewController sharedController].currentController;
    NSString *mainTitle = theme_type == 0 ? @"直播" : @"文章";
    NSString *affirmTitle = theme_type == 0 ? @"确定删除直播吗？" : @"确定删除文章吗？";
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:mainTitle message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];

    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        UIAlertController *affirmController = [UIAlertController alertControllerWithTitle:affirmTitle message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *affirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [BYCommonTool loadRequestDelTheme_id:theme_id theme_type:theme_type cb:cb];
        }];
        
        [affirmController addAction:affirmAction];
        [affirmController addAction:cancelAction];
        [currentController.navigationController presentViewController:affirmController animated:YES completion:nil];
    }];
    

    [sheetController addAction:confirmAction];
    [sheetController addAction:cancelAction];
    [currentController.navigationController presentViewController:sheetController animated:YES completion:nil];
}

+ (void)loadRequestDelTheme_id:(NSString *)theme_id theme_type:(BY_THEME_TYPE)theme_type cb:(void(^)(void))cb{
    UIViewController *currentController = [BYTabbarViewController sharedController].currentController;
    if (!theme_id.length) {
        showToastView(@"参数为空",currentController.view);
        return;
    }
    NSString *url = theme_type == 0 ? RPC_delete_live : RPC_delete_article;
    NSDictionary *param = theme_type == 0 ? @{@"live_record_id":theme_id} : @{@"article_id":theme_id};
    [BYRequestManager ansyRequestWithURLString:url parameters:param operationType:BYOperationTypePost successBlock:^(id result) {
        showToastView(@"删除成功", currentController.view);
        if (cb) cb();
    } failureBlock:^(NSError *error) {
        showToastView(@"删除失败", currentController.view);
    }];
}

/** 获取成就参数 */
+ (ServerBadgeModel *)getAchievementData:(NSString *)bagdId{
    NSArray *vipDatas = [AccountModel sharedAccountModel].serverModel.badge.achievement_badge_list;
    PDImgStyle style = [bagdId integerValue];
    for (ServerBadgeModel *model in vipDatas) {
        if ([model.badge_id integerValue] == style) {
            return model;
        }
    }
    return nil;
}

/** 获取vip参数 */
+ (ServerBadgeModel *)getVipImageName:(NSString *)bagdId{
    NSArray *vipDatas = [AccountModel sharedAccountModel].serverModel.badge.cert_badge_list;
    PDImgStyle style = [bagdId integerValue];
    for (ServerBadgeModel *model in vipDatas) {
        if ([model.badge_id integerValue] == style) {
            return model;
        }
    }
    return nil;
}

+ (void)openMapWithLocation:(NSString *)location{
    // 获取定位location
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    __block CLLocationCoordinate2D endLocation;
    [geocoder geocodeAddressString:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        for (CLPlacemark *placemark in placemarks) {
            endLocation = placemark.location.coordinate;
            [BYCommonTool setMapData:endLocation location:location];
        }
    }];
}

+ (void)setMapData:(CLLocationCoordinate2D)endLocation location:(NSString *)location{
    NSMutableArray *mapsA = [NSMutableArray array];
    //苹果原生地图方法和其他不一样
    NSMutableDictionary *iosMapDic = [NSMutableDictionary dictionary];
    iosMapDic[@"title"] = @"苹果地图";
    iosMapDic[@"url"]   = [NSString stringWithFormat:@"%f-%f",endLocation.latitude,endLocation.longitude];
    [mapsA addObject:iosMapDic];
    
    //高德地图
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iosamap://"]]) {
        NSMutableDictionary *gaodeMapDic = [NSMutableDictionary dictionary];
        gaodeMapDic[@"title"] = @"高德地图";
        NSString *urlString = [[NSString stringWithFormat:@"iosamap://navi?sourceApplication=%@&backScheme=%@&lat=%f&lon=%f&dev=0&style=2",@"导航功能",@"Bitben",endLocation.latitude,endLocation.longitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        gaodeMapDic[@"url"] = urlString;
        [mapsA addObject:gaodeMapDic];
    }
    
    //腾讯地图
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"qqmap://"]]) {
        NSMutableDictionary *qqMapDic = [NSMutableDictionary dictionary];
        qqMapDic[@"title"] = @"腾讯地图";
        CLLocationCoordinate2D afterLocation =endLocation;
        NSString *urlString = [[NSString stringWithFormat:@"qqmap://map/routeplan?from=我的位置&type=drive&tocoord=%f,%f&to=终点&coord_type=1&policy=0",afterLocation.latitude, afterLocation.longitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        qqMapDic[@"url"] = urlString;
        [mapsA addObject:qqMapDic];
    }
    
    //百度地图
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"baidumap://"]]) {
        NSMutableDictionary *baiduMapDic = [NSMutableDictionary dictionary];
        baiduMapDic[@"title"] = @"百度地图";
        //坐标转换
        CLLocationCoordinate2D afterLocation =endLocation;
        NSString *urlString = [[NSString stringWithFormat:@"baidumap://map/direction?origin={{我的位置}}&destination=latlng:%f,%f|name=目的地&mode=driving&coord_type=gcj02",afterLocation.latitude,afterLocation.longitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        baiduMapDic[@"url"] = urlString;
        [mapsA addObject:baiduMapDic];
    }
    
    //谷歌地图
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
        NSMutableDictionary *googleMapDic = [NSMutableDictionary dictionary];
        googleMapDic[@"title"] = @"谷歌地图";
        CLLocationCoordinate2D afterLocation =endLocation;
        NSString *urlString = [[NSString stringWithFormat:@"comgooglemaps://?x-source=%@&x-success=%@&saddr=&daddr=%f,%f&directionsmode=driving",@"导航测试",@"Bitben",afterLocation.latitude, afterLocation.longitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        googleMapDic[@"url"] = urlString;
        [mapsA addObject:googleMapDic];
    }
    
    //手机地图个数判断
    if (mapsA.count > 0) {
        //选择
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"使用导航" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        NSInteger index = mapsA.count;
        
        for (int i = 0; i < index; i++) {
            
            NSString *title = mapsA[i][@"title"];
            NSString *urlString = mapsA[i][@"url"];
            //苹果原生地图方法
            if (i == 0) {
                
                UIAlertAction *iosAntion = [UIAlertAction actionWithTitle:title style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                    [BYCommonTool jumpToAppleMap:location];
                }];
                [alertVC addAction:iosAntion];
                continue;
            }
            
            UIAlertAction *action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
            }];
            
            [alertVC addAction:action];
        }
        
        UIAlertAction *cancleAct = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertVC addAction:cancleAct];
        
        [CURRENT_VC.navigationController presentViewController:alertVC animated:YES completion:nil];
    }else{
        showToastView(@"未检测到地图应用", kCommonWindow);
    }
}

#pragma mark - 跳转到苹果地图
+ (void)jumpToAppleMap:(NSString *)urlString {
    
    //这个判断其实是不需要的
    if ( [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"http://maps.apple.com/"]]){
        //MKMapItem 使用场景: 1. 跳转原生地图 2.计算线路
        MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];
        
        //地理编码器
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:urlString completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            CLPlacemark *endPlacemark  = placemarks.lastObject;
            
            //创建一个地图的地标对象
            MKPlacemark *endMKPlacemark = [[MKPlacemark alloc] initWithPlacemark:endPlacemark];
            //在地图上标注一个点(终点)
            MKMapItem *endMapItem = [[MKMapItem alloc] initWithPlacemark:endMKPlacemark];
            
            //MKLaunchOptionsDirectionsModeKey 指定导航模式
            //NSString * const MKLaunchOptionsDirectionsModeDriving; 驾车
            //NSString * const MKLaunchOptionsDirectionsModeWalking; 步行
            //NSString * const MKLaunchOptionsDirectionsModeTransit; 公交
            [MKMapItem openMapsWithItems:@[currentLocation, endMapItem]
                           launchOptions:@{MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving,MKLaunchOptionsShowsTrafficKey: [NSNumber numberWithBool:YES]}];
            
        }];
    }
    
}

#pragma mark - 绘制文章分享长图
+ (void)drawAricleShareImage:(NSString *)content time:(NSTimeInterval)time articleId:(NSString *)articleId{
    // Do any additional setup after loading the view.
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    //自定义脚本等
    WKUserContentController *controller = [[WKUserContentController alloc] init];
    //添加js全局变量
    WKUserScript *script = [[WKUserScript alloc] initWithSource:@"var interesting = 123;" injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
    //页面加载完成立刻回调，获取页面上的所有Cookie
    WKUserScript *cookieScript = [[WKUserScript alloc] initWithSource:@"                window.webkit.messageHandlers.currentCookies.postMessage(document.cookie);" injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
    //添加自定义的cookie
    WKUserScript *newCookieScript = [[WKUserScript alloc] initWithSource:@"                document.cookie = 'DarkAngelCookie=DarkAngel;'" injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
    //添加脚本
    [controller addUserScript:script];
    [controller addUserScript:cookieScript];
    //    [controller addUserScript:alertCookieScript];
    [controller addUserScript:newCookieScript];
    //注册回调
    [controller addScriptMessageHandler:[BYCommonTool shareManager] name:@"share"];
    [controller addScriptMessageHandler:[BYCommonTool shareManager] name:@"currentCookies"];
    [controller addScriptMessageHandler:[BYCommonTool shareManager] name:@"shareNew"];
    
    configuration.userContentController = controller;
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:configuration];
    webView.userInteractionEnabled = YES;
    webView.opaque = NO;
    webView.scrollView.bounces=NO;
    webView.scrollView.scrollEnabled = NO;
    webView.scrollView.decelerationRate=UIScrollViewDecelerationRateNormal;
    webView.UIDelegate = [BYCommonTool shareManager];
    webView.navigationDelegate = [BYCommonTool shareManager];
    NSString *documentDir = [[NSBundle mainBundle] pathForResource:@"layui.css" ofType:nil];//
   NSString *resultStr = [NSString stringWithContentsOfFile:documentDir encoding:NSUTF8StringEncoding error:nil];

   NSString *smartUrl = @"";
   smartUrl = @"<meta content=\"width=device-width, initial-scale=1.0, maximum-scale=3.0, user-scalable=0;\" name=\"viewport\" /> ";
   smartUrl = [smartUrl stringByAppendingString:resultStr];
   smartUrl = [smartUrl stringByAppendingString:@"<div class=\"ql-container ql-snow\"><div class=\"ql-editor\">"];
   smartUrl = [smartUrl stringByAppendingString:content];
   smartUrl = [smartUrl stringByAppendingString:@"<div id=\"testDiv\" style = \"height:0px; width:1px\">"];
   smartUrl = [smartUrl stringByAppendingString:@"</div>"];
   smartUrl = [smartUrl stringByAppendingString:@"</div>"];
   smartUrl = [smartUrl stringByAppendingString:@"</div>"];

   [webView loadHTMLString:smartUrl baseURL:[NSURL fileURLWithPath: [[NSBundle mainBundle]  bundlePath]]];
    
}

//- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
//    __weak typeof(self)weakSelf = self;
//    if (self.transferArticleModel.webLoadTempModel.hasLoad == NO){
//        [webView evaluateJavaScript:@"document.getElementById(\"testDiv\").offsetTop"completionHandler:^(id _Nullable result,NSError * _Nullable error) {
//            if(!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            //获取页面高度，并重置webview的frame
//            CGFloat lastHeight  = [result doubleValue] + 50;
//            if (strongSelf.transferArticleModel.webLoadTempModel){
//                strongSelf.transferArticleModel.webLoadTempModel.hasLoad = YES;
//                strongSelf.transferArticleModel.webLoadTempModel.transferCellHeight = lastHeight;
//            }
//
//            void(^block)() = objc_getAssociatedObject(strongSelf, &actionWebViewDidLoadKey);
//            if (block){
//                block();
//            }
//        }];
//    }
//}
@end
