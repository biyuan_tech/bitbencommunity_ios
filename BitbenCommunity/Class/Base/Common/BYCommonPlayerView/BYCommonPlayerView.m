//
//  BYCommonPlayerView.m
//  BY
//
//  Created by 黄亮 on 2018/8/1.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYCommonPlayerView.h"
#import "SuperPlayerView+Private.h"

@implementation BYCommonPlayerView

- (void)setEnableFloatWindow:(BOOL)enableFloatWindow{
    _enableFloatWindow = enableFloatWindow;
//    SuperPlayerGlobleConfigShared.enableFloatWindow = enableFloatWindow;
}

- (void)playVideoWithUrl:(NSString *)url title:(NSString *)title{
    SuperPlayerModel *playerModel                  = [[SuperPlayerModel alloc] init];
    playerModel.videoURL         = url;
    self.playerConfig.renderMode = 0;
//    playerModel.placeholderImage = nil;
    [self.controlView setTitle:title];
    [self playWithModel:playerModel];
}

- (void)playVideoWithUrl:(NSString *)url{
    SuperPlayerModel *playerModel                  = [[SuperPlayerModel alloc] init];
    playerModel.videoURL         = url;
    [self.controlView setTitle:@""];
    [self playWithModel:playerModel];
}

- (void)playVideoWithVideoId:(NSString *)videoId title:(NSString *)title{
    SuperPlayerModel *playerModel                  = [[SuperPlayerModel alloc] init];
//    playerModel.title            = title;
    playerModel.fileId           = videoId;
    [self.controlView setTitle:title];
    [self playWithModel:playerModel];
}

- (void)playLiveWithRtmp:(NSString *)rtmp{
    SuperPlayerModel *playerModel                  = [[SuperPlayerModel alloc] init];
    playerModel.videoURL         = rtmp;
    self.isLive                  = YES;
    [self playWithModel:playerModel];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
}
@end
