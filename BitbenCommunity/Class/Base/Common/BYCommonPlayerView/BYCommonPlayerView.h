//
//  BYCommonPlayerView.h
//  BY
//
//  Created by 黄亮 on 2018/8/1.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "SuperPlayerView.h"
#import "SuperPlayerView+Private.h"

//// 配置的单例
//#define SuperPlayerGlobleConfigShared       [SuperPlayerGlobleConfig sharedInstance]
//// 小窗单例
//#define SuperPlayerWindowShared             [SuperPlayerWindow sharedInstance]


// ** 继承于腾讯的超级播放器,便于后期超级播放器变动做隔离
@interface BYCommonPlayerView : SuperPlayerView

/** 是否启用悬浮窗 */
@property (nonatomic ,assign ) BOOL enableFloatWindow;


/**
 开始播放视频

 @param url 视频地址
 @param title 标题
 */
- (void)playVideoWithUrl:(NSString *)url title:(NSString *)title;

- (void)playVideoWithUrl:(NSString *)url;

/**
 开始播放视频

 @param videoId 视频id
 @param title 标题
 */
- (void)playVideoWithVideoId:(NSString *)videoId title:(NSString *)title;

/**
 开始播放直播

 @param rtmp 直播链接
 */
- (void)playLiveWithRtmp:(NSString *)rtmp;
@end
