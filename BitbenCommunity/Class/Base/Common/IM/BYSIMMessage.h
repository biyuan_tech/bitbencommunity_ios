//
//  BYSIMMessage.h
//  BibenCommunity
//
//  Created by 随风 on 2019/4/9.
//  Copyright © 2019 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger , BY_SIM_MSG_TYPE) {  // 消息类型
    BY_SIM_MSG_TYPE_SYSTEM = 0,  // 系统信息类型
    BY_SIM_MSG_TYPE_TEXT,    // 文字类型
    BY_SIM_MSG_TYPE_IMAGE,   // 图片类型
    BY_SIM_MSG_TYPE_AUDIO,   // 音频类型
    BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW = 5, // 广播但历史消息不显示
    BY_SIM_MSG_TYPE_ASK = 8, // 提问消息
    BY_SIM_MSG_TYPE_CUSTOME = 9, // 自定义类型
};

typedef NS_ENUM(NSInteger , BY_SIM_MSG_STATE) { // 消息状态
    BY_SIM_MSG_STATE_NORMAL = 0,         //
    BY_SIM_MSG_STATE_SENDING,        // 消息发送中
    BY_SIM_MSG_STATE_SEND_FAILD,     // 消息发送失败
    BY_SIM_MSG_STATE_DOWN_LOADING,   // 消息下载中
    BY_SIM_MSG_STATE_DOWN_FAILD      // 消息下载失败
};

@class BYSIMElem;

@interface BYSIMMessage : NSObject

/** 消息类型 */
@property (nonatomic ,assign) BY_SIM_MSG_TYPE msg_type;
/** 消息id */
@property (nonatomic ,copy) NSString *msg_id;
/** 消息时间 */
@property (nonatomic ,copy) NSString *time;
/** 消息发送方id */
@property (nonatomic ,copy) NSString *msg_sender;
/** 发送者昵称 */
@property (nonatomic ,copy) NSString *msg_senderName;
/** 发送者头像 */
@property (nonatomic ,copy) NSString *msg_senderlogoUrl;
/** 消息发送状态 */
@property (nonatomic ,assign) BY_SIM_MSG_STATE msg_state ;
/** 消息内容 */
@property (nonatomic ,strong) BYSIMElem *elem;
/** 自定义内容 */
@property (nonatomic ,strong) NSDictionary *customData;

/** 添加消息 */
//- (void)addElem:(BYSIMElem *)elem;

/** 添加自定义的消息内容 */
- (void)addCustomeData:(NSDictionary *)data;

@end

@interface BYSIMElem : NSObject

@end

@interface BYSIMTextElem : BYSIMElem

/** 文本 */
@property (nonatomic ,copy) NSString *text;


@end

@interface BYSIMImageElem : BYSIMElem

/** 图片上传路径 */
@property (nonatomic ,copy) NSString *path;


/** 查询上传进度 */
- (float)getUploadingProgress;

@end


@interface BYSIMAudioElem : BYSIMElem

/** 图片上传路径 */
@property (nonatomic ,copy) NSString *path;
/** 语音时长 */
@property (nonatomic ,assign) NSTimeInterval duration;

@end

NS_ASSUME_NONNULL_END
