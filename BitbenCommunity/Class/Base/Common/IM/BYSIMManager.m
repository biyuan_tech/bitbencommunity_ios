//
//  BYSIMManager.m
//  BibenCommunity
//
//  Created by 随风 on 2019/4/10.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYSIMManager.h"
#import "BYHistoryIMModel.h"

NSString * const msg_broadcast_in_room = @"broadcast_in_room"; // 聊天消息
NSString * const msg_create_room = @"create_room";             // 房间创建
NSString * const msg_close_room = @"close_room";               // 房间关闭
NSString * const msg_join_room = @"join_room";                 // 进入房间
NSString * const msg_leave_room = @"leave_room";               // 退出房间
NSString * const msg_open_forbid = @"open_forbid";             // 开启禁言
NSString * const msg_close_forbid = @"close_forbid";           // 关闭禁言
NSString * const msg_get_forbid_list = @"get_forbid_list";     // 获取被禁言的成员列表

static char const joinRoomSucKey;
static char const joinRoomFailKey;

static char const createRoomSucKey;
static char const createRoomFailKey;

static char const closeRoomSucKey;
static char const closeRoomFailKey;

static char const sendMsgSucKey;
static char const sendMsgFailKey;

static char const customMsgSucKey;
static char const customMsgFailKey;

static char const forbidMsgSucKey;
static char const forbidMsgFailKey;

static char const getForbidListSucKey;
static char const getForbidListFailKey;

typedef NS_ENUM(NSInteger ,BY_HANDLE_TYPE) { // 回调类型
    BY_HANDLE_TYPE_JOINROOM,    // 加入房间消息回调
    BY_HANDLE_TYPE_LEAVEROOM,   // 离开房间消息回调
    BY_HANDLE_TYPE_ClOSE,       // 关闭房间消息回调
    BY_HANDLE_TYPE_CREATEROOM,  // 创建房间消息回调
    BY_HANDLE_TYPE_SENDMSG,     // 发送消息回调
    BY_HANDLE_TYPE_CUSTOM,      // 自定义消息回调
    BY_HANDLE_TYPE_FORBID,      // 禁言消息回调
    BY_HANDLE_TYPE_FORBID_LIST, // 禁言列表回调
    BY_HANDLE_TYPE_ALL,         // 移除全部之前存在的回调
};

@interface BYSIMManager ()<NetworkAdapterDelegate>

@property (nonatomic ,copy) void (^messageHandle)(NSArray *msgs);

/** 成功返回 */
@property (nonatomic ,copy) succHandle tmpSucHandle;
@property (nonatomic ,copy) succRoomIdHandle tmpSucRoomIdHandle;

/** 错误返回 */
@property (nonatomic ,copy) failHandle tmpFailHandle;
/** 房间id */
@property (nonatomic ,copy ) NSString *roomId;
@property (nonatomic ,copy ) NSString *joinTime;
/** subManager */
@property (nonatomic ,strong) NSMutableArray *subManagers;
/** params */
@property (nonatomic ,strong) NSDictionary *msgParams;


@end

@implementation BYSIMManager

+ (instancetype)shareManager{
    static BYSIMManager *staticManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        staticManager = [[BYSIMManager alloc] init];
        staticManager.subManagers = [NSMutableArray array];
    });
    return staticManager;
}

+ (instancetype)allocManager{
    BYSIMManager *manager = [[BYSIMManager alloc] init];
    manager.joinTime = [BYSIMManager shareManager].joinTime;
    manager.roomId = [BYSIMManager shareManager].roomId;
    //    [manager addNSNotic];
    return manager;
}

- (void)resetJoinTime{
    [BYSIMManager shareManager].joinTime = @"";
}

- (void)resetScoketStatus{
    [BYSIMManager shareManager].joinTime = @"";
    [BYSIMManager shareManager].roomId = @"";
    [BYSIMManager shareManager].user_id = @"";
    [BYSIMManager shareManager].isForbid = NO;
    [BYSIMManager shareManager].isUserForbid = NO;
    [self removeHandle:BY_HANDLE_TYPE_ALL];
}

- (void)sendMessage:(BYSIMMessage *)message suc:(succHandle)suc fail:(failHandle)fail{
    if (![BYSIMManager shareManager].roomId) {
        showDebugToastView(@"房间号为空", kCommonWindow);
        return;
    }
    
    if ([NetworkAdapter sharedAdapter].mainRootSocketConnection.socketConnectType != SocketConnectTypeOpen) {
        if (fail) fail();
        return;
    }
    
    if (self != [BYSIMManager shareManager]) {
        [[BYSIMManager shareManager].subManagers addObject:self];
    }
    
    objc_setAssociatedObject(self, &sendMsgSucKey, suc, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &sendMsgFailKey, fail, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(message.msg_type) forKey:@"content_type"];
    [params setObject:message.time forKey:@"send_time"];
    [params setObject:nullToEmpty(message.msg_sender) forKey:@"user_id"];
    [params setObject:msg_broadcast_in_room forKey:@"type"];
    [params setObject:[BYSIMManager shareManager].roomId forKey:@"room_id"];
    [params setObject:message.msg_senderName.length ? nullToEmpty(message.msg_senderName) : @"昵称未取到" forKey:@"nickname"];
    [params setObject:nullToEmpty(message.msg_senderlogoUrl) forKey:@"head_img"];
    [params setObject:@([AccountModel sharedAccountModel].loginServerModel.user.cert_badge) forKey:@"cert_badge"];
    if (message.msg_type == BY_SIM_MSG_TYPE_TEXT) {
        BYSIMTextElem *elem = (BYSIMTextElem *)message.elem;
        [params setObject:elem.text forKey:@"content"];
    }
    else if (message.msg_type == BY_SIM_MSG_TYPE_AUDIO) {
        BYSIMAudioElem *elem = (BYSIMAudioElem *)message.elem;
        [params setObject:@(floor(elem.duration)) forKey:@"duration"];
        [params setObject:elem.path forKey:@"content"];
    }
    else if (message.msg_type == BY_SIM_MSG_TYPE_IMAGE) {
        BYSIMImageElem *elem = (BYSIMImageElem *)message.elem;
        [params setObject:elem.path forKey:@"content"];
    }else if (message.msg_type == BY_SIM_MSG_TYPE_ASK) {
        BYSIMTextElem *elem = (BYSIMTextElem *)message.elem;
        [params setObject:elem.text forKey:@"content"];
    }
    self.msgParams = params;
    [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
}


- (void)sendCustomMessage:(BY_GUEST_OPERA_TYPE)msgType data:(NSDictionary * __nullable )data suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail{
    
    if ([NetworkAdapter sharedAdapter].mainRootSocketConnection.socketConnectType != SocketConnectTypeOpen) {
        if (fail) fail();
        return;
    }
    if (!self.roomId.length) return;
    
    objc_setAssociatedObject(self, &customMsgSucKey, suc, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &customMsgFailKey, fail, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSData *jsonData;
    NSString *jsonString;
    switch (msgType) {
        case BY_GUEST_OPERA_TYPE_RECEIVE_STREAM: // 开始直播
        {
            NSError *error = nil;
            jsonData = [NSJSONSerialization dataWithJSONObject:@{kBYCustomMessageType:@(BY_GUEST_OPERA_TYPE_RECEIVE_STREAM)} options:NSJSONWritingPrettyPrinted error:&error];
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

        }
            break;
        case BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM: // 关闭直播
        {
            NSError *error = nil;
            NSMutableDictionary *customData = [NSMutableDictionary dictionary];
            [customData setObject:@(BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM) forKey:kBYCustomMessageType];
            if (data) {
                [customData addEntriesFromDictionary:data];
            }
            jsonData = [NSJSONSerialization dataWithJSONObject:customData options:NSJSONWritingPrettyPrinted error:&error];
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

        }
            break;
        case BY_GUEST_OPERA_TYPE_REWARD: // 打赏
        case BY_GUEST_OPERA_TYPE_UP_VIDEO:  // 上麦
        case BY_GUEST_OPERA_TYPE_AGREE_UP_VIDEO: // 同意上麦
        case BY_GUEST_OPERA_TYPE_DOWN_VIDEO: // 下麦
        {
            NSError *error = nil;
            jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&error];
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
            break;
        default:
            break;
    }
    [params setObject:@(BY_SIM_MSG_TYPE_SYSTEM) forKey:@"content_type"];
    if (msgType == BY_GUEST_OPERA_TYPE_REWARD) {
        [params setObject:@(BY_SIM_MSG_TYPE_CUSTOME) forKey:@"content_type"];
    }

    [params setObject:jsonString forKey:@"content"];
    [params setObject:[NSDate getCurrentTimeStr] forKey:@"send_time"];
    [params setObject:[AccountModel sharedAccountModel].account_id forKey:@"user_id"];
    [params setObject:nullToEmpty(LOGIN_MODEL.user.nickname) forKey:@"nickname"];
    [params setObject:[AccountModel sharedAccountModel].loginServerModel.account.head_img forKey:@"head_img"];
    [params setObject:@([AccountModel sharedAccountModel].loginServerModel.user.cert_badge) forKey:@"cert_badge"];
    [params setObject:nullToEmpty(self.roomId) forKey:@"room_id"];
    [params setObject:msg_broadcast_in_room forKey:@"type"];

    [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
}

- (void)setMessageListener:(void(^)(NSArray *msgs))msgs{
    self.messageHandle = msgs;
}

- (void)createRoom:(succRoomIdHandle)suc fail:(failHandle)fail{
    
    objc_setAssociatedObject(self, &createRoomSucKey, suc, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &createRoomFailKey, fail, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [[BYSIMManager shareManager] connectionSocket:^{
        BYSIMMessage *message = [[BYSIMMessage alloc] init];
        message.msg_type = BY_SIM_MSG_TYPE_SYSTEM;
        message.msg_sender = [AccountModel sharedAccountModel].account_id;
        message.time = [NSDate getCurrentTimeStr];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(message.msg_type) forKey:@"content_type"];
        [params setObject:message.time forKey:@"send_time"];
        [params setObject:nullToEmpty(message.msg_sender) forKey:@"user_id"];
        [params setObject:msg_create_room forKey:@"type"];
        
        [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
    }];
}

- (void)joinRoom:(NSString *)roomId suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail{
//    self.tmpSucHandle = suc;
    if (self.user_id.length && ![self.user_id verifyIsNum]) {
        [self leaveRoom:self.roomId suc:nil fail:nil];
    }
    if (![AccountModel sharedAccountModel].hasLoggedIn) {
        // 游客模式进入房间
        [self touristsJoinRoom:roomId suc:suc fail:fail];
        return;
    }
    objc_setAssociatedObject(self, &joinRoomSucKey, suc, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &joinRoomFailKey, fail, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.roomId = roomId;
    self.user_id = ACCOUNT_ID;
    [[BYSIMManager shareManager] connectionSocket:^{
        BYSIMMessage *message = [[BYSIMMessage alloc] init];
        message.msg_type = BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW;
        message.msg_sender = [AccountModel sharedAccountModel].account_id;
        message.time = [NSDate getCurrentTimeStr];
        message.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
        message.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
        NSString *string = [NSString stringWithFormat:@"欢迎 %@ 进入直播间",nullToEmpty(message.msg_senderName)];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(message.msg_type) forKey:@"content_type"];
        [params setObject:message.time forKey:@"send_time"];
        [params setObject:string forKey:@"content"];
        [params setObject:nullToEmpty(roomId) forKey:@"room_id"];
        [params setObject:nullToEmpty(message.msg_sender) forKey:@"user_id"];
        [params setObject:nullToEmpty(message.msg_senderName) forKey:@"nickname"];
        [params setObject:nullToEmpty(message.msg_senderlogoUrl) forKey:@"head_img"];
        [params setObject:@([AccountModel sharedAccountModel].loginServerModel.user.cert_badge) forKey:@"cert_badge"];
        [params setObject:msg_join_room forKey:@"type"];

        [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
    }];
}

- (void)touristsJoinRoom:(NSString *)roomId suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail{
    objc_setAssociatedObject(self, &joinRoomSucKey, suc, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &joinRoomFailKey, fail, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.roomId = roomId;
    NSString *user = [NSString stringWithFormat:@"游客_%@%i",[NSDate getCurrentTimeStr],(int)random()*100];
    self.user_id = user;
    [[BYSIMManager shareManager] connectionSocket:^{
        BYSIMMessage *message = [[BYSIMMessage alloc] init];
        message.msg_type = BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW;
        message.msg_sender = user;
        message.time = [NSDate getCurrentTimeStr];
        message.msg_senderName = user;
        message.msg_senderlogoUrl = @"avatar-20191015101135";
        NSString *string = [NSString stringWithFormat:@"欢迎 %@ 进入直播间",nullToEmpty(message.msg_senderName)];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(message.msg_type) forKey:@"content_type"];
        [params setObject:message.time forKey:@"send_time"];
        [params setObject:string forKey:@"content"];
        [params setObject:nullToEmpty(roomId) forKey:@"room_id"];
        [params setObject:nullToEmpty(message.msg_sender) forKey:@"user_id"];
        [params setObject:nullToEmpty(message.msg_senderName) forKey:@"nickname"];
        [params setObject:nullToEmpty(message.msg_senderlogoUrl) forKey:@"head_img"];
        [params setObject:@(-1) forKey:@"cert_badge"];
        [params setObject:msg_join_room forKey:@"type"];
        
        [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
    }];
}

- (void)leaveRoom:(NSString *)roomId suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail{
    self.tmpSucHandle = suc;
    self.tmpFailHandle = fail;
    @weakify(self);
    [[BYSIMManager shareManager] connectionSocket:^{
        @strongify(self);
        BYSIMMessage *message = [[BYSIMMessage alloc] init];
        message.msg_type = BY_SIM_MSG_TYPE_SYSTEM;
        message.msg_sender = self.user_id.length ? self.user_id : ACCOUNT_ID;
        message.time = [NSDate getCurrentTimeStr];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(message.msg_type) forKey:@"content_type"];
        [params setObject:message.time forKey:@"send_time"];
        [params setObject:nullToEmpty(roomId) forKey:@"room_id"];
        [params setObject:nullToEmpty(message.msg_sender) forKey:@"user_id"];
        [params setObject:msg_leave_room forKey:@"type"];
        
        [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
    }];
}

- (void)closeRoom:(NSString *)roomId suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail{
    objc_setAssociatedObject(self, &closeRoomSucKey, suc, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &closeRoomFailKey, fail, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [[BYSIMManager shareManager] connectionSocket:^{
        BYSIMMessage *message = [[BYSIMMessage alloc] init];
        message.msg_type = BY_SIM_MSG_TYPE_SYSTEM;
        message.msg_sender = [AccountModel sharedAccountModel].account_id;
        message.time = [NSDate getCurrentTimeStr];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(message.msg_type) forKey:@"content_type"];
        [params setObject:message.time forKey:@"send_time"];
        [params setObject:nullToEmpty(roomId) forKey:@"room_id"];
        [params setObject:nullToEmpty(message.msg_sender) forKey:@"user_id"];
        [params setObject:msg_close_room forKey:@"type"];
        
        [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
    }];
}


- (void)forbidMessage:(BOOL)isForbid user_id:(NSString *)user_id suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail{
    if (!self.roomId.length) return;
    objc_setAssociatedObject(self, &forbidMsgSucKey, suc, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &forbidMsgFailKey, fail, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [[BYSIMManager shareManager] connectionSocket:^{
        BYSIMMessage *message = [[BYSIMMessage alloc] init];
        message.msg_type = BY_SIM_MSG_TYPE_SYSTEM;
        message.msg_sender = [AccountModel sharedAccountModel].account_id;
        message.time = [NSDate getCurrentTimeStr];
        message.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
        message.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
        NSString *string = user_id.length ? @"" : @"管理员开启了全员禁言";
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(message.msg_type) forKey:@"content_type"];
        [params setObject:message.time forKey:@"send_time"];
        [params setObject:string forKey:@"content"];
        [params setObject:nullToEmpty(user_id) forKey:@"forbid_user_id"];
        [params setObject:nullToEmpty(message.msg_sender) forKey:@"user_id"];
        [params setObject:nullToEmpty(message.msg_senderName) forKey:@"nickname"];
        [params setObject:nullToEmpty(message.msg_senderlogoUrl) forKey:@"head_img"];
        [params setObject:nullToEmpty(self.roomId) forKey:@"room_id"];
        NSString *type = isForbid ? msg_open_forbid : msg_close_forbid;
        [params setObject:type forKey:@"type"];
        
        [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
    }];
}

- (void)getForbidMembers:(__nullable succArrHandle)suc fail:(__nullable failHandle)fail{
    if ([NetworkAdapter sharedAdapter].mainRootSocketConnection.socketConnectType != SocketConnectTypeOpen) {
        if (fail) fail();
        return;
    }
    if (!self.roomId.length) return;
    
    objc_setAssociatedObject(self, &getForbidListSucKey, suc, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &getForbidListFailKey, fail, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    [[BYSIMManager shareManager] connectionSocket:^{
        BYSIMMessage *message = [[BYSIMMessage alloc] init];
        message.msg_type = BY_SIM_MSG_TYPE_SYSTEM;
        message.msg_sender = ACCOUNT_ID;
        message.time = [NSDate getCurrentTimeStr];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(message.msg_type) forKey:@"content_type"];
        [params setObject:message.time forKey:@"send_time"];
        [params setObject:nullToEmpty(self.roomId) forKey:@"room_id"];
        [params setObject:nullToEmpty(message.msg_sender) forKey:@"user_id"];
        [params setObject:msg_get_forbid_list forKey:@"type"];
        
        [[NetworkAdapter sharedAdapter] webSocketFetchModelWithRequestParams:params socket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
    }];
    
}

- (void)connectionSocket{
    [self connectionSocket:nil];
}

- (void)connectionSocket:(__nullable succHandle)suc{
    _tmpSucHandle = suc;
    if ([NetworkAdapter sharedAdapter].mainRootSocketConnection.socketConnectType == SocketConnectTypeOpen) {
        if (suc) suc();
        return;
    }
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [NetworkAdapter sharedAdapter].mainRootSocketConnection = [[NetworkAdapter sharedAdapter] webSocketConnection:[NetworkAdapter sharedAdapter].mainRootSocketConnection host:kSocketHost port:kSocketPort];
    });
}

- (void)closeSocket{
    [[NetworkAdapter sharedAdapter] webSocketCloseConnectionWithSoceket:[NetworkAdapter sharedAdapter].mainRootSocketConnection];
}

- (void)removeHandle:(BY_HANDLE_TYPE)type{
    switch (type) {
        case BY_HANDLE_TYPE_JOINROOM:
            objc_setAssociatedObject(self, &joinRoomSucKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            objc_setAssociatedObject(self, &joinRoomFailKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            break;
        case BY_HANDLE_TYPE_LEAVEROOM:
            self.tmpSucHandle = NULL;
            self.tmpFailHandle = NULL;
            break;
        case BY_HANDLE_TYPE_ClOSE:
            objc_setAssociatedObject(self, &closeRoomSucKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            objc_setAssociatedObject(self, &closeRoomFailKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            break;
        case BY_HANDLE_TYPE_CREATEROOM:
            objc_setAssociatedObject(self, &createRoomSucKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            objc_setAssociatedObject(self, &createRoomFailKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            break;
        case BY_HANDLE_TYPE_SENDMSG:
            objc_setAssociatedObject(self, &sendMsgSucKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            objc_setAssociatedObject(self, &sendMsgFailKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            break;
        case BY_HANDLE_TYPE_CUSTOM:
            objc_setAssociatedObject(self, &customMsgSucKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            objc_setAssociatedObject(self, &customMsgFailKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            break;
        case BY_HANDLE_TYPE_FORBID:
            objc_setAssociatedObject(self, &forbidMsgSucKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            objc_setAssociatedObject(self, &forbidMsgFailKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            break;
        case BY_HANDLE_TYPE_FORBID_LIST:
            objc_setAssociatedObject(self, &getForbidListSucKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            objc_setAssociatedObject(self, &getForbidListFailKey, NULL, OBJC_ASSOCIATION_COPY_NONATOMIC);
            break;
        case BY_HANDLE_TYPE_ALL:
        {
            [self removeHandle:BY_HANDLE_TYPE_JOINROOM];
            [self removeHandle:BY_HANDLE_TYPE_LEAVEROOM];
            [self removeHandle:BY_HANDLE_TYPE_ClOSE];
            [self removeHandle:BY_HANDLE_TYPE_CREATEROOM];
            [self removeHandle:BY_HANDLE_TYPE_SENDMSG];
            [self removeHandle:BY_HANDLE_TYPE_CUSTOM];
            [self removeHandle:BY_HANDLE_TYPE_FORBID];
            [self removeHandle:BY_HANDLE_TYPE_FORBID_LIST];
        }
            break;
        default:
            break;
    }
}

- (void)verifySubManagerMessage:(WebSocketReceiveMainModel *)data{
    NSMutableArray *tmpArr = self.subManagers.mutableCopy;
    for (BYSIMManager *manager in self.subManagers) {
        if ([manager.msgParams[@"send_time"] isEqualToString:data.receiveDataDic[@"data"][@"send_time"]] &&
            [manager.msgParams[@"content"] isEqualToString:data.receiveDataDic[@"data"][@"content"]]) {
            [manager webSocketReceiveData:WebSocketTypeIM data:data];
            [tmpArr removeObject:manager];
        }
    }
    self.subManagers = tmpArr;
}

#pragma mark - socker消息回调
-(void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data{
    if (!self) return;
    if (type == WebSocketTypeIM) {
        if (![data.receiveDataDic[@"type"] isEqualToString:msg_create_room]) {
            if (!self.roomId.length) return;
        }
        
        if (self == [BYSIMManager shareManager]) {
            if (self.subManagers.count) {
                [self verifySubManagerMessage:data];
            }
        }
        
        succHandle joinSucHandle = objc_getAssociatedObject(self, &joinRoomSucKey);
        failHandle joinFailHandle = objc_getAssociatedObject(self, &joinRoomFailKey);
        
        succRoomIdHandle createSucHandle = objc_getAssociatedObject(self, &createRoomSucKey);
        failHandle createFailHandle = objc_getAssociatedObject(self, &createRoomFailKey);
        
        succHandle closeSucHandle = objc_getAssociatedObject(self, &closeRoomSucKey);
        failHandle closeFailHandle = objc_getAssociatedObject(self, &closeRoomFailKey);
        
        succHandle sendSucHandle = objc_getAssociatedObject(self, &sendMsgSucKey);
        failHandle sendFailHandle = objc_getAssociatedObject(self, &sendMsgFailKey);
        
        succHandle customSucHandle = objc_getAssociatedObject(self, &customMsgSucKey);
        failHandle customFailHandle = objc_getAssociatedObject(self, &customMsgFailKey);
        
        succHandle forbidSucHandle = objc_getAssociatedObject(self, &forbidMsgSucKey);
        failHandle forbidFailHandle = objc_getAssociatedObject(self, &forbidMsgFailKey);
        
        succArrHandle getForbidListSucHandle = objc_getAssociatedObject(self, &getForbidListSucKey);
        failHandle getForbidListFailHandle = objc_getAssociatedObject(self, &getForbidListFailKey);


        if ([data.receiveDataDic[@"result"] integerValue] == 0) { // 操作成功
            if ([data.receiveDataDic[@"type"] isEqualToString:msg_join_room]) { // 加入房间
                self.joinTime = data.receiveDataDic[@"data"][@"send_time"];
                self.isForbid = [data.receiveDataDic[@"data"][@"forbid_status"] boolValue];
                self.isUserForbid = [data.receiveDataDic[@"data"][@"user_forbid_status"] boolValue];
                if (joinSucHandle) {
                    joinSucHandle();
                    [self removeHandle:BY_HANDLE_TYPE_JOINROOM];
                }
            }
            else if ([data.receiveDataDic[@"type"] isEqualToString:msg_create_room]) { // 创建房间
                if (createSucHandle) {
                    createSucHandle(data.receiveDataDic[@"data"][@"room_id"]);
                    [self removeHandle:BY_HANDLE_TYPE_CREATEROOM];
                }
                [[BYSIMManager shareManager] closeSocket];
            }
            else if ([data.receiveDataDic[@"type"] isEqualToString:msg_close_room]) { // 关闭房间
                if (closeSucHandle) {
                    closeSucHandle();
                    [self removeHandle:BY_HANDLE_TYPE_ClOSE];
                }
            }
            else if ([data.receiveDataDic[@"type"] isEqualToString:msg_broadcast_in_room]) { // 接收聊天消息
                if (sendSucHandle) {
                    sendSucHandle();
                    [self removeHandle:BY_HANDLE_TYPE_SENDMSG];
                }
                NSDictionary *msgs = data.receiveDataDic[@"data"];
                if (self.messageHandle) {
                    self.messageHandle(@[msgs]);
                }
            }
            else if ([data.receiveDataDic[@"type"] isEqualToString:msg_open_forbid] ||
                     [data.receiveDataDic[@"type"] isEqualToString:msg_close_forbid]) { // 禁言
                if (forbidSucHandle) {
                    forbidSucHandle();
                    [self removeHandle:BY_HANDLE_TYPE_FORBID];
                }
            }
            else if ([data.receiveDataDic[@"type"] isEqualToString:msg_get_forbid_list]) { // 禁言列表
                if (getForbidListSucHandle) {
                    NSArray *list = data.receiveDataDic[@"data"][@"forbid_user"];
                    getForbidListSucHandle(list);
                    [self removeHandle:BY_HANDLE_TYPE_FORBID_LIST];
                }
            }
            
            // 接收appp显示的系统消息
//            if ([data.receiveDataDic[@"data"][@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM_APP_SHOW) {
//                NSDictionary *msgs = data.receiveDataDic[@"data"];
//                if (self.messageHandle) {
//                    self.messageHandle(@[msgs]);
//                }
//            }
            // 接收自定义消息
            if ([data.receiveDataDic[@"data"][@"content_type"] integerValue] == BY_SIM_MSG_TYPE_CUSTOME ||
                [data.receiveDataDic[@"data"][@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
                if (customSucHandle) {
                    customSucHandle();
                    [self removeHandle:BY_HANDLE_TYPE_CUSTOM];
                }
            }
        }
        else{
            if ([data.receiveDataDic[@"type"] isEqualToString:msg_join_room]) { // 加入房间
                if (joinFailHandle) {
                    joinFailHandle();
                    [self removeHandle:BY_HANDLE_TYPE_JOINROOM];
                    self.roomId = @"";
                }
            }
            else if ([data.receiveDataDic[@"type"] isEqualToString:msg_create_room]) { // 创建房间
                if (createFailHandle) {
                    createFailHandle();
                    [self removeHandle:BY_HANDLE_TYPE_CREATEROOM];
                }
                [[BYSIMManager shareManager] closeSocket];
            }
            else if ([data.receiveDataDic[@"type"] isEqualToString:msg_close_room]) { // 关闭房间
                if (closeFailHandle) {
                    closeFailHandle();
                    [self removeHandle:BY_HANDLE_TYPE_ClOSE];
                }
            }
            else if ([data.receiveDataDic[@"type"] isEqualToString:msg_broadcast_in_room]) { // 接收聊天消息
                if (sendFailHandle) {
                    sendFailHandle();
                    [self removeHandle:BY_HANDLE_TYPE_SENDMSG];
                }
            }
            else if ([data.receiveDataDic[@"type"] isEqualToString:msg_open_forbid] ||
                     [data.receiveDataDic[@"type"] isEqualToString:msg_close_forbid]) { // 禁言
                if (forbidFailHandle) {
                    forbidFailHandle();
                    [self removeHandle:BY_HANDLE_TYPE_FORBID];
                }
            }
            else if ([data.receiveDataDic[@"type"] isEqualToString:msg_get_forbid_list]) { // 禁言列表
                if (getForbidListFailHandle) {
                    getForbidListFailHandle();
                    [self removeHandle:BY_HANDLE_TYPE_FORBID_LIST];
                }
            }
            
            // 接收自定义消息
            if ([data.receiveDataDic[@"data"][@"content_type"] integerValue] == BY_SIM_MSG_TYPE_CUSTOME ||
                [data.receiveDataDic[@"data"][@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
                if (customFailHandle) {
                    customFailHandle();
                    [self removeHandle:BY_HANDLE_TYPE_CUSTOM];
                }
            }
            
            showDebugToastView(data.receiveDataDic[@"resultMsg"], kCommonWindow);
        
        }
    }
    PDLog(@"socket消息回调--------------%@",data);
}

-(void)webSocketDidConnectedWithSocket:(WebSocketConnection *)webSocket{
    if (_tmpSucHandle) {
        _tmpSucHandle();
        _tmpSucHandle = NULL;
    }
}

@end
