//
//  BYSIMManager.h
//  BibenCommunity
//
//  Created by 随风 on 2019/4/10.
//  Copyright © 2019 币本. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BYSIMMessage.h"
#import "NetworkAdapter.h"
#import "NetworkAdapter+MemberVIP.h"


NS_ASSUME_NONNULL_BEGIN

// 聊天消息
FOUNDATION_EXPORT NSString * const msg_broadcast_in_room;
// 房间创建
FOUNDATION_EXPORT NSString * const msg_create_room;
// 房间关闭
FOUNDATION_EXPORT NSString * const msg_close_room;
// 进入房间
FOUNDATION_EXPORT NSString * const msg_join_room;
// 退出房间
FOUNDATION_EXPORT NSString * const msg_leave_room;
// 开启禁言
FOUNDATION_EXPORT NSString * const msg_open_forbid;
// 关闭禁言
FOUNDATION_EXPORT NSString * const msg_close_forbid;
// 获取被禁言的成员列表
FOUNDATION_EXPORT NSString * const msg_get_forbid_list;

typedef NS_ENUM(NSInteger ,BY_MSG_CUSTOM_TYPE) { // 自定义消息类型
    BY_MSG_CUSTOM_TYPE_OPENLIVE, // 开播
    BY_MSG_CUSTOM_TYPE_CLOSELIVE, // 关播
    BY_MSG_CUSTOM_TYPE_REWARD, // 赞赏
};

#define kBYCustomMessageType @"opera_type"

// 结束直播
static NSString * const BYCustomMessageType_EndLive = @"ENDLIVE";
// 开始直播
static NSString * const BYCustomMessageType_BeginLive = @"BEGINLIVE";
// 打赏
static NSString * const BYCustomMessageType_Reward = @"REWARD";

typedef void (^succHandle)(void);
typedef void (^failHandle)(void);

typedef void (^succRoomIdHandle)(NSString *roomId);
typedef void (^succArrHandle)(NSArray *members);

@interface BYSIMManager : NSObject

/** 房间号，记录当前聊天 */
@property (nonatomic ,copy ,readonly) NSString *roomId;
/** 加入聊天室时间 */
@property (nonatomic ,copy ,readonly) NSString *joinTime;
/** 当前房间的禁言状态 */
@property (nonatomic ,assign) BOOL isForbid;
/** 当前用户在房间的禁言状态 */
@property (nonatomic ,assign) BOOL isUserForbid;
/** 当前登录用户id */
@property (nonatomic ,copy) NSString *user_id;


+ (instancetype)shareManager;

+ (instancetype)allocManager;

/** 消息发送 */
- (void)sendMessage:(BYSIMMessage *)message suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail;

/** 自定义消息 */
- (void)sendCustomMessage:(BY_GUEST_OPERA_TYPE)msgType data:(NSDictionary *__nullable)data suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail;

/** 消息回调 */
- (void)setMessageListener:(void(^)(NSArray *msgs))msgs;
/** 创建房间 */
- (void)createRoom:(__nullable succRoomIdHandle)suc fail:(__nullable failHandle)fail;
/** 进入房间 */
- (void)joinRoom:(NSString *)roomId suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail;
/** 离开房间 */
- (void)leaveRoom:(NSString *)roomId suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail;
/** 关闭房间 */
- (void)closeRoom:(NSString *)roomId suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail;
/** 禁言消息 (不传user_id则为全员禁言)*/
- (void)forbidMessage:(BOOL)isForbid user_id:(NSString *)user_id suc:(__nullable succHandle)suc fail:(__nullable failHandle)fail;

/** 获取禁言列表 */
- (void)getForbidMembers:(__nullable succArrHandle)suc fail:(__nullable failHandle)fail;

- (void)connectionSocket;
- (void)connectionSocket:(__nullable succHandle)suc;
- (void)closeSocket;


- (void)webSocketReceiveData:(WebSocketType)type data:(WebSocketReceiveMainModel *)data;
- (void)webSocketDidConnectedWithSocket:(WebSocketConnection *)webSocket;

- (void)resetJoinTime;

- (void)resetScoketStatus;
@end

NS_ASSUME_NONNULL_END
