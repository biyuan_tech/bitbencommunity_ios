//
//  BYRewardView.m
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYRewardView.h"
#import "UILabel+BYExtension.h"

@interface BYRewardView()<UITextFieldDelegate>

/** 所加载的界面 */
@property (nonatomic ,weak) UIView *fathureView;
/** 显示内容界面 */
@property (nonatomic ,strong) UIView *contentView;
/** 打赏用户名 */
@property (nonatomic ,strong) NSString *rewardUserName;
/** 剩余余额 */
@property (nonatomic ,assign) CGFloat surplusBBT;
/** 金额输入 */
@property (nonatomic ,strong) UITextField *textField;
/** 打赏用户 */
@property (nonatomic ,strong) UILabel *rewardUserLab;
/** 当前余额 */
@property (nonatomic ,strong) UILabel *balanceBBTLab;
/** 展示输入金额 */
//@property (nonatomic ,strong) UILabel *bbtNumLab;
/** 遮罩层 */
@property (nonatomic ,strong) UIView *maskView;
@property (nonatomic ,strong) NSArray *amountBtns;
@property (nonatomic ,assign) CGFloat amountBBT;
@property (nonatomic ,strong) NSArray *dfAmounts;
/** 使用默认选择金额 */
@property (nonatomic ,assign) BOOL isDfAmount;
@end

#define conteneViewH (IS_iPhoneX ? 402 : 380)
static NSInteger baseTag = 0x642;
@implementation BYRewardView

- (instancetype)initWithFathureView:(UIView *)fathureView{
    self = [super init];
    if (self) {
        self.fathureView = fathureView;
        self.isDfAmount = NO;
        [self setContentView];
    }
    return self;
}

- (void)setReceicer_name:(NSString *)receicer_name{
    _receicer_name = receicer_name;
    self.rewardUserLab.text = receicer_name;
}

- (void)setSurplusBBT:(CGFloat)surplusBBT{
    _surplusBBT = surplusBBT;
    _balanceBBTLab.text = [NSString stringWithFormat:@"当前余额 : %.2fBP",surplusBBT];
//    NSString *string = [NSString stringWithFormat:@"%.2f BBT",surplusBBT];
//    NSAttributedString *tagAttributedString = [[NSAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]}];
//    [_balanceBBTLab setTagAttributedString:tagAttributedString];
}

- (void)setChooseBBTAmount:(NSArray *)amounts{
    if (!amounts.count) return;
    _dfAmounts = amounts;
    CGFloat amountBtnW = 85;
    CGFloat amountBtnH = 33;
    CGFloat spaceW = (kCommonScreenWidth - 30 - amountBtnW*3)/2;
    NSMutableArray *tmpArr = [NSMutableArray array];
    for (int i = 0; i < amounts.count; i ++) {
        UIButton *amountBtn = [self.contentView viewWithTag:baseTag + i];
        CGFloat bbt = [amounts[i] floatValue];
        if (!amountBtn) {
            amountBtn = [UIButton by_buttonWithCustomType];
            amountBtn.tag = baseTag + i;
            NSString *title = [NSString stringWithFormat:@"%.2fBP",bbt];
            [amountBtn setBy_attributedTitle:@{@"title":title,NSForegroundColorAttributeName:kTextColor_117,NSFontAttributeName:[UIFont systemFontOfSize:14]} forState:UIControlStateNormal];
            [amountBtn setBy_attributedTitle:@{@"title":title,NSForegroundColorAttributeName:kTextColor_238,NSFontAttributeName:[UIFont systemFontOfSize:14]} forState:UIControlStateSelected];
            amountBtn.layer.cornerRadius = 5;
            amountBtn.layer.borderWidth = 1;
            amountBtn.layer.borderColor = kTextColor_117.CGColor;
            [amountBtn addTarget:self action:@selector(amountBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [_contentView addSubview:amountBtn];
            [amountBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(amountBtnW);
                make.height.mas_equalTo(amountBtnH);
                make.top.mas_equalTo(210 + (amountBtnH + 17)*(i/3));
                make.left.mas_equalTo(kCellLeftSpace + (amountBtnW + spaceW)*(i%3));
            }];
        }
        [tmpArr addObject:amountBtn];
    }
    self.amountBtns = [tmpArr copy];
}

- (void)reloadSurplusBBT:(CGFloat)bbt{
    CGFloat tmpBbt = _surplusBBT - bbt;
    [self setSurplusBBT:tmpBbt];
}

- (void)resetData{
    self.textField.text = @"";
    self.isDfAmount = NO;
    [self resetAmountBtn:nil];
}

- (void)showAnimation{
    NSAssert(self.fathureView, @"父视图不能为nil");
    [self loadRequestGetSurplusBBT];
    [_fathureView addSubview:self];
    [self mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    self.center = CGPointMake(_fathureView.center.x, _fathureView.center.y);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self setNeedsUpdateConstraints];
        [UIView animateWithDuration:0.2 animations:^{
            self.maskView.opaque = 1.0f;
            [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
            }];
            [self layoutIfNeeded];
        }];
    });
}

- (void)hiddenAnimation{
    [self resetData];
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.2 animations:^{
        self.maskView.opaque = 0.0f;
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(conteneViewH);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)exitBtnAction{
    [self hiddenAnimation];
}

- (void)confirmBtnAction{
    [_textField endEditing:YES];
    if (!_textField.text.length) {
        showToastView(@"请输入打赏金额", kCommonWindow);
        return;
    }
    if ([_textField.text floatValue] > _surplusBBT) {
        showToastView(@"余额不足", kCommonWindow);
        return;
    }
    if ([_textField.text floatValue] < 1.0) {
        showToastView(@"最低打赏金额为1.00BP", kCommonWindow);
        return;
    }
    if (_confirmBtnHandle) _confirmBtnHandle([_textField.text floatValue]);
}

- (void)amountBtnAction:(UIButton *)sender{
    self.isDfAmount = YES;
    [self resetAmountBtn:sender];
}

- (void)resetAmountBtn:(UIButton *)sender{
    for (UIButton *btn in _amountBtns) {
        if (!sender) {
            btn.selected = NO;
            _amountBBT = 0;
            btn.layer.borderColor = kTextColor_117.CGColor;
            return;
        }
        else if (sender){
            btn.selected = btn == sender ? YES : NO;
            if (btn != sender) {
                btn.layer.borderColor = kTextColor_117.CGColor;
            }
            else{
                btn.layer.borderColor = kColorRGBValue(0xee4944).CGColor;
                _textField.text = [NSString stringWithFormat:@"%.2f",[_dfAmounts[sender.tag - baseTag] floatValue]];
            }
        }
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *rangString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (rangString.length > 7) return NO;
    if (_isDfAmount) {
        // 重置默认金额选择状态
        [self resetAmountBtn:nil];
    }
    rangString = rangString.length ? rangString : @"0";
    return YES;
}

#pragma mark - request
- (void)loadRequestGetSurplusBBT{
//    BYToastView *toastView = [BYToastView toastViewPresentLoadingInView:self.contentView];
    self.contentView.userInteractionEnabled = NO;
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestSurplusBBTSuccessBlock:^(id object) {
        @strongify(self);
        [self setSurplusBBT:[object[@"balance"] floatValue]];
        NSArray *amounts = object[@"BBT_amount"];
        if (amounts.count) {
            [self setChooseBBTAmount:amounts];
        }
//        [toastView dissmissToastView];
        self.contentView.userInteractionEnabled = YES;
    } faileBlock:^(NSError *error) {
//        [toastView dissmissToastView];
    }];
}


#pragma mark - configSubView Mehtod
- (void)setContentView{
    
    // 背景遮罩
    self.maskView = [UIView new];
    _maskView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenAnimation)];
    [_maskView addGestureRecognizer:tapGestureRecognizer];
    [self addSubview:self.maskView];
    [_maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    UIView *contentView = [[UIView alloc] init];
    [contentView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:contentView];
    _contentView = contentView;
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth);
        make.height.mas_equalTo(conteneViewH);
        make.centerX.mas_equalTo(0);
        make.bottom.mas_equalTo(conteneViewH);
    }];
    
    // 打赏文案
    UILabel *title = [[UILabel alloc] init];
    title.text = @"打赏";
    title.textColor = kTextColor_53;
    title.font = [UIFont systemFontOfSize:15];
    [contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(title.text, 15));
        make.height.mas_equalTo(stringGetHeight(title.text, 15));
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(10);
    }];
    
    UILabel *subTitle = [[UILabel alloc] init];
    subTitle.text = @"爱赞赏的人，运气不会差哦~";
    subTitle.textColor = kTextColor_154;
    subTitle.font = [UIFont systemFontOfSize:11];
    [contentView addSubview:subTitle];
    [subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(subTitle.text, 11));
        make.height.mas_equalTo(stringGetHeight(subTitle.text, 11));
        make.centerX.mas_equalTo(0);
        make.top.equalTo(title.mas_bottom).with.offset(5);
    }];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kBgColor_237];
    [contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(50);
        make.height.mas_equalTo(0.5);
    }];
    
    // 隐藏按钮
    UIButton *downBtn = [UIButton by_buttonWithCustomType];
    [downBtn setBy_imageName:@"icon_live_opera_down" forState:UIControlStateNormal];
    [downBtn addTarget:self action:@selector(hiddenAnimation) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:downBtn];
    [downBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(42);
        make.height.mas_equalTo(48);
        make.top.right.mas_equalTo(0);
    }];
    
    UIView *rewardBgView = [UIView by_init];
    rewardBgView.layer.cornerRadius = 5;
    rewardBgView.backgroundColor = kBgColor_248;
    [self.contentView addSubview:rewardBgView];
    [rewardBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.right.mas_equalTo(-kCellRightSpace);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(64);
    }];
    
    // 打赏给
    UILabel *rewardLab = [[UILabel alloc] init];
    rewardLab.text = @"被打赏的人";
    rewardLab.font = [UIFont systemFontOfSize:13];
    rewardLab.textColor = kColorRGBValue(0x2c2c2c);
    [rewardBgView addSubview:rewardLab];
    [rewardLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.width.mas_equalTo(stringGetWidth(rewardLab.text, 13));
        make.height.mas_equalTo(stringGetHeight(rewardLab.text, 13));
        make.centerY.mas_equalTo(0);
    }];
    
    // 打赏给的用户
    UILabel *rewardUserLab = [[UILabel alloc] init];
    rewardUserLab.font = [UIFont systemFontOfSize:13];
    rewardUserLab.textColor = kColorRGBValue(0x2c2c2c);
    rewardUserLab.textAlignment = NSTextAlignmentRight;
//    rewardUserLab.text = _rewardUserName;
    [rewardBgView addSubview:rewardUserLab];
    _rewardUserLab = rewardUserLab;
    [rewardUserLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.left.equalTo(rewardLab.mas_right).with.offset(-20);
        make.right.mas_equalTo(-34);
        make.height.mas_equalTo(14);
    }];
    
    // 下标图片
    UIImageView *dowmImgView = [[UIImageView alloc] init];
    [dowmImgView setImage:[UIImage imageNamed:@"reward_down"]];
    [rewardBgView addSubview:dowmImgView];
    [dowmImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(dowmImgView.image.size.width);
        make.height.mas_equalTo(dowmImgView.image.size.height);
        make.centerY.equalTo(rewardUserLab.mas_centerY).with.offset(0);
        make.right.mas_equalTo(-kCellRightSpace);
    }];
    
    // 金额输入容器
    UIView *amountBgView = [UIView by_init];
    amountBgView.backgroundColor = kBgColor_248;
    [self.contentView addSubview:amountBgView];
    [amountBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.right.mas_equalTo(-kCellRightSpace);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(122);
    }];
    
    // 总金额
    UILabel *amountLab = [[UILabel alloc] init];
    amountLab.text = @"金额";
    amountLab.font = [UIFont systemFontOfSize:13];
    amountLab.textColor = kColorRGBValue(0x3a3a3a);
    [amountBgView addSubview:amountLab];
    [amountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kCellLeftSpace);
        make.width.mas_equalTo(stringGetWidth(amountLab.text, 13));
        make.height.mas_equalTo(stringGetHeight(amountLab.text, 13));
        make.centerY.mas_equalTo(0);
    }];
    
    // 单位（bbt）
    UILabel *unitLab = [[UILabel alloc] init];
    unitLab.text = @"BP";
    unitLab.font = [UIFont systemFontOfSize:14];
    unitLab.textColor = kTextColor_53;
    [amountBgView addSubview:unitLab];
    [unitLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kCellRightSpace);
        make.centerY.equalTo(amountLab.mas_centerY).with.offset(0);
        make.width.mas_equalTo(stringGetWidth(unitLab.text, unitLab.font.pointSize));
        make.height.mas_equalTo(stringGetHeight(unitLab.text, unitLab.font.pointSize));
    }];
    
    // 金额输入
    UITextField *textField = [[UITextField alloc] init];
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"最低1.00BP哦" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:kTextColor_168}];
    textField.textAlignment = NSTextAlignmentRight;
    textField.font = [UIFont systemFontOfSize:13];
    textField.keyboardType = UIKeyboardTypeDecimalPad;
    textField.keyboardDistanceFromTextField = 30;
    textField.delegate = self;
    [amountBgView addSubview:textField];
    _textField = textField;
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(unitLab.mas_left).with.offset(-10);
        make.centerY.equalTo(unitLab.mas_centerY).with.offset(0);
        make.left.equalTo(amountLab.mas_right).with.offset(35);
        make.height.mas_equalTo(45);
    }];
    
    // 当前余额
    UILabel *balanceBBTLab = [[UILabel alloc] init];
    balanceBBTLab.text = @"当前余额 : 00.00 BP";
    balanceBBTLab.font = [UIFont systemFontOfSize:12];
    balanceBBTLab.textColor = kTextColor_135;
    balanceBBTLab.textAlignment = NSTextAlignmentRight;
    [contentView addSubview:balanceBBTLab];
    _balanceBBTLab = balanceBBTLab;
    [balanceBBTLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-30);
        make.top.equalTo(amountBgView.mas_bottom).with.offset(10);
        make.left.mas_equalTo(35);
        make.height.mas_equalTo(14);
    }];
    
    // 确定按钮
    UIButton *confirmBtn = [UIButton by_buttonWithCustomType];
    [confirmBtn setBackgroundColor:kBgColor_238];
    [confirmBtn setBy_attributedTitle:@{@"title":@"确定",NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [confirmBtn addTarget:self action:@selector(confirmBtnAction) forControlEvents:UIControlEventTouchUpInside];
    confirmBtn.layer.cornerRadius = 10;
    [_contentView addSubview:confirmBtn];
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-kSafe_Mas_Bottom(15));
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(46);
    }];
}


@end
