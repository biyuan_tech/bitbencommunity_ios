//
//  BYReportInCellView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYReportInCellView : UIView

/** themeId */
@property (nonatomic ,copy) NSString *theme_id;
/** theme_type */
@property (nonatomic ,assign) BY_THEME_TYPE theme_type;


- (void)showAnimationWithPoint:(CGPoint)point;

+ (instancetype)initReportViewShowInView:(UIView *)view;

@end

NS_ASSUME_NONNULL_END
