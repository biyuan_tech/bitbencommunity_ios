//
//  BYLiveToolBar.m
//  BibenCommunity
//
//  Created by 随风 on 2019/3/13.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYLiveToolBar.h"
#import "BYPluginView.h"

#import "BYPluginAudioMoreView.h"
#import "BYIMMessageInputView.h"
#import "BYMediaMoreView.h"

#import "AATAudioTool.h"


@interface BYLiveToolBar ()<BYIMMessageInputViewDelegate>

/** 插件组 */
@property (nonatomic ,strong) NSArray *plugins;
/** pluginView数组 */
@property (nonatomic ,strong) NSArray *pluginViews;
/** 当前选中的插件 */
@property (nonatomic ,strong) BYPluginView *selectedPluginView;
/** 遮罩 */
@property (nonatomic ,strong) UIControl *maskView;

@end

@implementation BYLiveToolBar

- (void)dealloc
{
    
}

- (void)destory{
    [self removeMoreContentView];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initToolBar:(NSArray *)inputViewPlugins{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        self.plugins = inputViewPlugins;
        [self setBackgroundColor:kColorRGBValue(0xfafafa)];
        [self setContentView];
        [self addNSNotification];
    }
    return self;
}

- (CGFloat)calculateTextViewHeight:(UITextView *)textView text:(NSString *)text{
    CGSize constraninSize = CGSizeMake(textView.contentSize.width, MAXFLOAT);
    CGSize size = [textView sizeThatFits:constraninSize];
    return size.height + 14 + textView.textContainerInset.top;

}

#pragma mark - action

- (void)removeMoreContentView{
    for (BYPluginView *pluginView in self.pluginViews) {
        if (pluginView.pluginData.inputViewMoreContentView) {
            [pluginView.pluginData.inputViewMoreContentView removeFromSuperview];
            pluginView.pluginData.inputViewMoreContentView = nil;
        }
    }
}

- (void)hiddenToolBar{
    BYPluginView *selectPluginView = self.selectedPluginView;
    if (self.selectedPluginView) {
        [self resertTextInputViewSuperView];
        [self hiddenMoreContentViewAnimation:selectPluginView];
        selectPluginView.selected = NO;
        self.selectedPluginView = nil;
    }
}

- (void)tapAction:(UIControl *)sender{

    BYPluginView *pluginView = (BYPluginView *)sender;
    BYPluginView *selectPluginView = self.selectedPluginView;
    if (self.selectedPluginView) {
        [self hiddenMoreContentViewAnimation:selectPluginView];
        selectPluginView.selected = NO;
//        [self showMoreContentView:NO];
        self.selectedPluginView = nil;
    }
    
    if (pluginView.pluginData.pluginType == BYInputPluginTypePPT ||
        pluginView.pluginData.pluginType == BYInputPluginTypeOperation ||
        pluginView.pluginData.pluginType == BYInputPluginTypeImage) { // ppt,operation模式 置为no
        selectPluginView = nil;
    }
    
    // 设置toolbar点击状态
    if (pluginView != selectPluginView) {
        self.selectedPluginView = pluginView;
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        for (int i = 0; i < self.pluginViews.count ; i ++) {
            BYPluginView *tmpPluginView = self.pluginViews[i];
            tmpPluginView.selected = tmpPluginView == pluginView ? YES : NO;
            if (pluginView.pluginData.pluginType == BYInputPluginTypePPT ||
                pluginView.pluginData.pluginType == BYInputPluginTypeOperation ||
                pluginView.pluginData.pluginType == BYInputPluginTypeImage) { // ppt,operation模式 置为no
                tmpPluginView.selected = NO;
            }
            if (i == self.pluginViews.count - 1) {
                dispatch_semaphore_signal(semaphore);
            }
        }
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        [self showMoreContentView:YES];
    }
    
    // 点击出文字外bar时移除当前第一界面编辑
    if (pluginView.pluginData.pluginType != BYInputPluginTypeText) {
        [self.superview endEditing:YES];
    }
    else{ // 点击文字设置当前为第一响应者
        [((BYIMMessageInputView *)pluginView.pluginData.inputViewMoreContentView).inputView becomeFirstResponder];
    }
}

- (void)didTapMediaAtIndex:(NSInteger )index{
    if (index == 0) { // 进入图片选择
        [self openPhotoLibrary];
    }
}

- (void)dismissView{
    [kCommonWindow endEditing:YES];
    [self.superview endEditing:YES];
    [self hiddenToolBar];
}

- (void)resertTextInputViewSuperView{
    if (self.selectedPluginView.pluginData.pluginType == BYInputPluginTypeText) {
        UIView *moreContentView = self.selectedPluginView.pluginData.inputViewMoreContentView;
        if (moreContentView.superview == kCommonWindow) {
            [self.maskView removeFromSuperview];
//            [moreContentView removeFromSuperview];
//            [self.fatherView addSubview:moreContentView];
        }
    }
}



#pragma mark - 打开相册 发送图片
- (void)openPhotoLibrary{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UITabBarController *tabBarController = [BYTabbarViewController sharedController];
        UINavigationController *navigationController = [tabBarController selectedViewController];
        UIViewController *viewController = [navigationController.viewControllers lastObject];
        UIViewController *currentController;
        if ([viewController isKindOfClass:[RTContainerController class]]) {
            currentController = ((RTContainerController *)viewController).contentViewController;
        }
        else{
            currentController = viewController;
        }
        GWAssetsLibraryViewController *assetVC = [[GWAssetsLibraryViewController alloc] init];
        BYNavigationController *naviController = [[BYNavigationController alloc] initWithRootViewController:assetVC];

//        @weakify(self);
        [assetVC selectImageArrayFromImagePickerWithMaxSelected:5 andBlock:^(NSArray *selectedImgArr) {
//            @strongify(self);
            // 在新增ppt课件后重置七牛上传状态
           
        }];
        [currentController presentViewController:naviController animated:YES completion:nil];
    }
}

#pragma mark - 插件时间回调
- (void)setPluginHandle:(BYPluginView *)plugin{
    switch (plugin.pluginData.pluginType) {
        case BYInputPluginTypeAudio:
        {
            BYPluginAudioMoreView *moreView = (BYPluginAudioMoreView *)plugin.pluginData.inputViewMoreContentView;
            @weakify(self);
            moreView.finishRecordAudioHandle = ^(NSURL * _Nonnull audioPath) {
                @strongify(self);
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    [BYToastView tostViewPresentInView:kCommonWindow title:@"录音回调" duration:1 complete:nil];
//                });
                if ([self.delegate respondsToSelector:@selector(recordAudioDidFinish:)]) {
                    [self.delegate recordAudioDidFinish:audioPath];
                }
            };
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - animation

- (void)showMoreContentView:(BOOL)show{
    if (show)
        [self showMoreContentViewAnimation:self.selectedPluginView];
    else
        [self hiddenMoreContentViewAnimation:self.selectedPluginView];
}

- (void)showMoreContentViewAnimation:(BYPluginView *)pluginView{
    UIView *moreContentView = pluginView.pluginData.inputViewMoreContentView;
    if (!moreContentView) return;
    UIView *superview = pluginView.pluginData.pluginType == BYInputPluginTypeText ? kCommonWindow : self.superview;
    [superview addSubview:moreContentView];
    
    // 检查麦克风权限
    if (pluginView.pluginData.pluginType == BYInputPluginTypeAudio) {
        [AATAudioTool checkCameraAuthorizationGrand:nil withNoPermission:nil];
    }
    
    // 是否需要延迟
    BOOL isDelay = NO;
    if (CGRectEqualToRect(moreContentView.frame, CGRectZero)) {
        isDelay = YES;
        if (pluginView.pluginData.pluginType == BYInputPluginTypeText) {
            moreContentView.frame = CGRectMake(0, kCommonScreenHeight, kCommonScreenWidth, pluginView.pluginData.inputMoreContentViewHeight);
//            moreContentView.center = CGPointMake(kCommonScreenWidth/2, moreContentView.center.y);
        }
        else{
            moreContentView.frame = CGRectMake(0, CGRectGetMaxY(self.frame), kCommonScreenWidth, pluginView.pluginData.inputMoreContentViewHeight);
        }
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        CGFloat bottom = pluginView.pluginData.pluginType == BYInputPluginTypeText ? -pluginView.pluginData.inputMoreContentViewHeight : 0;
        [moreContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.height.mas_equalTo(pluginView.pluginData.inputMoreContentViewHeight);
            make.width.mas_equalTo(kCommonScreenWidth);
            make.bottom.mas_equalTo(-bottom);
        }];
        
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-pluginView.pluginData.inputMoreContentViewHeight);
        }];
        
        [superview layoutIfNeeded];
    }];
}

- (void)hiddenMoreContentViewAnimation:(BYPluginView *)pluginView{
    UIView *moreContentView = pluginView.pluginData.inputViewMoreContentView;
    if (!moreContentView.superview) return;
    if (pluginView.pluginData.pluginType == BYInputPluginTypeText) {
        [UIView animateWithDuration:0.2 animations:^{
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
            }];
            [self.superview layoutIfNeeded];
        } completion:^(BOOL finished) {
        }];
        return;
    }
    [UIView animateWithDuration:0.2 animations:^{
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(0);
        }];
        
        [moreContentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(pluginView.pluginData.inputMoreContentViewHeight);
        }];
        [self.superview layoutIfNeeded];
    } completion:^(BOOL finished) {
        [moreContentView removeFromSuperview];
    }];
}

#pragma mark - keyBoardNSNotification

- (void)keyBoardWillShow:(NSNotification *)notic{
    if (self.selectedPluginView.pluginData.pluginType != BYInputPluginTypeText) return;
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardDidShow:(NSNotification *)notic{
    if (self.selectedPluginView.pluginData.pluginType != BYInputPluginTypeText) return;
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardWillHidden:(NSNotification *)notic{
    BYPluginView *selectedPluginView = self.selectedPluginView;
    [self hiddenToolBar];
    if (selectedPluginView.pluginData.pluginType != BYInputPluginTypeText) return;
    UIView *moreContentView = selectedPluginView.pluginData.inputViewMoreContentView;
    if (self.superview) {
        CGFloat detalY = selectedPluginView.pluginData.inputMoreContentViewHeight;
        [UIView animateWithDuration:0.2 animations:^{
            [moreContentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(detalY);
            }];
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
            }];
            [kCommonWindow layoutIfNeeded];
        } completion:^(BOOL finished) {
            [moreContentView removeFromSuperview];
        }];
    }
}

- (void)keyBoardShowAnimation:(NSValue *)value{
    UIView *moreContentView = self.selectedPluginView.pluginData.inputViewMoreContentView;
    if (!moreContentView || ![moreContentView isKindOfClass:[BYIMMessageInputView class]]) {
        return;
    }
    BYIMMessageTextView *textView = ((BYIMMessageInputView *)moreContentView).inputView;
    if (moreContentView.superview && textView.isFirstResponder) {
        CGFloat detalY = [value CGRectValue].size.height;
        [UIView animateWithDuration:0.1 animations:^{
            [moreContentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(0);
                make.width.mas_equalTo(kCommonScreenWidth);
                make.bottom.mas_equalTo(-detalY);
            }];
            [kCommonWindow layoutIfNeeded];
        }];
    }
}

#pragma mark - regisetNSNotic
- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - BYIMMessageInputViewDelegate 文本输入代理
- (BOOL)messageInputViewShouldBeginEditing:(BYIMMessageInputView *)inputView{
    UIView *moreContentView = self.selectedPluginView.pluginData.inputViewMoreContentView;
    if (kCommonWindow == moreContentView.superview && moreContentView.superview) {
//        [moreContentView removeFromSuperview];
//        [kCommonWindow addSubview:moreContentView];
        [kCommonWindow addSubview:self.maskView];
        [kCommonWindow insertSubview:self.maskView belowSubview:moreContentView];
        [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }
    return YES;
}

- (BOOL)messageInputViewShouldEndEditing:(BYIMMessageInputView *)inputView{
    [self resertTextInputViewSuperView];
    return YES;
}

- (BOOL)messageInputView:(BYIMMessageInputView *)inputView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return YES;
}

- (void)messageInputViewDidChange:(BYIMMessageInputView *)inputView text:(NSString *)text{
    CGFloat maxHeight = 70;
    CGFloat height = [self calculateTextViewHeight:inputView.inputView text:text];
    inputView.inputView.scrollEnabled = height > maxHeight ? YES : NO;
    if (height < maxHeight ) {
        [inputView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height < 48 ? 48 : height);
        }];
    }

}


#pragma mark - configUI

- (void)setContentView{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < self.plugins.count; i ++) {
        BYBaseInputViewPlugin *pluginData = self.plugins[i];
        CGFloat width = kCommonScreenWidth/self.plugins.count;
        BYPluginView *pluginView = [[BYPluginView alloc] init];
        @weakify(self);
        pluginView.didTapPluginView = ^(BYInputPluginType type, BOOL selected) {
            @strongify(self);
            if (self.didTapPluginView && selected) {
                self.didTapPluginView(type);
            }
        };
        
        // 委托代理设置
        if (pluginData.pluginType == BYInputPluginTypeText) {
            ((BYIMMessageInputView *)pluginData.inputViewMoreContentView).delegate = self;
        }
        else if (pluginData.pluginType == BYInputPluginTypeMedia){
            @weakify(self);
            ((BYMediaMoreView *)pluginData.inputViewMoreContentView).didTapPluginAtIndexHandle = ^(NSInteger index) {
                @strongify(self);
                [self didTapMediaAtIndex:index];
            };

        }
        
        [pluginView addTarget:self action:@selector(tapAction:) forControlEvents:UIControlEventTouchUpInside];
        [pluginView configUIWithPluginData:pluginData];
        [self addSubview:pluginView];
        [array addObject:pluginView];
        [pluginView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(width);
            make.left.mas_equalTo(width*i);
            make.top.bottom.mas_equalTo(0);
        }];
        [self setPluginHandle:pluginView];
    }
    self.pluginViews = [array copy];
    
    for (int i = 0; i < 1; i ++) {
        UIView *lineView = [[UIView alloc] init];
        [lineView setBackgroundColor:kColorRGBValue(0xededed)];
        [self addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0.5);
            make.left.right.mas_equalTo(0);
            if (i == 0) {
                make.top.mas_equalTo(0);
            }
            else{
                make.bottom.mas_equalTo(0);
            }
        }];
    }
}

- (UIControl *)maskView{
    if (!_maskView) {
        _maskView = [UIControl by_init];
        [_maskView setBackgroundColor:[UIColor clearColor]];
        [_maskView addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _maskView;
}

@end
