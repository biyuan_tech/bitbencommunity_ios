//
//  BYLiveAudienceToolBar.h
//  BibenCommunity
//
//  Created by 随风 on 2019/4/13.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYLiveAudienceToolBar : UIView

/** more handle */
@property (nonatomic ,copy) void (^moreBtnHandle)(void);
/** superView */
@property (nonatomic ,weak) UIView *fatherView;

@end

NS_ASSUME_NONNULL_END
