//
//  BYPluginMoreView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYPluginMoreView : UIView


/** 点击插件回调 */
@property (nonatomic ,copy) void (^didTapPluginAtIndexHandle)(NSInteger index);
/** isHost */
@property (nonatomic ,assign) BOOL isHost;


@end

NS_ASSUME_NONNULL_END
