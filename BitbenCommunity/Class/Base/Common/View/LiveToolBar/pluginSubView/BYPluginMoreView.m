//
//  BYPluginMoreView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPluginMoreView.h"

static NSInteger baseTag = 0x541;
static CGFloat   pluginW = 52;
@implementation BYPluginMoreView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setContentView];
    }
    return self;
}

- (void)tapAction:(UIButton *)sender{
    if (self.didTapPluginAtIndexHandle) {
        self.didTapPluginAtIndexHandle(sender.superview.tag - baseTag);
    }
}

- (void)setIsHost:(BOOL)isHost{
    _isHost = isHost;
    if (!isHost) {
        UIView *plugin = [self viewWithTag:baseTag + 1];
        plugin.hidden = YES;
    }
}

- (void)setContentView{
    
    UIView *topLine = [UIView by_init];
    [topLine setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self addSubview:topLine];
    [topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    NSArray *images = @[@"microlive_plugin_image",@"microlive_plugin_exitLive"];
    NSArray *titles = @[@"图片",@"结束"];
    for (int i = 0; i < 2; i ++) {
        UIView *pluginView =[self addPluginView:images[i] title:titles[i]];
        pluginView.tag = baseTag + i;
        [self addSubview:pluginView];
        [pluginView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(pluginW);
            make.left.mas_equalTo(36 + (pluginW + 32)*i);
            make.top.mas_equalTo(16);
            make.height.mas_greaterThanOrEqualTo(74);
        }];
    }
}

- (UIView *)addPluginView:(NSString *)imageName title:(NSString *)title{
    UIView *view = [[UIView alloc] init];
    
    UIButton *button = [UIButton by_buttonWithCustomType];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(tapAction:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = 2*baseTag;
    [view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(pluginW);
        make.height.mas_equalTo(pluginW);
        make.left.mas_equalTo(0);
    }];
    
    
    CGSize size = [title getStringSizeWithFont:[UIFont systemFontOfSize:12] maxWidth:pluginW];
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.text = title;
    titleLab.font = [UIFont systemFontOfSize:12];
    titleLab.textColor = kColorRGBValue(0x323232);
    titleLab.textAlignment = NSTextAlignmentCenter;
    [view addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(button.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(size.height > 28 ? 28 : size.height);
        make.bottom.mas_equalTo(0);
    }];
    
    return view;
}

@end
