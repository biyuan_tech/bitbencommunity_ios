//
//  BYPluginAudioMoreView.m
//  BibenCommunity
//
//  Created by 随风 on 2019/3/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYPluginAudioMoreView.h"
#import "AATAudioTool.h"
#import "SDImageCache+ChatCaculator.h"
#import <EMVoiceConverter.h>

/** 音频录制按钮 */
static NSInteger audioBtnTag = 0x321;
@interface BYPluginAudioMoreView ()<AATAudioToolProtocol>
{
    NSTimeInterval _currentAudioDuration;
}
/** title */
@property (nonatomic ,strong) UILabel *titleLab;
/** 音频录制按钮 */
@property (nonatomic ,strong) UIButton *audioBtn;
/** 手指是否在输入栏内部 */
@property (nonatomic ,assign) BOOL isRecordTouchingOutSide;
/** 当前录音时长 */
@property (nonatomic ,assign) NSTimeInterval audioDuration;
/** 长按录音按钮 */
@property (nonatomic ,strong) UIImageView *longAudioView;
/** 回调的视频路径 */
@property (nonatomic ,strong) NSURL *audioPath;
/** 取消录音按钮 */
@property (nonatomic ,strong) UIButton *cancelRecordBtn;


@end

@implementation BYPluginAudioMoreView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        self.status = BYRecordAudioStatusTapNormal;
        [self configUI];
    }
    return self;
}

- (void)configUI{
    UIView *topLine = [UIView by_init];
    [topLine setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self addSubview:topLine];
    [topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
    
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.textColor = kColorRGBValue(0x7e7e7e);
    titleLab.font = [UIFont systemFontOfSize:11];
    titleLab.textAlignment = NSTextAlignmentCenter;
    titleLab.text = @"点击开始录音";
    [self addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(12);
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(stringGetHeight(titleLab.text, titleLab.font.pointSize));
    }];
    
    
    UIButton *audioBtn = [UIButton by_buttonWithCustomType];
    [audioBtn setImage:[UIImage imageNamed:@"audio_singleTap_normal"] forState:UIControlStateNormal];
    [audioBtn addTarget:self action:@selector(audioBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:audioBtn];
    audioBtn.tag = audioBtnTag;
    self.audioBtn = audioBtn;
    [audioBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(56);
        make.height.mas_equalTo(56);
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(12);
        make.centerX.mas_equalTo(0);
    }];
    
    UIImageView *longAudioView = [[UIImageView alloc] init];
    [longAudioView by_setImageName:@"audio_longTap_normal"];
    longAudioView.userInteractionEnabled = YES;
    longAudioView.hidden = YES;
    [self addSubview:longAudioView];
    self.longAudioView = longAudioView;
    [longAudioView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(56);
        make.height.mas_equalTo(56);
        make.top.mas_equalTo(titleLab.mas_bottom).mas_offset(12);
        make.centerX.mas_equalTo(0);
    }];
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGesAction:)];
    longPress.minimumPressDuration = 0.1;
    [longAudioView addGestureRecognizer:longPress];
    
    UIButton *tapBtn = [UIButton by_buttonWithCustomType];
    tapBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [tapBtn setTitle:@"单击" forState:UIControlStateNormal];
    [tapBtn setTitleColor:kColorRGBValue(0xee4944) forState:UIControlStateSelected];
    [tapBtn setTitleColor:kColorRGBValue(0xa2a2a2) forState:UIControlStateNormal];
    [tapBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    tapBtn.selected = YES;
    [self addSubview:tapBtn];
    tapBtn.tag = 244;
    [tapBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(20);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(audioBtn.mas_bottom).mas_offset(5);
    }];
    
    UIButton *longTapBtn = [UIButton by_buttonWithCustomType];
    longTapBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [longTapBtn setTitle:@"长按" forState:UIControlStateNormal];
    [longTapBtn setTitleColor:kColorRGBValue(0xee4944) forState:UIControlStateSelected];
    [longTapBtn setTitleColor:kColorRGBValue(0xa2a2a2) forState:UIControlStateNormal];
    [longTapBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    longTapBtn.selected = NO;
    [self addSubview:longTapBtn];
    longTapBtn.tag = 245;
    [longTapBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(20);
        make.centerY.mas_equalTo(tapBtn.mas_centerY).mas_offset(0);
        make.left.mas_equalTo(tapBtn.mas_right).mas_offset(5);
    }];
    
    self.cancelRecordBtn = [UIButton by_buttonWithCustomType];
    [self.cancelRecordBtn setBy_attributedTitle:@{NSForegroundColorAttributeName:kColorRGBValue(0x555454),
                                                  @"title":@"取消",
                                                  NSFontAttributeName:[UIFont systemFontOfSize:12]
                                                  } forState:UIControlStateNormal];
    [self.cancelRecordBtn addTarget:self action:@selector(cancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
    self.cancelRecordBtn.hidden = YES;
    [self addSubview:self.cancelRecordBtn];
    [self.cancelRecordBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-5);
        make.top.mas_equalTo(5);
        make.width.mas_equalTo(43);
        make.height.mas_equalTo(32);
    }];
}

#pragma mark - action

- (void)buttonAction:(UIButton *)sender{
    for (int i = 0; i < 2; i ++) {
        UIButton *btn = [self viewWithTag:244 + i];
        btn.selected = btn == sender ? YES : NO;
    }
    
    UIButton *tapBtn = [self viewWithTag:244];
    self.status = tapBtn == sender ? BYRecordAudioStatusTapNormal : BYRecordAudioStatusLongNormal;
    [UIView animateWithDuration:0.2 animations:^{
        [tapBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(tapBtn == sender ? 0 : - 40);
        }];
        [self layoutIfNeeded];
    }];
}

- (void)cancelBtnAction{
    BYAlertView *alertView = [BYAlertView initWithTitle:@"取消该条语音录制？" message:nil inView:kCommonWindow alertViewStyle:BYAlertViewStyleAlert];
    [alertView addActionCancleTitle:@"取消" handle:NULL];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"确定" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:kTextColor_237}];
    @weakify(self);
    [alertView addActionCustomAttributedString:attributedString handle:^{
        @strongify(self);
        self.status = BYRecordAudioStatusTapNormal;
    }];
    [alertView setMaskViewStyle:BYAlertViewMaskStyleBlack];
    [alertView showAnimation];
}

- (void)audioBtnAction:(UIButton *)sender{
    if (self.status == BYRecordAudioStatusTapNormal) {
        self.status = BYRecordAudioStatusTapPress;
        [self hiddenTapBtn:YES];
    }
    else if (self.status == BYRecordAudioStatusTapPress){
        self.status = BYRecordAudioStatusTapFinish;
    }
    else if (self.status == BYRecordAudioStatusTapFinish){
        self.status = BYRecordAudioStatusTapNormal;
        [self sendAudio];
        if (self.finishRecordAudioHandle) {
            self.finishRecordAudioHandle(self.audioPath);
        }
    }
    else if (self.status == BYRecordAudioStatusLongNormal){
        self.status = BYRecordAudioStatusLongPress;
    }
}

-(void)longPressGesAction:(UILongPressGestureRecognizer *)ges{
    if (self.status == BYRecordAudioStatusLongNormal ||
        self.status == BYRecordAudioStatusLongPress ||
        self.status == BYRecordAudioStatusLongWillCancel ){
        switch (ges.state) {
            case UIGestureRecognizerStateBegan:
            {
                NSLog(@"UIGestureRecognizerStateBegan");
                self.status = BYRecordAudioStatusLongPress;
                //    开始录音
                [AATAudioTool checkCameraAuthorizationGrand:^{
                    [[AATAudioTool share] startRecord];
                    self.isRecordTouchingOutSide = NO;
                    [AATAudioTool share].delegate = self;
                } withNoPermission:^{
                    BYAlertView *alertView = [BYAlertView showAlertAnimationWithTitle:@"请到设置-币本-麦克风,开启麦克风权限" message:nil inView:kCommonWindow];
                    [alertView addActionCancleTitle:@"确定" handle:nil];
                    [alertView setMaskViewStyle:BYAlertViewMaskStyleBlack];
                    [alertView showAnimation];
                }];
            }
                break;
            case UIGestureRecognizerStateChanged:
            {
                NSLog(@"UIGestureRecognizerStateChanged");
                CGPoint loca = [ges locationInView:ges.view];
                BOOL bol = CGRectContainsPoint(ges.view.bounds, loca);
                if (bol) {
                    // 拖拽到内部，如果在录音，则恢复正常显示，否则什么都不做
                    if ([AATAudioTool share].isRecorderRecording) {
                        self.status = BYRecordAudioStatusLongPress;
                        self.isRecordTouchingOutSide = NO;
                    }
                } else {
                    self.status = BYRecordAudioStatusLongWillCancel;
                    self.isRecordTouchingOutSide = YES;
                }
            }
                break;
            case UIGestureRecognizerStateEnded:
            {
                CGPoint loca = [ges locationInView:ges.view];
                BOOL bol = CGRectContainsPoint(ges.view.bounds, loca);
                if (bol) {
                    //    结束录音
                    [[AATAudioTool share] stopRecord];
                } else {
                    //    外部抬起，取消录音
                    [[AATAudioTool share] intertrptRecord];
                    self.status = BYRecordAudioStatusLongNormal;
                }
            }
                break;
            default:
                break;
        }
    }
}

- (void)hiddenTapBtn:(BOOL)isHidden{
    for (int i = 0; i < 2; i ++) {
        UIButton *btn = [self viewWithTag:244 + i];
        btn.hidden = isHidden;
    }
}

- (void)setStatus:(BYRecordAudioStatus)status{
    _status = status;
    [self updateAudioStatus];
}

- (void)updateAudioStatus{
    NSString *title;
    NSAttributedString *attributedString;
    NSString *imageName;
    self.cancelRecordBtn.hidden = self.status == BYRecordAudioStatusTapFinish ? NO : YES;
    switch (self.status) {
        case BYRecordAudioStatusTapNormal:
        {
            self.audioBtn.hidden = NO;
            self.longAudioView.hidden = YES;
            self.audioDuration = 0;
            imageName = @"audio_singleTap_normal";
            title = @"点击开始录音";
            [self hiddenTapBtn:NO];
        }
            break;
        case BYRecordAudioStatusTapPress:
        case BYRecordAudioStatusTapFinish:
        {
            if (self.status == BYRecordAudioStatusTapPress &&
                ![AATAudioTool share].isRecorderRecording) { // 录音
                [AATAudioTool checkCameraAuthorizationGrand:^{
                    [[AATAudioTool share] startRecord];
                    self.isRecordTouchingOutSide = NO;
                    [AATAudioTool share].delegate = self;
                } withNoPermission:^{
                    
                }];
                [self hiddenTapBtn:YES];
            }
            if (self.status == BYRecordAudioStatusTapFinish) { // 结束录音
                [[AATAudioTool share] stopRecord];
            }
        
            imageName = self.status == BYRecordAudioStatusTapPress ? @"audio_singleTap_press" : @"audio_singleTap_finish";
            title = [NSString stringWithFormat:@"%.0fs/180s",floor(self.audioDuration)];
            attributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.0fs",floor(self.audioDuration)] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:kColorRGBValue(0xee4944)}];
        }
            break;
        case BYRecordAudioStatusLongNormal:
            self.audioBtn.hidden = YES;
            self.longAudioView.hidden = NO;
            self.audioDuration = 0;
            imageName = @"audio_longTap_normal";
            title = @"按住说话";
            [self hiddenTapBtn:NO];
            break;
        case BYRecordAudioStatusLongPress:
        {
            imageName = @"audio_longTap_normal";
            title = [NSString stringWithFormat:@"录音中，%.0fs后将自动发送，上滑可取消",(180 - floor(self.audioDuration))];
            attributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.0fs",(180 - floor(self.audioDuration))] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:kColorRGBValue(0xee4944)}];
            [self hiddenTapBtn:YES];
        }
            break;
        case BYRecordAudioStatusLongWillCancel:
            imageName = @"audio_longTap_normal";
            title = @"松开手指，取消发送";
            break;
        default:
            break;
    }

    [self setTitleAttributedString:attributedString string:title];
    [self.audioBtn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}

- (void)setTitleAttributedString:(NSAttributedString *)attributedString string:(NSString *)string{
    if (!string.length) return;
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:kColorRGBValue(0x7e7e7e)}];
    if (!attributedString) {
        self.titleLab.attributedText = mutableAttributedString;
        return;
    }
    NSRange rang = [string rangeOfString:attributedString.string];
    NSRange tagRang = NSMakeRange(0, attributedString.string.length);
    NSDictionary *tagAttributedStringKey = [attributedString attributesAtIndex:0 effectiveRange:&tagRang];
    [mutableAttributedString addAttributes:tagAttributedStringKey range:rang];
    self.titleLab.attributedText = mutableAttributedString;
}

#pragma mark - AATAudioToolDelegate
// 开始录音回调
-(void)aatAudioToolDidStartRecord:(NSTimeInterval)currentTime{
    self.audioPath = nil;
}

-(void)aatAudioToolUpdateCurrentTime:(NSTimeInterval)currentTime
                            fromTime:(NSTimeInterval)startTime
                               power:(float)power{
    
    if (self.isRecordTouchingOutSide){
//        [AATVoiceHudAlert showRevocationHud];
    } else {
        self.audioDuration = currentTime - startTime;
        _currentAudioDuration = currentTime - startTime;
        [self updateAudioStatus];
    }
}

-(void)aatAudioToolDidStopRecord:(NSURL *)dataPath
                       startTime:(NSTimeInterval)start
                         endTime:(NSTimeInterval)end
                       errorInfo:(NSString *)info{
    if (info) {
        if (self.status == BYRecordAudioStatusTapFinish) {
            self.status = BYRecordAudioStatusTapNormal;
        }
        else if (self.status == BYRecordAudioStatusLongPress){
            self.status = BYRecordAudioStatusLongNormal;
        }
        [BYToastView tostViewPresentInView:kCommonWindow title:@"录音时长太短" duration:1 complete:nil];
        //        [AATHUD showInfo:info andDismissAfter:0.5];
    } else {
        self.audioPath = dataPath;
        // 达到最长录音时长自动结束
        if (self.status == BYRecordAudioStatusTapPress) {
            self.status = BYRecordAudioStatusTapNormal;
            [self sendAudio];
            if (self.finishRecordAudioHandle) {
                self.finishRecordAudioHandle(self.audioPath);
            }
        }
        else if (self.status == BYRecordAudioStatusLongPress ||
                 self.status == BYRecordAudioStatusLongWillCancel){
            self.status = BYRecordAudioStatusLongNormal;
            [self sendAudio];
            if (self.finishRecordAudioHandle) {
                self.finishRecordAudioHandle(self.audioPath);
            }
        }
    }
    [AATAudioTool share].delegate = nil;
}
//
-(void)aatAudioToolDidSInterrupted{
    [AATAudioTool share].delegate = nil;
}

#pragma mark - 发送语音
- (void)sendAudio{
    if (!self.audioPath.absoluteString.length) {
        showDebugToastView(@"音频路径为nil", kCommonWindow);
        return;
    }
    
    // 将wav格式转换为amr格式后上传至七牛
    NSString *name = [NSString stringWithFormat:@"%@-%@-%@.amr",@"PPTAUDIO",[AccountModel sharedAccountModel].account_id,[NSDate by_stringFromDate:[NSDate by_date] dateformatter:@"yyyy-MM-dd-HH-mm-ss"]];
    NSString *directoryPath = [NSString stringWithFormat:@"%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];
    NSString *file_path = [directoryPath stringByAppendingString:[NSString stringWithFormat:@"/%@",name]];
    if (![EMVoiceConverter wavToAmr:[self.audioPath.absoluteString formatFilePath] amrSavePath:file_path]) {
        OSSFileModel *model = [[OSSFileModel alloc] init];
        model.objcPath = file_path;
        model.objcName  = [NSString stringWithFormat:@"%@-%@-%@",@"PPTAUDIO",[AccountModel sharedAccountModel].account_id,[NSDate by_stringFromDate:[NSDate by_date] dateformatter:@"yyyy-MM-dd-HH-mm-ss"]];
        
        @weakify(self);
        [[OSSManager sharedUploadManager] uploadAudioManagerWithModel:model suc:^(NSString *url) {
            @strongify(self);
            PDLog(@"audiourl-------%@",url);
            [self cacheAudioData:self.audioPath key:url];
            [self sendAudioMessage:url];
        }];
    }
    else{
        showDebugToastView(@"音频转换amr格式错误", kCommonWindow);
    }
    
}

- (void)sendAudioMessage:(NSString *)audioUrl{
    BYSIMMessage *msg = [[BYSIMMessage alloc] init];
    msg.msg_type = BY_SIM_MSG_TYPE_AUDIO;
    msg.msg_sender = [AccountModel sharedAccountModel].account_id;
    msg.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
    msg.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
    msg.time = [NSDate getCurrentTimeStr];
    BYSIMAudioElem *elem = [[BYSIMAudioElem alloc] init];
    elem.path = audioUrl;
    elem.duration = _currentAudioDuration;
    msg.elem = elem;
    [[BYSIMManager shareManager] sendMessage:msg suc:nil fail:nil];
    _currentAudioDuration = 0;
}

// 本地缓存录音文件
- (void)cacheAudioData:(NSURL *)audioPath key:(NSString *)key{
    NSData *data = [NSData dataWithContentsOfURL:audioPath];
    NSString *keyStr = [NSString stringWithFormat:@"%@.wav",key];
    [[SDImageCache sharedImageCache] cd_storeImageData:data forKey:keyStr toDisk:YES completion:^{
        
    }];
}


@end
