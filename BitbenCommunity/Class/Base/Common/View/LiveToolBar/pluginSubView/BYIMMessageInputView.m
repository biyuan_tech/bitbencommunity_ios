//
//  BYIMMessageInputView.m
//  BibenCommunity
//
//  Created by 随风 on 2019/3/23.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYIMMessageInputView.h"
#import "BYSIMManager.h"
#import "NSDate+BYExtension.h"

@interface BYIMMessageInputView ()<UITextViewDelegate>

/** 消息发送按钮 */
@property (nonatomic ,strong) UIButton *sendBtn;


@end

@implementation BYIMMessageInputView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self configUI];
    }
    return self;
}

- (void)willMoveToWindow:(UIWindow *)newWindow{
    [super willMoveToWindow:newWindow];
    if (newWindow) {
//        self.frame = CGRectMake(0, kCommonScreenHeight - kSafeAreaInsetsBottom - 48, kCommonScreenWidth, 48);
//        self.center = CGPointMake(kCommonScreenWidth/2, self.center.y);
    }
}

- (void)configUI{
    self.inputView = [[BYIMMessageTextView alloc] init];
    self.inputView.backgroundColor = kColorRGBValue(0xeeeeee);
    self.inputView.layer.cornerRadius = 5.0f;
    self.inputView.tintColor = kBgColor_238;
    self.inputView.font = [UIFont systemFontOfSize:16];
//    self.inputView.textContainerInset = UIEdgeInsetsMake(10, 8, 0, 0);
    self.inputView.textContainerInset = UIEdgeInsetsMake(7, 8, 0, 5);
    self.inputView.delegate = self;
    [self addSubview:self.inputView];
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 15, 7, 80));
    }];
    
    // 发送按钮
    self.sendBtn = [UIButton by_buttonWithCustomType];
    [self.sendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [self.sendBtn setBackgroundColor:kColorRGBValue(0x424242)];
    [self.sendBtn addTarget:self action:@selector(sendBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    self.sendBtn.layer.cornerRadius = 16.0f;
    [self addSubview:self.sendBtn];
    @weakify(self);
    [self.sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerY.mas_equalTo(self.inputView.mas_centerY).mas_offset(0);
        make.height.mas_equalTo(32);
        make.width.mas_equalTo(60);
        make.right.mas_equalTo(-10);
    }];
    
    
    
}

- (void)sendBtnAction:(UIButton *)sender{
    NSString *string = [self.inputView.text removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
    if (!string.length) return;
    BYSIMMessage *msg = [[BYSIMMessage alloc] init];
    msg.msg_type = BY_SIM_MSG_TYPE_TEXT;
    msg.msg_sender = [AccountModel sharedAccountModel].account_id;
    msg.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
    msg.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
    msg.time = [NSDate getCurrentTimeStr];
    BYSIMTextElem *elem = [[BYSIMTextElem alloc] init];
    elem.text = string;
    msg.elem = elem;
    [[BYSIMManager shareManager] sendMessage:msg suc:nil fail:nil];
    self.inputView.text = @"";
    [self.inputView resignFirstResponder];
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(48);
    }];
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if ([self.delegate respondsToSelector:@selector(messageInputViewShouldBeginEditing:)]) {
        return [self.delegate messageInputViewShouldBeginEditing:self];
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if ([self.delegate respondsToSelector:@selector(messageInputViewShouldEndEditing:)]) {
        return [self.delegate messageInputViewShouldEndEditing:self];
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([self.delegate respondsToSelector:@selector(messageInputView:shouldChangeTextInRange:replacementText:)]) {
        return [self.delegate messageInputView:self shouldChangeTextInRange:range replacementText:text];
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
//    _sendBtn.enabled = textView.text.length ? YES : NO;
    if ([self.delegate respondsToSelector:@selector(messageInputViewDidChange:text:)]) {
        [self.delegate messageInputViewDidChange:self text:textView.text];
    }
}



@end



@interface BYIMMessageTextView ()

@end

@implementation BYIMMessageTextView


- (CGRect)caretRectForPosition:(UITextPosition *)position
{
    CGRect originalRect = [super caretRectForPosition:position];
    originalRect.size.height = 38 - 14;
    originalRect.size.width = 2;
    return originalRect;
}

@end


