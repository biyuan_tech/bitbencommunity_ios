//
//  BYBaseInputViewPlugin.m
//  BibenCommunity
//
//  Created by 随风 on 2019/3/13.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYBaseInputViewPlugin.h"
#import "BYPluginAudioMoreView.h"
#import "BYIMMessageInputView.h"
#import "BYMediaMoreView.h"

@interface BYBaseInputViewPlugin ()

/** moreContentView */
@property (nonatomic ,strong) UIView *moreContentView;

@end

@implementation BYBaseInputViewPlugin
@synthesize inputViewMoreContentView = _inputViewMoreContentView;

- (void)dealloc
{
    
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (instancetype)initWithType:(BYInputPluginType)type{
    self = [super init];
    if (self) {
        self.pluginType = type;
    }
    return self;
}

- (UIImage *)pluginIconNormalIcon{
    NSString *iconName = @"";
    switch (self.pluginType) {
        case BYInputPluginTypeAudio:
            iconName = @"live_plugin_audio_normal";
            break;
        case BYInputPluginTypeText:
            iconName = @"live_plugin_text_normal";
            break;
        case BY_SIM_MSG_TYPE_IMAGE:
            iconName = @"live_plugin_image_normal";
            break;
        case BYInputPluginTypeMedia:
            iconName = @"live_plugin_media_normal";
            break;
        case BYInputPluginTypePPT:
            iconName = @"live_plugin_ppt_normal";
            break;
        case BYInputPluginTypeOperation:
            iconName = @"live_plugin_operation_normal";
            break;
        default:
            break;
    }
    return [UIImage imageNamed:iconName];
}

- (NSString *)pluginName{
    NSString *name = @"";
    switch (self.pluginType) {
        case BYInputPluginTypeAudio:
            name = @"语音";
            break;
        case BYInputPluginTypeText:
            name = @"文字";
            break;
        case BY_SIM_MSG_TYPE_IMAGE:
            name = @"图片";
            break;
        case BYInputPluginTypeMedia:
            name = @"媒体库";
            break;
        case BYInputPluginTypePPT:
            name = @"课件";
            break;
        case BYInputPluginTypeOperation:
            name = @"操作";
            break;
        default:
            break;
    }
    return name;
}

- (UIImage *)pluginIconPressedIcon{
    return nil;
}

- (UIImage *)pluginIconSelectIcon{
    NSString *iconName = @"";
    switch (self.pluginType) {
        case BYInputPluginTypeAudio:
            iconName = @"live_plugin_audio_highlight";
            break;
        case BYInputPluginTypeText:
            iconName = @"live_plugin_text_highlight";
            break;
        case BY_SIM_MSG_TYPE_IMAGE:
            iconName = @"live_plugin_image_highlight";
            break;
        case BYInputPluginTypeMedia:
            iconName = @"live_plugin_media_highlight";
            break;
        case BYInputPluginTypePPT:
            iconName = @"live_plugin_ppt_highlight";
            break;
        case BYInputPluginTypeOperation:
            iconName = @"live_plugin_operation_highlight";
            break;
        default:
            break;
    }
    return [UIImage imageNamed:iconName];
}

- (UIView *)inputViewMoreContentView{
    UIView *moreContentView;
    switch (self.pluginType) {
        case BYInputPluginTypeAudio:
            moreContentView = _inputViewMoreContentView ? _inputViewMoreContentView : [[BYPluginAudioMoreView alloc] init];
            break;
        case BYInputPluginTypeText:
            moreContentView = _inputViewMoreContentView ? _inputViewMoreContentView : [[BYIMMessageInputView alloc] init];
            break;
        case BYInputPluginTypeMedia:
            moreContentView = _inputViewMoreContentView ? _inputViewMoreContentView : [[BYMediaMoreView alloc] init];
            break;
        case BYInputPluginTypePPT:
            moreContentView = nil;
            break;
        case BYInputPluginTypeOperation:
            moreContentView = nil;
            break;
        case BYInputPluginTypeImage:
            moreContentView = nil;
            break;
        default:
            break;
    }
    _inputViewMoreContentView = moreContentView;
    [moreContentView setBackgroundColor:[UIColor whiteColor]];
    return moreContentView;
}

- (CGFloat )inputMoreContentViewHeight{
    CGFloat height;
    switch (self.pluginType) {
        case BYInputPluginTypeAudio:
            height = 128;
            break;
        case BYInputPluginTypeText:
            height = 48;
            break;
        case BYInputPluginTypeMedia:
            height = 111;
            break;
        case BYInputPluginTypePPT:
            height = 128;
            break;
        case BYInputPluginTypeOperation:
            height = 80;
            break;
        default:
            height = 0;
            break;
    }
    return height;
}

- (void)setInputViewMoreContentView:(UIView *)inputViewMoreContentView{
    _inputViewMoreContentView = inputViewMoreContentView;
}


@end
