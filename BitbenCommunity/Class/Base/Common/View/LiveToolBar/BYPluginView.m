//
//  BYPluginView.m
//  BibenCommunity
//
//  Created by 随风 on 2019/3/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYPluginView.h"

@interface BYPluginView ()

/** 图标 */
@property (nonatomic ,strong) UIImageView *pluginImageView;
/** 插件名 */
@property (nonatomic ,strong) UILabel *pluginLab;
@end

@implementation BYPluginView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configSubView];
    }
    return self;
}

- (void)configUIWithPluginData:(BYBaseInputViewPlugin *)pluginData{
    self.pluginLab.text = pluginData.pluginName;
    self.pluginData = pluginData;
    [self updatePluginImage];
    
    @weakify(self);
    [self.pluginImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(self.pluginImageView.image.size.width);
        make.height.mas_equalTo(self.pluginImageView.image.size.height);
    }];
    
    [self.pluginLab mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.height.mas_equalTo(stringGetHeight(self.pluginLab.text, self.pluginLab.font.pointSize));
    }];
    [self layoutIfNeeded];
}

- (void)configSubView{
    self.pluginImageView = [[UIImageView alloc] init];
    [self addSubview:self.pluginImageView];
    [self.pluginImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_greaterThanOrEqualTo(0);
        make.centerY.mas_equalTo(-7);
        make.centerX.mas_equalTo(0);
    }];
    
    self.pluginLab = [[UILabel alloc] init];
    self.pluginLab.textColor = kColorRGBValue(0x353535);
    self.pluginLab.font = [UIFont systemFontOfSize:12];
    self.pluginLab.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.pluginLab];
    [self.pluginLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-6);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
}

- (void)updatePluginImage{
    self.pluginImageView.image = self.selected ? self.pluginData.pluginIconSelectIcon : self.pluginData.pluginIconNormalIcon;
}

- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    [self updatePluginImage];
    if (self.didTapPluginView) {
        self.didTapPluginView(self.pluginData.pluginType, self.selected);
    }
}


@end
