//
//  BYBaseInputViewPlugin.h
//  BibenCommunity
//
//  Created by 随风 on 2019/3/13.
//  Copyright © 2019 币本. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,BYInputPluginType) {
    BYInputPluginTypeAudio, // 语音
    BYInputPluginTypeText,  // 文字
    BYInputPluginTypeImage, // 图片
    BYInputPluginTypeMedia, // 媒体库
    BYInputPluginTypePPT,   // 课件
    BYInputPluginTypeOperation, // 操作
};

NS_ASSUME_NONNULL_BEGIN

@interface BYBaseInputViewPlugin : NSObject

/** 插件图标 */
@property (nonatomic ,strong) UIImage *pluginIconNormalIcon;
/** 插件名 */
@property (nonatomic ,strong) NSString *pluginName;
/** 插件类型 */
@property (nonatomic ,assign) BYInputPluginType pluginType;
/** inputViewMoreContentView */
@property (nonatomic ,strong) UIView *inputViewMoreContentView;

- (instancetype)initWithType:(BYInputPluginType)type;
- (instancetype)init;



/** 插件图标 */
- (UIImage *)pluginIconNormalIcon;
/** 插件高亮图标 */
- (UIImage *)pluginIconPressedIcon;
/** 插件选中图标 */
- (UIImage *)pluginIconSelectIcon;
/** 插件打开的View */
//- (UIView *)inputViewMoreContentView;
/** 插件打开的View高度 */
- (CGFloat)inputMoreContentViewHeight;


@end

NS_ASSUME_NONNULL_END
