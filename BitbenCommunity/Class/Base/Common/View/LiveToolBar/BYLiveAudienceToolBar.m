//
//  BYLiveAudienceToolBar.m
//  BibenCommunity
//
//  Created by 随风 on 2019/4/13.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYLiveAudienceToolBar.h"
#import "BYIMMessageInputView.h"

@interface BYLiveAudienceToolBar ()<UITextViewDelegate>

/** 输入框 */
@property (nonatomic ,strong) BYIMMessageTextView *inputView;
/** 遮罩 */
@property (nonatomic ,strong) UIControl *maskView;


@end

@implementation BYLiveAudienceToolBar

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self configUI];
        [self addNSNotification];
    }
    return self;
}

- (void)dismissView{
    [_inputView resignFirstResponder];
}

- (void)moreBtnAction:(UIButton *)sender{
    [self.inputView resignFirstResponder];
    if (self.moreBtnHandle) self.moreBtnHandle();
}

- (void)sendMessageAction{
    if ([BYSIMManager shareManager].isForbid) {
        showToastView(@"当前直播间已被全员禁言", _fatherView);
        return;
    }
    NSString *string = [self.inputView.text removeBothEndsEmptyString:BYRemoveTypeBothEndsSpaceWithWrap];
    if (!string.length) return;
    BYSIMMessage *msg = [[BYSIMMessage alloc] init];
    msg.msg_type = BY_SIM_MSG_TYPE_TEXT;
    msg.msg_sender = [AccountModel sharedAccountModel].account_id;
    msg.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
    msg.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
    msg.time = [NSDate getCurrentTimeStr];
    BYSIMTextElem *elem = [[BYSIMTextElem alloc] init];
    elem.text = string;
    msg.elem = elem;
    [[BYSIMManager shareManager] sendMessage:msg suc:nil fail:nil];
    self.inputView.text = @"";
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(48);
    }];
}

- (CGFloat)calculateTextViewHeight:(UITextView *)textView text:(NSString *)text{
    CGSize constraninSize = CGSizeMake(textView.contentSize.width, MAXFLOAT);
    CGSize size = [textView sizeThatFits:constraninSize];
    return size.height + 14 + textView.textContainerInset.top;
}

- (void)willMoveToWindow:(UIWindow *)newWindow{
    [super willMoveToWindow:newWindow];
    if (newWindow) {
        self.frame = CGRectMake(0, kCommonScreenHeight - kSafeAreaInsetsBottom - 48, kCommonScreenWidth, 48);
    }
}

#pragma mark - UITextViewDelegate 文本输入代理
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if (self.fatherView == self.superview) {
        [self removeFromSuperview];
        [kCommonWindow addSubview:self];
        [kCommonWindow addSubview:self.maskView];
        [kCommonWindow insertSubview:self.maskView belowSubview:self];
    }
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
//    NSString *string = [textView.text stringByReplacingCharactersInRange:range withString:text];

    if ([text isEqualToString:@"\n"] && textView.text.length) {
        [textView resignFirstResponder];
        [self sendMessageAction];
        return YES;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    CGFloat maxHeight = 70;
    CGFloat height = [self calculateTextViewHeight:textView text:textView.text];
    textView.scrollEnabled = height > maxHeight ? YES : NO;
    if (height < maxHeight ) {
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height < 48 ? 48 : height);
        }];
    }
}

#pragma mark - keyBoardNSNotification

- (void)keyBoardWillShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardDidShow:(NSNotification *)notic{
    NSDictionary *useInfo = [notic userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [self keyBoardShowAnimation:value];
}

- (void)keyBoardWillHidden:(NSNotification *)notic{
    if (self.superview && self.inputView.isFirstResponder) {
        if (self.superview == kCommonWindow) {
            [self.maskView removeFromSuperview];
            [self removeFromSuperview];
            [self.fatherView addSubview:self];
        }
        [UIView animateWithDuration:0.1 animations:^{
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
                make.left.right.mas_equalTo(0);
            }];
            [self.superview layoutIfNeeded];
        }];
    }
}

- (void)keyBoardShowAnimation:(NSValue *)value{
    if (self.superview && self.inputView.isFirstResponder) {
        CGFloat detalY = [value CGRectValue].size.height;
        [UIView animateWithDuration:0.1 animations:^{
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.right.mas_equalTo(0);
                make.bottom.mas_equalTo(-detalY);
            }];
            [self.maskView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
            [self.superview layoutIfNeeded];
        }];
    }
}

#pragma mark - UI

- (void)configUI{
    self.inputView = [[BYIMMessageTextView alloc] init];
    self.inputView.layer.cornerRadius = 5.0f;
    [self.inputView setBackgroundColor:kColorRGBValue(0xeeeeee)];
    self.inputView.tintColor = kBgColor_238;
    self.inputView.font = [UIFont systemFontOfSize:16];
    self.inputView.textContainerInset = UIEdgeInsetsMake(7, 8, 0, 5);
    self.inputView.textContainer.lineFragmentPadding = 0;
    self.inputView.returnKeyType = UIReturnKeySend;
    self.inputView.delegate = self;
    [self addSubview:self.inputView];
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 15, 7, 57));
    }];
    
    UIButton *moreBtn = [UIButton by_buttonWithCustomType];
    [moreBtn setBy_imageName:@"talk_more" forState:UIControlStateNormal];
    [moreBtn addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:moreBtn];
    @weakify(self);
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(self.inputView.center_y).mas_offset(0);
        make.width.mas_equalTo(moreBtn.imageView.image.size.width);
        make.height.mas_equalTo(moreBtn.imageView.image.size.height);
    }];
    
    UIView *topLineView = [UIView new];
    [topLineView setBackgroundColor:kColorRGBValue(0xededed)];
    [self addSubview:topLineView];
    [topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(kCommonScreenWidth);
    }];
}

#pragma mark - regisetNSNotic
- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (UIControl *)maskView{
    if (!_maskView) {
        _maskView = [UIControl by_init];
        [_maskView setBackgroundColor:[UIColor clearColor]];
        [_maskView addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _maskView;
}

@end
