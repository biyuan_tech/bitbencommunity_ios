//
//  BYSIMController.m
//  BibenCommunity
//
//  Created by 随风 on 2019/3/25.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYSIMController.h"

#import "BYPPTUploadController.h"
#import "MsgPicViewController.h"
#import "BYIMChatViewController.h"

#import "BYLiveToolBar.h"
#import "BYBaseInputViewPlugin.h"
#import "BYHostOperationView.h"
#import "CDChatList.h"
#import "BYSIMManager.h"
#import "BYHistoryIMModel.h"
#import "BYLiveAudienceToolBar.h"
#import "BYVideoLiveRewradTableCell.h"
#import "BYReportSheetView.h"


@interface BYSIMController ()<ChatListProtocol,UIScrollViewDelegate,BYLiveToolBarPluginDelegate>

/** tableview */
@property (nonatomic ,strong) CDChatListView *tableView;
/** toolBar */
@property (nonatomic ,strong) BYLiveToolBar *toolBar;
/** 观众toolBar */
@property (nonatomic ,strong) BYLiveAudienceToolBar *auToolBar;
@property (nonatomic,strong) BYHostOperationView *hostOpeartionView;
/** 页码 */
@property (nonatomic ,assign) NSInteger pageNum;
/** maskView */
@property (nonatomic ,strong) UIView *reportMaskView;
/** reportView */
@property (nonatomic ,strong) BYReportSheetView *reportSheetView;
/** 举报 */
@property (nonatomic ,strong) UIImageView *reportMarkView;
/** 记录当前发送中的图片信息 */
@property (nonatomic ,strong) NSMutableArray *image_msgs;


@end

@implementation BYSIMController

- (void)dealloc
{
    [_toolBar destory];
    [_toolBar removeFromSuperview];
    [_auToolBar removeFromSuperview];
    _toolBar = nil;
    _auToolBar = nil;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[AATAudioTool share] stopPlay];
    [[AATAudioTool share] stopRecord];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)viewDidLoad {
    _image_msgs = [NSMutableArray array];
    [super viewDidLoad];
    self.pageNum = 0;
    [self configDefaultResource];
    [self addToolBar];
    [self addTableView];
    [self addRepeortMarkView];
    [self addMessageListener];
    [self loadRequestGetHistoryMsg:nil];
}

- (void)reportMaskViewAction{
    [self showReportMarkViewHidden:YES];
}

- (void)showReportMarkViewHidden:(BOOL)hidden{
    [self.superController.view bringSubviewToFront:self.reportMaskView];
    [self.superController.view bringSubviewToFront:self.reportMarkView];
    [UIView animateWithDuration:0.1 animations:^{
        self.reportMarkView.layer.opacity = !hidden;
        self.reportMaskView.layer.opacity = !hidden;
    }];
}

// 选择发送图片
- (void)selectMessagePic{
    GWAssetsLibraryViewController *assetVC = [[GWAssetsLibraryViewController alloc] init];
    @weakify(self);
    [assetVC selectImageArrayFromImagePickerWithMaxSelected:9 andBlock:^(NSArray *selectedImgArr) {
        @strongify(self);
        NSArray *msgArr = [BYHistoryIMModel getNoSendImgMsgData:selectedImgArr liveData:self.model];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView addMessagesToBottom:msgArr];
        });
        [self.image_msgs addObjectsFromArray:msgArr];
        [self loadUploadSelectImg:selectedImgArr msgArr:msgArr];
    }];
    [self.superController.navigationController pushViewController:assetVC animated:YES];

}

- (NSArray *)removeSelfSendingMsg:(NSArray *)msgs{
    if (!self.image_msgs.count) {
        return msgs;
    }
    NSMutableArray *data = [NSMutableArray arrayWithArray:msgs];
    NSMutableArray *tmpArr = msgs.mutableCopy;
    NSMutableArray *tmpImgsMsgArr = self.image_msgs.mutableCopy;
    for (BYHistoryIMModel *model in tmpArr) {
        for (BYHistoryIMModel *imgMsgModel in tmpImgsMsgArr) {
            if ([[model.msg getFullImageUploadUrl] isEqualToString:[imgMsgModel.msg getFullImageUploadUrl]]) {
                imgMsgModel.messageId = model.messageId;
                [data removeObject:model];
                [self.image_msgs removeObject:model];
            }
        }
    }
    return data;
}

#pragma mark - buttonAction
- (void)reportBtnAction:(UIButton *)sender{
    [self showReportMarkViewHidden:YES];
    [self.reportSheetView showAnimation];
}
#pragma mark - request

- (void)loadRequestGetHistoryMsg:(void(^)(NSArray *data))cb{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestGetPageChatHistory:self.model.group_id pageNum:self.pageNum sendTime:[BYSIMManager shareManager].joinTime successBlock:^(id object) {
        @strongify(self);
        if (!self) return ;
        if (![object[@"content"] isKindOfClass:[NSArray class]] || ![object[@"content"] count]) {
            if (cb) cb(@[]);
            return;
        }
        if (self.pageNum == 0) {
            NSMutableArray *data = [NSMutableArray array];
            NSArray *array = [BYHistoryIMModel getSIMTableData:object[@"content"] liveData:self.model];
            array = [[array reverseObjectEnumerator] allObjects];
            [data addObjectsFromArray:array];
            [data addObjectsFromArray:self.tableView.msgArr];
            self.tableView.msgArr = array;
            [self.tableView reloadData];
            [self.tableView relayoutTable:NO];
        }
        else{
            NSArray *array = [BYHistoryIMModel getSIMTableData:object[@"content"] liveData:self.model];
            array = [[array reverseObjectEnumerator] allObjects];
            cb(array);
        }
        self.pageNum ++;
        
    } faileBlock:^(NSError *error) {

    }];
}

- (void)loadUploadSelectImg:(NSArray *)images msgArr:(NSArray *)msgArr{
    NSMutableArray *fileModels = [NSMutableArray array];
    for (int i = 0 ; i < images.count; i ++) {
        UIImage *image = images[i];
        OSSFileModel *fileModel = [[OSSFileModel alloc] init];
        fileModel.objcImage = image;
        fileModel.objcName  = [NSString stringWithFormat:@"%@-%@-%d",[NSDate by_stringFromDate:[NSDate by_date] dateformatter:@"yyyy-MM-dd-HH:mm:ss"],@"MSG_IMG",i];
        [fileModels addObject:fileModel];
    }
    [[OSSManager sharedUploadManager] uploadImageManagerWithImgList:[fileModels copy] progress:^(NSInteger index, float percent) {
        
    } finish:^(NSArray *url, NSInteger index, BOOL allUpload) {
        PDLog(@"接收到图片上传回调---index:%li",index);
        if (allUpload) return ;
        BYHistoryIMModel *msgModel = msgArr[index];
        BYSIMMessage *msg = [[BYSIMMessage alloc] init];
        msg.msg_type = BY_SIM_MSG_TYPE_IMAGE;
        msg.msg_sender = [AccountModel sharedAccountModel].account_id;
        msg.msg_senderName = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
        msg.msg_senderlogoUrl = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
        msg.time = [NSDate getCurrentTimeStr];
        BYSIMImageElem *elem = [[BYSIMImageElem alloc] init];
        elem.path = url[0];
        msg.elem = elem;
        msgModel.msg = url[0];
        @weakify(self);
        [[BYSIMManager allocManager] sendMessage:msg suc:^{
            @strongify(self);
            dispatch_async(dispatch_get_main_queue(), ^{
                msgModel.msgState = CDMessageStateNormal;
                [self.tableView updateMessage:msgModel];
            });
        } fail:^{
            @strongify(self);
            dispatch_async(dispatch_get_main_queue(), ^{
                msgModel.msgState = CDMessageStateSendFaild;
                [self.tableView updateMessage:msgModel];
            });
        }];
    }];
}

- (void)addMessageListener{
    // 消息回调
    @weakify(self);
    [[BYSIMManager shareManager] setMessageListener:^(NSArray * _Nonnull msgs) {
        @strongify(self);
        NSDictionary *dic = msgs[0];
        if (dic) {
            if ([dic[@"content_type"] integerValue] == BY_SIM_MSG_TYPE_SYSTEM) {
                NSDictionary *customData;
                if ([dic[@"content"] isKindOfClass:[NSString class]]) {
                    // 现返回的为纯json字符串，需要转两次
                    NSData *data = [dic[@"content"] dataUsingEncoding:NSUTF8StringEncoding];
                    customData =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                }
                else{
                    customData = dic[@"content"];
                }
                if ([customData[kBYCustomMessageType] integerValue] == BY_GUEST_OPERA_TYPE_RECEIVE_STREAM) { // 开播消息
                    if (self.didBeginLiveHandle && !self.isHost) {
                        self.didBeginLiveHandle();
                    }
                }
                else if ([customData[kBYCustomMessageType] integerValue] == BY_GUEST_OPERA_TYPE_STOP_PUSH_STREAM) { // 关播消息
                    if (self.didReceiveLiveEndMsgHandle && !self.isHost) {
                        self.didReceiveLiveEndMsgHandle();
                    }
                }
                return ;
            }
        }
        
        NSArray *data = [BYHistoryIMModel getSIMTableData:msgs liveData:self.model];
        NSArray *array = [self removeSelfSendingMsg:data];
        if (!array.count) return;
        [self.tableView addMessagesToBottom:array];
    }];
}

// 关闭直播弹窗
- (void)showExitSheetView{
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:@"确定要结束直播吗？" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    @weakify(self);
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        if (self.didFinishLiveHandle) {
            self.didFinishLiveHandle();
        }
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [sheetController addAction:confirmAction];
    [sheetController addAction:cancelAction];
    [self.navigationController presentViewController:sheetController animated:YES completion:nil];
}

#pragma mark - chatListViewDelegate
- (void)chatlistBecomeFirstResponder{
    [self.view endEditing:YES];
    [_toolBar hiddenToolBar];
}

- (void)chatlistDidDidScroll:(UIScrollView *)scrollView{
    if (self.scrollViewDidScroll) self.scrollViewDidScroll(scrollView);
}

-(void)chatlistLoadMoreMsg: (CDChatMessage)topMessage
                  callback: (void(^)(CDChatMessageArray, BOOL))finnished{
    [self loadRequestGetHistoryMsg:^(NSArray *data) {
        finnished(data,data.count ? YES : NO);
    }];
}

- (NSDictionary<NSString *,Class> *)chatlistCustomeCellsAndClasses{
    return @{@"RewardCell": BYVideoLiveRewradTableCell.class};

}
-(CGSize)chatlistSizeForMsg:(CDChatMessage)msg ofList:(CDChatListView *)list{
    if (![msg.customData[@"text"] length]) {
        return CGSizeZero;
    }
    CGFloat maxWidth = iPhone5 ? kCommonScreenWidth - 70*2 : kCommonScreenWidth - 80*2;
    CGSize size = [msg.customData[@"text"] getStringSizeWithFont:[UIFont systemFontOfSize:12] maxWidth:maxWidth];
    return CGSizeMake(kCommonScreenWidth, ceil(size.height) + 14 + 15);
}

-(void)chatlistClickMsgEvent: (ChatListInfo *)listInfo{
    switch (listInfo.eventType) {
        case ChatClickEventTypeIMAGE:
        {
            CGRect newe =  [listInfo.containerView.superview convertRect:listInfo.containerView.frame toView:kCommonWindow];
            [MsgPicViewController addToRootViewController:listInfo.image ofMsgId:listInfo.msgModel.messageId in:newe from:self.tableView.msgArr];
        }
            break;
        case ChatClickEventTypeTEXT:
            break;
        case ChatClickEventTypeAUDIO:
        {
            
        }
            break;
        case ChatClickEventTypeREWARD: // 点击赞赏
        {
//            self.rewardView.receicer_id = listInfo.msgModel.userId;
//            self.rewardView.receicer_name = listInfo.msgModel.userName;
//            [self.rewardView showAnimation];
        }
            break;
        case ChatClickEventTypeBanned: // 点击禁言
            showToastView(@"点击禁言", self.view);
            break;
    }
}

- (void)chatlistAudioMsgReadStatusDidChange:(CDChatMessage)msg{
    // 设置语音消息为已读
    [[BYIMMessageDB shareManager] modifyIMMessageIsRead:msg.messageId];
}

#pragma mark - BYLiveToolBarPluginDelegate 音频录制回调
- (void)recordAudioDidFinish:(NSURL *)audioPath{
   
}

#pragma mark - UI

- (void)addToolBar{
    if (!_isHost) {
        self.auToolBar = [[BYLiveAudienceToolBar alloc] init];
        self.auToolBar.fatherView = self.view;
        @weakify(self);
        self.auToolBar.moreBtnHandle = ^{
            @strongify(self);
            [self showReportMarkViewHidden:NO];
        };
        [self.view addSubview:self.auToolBar];
        [self.auToolBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.height.mas_equalTo(48);
            make.bottom.mas_equalTo(0);
        }];
        return;
    }
    NSMutableArray *plugins = [NSMutableArray array];
    for (int i = 0; i < 5; i ++) {
        BYInputPluginType type;
        switch (i) {
            case 0:
                type = BYInputPluginTypeAudio;
                break;
            case 1:
                type = BYInputPluginTypeText;
                break;
            case 2:
                type = BYInputPluginTypeImage;
                break;
            case 3:
                type = BYInputPluginTypePPT;
                break;
            default:
                type = BYInputPluginTypeOperation;
                break;
        }
        BYBaseInputViewPlugin *pluginData = [[BYBaseInputViewPlugin alloc] initWithType:type];
        if (i == 1 || i == 4) {
            [plugins addObject:pluginData];
        }
        else if (i == 3 || i == 0){
            if (self.model.live_type == BY_NEWLIVE_TYPE_AUDIO_PPT ||
                self.model.live_type ==  BY_NEWLIVE_TYPE_AUDIO_LECTURE) {
                [plugins addObject:pluginData];
            }
        }
        else if (i == 2){
            if (self.model.live_type == BY_NEWLIVE_TYPE_AUDIO_PPT ||
                self.model.live_type ==  BY_NEWLIVE_TYPE_AUDIO_LECTURE ||
                self.model.live_type == BY_NEWLIVE_TYPE_VIDEO) {
                [plugins addObject:pluginData];
            }
        }
    }
    
    BYLiveToolBar *toolBar = [[BYLiveToolBar alloc] initToolBar:plugins];
    toolBar.fatherView = self.view;
    toolBar.delegate = self;
    self.toolBar = toolBar;
    @weakify(self);
    toolBar.didTapPluginView = ^(BYInputPluginType type) {
        @strongify(self);
        if (type == BYInputPluginTypePPT) { // ppt
            BYPPTUploadController *pptUploadController = [[BYPPTUploadController alloc] init];
            pptUploadController.record_id = self.model.live_record_id;
            pptUploadController.pptImgs = ((BYIMChatViewController *)self.superController).headerView.advertData;
            pptUploadController.live_type = self.model.live_type;
            @weakify(self);
            pptUploadController.didUploadPPTHandle = ^(NSArray * _Nonnull pptImgs) {
                @strongify(self);
                if (self.didUpdatePPTImgs) self.didUpdatePPTImgs(pptImgs);
            };
            [self.navigationController pushViewController:pptUploadController animated:YES];
        }else if (type == BYInputPluginTypeOperation) { // 操作
            [self.hostOpeartionView showAnimation];
        }else if (type == BYInputPluginTypeImage){ // 图片
            [self selectMessagePic];
        }
    };
    [self.view addSubview:toolBar];
    [toolBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(48);
        make.bottom.mas_equalTo(0);
    }];
    
    self.hostOpeartionView = [[BYHostOperationView alloc] initWithFathureView:kCommonWindow];
    _hostOpeartionView.didEndLiveHandle = ^{
        @strongify(self);
        [self showExitSheetView];
    };
}

- (void)addTableView{
    if (_tableView) return;
    self.tableView = [[CDChatListView alloc] initWithFrame:CGRectZero];
    self.tableView.msgDelegate = self;
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.tableView];
    UIView *toolBar = _isHost ? self.toolBar : self.auToolBar;
    [self.view insertSubview:self.tableView belowSubview:toolBar];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 48, 0));
    }];
}

- (void)addRepeortMarkView{
    if (_reportMarkView) return;
    
    self.reportSheetView = [BYReportSheetView initReportSheetViewShowInView:kCommonWindow];
    self.reportSheetView.theme_id = self.model.live_record_id;
    self.reportSheetView.theme_type = BY_THEME_TYPE_LIVE;
    self.reportSheetView.user_id = self.model.user_id;
    
    self.reportMaskView = [UIView by_init];
    self.reportMaskView.layer.opacity = 0.0f;
    [self.superController.view addSubview:self.reportMaskView];
    [self.reportMaskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reportMaskViewAction)];
    [self.reportMaskView addGestureRecognizer:tapGestureRecognizer];
    
    self.reportMarkView = [[UIImageView alloc] init];
    [self.reportMarkView by_setImageName:@"common_report_single"];
    [self.superController.view addSubview:self.reportMarkView];
    self.reportMarkView.userInteractionEnabled = YES;
    self.reportMarkView.layer.shadowColor = kColorRGBValue(0x4e4e4e).CGColor;
    self.reportMarkView.layer.shadowOpacity = 0.2;
    self.reportMarkView.layer.shadowRadius = 25;
    self.reportMarkView.layer.shadowOffset = CGSizeMake(0, -2);
    self.reportMarkView.layer.opacity = 0.0f;
    @weakify(self);
    [self.reportMarkView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(-40);
        make.width.mas_equalTo(self.reportMarkView.image.size.width);
        make.height.mas_equalTo(self.reportMarkView.image.size.height);
    }];
    
    UIButton *reportBtn = [UIButton by_buttonWithCustomType];
    [reportBtn setBy_attributedTitle:@{@"title":@"举报",NSForegroundColorAttributeName:kColorRGBValue(0x4f4f4f),NSFontAttributeName:[UIFont systemFontOfSize:13]} forState:UIControlStateNormal];
    [reportBtn addTarget:self action:@selector(reportBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.reportMarkView addSubview:reportBtn];
    [reportBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
}

-(void)configDefaultResource{
    
    // 聊天页面图片资源配置
    
    ChatHelpr.share.config.msgBackGroundColor = [UIColor whiteColor];
    NSMutableDictionary *resDic = [NSMutableDictionary dictionaryWithDictionary:ChatHelpr.share.imageDic];
    [resDic setObject:[UIImage imageNamed:@"voice_left_1"] forKey:@"voice_left_1"];
    [resDic setObject:[UIImage imageNamed:@"voice_left_2"] forKey:@"voice_left_2"];
    [resDic setObject:[UIImage imageNamed:@"voice_left_3"] forKey:@"voice_left_3"];
    [resDic setObject:[UIImage imageNamed:@"voice_right_1"] forKey:@"voice_right_1"];
    [resDic setObject:[UIImage imageNamed:@"voice_right_2"] forKey:@"voice_right_2"];
    [resDic setObject:[UIImage imageNamed:@"voice_right_3"] forKey:@"voice_right_3"];
    [resDic setObject:[[UIImage imageNamed:@"talk_pop_l_nor"] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 8, 6, 6)] forKey:@"talk_pop_l_nor"];
    [resDic setObject:[UIImage imageNamed:@"common_userlogo_bg"] forKey:@"common_userlogo_bg"];
    [resDic setObject:[UIImage imageNamed:@"iliveroom_reward"] forKey:@"reward_left"];
    [resDic setObject:[UIImage imageNamed:@"icon_live_banned"] forKey:@"banned_left"];
    NSDictionary *drawImages = [ChatImageDrawer defaultImageDic];
    for (NSString *imageName in drawImages) {
        resDic[imageName] = drawImages[imageName];
    }
    
    ChatHelpr.share.imageDic = resDic;
    ChatHelpr.share.config.left_box = @"talk_pop_l_nor";
    
    // 添加语音输入功能
    [CTinputHelper.share.config addVoice];
    
}

@end
