//
//  BYToastView.h
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger ,kToastViewType) {
    kToastViewTypeNormal,  // 普通纯文案类
    kToastViewTypeSuccess,  // 成功样式
    kToastViewTypeLoading,  // 加载样式
    kToastViewTypeLike, // 收藏样式
    kToastViewTypeHomeSwith , // 首页切换列表样式
};

@interface BYToastView : UIView

/**
 *  创建loadingToastView,当前视图不响应用户操作，需手动调用dissmiss消失
 *
 *  @return toastView
 */
+ (BYToastView *)toastViewPresentLoading;

/**
 *  创建loadingToastView，需手动调用dissmiss消失
 *
 *  @param superView 父视图
 *
 *  @return toastView
 */
+ (BYToastView *)toastViewPresentLoadingInView:(UIView *)superView;

/**
 *  创建加载至window上的toastView（默认当前父视图不可点击）
 *
 *  @param title         标题
 *  @param duration      时长
 *  @param toastViewType 展示样式
 *  @param complete      消失后回调
 *
 *  @return toastView
 */
+ (BYToastView *)toastViewPresentInWindowTitle:(NSString *)title
                                       duration:(NSTimeInterval)duration
                                  toastViewType:(kToastViewType)toastViewType
                                       complete:(void(^)(void))complete;
/**
 *  创建toastView
 *
 *  @param view          superView
 *  @param title         标题
 *  @param duration      显示时长
 *  @param toastViewType 展示样式
 *  @param complete      消失后回调
 *
 *  @return toastView
 */
+ (BYToastView *)tostViewPresentInView:(UIView *)view
                                  title:(NSString *)title
                               duration:(NSTimeInterval)duration
                          toastViewType:(kToastViewType)toastViewType
                               complete:(void(^)(void))complete;

/**
 *  创建纯文案类toastView
 *
 *  @param view     superView
 *  @param title    标题
 *  @param duration 显示时长
 *
 *  @return toastView
 */
+ (BYToastView *)tostViewPresentInView:(UIView *)view
                                  title:(NSString *)title
                               duration:(NSTimeInterval)duration
                               complete:(void(^)(void))complete;

/**
 *  创建toastView
 *
 *  @param view          superView
 *  @param title         标题
 *  @param color         标题颜色
 *  @param duration      显示时长
 *  @param toastViewType 展示样式
 *
 *  @return toastView
 */
+ (BYToastView *)tostViewPresentInView:(UIView *)view
                                  title:(NSString *)title
                             titleColor:(UIColor *)color
                               duration:(NSTimeInterval)duration
                          toastViewType:(kToastViewType)toastViewType
                               complete:(void(^)(void))complete;

- (void)dissmissToastView;

@end
