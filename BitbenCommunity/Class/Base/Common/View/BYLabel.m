//
//  BYLabel.m
//  BibenCommunity
//
//  Created by 随风 on 2019/4/3.
//  Copyright © 2019 币本. All rights reserved.
//

#import "BYLabel.h"

static char const fontStrokeWidthKey;
static char const fontStrokeColorKey;
static char const labelColorKey;
@implementation BYLabel

- (void)drawTextInRect:(CGRect)rect{
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(c, self.fontStrokeWidth);
    CGContextSetLineJoin(c, kCGLineJoinRound);
    CGContextSetTextDrawingMode(c, kCGTextStroke);
    self.textColor = self.fontStrokeColor;
    [super drawTextInRect:rect];
    self.textColor = self.labelColor;
    CGContextSetTextDrawingMode(c, kCGTextFill);
    [super drawTextInRect:rect];
  
}

- (void)setFontStrokeColor:(UIColor *)fontStrokeColor{
    objc_setAssociatedObject(self, &fontStrokeColorKey, fontStrokeColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setFontStrokeWidth:(CGFloat)fontStrokeWidth{
    objc_setAssociatedObject(self, &fontStrokeWidthKey, @(fontStrokeWidth), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setLabelColor:(UIColor *)labelColor{
    objc_setAssociatedObject(self, &labelColorKey, labelColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIColor *)fontStrokeColor{
    UIColor *color = objc_getAssociatedObject(self, &fontStrokeColorKey);
    return color;
}

- (CGFloat )fontStrokeWidth{
    NSNumber *value = objc_getAssociatedObject(self, &fontStrokeWidthKey);
    return [value floatValue];
}

- (UIColor *)labelColor{
    UIColor *color = objc_getAssociatedObject(self, &labelColorKey);
    return color;
}


@end
