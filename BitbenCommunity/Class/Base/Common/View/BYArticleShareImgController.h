//
//  BYArticleShareImgController.h
//  BitbenCommunity
//
//  Created by Belief on 2020/2/5.
//  Copyright © 2020 币源网络. All rights reserved.
//  文章生成长图分享

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYArticleShareImgController : UIViewController

- (void)loadWebContent:(NSString *)content;

@end

NS_ASSUME_NONNULL_END
