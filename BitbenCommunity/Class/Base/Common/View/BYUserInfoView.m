//
//  BYUserInfoView.m
//  BY
//
//  Created by 黄亮 on 2018/8/10.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYUserInfoView.h"
#import "UILabel+BYExtension.h"
#import "BYCommonUserModel.h"

#define kBaseNumTag 0x4153
#define kBaseBtnTag 0x5934
@interface BYUserInfoView()

@property (nonatomic ,weak) UIView *superView;
@property (nonatomic ,strong) UIView *contentView;

// ** 用户头像
@property (nonatomic ,strong) UIImageView *logoImgView;

// ** 用户昵称
@property (nonatomic ,strong) UILabel *userNameLab;

// ** 用户id
@property (nonatomic ,strong) UILabel *userIdLab;

// ** 签名
@property (nonatomic ,strong) UILabel *signLab;

@end

@implementation BYUserInfoView

+ (id)initWithShowInView:(UIView *)view
               userModel:(BYCommonUserModel *)userModel{
    BYUserInfoView *userInfoView = [[BYUserInfoView alloc] initWithFrame:CGRectMake(0, 0, view.bounds.size.width, view.bounds.size.height)];
    userInfoView.superView = view;
    userInfoView.userModel = userModel;
    return userInfoView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.layer.opaque = 0.0;
        self.contentView.alpha = 0.0;
        [self setContentView];
    }
    return self;
}

- (void)setUserModel:(BYCommonUserModel *)userModel{
    _userModel = userModel;
    [_logoImgView sd_setImageWithURL:[NSURL URLWithString:_userModel.user_headImgURL]];
    _userNameLab.text = _userModel.user_name;
    _userIdLab.text = [NSString stringWithFormat:@"ID:%li",(long)_userModel.user_id];
    _signLab.text = _userModel.user_sign;
//    @weakify(self);
//    [_signLab mas_updateConstraints:^(MASConstraintMaker *make) {
//        @strongify(self);
//        make.height.mas_equalTo(CGRectGetHeight(self.signLab.frame));
//    }];
    
    for (int i = 0; i < 4; i ++) {
        UILabel *labelNum = (UILabel *)[_contentView viewWithTag:kBaseNumTag + i];
        switch (i) {
            case 0:
                labelNum.text = stringFormatInteger(_userModel.user_attentionNum);
                break;
            case 1:
                labelNum.text = stringFormatInteger(_userModel.user_liveNum);
                break;
            case 2:
                labelNum.text = stringFormatInteger(_userModel.user_fansNum);
                break;
            case 3:
                labelNum.text = stringFormatInteger(_userModel.user_schoolNum);
                break;
            default:
                break;
        }
    }
}

- (void)showAnimation{
    [self.superView addSubview:self];
    self.contentView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView beginAnimations:@"animation" context:nil];
    [UIView setAnimationDuration:0.1];
    self.contentView.alpha = 1.0;
    self.contentView.layer.transform = CATransform3DMakeScale(1.1, 1.1, 1);
//    [UIView setAnimationDelay:0.1];
//    [UIView setAnimationDuration:0.1];
//    self.contentView.layer.transform = CATransform3DMakeScale(0.8, 0.8, 1);
    [UIView setAnimationDelay:0.1];
    [UIView setAnimationDuration:0.1];
    self.contentView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView commitAnimations];
}

- (void)hiddenAnimation{
    [UIView beginAnimations:@"hidden" context:nil];
    [UIView setAnimationDuration:0.2];
    self.contentView.layer.opaque = 0;
    [UIView commitAnimations];
    [self removeFromSuperview];
}

// 举报事件
- (void)reportBtnAction{
    _reportActionHandle(_userModel.user_id);
}

// 退出事件
- (void)exitBtnAction{
    [self hiddenAnimation];
}

// 主页与关注点击事件
- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - kBaseBtnTag;
    if (index == 0) { // 主页事件
        _homePageActionHandle(_userModel.user_id);
    }
    else // 关注事件
    {
        _attentionActionHandle(_userModel.user_id);
    }
}

#pragma mark - configSubView Method
- (void)setContentView{
    // 添加毛玻璃效果
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    effectView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    effectView.layer.opacity = 0.7;
    [self addSubview:effectView];
    
    CGFloat rate = 283/375.0;
    CGFloat rateWH = 283/330.0;
    CGFloat width = kCommonScreenWidth*rate;
    UIView *contentView = [[UIView alloc] init];
    [contentView setBackgroundColor:[UIColor whiteColor]];
    [contentView layerCornerRadius:5 size:CGSizeMake(width, width/rateWH)];
    [self addSubview:contentView];
    _contentView = contentView;
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(width/rateWH);
        make.centerX.centerY.mas_equalTo(0);
    }];
    
    // 举报
    UIButton *reportBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    NSAttributedString *reportAttributedString = [[NSAttributedString alloc] initWithString:@"举报" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13],NSForegroundColorAttributeName:kColorRGBValue(0x808080)}];
    [reportBtn setAttributedTitle:reportAttributedString forState:UIControlStateNormal];
    [reportBtn addTarget:self action:@selector(reportBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:reportBtn];
    [reportBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(46);
        make.height.mas_equalTo(33);
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(10);
    }];
    
    // 退出按钮
    UIButton *exitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [exitBtn addTarget:self action:@selector(exitBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [exitBtn setImage:[UIImage imageNamed:@"liveRoom_exit"] forState:UIControlStateNormal];
    [contentView addSubview:exitBtn];
    [exitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(50);
        make.centerY.equalTo(reportBtn.mas_centerY).with.offset(0);
        make.right.mas_equalTo(0);
    }];
    
    // 用户头像
    UIImageView *logoImgView = [[UIImageView alloc] init];
    [logoImgView layerCornerRadius:32 size:CGSizeMake(64, 64)];
    [contentView addSubview:logoImgView];
    _logoImgView = logoImgView;
    [logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(64);
        make.top.mas_equalTo(50);
        make.centerX.mas_equalTo(0);
    }];
    
    // 用户名
    UILabel *userNameLab = [[UILabel alloc] init];
    userNameLab.font = [UIFont systemFontOfSize:13];
    userNameLab.textColor = kColorRGBValue(0x3a3a3a);
    userNameLab.textAlignment = NSTextAlignmentCenter;
    [contentView addSubview:userNameLab];
    _userNameLab = userNameLab;
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.equalTo(logoImgView.mas_bottom).with.offset(10);
        make.left.mas_equalTo(40);
        make.right.mas_equalTo(-40);
        make.height.mas_equalTo(14);
    }];
    
    // 用户ID
    UILabel *userIdLab = [[UILabel alloc] init];
    userIdLab.font = [UIFont systemFontOfSize:12];
    userIdLab.textColor = kColorRGBValue(0x595959);
    userIdLab.textAlignment = NSTextAlignmentCenter;
    [contentView addSubview:userIdLab];
    _userIdLab = userIdLab;
    [userIdLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(40);
        make.right.mas_equalTo(-40);
        make.top.equalTo(userNameLab.mas_bottom).with.offset(8);
        make.height.mas_equalTo(10);
    }];
    
    // 签名
    UILabel *signLab = [[UILabel alloc] init];
    signLab.text = @"TA好像忘记签名了";
    signLab.font = [UIFont systemFontOfSize:10];
    signLab.textColor = kColorRGBValue(0x777777);
    signLab.textAlignment = NSTextAlignmentCenter;
    signLab.numberOfLines = 0;
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"TA" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]}];
    [signLab setTagAttributedString:attributedString];
    [contentView addSubview:signLab];
    _signLab = signLab;
    [signLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo (40);
        make.right.mas_equalTo(-40);
        make.top.equalTo(userIdLab.mas_bottom).with.offset(15);
        make.height.mas_greaterThanOrEqualTo(13);
    }];
    
    // 创建关注,直播,粉丝，学堂数
    NSArray *labelTitles = @[@"关注 :",@"直播 :",@"粉丝 :",@"学堂 :"];
    for (int i = 0; i < 4; i++) {
        // 标题
        UILabel *label = [[UILabel alloc] init];
        label.text = labelTitles[i];
        label.font = [UIFont systemFontOfSize:12];
        label.textColor = kColorRGBValue(0x393939);
        [contentView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i < 2)
                make.right.equalTo(logoImgView.mas_left).with.offset(-36);
            else
                make.left.equalTo(logoImgView.mas_right).with.offset(36);
            make.width.mas_equalTo(32);
            make.height.mas_equalTo(13);
            make.top.equalTo(signLab.mas_bottom).with.offset(i == 0 || i == 2 ? 42 : 66);
        }];
        
        // 数值
        UILabel *labelNum = [[UILabel alloc] init];
        labelNum.font = [UIFont systemFontOfSize:13];
        labelNum.textColor = kColorRGBValue(0x919191);
        [contentView addSubview:labelNum];
        labelNum.tag = kBaseNumTag + i;
        [labelNum mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(60);
            make.height.mas_equalTo(13);
            make.centerY.equalTo(label.mas_centerY).with.offset(0);
            make.left.equalTo(label.mas_right).with.offset(5);
        }];
    }
    
    // 创建底部按钮
    NSArray *btnTitles = @[@"主页",@"关注"];
    for (int i = 0; i < 2; i ++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:btnTitles[i] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13],NSForegroundColorAttributeName:i == 0 ? kColorRGBValue(0x242424) : kColorRGBValue(0xed4a45)}];
        [button setAttributedTitle:attributedString forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:button];
        button.tag = kBaseBtnTag + i;
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(width/2);
            make.left.mas_equalTo(i == 0 ? 0 : width/2);
            make.bottom.mas_equalTo(0);
            make.height.mas_equalTo(49);
        }];
    }
    
    // 创建横竖线
    for (int j = 0; j < 2; j ++) {
        UIView *lineView = [[UIView alloc] init];
        [lineView setBackgroundColor:kColorRGBValue(0xdbdbdb)];
        [contentView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (j == 0) {
                make.right.left.mas_equalTo(0);
                make.bottom.mas_equalTo(-49);
                make.height.mas_equalTo(0.5);
            }
            else
            {
                make.centerX.mas_equalTo(0);
                make.height.mas_equalTo(49);
                make.width.mas_equalTo(0.5);
                make.bottom.mas_equalTo(0);
            }
        }];
    }
}

@end
