//
//  BYDaySignInView.m
//  BitbenCommunity
//
//  Created by Belief on 2020/2/5.
//  Copyright © 2020 币源网络. All rights reserved.
//

#import "BYDaySignInView.h"
#import "VersionInvitationViewController.h"
#import "CenterBBTViewController.h"

@interface BYDaySignInView ()

/** maskView遮罩 */
@property (nonatomic ,strong) UIView *maskView;
/** contenView */
@property (nonatomic ,strong) UIView *contentView;
/** bbt数量*/
@property (nonatomic ,strong) UILabel *bbtAmountLab;
/** bp数量*/
@property (nonatomic ,strong) UILabel *bpAmountLab;
/** 隐藏按钮*/
@property (nonatomic ,strong) UIButton *dismissBtn;
@end

@implementation BYDaySignInView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = kCommonWindow.bounds;
        [self setContentView];
    }
    return self;
}

+ (void)showAnimation:(NSString *)bbt bp:(NSString *)bp{
    BYDaySignInView *signInView = [[BYDaySignInView alloc] init];
    [signInView showAnimation:bbt bp:bp];
}

- (void)showAnimation:(NSString *)bbt bp:(NSString *)bp{
    [kCommonWindow addSubview:self];
    self.bbtAmountLab.text = nullToEmpty(bbt);
    self.bpAmountLab.text = nullToEmpty(bp);
    CGSize bbtSize = [self.bbtAmountLab.text getStringSizeWithFont:self.bbtAmountLab.font maxWidth:200];
    CGSize bpSize = [self.bpAmountLab.text getStringSizeWithFont:self.bpAmountLab.font maxWidth:200];

    [self.bbtAmountLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceil(bbtSize.width));
    }];

    [self.bpAmountLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ceil(bpSize.width));
    }];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.contentView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            self.contentView.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0);
            self.dismissBtn.alpha = 1.0f;
        }];
    }];
}

- (void)hiddenAnimation{
    [UIView animateWithDuration:0.2 animations:^{
        self.layer.transform = CATransform3DMakeScale(2.0, 2.0, 2.0);
        self.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - action
/** 奖励详情*/
- (void)awardBtnAction{
    [self hiddenAnimation];
    CenterBBTViewController *bbtViewController = [[CenterBBTViewController alloc] init];
    [CURRENT_VC.navigationController pushViewController:bbtViewController animated:YES];
}

/** 邀请好友*/
- (void)inviteFriendBtnAction{
    [self hiddenAnimation];
    VersionInvitationViewController *controller = [[VersionInvitationViewController alloc] init];
    [CURRENT_VC.navigationController pushViewController:controller animated:YES];
}

#pragma mark - configUI
- (void)setContentView{
    self.maskView = [UIView by_init];
    [self.maskView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.4]];
    [self addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    self.contentView = [UIView by_init];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    self.contentView.layer.cornerRadius = 10.0f;
    [self addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(335);
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(-30);
    }];
    
    UIImageView *headerImgView = [UIImageView by_init];
    [headerImgView by_setImageName:@"common_daysign_header"];
    [self.contentView addSubview:headerImgView];
    [headerImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(130);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    [titleLab setBy_font:12];
    titleLab.textColor = kColorRGBValue(0xa1a1a1);
    titleLab.textAlignment = NSTextAlignmentCenter;
    titleLab.text = @"根据您昨日活跃贡献，共获得奖励";
    [self.contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(headerImgView.mas_bottom);
        make.height.mas_equalTo(titleLab.font.pointSize);
    }];
    
    UILabel *bbtUnitLab = [UILabel by_init];
    [bbtUnitLab setBy_font:12];
    bbtUnitLab.textColor = kColorRGBValue(0xa1a1a1);
    bbtUnitLab.text = @"BBT";
    [self.contentView addSubview:bbtUnitLab];
    [bbtUnitLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-130);
        make.width.mas_equalTo(stringGetWidth(bbtUnitLab.text, 12));
        make.height.mas_equalTo(stringGetHeight(bbtUnitLab.text, 12));
        make.top.mas_equalTo(titleLab.mas_bottom).offset(34);
    }];
    
    self.bbtAmountLab = [UILabel by_init];
    self.bbtAmountLab.font = [UIFont boldSystemFontOfSize:22];
    self.bbtAmountLab.textColor = kColorRGBValue(0xea6438);
    self.bbtAmountLab.text = @"42532";
    [self.contentView addSubview:self.bbtAmountLab];
    CGSize bbtSize = [self.bbtAmountLab.text getStringSizeWithFont:self.bbtAmountLab.font maxWidth:200];
    [self.bbtAmountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bbtUnitLab.mas_left).offset(-2);
        make.bottom.mas_equalTo(bbtUnitLab).offset(2);
        make.height.mas_equalTo(ceil(bbtSize.height));
        make.width.mas_equalTo(ceil(bbtSize.width));
    }];
    
    self.bpAmountLab = [UILabel by_init];
    self.bpAmountLab.font = [UIFont boldSystemFontOfSize:22];
    self.bpAmountLab.textColor = kColorRGBValue(0xea6438);
    self.bpAmountLab.text = @"2.54万";
    [self.contentView addSubview:self.bpAmountLab];
    CGSize bpSize = [self.bpAmountLab.text getStringSizeWithFont:self.bpAmountLab.font maxWidth:200];
    [self.bpAmountLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(130);
        make.bottom.mas_equalTo(bbtUnitLab).offset(2);
        make.height.mas_equalTo(ceil(bpSize.height));
        make.width.mas_equalTo(ceil(bpSize.width));
    }];
    
    UILabel *bpUnitLab = [UILabel by_init];
    [bpUnitLab setBy_font:12];
    bpUnitLab.textColor = kColorRGBValue(0xa1a1a1);
    bpUnitLab.text = @"BP";
    [self.contentView addSubview:bpUnitLab];
    [bpUnitLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bpAmountLab.mas_right).offset(2);
        make.bottom.mas_equalTo(bbtUnitLab);
        make.height.mas_equalTo(bpUnitLab.font.pointSize);
        make.width.mas_equalTo(stringGetWidth(bpUnitLab.text, 12));
    }];
    
    UILabel *awardDetailLab = [UILabel by_init];
    awardDetailLab.font = [UIFont boldSystemFontOfSize:13];
    awardDetailLab.textColor = kColorRGBValue(0x333333);
    awardDetailLab.text = @"查看昨日奖励详情";
    [self.contentView addSubview:awardDetailLab];
    CGSize awardSize = [awardDetailLab.text getStringSizeWithFont:awardDetailLab.font maxWidth:200];
    [awardDetailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bbtUnitLab.mas_bottom).offset(42);
        make.width.mas_equalTo(ceil(awardSize.width));
        make.height.mas_equalTo(ceil(awardSize.height));
        make.centerX.mas_equalTo(-10);
    }];
    
    UIImageView *arrowImgView = [[UIImageView alloc] init];
    [arrowImgView by_setImageName:@"common_daysignin_arrow"];
    [self.contentView addSubview:arrowImgView];
    [arrowImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(awardDetailLab);
        make.left.mas_equalTo(awardDetailLab.mas_right).offset(8);
        make.width.height.mas_equalTo(15);
    }];
    
    UIButton *awardBtn = [UIButton by_buttonWithCustomType];
    [awardBtn addTarget:self action:@selector(awardBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:awardBtn];
    [awardBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(awardDetailLab);
        make.right.mas_equalTo(arrowImgView);
        make.top.bottom.mas_equalTo(awardDetailLab);
    }];
    
    UIButton *inviteFriendBtn = [UIButton by_buttonWithCustomType];
    [inviteFriendBtn setBy_attributedTitle:@{@"title":@"邀请好友",
                                             NSFontAttributeName:[UIFont boldSystemFontOfSize:14],
                                             NSForegroundColorAttributeName:[UIColor whiteColor]
    } forState:UIControlStateNormal];
    [inviteFriendBtn setBackgroundColor:kColorRGBValue(0xea6438)];
    inviteFriendBtn.layer.cornerRadius = 18;
    [inviteFriendBtn addTarget:self action:@selector(inviteFriendBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:inviteFriendBtn];
    [inviteFriendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(36);
        make.bottom.mas_equalTo(-25);
    }];
    
    self.dismissBtn = [UIButton by_buttonWithCustomType];
    [self.dismissBtn setBy_imageName:@"icon_every_cancel" forState:UIControlStateNormal];
    [self.dismissBtn addTarget:self action:@selector(hiddenAnimation) forControlEvents:UIControlEventTouchUpInside];
    self.dismissBtn.alpha = 0.0;
    [self addSubview:self.dismissBtn];
    [self.dismissBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(35);
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(self.contentView.mas_bottom).offset(30);
    }];
}

@end
