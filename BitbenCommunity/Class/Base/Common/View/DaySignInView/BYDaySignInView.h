//
//  BYDaySignInView.h
//  BitbenCommunity
//
//  Created by Belief on 2020/2/5.
//  Copyright © 2020 币源网络. All rights reserved.
//  每日签到

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYDaySignInView : UIView

+ (void)showAnimation:(NSString *)bbt bp:(NSString *)bp;

@end

NS_ASSUME_NONNULL_END
