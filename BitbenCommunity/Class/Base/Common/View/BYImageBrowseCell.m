//
//  BYImageBrowseCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYImageBrowseCell.h"

#define kMaxZoom 4.0
@interface BYImageBrowseCell ()<UIScrollViewDelegate>

/** uiscrollview */
@property (nonatomic ,strong) UIScrollView *scrollView;
/** imageView */
@property (nonatomic ,strong) PDImageView *imageView;

@end

@implementation BYImageBrowseCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object placeholder:(UIImage *)placeholder indexPath:(NSIndexPath *)indexPath{
    if ([object isKindOfClass:[UIImage class]]) {
        self.imageView.image = (UIImage *)object;
    }else if ([object isKindOfClass:[NSString class]]){
//        [self.imageView sd_setImageWithURL:[NSURL URLWithString:(NSString *)object] placeholderImage:placeholder];;
        [self.imageView uploadMainImageWithURL:(NSString *)object placeholder:placeholder imgType:PDImgTypeOriginal callback:nil];
    }
}

- (void)verifyImageFrame:(UIImage *)image{
    if (!image) return;
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    if (width >= height) {
        CGFloat imageH = kCommonScreenWidth*height/width;
        CGFloat frameH = imageH < kCommonScreenHeight ? kCommonScreenHeight : imageH;
        self.imageView.frame = CGRectMake(0, 0, kCommonScreenWidth, frameH);
        self.scrollView.contentSize = CGSizeMake(kCommonScreenWidth, frameH);
    }else if (width < height) {
        CGFloat imageH = kCommonScreenWidth*height/width;
        CGFloat frameH = imageH < kCommonScreenHeight ? kCommonScreenHeight : imageH;
        self.imageView.frame = CGRectMake(0, 0, kCommonScreenWidth, frameH);
        self.scrollView.contentSize = CGSizeMake(kCommonScreenWidth, frameH);
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.imageView;
}

- (CGPoint)centerOfScrollViewContent:(UIScrollView *)scrollView{
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    CGPoint actualCenter = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                       scrollView.contentSize.height * 0.5 + offsetY);
    return actualCenter;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
    _imageView.center = [self centerOfScrollViewContent:scrollView];
}

#pragma mark - action
- (void)imageOneTapGestureAction{
    if (self.didSelectImageView) {
        self.didSelectImageView();
    }
}

- (void)imageDoubleTapGestureAction:(UIGestureRecognizer *)sender{
    if(!_imageView.image) return;
    
    if(_scrollView.zoomScale <= 1){
        CGFloat x = [sender locationInView:self].x + _scrollView.contentOffset.x;
        
        CGFloat y = [sender locationInView:self].y + _scrollView.contentOffset.y;
        [_scrollView zoomToRect:(CGRect){{x,y},CGSizeZero} animated:true];
    }else{
        // set scrollView zoom to original
        [_scrollView setZoomScale:1.f animated:true];
    }
}

#pragma mark - configUI

- (void)setContentView{
    [self.contentView addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    [self.scrollView addSubview:self.imageView];
}

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.maximumZoomScale = kMaxZoom;
        _scrollView.minimumZoomScale = 0.5;
        _scrollView.bouncesZoom = YES;
        _scrollView.delegate = self;
    }
    return _scrollView;
}

- (PDImageView *)imageView{
    if (!_imageView) {
        _imageView = [[PDImageView alloc] init];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        _imageView.clipsToBounds = YES;
        _imageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *oneTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageOneTapGestureAction)];
        [_imageView addGestureRecognizer:oneTap];
        
        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageDoubleTapGestureAction:)];
        doubleTap.numberOfTapsRequired = 2;
        [_imageView addGestureRecognizer:doubleTap];
        [oneTap requireGestureRecognizerToFail:doubleTap];

    }
    return _imageView;
}

@end
