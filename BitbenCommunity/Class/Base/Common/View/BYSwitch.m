//
//  BYSwitch.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYSwitch.h"

@interface BYSwitch ()

/** 状态 */
@property (nonatomic ,assign) BOOL isOn;
/** onTintColor */
@property (nonatomic ,strong) UIColor *onTintColor;
/** offTintColor */
@property (nonatomic ,strong) UIColor *offTintColor;
/** onCornerColor */
@property (nonatomic ,strong) UIColor *onCornerColor;
/** offCornerColor */
@property (nonatomic ,strong) UIColor *offCornerColor;
/** onThumbImage */
@property (nonatomic ,strong) UIImage *onThumbImage;
/** offThumbImage */
@property (nonatomic ,strong) UIImage *offThumbImage;

/** cornerView */
@property (nonatomic ,strong) UIControl *cornerView;
/** switcBtn */
@property (nonatomic ,strong) UIImageView *switchBtn;


@end

@implementation BYSwitch


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self configDefultValue];
        [self configUI];
    }
    return self;
}

- (BOOL)isOn{
    return self.selected;
}

- (void)switchAction{
    self.selected = !self.selected;
    [self setOn:self.selected animated:YES];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void)setOn:(BOOL)on animated:(BOOL)animated{
    self.selected = on;
    NSTimeInterval time = animated ? 0.15 : 0.0;
    [UIView animateWithDuration:time animations:^{
        [self.cornerView setBackgroundColor:on ? self.onTintColor : self.offTintColor];
        self.cornerView.layer.borderColor = on ? self.onCornerColor.CGColor : self.offCornerColor.CGColor;
        CGRect rect = self.switchBtn.frame;
        rect.origin.x = on ? CGRectGetWidth(self.frame) - CGRectGetWidth(self.switchBtn.frame) : 0;
        self.switchBtn.frame = rect;
        [self.switchBtn setImage:on ? self.onThumbImage : self.offThumbImage];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:time animations:^{
            CGRect rect = self.switchBtn.frame;
            rect.origin.x = on ? CGRectGetWidth(self.frame) - CGRectGetWidth(self.switchBtn.frame) - self.cornerWidth: self.cornerWidth;
            self.switchBtn.frame = rect;
        }];
    }];
}

/** 设置按钮的image */
- (void)setThumbImage:(UIImage *)image forOn:(BOOL)on{
    if (on) {
        self.onThumbImage    = image;
    }else{
        self.offThumbImage   = image;
    }
    [self.switchBtn setImage:on ? self.onThumbImage : self.offThumbImage];
}
/** 设置switch 背景色 */
- (void)setTintColor:(UIColor *)color forOn:(BOOL)on{
    if (on) {
        self.onTintColor    = color;
    }else{
        self.offTintColor   = color;
    }
    [self.cornerView setBackgroundColor:on ? self.onTintColor : self.offTintColor];
}
/** 设置swith外描边色 */
- (void)setCornerColor:(UIColor *)color forOn:(BOOL)on{
    if (on) {
        self.onCornerColor  = color;
    }else{
        self.offCornerColor = color;
    }
    self.cornerView.layer.borderColor = on ? self.onCornerColor.CGColor : self.offCornerColor.CGColor;
}

- (void)configDefultValue{
    self.onTintColor    = kColorRGBValue(0x6384ff);
    self.offTintColor   = kColorRGBValue(0xf2f4f5);
    self.onCornerColor  = kColorRGBValue(0x6384ff);
    self.offCornerColor = kColorRGBValue(0xdddddd);
    self.onThumbImage   = [UIImage imageWithColor:[UIColor whiteColor]];
    self.offThumbImage  = [UIImage imageWithColor:[UIColor whiteColor]];
    self.cornerWidth    = 2.0f;
    self.selected       = NO;
}

- (void)configUI{
    self.cornerView = [[UIControl alloc] init];
    self.cornerView.frame = self.bounds;
    self.cornerView.layer.cornerRadius = self.frame.size.height/2;
    self.cornerView.layer.borderWidth  = self.cornerWidth;
    self.cornerView.layer.borderColor  = self.offCornerColor.CGColor;
    self.cornerView.backgroundColor = self.offTintColor;
    [self.cornerView addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.cornerView];
    
    self.switchBtn = [[UIImageView alloc] init];
    CGFloat width = self.frame.size.height - 2*self.cornerWidth;
    self.switchBtn.frame = CGRectMake(self.cornerWidth,
                                      self.cornerWidth,
                                      width,
                                      width);
    self.switchBtn.layer.cornerRadius = width/2;
    self.switchBtn.clipsToBounds = YES;
    [self.switchBtn setImage:self.offThumbImage];
    [self addSubview:self.switchBtn];
}

@end
