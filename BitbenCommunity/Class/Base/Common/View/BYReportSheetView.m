//
//  BYReportSheetView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/19.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYReportSheetView.h"

static NSInteger const baseTag = 0x683;
@interface BYReportSheetView ()

/**  superView */
@property (nonatomic ,strong) UIView *fathuerView;
/** contentView */
@property (nonatomic ,strong) UIView *contentView;
/** maskView */
@property (nonatomic ,strong) UIView *maskView;
/** titles */
@property (nonatomic ,strong) NSArray *titles;
/** height */
@property (nonatomic ,assign) CGFloat contentH;


@end

@implementation BYReportSheetView

+ (instancetype)initReportSheetViewShowInView:(UIView *)view{
    BYReportSheetView *sheetView = [[BYReportSheetView alloc] init];
    sheetView.fathuerView = view ? view : kCommonWindow;
    sheetView.frame = sheetView.fathuerView.bounds;
    [sheetView configView];
    return sheetView;
}

- (void)showAnimation{
    [self.fathuerView addSubview:self];
    self.maskView.layer.opacity = 1.0f;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.2 animations:^{
            [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
            }];
            [self layoutIfNeeded];
        }];
    });
}

- (void)hiddenAnimation{
    self.maskView.layer.opacity = 0.0f;
    [UIView animateWithDuration:0.2 animations:^{
        @weakify(self);
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.bottom.mas_equalTo(self.contentH);
        }];
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - baseTag;
    if (index == 0) return;
    [self hiddenAnimation];
    if ([self.user_id isEqualToString:[AccountModel sharedAccountModel].account_id]) {
        showToastView(@"不能举报自己！", self.fathuerView);
        return;
    }
    [[BYLiveHomeRequest alloc] loadRequestReportThemeId:self.theme_id themeType:self.theme_type reportType:self.titles[index] successBlock:^(id object) {
        showToastView(@"举报成功", kCommonWindow);
    } faileBlock:^(NSError *error) {
        showToastView(@"举报失败", kCommonWindow);
    }];
}

- (void)configView{
    self.maskView = [UIView by_init];
    self.maskView.layer.opacity = 0.0f;
    [self addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenAnimation)];
    [self.maskView addGestureRecognizer:tapGestureRecognizer];
    
    self.titles = [BYReportSheetView actionItemsArr];
    self.contentView = [UIView by_init];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    self.contentView.layer.shadowColor = kColorRGBValue(0x4e4e4e).CGColor;
    self.contentView.layer.shadowOpacity = 0.2;
    self.contentView.layer.shadowRadius = 25;
    [self addSubview:self.contentView];
    CGFloat singleH = 50;
    self.contentH = self.titles.count*singleH;
    @weakify(self);
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.contentH);
        make.height.mas_equalTo(self.contentH);
    }];
    for (int i = 0; i < self.titles.count; i ++) {
        UIView *subView = [self getSingleView:i];
        [self.contentView addSubview:subView];
        [subView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(singleH*i);
            make.left.right.mas_equalTo(0);
            make.height.mas_equalTo(singleH);
        }];
    }
}

- (UIView *)getSingleView:(NSInteger)index{
    UIButton *button = [UIButton by_buttonWithCustomType];
    button.tag = baseTag + index;
    UIColor *color = index == 0 ? kColorRGBValue(0x353535) : kColorRGBValue(0x6f6f6f);
    UIFont *font = index == 0 ? [UIFont systemFontOfSize:15] : [UIFont systemFontOfSize:13];
    NSDictionary *attribut = @{@"title":self.titles[index],NSForegroundColorAttributeName:color,NSFontAttributeName:font};
    [button setBy_attributedTitle:attribut forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7e7)];
    [button addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(-0.5);
    }];
    if (index == 0) {
        UIImageView *arrowImgView = [[UIImageView alloc] init];
        [arrowImgView by_setImageName:@"common_arrow_down"];
        [button addSubview:arrowImgView];
        [arrowImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-15);
            make.centerY.mas_equalTo(0);
            make.width.mas_equalTo(arrowImgView.image.size.width);
            make.height.mas_equalTo(arrowImgView.image.size.height);
        }];
    }
    return button;
}

// ares
+(NSArray *)actionItemsArr{
    return @[@"举报",@"违法违规",@"色情暴力",@"攻击歧视",@"广告营销",@"与话题无关"];
}

@end
