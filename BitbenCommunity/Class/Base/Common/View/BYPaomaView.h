//
//  BYPaomaView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/5/31.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYPaomaView : UIControl

//- (instancetype)initWithFrame:(CGRect)frame withTitle:(NSString *)title;

/** color */
@property (nonatomic ,strong) UIColor *tiltleColor;


- (void)startAnimation:(NSString *)title;

//- (void)start;

- (void)stop;

@end

NS_ASSUME_NONNULL_END
