//
//  BYLikeAnimationView.m
//  BibenCommunity
//
//  Created by 随风 on 2018/11/3.
//  Copyright © 2018 币本. All rights reserved.
//

#import "BYLikeAnimationView.h"

#if defined(__IPHONE_10_0) && (__IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0)
@interface BYLikeAnimationView () <CAAnimationDelegate>
#else
@interface BYLikeAnimationView ()
#endif



@property (nonatomic, strong) NSMutableArray *keepArray;
@property (nonatomic, strong) NSMutableArray *deletArray;
@property (nonatomic, assign) NSInteger zanCount;
@property (nonatomic, weak) UIView *superView;
@property (nonatomic, assign) CGPoint beginPoint;
@end

@implementation BYLikeAnimationView

// 采用tableview的回收机制
- (NSMutableArray *)keepArray
{
    if (!_keepArray) {
        _keepArray = [[NSMutableArray alloc] init];
    }
    return _keepArray;
}

- (NSMutableArray *)deletArray
{
    if (!_deletArray) {
        _deletArray = [[NSMutableArray alloc] init];
    }
    return _deletArray;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.zanCount = 0;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.zanCount = 0;
    }
    return self;
}

- (void)beginUpAniamtion:(UIView *)superView point:(CGPoint)point type:(BY_LIKE_ANIMATION_TYPE)type{
    self.superView = superView;
    self.beginPoint = point;
    _zanCount ++;
    if (_zanCount == INT_MAX) {
        _zanCount = 0;
    }
    CALayer *shipLabyer = nil;
    if (self.deletArray.count > 0) {
        for (CALayer *layer in self.deletArray) {
            if (layer.name == stringFormatInteger(type)) {
                shipLabyer = layer;
                [self.deletArray removeObject:shipLabyer];
                break;
            }
        }
        if (!shipLabyer) {
            shipLabyer = [self newLayer:point type:type];
        }
    } else {
        shipLabyer = [self newLayer:point type:type];
    }
    shipLabyer.opacity = 1.0;
    [superView.layer addSublayer:shipLabyer];
    [self.keepArray addObject:shipLabyer];
    
    [self animationKeyFrameWithLayer:shipLabyer];
}

- (CALayer *)newLayer:(CGPoint)point type:(BY_LIKE_ANIMATION_TYPE)type{
    NSString *imageName = @"";
    switch (type) {
        case BY_LIKE_ANIMATION_TYPE_UP:
            imageName = @"iliveroom_top";
            break;
        case BY_LIKE_ANIMATION_TYPE_PF_UP:
            imageName = @"videolive_like";
            break;
        default:
            imageName = @"iliveroom_trample";
            break;
    }
    CALayer *shipLabyer = [CALayer layer];
    shipLabyer.contents = (__bridge id _Nullable)([UIImage imageNamed:imageName].CGImage);
    shipLabyer.contentsScale = [UIScreen mainScreen].scale;
    shipLabyer.frame = CGRectMake(point.x, point.y, 40, 40);
    shipLabyer.transform = CATransform3DMakeRotation(M_PI_2, 0, 0, 1);
    shipLabyer.name = stringFormatInteger(type);
    return shipLabyer;
}

- (void)animationKeyFrameWithLayer:(CALayer *)layer
{
    NSInteger with = 80;
    NSInteger height = 400;
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(_beginPoint.x, _beginPoint.y)];
    [path addCurveToPoint:CGPointMake(_beginPoint.x - 40 + arc4random() % with, 0) controlPoint1:CGPointMake((arc4random() % with) / 2.0 + (_beginPoint.x - 80), (arc4random() % height) / 2.0) controlPoint2:CGPointMake((_beginPoint.x - 40) + (arc4random() % with) / 2.0, height / 2.0 + (arc4random() % height) / 2.0)];
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    animation.duration = 1.2 + (arc4random() % 9) / 10.0;
    animation.rotationMode = kCAAnimationRotateAuto;
    animation.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    animation.path = path.CGPath;
    animation.fillMode = kCAFillModeForwards;
    
    // 缩放
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnimation.fromValue = [NSNumber numberWithFloat:1.1];
    scaleAnimation.toValue = [NSNumber numberWithFloat:0.7];
    scaleAnimation.duration = 1 + (arc4random() % 10) / 10.0;
    
    CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    alphaAnimation.fromValue = @(1.0);
    alphaAnimation.toValue = @(0);
    alphaAnimation.duration = animation.duration;
    alphaAnimation.fillMode = kCAFillModeForwards;
    
    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    animationGroup.repeatCount = 1;
    animationGroup.removedOnCompletion = NO;
    animationGroup.duration = animation.duration;
    animationGroup.fillMode = kCAFillModeForwards;
    animationGroup.animations = @[animation, scaleAnimation, alphaAnimation];
    animationGroup.delegate = self;
    
    [layer addAnimation:animationGroup forKey:[NSString stringWithFormat:@"animation%zd", _zanCount]];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    CALayer *layer = [self.keepArray firstObject];
    [layer removeAllAnimations];
    [self.deletArray addObject:layer];
    [layer removeFromSuperlayer];
    [self.keepArray removeObject:layer];
}

@end
