//
//  BYImageBrowseController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/26.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYImageBrowseController.h"
#import "BYImageBrowseCell.h"
#import <Photos/Photos.h>

@interface BYImageBrowseController ()<UICollectionViewDelegate,UICollectionViewDataSource>

/** collection */
@property (nonatomic ,strong) UICollectionView *collectionView;
/** 保存 */
@property (nonatomic ,strong) UIButton *saveBtn;
/** 图片组 */
@property (nonatomic ,strong) NSArray *imageData;
/** 加载图片的index */
@property (nonatomic ,assign) NSInteger index;
/** 加载图片的位置 */
@property (nonatomic ,assign) CGRect imageRect;
/** 加载图片的预览图 */
@property (nonatomic ,strong) UIImage *previewImg;
/** begin */
@property (nonatomic ,assign) BOOL isBegin;
/** 底部占位scrollView */
@property (nonatomic ,strong) UIScrollView *scrollView;
/** 页码 */
@property (nonatomic ,strong) UILabel *pageLab;


@end

@implementation BYImageBrowseController

+ (void)addToRootViewController:(UIImage *)PreviewImg
                          index:(NSInteger)index
                           rect:(CGRect)rect
                         images:(NSArray *)images
                         inView:(UIViewController *)view
{
    if (!PreviewImg) return;
    BYImageBrowseController *imageBrowseController = [[BYImageBrowseController alloc] init];
    imageBrowseController.index = index;
    imageBrowseController.imageRect = rect;
    imageBrowseController.imageData = images;
    imageBrowseController.previewImg = PreviewImg;
    imageBrowseController.isBegin = YES;
    imageBrowseController.view.frame = kCommonWindow.bounds;
    [imageBrowseController willMoveToParentViewController:kCommonWindow.rootViewController];
    [kCommonWindow.rootViewController addChildViewController:imageBrowseController];
    [kCommonWindow addSubview:imageBrowseController.view];
    
    [imageBrowseController.view addSubview:imageBrowseController.collectionView];
    [imageBrowseController.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [imageBrowseController.collectionView reloadData];
    
    [imageBrowseController addOtherSubView];
    [imageBrowseController setPageLabIndex:index + 1];
    
    [imageBrowseController.view layoutIfNeeded];
    [imageBrowseController.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:imageBrowseController.index inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self.view setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)setPageLabIndex:(NSInteger)index{
    self.pageLab.hidden = self.imageData.count > 1 ? NO : YES;
    self.pageLab.text = [NSString stringWithFormat:@"%i / %i",(int)index,(int)self.imageData.count];
}

#pragma mark - action
- (void)savaBtnAction{
    BYImageBrowseCell *cell = (BYImageBrowseCell *)self.collectionView.visibleCells[0];
    if (!cell.imageView.image) return;
    UIImage *image = cell.imageView.image;
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            [PHAssetChangeRequest creationRequestForAssetFromImage:image];
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                showToastView(@"保存失败", kCommonWindow);
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                showToastView(@"保存成功", kCommonWindow);
            });
        }
    }];
}

#pragma mark - UICollectionViewDelegate/DataScoure
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.imageData.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BYImageBrowseCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    UIImage *placeholder = self.index == indexPath.row ? self.previewImg : nil;
    [cell setContentWithObject:self.imageData[indexPath.row] placeholder:placeholder indexPath:indexPath];
    @weakify(self);
    @weakify(cell);
    cell.didSelectImageView = ^{
        @strongify(self);
        @strongify(cell);
        ((AbstractViewController *)CURRENT_VC).statusBarHidden = NO;
        [CURRENT_VC setNeedsStatusBarAppearanceUpdate];
        [UIView animateWithDuration:0.2 animations:^{
            [cell.scrollView setZoomScale:1.0];
            cell.imageView.transform    = CGAffineTransformIdentity;
            self.view.backgroundColor   = [UIColor colorWithWhite:0 alpha:0.3];
            self.saveBtn.alpha          = 0.0f;
            self.pageLab.alpha          = 0.0f;
            cell.imageView.alpha        = 0.0f;
            cell.imageView.frame        = self.imageRect;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 animations:^{
                self.view.backgroundColor   = [UIColor colorWithWhite:0 alpha:0.0];
            }completion:^(BOOL finished) {
                [self.view removeFromSuperview];
                [self removeFromParentViewController];
            }];
        }];
    };
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    BYImageBrowseCell *currentCell = (BYImageBrowseCell *)[collectionView cellForItemAtIndexPath:indexPath];
   
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(8_0){
    BYImageBrowseCell *currentCell = (BYImageBrowseCell *)cell;
    if (self.isBegin){
        if (self.index == indexPath.row){
            self.isBegin = NO;
//            currentCell.imageView.image = self.previewImg;
            currentCell.imageView.frame = self.imageRect;

            [UIView animateWithDuration:0.15 animations:^{
                self.view.backgroundColor    = [UIColor blackColor];
                self.saveBtn.alpha           = 1.0f;
                self.pageLab.alpha           = 1.0;
                [currentCell verifyImageFrame:currentCell.imageView.image];
            } completion:^(BOOL finished) {
                ((AbstractViewController *)CURRENT_VC).statusBarHidden = YES;
                [CURRENT_VC setNeedsStatusBarAppearanceUpdate];
            }];
        }
    }
    else{
        [currentCell verifyImageFrame:currentCell.imageView.image];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    if (!self.isBegin) {
        BYImageBrowseCell *currentCell  = (BYImageBrowseCell *)cell;
        [currentCell.scrollView setZoomScale:1.0];
        currentCell.imageView.transform = CGAffineTransformIdentity;
        [currentCell verifyImageFrame:currentCell.imageView.image];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger index = scrollView.contentOffset.x/kCommonScreenWidth;
    [self setPageLabIndex:index + 1];
}

#pragma mark - configUI

- (void)addOtherSubView{
    [self.view addSubview:self.saveBtn];
    [self.saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.width.height.mas_equalTo(40);
        make.bottom.mas_equalTo(-kSafe_Mas_Bottom(15));
    }];
    
    [self.view addSubview:self.pageLab];
    [self.pageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(100);
        make.centerY.mas_equalTo(self.saveBtn);
        make.height.mas_equalTo(self.pageLab.font.pointSize);
        make.centerX.mas_equalTo(0);
    }];
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout= [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(kCommonScreenWidth, kCommonScreenHeight);
        layout.headerReferenceSize = CGSizeZero;
        layout.footerReferenceSize = CGSizeZero;
        layout.minimumLineSpacing = 0.0;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        [_collectionView registerClass:[BYImageBrowseCell class] forCellWithReuseIdentifier:@"cell"];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        _collectionView.pagingEnabled = YES;
//        _collectionView.scrollEnabled = NO;
    }
    return _collectionView;
}

- (UIButton *)saveBtn{
    if (!_saveBtn) {
        _saveBtn = [UIButton by_buttonWithCustomType];
        [_saveBtn setBy_imageName:@"common_imagebrowse_save" forState:UIControlStateNormal];
        [_saveBtn addTarget:self action:@selector(savaBtnAction) forControlEvents:UIControlEventTouchUpInside];
        _saveBtn.alpha = 0.0f;
    }
    return _saveBtn;
}

- (UILabel *)pageLab{
    if (!_pageLab) {
        _pageLab = [UILabel by_init];
        [_pageLab setBy_font:14];
        _pageLab.textAlignment  = NSTextAlignmentCenter;
        _pageLab.textColor      = [UIColor whiteColor];
        _pageLab.alpha          = 0.0;
    }
    return _pageLab;
}

@end
