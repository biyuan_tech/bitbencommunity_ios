//
//  BYImageBrowseController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/26.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYImageBrowseController : UIViewController

+ (void)addToRootViewController:(UIImage *)PreviewImg
                          index:(NSInteger)index
                           rect:(CGRect)rect
                         images:(NSArray *)images
                         inView:(UIViewController *)view;

@end

NS_ASSUME_NONNULL_END
