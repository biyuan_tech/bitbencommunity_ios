//
//  BYToastView.m
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYToastView.h"

#define kActivityViewTag 0x1002
#define kMaskViewTag 0x1003
#define kMaxDuration 60 // toastView显示最长时间

@interface UIWindow (toastView)

@property (nonatomic ,readonly) NSMutableArray *toastViewStack;

@end

// 固定宽度
static const CGFloat fixedWidth = 110;

// 左右侧间距
static const CGFloat spaceWidth = 16;

// 上下侧间距
//static const CGFloat spaceHeight = 35;
static const CGFloat spaceHeight = 15;

// 菊花宽高
static const CGFloat activityWidth = 46;

@interface BYToastView ()

// 文案title
@property (nonatomic ,strong) UILabel *titleLabel;
// toastView消失回调
@property (nonatomic ,copy) void(^completeHandle)(void);
// 消失定时器
@property (nonatomic ,strong) NSTimer *dismmTimer;


@end

@implementation BYToastView

- (void)dealloc
{

}

+ (BYToastView *)toastViewPresentLoading{
    return [self toastViewPresentInWindowTitle:nil duration:0 toastViewType:kToastViewTypeLoading complete:nil];
}

+ (BYToastView *)toastViewPresentLoadingInView:(UIView *)superView{
    return [self tostViewPresentInView:superView title:nil duration:0 toastViewType:kToastViewTypeLoading complete:nil];
}

+ (BYToastView *)toastViewPresentInWindowTitle:(NSString *)title
                                       duration:(NSTimeInterval)duration
                                  toastViewType:(kToastViewType)toastViewType
                                       complete:(void(^)(void))complete{
    
   
    BYToastView *toastView = [self tostViewPresentInView:kCommonWindow title:title duration:duration toastViewType:toastViewType complete:complete];
    if (toastView) {
        UIView *maskView = [[UIView alloc] initWithFrame:kCommonWindow.bounds];
        [kCommonWindow addSubview:maskView];
        [kCommonWindow insertSubview:maskView belowSubview:toastView];
        maskView.tag = kMaskViewTag
        ;
    }
    return toastView;
    
}

+ (BYToastView *)tostViewPresentInView:(UIView *)view
                                  title:(NSString *)title
                               duration:(NSTimeInterval)duration
                          toastViewType:(kToastViewType)toastViewType
                               complete:(void(^)(void))complete
{
    return [self tostViewPresentInView:view title:title titleColor:[UIColor whiteColor] duration:duration toastViewType:toastViewType complete:complete];
}

+ (BYToastView *)tostViewPresentInView:(UIView *)view
                                  title:(NSString *)title
                               duration:(NSTimeInterval)duration
                               complete:(void(^)(void))complete
{
    return [self tostViewPresentInView:view title:title duration:duration toastViewType:kToastViewTypeNormal complete:complete];
}

+ (BYToastView *)tostViewPresentInView:(UIView *)view
                                  title:(NSString *)title
                             titleColor:(UIColor *)color
                               duration:(NSTimeInterval)duration
                          toastViewType:(kToastViewType)toastViewType
                               complete:(void(^)(void))complete
{
    if (!title.length && toastViewType == kToastViewTypeNormal) {
        return nil;
    }
    BYToastView *toastView = [[BYToastView alloc] initWithTitle:title toastViewType:toastViewType];
    toastView.titleLabel.textColor = color;
    toastView.completeHandle = complete;
    [view addSubview:toastView];
    duration = duration > 0 ? duration : kMaxDuration;
    NSTimer *dismmTimer = [NSTimer scheduledTimerWithTimeInterval:duration target:toastView selector:@selector(dissmissToastView) userInfo:nil repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:dismmTimer forMode:NSRunLoopCommonModes];
    toastView.dismmTimer = dismmTimer;
    [toastView addToToastStack];
    toastView.center = CGPointMake(toastView.center.x, view.frame.size.height/2);// + detalPoint.y
    return toastView;
}

// 将toastView添加入数组堆栈中管理
- (void)addToToastStack{
    [self.window.toastViewStack addObject:self];
    [self popOtherToast];
    
}

// 避免多个toastView叠加
- (void)popOtherToast{
    
    for (BYToastView *toastView in self.window.toastViewStack) {
        if (toastView == self) {
            break;
        }
        toastView.alpha = 0;
    }
}

// 初始化toastView
- (id)initWithTitle:(NSString *)title toastViewType:(kToastViewType)toastViewType{
    self = [super init];
    if (self) {
        self.titleLabel = [[UILabel alloc] init];
        _titleLabel.text = title;
        _titleLabel.font = [UIFont systemFontOfSize:15];
        _titleLabel.numberOfLines = 0;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_titleLabel];
        
        CGSize titleSize = [title getStringSizeWithFont:_titleLabel.font maxWidth:MAXFLOAT];
        
        CGFloat toastViewWidth = 0;
        CGFloat toastViewHeight = 0;
        if (toastViewType == kToastViewTypeNormal)
        {
            
            _titleLabel.frame = CGRectMake(spaceWidth, spaceHeight, titleSize.width, titleSize.height);
            toastViewWidth = titleSize.width + 2*spaceWidth;
            toastViewHeight = titleSize.height + 2*spaceHeight;
        }
        else if (toastViewType == kToastViewTypeHomeSwith){
            _titleLabel.frame = CGRectMake(spaceWidth, spaceHeight, titleSize.width, titleSize.height);
            UIView *iconView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            CGFloat iconViewY = title.length ? 22 : (fixedWidth - activityWidth)/2;
            iconView.frame = CGRectMake(CGRectGetMaxX(_titleLabel.frame) + 10, iconViewY, 23, 23);
            iconView.center = CGPointMake(iconView.center.x, _titleLabel.center.y);
            [(UIActivityIndicatorView *)iconView startAnimating];
            [self addSubview:iconView];
            iconView.tag = kActivityViewTag;
            toastViewWidth = titleSize.width + 23 + 2*spaceWidth + 10;
            toastViewHeight = titleSize.height + 2*spaceHeight;
        }
        else
        {
            
            toastViewWidth = fixedWidth;
            toastViewHeight = fixedWidth;
            toastViewWidth = titleSize.width > toastViewWidth ? titleSize.width + 20 : toastViewWidth;
            UIView *iconView;
            if (toastViewType == kToastViewTypeLoading) {
                iconView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                CGFloat iconViewY = title.length ? 22 : (fixedWidth - activityWidth)/2;
                iconView.frame = CGRectMake((fixedWidth - activityWidth)/2, iconViewY, activityWidth, activityWidth);
                [(UIActivityIndicatorView *)iconView startAnimating];
                [self addSubview:iconView];
                iconView.tag = kActivityViewTag;
            }
            else
            {
                NSString *imageName = nil;
                switch (toastViewType) {
                    case kToastViewTypeSuccess:
                        imageName = @"private_pic_chose_right";
                        break;
                    case kToastViewTypeLike:
                        imageName = @"public_red_heart";
                        break;
                    default:
                        break;
                }
                UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
                imageView.frame = CGRectMake((toastViewWidth - imageView.image.size.width)/2, 22, imageView.image.size.width, imageView.image.size.height);
                iconView = imageView;
                [self addSubview:iconView];
            }
            
            _titleLabel.frame = CGRectMake((toastViewWidth - titleSize.width)/2, CGRectGetMaxY(iconView.frame) + 17, titleSize.width, titleSize.height);
            
        }
        self.frame = CGRectMake((kCommonScreenWidth - toastViewWidth)/2, 0, toastViewWidth, toastViewHeight);
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.8];
        self.layer.cornerRadius = 6;
        self.layer.masksToBounds = YES;
    }
    return self;
}

- (void)dissmissToastView{
    // toastView消失后回调
    if (_completeHandle)
    {
        _completeHandle();
    }
    [self destoryToastView:self];
}

- (void)destoryToastView:(BYToastView *)toastView{
    toastView.titleLabel = nil;
    [toastView.dismmTimer invalidate];
    toastView.dismmTimer = nil;
    // 移除window上的透明层
    if ([kCommonWindow viewWithTag:kMaskViewTag]) {
        UIView *maskView = [kCommonWindow viewWithTag:kMaskViewTag];
        [maskView removeFromSuperview];
        maskView = nil;
    }
    UIActivityIndicatorView *activityView = [toastView viewWithTag:kActivityViewTag];
    [activityView stopAnimating];
    [toastView removeFromSuperview];
    
    [self.window.toastViewStack removeObject:self];
}

@end

static const char toastStack;
@implementation UIWindow (toastView)

- (NSMutableArray *)toastViewStack
{
    NSMutableArray *ret = objc_getAssociatedObject(self, &toastStack);
    if (!ret) {
        ret = [NSMutableArray array];
        objc_setAssociatedObject(self, &toastStack, ret, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return ret;
}

@end
