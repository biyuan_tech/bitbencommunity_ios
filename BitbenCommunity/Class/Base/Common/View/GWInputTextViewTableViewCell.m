//
//  GWInputTextViewTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/3/18.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "GWInputTextViewTableViewCell.h"


static char textViewTextDidChangedBlockKey;
static char textViewShouldBeginEditingKey;
@interface GWInputTextViewTableViewCell()<UITextViewDelegate>
@property (nonatomic,strong)UILabel *titleFixedLabel;
@property (nonatomic,strong)UILabel *placeholderLabel;
@property (nonatomic,strong)UILabel *limitLabel;


@end

@implementation GWInputTextViewTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.limitCount = 300;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleFixedLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.titleFixedLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.titleFixedLabel];
    
    // 输入框
    self.inputTextView = [[UITextView alloc]init];
    self.inputTextView.delegate = self;
    self.inputTextView.textColor = [UIColor colorWithCustomerName:@"黑"];
    self.inputTextView.font = [UIFont fontWithCustomerSizeName:@"13"];
    self.inputTextView.backgroundColor = [UIColor whiteColor];
    self.inputTextView.scrollEnabled = YES;
    self.inputTextView.layer.cornerRadius = LCFloat(5);
    self.inputTextView.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
    self.inputTextView.layer.borderWidth = .5f;
    [self addSubview:self.inputTextView];
    
    // placeholder
    self.placeholderLabel = [GWViewTool createLabelFont:@"13" textColor:@"黑"];
    self.placeholderLabel.backgroundColor = [UIColor clearColor];
    self.placeholderLabel.numberOfLines = 0;
    [self.inputTextView addSubview:self.placeholderLabel];
    
    // limit
    self.limitLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"灰"];
    self.limitLabel.textColor = [UIColor colorWithRed:200/256.0 green:200/256.0 blue:200/256.0 alpha:1];
    [self addSubview:self.limitLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferPlaceholder:(NSString *)transferPlaceholder{
    _transferPlaceholder = transferPlaceholder;
    self.placeholderLabel.text = transferPlaceholder;
    [self adjustToUploadInfo];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleFixedLabel.text = transferTitle;
}

-(void)setTransferInputText:(NSString *)transferInputText{
    _transferInputText = transferInputText;
}

-(void)adjustToUploadInfo{
    // 1.title
    CGSize titleSize = [self.titleFixedLabel.text sizeWithCalcFont:self.titleFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleFixedLabel.font])];
    self.titleFixedLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), titleSize.width, [NSString contentofHeightWithFont:self.titleFixedLabel.font]);
    
    // 2. inputView
    self.inputTextView.frame = CGRectMake(LCFloat(15), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(15),[GWInputTextViewTableViewCell calculationCellHeight] - 2 * LCFloat(11));

    // placeholder
    CGSize placeholderSize = [self.placeholderLabel.text sizeWithCalcFont:self.placeholderLabel.font constrainedToSize:CGSizeMake(self.inputTextView.size_width, CGFLOAT_MAX)];
    self.placeholderLabel.frame = CGRectMake(LCFloat(5), LCFloat(6), self.inputTextView.size_width - 2 * LCFloat(5), placeholderSize.height);
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"])// 回车可以改变
    {
        return YES;
    }
    NSString *toBeString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if(self.inputTextView == textView)
    {
        if(toBeString.length > self.limitCount)
        {
            textView.text = [toBeString substringToIndex:self.limitCount];
            return NO;
        }
    }
    return YES;
}

#pragma mark 文字修改代理
-(void)textViewDidChange:(UITextView *)textView{
    if (self.inputTextView.text.length == 0){
        self.placeholderLabel.text = self.transferPlaceholder;
    } else {
        self.placeholderLabel.text = @"";
    }
    self.limitLabel.text = [NSString stringWithFormat:@"%li/%li",(long)[Tool calculateCharacterLengthForAres:self.inputTextView.text],(long)self.limitCount];
    void(^block)(NSString *info) = objc_getAssociatedObject(self, &textViewTextDidChangedBlockKey);
    if (block){
        block(self.inputTextView.text);
    }
}


-(void)textViewTextDidChangedBlock:(void(^)(NSString *info))block{
    objc_setAssociatedObject(self, &textViewTextDidChangedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    void(^block)() = objc_getAssociatedObject(self, &textViewShouldBeginEditingKey);
    if (block){
        block();
    }
    return YES;
}

-(void)textViewBeginChangeblock:(void(^)())block{
    objc_setAssociatedObject(self, &textViewShouldBeginEditingKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(90);
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end
