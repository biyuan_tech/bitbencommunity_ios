//
//  BYLikeAnimationView.h
//  BibenCommunity
//
//  Created by 随风 on 2018/11/3.
//  Copyright © 2018 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, BY_LIKE_ANIMATION_TYPE) {
    BY_LIKE_ANIMATION_TYPE_UP = 0, //顶
    BY_LIKE_ANIMATION_TYPE_STAMP, // 踩
    BY_LIKE_ANIMATION_TYPE_PF_UP, // 推流直播点赞
};

NS_ASSUME_NONNULL_BEGIN

@interface BYLikeAnimationView : UIView


- (void)beginUpAniamtion:(UIView *)superView point:(CGPoint)point type:(BY_LIKE_ANIMATION_TYPE)type;
@end

NS_ASSUME_NONNULL_END
