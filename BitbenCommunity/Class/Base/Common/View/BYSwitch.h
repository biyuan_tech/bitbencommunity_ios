//
//  BYSwitch.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/10/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYSwitch : UIControl

/** 外描边宽度 */
@property (nonatomic ,assign) CGFloat cornerWidth;
/** 获取当前状态 */
@property(nonatomic,getter=isOn) BOOL on;

- (instancetype)initWithFrame:(CGRect)frame;

- (void)setOn:(BOOL)on animated:(BOOL)animated;

/** 设置按钮的image */
- (void)setThumbImage:(UIImage *)image forOn:(BOOL)on;
/** 设置switch 背景色 */
- (void)setTintColor:(UIColor *)color forOn:(BOOL)on;
/** 设置swith外描边色 */
- (void)setCornerColor:(UIColor *)color forOn:(BOOL)on;


@end

NS_ASSUME_NONNULL_END
