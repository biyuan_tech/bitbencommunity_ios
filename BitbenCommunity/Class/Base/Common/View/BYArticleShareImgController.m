
//
//  BYArticleShareImgController.m
//  BitbenCommunity
//
//  Created by Belief on 2020/2/5.
//  Copyright © 2020 币源网络. All rights reserved.
//

#import "BYArticleShareImgController.h"
#import <Photos/Photos.h>
#import "PPSnapshotHandler.h"

@interface BYArticleShareImgController ()<
PPSnapshotHandlerDelegate,
UIGestureRecognizerDelegate,
WKNavigationDelegate,
WKScriptMessageHandler>

/** webView*/
@property (nonatomic ,strong) WKWebView *webView;
/** 是否加载完*/
@property (nonatomic ,assign) BOOL isLoad;
/** web高度*/
@property (nonatomic ,assign) CGFloat webHeight;
/** toastView*/
@property (nonatomic ,strong) BYToastView *toastView;
@end

@implementation BYArticleShareImgController

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configUI];
        self.view.frame = kCommonWindow.bounds;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)dismissAnimation{
    [self.view removeFromSuperview];
}

- (void)loadWebContent:(NSString *)content{
    if (!content.length) return;
    NSString *documentDir = [[NSBundle mainBundle] pathForResource:@"layui.css" ofType:nil];//
           NSString *resultStr = [NSString stringWithContentsOfFile:documentDir encoding:NSUTF8StringEncoding error:nil];

           NSString *smartUrl = @"";
           smartUrl = @"<meta content=\"width=device-width, initial-scale=1.0, maximum-scale=3.0, user-scalable=0;\" name=\"viewport\" /> ";
           smartUrl = [smartUrl stringByAppendingString:resultStr];
           smartUrl = [smartUrl stringByAppendingString:@"<div class=\"ql-container ql-snow\"><div class=\"ql-editor\">"];
           smartUrl = [smartUrl stringByAppendingString:content];
           smartUrl = [smartUrl stringByAppendingString:@"<div id=\"testDiv\" style = \"height:0px; width:1px\">"];
           smartUrl = [smartUrl stringByAppendingString:@"</div>"];
           smartUrl = [smartUrl stringByAppendingString:@"</div>"];
           smartUrl = [smartUrl stringByAppendingString:@"</div>"];

           [_webView loadHTMLString:smartUrl baseURL:[NSURL fileURLWithPath: [[NSBundle mainBundle]  bundlePath]]];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self drawShareImageView];
//    });
}

- (void)drawShareImageView{
//    PPSnapshotHandler.defaultHandler.delegate = self;
//    [PPSnapshotHandler.defaultHandler snapshotForView:self.webView];
    CGRect rect = CGRectMake(0, 0, kCommonScreenWidth, self.webHeight);
    UIGraphicsBeginImageContextWithOptions(rect.size, YES, [UIScreen mainScreen].scale);
//    for (UIView *subview in self.webView.subviews) {
        [self.webView drawViewHierarchyInRect:self.webView.bounds afterScreenUpdates:YES];
//    }
    UIImage *fullImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
           [PHAssetChangeRequest creationRequestForAssetFromImage:fullImage];
       } completionHandler:^(BOOL success, NSError * _Nullable error) {
              if (error) {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      showToastView(@"保存失败", kCommonWindow);
                  });
              } else {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      showToastView(@"保存成功", kCommonWindow);
                  });
              }
          }];
}

- (void)configUI{
   
    // Do any additional setup after loading the view.
       WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];

       //自定义脚本等
       WKUserContentController *controller = [[WKUserContentController alloc] init];
       //添加js全局变量
       WKUserScript *script = [[WKUserScript alloc] initWithSource:@"var interesting = 123;" injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
       //页面加载完成立刻回调，获取页面上的所有Cookie
       WKUserScript *cookieScript = [[WKUserScript alloc] initWithSource:@"                window.webkit.messageHandlers.currentCookies.postMessage(document.cookie);" injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
       //alert Cookie
       //    WKUserScript *alertCookieScript = [[WKUserScript alloc] initWithSource:@"alert(document.cookie);" injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
       //添加自定义的cookie
       WKUserScript *newCookieScript = [[WKUserScript alloc] initWithSource:@"                document.cookie = 'DarkAngelCookie=DarkAngel;'" injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];

       //添加脚本
       [controller addUserScript:script];
       [controller addUserScript:cookieScript];
       //    [controller addUserScript:alertCookieScript];
       [controller addUserScript:newCookieScript];
       //注册回调
       [controller addScriptMessageHandler:self name:@"share"];
       [controller addScriptMessageHandler:self name:@"currentCookies"];
       [controller addScriptMessageHandler:self name:@"shareNew"];

       configuration.userContentController = controller;

    self.webView = [[WKWebView alloc] initWithFrame:self.view.bounds configuration:configuration];

    self.webView.backgroundColor = [UIColor whiteColor];
    self.webView.navigationDelegate = self;
//    _webView.userInteractionEnabled = YES;
    [self.view addSubview:self.webView];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissAnimation)];
    tapGesture.delegate = self;
    [self.webView addGestureRecognizer:tapGesture];
//    [self.view addGestureRecognizer:tapGesture];
    


}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer

{

    return YES;

}

//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//
//}
//
//
- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
    if (self.isLoad == NO){
        @weakify(self);
        [webView evaluateJavaScript:@"document.getElementById(\"testDiv\").offsetTop"completionHandler:^(id _Nullable result,NSError * _Nullable error) {
            @strongify(self);
            //获取页面高度，并重置webview的frame
            CGFloat lastHeight  = [result doubleValue] + 50;

            self.isLoad = YES;
            self.webHeight = lastHeight;
            dispatch_async(dispatch_get_main_queue(), ^{
                CGRect frame = self.webView.frame;
                frame.size.height = lastHeight;
                self.webView.frame = frame;
            });

            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self drawShareImageView];
            });
        }];
    }
}

#pragma mark - WKScriptMessageHandler  js -> oc

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    if ([message.name isEqualToString:@"share"]) {
        id body = message.body;
        NSLog(@"share分享的内容为：%@", body);
    }
    else if ([message.name isEqualToString:@"shareNew"] || [message.name isEqualToString:@"nativeShare"]) {
        NSDictionary *shareData = message.body;
        NSLog(@"%@分享的数据为： %@", message.name, shareData);
        //模拟异步回调
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //读取js function的字符串
            NSString *jsFunctionString = shareData[@"result"];
            //拼接调用该方法的js字符串
            NSString *callbackJs = [NSString stringWithFormat:@"(%@)(%d);", jsFunctionString, NO];    //后面的参数NO为模拟分享失败
            //执行回调
            [self.webView evaluateJavaScript:callbackJs completionHandler:^(id _Nullable result, NSError * _Nullable error) {
                if (!error) {
                    NSLog(@"模拟回调，分享失败");
                }
            }];
        });
    }
    else if ([message.name isEqualToString:@"currentCookies"]) {
        NSString *cookiesStr = message.body;
        NSLog(@"当前的cookie为： %@", cookiesStr);
    }
    else if ([message.name isEqualToString:@"imageDidClick"]) {

        NSDictionary *dict = message.body;
        NSString *selectedImageUrl = dict[@"imgUrl"];
        CGFloat x = [dict[@"x"] floatValue] + + self.webView.scrollView.contentInset.left;
        CGFloat y = [dict[@"y"] floatValue] + self.webView.scrollView.contentInset.top;
        CGFloat width = [dict[@"width"] floatValue];
        CGFloat height = [dict[@"height"] floatValue];
        CGRect frame = CGRectMake(x, y, width, height);
        NSUInteger index = [dict[@"index"] integerValue];
        NSLog(@"点击了第%@个图片，\n链接为%@，\n在Screen中的绝对frame为%@，\n所有的图片数组为%@", @(index), selectedImageUrl, NSStringFromCGRect(frame), dict[@"imgUrls"]);
        NSArray *imgArr = dict[@"imgUrls"];
    }
    //选择联系人
    else if ([message.name isEqualToString:@"nativeChoosePhoneContact"]) {
        NSLog(@"正在选择联系人");

    }
}


#pragma mark - PPSnapshotHandlerDelegate

- (void)snapshotHandler:(PPSnapshotHandler *)snapshotHandler didFinish:(UIImage *)captureImage forView:(UIView *)view
{
    PPSnapshotHandler.defaultHandler.delegate = nil;
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        [PHAssetChangeRequest creationRequestForAssetFromImage:captureImage];
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
           if (error) {
               dispatch_async(dispatch_get_main_queue(), ^{
                   showToastView(@"保存失败", kCommonWindow);
               });
           } else {
               dispatch_async(dispatch_get_main_queue(), ^{
                   showToastView(@"保存成功", kCommonWindow);
               });
           }
       }];
}

@end
