//
//  BYImageBrowseCell.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/11/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYImageBrowseCell : UICollectionViewCell


/** uiscrollview */
@property (nonatomic ,strong ,readonly) UIScrollView *scrollView;
/** imageView */
@property (nonatomic ,strong ,readonly) PDImageView *imageView;
/** 点击 */
@property (nonatomic ,copy) void (^didSelectImageView)(void);


- (void)setContentWithObject:(id)object placeholder:(UIImage *)placeholder indexPath:(NSIndexPath *)indexPath;
- (void)verifyImageFrame:(UIImage *)image;
@end

NS_ASSUME_NONNULL_END
