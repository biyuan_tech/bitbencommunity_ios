//
//  BYCommonTitleTypeCell.m
//  BY
//
//  Created by 黄亮 on 2018/9/7.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYCommonTitleTypeCell.h"

@interface BYCommonTitleTypeCell()
{
    BOOL _showBottomLine;
}
@property (nonatomic ,strong) UIView *bottomLineView;

@end

@implementation BYCommonTitleTypeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setTitle:(NSString *)title{
    [self.titleLab setText:title];
    [self setNeedsLayout];
}

- (void)setTextFieldText:(NSString *)text{
    self.textField.text = text;
    [self addView:self.textField];
    [self setNeedsLayout];
}

- (void)setPlaceholder:(NSString *)placeholder{
    [self addView:self.textField];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:kTextColor_168}];
    [_textField setAttributedPlaceholder:attributedString];
    [self setNeedsLayout];
}

- (void)setRightImage:(UIImage *)image{
    if (!image) {
        if (_rightImageView) {
            [_rightImageView setImage:nil];
        }
        return;
    }
    [self addView:self.rightImageView];
    [_rightImageView setImage:image];
    [self setNeedsLayout];
}

- (void)setRightTitle:(NSString *)title{
    [self.rightTitleLab setText:title];
    [self addView:_rightTitleLab];
    [self setNeedsLayout];
}

- (void)setRightLogoImage:(UIImage *)image{
    if (!image) {
        if (_rightLogoImageView) {
            [_rightLogoImageView setImage:nil];
        }
        return;
    }
    [self addView:self.rightLogoImageView];
    [_rightLogoImageView setImage:image];
    [self setNeedsLayout];
}

- (void)serRightLogoImageUrl:(NSString *)url{
    if (!url.length) {
        [self.rightLogoImageView setImage:nil];
        return;
    }
    [self addView:self.rightLogoImageView];
    [_rightLogoImageView uploadImageWithURL:url placeholder:nil callback:nil];
    [self setNeedsLayout];
}


- (void)showBottomLine:(BOOL)show{
    _showBottomLine = show;
    [self addView:self.bottomLineView];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGFloat cellW = CGRectGetWidth(self.contentView.frame);
    CGFloat cellH = CGRectGetHeight(self.contentView.frame);
    if (_titleLab.text.length) {
        CGFloat titleLabW = stringGetWidth(_titleLab.text, _titleLab.font.pointSize);
        CGFloat titleLabH = stringGetHeight(_titleLab.text, _titleLab.font.pointSize);
        _titleLab.frame = CGRectMake(kCellLeftSpace, (cellH - titleLabH)/2, titleLabW, titleLabH);
    }
    
    if (_rightImageView.image) {
        CGFloat rightImgViewW = _rightImageView.image.size.width;
        CGFloat rightImgViewH = _rightImageView.image.size.height;
        CGFloat rightImgViewX = cellW - rightImgViewW - kCellRightSpace;
        _rightImageView.frame = CGRectMake(rightImgViewX, (cellH - rightImgViewH)/2, rightImgViewW, rightImgViewH);
    }
    
    if (_rightTitleLab.text.length) {
        CGFloat rightTitleLabW = stringGetWidth(_rightTitleLab.text, _rightTitleLab.font.pointSize);
        CGFloat rightTitleLabH = stringGetHeight(_rightTitleLab.text, _rightTitleLab.font.pointSize);
        CGFloat rightTitleX = _rightImageView.image ? CGRectGetMinX(_rightImageView.frame) - kCellRightTitleSapce - rightTitleLabW : cellW - kCellRightTitleSapce - rightTitleLabW;
        _rightTitleLab.frame = CGRectMake(rightTitleX, (cellH - rightTitleLabH)/2, rightTitleLabW, rightTitleLabH);
    }
    
    if (_rightLogoImageView.image) {
        CGFloat rightLogoImgViewW = 26;
        CGFloat rightLogoImgViewH = 26;
        CGFloat rightLogoImgViewX = _rightImageView.image ? CGRectGetMinX(_rightImageView.frame) - kCellRightTitleSapce - rightLogoImgViewW : cellW - rightLogoImgViewW - kCellRightSpace;
        _rightLogoImageView.frame = CGRectMake(rightLogoImgViewX, (cellH - rightLogoImgViewH)/2, rightLogoImgViewW, rightLogoImgViewH);
        [_rightLogoImageView addCornerRadius:13 size:CGSizeMake(26, 26)];
    }
    
    if (_textField) {
        CGFloat textFieldX = _titleLab.text.length ? CGRectGetMaxX(_titleLab.frame) + 10 : kCellLeftSpace;
        CGFloat textFieldW = _rightImageView.image ? CGRectGetMinX(_rightImageView.frame) - textFieldX : (_rightTitleLab.text ? CGRectGetMinX(_rightTitleLab.frame) - textFieldX : (_rightLogoImageView.image ? CGRectGetMinX(_rightLogoImageView.frame) - textFieldX : cellW - kCellRightSpace));
        _textField.frame = CGRectMake(textFieldX, 0, textFieldW, cellH);
    }
    if (_showBottomLine) {
        _bottomLineView.frame = CGRectMake(0, cellH - 0.5, cellW, 0.5);
    }
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    
}

- (void)addView:(UIView *)view{
    if (view) {
        [self.contentView addSubview:view];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

#pragma mark - initMethod
- (UITextField *)textField{
    if (!_textField) {
        _textField = [UITextField by_init];
        _textField.font = [UIFont systemFontOfSize:13];
    }
    return _textField;
}

- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel by_init];
        _titleLab.textColor = kTextColor_39;
        [_titleLab setBy_font:13];
        _titleLab.textAlignment = NSTextAlignmentLeft;
        [self addView:_titleLab];
    }
    return _titleLab;
}

- (UIImageView *)rightImageView{
    if (!_rightImageView) {
        _rightImageView = [UIImageView by_init];
    }
    return _rightImageView;
}

- (UILabel *)rightTitleLab{
    if (!_rightTitleLab) {
        _rightTitleLab = [UILabel by_init];
        [_rightTitleLab setBy_font:12];
        _rightTitleLab.textColor = kTextColor_154;
        _rightTitleLab.textAlignment = NSTextAlignmentRight;
    }
    return _rightTitleLab;
}

- (PDImageView *)rightLogoImageView{
    if (!_rightLogoImageView) {
        _rightLogoImageView = [PDImageView by_init];
        _rightLogoImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _rightLogoImageView;
}

- (UIView *)bottomLineView{
    if (!_bottomLineView) {
        _bottomLineView = [UIView by_init];
        _bottomLineView.backgroundColor = kBgColor_237;
    }
    return _bottomLineView;
}
@end
