//
//  BYEditingTextView.m
//  BY
//
//  Created by 黄亮 on 2018/9/11.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "BYEditingTextView.h"
#import "UITextView+BYExtension.h"

@interface BYEditingTextView()<UITextViewDelegate>
{
    CGFloat _contentViewH;
}

@property (nonatomic ,weak) UIView *fathureView;

@property (nonatomic ,assign) BYEditingTextViewType editingType;
/** 遮罩 */
@property (nonatomic ,strong) UIView *maskView;

@property (nonatomic ,strong) UIView *contentView;

@property (nonatomic ,strong) UILabel *maxNumLab;
@end

static NSInteger baseTag = 0x4123;
static NSInteger keyboardDistanceFromContentView = 15;
@implementation BYEditingTextView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFathureView:(UIView *)fathureView editingType:(BYEditingTextViewType)editingType{
    self = [super init];
    if (self) {
        _fathureView = fathureView;
        _editingType = editingType;
        _maxlength = editingType == BYEditingTextViewTypeNormal ? 15 : 500;
        self.frame = fathureView.bounds;
        [self setContentView];
    }
    return self;
}

- (void)setPlaceholder:(NSString *)placeholder{
    _placeholder = placeholder;
    _textView.by_placeholder = placeholder;
}

- (void)setMaxlength:(NSInteger)maxlength{
    _maxlength = maxlength;
    _maxNumLab.text = [NSString stringWithFormat:@"0 / %d",(int)_maxlength];
}

- (void)addNSNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)showAnimation{
    [self.fathureView addSubview:self];
    self.contentView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView beginAnimations:@"animation" context:nil];
    [UIView setAnimationDuration:0.1];
    self.contentView.alpha = 1.0;
    self.maskView.alpha = 1.0;
    self.contentView.layer.transform = CATransform3DMakeScale(1.1, 1.1, 1);
    [UIView setAnimationDelay:0.1];
    [UIView setAnimationDuration:0.1];
    self.contentView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView commitAnimations];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.textView becomeFirstResponder];
    });
}

- (void)hiddenAnimation{
    [UIView beginAnimations:@"hidden" context:nil];
    [UIView setAnimationDuration:0.2];
    self.contentView.alpha = 0;
    self.maskView.alpha = 0;
    [UIView commitAnimations];
    [self removeFromSuperview];
}

- (void)resetFrame{
    [self updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
        }];
        [self layoutIfNeeded];
    }];

}

- (void)endEditing{
    [self endEditing:YES];
}

- (void)buttonAction:(UIButton *)sender{
    if (sender.tag == baseTag) {
        [self hiddenAnimation];
    }
    else if (sender.tag == baseTag + 1){
        if (_didConfirmHandle && self.textView.text.length) {
            _didConfirmHandle(self.textView.text);
        }
        [self hiddenAnimation];
    }
}

#pragma mark - NSNotification
- (void)keyBoardWillShow:(NSNotification *)notic{
    NSDictionary *userInfo = notic.userInfo;
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyBoardRect = [value CGRectValue];
    CGFloat keyBoardH = keyBoardRect.size.height;
    
    if (kCommonScreenHeight/2 - _contentViewH/2 < keyBoardH) {
        [self updateConstraintsIfNeeded];
        [UIView animateWithDuration:0.2 animations:^{
            @weakify(self);
            [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
                @strongify(self);
                make.centerY.mas_equalTo((kCommonScreenHeight - self->_contentViewH)/2 - keyBoardH - keyboardDistanceFromContentView);
            }];
            [self layoutIfNeeded];
        }];
    }
}

- (void)keyBoardWillHidden:(NSNotification *)notic{
    // 重置contentView frame
    [self resetFrame];
}

#pragma mark - UITextViewDelegate

//- (void)

- (void)textViewDidChange:(UITextView *)textView{
    _maxNumLab.text = [NSString stringWithFormat:@"%d / %d",(int)textView.text.length,(int)_maxlength];
    if (textView.text.length > _maxlength) {
        UITextRange *markedRange = [textView markedTextRange];
        if (markedRange) {
            return;
        }
        NSRange range = [textView.text rangeOfComposedCharacterSequenceAtIndex:_maxlength];
        textView.text = [textView.text substringToIndex:range.location];
        _maxNumLab.text = [NSString stringWithFormat:@"%d / %d",(int)_maxlength,(int)_maxlength];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
//    if (range.location >= _maxlength) {
//
//        return NO;
//    }
//    else if ([text isEqualToString:@"\n"]) { // 点击return收起键盘
//        [textView resignFirstResponder];
//        return NO;
//    }
    return YES;
}

- (void)setContentView{
    
    [self addNSNotification];
    
    self.maskView = [UIView by_init];
    _maskView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.47];
    [self addSubview:_maskView];
    [_maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEditing)];
    [_maskView addGestureRecognizer:tapGestureRecognizer];
    
    CGFloat whRatio = _editingType == BYEditingTextViewTypeNormal ? 276.0/188.0 : 345.0/229.0;
    CGFloat wwRation = _editingType == BYEditingTextViewTypeNormal ? 276.0/375.0 : 345.0/375.0;
    CGFloat width = wwRation*kCommonScreenWidth;
    CGFloat height = width/whRatio;
    _contentViewH = height;
    self.contentView = [UIView by_init];
    _contentView.backgroundColor = [UIColor whiteColor];
    [_contentView layerCornerRadius:10 size:CGSizeMake(width, height)];
    [self addSubview:_contentView];
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
        make.centerX.centerY.mas_equalTo(0);
    }];
    
    UITextView *textView = [[UITextView alloc] init];
    textView.by_placeholder = _editingType == BYEditingTextViewTypeNormal ? @"请输入名称" : @"请输入简介";
    textView.delegate = self;
    [textView registerNSNotification];
    [_contentView addSubview:textView];
    _textView = textView;
    [textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(8, 8, 70, 8));
    }];
    
    UIView *lineView = [UIView by_init];
    lineView.backgroundColor = kBgColor_237;
    [_contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-48.5);
        make.height.mas_equalTo(0.5);
    }];
    
    for (int i = 0; i < 2; i ++) {
        UIButton *button = [UIButton by_buttonWithCustomType];
        NSDictionary *attributeds = @{@"title":i == 0 ? @"取消":@"保存",NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:i==0?kTextColor_154:kTextColor_39};
        [button setBy_attributedTitle:attributeds forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [_contentView addSubview:button];
        button.tag = baseTag + i;
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo((width/2)*i);
            make.bottom.mas_equalTo(0);
            make.height.mas_equalTo(48);
            make.width.mas_equalTo(width/2);
        }];
    }
    
    UIView *verticalLineView = [UIView by_init];
    verticalLineView.backgroundColor = kBgColor_237;
    [_contentView addSubview:verticalLineView];
    [verticalLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.centerX.mas_equalTo(0);
        make.height.mas_equalTo(48);
        make.width.mas_equalTo(0.5);
    }];
    
    // 最大字数限制
    UILabel *maxNumLab = [UILabel by_init];
    [maxNumLab setBy_font:13];
    [maxNumLab setTextColor:kTextColor_154];
    maxNumLab.textAlignment = NSTextAlignmentRight;
    maxNumLab.text = [NSString stringWithFormat:@"0 / %d",(int)_maxlength];
    [_contentView addSubview:maxNumLab];
    _maxNumLab = maxNumLab;
    [maxNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(80);
        make.right.mas_equalTo(-19);
        make.bottom.mas_equalTo(-60);
        make.height.mas_equalTo(10);
    }];
}

@end
