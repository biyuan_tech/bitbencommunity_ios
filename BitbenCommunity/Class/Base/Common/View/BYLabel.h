//
//  BYLabel.h
//  BibenCommunity
//
//  Created by 随风 on 2019/4/3.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYLabel : UILabel

/** 字体描边 */
@property (nonatomic ,assign) CGFloat fontStrokeWidth;

/** 字体秒表色值 */
@property (nonatomic ,strong) UIColor *fontStrokeColor;

/** label色值 */
@property (nonatomic ,strong) UIColor *labelColor;

@end

NS_ASSUME_NONNULL_END
