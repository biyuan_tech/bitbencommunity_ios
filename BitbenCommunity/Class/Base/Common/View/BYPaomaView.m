//
//  BYPaomaView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/5/31.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYPaomaView.h"

//单次循环的时间
static const NSInteger animationDuration = 15;


@interface BYPaomaView ()<CAAnimationDelegate>
{
    //左侧label的frame
    CGRect currentFrame;
    
    //右侧label的frame
    CGRect behindFrame;
    
    //存放左右label的数组
    NSMutableArray *labelArray;
    
    //label的高度
    CGFloat labelHeight;
    
    //是否为暂停状态
    BOOL isStop;
    
}

@end

@implementation BYPaomaView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
    }
    return self;
}

- (void)setTiltleColor:(UIColor *)tiltleColor{
    _tiltleColor = tiltleColor;;
    UILabel *lableOne = labelArray[0];
    UILabel *lableTwo = labelArray[1];
    lableOne.textColor = tiltleColor;
    lableTwo.textColor = tiltleColor;
}

- (void)doCustomAnimation
{
    //取到两个label
    UILabel *lableOne = labelArray[0];
    UILabel *lableTwo = labelArray[1];
    
    [lableOne.layer removeAnimationForKey:@"LabelOneAnimation"];
    [lableTwo.layer removeAnimationForKey:@"LabelTwoAnimation"];
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    animation.values = @[[NSValue valueWithCGPoint:CGPointMake(lableOne.frame.origin.x+currentFrame.size.width/2, lableOne.frame.origin.y+labelHeight/2)],[NSValue valueWithCGPoint:CGPointMake(lableOne.frame.origin.x-currentFrame.size.width/2, lableOne.frame.origin.y+labelHeight/2)]];
    animation.duration = animationDuration;
    animation.delegate = self;
    animation.calculationMode = kCAAnimationLinear;
    animation.removedOnCompletion = NO;
    animation.repeatCount = MAXFLOAT;
    [lableOne.layer addAnimation:animation forKey:@"LabelOneAnimation"];
    
    
    CAKeyframeAnimation *animationTwo = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    animationTwo.values = @[[NSValue valueWithCGPoint:CGPointMake(lableTwo.frame.origin.x+currentFrame.size.width/2, lableTwo.frame.origin.y+labelHeight/2)],[NSValue valueWithCGPoint:CGPointMake(lableTwo.frame.origin.x-currentFrame.size.width/2, lableTwo.frame.origin.y+labelHeight/2)]];
    animationTwo.duration = animationDuration;
    animationTwo.delegate = self;
    animationTwo.removedOnCompletion = NO;
    animationTwo.repeatCount = MAXFLOAT;
    animationTwo.calculationMode = kCAAnimationLinear;
    
    [lableTwo.layer addAnimation:animationTwo forKey:@"LabelTwoAnimation"];
    
}



- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    
    UILabel *lableOne = labelArray[0];
    UILabel *lableTwo = labelArray[1];
    
    if (flag && anim == [lableOne.layer animationForKey:@"LabelOneAnimation"]) {
        
        
        //两个label水平相邻摆放 内容一样 label1为初始时展示的 label2位于界面的右侧，未显示出来
        //当完成动画时，即第一个label在界面中消失，第二个label位于第一个label的起始位置时，把第一个label放置到第二个label的初始位置
        lableOne.frame = behindFrame;
        
        lableTwo.frame = currentFrame;
        
        //在数组中将第一个label放置到右侧，第二个label放置到左侧（因为此时展示的就是labelTwo）
        [labelArray replaceObjectAtIndex:1 withObject:lableOne];
        [labelArray replaceObjectAtIndex:0 withObject:lableTwo];
        
        
        
        [self doCustomAnimation];
        
    }
    
    
}


- (CGFloat) widthForTextString:(NSString *)tStr height:(CGFloat)tHeight fontSize:(CGFloat)tSize{
    
    NSDictionary *dict = @{NSFontAttributeName:[UIFont systemFontOfSize:tSize]};
    CGRect rect = [tStr boundingRectWithSize:CGSizeMake(MAXFLOAT, tHeight) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    return rect.size.width+5;
    
}

- (void)removeAllSubViews{
    for (UIView *subView in self.subviews) {
        [subView removeFromSuperview];
    }
}

- (void)startAnimation:(NSString *)title{
    
    [self removeAllSubViews];
    
    labelHeight = self.frame.size.height;
    CGFloat width = stringGetWidth(title, 13) + 50;
    
    //这两个frame很重要 分别记录的是左右两个label的frame 而且后面也会需要到这两个frame
    currentFrame = CGRectMake(0, 0, width, labelHeight);
    behindFrame = CGRectMake(currentFrame.origin.x+currentFrame.size.width, 0, width, labelHeight);

    UILabel *titleLab = [UILabel by_init];
    titleLab.text = title;
    [titleLab setBy_font:13];
    titleLab.textColor = _tiltleColor ? _tiltleColor : kColorRGBValue(0x323232);
    titleLab.frame = currentFrame;
    [self addSubview:titleLab];
    
    labelArray  = [NSMutableArray arrayWithObject:titleLab];

    if (width > self.frame.size.width) {
        UILabel *behindLabel = [[UILabel alloc]init];
        behindLabel.frame = behindFrame;
        behindLabel.text = title;
        behindLabel.font = [UIFont systemFontOfSize:13];
        behindLabel.textColor = _tiltleColor ? _tiltleColor : kColorRGBValue(0x323232);
        [labelArray addObject:behindLabel];
        [self addSubview:behindLabel];
        
        [self doCustomAnimation];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self start];
    });
}


- (void)start
{
    
    UILabel *lableOne = labelArray[0];
    [self resumeLayer:lableOne.layer];
    
    UILabel *lableTwo = labelArray[1];
    [self resumeLayer:lableTwo.layer];
    
    isStop = NO;
    
}

- (void)stop
{
    UILabel *lableOne = labelArray[0];
    [self pauseLayer:lableOne.layer];
    
    UILabel *lableTwo = labelArray[1];
    [self pauseLayer:lableTwo.layer];
    
    isStop = YES;
}

//暂停动画
- (void)pauseLayer:(CALayer*)layer
{
    CFTimeInterval pausedTime = [layer convertTime:CACurrentMediaTime() fromLayer:nil];
    
    layer.speed = 0;
    
    layer.timeOffset = pausedTime;
}

//恢复动画
- (void)resumeLayer:(CALayer*)layer
{
    //当你是停止状态时，则恢复
    if (isStop) {
        
        CFTimeInterval pauseTime = [layer timeOffset];
        
        layer.speed = 1.0;
        
        layer.timeOffset = 0.0;
        
        layer.beginTime = 0.0;
        
        CFTimeInterval timeSincePause = [layer convertTime:CACurrentMediaTime() fromLayer:nil]-pauseTime;
        
        layer.beginTime = timeSincePause;
    }
    
}

@end
