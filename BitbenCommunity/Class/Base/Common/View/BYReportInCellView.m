//
//  BYReportInCellView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/6/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYReportInCellView.h"

static NSInteger kBaseTag = 0x5352;
@interface BYReportInCellView ()
{
    CGPoint _point;
}

/**  superView */
@property (nonatomic ,strong) UIView *fathuerView;
/** titles */
@property (nonatomic ,strong) NSArray *titles;
/** reportView */
@property (nonatomic ,strong) UIView *reportView;
/** 举报内容 */
@property (nonatomic ,strong) UIView *reportContentView;
/** maskView */
@property (nonatomic ,strong) UIControl *maskView;

@end

@implementation BYReportInCellView

+ (instancetype)initReportViewShowInView:(UIView *)view{
    BYReportInCellView *reportView = [[BYReportInCellView alloc] init];
    reportView.fathuerView = view ? view : kCommonWindow;
    reportView.frame = reportView.fathuerView.bounds;
    reportView.titles = @[@"违法违规",@"色情暴力",@"攻击歧视",@"广告营销",@"与话题无关"];
    [reportView setContentView];
    return reportView;
}

//- (instancetype)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        _titles = @[@"违法违规",@"色情暴力",@"攻击歧视",@"广告营销",@"与话题无关"];
//        [self setContentView];
//    }
//    return self;
//}

- (void)showAnimationWithPoint:(CGPoint)point{
    _point = point;
    self.reportView.frame = CGRectMake(point.x - 105 - 15, point.y, 105, 45);
    if (!self.superview) {
        [self.fathuerView addSubview:self];
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.reportView.alpha = 1.0;
        self.maskView.alpha = 1.0;
    }];
}

- (void)hiddenAnimation{
    [UIView animateWithDuration:0.1 animations:^{
        self.reportContentView.alpha = 0.0;
        self.maskView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - action
- (void)reportLabAction{
    self.reportContentView.frame = CGRectMake(_point.x - 120 - 15, _point.y, 120, 230);
    [UIView animateWithDuration:0.2 animations:^{
        self.reportView.alpha = 0.0f;
        self.reportContentView.alpha = 1.0f;
    }];
}

- (void)reportBtnAction:(UIButton *)sender{
    [self hiddenAnimation];
    NSInteger index = sender.tag - kBaseTag;
    [[BYLiveHomeRequest alloc] loadRequestReportThemeId:self.theme_id themeType:self.theme_type reportType:self.titles[index] successBlock:^(id object) {
        showToastView(@"举报成功", kCommonWindow);
    } faileBlock:^(NSError *error) {
        showToastView(@"举报失败", kCommonWindow);
    }];
}

- (void)maskViewAction{
    [self hiddenAnimation];
}

#pragma mark - configUI

- (void)setContentView{
    self.maskView = [UIControl by_init];
    [self.maskView addTarget:self action:@selector(maskViewAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    self.reportView = [UIView by_init];
    [self.reportView setBackgroundColor:[UIColor whiteColor]];
    self.reportView.frame = CGRectMake(0, 0, 105, 45);
    self.reportView.layer.cornerRadius = 4.0f;
    self.reportView.layer.shadowColor = kColorRGB(198, 198, 198, 0.3).CGColor;
    self.reportView.layer.shadowOffset = CGSizeMake(0,0);
    self.reportView.layer.shadowOpacity = 1.0;
    self.reportView.layer.shadowRadius = 23;
    self.reportView.alpha = 1.0;
    self.reportView.clipsToBounds = NO;
    [self addSubview:self.reportView];
    
    UIButton *reportLab = [UIButton by_buttonWithCustomType];
    [reportLab.titleLabel setBy_font:14];
    [reportLab setTitle:@"举报" forState:UIControlStateNormal];
    [reportLab setTitleColor:kColorRGBValue(0x323232) forState:UIControlStateNormal];
    [reportLab addTarget:self action:@selector(reportLabAction) forControlEvents:UIControlEventTouchUpInside];
    [self.reportView addSubview:reportLab];
    [reportLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self addReportContentView];
}

- (void)addReportContentView{
    
    self.reportContentView = [UIView by_init];
    [self.reportContentView setBackgroundColor:[UIColor whiteColor]];
    self.reportContentView.frame = CGRectMake(0, 0, 120, 230);
    self.reportContentView.layer.cornerRadius = 4.0f;
    self.reportContentView.layer.shadowColor = kColorRGB(198, 198, 198, 0.3).CGColor;
    self.reportContentView.layer.shadowOffset = CGSizeMake(0,0);
    self.reportContentView.layer.shadowOpacity = 1;
    self.reportContentView.layer.shadowRadius = 23;
    self.reportContentView.alpha = 0.0;
    [self addSubview:self.reportContentView];
    
    for (int i = 0; i < 5; i ++) {
        NSString *title = _titles[i];
        UIButton *btn = [UIButton by_buttonWithCustomType];
        [btn setBy_attributedTitle:@{@"title":title,
                                     NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:kColorRGBValue(0x323232)
                                     } forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(reportBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = kBaseTag + i;
        [self.reportContentView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.height.mas_equalTo(46);
            make.top.mas_equalTo(46*i);
        }];
    }
    
    for (int i = 0; i < 4; i ++) {
        UIView *lineView = [UIView by_init];
        [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
        [self.reportContentView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(102);
            make.centerX.mas_equalTo(0);
            make.top.mas_equalTo(46*(i + 1));
            make.height.mas_equalTo(0.5);
        }];
    }
}
@end
