//
//  BYCommonViewController.h
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYBaseViewController.h"

@class BYCommonViewModel;
@interface BYCommonViewController : BYBaseViewController

@property (nonatomic ,strong ,readonly) BYCommonViewModel *viewModel;


// ** 设置绑定的viewModel */
- (Class)getViewModelClass;

- (void)showAlertView:(NSString *)title message:(NSString *)message;

- (void)showToastView:(NSString *)title;


- (void)authorizePush:(AbstractViewController *)viewController animation:(BOOL)animation;
+ (void)authorizeWithCompletionHandler:(void(^)(BOOL successed))handler;
+ (void)centerItemBtnClickAction;


void showToastView(NSString *title,UIView *view);
void showDebugToastView(NSString *title,UIView *view);

@end
