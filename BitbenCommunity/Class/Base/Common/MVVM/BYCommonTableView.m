//
//  BYCommonTableView.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYCommonTableView.h"
//#import <MJRefresh/MJRefresh.h>
#import "BYCommonModel.h"
#import <objc/runtime.h>
#import "UITableViewCell+BYExtension.h"
#import "BYRefreshNormalHeader.h"
#import "BYRefreshNormalFooter.h"

#define CELLNAMEKEY @"cellString"
#define SECTIONKEY @"sectoinString"
#define IDENTIFIER @"identifier"


@interface BYCommonTableView()
{
    BOOL _pullRefreshing;
    
    UILabel *stateLabel;
    
    BOOL isLoadingMore;
}

@end

@implementation BYCommonTableView

- (void)dealloc
{
    self.dataSource = nil;
    self.delegate = nil;
    _tableData = nil;
    _cellDelegateObject = nil;
    _group_delegate = nil;
    self.tableHeaderView = nil;
    self.tableFooterView = nil;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:style];
    if (self)
    {
        self.dataSource = self;
        self.delegate = self;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.backgroundColor = [UIColor clearColor];
        
        if (@available(iOS 11.0, *)) {
            self.contentInsetAdjustmentBehavior =UIScrollViewContentInsetAdjustmentNever;
            self.contentInset =UIEdgeInsetsMake(0,0,0,0);
            self.scrollIndicatorInsets = self.contentInset;
            self.estimatedRowHeight = 0;
            self.estimatedSectionFooterHeight = 0;
            self.estimatedSectionHeaderHeight = 0;
        }
    }
    return self;
}

#pragma mark - custom method

- (void)receiveActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([self.group_delegate respondsToSelector:@selector(by_tableView:reveiveCellActionName:param:indexPath:)]) {
        [self.group_delegate by_tableView:self reveiveCellActionName:actionName param:param indexPath:indexPath];
    }
}

- (void)addHeaderRefreshTarget:(id)target action:(SEL)action{
    self.mj_header = [BYRefreshNormalHeader headerWithRefreshingTarget:target refreshingAction:action];
}
- (void)addFooterRefreshTarget:(id)target action:(SEL)action{
    self.mj_footer = [BYRefreshNormalFooter footerWithRefreshingTarget:target refreshingAction:action];
}

- (void)addHeaderRefreshHandle:(void(^)(void))handle{
    self.mj_header = [BYRefreshNormalHeader headerWithRefreshingBlock:handle];
}

- (void)addFooterRefreshHandle:(void(^)(void))handle{
    self.mj_footer = [BYRefreshNormalFooter footerWithRefreshingBlock:handle];
}

- (void)beginRefreshing{
    [self.mj_header beginRefreshing];
}

- (void)endRefreshing{
    [self.mj_header endRefreshing];
    [self.mj_footer endRefreshing];
}

- (void)endRefreshingWithNoMoreData{
    [self.mj_footer endRefreshingWithNoMoreData];
}

- (void)scrollToBottom:(BOOL)animation{
    if (_tableData.count == 0) {
        return;
    }
    if (self.tracking) {
        return;
    }
    
    id tmpObj = _tableData[0];
    if (![tmpObj isKindOfClass:[NSDictionary class]]) {
        // 异步让tableview滚到最底部
        NSInteger cellCount = [self numberOfRowsInSection:0];
        NSInteger num = cellCount - 1 > 0 ? cellCount - 1 : 0;
        NSIndexPath *index = [NSIndexPath indexPathForRow:num  inSection:0];
        [self scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionBottom animated:animation];
    }
}

- (void)addDataToTop:(NSArray *)data{
    NSMutableArray *array = [NSMutableArray arrayWithArray:data];
    [array addObjectsFromArray:self.tableData];
    self.tableData = array;
    
    // 异步让tableview滚到最底部
    @weakify(self);
    [self caculatorDataTotalCellHeight:data cb:^(CGFloat height) {
        @strongify(self);
        [self setContentOffset:CGPointMake(0, height - 50)];
    }];
}

- (void)caculatorDataTotalCellHeight:(NSArray *)data cb:(void(^)(CGFloat height))cb{
    if (!data.count) {
        cb(0);
        return;
    }
    id tmpObj = _tableData[0];
    __block CGFloat height = 0;
    if (![tmpObj isKindOfClass:[NSDictionary class]]) {
        [data enumerateObjectsUsingBlock:^(BYCommonModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            height = height + obj.cellHeight;
            if (idx == data.count - 1) {
                cb(height);
            }
        }];
    }else{
        [data enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
            NSArray *cellData = dic.allValues[0];
            [cellData enumerateObjectsUsingBlock:^(BYCommonModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                height = height + obj.cellHeight;
            }];
            if (idx == data.count - 1) {
                cb(height);
            }
        }];
    }
}

/**
 *  用于设置section的header和footer
 *
 *  @param dataObject header和footer的数据
 *  @param tableView  表
 *  @param section    section的index
 *
 *  @return 返回自定义的section的header和footerview
 */
- (UIView *) sectionViewWithObject:(id) dataObject tableView:(UITableView *) tableView section:(NSInteger) section
{
    //读取section的classstring
    NSString *classString = nil;
    
    if (tableView.style == UITableViewStylePlain ) {
        classString = [[_tableData objectAtIndex:section] allKeys][0];
    }
    else{
        //        return [FTViewTool creatSectionView:nil];
    }
    
    if (classString.length == 0)
    {
        return nil;
        
    }
    
    //和表的cell使用相似
    NSString *identifier = classString;
    UIView *sectionView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:identifier];
    
    if (!sectionView)
    {
        sectionView = [[NSClassFromString(classString) alloc] init];
    }
#pragma clang diagnostic ignored "-Wundeclared-selector"
    if ([sectionView respondsToSelector:@selector(setContentWithObject:AtSection:)])
    {
        [sectionView performSelector:@selector(setContentWithObject:AtSection:) withObject:dataObject withObject:[NSNumber numberWithInteger:section]];
    }
    return sectionView;
}

- (void) setTableData:(NSArray *)tableData
{
    _tableData = tableData;
    
    if (_tableData && !_notAutoReload)
    {
        [self reloadData];
    }
}

- (void) setTableSectionFooterData:(NSArray *)tableSectionFooterData
{
    _tableSectionFooterData = tableSectionFooterData;
    if (_tableSectionFooterData)
    {
        [self reloadData];
    }
    
}

- (void)setTableSectionHeaderData:(NSArray *)tableSectionHeaderData
{
    _tableSectionHeaderData = tableSectionHeaderData;
    if (_tableSectionHeaderData)
    {
        [self reloadData];
    }
    
}

#pragma mark - 表的代理方法
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    //判断是否有section
    if (_tableData.count == 0 || !_tableData) {
        return 1;
    }
    id tmpObj = _tableData[0];
    if ([tmpObj isKindOfClass:[NSDictionary class]]) {
        
        return _tableData.count;
    }
    else
    {
        return 1;
    }
    
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_tableData.count == 0 || !_tableData) {
        return 0;
    }
    id tmpObj = _tableData[section];
    
    //判断是否是对象还是Dictionary
    if ([tmpObj isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *tmpDic = (NSDictionary *)tmpObj;
        NSArray *tmpArr = tmpDic.allValues[0];
        
        return tmpArr.count;
    }
    else
    {
        return _tableData.count;
    }
    
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = IDENTIFIER;
    
    //获取section
    if (_tableData) {
        id tmpObj = _tableData[indexPath.section];
        
        NSArray *tmpArr = nil;
        
        //判断是否是对象还是Dictionary
        if ([tmpObj isKindOfClass:[NSDictionary class]]) {
            
            //如果是有多个section，需要取出每个section的数据源
            NSDictionary *tmpDic = (NSDictionary *)tmpObj;
            
            //获取section的数据源
            tmpArr = tmpDic.allValues[0];
        }
        else
        {
            //就是不同的对象
            tmpArr = _tableData;
        }
        
        if (self.cellString == nil)
        {
            //获取对象
            id object = tmpArr[indexPath.row];
            
            //根据不同样式初始化不同的cell
            //获取对象所有key
            identifier = [object valueForKey:CELLNAMEKEY];
            
            
            
        }
        else
        {
            identifier = self.cellString;
        }
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil)
    {
        cell = [[NSClassFromString(identifier) alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.tableView = self;
    }
    
    if ([cell respondsToSelector:@selector(setContentWithObject:indexPath:)])
    {
        [cell performSelector:@selector(setContentWithObject:indexPath:) withObject:_tableData withObject:indexPath];
    }
    
    if ([cell respondsToSelector:@selector(setCellDelegate:)])
    {
        [cell performSelector:@selector(setCellDelegate:) withObject:_cellDelegateObject];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    if ([cell respondsToSelector:@selector(setTableViewCellPositionType:)]) {
//        JDPTableViewCellPositionType positionType = [JDPBaseTableViewCell getTableViewCellPositionType:[tableView numberOfRowsInSection:indexPath.section] indexPathRow:indexPath.row];
//        SEL selector = NSSelectorFromString(@"setTableViewCellPositionType:");
//        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[JDPBaseTableViewCell instanceMethodSignatureForSelector:selector]];
//        [invocation setSelector:selector];
//        [invocation setTarget:cell];
//        [invocation setArgument:&positionType atIndex:2];
//        [invocation invoke];
//    }
    return cell;
}

//行单击
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BYCommonTableView *table = (BYCommonTableView *)tableView;
    if ([self.group_delegate respondsToSelector:@selector(by_tableView:tabledidSelectRowAtIndexPath:)])
    {
        [self.group_delegate by_tableView:table tabledidSelectRowAtIndexPath:indexPath];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//行高
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.estimatedRowHeight > 0) {
        return UITableViewAutomaticDimension;
    }
    CGFloat rowHeight = 0.0f;
    if (_tableData.count == 0 || !_tableData) {
        return rowHeight;
    }
    id tmpObj = _tableData[indexPath.section];
    
    //判断是否是对象还是Dictionary
    if ([tmpObj isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *tmpDic = (NSDictionary *)tmpObj;
        NSArray *tmpArr = tmpDic.allValues[0];
        
        return ((BYCommonModel *)tmpArr[indexPath.row]).cellHeight;
    }
    else
    {
        return ((BYCommonModel *)_tableData[indexPath.row]).cellHeight;
    }

    return rowHeight;
}

//section headerView 高度
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 0.0f;
    
    if (tableView.style == UITableViewStyleGrouped) {
        return 0.1;
    }
    if ([self.group_delegate respondsToSelector:@selector(by_tableView:tableViewHeightForHeaderInSection:)])
    {
        height = [self.group_delegate by_tableView:self tableViewHeightForHeaderInSection:section];
    }
    return height;
}

//section headerView
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView.style == UITableViewStyleGrouped) {
        if ([self.group_delegate respondsToSelector:@selector(by_tableView:tableViewForHeaderInSection:)]) {
            return [self.group_delegate by_tableView:self tableViewForHeaderInSection:section];
        }
        return nil;
    }
    else
    {
        if ([self.group_delegate respondsToSelector:@selector(by_tableView:tableViewForHeaderInSection:)]) {
            return [self.group_delegate by_tableView:self tableViewForHeaderInSection:section];
        }
        if (_tableSectionHeaderData) {
            return [self sectionViewWithObject:_tableSectionHeaderData tableView:tableView section:section];
        }
    }
    
    
    
    //    return [FTViewTool creatSectionView:nil upSpace:10 downSpace:10];
    return nil;
}

//section footerView 高度
- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    CGFloat height = 0.0f;
    
    if ([self.group_delegate respondsToSelector:@selector(by_tableView:tableViewHeightForFooterInSection:)])
    {
        height = [self.group_delegate by_tableView:self tableViewHeightForFooterInSection:section];
    }
    
    return height;
}

//section footerView
- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (tableView.style == UITableViewStyleGrouped) {
        if ([self.group_delegate respondsToSelector:@selector(by_tableView:tableViewForFooterInSection:)]) {
            return [self.group_delegate by_tableView:self tableViewForFooterInSection:section];
        }
        return nil;
    }
    else{
        if ([self.group_delegate respondsToSelector:@selector(by_tableView:tableViewForFooterInSection:)]) {
            return [self.group_delegate by_tableView:self tableViewForFooterInSection:section];
        }
        if (_tableSectionFooterData) {
            return [self sectionViewWithObject:_tableSectionFooterData tableView:tableView section:section];
        }
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.group_delegate respondsToSelector:@selector(by_tableView:willDisplayCell:forRowAtIndexPath:)]) {
        [self.group_delegate by_tableView:tableView willDisplayCell:cell forRowAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(nullable NSIndexPath *)indexPath{
    
}

#pragma mark UIScrollViewDelegate Methods //仅当 self.options.pullRefresh = YES 用到
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([self.group_delegate respondsToSelector:@selector(by_tableViewDidScroll:)]) {
        [self.group_delegate by_tableViewDidScroll:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{

}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if ([self.group_delegate respondsToSelector:@selector(by_tableViewDidEndScroll:)]) {
        [self.group_delegate by_tableViewDidEndScroll:scrollView];
    }
}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if ([self.group_delegate respondsToSelector:@selector(by_tableViewWillBeginDragging:)]) {
        [self.group_delegate by_tableViewWillBeginDragging:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if ([self.group_delegate respondsToSelector:@selector(by_tableViewDidEndDecelerating:)]) {
        [self.group_delegate by_tableViewDidEndDecelerating:scrollView];
    }
    if ([self.group_delegate respondsToSelector:@selector(by_tableViewDidEndScroll:)]) {
        [self.group_delegate by_tableViewDidEndScroll:scrollView];
    }
}


- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    if ([self.group_delegate respondsToSelector:@selector(by_tableViewShouldScrollToTop:)]) {
        return [self.group_delegate by_tableViewShouldScrollToTop:scrollView];
    }
    return YES;
}

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    BOOL res = [super pointInside:point withEvent:event];
    if (res && [self.group_delegate respondsToSelector:@selector(by_tableViewBecomeFirstResponder:)]) {
        [self.group_delegate by_tableViewBecomeFirstResponder:self];
    }
    return res;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView *hitView = [super hitTest:point withEvent:event];
    if (hitView) {
        return _notResponder ? nil : hitView;
    }
    return hitView;
}


@end
