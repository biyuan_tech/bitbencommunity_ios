//
//  BYCommonViewController.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYCommonViewController.h"
#import "BYCommonViewModel.h"
#import "BYCreatLiveRoomController.h"
#import "BYNewLiveController.h"
#import "BYNewLiveControllerV1.h"

@interface BYCommonViewController ()<UIGestureRecognizerDelegate>

@property (nonatomic ,strong) BYCommonViewModel *viewModel;

@end

@implementation BYCommonViewController

- (Class)getViewModelClass{
    return [BYCommonViewModel class];
}

- (void)dealloc
{
    NSLog(@"%@ delloc",NSStringFromClass([self class]));
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        BYCommonViewModel *viewModel = (BYCommonViewModel *)[[self getViewModelClass] alloc];
        _viewModel = [viewModel initWithViewController:self];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [_viewModel setContentView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.viewModel viewWillAppear];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [self.navigationController setNavigationBarHidden:NO];
 

//    if ([NSStringFromClass([self class]) isEqualToString:@"BYLiveHomeController"] ||
//        [NSStringFromClass([self class]) isEqualToString:@"CenterRootViewController"]) {
//        [self setHidesBottomBarWhenPushed:NO];
//    }
//    else
//    {
//        [self setHidesBottomBarWhenPushed:YES];
//    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.viewModel viewDidAppear];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.viewModel viewDidDisappear];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.viewModel viewWillDisappear];
//    [self.viewModel viewControllerPopAction];
}


- (void)popAction{
    [self.viewModel viewControllerPopAction];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAlertView:(NSString *)title message:(NSString *)message{
    [BYAlertView showAlertAnimationWithTitle:title message:message inView:self.view];
}

- (void)showToastView:(NSString *)title{
    [BYToastView tostViewPresentInView:self.view title:nullToEmpty(title) duration:kToastDuration complete:nil];
}

- (void)authorizePush:(AbstractViewController *)viewController animation:(BOOL)animation{
    @weakify(self);
    [self authorizeWithCompletionHandler:^(BOOL successed) {
        @strongify(self);
        if (!successed) return ;
        @weakify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            @strongify(self);
            [viewController hidesTabBarWhenPushed];
            [self.navigationController pushViewController:viewController animated:animation];
        });
    }];
}

+ (void)authorizeWithCompletionHandler:(void(^)(BOOL successed))handler{
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){
        handler(YES);
    } else {
        if ([Tool userDefaultGetWithKey:LoginPassword].length && [Tool userDefaultGetWithKey:LoginAccount].length && [Tool userDefaultGetWithKey:LoginType].length){
            [[AccountModel sharedAccountModel] autoLoginWithBlock:^(BOOL isSuccessed) {
                handler(isSuccessed);
            }];
        } else {
            [StatusBarManager statusBarHidenWithText:@"请先进行登录"];
            LoginRootViewController *loginMainViewController = [[LoginRootViewController alloc]init];
            [loginMainViewController loginSuccess:^(BOOL success) {
                if (success){
                    handler(YES);
                } else {
                    handler(NO);
                    [StatusBarManager statusBarHidenWithText:@"取消登录&登录失败"];
                }
            }];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginMainViewController];
            [[BYTabbarViewController sharedController] presentViewController:nav animated:YES completion:NULL];
        }
    }
}

+ (void)centerItemBtnClickAction{
    [BYCommonViewController authorizeWithCompletionHandler:^(BOOL successed) {
        if (!successed) return;
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:kiSHaveLiveRoomKey] isEqual:@YES]) {
//            BYNewLiveController *newLiveController = [[BYNewLiveController alloc] init];
            BYNewLiveControllerV1 *newLiveController = [[BYNewLiveControllerV1 alloc] init];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:newLiveController];
            nav.modalPresentationStyle = UIModalPresentationFullScreen;
            [[BYTabbarViewController sharedController] presentViewController:nav animated:YES completion:NULL];
        }
        else{
            BYCreatLiveRoomController *creatLiveRoomController = [[BYCreatLiveRoomController alloc] init];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:creatLiveRoomController];
            UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
            [currentController.navigationController presentViewController:nav animated:YES completion:NULL];
        }
    }];
}

- (void)configKeyBoard{
    
}

void showToastView(NSString *title,UIView *view){
    [BYToastView tostViewPresentInView:view title:nullToEmpty(title) duration:kToastDuration complete:nil];
}

void showDebugToastView(NSString *title,UIView *view){
#ifdef DEBUG
    NSLog(@"-----%@",title);
    [BYToastView tostViewPresentInView:view title:nullToEmpty(title) duration:kToastDuration complete:nil];
#endif
}


@end
