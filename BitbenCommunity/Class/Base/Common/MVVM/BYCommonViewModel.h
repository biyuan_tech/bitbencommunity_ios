//
//  BYCommonViewModel.h
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BYCommonViewController.h"


#define S_VC self.viewController
#define S_V_VIEW S_VC.view
#define S_V_NC self.viewController.navigationController
#define S_V_RTNC self.viewController.rt_navigationController

@class BYCommonModel;
@interface BYCommonViewModel : NSObject

@property (nonatomic ,weak ,readonly) BYCommonViewController *viewController;
@property (nonatomic ,strong ,readonly) BYCommonModel *dataModel;

// ** 初始化并传入绑定的viewController */
- (instancetype)initWithViewController:(BYCommonViewController *)viewController;

// ** 设置绑定的dataModel */
- (Class)getDataModelClass;

// ** 统一布局调用方法 */
- (void)setContentView;

// ** 控制器返回事件的监听 */
- (void)viewControllerPopAction;

- (void)viewWillAppear;
- (void)viewDidAppear;
- (void)viewWillDisappear;
- (void)viewDidDisappear;
@end
