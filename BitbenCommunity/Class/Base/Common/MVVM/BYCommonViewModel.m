//
//  BYCommonViewModel.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYCommonViewModel.h"
#import "BYCommonModel.h"

@implementation BYCommonViewModel

- (instancetype)initWithViewController:(BYCommonViewController *)viewController{
    self = [super init];
    if (self) {
        _viewController = viewController;
        BYCommonModel *model = (BYCommonModel *)[[self getDataModelClass] alloc];
        _dataModel = [model init];
        [_dataModel getMokeData];
    }
    return self;
}

- (Class)getDataModelClass{
    return [BYCommonModel class];
}

- (void)setContentView{}

- (void)viewControllerPopAction{}

- (void)viewWillAppear{}

- (void)viewDidAppear{}

- (void)viewWillDisappear{}

- (void)viewDidDisappear{}

@end
