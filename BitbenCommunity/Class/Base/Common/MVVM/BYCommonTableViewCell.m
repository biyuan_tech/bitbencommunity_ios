//
//  BYCommonTableViewCell.m
//  BY
//
//  Created by 黄亮 on 2018/7/31.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "BYCommonTableViewCell.h"

@implementation BYCommonTableViewCell
#pragma clang diagnostic ignored "-Wundeclared-selector"

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)sendActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    BYCommonTableView *tableView = (BYCommonTableView *)self.tableView;
    NSAssert(tableView != nil, @"tableView不应为nil");
    if ([tableView respondsToSelector:@selector(receiveActionName:param:indexPath:)]) {
        SEL selector = NSSelectorFromString(@"receiveActionName:param:indexPath:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[BYCommonTableView instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:tableView];
        [invocation setArgument:&actionName atIndex:2];
        [invocation setArgument:&param atIndex:3];
        [invocation setArgument:&indexPath atIndex:4];
        [invocation invoke];
    }
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    self.indexPath = indexPath;
}

- (void)setCellDelegate:(id)delegate{
    
}


@end
