//
//  BYCommonTableView.h
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BYCommonTableView;
@protocol BYCommonTableViewDelegate <NSObject>

@optional

// 点击代理事件
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath;

/**
 从cell接收到的点击事件

 @param tableView tableView
 @param actionName 事件名
 @param param 参数
 @param indexPath indexPath
 */
- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath;

- (void)by_tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;


// block事件回调时回调方法
- (void)by_tableView:(BYCommonTableView *)tableView cellActionHandleAtIndexPath:(NSIndexPath *)indexPath param:(NSDictionary *)param;
//footer header View 高

- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForFooterInSection:(NSInteger)section;
- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForHeaderInSection:(NSInteger)section;
//
- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForHeaderInSection:(NSInteger)section;
- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForFooterInSection:(NSInteger)section;

- (NSArray *)by_sectionIndexTitlesForTableView:(BYCommonTableView *)tableView;
/**
 *  如不实现此方法则默认自动选择至与index对应的section
 *
 *  @param index index
 */
- (void)by_tableIndexViewDidSelectIndex:(NSInteger )index;

- (void)by_tableViewDidScroll:(UIScrollView *)scrollView;

- (void)by_tableViewWillBeginDragging:(UIScrollView *)scrollView;

- (void)by_tableViewDidEndScroll:(UIScrollView *)scrollView;

- (void)by_tableViewDidEndDecelerating:(UIScrollView *)scrollView;

- (BOOL)by_tableViewShouldScrollToTop:(UIScrollView *)scrollView;

- (void)by_tableViewBecomeFirstResponder:(BYCommonTableView *)tableView;
@end

@interface BYCommonTableView : UITableView <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSString *cellString;

// ** 数组  数组成员是字典
@property (nonatomic, strong) NSArray *tableData;
@property (nonatomic, strong) NSArray *tableSectionHeaderData;
@property (nonatomic, strong) NSArray *tableSectionFooterData;
/** 赋值tableData不自动刷新 */
@property (nonatomic ,assign) BOOL notAutoReload;

// ** cell回调代理
@property (weak) id cellDelegateObject;

// ** 表的代理
@property (nonatomic, weak) id<BYCommonTableViewDelegate> group_delegate;
/** 当前tableview不响应事件 */
@property (nonatomic ,assign) BOOL notResponder;


/**
 *  立即进去刷新状态
 */
- (void)beginRefreshing;
/**
 *  结束刷新状态
 */
- (void)endRefreshing;

/**
 *  结束上拉刷新
 */
- (void)endRefreshingWithNoMoreData;

- (void)addHeaderRefreshTarget:(id)target action:(SEL)action;
- (void)addFooterRefreshTarget:(id)target action:(SEL)action;

- (void)addHeaderRefreshHandle:(void(^)(void))handle;
- (void)addFooterRefreshHandle:(void(^)(void))handle;


- (void)scrollToBottom:(BOOL)animation;
- (void)addDataToTop:(NSArray *)data;
@end
