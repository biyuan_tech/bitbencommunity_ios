//
//  NSDate+BYExtension.h
//  BY
//
//  Created by 黄亮 on 2018/8/11.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <Foundation/Foundation.h>

#define K_D_F @"yyyy-MM-dd HH:mm:ss"
@interface NSDate (BYExtension)

/**
 *  获取一个单例模式的NSDateFormatter
 *
 *  @return 单例模式的NSDateFormatter
 */
- (NSDateFormatter *)by_getNSDateFormatter;

/** 获取当前时间 */
+ (NSDate *)by_date;

/**
 *  时间戳转NSDate
 *
 *  @param string        时间戳
 *  @param dateformatter 时间格式(例：yyyy-MM-dd HH:mm:ss)
 *
 *  @return Date
 */
+ (NSDate *)by_dateFromString:(NSString *)string dateformatter:(NSString *)dateformatter;

/**
 *  NSDate转时间戳
 *
 *  @param date          时间Date
 *  @param dateformatter 时间格式(例：yyyy-MM-dd HH:mm:ss)
 *
 *  @return 时间戳
 */
+ (NSString *)by_stringFromDate:(NSDate *)date dateformatter:(NSString *)dateformatter;

/** 计算时间差 */
+ (NSInteger )by_startTime:(NSDate *)startTime endTime:(NSDate *)endTime;

+ (NSDateComponents *)getCurrentDateComponents;

/**
 获取当前月份

 @return 月份
 */
+ (NSInteger)getCurrentMonth;

/**
 获取月份对应的天数

 @param month 月份
 @return 天数
 */
+ (NSInteger)getDayNumInMonth:(NSInteger)month;

/** 获取当前时间戳 */
+ (NSString *)getCurrentTimeStr;

/**
 获取当年剩余月数

 @return array
 */
+ (NSArray *)getSurplusMonth;

+ (NSString *)timeIntervalFromLastTime:(NSTimeInterval)time;

/** 转换直播简介的时间格式 */
+ (NSString *)transFromIntroTime:(NSString *)time;
@end
