//
//  UIImageView+BYExtension.h
//  BY
//
//  Created by 黄亮 on 2018/7/27.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (BYExtension)

/**
 设置color的iamge

 @param color 色值
 */
- (void)setImageColor:(UIColor *)color;

- (void)addCornerRadius:(CGFloat)radius size:(CGSize)size;

/** 添加点击手势 */
- (void)addTapGestureRecognizer:(void(^)(void))handle;
@end
