//
//  UIView+BYAttribute.m
//  BY
//
//  Created by 黄亮 on 2018/8/17.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "UIView+BYAttribute.h"

@implementation UIView (BYAttribute)

+ (instancetype)by_init{
    return [[self alloc] init];
}

@end

@implementation UILabel (BYAttribute)

- (void)setBy_font:(CGFloat)fontSize{
    self.font = [UIFont systemFontOfSize:fontSize];
}

- (void)setBy_attributedText:(NSDictionary *)attributes{
    NSString *title = attributes[@"title"];
    NSAssert(title, @"title不能为nil");
    if (!title.length) return;
    NSMutableDictionary *mutableAttributes = [NSMutableDictionary dictionaryWithDictionary:attributes];
    [mutableAttributes removeObjectForKey:@"title"];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:title attributes:mutableAttributes];
    self.attributedText = attributedString;
}

@end

@implementation UIButton (BYAttribute)

+ (instancetype)by_buttonWithCustomType{
    return [UIButton buttonWithType:UIButtonTypeCustom];
}

- (void)setBy_imageName:(NSString *)imageName forState:(UIControlState)state{
    [self setImage:[UIImage imageNamed:imageName] forState:state];
}

- (void)setBy_attributedTitle:(NSDictionary *)attributes forState:(UIControlState)state{
    NSString *title = attributes[@"title"];
    NSAssert(title, @"title不能为nil");
    if (!title.length) return;
    NSMutableDictionary *mutableAttributes = [NSMutableDictionary dictionaryWithDictionary:attributes];
    [mutableAttributes removeObjectForKey:@"title"];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:title attributes:mutableAttributes];
    [self setAttributedTitle:attributedString forState:state];
}

@end

@implementation UIImageView (BYAttribute)

- (void)by_setImageName:(NSString *)imageName{
    [self setImage:[UIImage imageNamed:imageName]];
}

@end
