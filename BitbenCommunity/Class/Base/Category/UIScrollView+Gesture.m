//
//  UIScrollView+Gesture.m
//  BY
//
//  Created by 黄亮 on 2018/9/4.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "UIScrollView+Gesture.h"

@implementation UIScrollView (Gesture)

//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
//    // 首先判断otherGestureRecognizer是不是系统pop手势
//    if ([otherGestureRecognizer.view isKindOfClass:NSClassFromString(@"UILayoutContainerView")]) {
//        // 再判断系统手势的state是began还是fail，同时判断scrollView的位置是不是正好在最左边
//        if (otherGestureRecognizer.state == UIGestureRecognizerStateBegan && self.contentOffset.x == 0) {
//            return YES;
//        }
//    }
//    return NO;
//}
//- (BOOL)panBack:(UIGestureRecognizer *)gestureRecognizer {
//    if (gestureRecognizer == self.panGestureRecognizer) {
//        UIPanGestureRecognizer *pan = (UIPanGestureRecognizer *)gestureRecognizer;
//        CGPoint point = [pan translationInView:self];
//        UIGestureRecognizerState state = gestureRecognizer.state;
//        if (UIGestureRecognizerStateBegan == state || UIGestureRecognizerStatePossible == state) {
//            CGPoint location = [gestureRecognizer locationInView:self];
//            if (point.x > 0 && location.x < 90 && self.contentOffset.x <= 0) {
//                return YES;
//            }
//        }
//    }
//    return NO;
//
//}
@end
