//
//  AppDelegate+Config.m
//  BY
//
//  Created by 黄亮 on 2018/8/23.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "AppDelegate+Config.h"
#import "BYIMManager.h"

#import "BYLiveConfig.h"
#import "BYILiveRoomController.h"
#import "BYVideoRoomController.h"
#import "BYVODPlayController.h"
#import "BYVideoLiveController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate (Config)

- (BOOL)openURLManagerWithURL:(NSURL *)url{
    if (!url) return NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 登录状态校验
//        @weakify(self);
//        [BYCommonViewController authorizeWithCompletionHandler:^(BOOL successed) {
//            @strongify(self);
//            if (!successed) return ;
            [self openSchemeWithURL:url];
//        }];
    });
    return NO;
}

- (BOOL)openSchemeWithURL:(NSURL *)url{
    NSString *sourceId = [url query];
    NSDictionary *param = [self getSchemeParam:sourceId];
//    if (![AccountModel sharedAccountModel].serverModel.server.MESSAGE_SERVER.ip.length &&
//        ![AccountModel sharedAccountModel].serverModel.server.MESSAGE_SERVER.port.length) {
//        showToastView(@"未分发", kCommonWindow);
//        return NO;
//    }
    UITabBarController *tabBarController = [BYTabbarViewController sharedController];
    UINavigationController *navigationController = [tabBarController selectedViewController];
    UIViewController *viewController = [navigationController.viewControllers lastObject];
    UIViewController *currentController;
    if ([viewController isKindOfClass:[RTContainerController class]]) {
        currentController = ((RTContainerController *)viewController).contentViewController;
    }
    else{
        currentController = viewController;
    }
    
    BY_THEME_TYPE themeType = [url.description hasPrefix:schemeAction_Live] ? BY_THEME_TYPE_LIVE : BY_THEME_TYPE_ARTICLE;
    NSString *themeId = [url.description hasPrefix:schemeAction_Live] ? nullToEmpty(param[@"recordId"]) : nullToEmpty(param[@"articleId"]);
    BY_NEWLIVE_TYPE liveType = [param[@"livetype"] integerValue];
    [DirectManager commonPushControllerWithThemeType:themeType liveType:liveType themeId:themeId];
    
    return YES;
}

- (NSDictionary *)getSchemeParam:(NSString *)param{
    NSArray *arr = [param componentsSeparatedByString:@"&"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    for (int i = 0; i < arr.count; i ++) {
        NSString *paramStr = arr[i];
        NSArray *paramArr = [paramStr componentsSeparatedByString:@"="];
        if (paramArr.count == 2) {
            [dic setObject:paramArr[1] forKey:paramArr[0]];
        }
    }
    return dic;
}


@end
