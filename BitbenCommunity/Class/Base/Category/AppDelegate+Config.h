//
//  AppDelegate+Config.h
//  BY
//
//  Created by 黄亮 on 2018/8/23.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Config)

/** scheme跳转 */
- (BOOL)openURLManagerWithURL:(NSURL *)url;
@end
