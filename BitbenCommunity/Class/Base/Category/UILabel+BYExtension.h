//
//  UILabel+BYExtension.h
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (BYExtension)






/**
 设置高亮文字属性

 @param attributedString attributedString description
 */
- (void)setTagAttributedString:(NSAttributedString *)attributedString;

/**
 设置高亮文字属性

 @param attributedString 高亮属性
 @param string label.text
 */
- (void)setTagAttributedString:(NSAttributedString *)attributedString string:(NSString *)string;

+ (instancetype)initTitle:(NSString *)title font:(CGFloat)font textColor:(UIColor *)textColor;


/**
 设置文本,并指定行间距
 
 @param text 文本内容
 @param lineSpacing 行间距
 */
-(void)setText:(NSString*)text lineSpacing:(CGFloat)lineSpacing;



-(void)hangjianjusetLabelSpace;

+(CGFloat)hangjianjugetSpaceLabelHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width limitHeight:(CGFloat)limitHeight;


@end
