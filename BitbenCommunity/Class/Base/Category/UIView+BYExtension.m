//
//  UIView+BYExtension.m
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "UIView+BYExtension.h"

static char const emptyViewKey;

@implementation UIView (BYExtension)

- (void)layerCornerRadius:(CGFloat)radius size:(CGSize)size{
//    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, size.width, size.height) byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(radius,radius)];
//    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
//    //设置大小
//    maskLayer.frame = CGRectMake(0, 0, size.width, size.height);
//    //设置图形样子
//    maskLayer.path = maskPath.CGPath;
//    self.layer.mask = maskLayer;
//    [self layoutIfNeeded];
    [self layerCornerRadius:radius borderWidth:1 fillColor:nil borderColor:nil size:size];
}

- (void)layerCornerRadius:(CGFloat)radius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor size:(CGSize)size{
    [self layerCornerRadius:radius borderWidth:borderWidth fillColor:self.backgroundColor borderColor:borderColor size:size];
}

- (void)layerCornerRadius:(CGFloat)radius borderWidth:(CGFloat)borderWidth fillColor:(UIColor *)fillColor borderColor:(UIColor *)borderColor size:(CGSize)size{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, size.width, size.height) byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(radius,radius)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    //设置大小
    maskLayer.frame = CGRectMake(0, 0, size.width, size.height);
    if (!borderColor && !fillColor) {
        maskLayer.fillColor = self.backgroundColor.CGColor;
        maskLayer.path = maskPath.CGPath;
        [self setBackgroundColor:[UIColor clearColor]];
//        [self.layer addSublayer:maskLayer];
        [self.layer insertSublayer:maskLayer atIndex:0];
        [self layoutIfNeeded];
        return;
    }
    //设置图形样子
    maskLayer.path = maskPath.CGPath;
    maskLayer.fillColor = fillColor ? fillColor.CGColor : self.backgroundColor.CGColor;
    maskLayer.strokeColor = borderColor.CGColor;
    maskLayer.lineWidth = borderWidth;
    [self setBackgroundColor:[UIColor clearColor]];
//    [self.layer addSublayer:maskLayer];
    [self.layer insertSublayer:maskLayer atIndex:0];
    [self layoutIfNeeded];
}

- (void)layerCornerRadius:(CGFloat)radius byRoundingCorners:(UIRectCorner)corners size:(CGSize)size{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, size.width, size.height) byRoundingCorners:corners cornerRadii:CGSizeMake(radius,radius)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    //设置大小
    maskLayer.frame = CGRectMake(0, 0, size.width, size.height);
    maskLayer.fillColor = self.backgroundColor.CGColor;
    //设置图形样子
    maskLayer.path = maskPath.CGPath;
    [self setBackgroundColor:[UIColor clearColor]];
    [self.layer addSublayer:maskLayer];
    [self.layer insertSublayer:maskLayer atIndex:0];
}

- (UIView *)addEmptyView:(NSString *)title{
    UIView *emptyView = objc_getAssociatedObject(self, &emptyViewKey);
    if (emptyView) {
        [emptyView removeFromSuperview];
    }
    emptyView = [[UIView alloc] init];
    objc_setAssociatedObject(self, &emptyViewKey, emptyView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self addSubview:emptyView];
    
    UIImageView *emptyImgView = [[UIImageView alloc] init];
    [emptyImgView by_setImageName:@"common_empty"];
    [emptyView addSubview:emptyImgView];
    [emptyImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(emptyImgView.image.size.width);
        make.height.mas_equalTo(emptyImgView.image.size.height);
        make.centerX.mas_equalTo(0);
    }];
    
    UILabel *emptyTitle = [UILabel by_init];
    emptyTitle.textColor = kColorRGBValue(0x353535);
    emptyTitle.textAlignment = NSTextAlignmentCenter;
    emptyTitle.font = [UIFont systemFontOfSize:15];
    emptyTitle.text = title;
    emptyTitle.numberOfLines = 0;
    [emptyView addSubview:emptyTitle];
    [emptyTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.top.mas_equalTo(emptyImgView.mas_bottom).mas_offset(35);
        make.width.mas_equalTo(emptyImgView.image.size.width - 20);
        make.height.mas_equalTo(stringGetHeight(title, 15));
    }];
    
    [emptyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(emptyImgView.image.size.width);
        make.centerX.mas_equalTo(0);
        make.centerY.mas_equalTo(0);
        make.bottom.mas_equalTo(emptyTitle.mas_bottom).mas_offset(0);
    }];
    
    return emptyView;
}

- (void)removeEmptyView{
    UIView *emptyView = objc_getAssociatedObject(self, &emptyViewKey);
    if (emptyView) {
        [emptyView removeFromSuperview];
    }
}

/** 绘制虚线 */
- (void)drawDottedLineByShapeLayer:(CGFloat)lineSapcing
                         lineWidth:(CGFloat)lineWidth
                         lineColor:(UIColor *)lineColor
                       isHorizonal:(BOOL)isHorizonal{
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    
    [shapeLayer setBounds:self.bounds];
    
    if (isHorizonal) {
        
        [shapeLayer setPosition:CGPointMake(CGRectGetWidth(self.frame) / 2, CGRectGetHeight(self.frame))];
        
    } else{
        [shapeLayer setPosition:CGPointMake(CGRectGetWidth(self.frame) / 2, CGRectGetHeight(self.frame)/2)];
    }
    
    [shapeLayer setFillColor:[UIColor clearColor].CGColor];
    //  设置虚线颜色为blackColor
    [shapeLayer setStrokeColor:lineColor.CGColor];
    //  设置虚线宽度
    if (isHorizonal) {
        [shapeLayer setLineWidth:CGRectGetHeight(self.frame)];
    } else {
        
        [shapeLayer setLineWidth:CGRectGetWidth(self.frame)];
    }
    [shapeLayer setLineJoin:kCALineJoinRound];
    //  设置线宽，线间距
    [shapeLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:lineWidth], [NSNumber numberWithInt:lineSapcing], nil]];
    //  设置路径
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 0, 0);
    
    if (isHorizonal) {
        CGPathAddLineToPoint(path, NULL,CGRectGetWidth(self.frame), 0);
    } else {
        CGPathAddLineToPoint(path, NULL, 0, CGRectGetHeight(self.frame));
    }
    
    [shapeLayer setPath:path];
    CGPathRelease(path);
    //  把绘制好的虚线添加上来
    [self.layer addSublayer:shapeLayer];
}

@end
