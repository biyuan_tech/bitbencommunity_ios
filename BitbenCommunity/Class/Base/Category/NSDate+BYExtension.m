//
//  NSDate+BYExtension.m
//  BY
//
//  Created by 黄亮 on 2018/8/11.
//  Copyright © 2018年 BY. All rights reserved.
//

#import "NSDate+BYExtension.h"

@implementation NSDate (BYExtension)

+ (id)shareManager{
    static NSDate *data;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        data = [[NSDate alloc] init];
    });
    return data;
}

/**
 *  获获取当前客户端的逻辑日历
 *
 *  @return 当前客户端的逻辑日历
 */
+ (NSCalendar *) currentCalendar {
    static NSCalendar *sharedCalendar = nil;
    if (!sharedCalendar) {
        sharedCalendar = [NSCalendar autoupdatingCurrentCalendar];
    }
    return sharedCalendar;
}

- (NSDateFormatter *)by_getNSDateFormatter{
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
    });
    return formatter;
}

+ (NSDate *)by_date{
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: date];
    NSDate *localeDate = [date  dateByAddingTimeInterval: interval];
    return localeDate;
}

+ (NSDate *)by_dateFromString:(NSString *)string dateformatter:(NSString *)dateformatter{
    NSDateFormatter *formatter = [[NSDate shareManager] by_getNSDateFormatter];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [formatter setDateFormat:string.length == 16 ? @"yyyy-MM-dd HH:mm" : dateformatter];
    NSDate *date = [formatter dateFromString:string];
    return date;
}

+ (NSString *)by_stringFromDate:(NSDate *)date dateformatter:(NSString *)dateformatter{
    NSDateFormatter *formatter = [[NSDate shareManager] by_getNSDateFormatter];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [formatter setDateFormat:dateformatter];
    NSString *dateString = [formatter stringFromDate:date];
    return dateString;
}

+ (NSInteger )by_startTime:(NSDate *)startTime endTime:(NSDate *)endTime{
    // 2.创建日历
    NSInteger timeDifference = 0;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yy-MM-dd HH:mm:ss"];
   
    NSTimeInterval oldTime = [startTime timeIntervalSince1970];
    NSTimeInterval newTime = [endTime timeIntervalSince1970];
    timeDifference = newTime - oldTime;
    return timeDifference;
}

+ (NSDateComponents *)getCurrentDateComponents{
    NSCalendar *calendar = [NSDate currentCalendar];
    NSDate *date = [NSDate date];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date];
    return components;
}

+ (NSInteger)getCurrentMonth{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *date = [NSDate date];
    NSDateComponents *components = [calendar components:NSCalendarUnitMonth fromDate:date];
    return components.month;
}

+ (NSInteger)getDayNumInMonth:(NSInteger)month{
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *components = [calender components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    NSDate *date = [NSDate by_dateFromString:[NSString stringWithFormat:@"%d-%d-01",(int)components.year,(int)month] dateformatter:@"yyyy-MM-dd"];
    NSRange range = [calender rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    return range.length;
}

+ (NSString *)getCurrentTimeStr{
    NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
    NSTimeInterval time=[date timeIntervalSince1970]*1000;// *1000 是精确到毫秒，不乘就是精确到秒
    NSString *timeString = [NSString stringWithFormat:@"%.0f", time];
    return timeString;
}

+ (NSArray *)getSurplusMonth{
    NSInteger currentMonth = [NSDate getCurrentMonth];
    NSMutableArray *allMonth = [NSMutableArray array];
    for (int i = 1; i < 13; i++) {
        [allMonth addObject:@(i)];
    }
    NSRange range = NSMakeRange(0, currentMonth - 1);
    [allMonth removeObjectsInRange:range];
    return allMonth;
}

+ (NSString *)timeIntervalFromLastTime:(NSTimeInterval)time{
    
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:time];
    NSDate *currentDate = [NSDate by_date];
    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:date];
//    NSInteger timeInterval = [currentDate timeIntervalSinceReferenceDate] - [date timeIntervalSinceReferenceDate];
//    timeInterval = - timeInterval;
    long temp = 0;
    NSString *result;
    if (timeInterval < 60) {
        result = [NSString stringWithFormat:@"刚刚"];
    }
    else if(timeInterval <= 60*60){
        int mins = timeInterval/60;
        result = [NSString stringWithFormat:@"%d分钟前",mins];
    }
    else if((temp = timeInterval/(60*60)) <= 24){
        NSString *currentData_ymd = [NSDate by_stringFromDate:currentDate dateformatter:@"yyyy-MM-dd"];
        NSString *date_ymd = [NSDate by_stringFromDate:date dateformatter:@"yyyy-MM-dd"];
        if ([currentData_ymd isEqualToString:date_ymd]) { // 当天
            result = [NSString stringWithFormat:@"今天 %@",[NSDate by_stringFromDate:date dateformatter:@"HH:mm"]];
        }
        else{
            result = [NSString stringWithFormat:@"昨天 %@",[NSDate by_stringFromDate:date dateformatter:@"HH:mm"]];
        }
//        result = [NSString stringWithFormat:@"%ld小时前",temp];
    }
    else{
        NSString *currentData_y = [NSDate by_stringFromDate:currentDate dateformatter:@"yyyy"];
        NSString *date_y = [NSDate by_stringFromDate:date dateformatter:@"yyyy"];
        if ([currentData_y isEqualToString:date_y]) {
            result = [NSString stringWithFormat:@"%@",[NSDate by_stringFromDate:date dateformatter:@"MM月dd日"]];
        }
        else{
            result = [NSString stringWithFormat:@"%@",[NSDate by_stringFromDate:date dateformatter:@"yyyy-MM-dd"]];
        }
    }
   
    return  result;
}

+ (NSString *)transFromIntroTime:(NSString *)time{
    NSArray *weakdays = @[[NSNull null],@"周日",@"周一",@"周二",@"周三",@"周四",@"周五",@"周六",];
    NSDate *date = [NSDate by_dateFromString:time dateformatter:K_D_F];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    calendar.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneForSecondsFromGMT:8];
    [calendar setTimeZone: timeZone];
    NSDateComponents *components = [calendar components:NSCalendarUnitWeekday fromDate:date];
    NSString *timeStr = [NSDate by_stringFromDate:date dateformatter:@"yyyy-MM-dd HH:mm"];
    return [NSString stringWithFormat:@"%@ %@",weakdays[components.weekday],timeStr];
}
@end
