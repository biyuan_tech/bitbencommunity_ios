//
//  NSString+BYExtension.m
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "NSString+BYExtension.h"

@implementation NSString (BYExtension)

- (NSString *)removeBothEndsEmptyString:(BYRemoveType )removeType{
    NSString *string = self;
    if (removeType == BYRemoveTypeBothEndsSpace) { // 移除两端空格
        while ([string hasPrefix:@" "]) {
            string = [string removeHeadString];
        }
        while ([string hasSuffix:@" "]) {
            string = [string removeBottomString];
        }
    }
    else if (removeType == BYRemoveTypeBothEndsWrap){ // 移除两端换行
        while ([string hasPrefix:@"\n"] ||
               [string hasPrefix:@"\r"]) {
            string = [string removeHeadString];
        }
        while ([string hasSuffix:@"\n"] ||
               [string hasSuffix:@"\r"]) {
            string = [string removeBottomString];
        }
    }
    else if (removeType == BYRemoveTypeBothEndsSpaceWithWrap) { // 移除两端空格换行
        while ([string hasPrefix:@" "] ||
               [string hasPrefix:@"\n"] ||
               [string hasPrefix:@"\r"]) {
            string = [string removeHeadString];
        }
        while ([string hasSuffix:@" "] ||
               [string hasSuffix:@"\n"] ||
               [string hasSuffix:@"\r"]) {
            string = [string removeBottomString];
        }
    }
    else if (removeType == BYRemoveTypeAll) { // 移除所有空格换行
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    }
    return string;
}

// 移除头部指定string
- (NSString *)removeHeadString{
    NSString *string = self;
    NSInteger length = 0;
    if ([string hasPrefix:@" "]) {
        length = [NSString stringWithFormat:@" "].length;
    }
    else if ([string hasPrefix:@"\n"]){
        length = [NSString stringWithFormat:@"\n"].length;
    }
    else if ([string hasPrefix:@"\r"]){
        length = [NSString stringWithFormat:@"\r"].length;
    }
//    NSInteger length = [string hasPrefix:@" "] ? 1 : 2;
    if (string.length > length) {
        string = [string substringFromIndex:length];
    }
    else
    {
        return @"";
    }
    return string;
}

// 移除尾部指定string
- (NSString *)removeBottomString{
    NSString *string = self;
    NSInteger length = 0;
    if ([string hasSuffix:@" "]) {
        length = [NSString stringWithFormat:@" "].length;
    }
    else if ([string hasSuffix:@"\n"]){
        length = [NSString stringWithFormat:@"\n"].length;
    }
    else if ([string hasSuffix:@"\r"]){
        length = [NSString stringWithFormat:@"\r"].length;
    }
//    NSInteger length = [string hasSuffix:@" "] ? 1 : 2;
    if (string.length > length) {
        string = [string substringWithRange:NSMakeRange(0, string.length - length)];
    }
    else
    {
        return @"";
    }
    return string;
}

- (CGSize )getStringSizeWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth{
    NSDictionary *attributes = @{NSFontAttributeName:font};
    return [self getStringSizeWithAttributes:attributes maxSize:CGSizeMake(maxWidth, MAXFLOAT)];
}

- (CGSize )getStringSizeWithAttributes:(NSDictionary<NSAttributedStringKey, id> *)attrs maxSize:(CGSize)maxSize{
    CGSize size;
    if (kCommonGetSystemVersion() >= 7) {
        size = [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attrs context:nil].size;
    }
    else{
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        size = [self sizeWithFont:attrs[NSFontAttributeName] constrainedToSize:maxSize lineBreakMode:NSLineBreakByWordWrapping];
    }
    return size;
}

- (NSMutableAttributedString *)tagKeyWordsStr:(NSString *)keyWord
                                   attributes:(NSDictionary<NSString *, id> *)attrs{
    // 设置标签文字
    NSMutableAttributedString *attrituteString = [[NSMutableAttributedString alloc] initWithString:nullToEmpty(self)];
    
    // 获取标红的位置和长度
    NSRange range = [self rangeOfString:nullToEmpty(keyWord)];
    
    // 设置标签文字的属性
    [attrituteString setAttributes:attrs range:range];
    
    // 显示在Label上
    return attrituteString;
}

- (NSString *)formatIntegerNum:(NSInteger)num{
    if (num > 9999) {
        CGFloat numF = num/1000.0;
        return [NSString stringWithFormat:@"%.f万",numF];
    }
    return [NSString stringWithFormat:@"%i",(int)num];
}

- (NSString *)formatFilePath{
    if (!self.length) return @"";
    if ([self hasPrefix:@"file://"]) {
        NSString *string = [self stringByReplacingOccurrencesOfString:@"file://" withString:@""];
        return string;
    }
    return self;
}

- (NSString *)getChannelIdForm:(NSString *)url{
    NSRange range = [url rangeOfString:@"_"];
    NSRange range1 = [url rangeOfString:@".m3u8"];
    NSInteger begin = range.location + range.length;
    NSString *channelId = [url substringWithRange:NSMakeRange(begin, range1.location - begin)];
    return channelId;
}

- (NSString *)getStreamId{
    NSRange range = [self rangeOfString:@"_"];
    NSRange range1 = [self rangeOfString:@".m3u8"];
    NSInteger begin = range.location + range.length;
    NSString *channelId = [self substringWithRange:NSMakeRange(begin, range1.location - begin)];
    return channelId;
}

- (NSString *)transBBTNum{
    return self;
}

+ (NSString *)transformIntegerShow:(NSInteger)integer{
    if (integer < 10000) {
        return stringFormatInteger(integer);
    }
    return [NSString stringWithFormat:@"%.2f万",(CGFloat)(integer/10000.0)];
}

+ (NSString *)transformIntergerShowDouble:(NSInteger)integer{
    if (integer < 10000) {
         return [NSString stringWithFormat:@"%.2f",(CGFloat)integer];
     }
    return [NSString stringWithFormat:@"%.2f万",(CGFloat)(integer/10000.0)];
}

- (NSString *)getFullImageUploadUrl{
    NSString *url;
    if ([self hasPrefix:@"http://"] || [self hasPrefix:@"https://"]){
        url = self;
    }else{
        NSString *baseURL = [PDImageView getUploadBucket:self];
        url = [NSString stringWithFormat:@"%@%@",baseURL,self];
    }
    return url;
}

- (NSString *)formatIntro{
    if (nullToEmpty(self).length) {
        return self;
    }
    return @"这个人很懒，什么也没留下";
}

- (BOOL)isDecimal
{
    if (self.length<=0) {
        return NO;
    }
    NSString *numberRegex = @"^[1-9]+(\\.[0-9]{2})?$";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegex];
    return [numberTest evaluateWithObject:self];
}

-(BOOL)isValidateEmail {
    //    if(!TTIsStringWithAnyText(self)) {
    //        return NO;
    //    }
    if (self.length<=0) {
        return NO;
    }
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

-(BOOL)isMobileNumber {
    
    /**
     
     *
     手机号码
     
     *
     移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     
     *
     联通：130,131,132,152,155,156,185,186
     
     *
     电信：133,1349,153,180,189
     
     */
    
    NSString*
    MOBILE=
    @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    
    /**
     *
     中国移动：China Mobile
     
     *
     134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     
     */
    
    //    NSString*
    //    CM=
    //    @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    //
    //    /**
    //
    //     *
    //     中国联通：China Unicom
    //
    //     *
    //     130,131,132,152,155,156,185,186
    //
    //     */
    //
    //    NSString*
    //    CU=
    //    @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    //
    //    /**
    //
    //     *
    //     中国电信：China Telecom
    //
    //     *
    //     133,1349,153,180,189
    //
    //     */
    //
    //    NSString*
    //    CT=
    //    @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    
    NSPredicate* regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    
    if(([regextestmobile evaluateWithObject:self] == YES))
        
    {
        return YES;
    }
    else
        
    {
        return NO;
    }
    
}

-(BOOL)isPhoneNum {
    /**
     
     *
     大陆地区固话及小灵通
     
     *
     区号：010,020,021,022,023,024,025,027,028,029
     
     *
     号码：七位或八位
     
     */
    
    //
    NSString * PHS = @"^(0(10|2[0-5789]|\\d{3})){0,4}-?\\d{7,8}$";
    NSPredicate* regextestphone = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", PHS];
    return [regextestphone evaluateWithObject:self];
}
#pragma mark -- 获取系统当前时间的字符串格式 格式例如yyyy-MM-dd HH:mm:ss:SSS
+ (NSString *)stringDateByFormatString:(NSString *)formatString {
    NSDateFormatter * dateFromatter=[[NSDateFormatter alloc] init];
    [dateFromatter setTimeStyle:NSDateFormatterShortStyle];
    if(formatString!=nil) {
        [dateFromatter setDateFormat:formatString];
    }
    NSString * strDate=[dateFromatter stringFromDate:[NSDate date]];
    return strDate;
}

#pragma mark - 把date转成字符串yyyy-MM-dd
+(NSString *)changeEnglishStringFromDate:(NSDate *)pickerDate
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    //    dateFormatter
    //    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:pickerDate];
}
#pragma mark - 把date转成字符串yyyy年MM月dd日
+(NSString *)changeIntoStringFromDate:(NSDate *)pickerDate
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    //    dateFormatter
    //    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
    return [dateFormatter stringFromDate:pickerDate];
}
#pragma mark - 把date转成字符串MM月dd日
+(NSString *)changeMMDDStringFromDate:(NSDate *)pickerDate
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    //    dateFormatter
    [dateFormatter setDateFormat:@"MM月dd日"];
    return [dateFormatter stringFromDate:pickerDate];
}
#pragma mark - 把date转成字符串YYYYorMMorDD 1.yyyy;2.MM;3.dd.
+(NSString *)changeYYYYorMMorDDStringFromDate:(NSDate *)pickerDate DateStyle:(NSInteger)style
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    //    dateFormatter
    if (style == 1) {
        [dateFormatter setDateFormat:@"yyyy"];
    } else if (style == 2) {
        [dateFormatter setDateFormat:@"MM"];
    } else {
        [dateFormatter setDateFormat:@"dd"];
    }
    
    return [dateFormatter stringFromDate:pickerDate];
}
#pragma mark - 根据label的宽度计算字体高度的方法
+(CGFloat)heightFromString:(NSString *)string withLabelWidth:(CGFloat)width andLabelFont:(UIFont *)yourFont{
    // 计算字体尺寸
    NSMutableDictionary *att = [NSMutableDictionary dictionary];
    att[NSFontAttributeName] = yourFont;
    CGSize textSize = [string boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:att context:nil].size;
    return textSize.height;
}
#pragma mark - 根据label的宽度和最大高度计算字体高度的方法
+(CGFloat)heightFromString:(NSString *)string LabelWidth:(CGFloat)width maxHeight:(CGFloat)maxH LabelFont:(UIFont *)yourFont{
    // 计算字体尺寸
    NSMutableDictionary *att = [NSMutableDictionary dictionary];
    att[NSFontAttributeName] = yourFont;
    CGSize textSize = [string boundingRectWithSize:CGSizeMake(width, maxH) options:NSStringDrawingUsesLineFragmentOrigin attributes:att context:nil].size;
    return textSize.height;
}
#pragma mark - 根据label的高度计算字体宽度的方法
+(CGFloat)widthFromString:(NSString *)string withLabelHeight:(CGFloat)height andLabelFont:(UIFont *)yourFont{
    // 计算字体尺寸
    NSMutableDictionary *att = [NSMutableDictionary dictionary];
    att[NSFontAttributeName] = yourFont;
    CGSize textSize = [string boundingRectWithSize:CGSizeMake(MAXFLOAT, height) options:NSStringDrawingUsesLineFragmentOrigin attributes:att context:nil].size;
    return textSize.width;
}
#pragma mark 转换时间戳
+ (NSString *)changeResultWithTimestamp:(NSString *)timestamp{
    NSTimeInterval timeInterval_since = [timestamp doubleValue];
    NSDate *date=[NSDate dateWithTimeIntervalSince1970:timeInterval_since];
    NSTimeInterval  timeInterval = [date timeIntervalSinceNow];
    timeInterval = -timeInterval;
    long temp = 0;
    NSString *result;
    if (timeInterval < 60) {
        result = [NSString stringWithFormat:@"刚刚"];
    }
    else if((temp = timeInterval/60) <60){
        result =[NSString stringWithFormat:@"%ld分钟前",temp];
    }
    
    else if((temp = temp/60) <24){
        result = [NSString stringWithFormat:@"%ld小时前",temp];
    }
    
    else if((temp = temp/24) <30){
        result = [NSString stringWithFormat:@"%ld天前",temp];
    }
    
    else if((temp = temp/30) <12){
        result = [NSString stringWithFormat:@"%ld月前",temp];
    }
    else{
        temp = temp/12;
        result = [NSString stringWithFormat:@"%ld年前",temp];
    }
    return result;
}
#pragma mark 转换时间戳(包含3种格式)
+ (NSString *)changeResultFromTimestamp:(NSString *)timestamp{
    NSTimeInterval timeInterval_since = [timestamp doubleValue];
    NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:timeInterval_since];
    NSTimeInterval  timeInterval = [detaildate timeIntervalSinceNow];
    timeInterval = -timeInterval;
    
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    long temp = 0;
    NSString *result;
    if (timeInterval < 60) {
        result = [NSString stringWithFormat:@"刚刚"];
    }
    else if ((temp = timeInterval/60) <60){
        result =[NSString stringWithFormat:@"%ld分钟前",temp];
    }
    
    else if ((temp = temp/60) <24){
        result = [NSString stringWithFormat:@"%ld小时前",temp];
    }
    
    else if ((temp = temp/24) <30){
        //        result = [NSString stringWithFormat:@"%ld天前",temp];
        [dateFormatter setDateFormat:@"MM-dd HH:mm"];
        result = [dateFormatter stringFromDate: detaildate];
    }
    
    else if ((temp = temp/30) <12){
        //        result = [NSString stringWithFormat:@"%ld月前",temp];
        [dateFormatter setDateFormat:@"MM-dd HH:mm"];
        result = [dateFormatter stringFromDate: detaildate];
    }
    else {
        temp = temp/12;
        //        result = [NSString stringWithFormat:@"%ld年前",temp];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        result = [dateFormatter stringFromDate: detaildate];
    }
    return result;
}
#pragma mark 转换时间戳格式yyyy-MM-dd HH:mm:ss
+ (NSString *)timeFormatted:(NSString *)totalSeconds
{
    NSTimeInterval time=[totalSeconds doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *currentDateStr = [dateFormatter stringFromDate: detaildate];
    return currentDateStr;
    
}
#pragma mark 转换时间戳格式yyyy-MM-dd HH:mm
+ (NSString *)timeToFormatted:(NSString *)totalSeconds
{
    NSTimeInterval time=[totalSeconds doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSString *currentDateStr = [dateFormatter stringFromDate: detaildate];
    return currentDateStr;
    
}
#pragma mark 转换时间戳格式MM月dd日 HH时mm分
+ (NSString *)timeFormattedInChiese:(NSString *)totalSeconds
{
    NSTimeInterval time=[totalSeconds doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"MM月dd日HH时mm分"];
    
    NSString *currentDateStr = [dateFormatter stringFromDate: detaildate];
    return currentDateStr;
    
}
#pragma mark 转换时间戳格式yyyy-MM-dd
+ (NSString *)timeFormattedToyyyyMMdd:(NSString *)totalSeconds
{
    NSTimeInterval time = [totalSeconds doubleValue];
    NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
    
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *currentDateStr = [dateFormatter stringFromDate: detaildate];
    return currentDateStr;
    
}
#pragma mark 转换时间戳格式yyyy年MM月dd日
+ (NSString *)timeFormattedToChieseyyyyMMdd:(NSString *)totalSeconds
{
    NSTimeInterval time = [totalSeconds doubleValue];
    NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
    
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
    
    NSString *currentDateStr = [dateFormatter stringFromDate: detaildate];
    return currentDateStr;
    
}
#pragma mark 转换时间戳格式MM月dd日
+ (NSString *)timeFormattedToMMdd:(NSString *)totalSeconds
{
    NSTimeInterval time = [totalSeconds doubleValue];
    NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
    
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"MM月dd日"];
    
    NSString *currentDateStr = [dateFormatter stringFromDate: detaildate];
    return currentDateStr;
    
}

#pragma mark 转换时间戳格式HHmmss(从现在开始算起)
+ (NSString *)timeFormattedFromNowToHHmmss:(NSString *)totalSeconds
{
    NSTimeInterval time = [totalSeconds doubleValue];
    NSDate *detaildate = [NSDate dateWithTimeIntervalSinceNow:time];
    
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"HHmmss"];
    
    NSString *currentDateStr = [dateFormatter stringFromDate: detaildate];
    
    return currentDateStr;
    
}
#pragma mark 转换时间戳判断是否今天
+ (NSString *)changeResultIntoTimestamp:(NSString *)timestamp{
    NSTimeInterval timeInterval_since = [timestamp doubleValue];
    NSDate *date=[NSDate dateWithTimeIntervalSince1970:timeInterval_since];
    
    NSTimeInterval  timeInterval = [date timeIntervalSinceNow];
    timeInterval = -timeInterval;
    long temp = 0;
    NSString *result;
    if (timeInterval < 60) {
        result = [NSString stringWithFormat:@"刚刚"];
        return result;
    }
    if((temp = timeInterval/60) <60){
        result =[NSString stringWithFormat:@"%ld分钟前",temp];
        return result;
    }
    
    if((temp = temp/60) <24){
        return result = [NSString stringWithFormat:@"%ld小时前",temp];
    }
    
    //    NSTimeInterval time=[timestamp doubleValue];
    //    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *currentDateStr = [dateFormatter stringFromDate: date];
    return currentDateStr;
    
}

- (NSString *)stringLocalizedStringFromNumber {
    if(![self isKindOfClass:[NSString class]]){
        return self;
    }
    float oldf = [self floatValue];
    long long oldll = [self longLongValue];
    float tmptf = oldf - oldll;
    NSString *currencyStr = nil;
    if(tmptf > 0){
        currencyStr = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithFloat:oldll]
                                                       numberStyle:NSNumberFormatterDecimalStyle];
    }else{
        currencyStr = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithLongLong:oldll]
                                                       numberStyle:NSNumberFormatterDecimalStyle];
    }
    return currencyStr;
}



+ (NSString *)translation:(NSString *)arebic
{
    if (!arebic.length) return @"三";
    
    NSString *str = arebic;
    NSArray *arabic_numerals = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"0"];
    NSArray *chinese_numerals = @[@"一",@"二",@"三",@"四",@"五",@"六",@"七",@"八",@"九",@"零"];
    NSArray *digits = @[@"个",@"十",@"百",@"千",@"万",@"十",@"百",@"千",@"亿",@"十",@"百",@"千",@"兆"];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:chinese_numerals forKeys:arabic_numerals];
    
    NSMutableArray *sums = [NSMutableArray array];
    for (int i = 0; i < str.length; i ++) {
        NSString *substr = [str substringWithRange:NSMakeRange(i, 1)];
        NSString *a = [dictionary objectForKey:substr];
        NSString *b = digits[str.length -i-1];
        NSString *sum = [a stringByAppendingString:b];
        if ([a isEqualToString:chinese_numerals[9]])
        {
            if([b isEqualToString:digits[4]] || [b isEqualToString:digits[8]])
            {
                sum = b;
                if ([[sums lastObject] isEqualToString:chinese_numerals[9]])
                {
                    [sums removeLastObject];
                }
            }else
            {
                sum = chinese_numerals[9];
            }
            
            if ([[sums lastObject] isEqualToString:sum])
            {
                continue;
            }
        }
        
        [sums addObject:sum];
    }
    
    NSString *sumStr = [sums  componentsJoinedByString:@""];
    NSString *chinese = [sumStr substringToIndex:sumStr.length-1];
    
    return chinese;
}
#pragma mark 把字典和数组转换成json字符串
+(NSString *)stringTOjson:(id)temps
{
    NSData* jsonData =[NSJSONSerialization dataWithJSONObject:temps options:NSJSONWritingPrettyPrinted error:nil];
    NSString *strs=[[NSString alloc] initWithData:jsonData
                                         encoding:NSUTF8StringEncoding];
    return strs;
}
#pragma mark 把字典和数组转换成kNilOptions格式json字符串
+(NSString *)stringTOkNilOptionsjson:(id)temps
{
    NSData* jsonData =[NSJSONSerialization dataWithJSONObject:temps options:kNilOptions error:nil];
    NSString *strs=[[NSString alloc] initWithData:jsonData
                                         encoding:NSUTF8StringEncoding];
    return strs;
}

- (BOOL)includeChinese{
    for(int i=0; i< [self length];i++)
    {
        int a =[self characterAtIndex:i];
        if( a >0x4e00&& a <0x9fff){
            return YES;
        }
    }
    return NO;
}

- (BOOL)verifyIsNum{
    if (self.length == 0) {
        return NO;
    }
    NSString *regex = @"[0-9]*";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    if ([pred evaluateWithObject:self]) {
        return YES;
    }
    return NO;
}

- (NSString *)removeH5Tag{
    NSString *string = self;
    NSScanner * scanner = [NSScanner scannerWithString:string];
    NSString * text = nil;
    while([scanner isAtEnd]==NO)
    {
        //找到标签的起始位置
        [scanner scanUpToString:@"<" intoString:nil];
        //找到标签的结束位置
        [scanner scanUpToString:@">" intoString:&text];
        //替换字符
        string = [string stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>",text] withString:@""];
    }
    return string;
 
}
@end
