//
//  UILabel+BYExtension.m
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import "UILabel+BYExtension.h"


@implementation UILabel (BYExtension)

- (void)setTagAttributedString:(NSAttributedString *)attributedString{
    NSString *string = self.text;
    NSRange rang = [string rangeOfString:attributedString.string];
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:self.text attributes:@{NSFontAttributeName:self.font,NSForegroundColorAttributeName:self.textColor}];
    NSRange tagRang = NSMakeRange(0, attributedString.string.length);
    NSDictionary *tagAttributedStringKey = [attributedString attributesAtIndex:0 effectiveRange:&tagRang];
    [mutableAttributedString addAttributes:tagAttributedStringKey range:rang];
    self.attributedText = mutableAttributedString;
}

- (void)setTagAttributedString:(NSAttributedString *)attributedString string:(NSString *)string{
    if (!string.length) return;
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName:self.font,NSForegroundColorAttributeName:self.textColor}];
    if (!attributedString) {
        self.attributedText = mutableAttributedString;
        return;
    }
    NSRange rang = [string rangeOfString:attributedString.string];
    NSRange tagRang = NSMakeRange(0, attributedString.string.length);
    NSDictionary *tagAttributedStringKey = [attributedString attributesAtIndex:0 effectiveRange:&tagRang];
    [mutableAttributedString addAttributes:tagAttributedStringKey range:rang];
    self.attributedText = mutableAttributedString;
}

+ (instancetype)initTitle:(NSString *)title font:(CGFloat)font textColor:(UIColor *)textColor{
    UILabel *label = [[UILabel alloc] init];
    label.text = nullToEmpty(title);
    label.font = [UIFont systemFontOfSize:font];
    label.textColor = textColor;
    return label;
}

-(void)setText:(NSString*)text lineSpacing:(CGFloat)lineSpacing {
    if (!text || lineSpacing < 0.01) {
        self.text = text;
        return;
    }
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:lineSpacing];        //设置行间距
    [paragraphStyle setLineBreakMode:self.lineBreakMode];
    [paragraphStyle setAlignment:self.textAlignment];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [text length])];
    self.attributedText = attributedString;
}




-(void)hangjianjusetLabelSpace{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = LCFloat(6); //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:self.font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f};
    
    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:self.text attributes:dic];
    self.attributedText = attributeStr;
    self.lineBreakMode = NSLineBreakByTruncatingTail;

}


+(CGFloat)hangjianjugetSpaceLabelHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width limitHeight:(CGFloat)limitHeight{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = LCFloat(6);
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f
};
    
    CGSize size = [str boundingRectWithSize:CGSizeMake(width,limitHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return size.height;
}

@end
