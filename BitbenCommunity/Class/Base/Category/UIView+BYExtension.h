//
//  UIView+BYExtension.h
//  BY
//
//  Created by 黄亮 on 2018/8/9.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BYExtension)

/**
 绘制圆角
 
 @param radius 圆角弧度
 */
- (void)layerCornerRadius:(CGFloat)radius size:(CGSize)size;

- (void)layerCornerRadius:(CGFloat)radius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor size:(CGSize)size;

- (void)layerCornerRadius:(CGFloat)radius borderWidth:(CGFloat)borderWidth fillColor:(UIColor *)fillColor borderColor:(UIColor *)borderColor size:(CGSize)size;

- (void)layerCornerRadius:(CGFloat)radius byRoundingCorners:(UIRectCorner)corners size:(CGSize)size;

/** 添加空view */
- (UIView *)addEmptyView:(NSString *)title;
- (void)removeEmptyView;

/** 绘制虚线 */
- (void)drawDottedLineByShapeLayer:(CGFloat)lineSapcing
                         lineWidth:(CGFloat)lineWidth
                         lineColor:(UIColor *)lineColor
                       isHorizonal:(BOOL)isHorizonal;


@end
