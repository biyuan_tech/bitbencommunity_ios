//
//  UIImage+BYExtension.h
//  BY
//
//  Created by 黄亮 on 2018/8/4.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Accelerate/Accelerate.h>

@interface UIImage (BYExtension)

/**
 返回色值image

 @param color 色值
 @return return value description
 */
+ (UIImage *)imageWithColor:(UIColor *)color;

/**
 图片裁剪

 @param size 裁剪大小
 @return 裁剪后的image
 */
- (UIImage *)cutImageSize:(CGSize)size;

/** 图片增加圆角 */
- (UIImage *)addCornerRadius:(CGFloat)radius size:(CGSize)size;
- (void )addCornerRadius:(CGFloat)radius size:(CGSize)size cb:(void(^)(UIImage *image))cb;

/**
 限制图片大小

 @param maxLength 例：1M = 1024*1024
 @return data
 */
- (NSData *)compressWithMaxLength:(NSUInteger)maxLength;

/**  图片尺寸根据最大宽高压缩 */
- (UIImage *)resizeImageToMaxWidthOrHeight:(CGFloat)maxValue;

/** 添加高斯模糊 */
+(UIImage *)boxblurImage:(UIImage *)image withBlurNumber:(CGFloat)blur;
@end
