//
//  UITextView+BYExtension.h
//  BY
//
//  Created by 黄亮 on 2018/9/11.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (BYExtension)

@property (nonatomic ,copy) NSString *by_placeholder;

- (void)registerNSNotification;

@end
