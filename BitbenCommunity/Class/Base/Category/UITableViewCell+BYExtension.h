//
//  UITableViewCell+BYExtension.h
//  BY
//
//  Created by 黄亮 on 2018/8/14.
//  Copyright © 2018年 BY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (BYExtension)

@property (nonatomic ,weak) UITableView *tableView;

@end
