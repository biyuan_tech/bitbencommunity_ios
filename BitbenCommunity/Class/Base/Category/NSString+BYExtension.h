//
//  NSString+BYExtension.h
//  BY
//
//  Created by 黄亮 on 2018/7/26.
//  Copyright © 2018年 Belief. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,BYRemoveType) {
    BYRemoveTypeBothEndsSpace = 0,             // 只移除两端空格
    BYRemoveTypeBothEndsWrap,              // 只移除两端换行
    BYRemoveTypeBothEndsSpaceWithWrap,    // 只移除两端的空格和换行
    BYRemoveTypeAll                       // 移除文案中的所有空格与换行
};

@interface NSString (BYExtension)

/**
 移除字符村两端的空格
 
 @return string
 */
- (NSString *)removeBothEndsEmptyString:(BYRemoveType )removeType;

/**
 计算string展示的Size
 
 @param font 字体大小
 @param maxWidth 展示的最大宽度
 @return 计算的Size
 */
- (CGSize )getStringSizeWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth;

/** 计算string展示的Size */
- (CGSize )getStringSizeWithAttributes:(NSDictionary<NSAttributedStringKey, id> *)attrs maxSize:(CGSize)maxSize;

/** 字符串高亮 */
- (NSMutableAttributedString *)tagKeyWordsStr:(NSString *)keyWord
                                   attributes:(NSDictionary<NSString *, id> *)attrs;

/**
 格式化integer返回相对于的x万、x千、x百

 @param num 输入
 @return 格式化格式
 */
- (NSString *)formatIntegerNum:(NSInteger)num;

/** 格式化file:// 开头的文件路径(移除file:) */
- (NSString *)formatFilePath;

/** 截取互动直播channel id */
- (NSString *)getChannelIdForm:(NSString *)url;
- (NSString *)getStreamId;

/** bbt奖励显示转换 */
- (NSString *)transBBTNum;
/** 个十百千转换显示格式 */
+ (NSString *)transformIntegerShow:(NSInteger)integer;
/** 固定显示小数点两位 */
+ (NSString *)transformIntergerShowDouble:(NSInteger)integer;


/** 获取完整的下载地址 */
- (NSString *)getFullImageUploadUrl;

- (NSString *)formatIntro;

- (BOOL)isDecimal;

-(BOOL)isValidateEmail;
/**是否手机号*/
-(BOOL)isMobileNumber;
/**是否固话*/
-(BOOL)isPhoneNum;


/** 获取系统当前时间的字符串格式 格式例如yyyy-MM-dd HH:mm:ss:SSS */
+ (NSString *)stringDateByFormatString:(NSString *)formatString;
/**
 *把date转成字符串yyyy-MM-dd
 */
+(NSString *)changeEnglishStringFromDate:(NSDate *)pickerDate;
/**
 *把date转成字符串中文yyyy年MM月dd日
 */
+(NSString *)changeIntoStringFromDate:(NSDate *)pickerDate;
/**
 *把date转成字符串中文MM月dd日
 */
+(NSString *)changeMMDDStringFromDate:(NSDate *)pickerDate;

/**把date转成字符串YYYYorMMorDD 1.yyyy;2.MM;3.dd.*/
+(NSString *)changeYYYYorMMorDDStringFromDate:(NSDate *)pickerDate DateStyle:(NSInteger)style;

/**
 *转换时间戳格式%ld小时前
 */
+ (NSString *)changeResultWithTimestamp:(NSString *)timestamp;

///转换时间戳(包含3种格式)
+ (NSString *)changeResultFromTimestamp:(NSString *)timestamp;

/***转换时间戳格式yyyy-MM-dd HH:mm:ss*/
+ (NSString *)timeFormatted:(NSString *)totalSeconds;

/***转换时间戳格式yyyy-MM-dd HH:mm*/
+ (NSString *)timeToFormatted:(NSString *)totalSeconds;

/** 转换时间戳格式yyyy-MM-dd */
+ (NSString *)timeFormattedToyyyyMMdd:(NSString *)totalSeconds;

/** 转换时间戳中文格式MM月dd日 */
+ (NSString *)timeFormattedToMMdd:(NSString *)totalSeconds;
/** 转换时间戳中文格式MM月dd日 HH时mm分*/
+ (NSString *)timeFormattedInChiese:(NSString *)totalSeconds;
/** 转换时间戳中文格式yyyy年MM月dd日*/
+ (NSString *)timeFormattedToChieseyyyyMMdd:(NSString *)totalSeconds;
/** 转换时间(从现在开始)戳格式HHmmss*/
+ (NSString *)timeFormattedFromNowToHHmmss:(NSString *)totalSeconds;
/**
 *判断是否今天转换时间戳格式yyyy-MM-dd-刚刚之类
 */
+ (NSString *)changeResultIntoTimestamp:(NSString *)timestamp;
/**
 *计算字体宽度的方法
 */
+(CGFloat)widthFromString:(NSString *)string withLabelHeight:(CGFloat)height andLabelFont:(UIFont *)yourFont;

/**
 *计算字体高度的方法
 */
+(CGFloat)heightFromString:(NSString *)string withLabelWidth:(CGFloat)width andLabelFont:(UIFont *)yourFont;
///根据label的宽度和最大高度计算字体高度的方法
+(CGFloat)heightFromString:(NSString *)string LabelWidth:(CGFloat)width maxHeight:(CGFloat)maxH LabelFont:(UIFont *)yourFont;

/// 数字格式化为每隔三位用逗号隔开
- (NSString *)stringLocalizedStringFromNumber;

/**
 *   阿拉伯数字转化为中文数字
 *
 *  @param arebic 阿拉伯数字
 *
 *  @return 中文数字
 */
+ (NSString *)translation:(NSString *)arebic;
///把字典和数组转换成json字符串
+(NSString *)stringTOjson:(id)temps;
///把字典和数组转换成kNilOptions格式json字符串
+(NSString *)stringTOkNilOptionsjson:(id)temps;

/** 是否包含汉子 */
- (BOOL)includeChinese;

/** 校验是否为纯数字 */
- (BOOL)verifyIsNum;

/** 移除h5内标签*/
- (NSString *)removeH5Tag;
@end
