
//
//  BYVideoRPCDefine.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/2.
//  Copyright © 2019 币源网络. All rights reserved.
//

#ifndef BYVideoRPCDefine_h
#define BYVideoRPCDefine_h

/** 新版视频列表 */
static NSString * RPC_new_video_list = @"live/new_video_list";

#pragma mark - 观点首页

/** 观点首页 */
static NSString * RPC_article_homepage = @"article/article_homepage";

/** 分页查询我收藏的直播或文章 */
static NSString * RPC_applet_page_collection = @"live/applet_page_collection";

#endif /* BYVideoRPCDefine_h */
