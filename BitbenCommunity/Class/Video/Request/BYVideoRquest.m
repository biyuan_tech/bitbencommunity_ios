//
//  BYVideoRequest.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/2.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYVideoRequest.h"
#import "BYVideoHomeModel.h"
@implementation BYVideoRequest

#pragma mark - home
- (void)loadRequest:(BY_VIDEO_SORT_TYPE)sortType
              topic:(NSString *)topic
            pageNum:(NSInteger)pageNum
       successBlock:(void(^)(id object))successBlock
         faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *params = @{@"type":@(sortType),
                             @"topic":nullToEmpty(topic),
                             @"page_number":@(pageNum),
                             @"page_size":@(10)
                             };
    [BYRequestManager ansyRequestWithURLString:RPC_video_live parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYVideoHomeModel getTableData:result[@"list"]];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

@end
