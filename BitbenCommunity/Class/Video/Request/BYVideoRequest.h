//
//  BYVideoRequest.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/2.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYRequestManager.h"
#import "BYVideoRPCDefine.h"

typedef NS_ENUM(NSInteger,BY_VIDEO_SORT_TYPE) {
    BY_VIDEO_SORT_TYPE_RECOMMEND = 0, // 推荐
    BY_VIDEO_SORT_TYPE_HOT,           // 热门
    BY_VIDEO_SORT_TYPE_TIME,          // 新鲜
};

typedef NS_ENUM(NSInteger,BY_VIDEO_TYPE) {
    BY_VIDEO_TYPE_LIVE = 0,     // 直播
    BY_VIDEO_TYPE_VIDEO,    // 视频
};

typedef NS_ENUM(NSInteger,BY_ARTICLE_SORT_TYPE) {
    BY_ARTICLE_SORT_TYPE_HOT = 0,           // 热门
    BY_ARTICLE_SORT_TYPE_SOFTTEXT,          // 微文
    BY_ARTICLE_SORT_TYPE_TIME,              // 新鲜
};

NS_ASSUME_NONNULL_BEGIN

@interface BYVideoRequest : BYRequestManager

#pragma mark - 首页

// 视频首页(sort_field : score 热门排序 datetime 新鲜排序)
- (void)loadHomeRequest:(NSString *)sort_field
                pageNum:(NSInteger)pageNum
           successBlock:(void(^)(id object))successBlock
             faileBlock:(void(^)(NSError *error))faileBlock;


- (void)loadVideoHomeRequest:(BY_NEWLIVE_TYPE)type
                       topic:(NSString *)topic
                     pageNum:(NSInteger)pageNum
                successBlock:(void(^)(id object))successBlock
                  faileBlock:(void(^)(NSError *error))faileBlock;



#pragma mark - 文章首页

/** 获取排行榜 */
- (void)loadRequestGetArticleRank:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock;

/** 文章首页 */
- (void)loadRequestGetArticle:(BY_ARTICLE_SORT_TYPE)sortType
                        topic:(NSString *)topic
                      pageNum:(NSInteger)pageNum
                 successBlock:(void(^)(id object))successBlock
                   faileBlock:(void(^)(NSError *error))faileBlock;

/** 分页查询我收藏的直播或文章 */
- (void)loadRequestGetCollectionData:(NSInteger)pageNum
                        successBlock:(void(^)(id object))successBlock
                          faileBlock:(void(^)(NSError *error))faileBlock;

@end

NS_ASSUME_NONNULL_END
