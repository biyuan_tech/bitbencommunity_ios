//
//  BYVideoRequest.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/2.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYVideoRequest.h"
#import "BYVideoHomeModel.h"

#import "BYArticleRootRankingModel.h"
#import "BYHomeArticleModel.h"
@implementation BYVideoRequest

#pragma mark - home

- (void)loadHomeRequest:(NSString *)sort_field
                pageNum:(NSInteger)pageNum
           successBlock:(void(^)(id object))successBlock
             faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *params = @{@"type":@(1),
                             @"topic":@"",
                             @"page_number":@(pageNum),
                             @"page_size":@(10),
                             @"sort_field":nullToEmpty(sort_field)
                             };
    [BYRequestManager ansyRequestWithURLString:RPC_new_video_list parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYVideoHomeModel getTableData:result];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

- (void)loadVideoHomeRequest:(BY_NEWLIVE_TYPE)type
                       topic:(NSString *)topic
                     pageNum:(NSInteger)pageNum
                successBlock:(void(^)(id object))successBlock
                  faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *params = @{@"type":@(0),
                             @"live_type":@(type),
                             @"topic":nullToEmpty(topic),
                             @"page_number":@(pageNum),
                             @"page_size":@(10)
                             };
    [BYRequestManager ansyRequestWithURLString:RPC_new_video_list parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYVideoHomeModel getVideoTableData:result[@"content"]];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}


#pragma mark - 观点首页

/** 排行榜 */
- (void)loadRequestGetArticleRank:(void(^)(id object))successBlock
                       faileBlock:(void(^)(NSError *error))faileBlock{
    [BYRequestManager ansyRequestWithURLString:get_article_rank parameters:nil operationType:BYOperationTypePost successBlock:^(id result) {
        BYArticleRootRankingModel *model = [BYArticleRootRankingModel getRankingData:result[@"rank_list"]];
        if (successBlock) successBlock(model);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 文章首页 */
- (void)loadRequestGetArticle:(BY_ARTICLE_SORT_TYPE)sortType
                        topic:(NSString *)topic
                      pageNum:(NSInteger)pageNum
                 successBlock:(void(^)(id object))successBlock
                   faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *params = @{@"type":@(sortType),
                             @"topic":nullToEmpty(topic),
                             @"page_number":@(pageNum),
                             @"page_size":@(10)
                             };
    [BYRequestManager ansyRequestWithURLString:RPC_article_homepage parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYHomeArticleModel getTopicCellData:result[@"result"] topData:@[] pageNum:0];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}

/** 分页查询我收藏的直播或文章 */
- (void)loadRequestGetCollectionData:(NSInteger)pageNum
                        successBlock:(void(^)(id object))successBlock
                          faileBlock:(void(^)(NSError *error))faileBlock{
    NSDictionary *params = @{@"page_number":@(pageNum),
                             @"page_size":@(10),
                             @"theme_type":@(2)
                             };
    [BYRequestManager ansyRequestWithURLString:RPC_applet_page_collection parameters:params operationType:BYOperationTypePost successBlock:^(id result) {
        NSArray *data = [BYHomeArticleModel getTopicCellData:result[@"content"] topData:@[] pageNum:pageNum];
        if (successBlock) successBlock(data);
    } failureBlock:^(NSError *error) {
        if (faileBlock) faileBlock(error);
    }];
}
@end
