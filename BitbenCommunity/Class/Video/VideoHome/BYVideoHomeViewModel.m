//
//  BYVideoHomeViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/2.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYVideoHomeViewModel.h"
#import "BYHomeSearchController.h"
#import "BYPersonHomeController.h"
#import "ShareRootViewController.h"
#import "BYTopicDetailController.h"
#import "BYVideoHomeSubController.h"
#import "BYVideoHomeLiveSubController.h"

#import "BYVideoHomeCell.h"
#import "BYVideoHomeTopicSelView.h"
#import "BYHomeSearchNavigationView.h"
#import "HGSegmentedPageViewController.h"
#import "HGCenterBaseTableView.h"


#import "BYVideoHomeModel.h"

@interface BYVideoHomeViewModel ()<HGSegmentedPageViewControllerDelegate, HGPageViewControllerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    // 话题
    NSString *_topic;
}
/** segmentView */
@property (nonatomic ,strong) UIView *segmentView;
/** 下拉话题按钮 */
@property (nonatomic ,strong) UIButton *downBtn;
/** selectView */
//@property (nonatomic ,strong) UIView *selectView;
/** selectLab */
@property (nonatomic ,strong) UILabel *selectLab;
/** topicView */
@property (nonatomic ,strong) BYVideoHomeTopicSelView *topicSelView;
/** searchView */
@property (nonatomic ,strong) BYHomeSearchNavigationView *searchView;
/** tableView */
@property (nonatomic ,strong) HGCenterBaseTableView *tableView;
/** segmentControllView */
@property (nonatomic ,strong) HGSegmentedPageViewController *segmentedPageViewController;
/** 是否可滑动 */
@property (nonatomic ,assign) BOOL cannotScroll;
/** tableFooterView */
@property (nonatomic ,strong) UIView *tableFooterView;
///** 直播 */
//@property (nonatomic ,strong) BYVideoHomeSubController *liveController;
///** 视频 */
//@property (nonatomic ,strong) BYVideoHomeSubController *videoController;
/** 全部 */
@property (nonatomic ,strong) BYVideoHomeLiveSubController *allController;
/** 微直播 */
@property (nonatomic ,strong) BYVideoHomeLiveSubController *microController;
/** 个人互动直播 */
@property (nonatomic ,strong) BYVideoHomeLiveSubController *iliveController;
/** 推流直播 */
@property (nonatomic ,strong) BYVideoHomeLiveSubController *videoController;


@end

@implementation BYVideoHomeViewModel


- (void)setContentView{
    _topic = nil;
    [self addSearchView];
    [self addTableView];
}

- (void)viewWillAppear{
    [self.searchView reloadRedDot];
}

#pragma mark - custom

- (void)autoReload{
    //    [self.tableView beginRefreshing];
    [self.segmentedPageViewController.currentPageViewController autoReload];
}

- (void)selectTopic:(NSString *)topic{
    _topic = topic;
    _selectLab.text = topic.length ? topic : _selectLab.text;
    _selectLab.hidden = topic.length ? NO : YES;
    [_selectLab layoutIfNeeded];
    self.allController.topic = _topic;
    self.microController.topic = _topic;
    self.iliveController.topic = _topic;
    self.videoController.topic = _topic;
    
    self.downBtn.selected = topic.length ? YES : NO;
   
}


#pragma mark - action


// 删除选择话题
- (void)delBtnAction:(UIButton *)sender{
    _topic = @"";
    _selectLab.hidden = YES;
    [self.topicSelView reset];
    self.allController.topic = _topic;
    self.microController.topic = _topic;
    self.iliveController.topic = _topic;
    self.videoController.topic = _topic;
}

// 展开条件视图
- (void)downBtnAction{
    if (!self.topicSelView.isShow) {
        [self.segmentedPageViewController.view bringSubviewToFront:self.segmentedPageViewController.categoryView];
        [self.topicSelView showAnimationInView:self.segmentedPageViewController.view];
        [self.segmentedPageViewController.view insertSubview:self.topicSelView atIndex:1];
    }else{
        [self.topicSelView hiddenAnimation];
    }
}

// 下拉图片动画
- (void)downBtnAnimation:(BOOL)isOpen{
    [UIView beginAnimations:@"animation" context:nil];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    CGFloat angle = isOpen ? M_PI : 0;
    self.downBtn.transform = CGAffineTransformMakeRotation(angle);
    [UIView commitAnimations];
}

#pragma mark - BYCommonTableViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat contentOffsetY = scrollView.contentOffset.y;
    CGFloat criticalPointOffsetY = 0;
    
    //利用contentOffset处理内外层scrollView的滑动冲突问题
    if (contentOffsetY >= criticalPointOffsetY) {
        
        self.cannotScroll = YES;
        scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        [self.segmentedPageViewController.currentPageViewController makePageViewControllerScroll:YES];
    } else {
        
        if (self.cannotScroll) {
            //“维持吸顶状态”
            scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        } else {
            /* 吸顶状态 -> 不吸顶状态
             * categoryView的子控制器的tableView或collectionView在竖直方向上的contentOffsetY小于等于0时，会通过代理的方式改变当前控制器self.canScroll的值；
             */
        }
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    [self.segmentedPageViewController.currentPageViewController makePageViewControllerScrollToTop];
    return YES;
}

#pragma mark - HGSegmentedPageViewControllerDelegate
- (void)segmentedPageViewControllerWillBeginDragging {
    self.tableView.scrollEnabled = NO;
}

- (void)segmentedPageViewControllerDidEndDragging {
    self.tableView.scrollEnabled = YES;
}

#pragma mark - HGPageViewControllerDelegate
- (void)pageViewControllerLeaveTop {
    self.cannotScroll = NO;
}

#pragma mark - configUI
- (void)addSearchView{
    self.searchView = [[BYHomeSearchNavigationView alloc] init];
    [S_V_VIEW addSubview:self.searchView];
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Top(60));
    }];
}

- (void)addTableView{
    HGCenterBaseTableView *tableView = [[HGCenterBaseTableView alloc] init];
    tableView.categoryViewHeight = 50;
    tableView.bounces = NO;
    tableView.delegate = self;
    tableView.tableFooterView = self.tableFooterView;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.showsHorizontalScrollIndicator = NO;
    [S_V_VIEW addSubview:tableView];
    [S_V_VIEW sendSubviewToBack:tableView];
    self.tableView = tableView;
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(kSafe_Mas_Top(60), 0, 0, 0));
    }];
}

- (void)addSelectView{
//    UIView *selectView = [UIView by_init];
//    [selectView setBackgroundColor:[UIColor whiteColor]];
//    selectView.hidden = YES;
//    [self.segmentedPageViewController.categoryView addSubview:selectView];
//    selectView.layer.cornerRadius = 2.0f;
//    self.selectView = selectView;
    
    CGFloat fontSize = iPhone5 ? 12 : 14;
    CGFloat width = 25;
    UILabel *selectLab = [UILabel by_init];
    selectLab.text = @"快讯";
    selectLab.hidden = YES;
    [selectLab setBy_font:fontSize];
    selectLab.textColor = kColorRGBValue(0xea6441);
    [self.segmentedPageViewController.categoryView addSubview:selectLab];
    self.selectLab = selectLab;
    [selectLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-33);
        make.top.mas_equalTo(12);
        make.height.mas_equalTo(26);
        make.width.mas_greaterThanOrEqualTo(width);
    }];
    
//    UIButton *delBtn = [UIButton by_buttonWithCustomType];
//    [delBtn setBy_imageName:@"videohome_del_icon" forState:UIControlStateNormal];
//    [delBtn addTarget:self action:@selector(delBtnAction:) forControlEvents:UIControlEventTouchUpInside];
//    [selectView addSubview:delBtn];
//    [delBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(selectLab.mas_right).mas_offset(5);
//        make.top.bottom.right.mas_equalTo(0);
//    }];
    
//    [selectView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(-33);
//        make.top.mas_equalTo(12);
//        make.height.mas_equalTo(26);
//        make.width.mas_greaterThanOrEqualTo(25);
//    }];
    
    // 下拉按钮
    UIButton *downBtn = [UIButton by_buttonWithCustomType];
    [downBtn setBy_imageName:@"videohome_down" forState:UIControlStateNormal];
    [downBtn setBy_imageName:@"videohome_down_highlight" forState:UIControlStateSelected];
    [downBtn addTarget:self action:@selector(downBtnAction) forControlEvents:UIControlEventTouchUpInside];
    downBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5);
    [self.segmentedPageViewController.categoryView addSubview:downBtn];
    self.downBtn = downBtn;
    [downBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(33);
        make.height.mas_equalTo(27);
        make.top.mas_equalTo(15);
        make.right.mas_equalTo(0);
    }];
    
}


- (BYVideoHomeTopicSelView *)topicSelView{
    if (!_topicSelView) {
        _topicSelView = [[BYVideoHomeTopicSelView alloc] init];
        _topicSelView.animationY = 60;
        @weakify(self);
        _topicSelView.didSelectTopicHandle = ^(NSString * _Nonnull topic) {
            @strongify(self);
            [self selectTopic:topic];
        };
        _topicSelView.showStatusHandle = ^(BOOL isShow) {
            @strongify(self);
            [self downBtnAnimation:isShow];
        };
    }
    return _topicSelView;
}

- (UIView *)tableFooterView{
    if (!_tableFooterView) {
        _tableFooterView = [[UIView alloc] init];
        _tableFooterView.frame = CGRectMake(0, 0, kCommonScreenWidth, kCommonScreenHeight - kSafe_Mas_Top(60 + 49));
        [S_VC addChildViewController:self.segmentedPageViewController];
        [_tableFooterView addSubview:self.segmentedPageViewController.view];
        [self.segmentedPageViewController didMoveToParentViewController:S_VC];
        @weakify(self);
        [self.segmentedPageViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.edges.equalTo(self.tableFooterView);
        }];
        [self addSelectView];
    }
    return _tableFooterView;
}

- (HGSegmentedPageViewController *)segmentedPageViewController {
    if (!_segmentedPageViewController) {
        NSMutableArray *controllers = [NSMutableArray array];
        NSArray *titles = @[@"全部",@"微直播", @"个人互动", @"推流直播"];
        for (int i = 0; i < titles.count; i++) {
            HGPageViewController *controller;
            if (i == 0) {
                controller = self.allController;
            } else if (i == 1) {
                controller = self.microController;
            } else if (i == 2) {
                controller = self.iliveController;
            } else if (i == 3) {
                controller = self.videoController;
            }
            controller.delegate = self;
            [controllers addObject:controller];
        }
        _segmentedPageViewController = [[HGSegmentedPageViewController alloc] init];
        _segmentedPageViewController.pageViewControllers = controllers;
        _segmentedPageViewController.categoryView.titles = titles;
        _segmentedPageViewController.categoryView.height = 50;
        _segmentedPageViewController.categoryView.alignment = HGCategoryViewAlignmentLeft;
        _segmentedPageViewController.categoryView.originalIndex = 0;
        _segmentedPageViewController.categoryView.leftAndRightMargin = 15.0f;
        _segmentedPageViewController.categoryView.itemSpacing = iPhone5 ? 0 : 10;
        _segmentedPageViewController.categoryView.titleNomalFont = [UIFont systemFontOfSize:iPhone5 ? 12 : 14];
        _segmentedPageViewController.categoryView.titleSelectedFont = [UIFont boldSystemFontOfSize:iPhone5 ? 14 : 16];
        _segmentedPageViewController.categoryView.titleNormalColor = kColorRGBValue(0x8f8f8f);
        _segmentedPageViewController.categoryView.titleSelectedColor = kColorRGBValue(0x323232);
        _segmentedPageViewController.categoryView.backgroundColor = [UIColor whiteColor];
        _segmentedPageViewController.categoryView.vernierHeight = 3;
        _segmentedPageViewController.categoryView.vernierWidth = 20;
        _segmentedPageViewController.categoryView.vernierBottomSapce = 6.0f;
        _segmentedPageViewController.categoryView.vernier.backgroundColor = kColorRGBValue(0xea6441);
        _segmentedPageViewController.categoryView.topBorder.hidden = YES;
        _segmentedPageViewController.categoryView.bottomBorder.hidden = YES;
        _segmentedPageViewController.delegate = self;
    }
    return _segmentedPageViewController;
}

- (BYVideoHomeLiveSubController *)allController{
    if (!_allController) {
        _allController = [[BYVideoHomeLiveSubController alloc] init];
        _allController.type = -1;
        [S_VC addChildViewController:_allController];
    }
    return _allController;
}

- (BYVideoHomeLiveSubController *)microController{
    if (!_microController) {
        _microController = [[BYVideoHomeLiveSubController alloc] init];
        _microController.type = 0;
        [S_VC addChildViewController:_microController];
    }
    return _microController;
}

- (BYVideoHomeLiveSubController *)iliveController{
    if (!_iliveController) {
        _iliveController = [[BYVideoHomeLiveSubController alloc] init];
        _iliveController.type = BY_NEWLIVE_TYPE_ILIVE;
        [S_VC addChildViewController:_iliveController];
    }
    return _iliveController;
}

- (BYVideoHomeLiveSubController *)videoController{
    if (!_videoController) {
        _videoController = [[BYVideoHomeLiveSubController alloc] init];
        _videoController.type = BY_NEWLIVE_TYPE_VIDEO;
        [S_VC addChildViewController:_videoController];
    }
    return _videoController;
}

@end
