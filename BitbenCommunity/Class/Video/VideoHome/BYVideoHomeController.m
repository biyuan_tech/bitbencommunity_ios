//
//  BYVideoHomeController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/2.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYVideoHomeController.h"
#import "BYVideoHomeViewModel.h"

@interface BYVideoHomeController ()

@end

@implementation BYVideoHomeController

- (Class)getViewModelClass{
    return [BYVideoHomeViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)autoReload{
    [(BYVideoHomeViewModel *)self.viewModel autoReload];
}

@end
