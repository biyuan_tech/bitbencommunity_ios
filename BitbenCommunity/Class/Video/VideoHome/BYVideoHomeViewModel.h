//
//  BYVideoHomeViewModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/2.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYVideoHomeViewModel : BYCommonViewModel

- (void)autoReload;

@end

NS_ASSUME_NONNULL_END
