//
//  BYVideoHomeTopicSelView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/5.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYVideoHomeTopicSelView : UIView

/** 选择回调 */
@property (nonatomic ,copy) void (^didSelectTopicHandle)(NSString *topic);
/** 隐现状态回调 */
@property (nonatomic ,copy) void (^showStatusHandle)(BOOL isShow);
/** 显示状态 */
@property (nonatomic ,assign ,readonly) BOOL isShow;
/** 起始动画y轴 */
@property (nonatomic ,assign) CGFloat animationY;


- (void)showAnimationInView:(UIView *)view;

- (void)hiddenAnimation;

- (void)reset;
@end

NS_ASSUME_NONNULL_END
