//
//  BYVideoHomeLiveSubController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/31.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYVideoHomeLiveSubController.h"
#import "BYTopicDetailController.h"

#import "BYVideoHomeModel.h"

@interface BYVideoHomeLiveSubController ()<BYCommonTableViewDelegate>

/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;

@end

@implementation BYVideoHomeLiveSubController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTableView];
    [self loadRequest];
}

#pragma mark - custom
- (void)autoReload{
    [self.tableView beginRefreshing];
    [self reloadData];
}

- (void)reloadData{
    _pageNum = 0;
    [self loadRequest];
}

- (void)loadMoreData{
    [self loadRequest];
}

- (void)setTopic:(NSString *)topic{
    _topic = topic;
    [self reloadData];
}

// 跳转话题详情
- (void)pushTopicDetailController:(NSString *)topic{
    if (!topic.length) return;
    BYTopicDetailController *topicDetailController = [[BYTopicDetailController alloc] init];
    topicDetailController.topic = topic;
    BYCommonViewController *currentController = (BYCommonViewController *)[BYTabbarViewController sharedController].currentController;
    [currentController authorizePush:topicDetailController animation:YES];
}

#pragma mark - BYCommonTableViewDelegate
- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [UIView by_init];
    [sectionView setBackgroundColor:[UIColor clearColor]];
    return sectionView;
}

- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForHeaderInSection:(NSInteger)section{
    return 10.0f;
}

- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = tableView.tableData[indexPath.section];
    BYVideoHomeModel *model = dic.allValues[0][indexPath.row];
    [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_LIVE liveType:model.live_type themeId:model.live_record_id];
}
#pragma mark - request
- (void)loadRequest{
    @weakify(self);
    [[BYVideoRequest alloc] loadVideoHomeRequest:_type topic:_topic pageNum:_pageNum successBlock:^(id  _Nonnull object) {
        @strongify(self);
        [self.tableView endRefreshing];
        [self.tableView removeEmptyView];
        if (self.pageNum == 0) {
            self.tableView.tableData = object;
            if (![object count]) {
                [self.tableView addEmptyView:@"暂无内容"];
            }
        }else{
            if (![object count]) return ;
            NSMutableArray *data = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [data addObjectsFromArray:object];
            self.tableView.tableData = data;
        }
        self.pageNum ++;
    } faileBlock:^(NSError * _Nonnull error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}


#pragma mark - configUI
- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    [self.tableView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.tableView addHeaderRefreshTarget:self action:@selector(reloadData)];
    [self.tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}


@end
