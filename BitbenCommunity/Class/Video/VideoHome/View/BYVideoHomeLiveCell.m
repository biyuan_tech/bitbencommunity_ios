//
//  BYVideoHomeLiveCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/31.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYVideoHomeLiveCell.h"
#import <YYLabel.h>
#import <SDWebImage/FLAnimatedImageView+WebCache.h>
#import "BYVideoHomeModel.h"

@interface BYVideoHomeLiveCell ()

/** 直播封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 直播状态 */
@property (nonatomic ,strong) UILabel *liveStatusLab;
/** 直播状态背景 */
@property (nonatomic ,strong) UIView *liveStatusBg;
/** 标题 */
@property (nonatomic ,strong) UILabel *titleLab;
/** 观看次数 */
@property (nonatomic ,strong) UILabel *watchNumLab;
/** bbt数 */
@property (nonatomic ,strong) UILabel *bbtNumLab;
/** 时间 */
@property (nonatomic ,strong) UILabel *timeLab;
/** 直播类型 */
@property (nonatomic ,strong) YYLabel *liveTypeLab;
/** 直播中动画 */
@property (nonatomic ,strong) FLAnimatedImageView *livIngImgView;


@end

@implementation BYVideoHomeLiveCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYVideoHomeModel *model = dic.allValues[0][indexPath.row];
    
    [_coverImgView uploadHDImageWithURL:model.live_cover_url callback:nil];
    _watchNumLab.text = [NSString stringWithFormat:@"%@人气值",[NSString transformIntegerShow:model.watch_times]];
    _titleLab.text = model.live_title;
    _timeLab.text = model.begin_time;
    _bbtNumLab.text = model.reward;
    
    [self reloadLiveStatus:model.status liveType:model.live_type];

}

- (void)reloadLiveStatus:(BY_LIVE_STATUS)status liveType:(BY_NEWLIVE_TYPE)liveType{
    if (liveType == BY_NEWLIVE_TYPE_VOD_AUDIO ||
        liveType == BY_NEWLIVE_TYPE_VOD_VIDEO) {
        _liveStatusBg.backgroundColor = [UIColor clearColor];
        _liveStatusLab.text = @"";
        @weakify(self);
        [_liveStatusLab mas_updateConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.width.mas_equalTo(0);
            make.height.mas_equalTo(stringGetHeight(self.liveStatusLab.text, 11) + 4);
        }];
        return;
    }
    switch (liveType) {
        case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
        case BY_NEWLIVE_TYPE_AUDIO_PPT:
            self.liveTypeLab.text = @"微直播";
            break;
        case BY_NEWLIVE_TYPE_ILIVE:
            self.liveTypeLab.text = @"个人互动";
            break;
        case BY_NEWLIVE_TYPE_VIDEO:
            self.liveTypeLab.text = @"推流直播";
            break;
        default:
            break;
    }
    switch (status) {
        case BY_LIVE_STATUS_LIVING:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xea6441);
            _liveStatusLab.text = @"直播中";
            break;
        case BY_LIVE_STATUS_SOON:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xffbe21);
            _liveStatusLab.text = @"直播预告";
            break;
        case BY_LIVE_STATUS_END:
        case BY_LIVE_STATUS_OVERTIME:
            _liveStatusBg.backgroundColor = kColorRGBValue(0x4165ea);
            _liveStatusLab.text = @"直播回放";
            break;
        case BY_LIVE_STATUS_NOTLIVE_ONTIME:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xf28d73);
            _liveStatusLab.text = @"暂未开播";
            break;
        default:
            break;
    }
    
    CGFloat labOriginX = status == BY_LIVE_STATUS_LIVING ? 35 : 25;
    @weakify(self);
    [_liveStatusLab mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(labOriginX);
        make.width.mas_equalTo(stringGetWidth(self.liveStatusLab.text, 11) + 10);
        make.height.mas_equalTo(stringGetHeight(self.liveStatusLab.text, 11) + 4);
    }];
    
    CGFloat bgWidth = status == BY_LIVE_STATUS_LIVING ? stringGetWidth(self.liveStatusLab.text, 11) + 18 : stringGetWidth(self.liveStatusLab.text, 11) + 10;
    [_liveStatusBg mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(bgWidth);
    }];
    
    self.livIngImgView.hidden = status == BY_LIVE_STATUS_LIVING ? NO : YES;
    
    [_liveTypeLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stringGetWidth(self.liveTypeLab.text, 11) + 8);
    }];
}

- (void)setContentView{
    // 封面图
    PDImageView *coverImgView = [[PDImageView alloc] init];
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.clipsToBounds = YES;
    coverImgView.layer.cornerRadius = 6.0;
    [self.contentView addSubview:coverImgView];
    _coverImgView = coverImgView;
    CGFloat coverH = (kCommonScreenWidth - 30)*180/345;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(10);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(coverH);
    }];
    
    [self addLiveStatusView];

    // 直播人数
    UILabel *watchNumLab = [UILabel by_init];
    [watchNumLab setBy_font:10];
    watchNumLab.textColor = [UIColor whiteColor];
    watchNumLab.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:watchNumLab];
    self.watchNumLab = watchNumLab;
    [watchNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-25);
        make.top.mas_equalTo(22);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(watchNumLab.font.pointSize);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    titleLab.font = [UIFont boldSystemFontOfSize:16];
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    self.titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(coverImgView.mas_bottom).offset(13);
        make.height.mas_equalTo(titleLab.font.pointSize);
    }];
    
    UILabel *timeLab = [UILabel by_init];
    timeLab.font = [UIFont systemFontOfSize:13];
    timeLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:timeLab];
    self.timeLab = timeLab;
    [timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(coverImgView.mas_bottom).offset(42);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(timeLab.font.pointSize);
    }];
    
    YYLabel *liveTypeLab = [[YYLabel alloc] init];
    liveTypeLab.font = [UIFont systemFontOfSize:11];
    liveTypeLab.textColor = kColorRGBValue(0x8f8f8f);
    liveTypeLab.backgroundColor = kColorRGBValue(0xf2f4f5);
    liveTypeLab.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:liveTypeLab];
    self.liveTypeLab = liveTypeLab;
    [liveTypeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(timeLab);
        make.left.mas_equalTo(timeLab.mas_right).offset(15);
        make.height.mas_equalTo(19);
        make.width.mas_greaterThanOrEqualTo(0);
    }];
    
    UILabel *bbtNumLab = [UILabel by_init];
    [bbtNumLab setBy_font:13];
    bbtNumLab.textColor = kColorRGBValue(0x8f8f8f);
    [self.contentView addSubview:bbtNumLab];
    self.bbtNumLab = bbtNumLab;
    [bbtNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(timeLab);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(16);
    }];
    
    UIImageView *bbtIcon = [UIImageView by_init];
    [bbtIcon by_setImageName:@"common_bp"];
    [self.contentView addSubview:bbtIcon];
    [bbtIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bbtNumLab.mas_left).mas_offset(-10);
        make.centerY.mas_equalTo(bbtNumLab);
        make.width.mas_equalTo(16);
        make.height.mas_equalTo(16);
    }];
}

- (void)addLiveStatusView{
    
    UILabel *liveStatusLab = [UILabel by_init];
    liveStatusLab.text = @"直播回放";
    [liveStatusLab setBy_font:11];
    [liveStatusLab setTextColor:[UIColor whiteColor]];
    liveStatusLab.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:liveStatusLab];
    _liveStatusLab = liveStatusLab;
    [liveStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
        make.top.mas_equalTo(20);
        make.width.mas_equalTo(stringGetWidth(liveStatusLab.text, liveStatusLab.font.pointSize) + 10);
        make.height.mas_equalTo(stringGetHeight(liveStatusLab.text, liveStatusLab.font.pointSize) + 4);
    }];
    
    UIView *liveStatusBg = [UIView by_init];
    liveStatusBg.layer.cornerRadius = 2.0f;
    [self.contentView addSubview:liveStatusLab];
    [self.contentView insertSubview:liveStatusBg belowSubview:liveStatusLab];
    self.liveStatusBg = liveStatusBg;
    [liveStatusBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
        make.top.mas_equalTo(20);
        make.width.mas_equalTo(0);
        make.height.mas_equalTo(liveStatusLab.mas_height).mas_offset(0);
    }];
    
    NSString *filePath = [[NSBundle bundleWithPath:[[NSBundle mainBundle] bundlePath]] pathForResource:@"video_living_animation.gif" ofType:nil];
    NSData *imageData = [NSData dataWithContentsOfFile:filePath];
    
    FLAnimatedImageView *livIngImgView = [FLAnimatedImageView by_init];
    livIngImgView.animatedImage = [FLAnimatedImage animatedImageWithGIFData:imageData];
    livIngImgView.hidden = YES;
    [self.contentView addSubview:livIngImgView];
    self.livIngImgView = livIngImgView;
    [livIngImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.centerY.mas_equalTo(liveStatusBg);
        make.width.mas_equalTo(8);
        make.height.mas_equalTo(9);
    }];
    
//    // 标题
//    YYLabel *titleLab = [[YYLabel alloc] init];
//    titleLab.font = [UIFont systemFontOfSize:15];
//    titleLab.textColor = [UIColor whiteColor];
//    titleLab.numberOfLines = 2;
//    [self.contentView addSubview:titleLab];
//    _titleLab = titleLab;
//    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(15);
//        make.top.mas_equalTo(12);
//        make.right.mas_equalTo(-15);
//        make.height.mas_greaterThanOrEqualTo(15);
//    }];
}

@end
