//
//  BYVideoHomeSubController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYVideoHomeSubController.h"
#import "BYPersonHomeController.h"
#import "ShareRootViewController.h"
#import "BYTopicDetailController.h"

#import "BYVideoHomeCell.h"

#import "BYVideoHomeModel.h"

@interface BYVideoHomeSubController ()<BYCommonTableViewDelegate>
/** tableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** pageNum */
@property (nonatomic ,assign) NSInteger pageNum;

@end

@implementation BYVideoHomeSubController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTableView];
    [self loadRequest];
}

#pragma mark - custom
- (void)autoReload{
    [self.tableView beginRefreshing];
    [self reloadData];
}

- (void)reloadData{
    _pageNum = 0;
    [self loadRequest];
}

- (void)loadMoreData{
    [self loadRequest];
}

#pragma mark - action

- (void)shareAction:(BYVideoHomeModel *)model{
    NSString *shareTitle;
    NSString *shareContent;
    NSString *shareImgUrl;
    NSString *shareUrl;
    shareType sharetype = shareTypeLive;
    NSDictionary *params = @{@"首页直播分享":(model.room_id.length?model.room_id:@"找不到房间号")};
    [MTAManager event:MTATypeHotLiveShare params:params];
    NSString *mainImgUrl = [PDImageView appendingImgUrl:model.shareMap.share_img];
    NSString *imgurl = [mainImgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    shareTitle = model.shareMap.share_title;
    shareContent = model.shareMap.share_intro;
    shareImgUrl = imgurl;
    shareUrl = model.shareMap.share_url;
    sharetype = shareTypeLive;
    
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.collection_status = model.collection_status;
    shareViewController.transferCopyUrl = shareUrl;
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        [ShareSDKManager shareManagerWithType:shareType title:shareTitle desc:shareContent img:shareImgUrl url:shareUrl callBack:NULL];
        [ShareSDKManager shareSuccessBack:sharetype block:NULL];
    }];
    
    [shareViewController actionClickWithCollectionBlock:^{
        model.collection_status = !model.collection_status;
    }];
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    [shareViewController showInView:currentController];
}

// 顶的操作
- (void)supportAction:(BYVideoHomeModel *)model indexPath:(NSIndexPath *)indexPath suc:(void(^)(void))suc{
    if (model.isSupport) {
        showToastView(@"你已经顶过该条内容", self.view);
        return;
    }
    [self loadRequestSupport:model.live_record_id user_id:model.user_id suc:^{
        model.isSupport = YES;
        model.count_support ++;
        if (suc) suc();
    }];
}

// 跳转话题详情
- (void)pushTopicDetailController:(NSString *)topic{
    if (!topic.length) return;
    BYTopicDetailController *topicDetailController = [[BYTopicDetailController alloc] init];
    topicDetailController.topic = topic;
    BYCommonViewController *currentController = (BYCommonViewController *)[BYTabbarViewController sharedController].currentController;
    [currentController authorizePush:topicDetailController animation:YES];
}

- (void)attentionBtnAction:(NSIndexPath *)indexPath{
    NSDictionary *dic = self.tableView.tableData[indexPath.section];
    BYVideoHomeModel *model = dic.allValues[0][indexPath.row];
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:model.user_id
                                             isAttention:!model.isAttention
                                            successBlock:^(id object) {
                                                @strongify(self);
                                                model.isAttention = !model.isAttention;
                                                [self reloadAttentin:indexPath isAttention:model.isAttention];
//                                                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
                                            } faileBlock:nil];
}

- (void)reloadAttentin:(NSIndexPath *)indexPath isAttention:(BOOL)isAttention{
    NSDictionary *dic = self.tableView.tableData[indexPath.section];
    BYVideoHomeModel *model = dic.allValues[0][indexPath.row];
    [self.tableView.tableData enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BYVideoHomeModel *tmpModel = obj.allValues[0][indexPath.row];
        if ([tmpModel.user_id isEqualToString:model.user_id]) {
            tmpModel.isAttention = isAttention;
        }
    }];
    
    [self.tableView reloadData];
}

#pragma mark - BYCommonTableViewDelegate
- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [UIView by_init];
    [sectionView setBackgroundColor:[UIColor clearColor]];
    return sectionView;
}

- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForHeaderInSection:(NSInteger)section{
    return 8.0f;
}

- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = tableView.tableData[indexPath.section];
    BYVideoHomeModel *model = dic.allValues[0][indexPath.row];
    [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_LIVE liveType:model.live_type themeId:model.live_record_id];
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = tableView.tableData[indexPath.section];
    BYVideoHomeModel *model = dic.allValues[0][indexPath.row];
    BYCommonViewController *currentController = (BYCommonViewController *)[BYTabbarViewController sharedController].currentController;
    if ([actionName isEqualToString:@"headImgAction"]){ // 头像点击
        BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
        personHomeController.user_id = model.user_id;
        [currentController authorizePush:personHomeController animation:YES];
    }else if ([actionName isEqualToString:@"shareAction"]) { // 分享
        [self shareAction:model];
    }else if ([actionName isEqualToString:@"likeAction"]) { // 顶
        BYVideoHomeCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        @weakify(self);
        [currentController authorizeWithCompletionHandler:^(BOOL successed) {
            @strongify(self);
            if (!successed) return ;
            [self supportAction:model indexPath:indexPath suc:^{
                [cell reloadAttentionAnimation];
            }];
        }];
    }else if ([actionName isEqualToString:@"selectTagAction"]){ // 点击话题tag
        NSString *tag = param[@"tag"];
        [self pushTopicDetailController:tag];
    }else if ([actionName isEqualToString:@"attentionAction"]) { // 关注
        [self attentionBtnAction:indexPath];
    }
}
#pragma mark - request
- (void)loadRequest{
    @weakify(self);
    [[BYVideoRequest alloc] loadHomeRequest:self.sort_field pageNum:self.pageNum successBlock:^(id  _Nonnull object) {
        [self.tableView endRefreshing];
        [self.tableView removeEmptyView];
        if (self.pageNum == 0) {
            self.tableView.tableData = object;
            if (![object count]) {
                [self.tableView addEmptyView:@"暂无内容"];
            }
        }else{
            if (![object count]) return ;
            NSMutableArray *data = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [data addObjectsFromArray:object];
            self.tableView.tableData = data;
        }
        self.pageNum ++;
    } faileBlock:^(NSError * _Nonnull error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

// 顶
- (void)loadRequestSupport:(NSString *)theme_id user_id:(NSString *)user_id suc:(void(^)(void))suc{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestLiveGuestOpearType:BY_GUEST_OPERA_TYPE_UP theme_id:theme_id theme_type:BY_THEME_TYPE_LIVE receiver_user_id:user_id successBlock:^(id object) {
        if (suc) suc();
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"顶操作失败", self.view);
    }];
}

#pragma mark - configUI
- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    [self.tableView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.tableView addHeaderRefreshTarget:self action:@selector(reloadData)];
    [self.tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

@end
