//
//  BYVideoHomeTopicSelView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/5.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYVideoHomeTopicSelView.h"

static NSInteger kBaseTag = 0x553;
@interface BYVideoHomeTopicSelView ()

/** titles */
@property (nonatomic ,strong) NSArray *titles;
/** contentView */
@property (nonatomic ,strong) UIView *contentView;
/** lastSelBtn */
@property (nonatomic ,strong) UIButton *lastSelBtn;
/** maskView */
@property (nonatomic ,strong) UIView *maskView;
@property (nonatomic ,assign) BOOL isShow;

@end

@implementation BYVideoHomeTopicSelView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.titles = @[@"热点要闻",@"快讯",@"独家",@"小白入门",@"创投",@"技术",@"专题",@"研报",@"行情",@"安全",@"项目",@"游戏",@"知识库",@"访谈",@"通证经济",@"活动",@"政策",@"交易平台",@"DAPP",@"钱包",@"挖矿",@"脱口秀",@"比特币",@"以太坊"];
        [self setContentView];
    }
    return self;
}

- (void)didMoveToSuperview{
    [super didMoveToSuperview];
    self.frame = self.superview.bounds;
}

- (void)reset{
    _lastSelBtn.selected = NO;
    [_lastSelBtn setBackgroundColor:kColorRGBValue(0xf2f2f2)];
    _lastSelBtn = nil;
}

- (void)buttonAction:(UIButton *)sender{
    NSInteger index = sender.tag - kBaseTag;
    if (_lastSelBtn != sender) {
        _lastSelBtn.selected = NO;
        [_lastSelBtn setBackgroundColor:kColorRGBValue(0xf2f2f2)];
    }
    sender.selected = !sender.selected;
    [sender setBackgroundColor:sender.selected ? kColorRGBValue(0xea6438):kColorRGBValue(0xf2f2f2)];
    if (self.didSelectTopicHandle) {
        self.didSelectTopicHandle(_lastSelBtn == sender ? @"" : _titles[index]);
    }
    if (_lastSelBtn == sender) {
        _lastSelBtn = nil;
    }else{
        _lastSelBtn = sender;
    }
    [self hiddenAnimation];
}

- (void)showAnimationInView:(UIView *)view{
    [view addSubview:self];
    self.hidden = NO;
    self.isShow = YES;
    if (self.showStatusHandle) self.showStatusHandle(YES);
    CGFloat y = _animationY > 0 ? _animationY : kSafe_Mas_Top(110) + 8;
    [UIView animateWithDuration:0.2 animations:^{
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(y);
        }];
        self.maskView.alpha = 1.0f;
        [self layoutIfNeeded];
    }];
}

- (void)hiddenAnimation{
    self.isShow = NO;
    if (self.showStatusHandle) self.showStatusHandle(NO);
    CGFloat y = _animationY > 0 ? _animationY - 385: kSafe_Mas_Top(110) - 385;
    [UIView animateWithDuration:0.2 animations:^{
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(y);
        }];
        self.maskView.alpha = 0.0f;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
        [self removeFromSuperview];
    }];
}

- (void)setContentView{
    
    UIView *topView = [UIView by_init];
    [topView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Top(110) + 8);
    }];
    
    UIView *maskView = [UIView by_init];
    [maskView setBackgroundColor:kColorRGB(50, 50, 50, 0.47)];
    [self addSubview:maskView];
    self.maskView = maskView;
    maskView.alpha = 1.0f;
    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.top.mas_equalTo(topView.mas_bottom).mas_offset(0);
    }];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenAnimation)];
    [maskView addGestureRecognizer:tapGesture];
    
    self.contentView = [UIView by_init];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kCommonScreenWidth);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(kSafe_Mas_Top(110) - 385);
        make.height.mas_equalTo(iPhone5 ? 350 : 385);
    }];
    
    UILabel *titleLab = [UILabel by_init];
    titleLab.text = @"热门话题";
    [titleLab setBy_font:16];
    titleLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(20);
        make.width.mas_equalTo(stringGetWidth(titleLab.text, 16));
        make.height.mas_equalTo(stringGetHeight(titleLab.text, 16));
    }];
    
    CGFloat width = iPhone5 ? 65 : 74;
    CGFloat fontSize = iPhone5 ? 12 : 14;
    CGFloat height = 34;
    CGFloat spaceW = (kCommonScreenWidth -4*width - 30)/3;
    for (int i = 0; i < 6; i ++) {
        for (int j = 0; j < 4; j ++) {
            NSInteger index = 4*i + j;
            UIButton *button = [UIButton by_buttonWithCustomType];
            [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
            [button setBy_attributedTitle:@{@"title":self.titles[index],
                                            NSForegroundColorAttributeName:kColorRGBValue(0xffffff),
                                            NSFontAttributeName:[UIFont systemFontOfSize:fontSize],
                                            } forState:UIControlStateSelected];
            [button setBy_attributedTitle:@{@"title":self.titles[index],
                                            NSForegroundColorAttributeName:kColorRGBValue(0x323232),
                                            NSFontAttributeName:[UIFont systemFontOfSize:fontSize],
                                            } forState:UIControlStateNormal];
            button.layer.cornerRadius = 4.0f;
            [self.contentView addSubview:button];
            button.tag = kBaseTag + index;
            [button setBackgroundColor:kColorRGBValue(0xf2f2f2)];
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(50 + (height + 16)*i);
                make.left.mas_equalTo(15 + (width + spaceW)*j);
                make.height.mas_equalTo(height);
                make.width.mas_equalTo(width);
            }];
        }
    }
}

@end
