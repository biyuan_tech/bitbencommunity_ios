//
//  BYVideoHomeSubController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "HGPageViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYVideoHomeSubController : HGPageViewController

/** 话题 */
//@property (nonatomic ,copy) NSString *topic;
/**  */
//@property (nonatomic ,assign) BY_VIDEO_TYPE type;
/** 排序方式 */
@property (nonatomic ,copy) NSString *sort_field;


- (void)reloadData;
@end

NS_ASSUME_NONNULL_END
