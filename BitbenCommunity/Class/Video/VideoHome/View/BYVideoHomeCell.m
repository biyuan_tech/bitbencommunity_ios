//
//  BYVideoHomeCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/2.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYVideoHomeCell.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>
#import "BYVideoHomeModel.h"
#import <SDWebImage/FLAnimatedImageView+WebCache.h>


static NSInteger kBaseTag = 0x439;

@interface BYVideoHomeCell ()

/** 直播封面图 */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 直播状态 */
@property (nonatomic ,strong) UILabel *liveStatusLab;
/** 直播状态背景 */
@property (nonatomic ,strong) UIView *liveStatusBg;
/** 标题 */
@property (nonatomic ,strong) YYLabel *titleLab;
/** 头像 */
@property (nonatomic ,strong) PDImageView *userlogo;
/** 昵称 */
@property (nonatomic ,strong) UILabel *userNameLab;
/** 观看次数 */
@property (nonatomic ,strong) UILabel *watchNumLab;
/** 点赞按钮 */
@property (nonatomic ,strong) UIButton *likeBtn;
/** model */
@property (nonatomic ,strong) BYVideoHomeModel *model;
/** 直播中动画 */
@property (nonatomic ,strong) FLAnimatedImageView *livIngImgView;
/** bbt数 */
@property (nonatomic ,strong) UILabel *bbtNumLab;
/** lineView */
@property (nonatomic ,strong) UIView *lineView;
/** 关注 */
@property (nonatomic ,strong) UIButton *attentionBtn;

@end

@implementation BYVideoHomeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.clipsToBounds = YES;
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = object[indexPath.section];
    BYVideoHomeModel *model = dic.allValues[0][indexPath.row];
    self.model = model;
    self.indexPath = indexPath;
    self.userlogo.style = model.cert_badge;
    [self reloadLiveStatus:model.status liveType:model.live_type];
    self.titleLab.attributedText = model.attributedString;
    [_coverImgView uploadHDImageWithURL:model.live_cover_url callback:nil];
    [_userlogo uploadHDImageWithURL:model.head_img callback:nil];
    _userNameLab.text = model.nickname;
    self.bbtNumLab.text = model.reward;
    
    [_titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(model.titleSize.height);
    }];
    
    self.likeBtn.selected = model.isSupport;
    for (int i = 1; i < 3; i ++) {
        UIButton *btn = [self.contentView viewWithTag:kBaseTag + i];
        NSString *title;
        if (i == 1) {
            title = [NSString transformIntegerShow:model.comment_count];
        }else{
            title = [NSString transformIntegerShow:model.count_support];
        }
        [btn setBy_attributedTitle:@{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                     NSForegroundColorAttributeName:kColorRGBValue(0x323232),
                                     @"title":title
                                     } forState:UIControlStateNormal];
        [btn setBy_attributedTitle:@{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                     NSForegroundColorAttributeName:kColorRGBValue(0xea6441),
                                     @"title":title
                                     } forState:UIControlStateSelected];
    }
    if (model.isAttention) {
        self.lineView.hidden = YES;
        self.attentionBtn.hidden = YES;
        [self.userNameLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_lessThanOrEqualTo(iPhone5 ? (kCommonScreenWidth - 50 -207 + 47) : (kCommonScreenWidth - 50 - -222 + 47));
        }];
    }else{
        self.lineView.hidden = NO;
        self.attentionBtn.hidden = NO;
        [self.userNameLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_lessThanOrEqualTo(iPhone5 ? (kCommonScreenWidth - 50 -207) : (kCommonScreenWidth - 50 - -222));
        }];
    }
}


#pragma mark - action
- (void)buttomBtnAction:(UIButton *)sender{
    NSInteger index = sender.tag - kBaseTag;
    if (index == 0) {
        [self sendActionName:@"shareAction" param:nil indexPath:self.indexPath];
    }else if (index == 2){
        [self sendActionName:@"likeAction" param:@{@"btn":sender} indexPath:self.indexPath];
    }
}

- (void)attentionBtnAction{
    [self sendActionName:@"attentionAction" param:nil indexPath:self.indexPath];
}

- (void)headerImgAction{
    [self sendActionName:@"headImgAction" param:nil indexPath:self.indexPath];
}

- (void)reloadLiveStatus:(BY_LIVE_STATUS)status liveType:(BY_NEWLIVE_TYPE)liveType{
    if (liveType == BY_NEWLIVE_TYPE_VOD_AUDIO ||
        liveType == BY_NEWLIVE_TYPE_VOD_VIDEO) {
        _liveStatusBg.backgroundColor = [UIColor clearColor];
        _liveStatusLab.text = @"";
        @weakify(self);
        [_liveStatusLab mas_updateConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.width.mas_equalTo(0);
            make.height.mas_equalTo(stringGetHeight(self.liveStatusLab.text, 11) + 4);
        }];
        _watchNumLab.text = [NSString stringWithFormat:@"%@人看过",[NSString transformIntegerShow:self.model.watch_times]];
        return;
    }
    switch (status) {
        case BY_LIVE_STATUS_LIVING:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xea6441);
            _liveStatusLab.text = @"直播中";
            _watchNumLab.text = [NSString stringWithFormat:@"%@人看过",[NSString transformIntegerShow:self.model.watch_times]];
            break;
        case BY_LIVE_STATUS_SOON:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xffbe21);
            _liveStatusLab.text = @"直播预告";
            _watchNumLab.text = [NSString stringWithFormat:@"%@人预约",[NSString transformIntegerShow:self.model.attention_count]];
            break;
        case BY_LIVE_STATUS_END:
        case BY_LIVE_STATUS_OVERTIME:
            _liveStatusBg.backgroundColor = kColorRGBValue(0x4165ea);
            _liveStatusLab.text = @"直播回放";
            _watchNumLab.text = [NSString stringWithFormat:@"%@人看过",[NSString transformIntegerShow:self.model.watch_times]];
            break;
        case BY_LIVE_STATUS_NOTLIVE_ONTIME:
            _liveStatusBg.backgroundColor = kColorRGBValue(0xf28d73);
            _liveStatusLab.text = @"暂未开播";
            _watchNumLab.text = [NSString stringWithFormat:@"%@人预约",[NSString transformIntegerShow:self.model.attention_count]];
            break;
        default:
            break;
    }
    
    CGFloat labOriginX = status == BY_LIVE_STATUS_LIVING ? 25 : 15;
    @weakify(self);
    [_liveStatusLab mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(labOriginX);
        make.width.mas_equalTo(stringGetWidth(self.liveStatusLab.text, 11) + 10);
        make.height.mas_equalTo(stringGetHeight(self.liveStatusLab.text, 11) + 4);
    }];
    
    CGFloat bgWidth = status == BY_LIVE_STATUS_LIVING ? stringGetWidth(self.liveStatusLab.text, 11) + 18 : stringGetWidth(self.liveStatusLab.text, 11) + 10;
    [_liveStatusBg mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(bgWidth);
    }];
    
    self.livIngImgView.hidden = status == BY_LIVE_STATUS_LIVING ? NO : YES;
}

- (void)reloadAttentionAnimation{
    UIButton *btn = (UIButton *)[self viewWithTag:kBaseTag + 2];
    btn.selected = _model.isSupport;
    NSString *title = [NSString transformIntegerShow:self.model.count_support];
    [btn setBy_attributedTitle:@{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                 NSForegroundColorAttributeName:kColorRGBValue(0x323232),
                                 @"title":title
                                 } forState:UIControlStateNormal];
    [btn setBy_attributedTitle:@{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                 NSForegroundColorAttributeName:kColorRGBValue(0xea6441),
                                 @"title":title
                                 } forState:UIControlStateSelected];
    
    btn.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView beginAnimations:@"animation" context:nil];
    [UIView setAnimationDuration:0.2];
    btn.layer.transform = CATransform3DMakeScale(1.2, 1.1, 1);
    [UIView setAnimationDelay:0.4];
    [UIView setAnimationDuration:0.2];
    btn.layer.transform = CATransform3DMakeScale(1, 1, 1);
    [UIView commitAnimations];
}

- (void)setContentView{
    // 封面图
    PDImageView *coverImgView = [[PDImageView alloc] init];
    coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    coverImgView.clipsToBounds = YES;
    [self.contentView addSubview:coverImgView];
    _coverImgView = coverImgView;
    [coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(190);
    }];
    
    UIView *maskView= [UIView by_init];
    [maskView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.3]];
    [self.contentView addSubview:maskView];
    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(190);
    }];
    
    UIImageView *playImgView = [UIImageView by_init];
    [playImgView by_setImageName:@"livehome_play"];
    [self.contentView addSubview:playImgView];
    [playImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(playImgView.image.size.width);
        make.height.mas_equalTo(playImgView.image.size.height);
        make.centerX.mas_equalTo(maskView.mas_centerX).mas_offset(0);
        make.centerY.mas_equalTo(maskView.mas_centerY).mas_offset(0);
    }];
    
   
    
    [self addLiveStatusView];
    
    // 直播人数
    UILabel *watchNumLab = [UILabel by_init];
    [watchNumLab setBy_font:10];
    watchNumLab.textColor = [UIColor whiteColor];
    [maskView addSubview:watchNumLab];
    self.watchNumLab = watchNumLab;
    [watchNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(27);
        make.bottom.mas_equalTo(-12);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(11);
    }];
    
    UILabel *bbtNumLab = [UILabel by_init];
    [bbtNumLab setBy_font:13];
    bbtNumLab.textColor = [UIColor whiteColor];
    [self.contentView addSubview:bbtNumLab];
    self.bbtNumLab = bbtNumLab;
    [bbtNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(watchNumLab);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_equalTo(16);
    }];
    
    UIImageView *bbtIcon = [UIImageView by_init];
    [bbtIcon by_setImageName:@"common_bp"];
    [self.contentView addSubview:bbtIcon];
    [bbtIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bbtNumLab.mas_left).mas_offset(-10);
        make.centerY.mas_equalTo(bbtNumLab);
        make.width.mas_equalTo(16);
        make.height.mas_equalTo(16);
    }];
    
    // 用户头像
    PDImageView *userlogo = [[PDImageView alloc] init];
    userlogo.contentMode = UIViewContentModeScaleAspectFill;
    userlogo.userInteractionEnabled = YES;
    userlogo.layer.cornerRadius = 18;
    userlogo.clipsToBounds = YES;
    [self.contentView addSubview:userlogo];
    self.userlogo = userlogo;
    [userlogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.bottom.mas_equalTo(-8);
        make.width.mas_equalTo(36);
        make.height.mas_equalTo(36);
    }];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headerImgAction)];
    [userlogo addGestureRecognizer:tapGestureRecognizer];

    
    // 用户昵称
    UILabel *userNameLab = [UILabel by_init];
    [userNameLab setBy_font:14];
    userNameLab.textColor = kColorRGBValue(0x323232);
    [self.contentView addSubview:userNameLab];
    self.userNameLab = userNameLab;
    [userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userlogo.mas_right).mas_offset(8);
        make.centerY.mas_equalTo(userlogo.mas_centerY).mas_offset(0);
        make.height.mas_equalTo(15);
        make.width.mas_lessThanOrEqualTo(kCommonScreenWidth/2);
    }];
    
    UIView *lineView = [UIView by_init];
    lineView.backgroundColor = kColorRGBValue(0xd2d2d2);
    [self.contentView addSubview:lineView];
    self.lineView = lineView;
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userNameLab.mas_right).offset(5);
        make.centerY.mas_equalTo(userlogo);
        make.height.mas_equalTo(13);
        make.width.mas_equalTo(0.5);
    }];
    
    UIButton *attentionBtn = [UIButton by_init];
    [attentionBtn setBy_attributedTitle:@{@"title":@"关注",
                                          NSFontAttributeName:[UIFont systemFontOfSize:14],
                                          NSForegroundColorAttributeName:kColorRGBValue(0xea6441)
                                          } forState:UIControlStateNormal];
    [attentionBtn addTarget:self action:@selector(attentionBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:attentionBtn];
    self.attentionBtn = attentionBtn;
    [attentionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineView.mas_right);
        make.height.mas_equalTo(20);
        make.centerY.mas_equalTo(lineView);
        make.width.mas_equalTo(47);
    }];
    
    [self addTagsView];
}

- (void)addLiveStatusView{
    
    UILabel *liveStatusLab = [UILabel by_init];
    liveStatusLab.text = @"直播回放";
    [liveStatusLab setBy_font:11];
    [liveStatusLab setTextColor:[UIColor whiteColor]];
    liveStatusLab.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:liveStatusLab];
    _liveStatusLab = liveStatusLab;
    [liveStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(12);
        make.width.mas_equalTo(stringGetWidth(liveStatusLab.text, liveStatusLab.font.pointSize) + 10);
        make.height.mas_equalTo(stringGetHeight(liveStatusLab.text, liveStatusLab.font.pointSize) + 4);
    }];
    
    UIView *liveStatusBg = [UIView by_init];
    liveStatusBg.layer.cornerRadius = 2.0f;
    [self.contentView addSubview:liveStatusLab];
    [self.contentView insertSubview:liveStatusBg belowSubview:liveStatusLab];
    self.liveStatusBg = liveStatusBg;
    [liveStatusBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(12);
        make.width.mas_equalTo(0);
        make.height.mas_equalTo(liveStatusLab.mas_height).mas_offset(0);
    }];
    
    NSString *filePath = [[NSBundle bundleWithPath:[[NSBundle mainBundle] bundlePath]] pathForResource:@"video_living_animation.gif" ofType:nil];
    NSData *imageData = [NSData dataWithContentsOfFile:filePath];
    
    FLAnimatedImageView *livIngImgView = [FLAnimatedImageView by_init];
    livIngImgView.animatedImage = [FLAnimatedImage animatedImageWithGIFData:imageData];
    livIngImgView.hidden = YES;
    [self.contentView addSubview:livIngImgView];
    self.livIngImgView = livIngImgView;
    [livIngImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.centerY.mas_equalTo(liveStatusBg);
        make.width.mas_equalTo(8);
        make.height.mas_equalTo(9);
    }];
    
    // 标题
    YYLabel *titleLab = [[YYLabel alloc] init];
    titleLab.font = [UIFont systemFontOfSize:15];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.numberOfLines = 2;
    [self.contentView addSubview:titleLab];
    _titleLab = titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(12);
        make.right.mas_equalTo(-15);
        make.height.mas_greaterThanOrEqualTo(15);
    }];
}

- (void)addTagsView{
    NSArray *imageNames = @[@"livehome_share",@"livehome_comment",@"livehome_like_normal"];
    CGFloat btnW = 60.0f;
    for (int i = 0; i < 3; i ++) {
        UIButton *btn = [UIButton by_buttonWithCustomType];
        UIButton *lastBtn = [self viewWithTag:kBaseTag + i - 1];
        [btn setImage:[UIImage imageNamed:imageNames[i]] forState:UIControlStateNormal];
        NSString *title = i == 0 ? @"" : @"0";
        [btn setTitle:title forState:UIControlStateNormal];
        btn.selected = NO;
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
        if (i == 0) {
            btn.imageEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0);
        }else{
            btn.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
        }
        btn.tag = kBaseTag + i;
        if (i == 2) {
            [btn setImage:[UIImage imageNamed:@"livehome_like_highlight"] forState:UIControlStateSelected];
            self.likeBtn = btn;
        }
        [btn addTarget:self action:@selector(buttomBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn];
        @weakify(self);
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.width.mas_equalTo(i == 0 ? 30 : btnW);
            if (i == 0) {
                make.right.mas_equalTo(0);
            }else{
                make.right.mas_equalTo(lastBtn.mas_left).mas_offset(-1);
            }
            make.centerY.mas_equalTo(self.userlogo.mas_centerY);
            make.height.mas_equalTo(50);
        }];
    }
}

@end
