//
//  BYVideoHomeLiveSubController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/12/31.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "HGPageViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYVideoHomeLiveSubController : HGPageViewController

/** 话题 */
@property (nonatomic ,copy) NSString *topic;
/**  */
@property (nonatomic ,assign) BY_NEWLIVE_TYPE type;

@end

NS_ASSUME_NONNULL_END
