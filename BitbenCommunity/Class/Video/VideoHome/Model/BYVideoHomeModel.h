//
//  BYVideoHomeModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/2.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BYVideoHomeModel : BYCommonLiveModel

/** size */
@property (nonatomic ,assign) CGSize titleSize;
/** attributedstring */
@property (nonatomic ,strong) NSAttributedString *attributedString;


+ (NSArray *)getTableData:(NSDictionary *)responds;

+ (NSArray *)getVideoTableData:(NSArray *)respond;

@end

NS_ASSUME_NONNULL_END
