//
//  BYVideoHomeModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/8/2.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYVideoHomeModel.h"
#import <YYLabel.h>
#import <NSAttributedString+YYText.h>

@implementation BYVideoHomeModel
+ (NSArray *)getTableData:(NSDictionary *)responds{
    NSArray *content = responds[@"content"];
    NSMutableArray *data = [NSMutableArray array];
    for (NSDictionary *dic in content) {
        BYVideoHomeModel *model = [BYVideoHomeModel mj_objectWithKeyValues:dic];
        model.cellString = @"BYVideoHomeCell";
        model.cellHeight = 242;
        CGFloat headIndent = 0.0f;
        if (model.live_type == BY_NEWLIVE_TYPE_VOD_VIDEO ||
            model.live_type == BY_NEWLIVE_TYPE_VOD_AUDIO) {
            headIndent = 0.0f;
        }else{
            NSString *text = @"";
            switch (model.status) {
                case BY_LIVE_STATUS_LIVING:
                    text = @"直播中";
                    break;
                case BY_LIVE_STATUS_SOON:
                    text = @"直播预告";
                    break;
                case BY_LIVE_STATUS_END:
                case BY_LIVE_STATUS_OVERTIME:
                    text = @"直播回放";
                    break;
                default:
                    break;
            }
            headIndent = stringGetWidth(text, 11) + 20;
        }
        
        NSMutableAttributedString *messageAttributed = [[NSMutableAttributedString alloc]initWithString:nullToEmpty(model.live_title)];
        messageAttributed.yy_firstLineHeadIndent = headIndent;
        messageAttributed.yy_lineSpacing = 8.0f;
        messageAttributed.yy_color = [UIColor whiteColor];
        messageAttributed.yy_font = [UIFont systemFontOfSize:15];
        model.attributedString = messageAttributed;
        YYTextContainer *textContainer = [YYTextContainer new];
        textContainer.size = CGSizeMake(kCommonScreenWidth - 30, MAXFLOAT);
        YYTextLayout *layout = [YYTextLayout layoutWithContainer:textContainer text:messageAttributed];
        model.titleSize = CGSizeMake(layout.textBoundingSize.width, layout.textBoundingSize.height > 46 ? 46 : layout.textBoundingSize.height);
        [data addObject:@{@"data":@[model]}];
    }
    return data;
}

+ (NSArray *)getVideoTableData:(NSArray *)respond{
    if (![respond isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if (![respond count]) {
        return @[];
    }
    __block NSMutableArray *data = [NSMutableArray array];
    CGFloat coverH = (kCommonScreenWidth - 30)*180/345;
    [respond enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BYVideoHomeModel *model = [BYVideoHomeModel mj_objectWithKeyValues:obj];
        model.cellString = @"BYVideoHomeLiveCell";
        model.cellHeight = 79 + coverH;
        
        NSDate *date = [NSDate by_dateFromString:model.begin_time dateformatter:K_D_F];
        NSString *time = [NSDate by_stringFromDate:date dateformatter:@"yyyy-MM-dd HH:mm"];

        model.begin_time = time;
        [data addObject:@{@"data":@[model]}];
    }];
    return [data copy];
}
@end
