//
//  BYCollectionController.m
//  BitbenCommunity
//
//  Created by 随风 on 2020/1/13.
//  Copyright © 2020 币源网络. All rights reserved.
//

#import "BYCollectionController.h"
#import "BYCollectionViewModel.h"

@interface BYCollectionController ()

@end

@implementation BYCollectionController

- (Class)getViewModelClass{
    return [BYCollectionViewModel class];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationTitle = @"我的收藏";
}


@end
