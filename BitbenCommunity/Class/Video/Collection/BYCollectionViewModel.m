//
//  BYCollectionViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2020/1/13.
//  Copyright © 2020 币源网络. All rights reserved.
//

#import "BYCollectionViewModel.h"
#import "ShareRootViewController.h"
#import "GWAssetsImgSelectedViewController.h"
#import "BYTopicDetailController.h"

#import "BYReportInCellView.h"
#import "BYHomeArticleTagCell.h"
#import "BYReportSheetView.h"

#import "BYPersonHomeHeaderModel.h"
#import "BYHomeArticleModel.h"
@interface BYCollectionViewModel ()<BYCommonTableViewDelegate>

/** BYCommonTableView */
@property (nonatomic ,strong) BYCommonTableView *tableView;
/** 页码 */
@property (nonatomic ,assign) NSInteger pageNum;
/** 举报 */
@property (nonatomic ,strong) BYReportSheetView *reportSheetView;

@end

@implementation BYCollectionViewModel

- (void)viewWillAppear{
    [self reloadData];
}

- (void)setContentView{
    [self addTableView];
}

- (void)reloadData{
    _pageNum = 0;
    [self loadRequestGetCollectionData];
}

- (void)loadMoreData{
    [self loadRequestGetCollectionData];
}

// 跳转话题详情
- (void)pushTopicDetailController:(NSString *)topic{
    if (!topic.length) return;
    UIViewController *currentController = [BYTabbarViewController sharedController].currentController;
    BYTopicDetailController *topicDetailController = [[BYTopicDetailController alloc] init];
    topicDetailController.topic = topic;
    [currentController.navigationController pushViewController:topicDetailController animated:YES];
}

// 关注
- (void)attentionAction:(id)model{
    if ([model isKindOfClass:[BYCommonLiveModel class]]) {
        BYCommonLiveModel *data = (BYCommonLiveModel *)model;
        @weakify(self);
        [self loadRequestAttentionUser:data.user_id isAttention:!data.isAttention suc:^(BOOL attention) {
            @strongify(self);
            data.isAttention = attention;
            [self.tableView reloadData];
        }];
    }else if ([model isKindOfClass:[BYHomeArticleModel class]]){
        BYHomeArticleModel *data = (BYHomeArticleModel *)model;
        @weakify(self);
        [self loadRequestAttentionUser:data.author_id isAttention:!data.isAttention suc:^(BOOL attention) {
            @strongify(self);
            data.isAttention = attention;
            [self.tableView reloadData];
        }];
    }
}


- (void)showMoreSheetView:(id)model indexPath:(NSIndexPath *)indexPath{
    NSString *theme_id;
    BY_THEME_TYPE type = BY_THEME_TYPE_LIVE;
    NSString *userId ;
    if ([model isKindOfClass:[BYCommonLiveModel class]]) {
        BYCommonLiveModel *data = (BYCommonLiveModel *)model;
        theme_id = data.live_record_id;
        type = BY_THEME_TYPE_LIVE;
        userId = data.user_id;
    }else if ([model isKindOfClass:[BYHomeArticleModel class]]){
        BYHomeArticleModel *data = (BYHomeArticleModel *)model;
        theme_id = data.article_id;
        type = BY_THEME_TYPE_ARTICLE;
        userId = data.author_id;
    }
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    UIAlertAction *reportAction = [UIAlertAction actionWithTitle:@"取消收藏" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @weakify(self);
        [[BYLiveHomeRequest alloc] loadRequestDeltOpearType:2 theme_id:theme_id theme_type:type successBlock:^(id object) {
            @strongify(self);
            [self.tableView beginUpdates];
            NSMutableArray *tmpArr = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [tmpArr removeObjectAtIndex:indexPath.section];
            self.tableView.tableData = [tmpArr copy];
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView endUpdates];
        } faileBlock:^(NSError *error) {
            showToastView(@"收藏操作失败", kCommonWindow);
        }];
    }];
    
    UIAlertAction *collectionAction = [UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.reportSheetView.theme_id = theme_id;
        self.reportSheetView.user_id = userId;
        self.reportSheetView.theme_type = type;
        [self.reportSheetView showAnimation];
    }];
    
    [sheetController addAction:reportAction];
    [sheetController addAction:collectionAction];
    [sheetController addAction:cancelAction];
    [S_V_NC presentViewController:sheetController animated:YES completion:nil];
}

// 顶的操作
- (void)supportAction:(id)model indexPath:(NSIndexPath *)indexPath suc:(void(^)(void))suc{
    if ([model isKindOfClass:[BYCommonLiveModel class]]) {
        BYCommonLiveModel *data = (BYCommonLiveModel *)model;
        if (data.isSupport) {
            showToastView(@"你已经顶过该条内容", S_V_VIEW);
            return;
        }
        [self loadRequestSupport:data.live_record_id theme_type:BY_THEME_TYPE_LIVE user_id:data.user_id suc:^{
            data.isSupport = YES;
            data.count_support ++;
            if (suc) suc();
        }];
    }else if ([model isKindOfClass:[BYHomeArticleModel class]]){
        BYHomeArticleModel *data = (BYHomeArticleModel *)model;
        if (data.isSupport) {
            showToastView(@"你已经顶过该条内容", S_V_VIEW);
            return;
        }
        [self loadRequestSupport:data.article_id theme_type:BY_THEME_TYPE_ARTICLE user_id:data.author_id suc:^{
            data.isSupport = YES;
            data.count_support ++;
            if (suc) suc();
        }];
    }
}

- (void)shareAction:(id)model{
    NSString *shareTitle;
    NSString *shareContent;
    NSString *shareImgUrl;
    NSString *shareUrl;
    shareType sharetype = shareTypeLive;
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    if ([model isKindOfClass:[BYCommonLiveModel class]]) {
        BYCommonLiveModel *data = (BYCommonLiveModel *)model;
        NSDictionary *params = @{@"首页直播分享":(data.room_id.length?data.room_id:@"找不到房间号")};
        [MTAManager event:MTATypeHotLiveShare params:params];
        NSString *mainImgUrl = [PDImageView appendingImgUrl:data.shareMap.share_img];
        NSString *imgurl = [mainImgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        shareTitle = data.shareMap.share_title;
        shareContent = data.shareMap.share_intro;
        shareImgUrl = imgurl;
        shareUrl = data.shareMap.share_url;
        sharetype = shareTypeLive;
        shareViewController.collection_status = data.collection_status;
        [shareViewController actionClickWithCollectionBlock:^{
            data.collection_status = ! data.collection_status;
        }];
    }else if ([model isKindOfClass:[BYHomeArticleModel class]]){
        BYHomeArticleModel *data = (BYHomeArticleModel *)model;
        NSDictionary *params = @{@"首页文章分享":(data.article_id.length?data.article_id:@"找不到文章")};
        [MTAManager event:MTATypeShortShare params:params];
        id smartImgUrl;
        if (data.picture.count){
            smartImgUrl = [data.picture firstObject];
        } else {
            smartImgUrl = data.head_img;
        }
        
        id mainImgUrl;
        if ([smartImgUrl isKindOfClass:[NSString class]]){
            NSString *urlStr = (NSString *)smartImgUrl;
            NSString *baseURL = [PDImageView getUploadBucket:urlStr];
            NSString *nUrl = [[NSString stringWithFormat:@"%@%@",baseURL,urlStr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            mainImgUrl = nUrl;
        } else if ([smartImgUrl isKindOfClass:[UIImage class]]){
            mainImgUrl = smartImgUrl;
        }
        if (data.article_type == article_typeWeb){
            shareTitle = data.title;
            if (data.subtitle.length && ![data.subtitle isEqualToString:@"null"]){
                shareContent = data.subtitle;
            } else {
                shareContent = @"最新最热区块链内容，尽在币本社区";
            }
        } else if (data.article_type == article_typeNormal){
            shareTitle = data.title.length?data.title:data.content;
            shareContent = @"最新最热区块链内容，尽在币本社区";
        }
        shareUrl = data.article_url;
        shareImgUrl = mainImgUrl;
        sharetype = shareTypeArticle;
        shareViewController.collection_status = data.collection_status;
        [shareViewController actionClickWithCollectionBlock:^{
            data.collection_status = ! data.collection_status;
        }];
    }
    
    
    shareViewController.transferCopyUrl = shareUrl;
    
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        [ShareSDKManager shareManagerWithType:shareType title:shareTitle desc:shareContent img:shareImgUrl url:shareUrl callBack:NULL];
        [ShareSDKManager shareSuccessBack:sharetype block:NULL];
    }];
    UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
    [shareViewController showInView:currentController];
}

#pragma mark - request

- (void)loadRequestGetCollectionData{
    @weakify(self);
    [[BYVideoRequest alloc] loadRequestGetCollectionData:self.pageNum successBlock:^(id  _Nonnull object) {
        @strongify(self);
        [self.tableView endRefreshing];
        [self.tableView removeEmptyView];
        if (self.pageNum == 0) {
            self.tableView.tableData = object;
            if (![object count]) {
                [self.tableView addEmptyView:@"暂无收藏"];
                return ;
            }
        }else{
            if (![object count]) return;
            NSMutableArray *tmpData = [NSMutableArray arrayWithArray:self.tableView.tableData];
            [tmpData addObjectsFromArray:object];
            self.tableView.tableData = tmpData;
        }
        self.pageNum ++;
        
    } faileBlock:^(NSError * _Nonnull error) {
        @strongify(self);
        [self.tableView endRefreshing];
    }];
}

// 关注
- (void)loadRequestAttentionUser:(NSString *)user_id isAttention:(BOOL)isAttention suc:(void(^)(BOOL attention))suc{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestCreatAttention:user_id isAttention:isAttention successBlock:^(id object) {
        @strongify(self);
        showToastView(@"已关注", S_V_VIEW);
        if (suc) {
            suc(isAttention);
        }
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"关注失败", S_V_VIEW);
    }];
}

// 顶
- (void)loadRequestSupport:(NSString *)theme_id theme_type:(BY_THEME_TYPE)theme_type user_id:(NSString *)user_id suc:(void(^)(void))suc{
    @weakify(self);
    [[BYLiveHomeRequest alloc] loadRequestLiveGuestOpearType:BY_GUEST_OPERA_TYPE_UP theme_id:theme_id theme_type:theme_type receiver_user_id:user_id successBlock:^(id object) {
        if (suc) suc();
    } faileBlock:^(NSError *error) {
        @strongify(self);
        showToastView(@"顶操作失败", S_V_VIEW);
    }];
}

#pragma mark - BYCommonTableViewDelegate
- (void)by_tableView:(BYCommonTableView *)tableView tabledidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = tableView.tableData[indexPath.section];
    id model = dic.allValues[0][indexPath.row];
    if ([model isKindOfClass:[BYCommonLiveModel class]]) {
        BYCommonLiveModel *data = (BYCommonLiveModel *)model;
        [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_LIVE
                                                liveType:data.live_type
                                                 themeId:data.live_record_id];
        
    }else if ([model isKindOfClass:[BYHomeArticleModel class]]){
        BYHomeArticleModel *data = (BYHomeArticleModel *)model;
        [DirectManager commonPushControllerWithThemeType:BY_THEME_TYPE_ARTICLE
                                                liveType:0
                                                 themeId:data.article_id];
    }
}

- (UIView *)by_tableView:(BYCommonTableView *)tableView tableViewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [UIView by_init];
    [sectionView setBackgroundColor:[UIColor clearColor]];
    return sectionView;
}

- (CGFloat)by_tableView:(BYCommonTableView *)tableView tableViewHeightForHeaderInSection:(NSInteger)section{
    return 8;
}

- (void)by_tableView:(BYCommonTableView *)tableView reveiveCellActionName:(NSString *)actionName param:(NSDictionary *)param indexPath:(NSIndexPath *)indexPath{
    if ([actionName isEqualToString:@"shareAction"]) { // 分享
        NSDictionary *dic = tableView.tableData[indexPath.section];
        id model = dic.allValues[0][indexPath.row];
        [self shareAction:model];
    }else if ([actionName isEqualToString:@"likeAction"]) { // 顶
        BYHomeArticleTagCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        NSDictionary *dic = tableView.tableData[indexPath.section];
        id model = dic.allValues[0][indexPath.row];
        @weakify(self);
        [S_VC authorizeWithCompletionHandler:^(BOOL successed) {
            @strongify(self);
            if (!successed) return ;
            [self supportAction:model indexPath:indexPath suc:^{
                [cell reloadAttentionAnimation];
            }];
        }];
    }else if ([actionName isEqualToString:@"attentionAction"]) { // 关注
        NSDictionary *dic = tableView.tableData[indexPath.section];
        id model = dic.allValues[0][indexPath.row];
        @weakify(self);
        [S_VC authorizeWithCompletionHandler:^(BOOL successed) {
            @strongify(self);
            if (!successed) return ;
            [self attentionAction:model];
        }];
    }else if ([actionName isEqualToString:@"imgAction"]) { // 图片点击放大
        NSInteger index = [param[@"index"] integerValue];
        NSDictionary *dic = tableView.tableData[indexPath.section];
        BYHomeArticleModel *model = dic.allValues[0][indexPath.row];
        UIViewController *currentController = [[BYTabbarViewController sharedController] getCurrentController];
        GWAssetsImgSelectedViewController *imgSelected = [[GWAssetsImgSelectedViewController alloc]init];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [imgSelected showInView:currentController imgArr:model.picture currentIndex:index cell:cell];
    }else if ([actionName isEqualToString:@"reportAction"]) { // 举报
        NSDictionary *dic = tableView.tableData[indexPath.section];
        id model = dic.allValues[0][indexPath.row];
//        NSValue *value = param[@"point"];
//        CGPoint point = [value CGPointValue];
//        [self showReportView:point model:model];
        [self showMoreSheetView:model indexPath:indexPath];
    }else if ([actionName isEqualToString:@"selectTagAction"]){ // 点击话题tag
        NSString *tag = param[@"tag"];
        [self pushTopicDetailController:tag];
    }
}

#pragma mark - configUI
- (void)addTableView{
    self.tableView = [[BYCommonTableView alloc] init];
    self.tableView.group_delegate = self;
    [self.tableView setBackgroundColor:kColorRGBValue(0xf2f4f5)];
    [self.tableView addHeaderRefreshTarget:self action:@selector(reloadData)];
    [self.tableView addFooterRefreshTarget:self action:@selector(loadMoreData)];
    [S_V_VIEW addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (BYReportSheetView *)reportSheetView{
    if (!_reportSheetView) {
        _reportSheetView = [BYReportSheetView initReportSheetViewShowInView:kCommonWindow];
    }
    return _reportSheetView;
}

@end
