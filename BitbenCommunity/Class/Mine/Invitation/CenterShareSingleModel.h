//
//  CenterShareSingleModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/9/26.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"

@interface CenterShareSingleModel : FetchModel

@property (nonatomic,copy)NSString *detail;
@property (nonatomic,copy)NSString *pic;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *url;

@end
