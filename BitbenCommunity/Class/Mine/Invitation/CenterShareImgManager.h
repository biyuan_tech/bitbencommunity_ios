//
//  CenterShareImgManager.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN
@protocol CenterShareImgsModel <NSObject>

@end


@interface CenterShareImgsModel:FetchModel
@property (nonatomic,strong)id views;
@property (nonatomic,assign)CGRect frame;
@property (nonatomic,assign)CGFloat hangjianju;
@end


@interface CenterShareImgManager : FetchModel
#pragma mark - 主方法
+(UIImage *)createShareImgWithBgImg:(UIImage *)bgImg viewArr:(NSArray <CenterShareImgsModel>*)viewArrs block:(void(^)(UIImage *img))block;

#pragma mark 创建二维码图片
+(UIImage *)createErweimaWithInfo:(NSString *)infoUrl;
#pragma mark 二维码变成指定大小
+ (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat) size;
@end

NS_ASSUME_NONNULL_END
