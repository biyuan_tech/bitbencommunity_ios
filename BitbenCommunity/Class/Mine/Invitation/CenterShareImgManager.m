//
//  CenterShareImgManager.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/12.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "CenterShareImgManager.h"

@implementation CenterShareImgsModel


@end


@implementation CenterShareImgManager

+(UIImage *)createShareImgWithBgImg:(UIImage *)bgImg viewArr:(NSArray <CenterShareImgsModel>*)viewArrs block:(void(^)(UIImage *img))block{
   // 1. 创建行间距
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = LCFloat(6); //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    //设置字间距 NSKernAttributeName:@1.5f

    
    
    // 1. 创建背景
    UIGraphicsBeginImageContextWithOptions(bgImg.size, NO, 0);
    [bgImg drawAtPoint:CGPointZero];
    //获得一个位图图形上下文
    CGContextRef context=UIGraphicsGetCurrentContext();
    CGContextDrawPath(context, kCGPathStroke);
    
    // 2. 加载内容
    for (CenterShareImgsModel *infoModel in viewArrs){
        if ([infoModel.views isKindOfClass:[UIImage class]]){           // 图片
            UIImage *imageMack = (UIImage *)infoModel.views;
            [imageMack drawInRect:infoModel.frame];
        } else if ([infoModel.views isKindOfClass:[UILabel class]]){
            UILabel *infoLabel = (UILabel *)infoModel.views;
            if (infoModel.hangjianju == 0){
                [infoLabel.text drawInRect:infoModel.frame withAttributes:@{NSFontAttributeName:infoLabel.font,NSForegroundColorAttributeName:infoLabel.textColor}];
            } else {
                [infoLabel.text drawInRect:infoModel.frame withAttributes:@{NSFontAttributeName:infoLabel.font,NSForegroundColorAttributeName:infoLabel.textColor,NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f}];
            }
        }
    }
    
    //4、从上下文中获取生成新图片
    UIImage *getImage = UIGraphicsGetImageFromCurrentImageContext();
    
    //5、关闭上下文
    UIGraphicsEndImageContext();
    return getImage;
}







#pragma mark - 根据信息变成二维码
+(UIImage *)createErweimaWithInfo:(NSString *)infoUrl{
    // 1.创建过滤器
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    
    // 2.恢复默认
    [filter setDefaults];
    
    // 3.给过滤器添加数据(正则表达式/账号和密码)
    NSString *dataString = infoUrl;
    NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKeyPath:@"inputMessage"];
    
    // 4.获取输出的二维码
    CIImage *outputImage = [filter outputImage];
    
    //因为生成的二维码模糊，所以通过createNonInterpolatedUIImageFormCIImage:outputImage来获得高清的二维码图片
    
    // 5.显示二维码
    UIImage *img = [CenterShareImgManager createNonInterpolatedUIImageFormCIImage:outputImage withSize:121];
    
    return img;
}

+ (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat) size
{
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    
    // 1.创建bitmap;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    
    // 2.保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    return [UIImage imageWithCGImage:scaledImage];
}

@end
