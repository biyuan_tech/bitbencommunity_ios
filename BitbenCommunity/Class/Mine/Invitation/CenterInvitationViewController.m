//
//  CenterInvitationViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/24.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterInvitationViewController.h"
#import "CenterInvitationAblumTableViewCell.h"
#import "CenterInvitationNumberTableViewCell.h"
#import "NetworkAdapter+Center.h"
#import "CenterInvitationHistoryTableViewCell.h"
#import "CenterInvitationNoneInfoTableViewCell.h"
#import "ShareRootViewController.h"
#import "CenterShareImgModel.h"

@interface CenterInvitationViewController ()<UITableViewDataSource,UITableViewDelegate>{
    CenterInvitationSingleModel *infoModel;
    CenterShareSingleModel *shareRootModel;
    UIImage *shareImg;
}
@property (nonatomic,strong)UITableView *invitationTableView;
@property (nonatomic,strong)NSArray *invitationArr;
@property (nonatomic,strong)NSMutableArray *activityMutableArr;
@property (nonatomic,strong)NSMutableArray *historyMutableArr;

@end

@implementation CenterInvitationViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];    
    [self createTableView];
    [self sendRequestToYaoqing];
    [self sendRequestToGetShareManager];                // 获取邀请内容
    [self sendRequestToGetYaoqingHistory];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"邀请好友";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.activityMutableArr = [NSMutableArray array];
    self.historyMutableArr = [NSMutableArray array];
    [self.activityMutableArr addObject:@"活动规则"];
    [self.activityMutableArr addObject:@"1.一级好友每成功邀请1位好友奖励400BBT。"];
    [self.activityMutableArr addObject:@"2.二级好友每邀请成功一个奖励100BBT。"];
    [self.activityMutableArr addObject:@"3.好友领取BBT但未完成注册视为无效邀请。"];
    [self.activityMutableArr addObject:@"4.邀请成功奖励BBT数量不设上限。"];
    [self.activityMutableArr addObject:@"5.每日邀请好友达到10人后额外每人奖励5个。"];
    [self.activityMutableArr addObject:@"6.每日邀请好友数量不设上限。"];
    
    self.invitationArr = @[@[@"封面",@"数量",@"邀请按钮"],self.activityMutableArr,self.historyMutableArr];
    
}

-(void)createTableView{
    if (!self.invitationTableView){
        self.invitationTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.invitationTableView.dataSource = self;
        self.invitationTableView.delegate = self;
        [self.view addSubview:self.invitationTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.invitationTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetYaoqingHistory];
    }];
    [self.invitationTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetYaoqingHistory];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.invitationArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.invitationArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"封面" sourceArr:self.invitationArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"封面" sourceArr:self.invitationArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        CenterInvitationAblumTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[CenterInvitationAblumTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"数量" sourceArr:self.invitationArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"数量" sourceArr:self.invitationArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        CenterInvitationNumberTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[CenterInvitationNumberTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferSingleModel = infoModel;
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"邀请按钮" sourceArr:self.invitationArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"邀请按钮" sourceArr:self.invitationArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.backgroundColor = [UIColor whiteColor];
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"立即邀请";
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            
            // 埋点
            NSDictionary *params = @{@"邀请人":[AccountModel sharedAccountModel].account_id};
            [MTAManager event:MTATypeInvitation params:params];
            
            [strongSelf actionShowShareInfoManager:shareRootModel];
        }];
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"活动规则" sourceArr:self.invitationArr]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            GWNormalTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowFour){
                cellWithRowFour = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            }
            cellWithRowFour.transferCellHeight = cellHeight;
            cellWithRowFour.titleLabel.font = [[UIFont fontWithCustomerSizeName:@"15"] boldFont];
            cellWithRowFour.transferTitle = @"活动规则";
            return cellWithRowFour;
        } else {
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            GWNormalTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            }
            cellWithRowFiv.transferCellHeight = cellHeight;
            cellWithRowFiv.transferTitle = [self.activityMutableArr objectAtIndex:indexPath.row];
            cellWithRowFiv.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
            cellWithRowFiv.titleLabel.textColor = [UIColor colorWithCustomerName:@"3B3B3B"];
            return cellWithRowFiv;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"邀请记录" sourceArr:self.invitationArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"邀请记录" sourceArr:self.invitationArr]){
            static NSString *cellIdentifyWithRowSex = @"cellIdentifyWithRowSex";
            GWNormalTableViewCell *cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSex];
            if (!cellWithRowSex){
                cellWithRowSex = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSex];
            }
            cellWithRowSex.transferCellHeight = cellHeight;
            cellWithRowSex.titleLabel.font = [[UIFont fontWithCustomerSizeName:@"15"] boldFont];
            cellWithRowSex.transferTitle = @"邀请记录";
            return cellWithRowSex;
        } else {
            if (indexPath.row == [self cellIndexPathRowWithcellData:@"历史空记录" sourceArr:self.invitationArr]){
                static NSString *cellIdentifyWithRowSev1 = @"cellIdentifyWithRowSev1";
                CenterInvitationNoneInfoTableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev1];
                if (!cellWithRowSev){
                    cellWithRowSev = [[CenterInvitationNoneInfoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev1];
                }
                
                return cellWithRowSev;
            } else {
                static NSString *cellIdentifyWithRowSev = @"cellIdentifyWithRowSev";
                CenterInvitationHistoryTableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev];
                if (!cellWithRowSev){
                    cellWithRowSev = [[CenterInvitationHistoryTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev];
                }
                cellWithRowSev.transferModel = [self.historyMutableArr objectAtIndex:indexPath.row];
                
                return cellWithRowSev;
            }
        }
    } else {
        static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
        GWNormalTableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
        if (!cellWithRowOther){
            cellWithRowOther = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
        }
        return cellWithRowOther;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"封面" sourceArr:self.invitationArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"封面" sourceArr:self.invitationArr]){
        return [CenterInvitationAblumTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"数量" sourceArr:self.invitationArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"数量" sourceArr:self.invitationArr]){
        return [CenterInvitationNumberTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"邀请按钮" sourceArr:self.invitationArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"邀请按钮" sourceArr:self.invitationArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"活动规则" sourceArr:self.invitationArr]){
        if (indexPath.row == 0){
            return [GWNormalTableViewCell calculationCellHeight];
        } else {
            return [GWNormalTableViewCell calculationCellHeight];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"邀请记录" sourceArr:self.invitationArr]){
        if (indexPath.row == 0){
            return [GWNormalTableViewCell calculationCellHeight];
        } else {
            if (indexPath.row == [self cellIndexPathRowWithcellData:@"历史空记录" sourceArr:self.invitationArr]){
                return [CenterInvitationNoneInfoTableViewCell calculationCellHeight];
            } else {
                return [CenterInvitationHistoryTableViewCell calculationCellHeight];
            }
        }
    } else {
        return [CenterInvitationHistoryTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else {
        return LCFloat(20);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = RGB(245, 245, 245, 1);
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *sectionOfArr = [self.invitationArr objectAtIndex:indexPath.section];
    if (self.invitationTableView && indexPath.section == [self cellIndexPathSectionWithcellData:@"邀请记录" sourceArr:self.invitationArr] && ! [self.historyMutableArr containsObject:@"历史空记录"]  ) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if (indexPath.row == sectionOfArr.count - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if (sectionOfArr.count == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}

#pragma mark - 分享的控制器
-(void)actionShowShareInfoManager:(CenterShareSingleModel *)model{
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.hasCollection = NO;
    
    shareViewController.showImgBackgroundImage = shareImg;
    shareViewController.isHasGesture = YES;
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        
        [ShareSDKManager shareManagerWithType:shareType title:nil desc:nil img:shareImg url:nil callBack:NULL];
        [ShareSDKManager shareSuccessBack:shareTypeInvitation block:NULL];
    }];
    [shareViewController showInView:self.parentViewController];
}


#pragma mark - 接口
#pragma mark 进行邀请
-(void)sendRequestToYaoqing{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerGetMoneyget_my_invitationBlock:^(BOOL isSuccessed, CenterInvitationSingleModel *model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            strongSelf->infoModel = model;
            [strongSelf.invitationTableView reloadData];
        }
    }];
}

#pragma mark 获取邀请历史
-(void)sendRequestToGetYaoqingHistory{
    __weak typeof(self)weakSelf = self;

    [[NetworkAdapter sharedAdapter] centerGetInvitationHistoryPage:self.invitationTableView.currentPage block:^(CenterInvitationHistoryListModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.invitationTableView.isXiaLa){            // 下拉
            [strongSelf.historyMutableArr removeAllObjects];
        }
        
        if (![strongSelf.historyMutableArr containsObject:@"邀请记录"]){
            [strongSelf.historyMutableArr addObject:@"邀请记录"];
        }
        
        if (singleModel.content.count){
            [strongSelf.historyMutableArr addObjectsFromArray:singleModel.content];
        }
        [strongSelf.invitationTableView reloadData];
        
        // 判断是否有历史记录
        if (strongSelf.historyMutableArr.count == 1 && [[strongSelf.historyMutableArr lastObject] isEqualToString:@"邀请记录"]){
            [strongSelf.historyMutableArr addObject:@"历史空记录"];
        }
        
        if (strongSelf.invitationTableView.isXiaLa){
            [strongSelf.invitationTableView stopPullToRefresh];
        } else {
            [strongSelf.invitationTableView stopFinishScrollingRefresh];
        }

    }];
}


#pragma mark 获取分享信息的接口
-(void)sendRequestToGetShareManager{
    __weak typeof(self)weakSelf = self;
    if (shareRootModel){
        CenterShareImgModel *shareImgModel = [[CenterShareImgModel alloc]init];
        shareImg = [shareImgModel createImgManagerWithShareType:ShareImgTypeOne model:shareRootModel];
    } else {
        [[NetworkAdapter sharedAdapter] centerGetShareInfoWithBlock:^(BOOL isSuccessed, CenterShareSingleModel *shareModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (isSuccessed){
                strongSelf->shareRootModel = shareModel;
                
                CenterShareImgModel *shareImgModel = [[CenterShareImgModel alloc]init];
                strongSelf->shareImg = [shareImgModel createImgManagerWithShareType:ShareImgTypeOne model:shareModel];
            }
        }];
    }
}



@end
