//
//  CenterInvitationHistoryTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/8.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterInvitationHistoryTableViewCell.h"

@interface CenterInvitationHistoryTableViewCell()
@property (nonatomic,strong)UILabel *titlelLabel;
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UILabel *numberFixedLabel;
@property (nonatomic,strong)UILabel *timeLabel;

@end

@implementation CenterInvitationHistoryTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titlelLabel = [GWViewTool createLabelFont:@"15" textColor:@"3B3B3B"];
    [self addSubview:self.titlelLabel];
    
    self.numberLabel = [GWViewTool createLabelFont:@"15" textColor:@"3E3E3E"];
    self.numberLabel.font = [self.numberLabel.font boldFont];
    [self addSubview:self.numberLabel];
    
    // fixed
    self.numberFixedLabel = [GWViewTool createLabelFont:@"12" textColor:@"787878"];
    [self addSubview:self.numberFixedLabel];
    
    self.timeLabel = [GWViewTool createLabelFont:@"14" textColor:@"9A9A9A"];
    [self addSubview:self.timeLabel];
}

-(void)setTransferModel:(CenterInvitationHistorySingleModel *)transferModel{
    _transferModel = transferModel;
    // phone
    self.titlelLabel.text = [Tool numberCutting:transferModel.phone];
    CGSize titleSize = [Tool makeSizeWithLabel:self.titlelLabel];
    self.titlelLabel.frame = CGRectMake(LCFloat(17), 0, titleSize.width, titleSize.height);
    
    // 2.time
    self.timeLabel.text = [NSDate getTimeWithString:transferModel.register_time / 1000.];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(17) - timeSize.width, 0, timeSize.width, timeSize.height);
    
    // 3. 创建width
    CGFloat margin = self.timeLabel.orgin_x - CGRectGetMaxX(self.titlelLabel.frame);
    
    self.numberLabel.text = [NSString stringWithFormat:@"%li",(long)transferModel.invite_account_reward];
    CGSize numberSize = [Tool makeSizeWithLabel:self.numberLabel];
    
    self.numberFixedLabel.text = @"BBT";
    CGSize numberFixedSize = [Tool makeSizeWithLabel:self.numberFixedLabel];
    
    CGFloat origin_x = (margin - numberSize.width - numberFixedSize.width) / 2.;
    
    self.numberLabel.frame = CGRectMake(CGRectGetMaxX(self.titlelLabel.frame) + origin_x, 0, numberSize.width, numberSize.height);
    self.numberFixedLabel.frame = CGRectMake(CGRectGetMaxX(self.numberLabel.frame) + 1, 0, numberFixedSize.width, numberFixedSize.height);
    
    
    self.titlelLabel.center_y = [CenterInvitationHistoryTableViewCell calculationCellHeight] / 2.;
    self.numberLabel.center_y = self.titlelLabel.center_y;
    self.numberFixedLabel.center_y = self.titlelLabel.center_y;
    self.timeLabel.center_y = self.titlelLabel.center_y;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(50);
}
@end
