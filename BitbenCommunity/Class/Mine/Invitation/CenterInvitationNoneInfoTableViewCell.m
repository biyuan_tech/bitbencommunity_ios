//
//  CenterInvitationNoneInfoTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/8.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterInvitationNoneInfoTableViewCell.h"

@interface CenterInvitationNoneInfoTableViewCell()
@property (nonatomic,strong)LOTAnimationView *logoImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *subTitleLabel;
@end

@implementation CenterInvitationNoneInfoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{

    self.logoImgView = [LOTAnimationView animationNamed:@"done"];
    self.logoImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(163)) / 2., LCFloat(40), LCFloat(163), LCFloat(163));

    [self addSubview:self.logoImgView];
    self.logoImgView.loopAnimation = YES;
    [self.logoImgView playWithCompletion:^(BOOL animationFinished) {

    }];
    
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"353535"];
    self.titleLabel.text = @"当前没有好友邀请记录";

    self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.logoImgView.frame) + LCFloat(30), kScreenBounds.size.width,[NSString contentofHeightWithFont:self.titleLabel.font]);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    self.subTitleLabel = [GWViewTool createLabelFont:@"15" textColor:@"353535"];
    self.subTitleLabel.text = @"赶紧邀请好友获取 奖励吧";
    self.subTitleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.subTitleLabel.font]);
    self.subTitleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.subTitleLabel];
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(40);
    cellHeight += LCFloat(163);
    cellHeight += LCFloat(30);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"15"]] * 2;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(11);
    return cellHeight;
}



@end
