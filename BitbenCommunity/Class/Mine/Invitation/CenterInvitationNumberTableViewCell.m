//
//  CenterInvitationNumberTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/30.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterInvitationNumberTableViewCell.h"

@interface CenterInvitationNumberTableViewCell()
@property (nonatomic,strong)CenterInvitationNumberSingleView *leftView;
@property (nonatomic,strong)CenterInvitationNumberSingleView *rightView;
@property (nonatomic,strong)UIView *lineView;


@end

@implementation CenterInvitationNumberTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.leftView = [[CenterInvitationNumberSingleView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width / 2., LCFloat(88))];;
    [self addSubview:self.leftView];
    
    self.rightView = [[CenterInvitationNumberSingleView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width / 2., 0, self.leftView.size_width, self.leftView.size_height)];
    [self addSubview:self.rightView];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.lineView.frame = CGRectMake(kScreenBounds.size.width / 2., LCFloat(88) / 3. , .5f, LCFloat(88) / 3.);
    [self addSubview:self.lineView];
}

-(void)setTransferSingleModel:(CenterInvitationSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    self.leftView.fixedLabel.text = @"已邀请好友数量";
    if ([AccountModel sharedAccountModel].isShenhe){
        self.rightView.fixedLabel.text = @"累计奖励积分";
    } else {
        self.rightView.fixedLabel.text = @"累计奖励BBT";
    }
    
    self.leftView.dymicLabel.text = [NSString stringWithFormat:@"%li",(long)transferSingleModel.count];
    self.rightView.dymicLabel.text = [NSString stringWithFormat:@"%li",(long)transferSingleModel.reward];
}

-(void)setTransferTaskModel:(TaskHeaderInfoModel *)transferTaskModel{
    _transferTaskModel = transferTaskModel;
    self.leftView.fixedLabel.text = @"已领BP";
    self.rightView.fixedLabel.text = @"增长声望值";
    
    self.leftView.dymicLabel.text = [NSString stringWithFormat:@"%@",transferTaskModel.fund];
    self.rightView.dymicLabel.text = [NSString stringWithFormat:@"%@",transferTaskModel.prestige];
}

+(CGFloat)calculationCellHeight{
    return LCFloat(88);
}

@end











@interface CenterInvitationNumberSingleView()

@end

@implementation CenterInvitationNumberSingleView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.fixedLabel = [GWViewTool createLabelFont:@"13" textColor:@"浅灰"];
    [self addSubview:self.fixedLabel];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"23" textColor:@"黑"];
    self.dymicLabel.font = [self.dymicLabel.font boldFont];
    [self addSubview:self.dymicLabel];
    
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    self.dymicLabel.textAlignment = NSTextAlignmentCenter;
    
    self.fixedLabel.frame = CGRectMake(0, LCFloat(24), kScreenBounds.size.width / 2., [NSString contentofHeightWithFont:self.fixedLabel.font]);
    self.dymicLabel.frame = CGRectMake(0, CGRectGetMaxY(self.fixedLabel.frame) + LCFloat(9), kScreenBounds.size.width / 2., [NSString contentofHeightWithFont:self.dymicLabel.font]);
}

@end
