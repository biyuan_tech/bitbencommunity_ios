//
//  ShareRootViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/14.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"


@interface ShareCollectionModel : FetchModel
@property (nonatomic,copy)NSString *theme_id;
@property (nonatomic,assign)BOOL hasCollection;

@end


@interface ShareRootViewController : UIViewController

// 相关属性
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势
@property (nonatomic,strong)UIImage *showImgBackgroundImage;                // 背景图片
@property (nonatomic,assign)BOOL hasJubao;                              // 是否有举报
@property (nonatomic,assign)BOOL hasCopy;                              // 是否有复制
@property (nonatomic,assign)BOOL hasCollection;                              // 是否有收藏
@property (nonatomic,copy)NSString *transferCopyUrl;                        // 传递过去的URL
@property (nonatomic,assign)BOOL collection_status;                             // 传递过去是否收藏
@property (nonatomic,copy)ShareCollectionModel *shareInfoModel;                        // 传递过去的URL

- (void)showInView:(UIViewController *)viewController;
- (void)dismissFromView:(UIViewController *)viewController;
- (void)hideParentViewControllerTabbar:(UIViewController *)viewController;              // 毛玻璃效果

-(void)actionClickWithShareBlock:(void(^)(NSString *type))block;
-(void)actionClickWithJubaoBlock:(void(^)())block;
-(void)actionClickWithCopyBlock:(void(^)())block;                   // 复制链接
-(void)actionClickWithCollectionBlock:(void(^)())block;                   // 复制链接

@end
