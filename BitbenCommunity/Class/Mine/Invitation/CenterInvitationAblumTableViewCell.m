//
//  CenterInvitationAblumTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/30.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterInvitationAblumTableViewCell.h"
#import "CenterShareImgModel.h"

@interface CenterInvitationAblumTableViewCell()
@property (nonatomic,strong)PDImageView *ablumImgView;
@end

@implementation CenterInvitationAblumTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.ablumImgView = [[PDImageView alloc]init];
    self.ablumImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [CenterInvitationAblumTableViewCell calculationCellHeight]);
    [self addSubview:self.ablumImgView];
    if ([AccountModel sharedAccountModel].isShenhe){
        self.ablumImgView.image = [UIImage imageNamed:@"center_yaoqing_bg"];
    } else {
        self.ablumImgView.image = [UIImage imageNamed:@"bg_center_yaoqing"];
    }
}


+(CGFloat)calculationCellHeight{
    return LCFloat(210);
}

@end
