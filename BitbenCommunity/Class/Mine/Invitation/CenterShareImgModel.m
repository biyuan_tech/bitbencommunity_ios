//
//  CenterShareImgModel.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/22.
//  Copyright © 2018 币本. All rights reserved.
//

#import "CenterShareImgModel.h"
#import "CenterShareImgManager.h"


@implementation CenterShareImgModel

#pragma mark - createImgManager
-(UIImage *)createImgManagerWithShareType:(ShareImgType)type model:(CenterShareSingleModel *)model{
    UIImage *baseImg;
    if (type == ShareImgTypeOne){
        baseImg = [UIImage imageNamed:@"bg_invitation_share"];
    }
    
    UIImage *image = baseImg;
    /*
     size：位图上下文尺寸（新图片的尺寸）
     opaque：不透明YES，透明NO，通常一般是透明上下文
     scale：缩放，通常不需要缩放，取值为0表示不缩放，
     */
    UIGraphicsBeginImageContextWithOptions(image.size, YES, 0);
    
//    //2、绘制原生图片
    [image drawAtPoint:CGPointZero];
    
    //给原生图片添加图片
    UIImage *imageMack = [self createErweimaWithInfo:model.url];
    [imageMack drawInRect:CGRectMake((image.size.width - imageMack.size.width) / 2., image.size.height - imageMack.size.height - 108, imageMack.size.width, imageMack.size.height)];
    

    //4、从上下文中获取生成新图片
    UIImage *getImage = UIGraphicsGetImageFromCurrentImageContext();
    
    //5、关闭上下文
    UIGraphicsEndImageContext();
    
    
    
    //    //3、给原生图片添加文字
//    NSString *str = [AccountModel sharedAccountModel].loginServerModel.account.my_invitation_code;
//    NSDictionary *dic = @{ NSForegroundColorAttributeName : [UIColor whiteColor],
//                           NSFontAttributeName : [UIFont fontWithCustomerSizeName:@"36."],};
//    CGFloat fontSizeHeight = [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"36."]];
//    CGSize strSize = [str sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"36."] constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontSizeHeight)];
//
//    [str drawAtPoint:CGPointMake((image.size.width - strSize.width) / 2., image.size.height - 2 * LCFloat(11) - fontSizeHeight) withAttributes:dic];
//
    UIImage *backImg = [self createShareImgWithStr:[AccountModel sharedAccountModel].loginServerModel.account.my_invitation_code img:getImage];
    UIImage *mainImg = [self compressImage:backImg toByte:1024 * 25];

    
    return mainImg;
}


#pragma mark - 根据信息变成二维码
-(UIImage *)createErweimaWithInfo:(NSString *)infoUrl{
    // 1.创建过滤器
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];

    // 2.恢复默认
    [filter setDefaults];
    
    // 3.给过滤器添加数据(正则表达式/账号和密码)
    NSString *dataString = infoUrl;
    NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKeyPath:@"inputMessage"];
    
    // 4.获取输出的二维码
    CIImage *outputImage = [filter outputImage];
    
    //因为生成的二维码模糊，所以通过createNonInterpolatedUIImageFormCIImage:outputImage来获得高清的二维码图片
    
    // 5.显示二维码
    UIImage *img = [self createNonInterpolatedUIImageFormCIImage:outputImage withSize:121];

    return img;
}


-(UIImage *)createShareImgWithStr:(NSString *)str img:(UIImage *)img{
    //获取要加工的图片
    UIImage *image = img;
    CGSize size= CGSizeMake (image.size . width , image.size . height ); // 画布大小
    UIGraphicsBeginImageContextWithOptions (size, NO , 0.0 );
    [image drawAtPoint : CGPointMake ( 0 , 0 )];
    // 获得一个位图图形上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawPath(context, kCGPathStroke);
    //画自己想画的内容。。。。。
    CGSize infoSize = [str sizeWithCalcFont:[UIFont systemFontOfCustomeSize:36] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:36] ])];
    
    [str drawAtPoint : CGPointMake ( (kScreenBounds.size.width - infoSize.width) / 2. , img.size.height - LCFloat(332) - infoSize.height + LCFloat(10)) withAttributes : @{ NSFontAttributeName :[ UIFont fontWithName : @"Arial-BoldMT" size : 36], NSForegroundColorAttributeName :[ UIColor hexChangeFloat:@"562F24"]}];
    
    // 返回绘制的新图形
    UIImage *newImage= UIGraphicsGetImageFromCurrentImageContext ();
    
    UIGraphicsEndImageContext ();
    
    return newImage;
}


/**
 *  根据CIImage生成指定大小的UIImage
 *
 *  @param image CIImage
 *  @param size  图片宽度
 */
- (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat) size
{
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    
    // 1.创建bitmap;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    
    // 2.保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    return [UIImage imageWithCGImage:scaledImage];
}




#pragma mark - createImgManager
+(UIImage *)createImgManagerWithPlacehorderWithRect:(CGSize)size{
//    UIImage *baseImg;
//    CGSize mainSize = size;
//    if (size.width <= 0 || size.height <= 0){
//        mainSize = CGSizeMake(1, 1);
//    }
//    baseImg = [Tool createImageWithColor:RGB(234, 234, 234, 1) frame:CGRectMake(0, 0, mainSize.width, mainSize.height)];
//    UIImage *image = baseImg;
//
//    /*
//     size：位图上下文尺寸（新图片的尺寸）
//     opaque：不透明YES，透明NO，通常一般是透明上下文
//     scale：缩放，通常不需要缩放，取值为0表示不缩放，
//     */
//    UIGraphicsBeginImageContextWithOptions(size, YES, 0);
//
//    //2、绘制原生图片
//    [image drawAtPoint:CGPointZero];
//
//    //给原生图片添加图片
//    UIImage *imageMack = [UIImage imageNamed:@"placehorder"];
//    [imageMack drawInRect:CGRectMake((size.width - imageMack.size.width) / 2., (size.height - imageMack.size.height) / 2., imageMack.size.width, imageMack.size.height)];
//
//    //    //3、给原生图片添加文字
//    //    NSString *str = @"币本";
//    //    NSDictionary *dic = @{ NSForegroundColorAttributeName : [UIColor whiteColor],
//    //                           NSFontAttributeName : [UIFont fontWithCustomerSizeName:@"20."],};
//    //    CGFloat fontSizeHeight = [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"20."]];
//    //    CGSize strSize = [str sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"20."] constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontSizeHeight)];
//
//    //    [str drawAtPoint:CGPointMake((image.size.width - strSize.width) / 2., image.size.height - 2 * LCFloat(11) - fontSizeHeight) withAttributes:dic];
//
//    //4、从上下文中获取生成新图片
//    UIImage *getImage = UIGraphicsGetImageFromCurrentImageContext();
//
//    //5、关闭上下文
//    UIGraphicsEndImageContext();
//
//    return getImage;
     UIImage *imageMack = [UIImage imageNamed:@"placehorder_bg.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(1, 1 ,1, 1);
    // 拉伸重新给image赋值
    UIImage *newimage = [imageMack resizableImageWithCapInsets:insets];
    
    
    return  newimage;
}



#pragma mark - 压缩图片
- (UIImage *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength {
    // Compress by quality
    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    if (data.length < maxLength) return image;
    
    CGFloat max = 1;
    CGFloat min = 0;
    for (int i = 0; i < 6; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(image, compression);
        if (data.length < maxLength * 0.9) {
            min = compression;
        } else if (data.length > maxLength) {
            max = compression;
        } else {
            break;
        }
    }
    UIImage *resultImage = [UIImage imageWithData:data];
    if (data.length < maxLength) return resultImage;
    
    // Compress by size
    NSUInteger lastDataLength = 0;
    while (data.length > maxLength && data.length != lastDataLength) {
        lastDataLength = data.length;
        CGFloat ratio = (CGFloat)maxLength / data.length;
        CGSize size = CGSizeMake((NSUInteger)(resultImage.size.width * sqrtf(ratio)),
                                 (NSUInteger)(resultImage.size.height * sqrtf(ratio))); // Use NSUInteger to prevent white blank
        UIGraphicsBeginImageContext(size);
        [resultImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        data = UIImageJPEGRepresentation(resultImage, compression);
    }
    
    return resultImage;
}



-(UIImage *)mainManager:(CenterShareSingleModel *)model{
    CenterShareImgsModel *view1 = [[CenterShareImgsModel alloc]init];
    UIImage *im1 = [UIImage imageNamed:@"bg_share_img_title"];
    view1.views = im1;
    view1.frame = CGRectMake(LCFloat(28), LCFloat(27), LCFloat(156), LCFloat(34));
    
    
    UIImage *im2 = [UIImage imageNamed:@"bg_share_hongbao"];
    CenterShareImgsModel *View2 = [[CenterShareImgsModel alloc]init];
    View2.views = im2;
    View2.frame = CGRectMake(LCFloat(42), CGRectGetMaxY(view1.frame) + LCFloat(50), kScreenBounds.size.width - LCFloat(42) - LCFloat(21), LCFloat(431));
    
    CenterShareImgsModel *View3 = [[CenterShareImgsModel alloc]init];
    UILabel *hongbaoTitleLabel = [GWViewTool createLabelFont:@"22" textColor:@"白"];
    hongbaoTitleLabel.text = @"送你一个红包最高￥110";
    CGSize hongBaoTitleSize = [Tool makeSizeWithLabel:hongbaoTitleLabel];
    View3.views = hongbaoTitleLabel;
    View3.frame = CGRectMake((kScreenBounds.size.width - hongBaoTitleSize.width) / 2., View2.frame.origin.y + LCFloat(37), hongBaoTitleSize.width, [NSString contentofHeightWithFont:hongbaoTitleLabel.font]);
    
    CenterShareImgsModel *view4 = [[CenterShareImgsModel alloc]init];
    UILabel *nameTitleLabel = [GWViewTool createLabelFont:@"15" textColor:@"白"];
    nameTitleLabel.text = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
    CGSize nameTitleLabelSize = [Tool makeSizeWithLabel:nameTitleLabel];
    view4.views = nameTitleLabel;
    view4.frame = CGRectMake((kScreenBounds.size.width - nameTitleLabelSize.width) / 2., View2.frame.origin.y + LCFloat(81), nameTitleLabelSize.width, nameTitleLabelSize.height);
    
    
    CenterShareImgsModel *view5 = [[CenterShareImgsModel alloc]init];
    UIImage *avatarBgImg = [UIImage imageNamed:@"bg_Share_img_avatar_bottom"];;
    view5.views = avatarBgImg;
    view5.frame = CGRectMake((kScreenBounds.size.width - LCFloat(67)) / 2.,CGRectGetMaxY(view4.frame) + LCFloat(14),LCFloat(67),LCFloat(67));
    
    CenterShareImgsModel *view6 = [[CenterShareImgsModel alloc]init];
    UIImage *avatarImg = [[AccountModel sharedAccountModel].userAvatarImg addCornerRadius:30 size:CGSizeMake(60, 60)];
    view6.views = avatarImg;
    view6.frame = CGRectMake((kScreenBounds.size.width - 60) / 2.,view5.frame.origin.y + (LCFloat(67) - 60) / 2. ,60,60);
    
    
    
    CenterShareImgsModel *view7 = [[CenterShareImgsModel alloc]init];
    UILabel *yaoqingmaLabel = [GWViewTool createLabelFont:@"15" textColor:@"白"];
    yaoqingmaLabel.text = @"邀请码";
    CGSize yaoqingmaLabelSize = [Tool makeSizeWithLabel:yaoqingmaLabel];
    view7.views = yaoqingmaLabel;
    view7.frame = CGRectMake((kScreenBounds.size.width - yaoqingmaLabelSize.width) / 2., CGRectGetMaxY(view5.frame) + LCFloat(20), yaoqingmaLabelSize.width, yaoqingmaLabelSize.height);
    
    CenterShareImgsModel *view8 = [[CenterShareImgsModel alloc]init];
    UILabel *yaoqingmaDescLabel = [GWViewTool createLabelFont:@"29" textColor:@"白"];
    yaoqingmaDescLabel.font = [yaoqingmaDescLabel.font boldFont];
    yaoqingmaDescLabel.text = [AccountModel sharedAccountModel].loginServerModel.account.my_invitation_code;
    CGSize yaoqingmaDescLabelSize = [Tool makeSizeWithLabel:yaoqingmaDescLabel];
    view8.views = yaoqingmaDescLabel;
    view8.frame = CGRectMake((kScreenBounds.size.width - yaoqingmaDescLabelSize.width) / 2., CGRectGetMaxY(view7.frame) + LCFloat(9), yaoqingmaDescLabelSize.width, yaoqingmaDescLabelSize.height);
    
    CenterShareImgsModel *view9 = [[CenterShareImgsModel alloc]init];
    UIImage *code = [CenterShareImgManager createErweimaWithInfo:model.url];
    view9.views = code ;
    view9.frame = CGRectMake((kScreenBounds.size.width - LCFloat(80)) / 2., CGRectGetMaxY(view8.frame) + LCFloat(22), LCFloat(80), LCFloat(80));
    
    CenterShareImgsModel *view10 = [[CenterShareImgsModel alloc]init];
    UILabel *ruheHongbaoLabel = [GWViewTool createLabelFont:@"15" textColor:@"白"];
    ruheHongbaoLabel.font = [ruheHongbaoLabel.font boldFont];
    ruheHongbaoLabel.text = @"如何领取红包?";
    view10.views = ruheHongbaoLabel;
    CGSize ruheHongbaoSize=  [Tool makeSizeWithLabel:ruheHongbaoLabel];
    view10.frame = CGRectMake(LCFloat(28), CGRectGetMaxY(View2.frame) + LCFloat(7), ruheHongbaoSize.width, ruheHongbaoSize.height);
    
    
    CenterShareImgsModel *view11 = [[CenterShareImgsModel alloc]init];
    UILabel *ruheHongbaoLabel1 = [GWViewTool createLabelFont:@"13" textColor:@"白"];
    ruheHongbaoLabel1.text = @"1.扫码领取红包";
    view11.views = ruheHongbaoLabel1;
    CGSize ruheHongbaoLabel1Size=  [Tool makeSizeWithLabel:ruheHongbaoLabel1];
    view11.frame = CGRectMake(LCFloat(28), CGRectGetMaxY(view10.frame) + LCFloat(7), ruheHongbaoLabel1Size.width, ruheHongbaoLabel1Size.height);
    
    CenterShareImgsModel *view12 = [[CenterShareImgsModel alloc]init];
    UILabel *ruheHongbaoLabel2 = [GWViewTool createLabelFont:@"13" textColor:@"白"];
    ruheHongbaoLabel2.text = @"2.下载币本App,注册登录";
    view12.views = ruheHongbaoLabel2;
    CGSize ruheHongbaoLabel2Size=  [Tool makeSizeWithLabel:ruheHongbaoLabel2];
    view12.frame = CGRectMake(LCFloat(28), CGRectGetMaxY(view11.frame) + LCFloat(7), ruheHongbaoLabel2Size.width, ruheHongbaoLabel2Size.height);
    
    
    CenterShareImgsModel *view13 = [[CenterShareImgsModel alloc]init];
    UILabel *ruheHongbaoLabel3 = [GWViewTool createLabelFont:@"13" textColor:@"白"];
    ruheHongbaoLabel3.text = @"3.进入“我的”页面，红包自动发放至“我的资产”中";
    view13.views = ruheHongbaoLabel3;
    CGSize ruheHongbaoLabel3Size = [Tool makeSizeWithLabel:ruheHongbaoLabel3];
    view13.frame = CGRectMake(LCFloat(28), CGRectGetMaxY(view12.frame) + LCFloat(7), ruheHongbaoLabel3Size.width, ruheHongbaoLabel3Size.height);
    
    NSArray <CenterShareImgsModel>* shareArr = [[NSArray <CenterShareImgsModel> alloc]init];
    shareArr = [@[view1,View2,View3,view4,view5,view6,view7,view8,view9,view10,view11,view12,view13] copy];
    UIImage *img = [CenterShareImgManager createShareImgWithBgImg:[UIImage imageNamed:@"bg_share_code_img"] viewArr:shareArr block:^(UIImage * _Nonnull img){
    }];
    return img;
}

-(UIImage *)createKuaixunImgWithModelWithModel:(BYHomeNewsletterModel *)model{
    UIImage *img;
    CGFloat margin = LCFloat(30);
    CGFloat main_width = kScreenBounds.size.width - 2 * margin;
    // 1. 封面
    UIImage *headerImg = [UIImage imageNamed:@"bg_kuaixun_share_header"];
    CenterShareImgsModel *View1 = [[CenterShareImgsModel alloc]init];
    View1.views = headerImg;
    View1.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(200));
    
    // 2. clock
    UIImage *clockImg = [UIImage imageNamed:@"icon_liaoxun_share_clock"];
    CenterShareImgsModel *View2 = [[CenterShareImgsModel alloc]init];
    View2.views = clockImg;
    View2.frame = CGRectMake(LCFloat(30), CGRectGetMaxY(View1.frame) + LCFloat(20), LCFloat(18), LCFloat(18));
    
    // 3. time
    CenterShareImgsModel *View3 = [[CenterShareImgsModel alloc]init];
    UILabel *label3 = [GWViewTool createLabelFont:@"13" textColor:@"黑"];
    label3.text = [NSDate getKuaixunImageString:model.create_time / 1000.];
//    [NSDate getCurrentTimeWithKuaixunShifen:model.create_time / 1000.];
    CGSize label3Size = [Tool makeSizeWithLabel:label3];
    View3.views = label3;
    View3.frame = CGRectMake(CGRectGetMaxX(View2.frame) + LCFloat(11), View2.frame.origin.y + (View2.frame.size.height - label3Size.height) / 2., label3Size.width, label3Size.height);
    
   // 4. 标题
    CenterShareImgsModel *View4 = [[CenterShareImgsModel alloc]init];
    UILabel *label4 = [GWViewTool createLabelFont:@"18" textColor:@"黑"];
    label4.font = [label4.font boldFont];
    label4.text = model.title;
    label4.numberOfLines = 0;
    View4.views = label4;
    
    CGFloat infoHeight = [UILabel hangjianjugetSpaceLabelHeight:model.title withFont:label4.font withWidth:main_width limitHeight:CGFLOAT_MAX];
    View4.frame = CGRectMake(margin, CGRectGetMaxY(View3.frame) + LCFloat(20), main_width, infoHeight);
    View4.hangjianju = LCFloat(6);
    
    CenterShareImgsModel *View5 = [[CenterShareImgsModel alloc]init];
    UILabel *label5 = [GWViewTool createLabelFont:@"14" textColor:@"黑"];
    label5.text = model.content;
    label5.numberOfLines = 0;
    View5.views = label5;
    CGFloat dymicHeight = [UILabel hangjianjugetSpaceLabelHeight:model.content withFont:label5.font withWidth:main_width limitHeight:CGFLOAT_MAX];
    View5.frame = CGRectMake(LCFloat(30), CGRectGetMaxY(View4.frame) + LCFloat(20), main_width, dymicHeight);
    View5.hangjianju = LCFloat(6);
    
    // 2. clock
    CenterShareImgsModel *ViewAblum = [[CenterShareImgsModel alloc]init];
    ViewAblum.views = model.ablum;
    // 1. 定义高度一定是150
    CGFloat imgRealProgress = main_width * 1.0 / model.ablum.size.width;
    CGFloat imgHeight = model.ablum.size.height * imgRealProgress;
    
    ViewAblum.frame = CGRectMake(margin, CGRectGetMaxY(View5.frame) + LCFloat(15), main_width, imgHeight);
    
    CGFloat nextOriginY = 0;
    if (model.ablum){
        nextOriginY = CGRectGetMaxY(ViewAblum.frame) + LCFloat(15);
    } else {
        nextOriginY = CGRectGetMaxY(View5.frame) + LCFloat(30);
    }
    
    // 插入
    CenterShareImgsModel *View51 = [[CenterShareImgsModel alloc]init];
    UILabel *label51 = [GWViewTool createLabelFont:@"12" textColor:@"AFAEAE"];
    label51.text = @"免责声明：本文不代表币本立场，且不构成投资建议，请谨慎对待。";
    label51.numberOfLines = 0;
    View51.views = label51;
    CGSize tishiSize = [label51.text sizeWithCalcFont:label51.font constrainedToSize:CGSizeMake(main_width, CGFLOAT_MAX)];
    
    View51.frame = CGRectMake(LCFloat(30),nextOriginY, main_width, tishiSize.height);
    

    
    
    // 利好
    CenterShareImgsModel *View6 = [[CenterShareImgsModel alloc]init];
    View6.frame = CGRectMake(LCFloat(30),CGRectGetMaxY(View51.frame) + LCFloat(15) , LCFloat(14), LCFloat(16));
    UIImage *img6 = [UIImage imageNamed:@"icon_liaoxun_up_hlt"];
    View6.views = img6;
    
    CenterShareImgsModel *View7 = [[CenterShareImgsModel alloc]init];
    UILabel *label7 = [GWViewTool createLabelFont:@"13" textColor:@"DA1A00"];
    label7.textColor = [UIColor hexChangeFloat:@"4DCC86"];
    label7.text = [NSString stringWithFormat:@"利好 %li",(long)model.count_support];
    CGSize label7Size = [Tool makeSizeWithLabel:label7];
    View7.views = label7;
    View7.frame = CGRectMake(CGRectGetMaxX(View6.frame) + LCFloat(4), View6.frame.origin.y, label7Size.width, LCFloat(20));
    
    // 利空
    CenterShareImgsModel *View8 = [[CenterShareImgsModel alloc]init];
    UILabel *label8 = [GWViewTool createLabelFont:@"13" textColor:@"2366FF"];
    label8.textColor = [UIColor hexChangeFloat:@"CC5F4D"];
    label8.text = [NSString stringWithFormat:@"利空 %li",(long)model.count_tread];
    CGSize label8Size = [Tool makeSizeWithLabel:label8];
    View8.views = label8;
    View8.frame = CGRectMake(kScreenBounds.size.width - LCFloat(30) - label8Size.width, View6.frame.origin.y, label8Size.width, LCFloat(20));
    
    CenterShareImgsModel *View9 = [[CenterShareImgsModel alloc]init];
    UIImage *img9 = [UIImage imageNamed:@"icon_liaoxun_down_hlt"];
    View9.views = img9;
    View9.frame = CGRectMake(View8.frame.origin.x - LCFloat(20), View6.frame.origin.y, LCFloat(14), LCFloat(16));
    
    CGFloat lineWidth = kScreenBounds.size.width - 2 * LCFloat(30);
    NSInteger count = model.count_tread + model.count_support;
    CGFloat leftWidth = (model.count_tread * 1.0) / count * lineWidth;
    CGFloat rightWith = lineWidth - leftWidth;
    
    CenterShareImgsModel *View10 = [[CenterShareImgsModel alloc]init];
    View10.frame = CGRectMake(LCFloat(30),CGRectGetMaxY(View8.frame) + LCFloat(10),rightWith,5);
    UIImage *img10 = [UIImage imageNamed:@"bg_liaoxun_red"];
    View10.views = img10;
    
    CenterShareImgsModel *View11 = [[CenterShareImgsModel alloc]init];
    View11.frame = CGRectMake(CGRectGetMaxX(View10.frame) + 2,CGRectGetMaxY(View8.frame) + LCFloat(10),leftWidth,5);
    UIImage *img11 = [UIImage imageNamed:@"bg_liaoxun_blue"];
     View11.views = img11;
    
    
    // 二维码
    CenterShareImgsModel *View12 = [[CenterShareImgsModel alloc]init];
    UIImage *img12 = [UIImage imageNamed:@"kuaixun_erweima"];
    View12.views = img12;
    View12.frame = CGRectMake(LCFloat(42), CGRectGetMaxY(View8.frame) + LCFloat(45), LCFloat(80), LCFloat(80));
    
    // 13 区块链投资 上币本
    CenterShareImgsModel *View13 = [[CenterShareImgsModel alloc]init];
    UILabel *label13 = [GWViewTool createLabelFont:@"14" textColor:@"000000"];
    label13.textColor = [UIColor hexChangeFloat:@"000000"];
    label13.text = @"区块链投资 就上币本APP";
    CGSize label13Size = [Tool makeSizeWithLabel:label13];
    View13.views = label13;
    View13.frame = CGRectMake(CGRectGetMaxX(View12.frame) + LCFloat(15), View12.frame.origin.y, label13Size.width, label13Size.height);
        
    // 14. 10万人领取
    CenterShareImgsModel *View14 = [[CenterShareImgsModel alloc]init];
    UILabel *label14 = [GWViewTool createLabelFont:@"14" textColor:@"000000"];
    label14.textColor = [UIColor hexChangeFloat:@"000000"];
    label14.text = @"已有10万人领取 立即扫码领取";
    CGSize label14Size = [Tool makeSizeWithLabel:label14];
    View14.views = label14;
    View14.frame = CGRectMake(CGRectGetMaxX(View12.frame) + LCFloat(15), CGRectGetMaxY(View13.frame) + LCFloat(10), label14Size.width, label14Size.height);
    
    // 15 注册送
    CenterShareImgsModel *View15 = [[CenterShareImgsModel alloc]init];
    UILabel *label15 = [GWViewTool createLabelFont:@"14" textColor:@"000000"];
    label15.textColor = [UIColor hexChangeFloat:@"000000"];
    label15.text = @"注册就送";
    CGSize label15Size = [Tool makeSizeWithLabel:label15];
    View15.views = label15;
    View15.frame = CGRectMake(CGRectGetMaxX(View12.frame) + LCFloat(15), CGRectGetMaxY(View14.frame) + LCFloat(16), label15Size.width, label15Size.height);
    // 16 2000
    CenterShareImgsModel *View16 = [[CenterShareImgsModel alloc]init];
    UILabel *label16 = [GWViewTool createLabelFont:@"22" textColor:@"DA1A00"];
    label16.font = [label16.font boldFont];
    label16.textColor = [UIColor hexChangeFloat:@"DA1A00"];
    label16.text = model.register_reward;
    CGSize label16Size = [Tool makeSizeWithLabel:label16];
    View16.views = label16;
    View16.frame = CGRectMake(CGRectGetMaxX(View15.frame), View15.frame.origin.y - LCFloat(6) , label16Size.width, label16Size.height);
    
    // 17 币本糖果
    CenterShareImgsModel *View17 = [[CenterShareImgsModel alloc]init];
    UILabel *label17 = [GWViewTool createLabelFont:@"14" textColor:@"000000"];
    label17.textColor = [UIColor hexChangeFloat:@"000000"];
    label17.text = @"币本糖果";
    CGSize label17Size = [Tool makeSizeWithLabel:label17];
    View17.views = label17;
    View17.frame = CGRectMake(CGRectGetMaxX(View16.frame), View15.frame.origin.y, label17Size.width, label17Size.height);
    
    NSArray <CenterShareImgsModel>* shareArr = [[NSArray <CenterShareImgsModel> alloc]init];
    if (!model.ablum){
        shareArr = [@[View1,View2,View3,View4,View5,View51,View6,View7,View8,View9,View10,View11,View12,View13,View14,View15,View16,View17] copy];

    } else {
        shareArr = [@[View1,View2,View3,View4,View5,ViewAblum,View51,View6,View7,View8,View9,View10,View11,View12,View13,View14,View15,View16,View17] copy];
    }
    
    CGRect mainRect = CGRectMake(0, 0, kScreenBounds.size.width, CGRectGetMaxY(View17.frame) + LCFloat(30));
    img = [CenterShareImgManager createShareImgWithBgImg:[Tool createImageWithColor:[UIColor whiteColor] frame:mainRect] viewArr:shareArr block:^(UIImage * _Nonnull img){
    }];
    
    [label17 hangjianjusetLabelSpace];
    return img;
}
@end
