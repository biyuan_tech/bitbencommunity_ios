//
//  VersionInvitationLeijiJiangliViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,VersionInvitationLeijiJiangliViewControllerType) {
    VersionInvitationLeijiJiangliViewControllerTypeLeijijiangli,
    VersionInvitationLeijiJiangliViewControllerTypeYaoqingjilu,
    VersionInvitationLeijiJiangliViewControllerTypeJiasujilu,
};

@interface VersionInvitationLeijiJiangliViewController : AbstractViewController

@property (nonatomic,assign)VersionInvitationLeijiJiangliViewControllerType transferType;


@end

NS_ASSUME_NONNULL_END
