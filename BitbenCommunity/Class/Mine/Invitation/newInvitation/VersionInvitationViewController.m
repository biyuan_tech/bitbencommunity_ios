//
//  VersionInvitationViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "VersionInvitationViewController.h"
#import "VersionInvitationHeaderCell.h"
#import "VersionInvitationTitleCell.h"
#import "VersionInvitationItemCell1.h"
#import "VersionInvitationBuzhouTableViewCell.h"
#import "VersionInvitationJiasuCell.h"
#import "VersionInvitationPaihangbangNormalTableViewCell.h"
#import "VersionInvitationLeijiJiangliViewController.h"
#import "NetworkAdapter+VersionInvitation.h"
#import "NetworkAdapter+Center.h"
#import "CenterShareImgModel.h"
#import "ShareRootViewController.h"
#import "CenterShareImgManager.h"


typedef NS_ENUM(NSInteger,InvitationShareType) {
    InvitationShareTypeUrl,
    InvitationShareTypeImg,
};

@interface VersionInvitationViewController ()<UITableViewDelegate,UITableViewDataSource>{
    VersionInvitationRootModel * _Nonnull mainModel;
    NSInteger page;
    UIImage *shareImg;
    CenterShareSingleModel *shareRootModel;
}
@property (nonatomic,strong)UITableView *invitationTableView;
@property (nonatomic,strong)NSMutableArray *invitationMutableArr;
@property (nonatomic,strong)NSMutableArray *paihangbangMutableArr;
@property (nonatomic,strong)UIButton *actionButton;
@property (nonatomic,strong)UIButton *actionButton2;
@end

@implementation VersionInvitationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createButton];
    [self createTableView];
    [self sendRequestToGetInfoManager];
    [self getListManagerWithAddPage:NO];
    [self sendRequestToGetShareManager];
}

#pragma mark - createView
-(void)createButton{
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.actionButton setTitle:@"链接邀请" forState:UIControlStateNormal];
    self.actionButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"17"];
    self.actionButton.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(55) - [BYTabbarViewController sharedController].navBarHeight, kScreenBounds.size.width / 2., LCFloat(55));
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        // 埋点
        NSDictionary *params = @{@"邀请人":[AccountModel sharedAccountModel].account_id};
        [MTAManager event:MTATypeInvitation params:params];
        
        [strongSelf actionShowShareInfoManager:shareRootModel type:InvitationShareTypeUrl];
    }];
    self.actionButton.backgroundColor = [UIColor hexChangeFloat:@"FFECE5"];
    [self.actionButton setTitleColor:[UIColor hexChangeFloat:@"E75E33"] forState:UIControlStateNormal];
    [self.view addSubview:self.actionButton];
    
    
    // 邀请2
    self.actionButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton2.backgroundColor = [UIColor hexChangeFloat:@"FFC522"];
    self.actionButton2.frame = CGRectMake(CGRectGetMaxX(self.actionButton.frame), self.actionButton.orgin_y, self.actionButton.size_width, self.actionButton.size_height);
    [self.actionButton2 setTitle:@"海报邀请" forState:UIControlStateNormal];
    [self.actionButton2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.actionButton2 buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        // 埋点
        NSDictionary *params = @{@"邀请人":[AccountModel sharedAccountModel].account_id};
        [MTAManager event:MTATypeInvitation params:params];
        
        [strongSelf actionShowShareInfoManager:shareRootModel type:InvitationShareTypeImg];
    }];
    [self.view addSubview:self.actionButton2];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"邀请好友";
    self.view.backgroundColor = RGB(231, 93, 50, 1);
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    page = 0;
    self.invitationMutableArr = [NSMutableArray array];
    self.paihangbangMutableArr = [NSMutableArray array];
    [self.paihangbangMutableArr addObjectsFromArray:@[@"排行榜标题",@"我的排行"]];

//    @[@"加速"],@[@"加速item"],
    [self.invitationMutableArr addObjectsFromArray:@[@[@"1"],@[@"items"],@[@"奖金"],@[@"步骤"],@[@"排行榜"],self.paihangbangMutableArr]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.invitationTableView){
        self.invitationTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.invitationTableView.size_height = kScreenBounds.size.height - self.actionButton.size_height;
        self.invitationTableView.dataSource = self;
        self.invitationTableView.delegate = self;
        [self.view addSubview:self.invitationTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.invitationMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.invitationMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        VersionInvitationHeaderCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[VersionInvitationHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickGuizeBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strogSelf = weakSelf;
            PDWebViewController *webViewController = [[PDWebViewController alloc]init];
            [webViewController webDirectedWebUrl:page1];
            [strogSelf.navigationController pushViewController:webViewController animated:YES];
        }];
        
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"items" sourceArr:self.invitationMutableArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        VersionInvitationItemCell1 *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[VersionInvitationItemCell1 alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferInvitationModel = mainModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo actionClickBlock:^(NSInteger index) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (index == 0){
                VersionInvitationLeijiJiangliViewController *leijijiangliVC = [[VersionInvitationLeijiJiangliViewController alloc]init];
                leijijiangliVC.transferType = VersionInvitationLeijiJiangliViewControllerTypeLeijijiangli;
                [strongSelf.navigationController pushViewController:leijijiangliVC animated:YES];
            } else if (index == 1){
                VersionInvitationLeijiJiangliViewController *leijijiangliVC = [[VersionInvitationLeijiJiangliViewController alloc]init];
                leijijiangliVC.transferType = VersionInvitationLeijiJiangliViewControllerTypeYaoqingjilu;
                [strongSelf.navigationController pushViewController:leijijiangliVC animated:YES];
            } else if (index == 2){
                return;
                VersionInvitationLeijiJiangliViewController *leijijiangliVC = [[VersionInvitationLeijiJiangliViewController alloc]init];
                leijijiangliVC.transferType = VersionInvitationLeijiJiangliViewControllerTypeJiasujilu;
                [strongSelf.navigationController pushViewController:leijijiangliVC animated:YES];
            }
        }];
        
        return cellWithRowTwo;
    } else  if (indexPath.section == [self cellIndexPathSectionWithcellData:@"奖金" sourceArr:self.invitationMutableArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"加速" sourceArr:self.invitationMutableArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"排行榜" sourceArr:self.invitationMutableArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        VersionInvitationTitleCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[VersionInvitationTitleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"奖金" sourceArr:self.invitationMutableArr]){
            cellWithRowThr.transferType = VersionInvitationTitleCellType1;
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"加速" sourceArr:self.invitationMutableArr]){
            cellWithRowThr.transferType = VersionInvitationTitleCellType2;
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"排行榜" sourceArr:self.invitationMutableArr]){
            cellWithRowThr.transferType = VersionInvitationTitleCellType3;
        }
        
        return cellWithRowThr;
    } else  if (indexPath.section == [self cellIndexPathSectionWithcellData:@"步骤" sourceArr:self.invitationMutableArr]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        VersionInvitationBuzhouTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[VersionInvitationBuzhouTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
        }
        return cellWithRowFour;
    } else  if (indexPath.section == [self cellIndexPathSectionWithcellData:@"加速item" sourceArr:self.invitationMutableArr]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        VersionInvitationJiasuCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[VersionInvitationJiasuCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
        }
        return cellWithRowFour;
    } else {
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"排行榜标题" sourceArr:self.invitationMutableArr]){
            static NSString *cellIdentifyWithRowPaihangbang1 = @"cellIdentifyWithRowPaihangbang1";
            VersionInvitationPaihangbangNormalTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowPaihangbang1];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[VersionInvitationPaihangbangNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowPaihangbang1];
            }
            cellWithRowFiv.transferType = VersionInvitationPaihangbangNormalTableViewCellTypeHeader;
            return cellWithRowFiv;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的排行" sourceArr:self.invitationMutableArr]){
            static NSString *cellIdentifyWithRowPaihangbang2 = @"cellIdentifyWithRowPaihangbang2";
            VersionInvitationPaihangbangNormalTableViewCell *cellWithRowSix = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowPaihangbang2];
            if (!cellWithRowSix){
                cellWithRowSix = [[VersionInvitationPaihangbangNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowPaihangbang2];
            }
            cellWithRowSix.transferModel = mainModel;
            cellWithRowSix.transferType = VersionInvitationPaihangbangNormalTableViewCellTypeTitle;
            return cellWithRowSix;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"查看更多" sourceArr:self.invitationMutableArr]){
            static NSString *cellIdentifyWithRowPaihangbang3 = @"cellIdentifyWithRowPaihangbang3";
            VersionInvitationPaihangbangNormalTableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowPaihangbang3];
            if (!cellWithRowSev){
                cellWithRowSev = [[VersionInvitationPaihangbangNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowPaihangbang3];
            }
            cellWithRowSev.transferType = VersionInvitationPaihangbangNormalTableViewCellTypeBottom;
            return cellWithRowSev;
        } else {
            static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
            VersionInvitationPaihangbangNormalTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
            if (!cellWithRowFour){
                cellWithRowFour = [[VersionInvitationPaihangbangNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
            }
            cellWithRowFour.transferInvitationModel = [[self.invitationMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            cellWithRowFour.transferIndex = indexPath.row;
            cellWithRowFour.transferType = VersionInvitationPaihangbangNormalTableViewCellTypeNormal;
            
            return cellWithRowFour;
        }
    }
}


#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"查看更多" sourceArr:self.invitationMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"查看更多" sourceArr:self.invitationMutableArr]){
        [self getListManagerWithAddPage:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [VersionInvitationHeaderCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"items" sourceArr:self.invitationMutableArr]){
        return [VersionInvitationItemCell1 calculationCellHeight];
    } else  if (indexPath.section == [self cellIndexPathSectionWithcellData:@"奖金" sourceArr:self.invitationMutableArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"加速" sourceArr:self.invitationMutableArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"排行榜" sourceArr:self.invitationMutableArr]){
        return [VersionInvitationTitleCell calculationCellHeight];
    } else  if (indexPath.section == [self cellIndexPathSectionWithcellData:@"步骤" sourceArr:self.invitationMutableArr]){
        return [VersionInvitationBuzhouTableViewCell calculationCellHeight];
    } else  if (indexPath.section == [self cellIndexPathSectionWithcellData:@"加速item" sourceArr:self.invitationMutableArr]){
        return [VersionInvitationJiasuCell calculationCellHeight];
    } else {
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"排行榜标题" sourceArr:self.invitationMutableArr]){
            return [VersionInvitationPaihangbangNormalTableViewCell calculationCellHeightWithType:VersionInvitationPaihangbangNormalTableViewCellTypeHeader];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的排行" sourceArr:self.invitationMutableArr]){
            return [VersionInvitationPaihangbangNormalTableViewCell calculationCellHeightWithType:VersionInvitationPaihangbangNormalTableViewCellTypeTitle];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"查看更多" sourceArr:self.invitationMutableArr]){
            return [VersionInvitationPaihangbangNormalTableViewCell calculationCellHeightWithType:VersionInvitationPaihangbangNormalTableViewCellTypeBottom];
        } else {
            return [VersionInvitationPaihangbangNormalTableViewCell calculationCellHeightWithType:VersionInvitationPaihangbangNormalTableViewCellTypeNormal];
        }
    }
}

-(void)sendRequestToGetInfoManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] getInvitationJiangliCount:^(VersionInvitationRootModel * _Nonnull model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->mainModel = model;
        [strongSelf.invitationTableView reloadData];
    }];
}

-(void)getListManagerWithAddPage:(BOOL)addPage{
    if (addPage){
        page ++;
    }
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] getMyInvitationRankListWithPage:page block:^(CenterInvitationHistoryListModel * _Nonnull listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (page == 0){
            [strongSelf.paihangbangMutableArr removeAllObjects];
        }
        if (![strongSelf.paihangbangMutableArr containsObject:@"排行榜标题"]){
            [strongSelf.paihangbangMutableArr addObjectsFromArray:@[@"排行榜标题",@"我的排行"]];
        }
        
        [strongSelf.paihangbangMutableArr addObjectsFromArray:listModel.content];
        if ([strongSelf.paihangbangMutableArr containsObject:@"查看更多"]){
            [strongSelf.paihangbangMutableArr removeObject:@"查看更多"];
        }
        [strongSelf.paihangbangMutableArr addObject:@"查看更多"];
        [strongSelf.invitationTableView reloadData];
    }];
    
}

#pragma mark 获取分享信息的接口
-(void)sendRequestToGetShareManager{
    __weak typeof(self)weakSelf = self;
    if (shareRootModel){
        CenterShareImgModel *shareImgModel = [[CenterShareImgModel alloc]init];
        shareImg = [shareImgModel mainManager:shareRootModel];
    } else {
        [[NetworkAdapter sharedAdapter] centerGetShareInfoWithBlock:^(BOOL isSuccessed, CenterShareSingleModel *shareModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (isSuccessed){
                strongSelf->shareRootModel = shareModel;
                
                CenterShareImgModel *shareImgModel = [[CenterShareImgModel alloc]init];
                strongSelf->shareImg = [shareImgModel mainManager:shareModel];
            }
        }];
    }
}

#pragma mark - 分享的控制器
-(void)actionShowShareInfoManager:(CenterShareSingleModel *)model type:(InvitationShareType)InvitationShareType{
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.transferCopyUrl = model.url;
    if (InvitationShareType == InvitationShareTypeImg){
        shareViewController.showImgBackgroundImage = shareImg;
    } else if (InvitationShareType == InvitationShareTypeUrl){
        shareViewController.showImgBackgroundImage = nil;
    }

    shareViewController.isHasGesture = YES;
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType = thirdLoginTypeWechat;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        }
        if (InvitationShareType == InvitationShareTypeImg){
            [ShareSDKManager shareManagerWithType:shareType title:nil desc:nil img:shareImg url:nil callBack:NULL];
        } else if (InvitationShareType == InvitationShareTypeUrl){
            
            NSString *title = [NSString stringWithFormat:@"%@ 送你一个红包",[AccountModel sharedAccountModel].loginServerModel.user.nickname];
            [ShareSDKManager shareManagerWithType:shareType title:title desc:@"币本，玩直播、发帖子、点点赞、每天拿收益！" img:[UIImage imageNamed:@"icon_yaoqing_share_img"] url:model.url callBack:NULL];
        }
        
        [ShareSDKManager shareSuccessBack:shareTypeInvitation block:NULL];
    }];
    [shareViewController showInView:self.parentViewController];
}
@end
