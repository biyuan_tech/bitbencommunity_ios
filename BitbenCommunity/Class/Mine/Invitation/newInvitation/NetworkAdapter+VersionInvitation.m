//
//  NetworkAdapter+VersionInvitation.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "NetworkAdapter+VersionInvitation.h"

@implementation NetworkAdapter (VersionInvitation)

#pragma mark - 1. 获取邀请奖励总额
-(void)getInvitationJiangliCount:(void(^)(VersionInvitationRootModel *model))block{
    __weak typeof(self)weakSelf = self;
    if (![AccountModel sharedAccountModel].account_id.length){
        [[UIAlertView alertViewWithTitle:@"提示" message:@"请登录" buttonTitles:@[@"确定"] callBlock:NULL]show];
        return;
    }
    NSDictionary *params = @{@"invite_account_id":[AccountModel sharedAccountModel].account_id};
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_my_invitation requestParams:params responseObjectClass:[VersionInvitationRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            VersionInvitationRootModel *singleModel = (VersionInvitationRootModel *)responseObject;
            if (block){
                block(singleModel);
            }
        }
    }];
}

#pragma mark - 2. 获取我的邀请分页
-(void)getMyInvitationListWithPage:(NSInteger)page block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"invite_account_id":[AccountModel sharedAccountModel].account_id,@"page_number":@(page),@"page_size":@(10)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:page_invitation_history requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            
        }
    }];
}

#pragma mark - 排行榜
-(void)getMyInvitationRankListWithPage:(NSInteger)page block:(void (^)(CenterInvitationHistoryListModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"invite_account_id":[AccountModel sharedAccountModel].account_id,@"page_number":@(page),@"page_size":@(10)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:page_invitation_rank requestParams:params responseObjectClass:[CenterInvitationHistoryListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterInvitationHistoryListModel *singleModel = (CenterInvitationHistoryListModel *)responseObject;
            if(block){
                block(singleModel);
            }
        }
    }];
}

#pragma mark - 获取加速
-(void)getMemberInvitationHistoryWithPage:(NSInteger)page block:(void (^)(CenterInvitationHistoryListModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"invite_account_id":[AccountModel sharedAccountModel].account_id,@"page_number":@(page),@"page_size":@(10)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:page_invitation_member_history requestParams:params responseObjectClass:[CenterInvitationHistoryListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterInvitationHistoryListModel *singleModel = (CenterInvitationHistoryListModel *)responseObject;
            if(block){
                block(singleModel);
            }
        }
    }];
}

#pragma mark - 进行会员绑定
-(void)InvitationBandingMemberWithCode:(NSString *)code Block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"invitation_code":code};
    [[NetworkAdapter sharedAdapter] fetchWithPath:band_member_invitation_code requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}

@end
