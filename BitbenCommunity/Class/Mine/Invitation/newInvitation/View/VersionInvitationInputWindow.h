//
//  VersionInvitationInputWindow.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VersionInvitationInputWindow : UIView


-(void)actionCancelManagerWithBlock:(void(^)())block;
-(void)actionCheckManagerWithBlock:(void(^)(NSString *info))block;
@end

NS_ASSUME_NONNULL_END
