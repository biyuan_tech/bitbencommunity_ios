//
//  VersionInvitationItemCell1.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "VersionInvitationItemCell1.h"

static char actionClickBlockKey;
@interface VersionInvitationItemCell1()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)VersionInvitationItemCellView *view1;
@property (nonatomic,strong)VersionInvitationItemCellView *view2;
@property (nonatomic,strong)VersionInvitationItemCellView *view3;


@end

@implementation VersionInvitationItemCell1

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.bgImgView.image = [Tool stretchImageWithName:@"bg_inviation_items_bf"];
    [self addSubview:self.bgImgView];
    self.backgroundView = self.bgImgView;
    
    // 1. 创建view1
    CGFloat width = (kScreenBounds.size.width - 2 * LCFloat(15)) / 3.;
    __weak typeof(self)weakSelf = self;
    self.view1 = [[VersionInvitationItemCellView alloc]initWithFrame:CGRectMake(LCFloat(15), 0, width, [VersionInvitationItemCell1 calculationCellHeight]) title:@"0" fixed:@"个" info:@"累计奖励BBT" action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSInteger index) = objc_getAssociatedObject(strongSelf, &actionClickBlockKey);
        if (block){
            block(0);
        }
    }];
    [self addSubview:self.view1];
    
    self.view2 = [[VersionInvitationItemCellView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.view1.frame), 0, width, [VersionInvitationItemCell1 calculationCellHeight]) title:@"0" fixed:@"人" info:@"成功邀请人数" action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSInteger index) = objc_getAssociatedObject(strongSelf, &actionClickBlockKey);
        if (block){
            block(1);
        }
    }];
    [self addSubview:self.view2];
    
    self.view3 = [[VersionInvitationItemCellView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.view2.frame), 0, width, [VersionInvitationItemCell1 calculationCellHeight]) title:@"0" fixed:@"元" info:@"累计收益" action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSInteger index) = objc_getAssociatedObject(strongSelf, &actionClickBlockKey);
        if (block){
            block(2);
        }
    }];
    [self addSubview:self.view3];
    [self.view1 reloadFrame];
    [self.view2 reloadFrame];
    [self.view3 reloadFrame];
}

+(CGFloat)calculationCellHeight{
    return LCFloat(85);
}

-(void)actionClickBlock:(void(^)(NSInteger index))block{
    objc_setAssociatedObject(self, &actionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)setTransferInvitationModel:(VersionInvitationRootModel *)transferInvitationModel{
    _transferInvitationModel = transferInvitationModel;
    
    self.view1.dymicLabel.text = transferInvitationModel.reward;

    self.view2.dymicLabel.text = transferInvitationModel.count;
    
    self.view3.dymicLabel.text = @"0";
//    transferInvitationModel.member_reward;
    
    [self.view1 reloadFrame];
    [self.view2 reloadFrame];
    [self.view3 reloadFrame];
}

@end



@implementation VersionInvitationItemCellView

-(instancetype)initWithFrame:(CGRect)frame title:(NSString *)title fixed:(NSString *)fixed info:(NSString *)info action:(void(^)())block{
    self = [super initWithFrame:frame];
    if (self){
        [self createViewWithTitle:title fixed:fixed info:info action:block];
    }
    return self;
}

#pragma mark - createView
-(void)createViewWithTitle:(NSString *)title fixed:(NSString *)fixed info:(NSString *)info action:(void(^)())block{
    self.dymicLabel = [GWViewTool createLabelFont:@"20" textColor:@"EA6441"];
    self.dymicLabel.text = title;
    [self addSubview:self.dymicLabel];
    
    self.fixedLabel = [GWViewTool createLabelFont:@"13" textColor:@"4C4C4C"];
    [self addSubview:self.fixedLabel];
    self.fixedLabel.text = fixed;
    
    self.infoLabel = [GWViewTool createLabelFont:@"12" textColor:@"8F8F8F"];
    [self addSubview:self.infoLabel];
    self.infoLabel.text = info;
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.actionButton];
    self.actionButton.frame = self.bounds;
    
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if(block){
            block();
        }
    }];
    
    [self reloadFrame];
}

-(void)reloadFrame{
    // 计算
    CGSize dymicSize = [Tool makeSizeWithLabel:self.dymicLabel];
    CGSize fixedSize = [Tool makeSizeWithLabel:self.fixedLabel];
    CGFloat itemsWidth = (self.size_width - dymicSize.width - fixedSize.width - LCFloat(4)) / 2.;
    self.dymicLabel.frame = CGRectMake(itemsWidth, LCFloat(25), dymicSize.width, dymicSize.height);
    self.fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.dymicLabel.frame) + LCFloat(4), 0, fixedSize.width, fixedSize.height);
    self.fixedLabel.center_y = self.dymicLabel.center_y;
    
    self.infoLabel.frame = CGRectMake(0, CGRectGetMaxY(self.dymicLabel.frame) + LCFloat(10), self.size_width, [NSString contentofHeightWithFont:self.infoLabel.font]);
    self.infoLabel.textAlignment = NSTextAlignmentCenter;
    
    
}

@end



