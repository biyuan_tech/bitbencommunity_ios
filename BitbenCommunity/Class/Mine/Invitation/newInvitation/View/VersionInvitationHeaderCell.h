//
//  VersionInvitationHeaderCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,VersionInvitationHeaderCellType) {
    VersionInvitationHeaderCellTypeNormal = 0,
    VersionInvitationHeaderCellTypeLeiji = 1,               /**< 累计奖励*/
    VersionInvitationHeaderCellTypeYaoqing = 2,             /**< 邀请奖励*/
    VersionInvitationHeaderCellTypeJiasu = 3,               /**< 加速奖励*/
};

@interface VersionInvitationHeaderCell : PDBaseTableViewCell

@property (nonatomic,assign)VersionInvitationHeaderCellType transferType;
-(void)actionClickGuizeBlock:(void(^)())block;
@end

NS_ASSUME_NONNULL_END
