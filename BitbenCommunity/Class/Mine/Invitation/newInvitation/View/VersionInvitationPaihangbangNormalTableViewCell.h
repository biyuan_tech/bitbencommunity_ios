//
//  VersionInvitationPaihangbangNormalTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "NetworkAdapter+VersionInvitation.h"
#import "CenterInvitationSingleModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,VersionInvitationPaihangbangNormalTableViewCellType) {
    VersionInvitationPaihangbangNormalTableViewCellTypeHeader,
    VersionInvitationPaihangbangNormalTableViewCellTypeHeaderLeiji,
    VersionInvitationPaihangbangNormalTableViewCellTypeHeaderYaoqing,
    VersionInvitationPaihangbangNormalTableViewCellTypeHeaderJiasu,
    VersionInvitationPaihangbangNormalTableViewCellTypeTitle,
    VersionInvitationPaihangbangNormalTableViewCellTypeNormal,
    VersionInvitationPaihangbangNormalTableViewCellTypeNormalLeiji,
    VersionInvitationPaihangbangNormalTableViewCellTypeNormalYaoqing,
    VersionInvitationPaihangbangNormalTableViewCellTypeNormalJiasu,
    VersionInvitationPaihangbangNormalTableViewCellTypeBottom,
};

@interface VersionInvitationPaihangbangNormalTableViewCell : PDBaseTableViewCell

@property (nonatomic,assign) VersionInvitationPaihangbangNormalTableViewCellType transferType;
@property (nonatomic,strong)VersionInvitationRootModel *transferModel;
@property (nonatomic,strong) CenterInvitationHistorySingleModel *transferInvitationModel ;          // 我的邀请记录
@property (nonatomic,assign)NSInteger transferIndex;
+(CGFloat)calculationCellHeightWithType:(VersionInvitationPaihangbangNormalTableViewCellType)transferType;

@end

NS_ASSUME_NONNULL_END
