//
//  VersionInvitationRootModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface VersionInvitationRootModel : FetchModel

@property (nonatomic,copy)NSString *_id;
@property (nonatomic,copy)NSString *account_id;
@property (nonatomic,copy)NSString *count;
@property (nonatomic,copy)NSString *friend_reward;
@property (nonatomic,copy)NSString *member_reward;
@property (nonatomic,copy)NSString *rank;
@property (nonatomic,copy)NSString *reward;
@property (nonatomic,copy)NSString *url;

@end

NS_ASSUME_NONNULL_END
