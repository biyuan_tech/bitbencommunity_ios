//
//  NetworkAdapter+VersionInvitation.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "NetworkAdapter.h"
#import "VersionInvitationRootModel.h"
#import "CenterInvitationSingleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetworkAdapter (VersionInvitation)

#pragma mark - 1. 获取邀请奖励总额
-(void)getInvitationJiangliCount:(void(^)(VersionInvitationRootModel *model))block;
#pragma mark - 2. 获取我的邀请分页
-(void)getMyInvitationListWithPage:(NSInteger)page block:(void(^)())block;
#pragma mark - 3. 获取排行榜分页
-(void)getMyInvitationRankListWithPage:(NSInteger)page block:(void (^)(CenterInvitationHistoryListModel *listModel))block;
#pragma mark - 获取加速
-(void)getMemberInvitationHistoryWithPage:(NSInteger)page block:(void (^)(CenterInvitationHistoryListModel *listModel))block;
#pragma mark - 进行会员绑定
-(void)InvitationBandingMemberWithCode:(NSString *)code Block:(void(^)())block;
@end

NS_ASSUME_NONNULL_END
