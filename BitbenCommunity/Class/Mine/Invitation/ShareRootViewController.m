//
//  ShareRootViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/14.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "ShareRootViewController.h"
#import "UIImage+ImageEffects.h"
#import "NetworkAdapter+Task.h"
#import "NetworkAdapter+Article.h"

@implementation ShareCollectionModel
@end


#define SheetViewHeight       LCFloat(165 )
@interface ShareRootViewController ()
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)UIScrollView *mainScrollView;;
@property (nonatomic,strong)PDImageView *actionBgimgView;
@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图片
@property (nonatomic,strong)NSArray *shareButtonArray;
@property (nonatomic,strong)UILabel *titleLabel;
@end

static char actionClickWithShareBlockKey;
static char actionClickWithJubaoBlockKey;
static char actionClickWithCopyBlockKey;
static char actionClickWithCollectionBlockKey;
@implementation ShareRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hasCollection = YES;
    self.hasCopy = YES;
    [self arrayWithInit];
    [self createSheetView];                      // 加载Sheetview
    [self createDismissButton];                  // 创建dismissButton
}

-(void)setHasCollection:(BOOL)hasCollection{
    _hasCollection = hasCollection;
}

#pragma mark - 创建sheetView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
        
    }];
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    self.mainScrollView.frame = kScreenBounds;
    [self.view addSubview:self.mainScrollView];
    
    self.actionBgimgView = [[PDImageView alloc]init];
    self.actionBgimgView.backgroundColor = [UIColor clearColor];
    self.actionBgimgView.image = self.showImgBackgroundImage;
    self.actionBgimgView.frame = self.view.bounds;
    self.actionBgimgView.alpha = 0;
    [self.mainScrollView addSubview:self.actionBgimgView];
    
    // 1. 换算比例
    if (self.showImgBackgroundImage){
        CGFloat progress = kScreenBounds.size.width * 1.0 / self.showImgBackgroundImage.size.width;
        CGFloat height = progress * self.showImgBackgroundImage.size.height;
        if (height > kScreenBounds.size.height){            // scr 大于屏幕
            self.actionBgimgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, height);
            self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width, height);
        } else {
            self.actionBgimgView.frame = CGRectMake(0, (kScreenBounds.size.height - height) / 2., kScreenBounds.size.width, height);
            self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width, kScreenBounds.size.height);
        }
    } else {
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width, kScreenBounds.size.height);
    }
    
    // 创建背景色
    self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35f];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sheetViewDismiss)];
    [self.backgrondView addGestureRecognizer:tapGesture];
    [self.view addSubview:self.backgrondView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    
    [self createSharetView];
}



-(void)arrayWithInit{
    self.shareButtonArray = @[@[@"微信好友",@"icon_center_share_wechat",@"wechat"],@[@"微信朋友圈",@"icon_center_share_wechat_friend",@"friendsCircle"],@[@"微博",@"icon_center_share_sina",@"sina"]];
}

#pragma mark - 创建view
-(void)createSharetView{
    CGFloat viewHeight = SheetViewHeight;
    if (self.hasJubao){
        viewHeight += LCFloat(51);
    }
    if (self.hasCopy){
        viewHeight += LCFloat(51);
    }
    if (self.hasCollection){
        viewHeight += LCFloat(51);
    }
    viewHeight += LCFloat(51);
    
    if (IS_iPhoneX){
        viewHeight += LCFloat(34);
    }
    
    CGRect screenRect = [[UIScreen mainScreen]bounds];
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, screenRect.size.height, screenRect.size.width, viewHeight)];
    _shareView.clipsToBounds = YES;
    _shareView.backgroundColor = UURandomColor;
    _shareView.image = self.backgroundImage;
    _shareView.userInteractionEnabled = YES;
    _shareView.contentMode = UIViewContentModeBottom;
    _shareView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_shareView];

    // 创建标题
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"353535"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(54));
    self.titleLabel.text = @"分享给好友";
    [_shareView addSubview:self.titleLabel];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame), kScreenBounds.size.width, .5f);
    [_shareView addSubview:lineView];
    lineView.backgroundColor = [UIColor hexChangeFloat:@"EDEDED"];
    
    
    
    
    // 创建图标
    [self createShareButton];
}

#pragma mark 创建分享图标
-(void)createShareButton{
    CGFloat margin = (kScreenBounds.size.width - self.shareButtonArray.count * LCFloat(70)) * 1.0 / (self.shareButtonArray.count + 1);
    CGFloat width = LCFloat(70);
    CGFloat height = LCFloat(47) + LCFloat(15) + [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    for (int i = 0 ; i < self.shareButtonArray.count;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        NSString *title = [[self.shareButtonArray objectAtIndex:i] objectAtIndex:0];
        NSString *img = [[self.shareButtonArray objectAtIndex:i]objectAtIndex:1];
        NSString *type = [[self.shareButtonArray objectAtIndex:i]objectAtIndex:2];
        [button setTitle:title forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithCustomerName:@"7F7F7F"] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithCustomerSizeName:@"12"];
        [button setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
        button.frame = CGRectMake(margin + (width + margin) * i, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(26), width, height);
        
        [button layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleTop imageTitleSpace:LCFloat(15)];
        __weak typeof(self)weakSelf = self;
        [button buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(NSString *type) = objc_getAssociatedObject(strongSelf, &actionClickWithShareBlockKey);
            if (block){
                block(type);
            }
            [strongSelf dismissFromView:_showViewController];
        }];
        [_shareView addSubview:button];
        
    }
}

#pragma mark 创建dismissButton
-(void)createDismissButton{
    UIView *lineView = [[UIView alloc]init];
    [_shareView addSubview:lineView];
    lineView.backgroundColor = [UIColor hexChangeFloat:@"EDEDED"];
    
    
    // 举报按钮
    NSMutableArray *infoMutableArr = [NSMutableArray array];
    [infoMutableArr addObject:@"取消"];
    if (self.hasCollection){
        if (self.collection_status){
            [infoMutableArr addObject:@"取消收藏"];
        } else {
            [infoMutableArr addObject:@"收藏"];
        }
        
    }
    if (self.hasJubao){
        [infoMutableArr addObject:@"举报"];
    }
    if (self.hasCopy){
        [infoMutableArr addObject:@"复制链接"];
    }
    __weak typeof(self)weakSelf = self;
    UIButton *dismissBtn;
    for (int i = 0 ; i < infoMutableArr.count;i++){
        NSString *infoStr = [infoMutableArr objectAtIndex:i];
        CGFloat origin_y = 0;
        if (IS_iPhoneX){
            origin_y = self.shareView.size_height - LCFloat(34) - LCFloat(51) * (i + 1);
        } else {
            origin_y = self.shareView.size_height - LCFloat(51) * (i + 1);
        }
         
        UIButton *copyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        copyBtn.frame = CGRectMake(0, origin_y, kScreenBounds.size.width, LCFloat(51));
        [copyBtn setTitle:infoStr forState:UIControlStateNormal];
        copyBtn.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
        copyBtn.backgroundColor = [UIColor whiteColor];
        [copyBtn setTitleColor:[UIColor colorWithCustomerName:@"353535"] forState:UIControlStateNormal];
        [copyBtn buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)();
            if ([copyBtn.titleLabel.text isEqualToString:@"收藏"] || [copyBtn.titleLabel.text isEqualToString:@"取消收藏"]){
                [strongSelf collectionManagerManager];
            } else if ([copyBtn.titleLabel.text isEqualToString:@"举报"]){
                block = objc_getAssociatedObject(strongSelf, &actionClickWithJubaoBlockKey);
            } else if ([copyBtn.titleLabel.text isEqualToString:@"复制链接"]){
                [Tool copyWithString:strongSelf.transferCopyUrl callback:^{
                    [StatusBarManager statusBarShowWithText:@"复制成功"];
                }];
                
                block = objc_getAssociatedObject(strongSelf, &actionClickWithCopyBlockKey);
            }
            if (block){
                block();
            }
          
            [strongSelf sheetViewDismiss];
        }];
        if ([copyBtn.titleLabel.text isEqualToString:@"取消"]){
            dismissBtn = copyBtn;
        }
        [_shareView addSubview:copyBtn];
        
        UIView *lineView = [[UIView alloc]init];
        lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
        lineView.frame = CGRectMake(LCFloat(11), copyBtn.orgin_y  , kScreenBounds.size.width - 2 * LCFloat(11), .5f);
        [_shareView addSubview:lineView];
    }
   
    
    lineView.frame = CGRectMake(0, dismissBtn.orgin_y - .5f, kScreenBounds.size.width, .5f);
    
    // Gesture
    if (self.isHasGesture){
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.backgrondView addGestureRecognizer:tapGestureRecognizer];
    }
}

#pragma mark - actionClick
-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}


#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak ShareRootViewController *weakVC = self;
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.actionBgimgView.alpha = 1;
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.shareView.frame = CGRectMake(0, screenHeight, weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
        self.actionBgimgView.alpha = 0;
        self.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    __weak ShareRootViewController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    
    [UIView animateWithDuration:.5f animations:^{
        weakVC.shareView.frame = CGRectMake(weakVC.shareView.frame.origin.x, screenHeight-_shareView.bounds.size.height-(IS_IOS7_LATER ? 0 : 20), weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
        
        self.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
        self.actionBgimgView.alpha = 1;
    } completion:^(BOOL finished) {
        self.actionBgimgView.alpha = 1;
    }];
}


- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}

-(void)actionClickWithShareBlock:(void(^)(NSString *type))block{
    objc_setAssociatedObject(self, &actionClickWithShareBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithJubaoBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithJubaoBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithCopyBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithCopyBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithCollectionBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithCollectionBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}



-(void)collectionManagerManager{
    // 1. 根据url 获取是分享直播还是文章
    if (self.transferCopyUrl.length > 0){
        NSString *infoId = @"";
        CollectionModelThemetype type;
        if ([self.transferCopyUrl rangeOfString:@"information"].location != NSNotFound) {       // 表示是文章
            NSArray *infoArr = [self.transferCopyUrl componentsSeparatedByString:@"/"];
            if (infoArr.count > 0){
                infoId = [infoArr lastObject];
            }
            
            type = CollectionModelThemetype1;
        } else {                                                                                //表示是直播
            NSArray *infoArr = [self.transferCopyUrl componentsSeparatedByString:@"="];
            if (infoArr.count > 0){
                infoId = [infoArr lastObject];
            }
            type = CollectionModelThemetype0;
        }
        
        __weak typeof(self)weakSelf = self;
        if (self.collection_status){
            [[NetworkAdapter sharedAdapter] collectionCancelManager:infoId themeType:type actionBlock:^(BOOL isSuccessed) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void(^collectionBlock)() = objc_getAssociatedObject(strongSelf, &actionClickWithCollectionBlockKey);
                if (collectionBlock){
                    collectionBlock();
                }
            }];
        } else {
            [[NetworkAdapter sharedAdapter] collectionManager:infoId themeType:type actionBlock:^(BOOL isSuccessed) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void(^collectionBlock)() = objc_getAssociatedObject(strongSelf, &actionClickWithCollectionBlockKey);
                if (collectionBlock){
                    collectionBlock();
                }
            }];
        }
        
    } else {
        [StatusBarManager statusBarShowWithText:@"收藏出现异常，请稍后再试"];
    }
}

@end
