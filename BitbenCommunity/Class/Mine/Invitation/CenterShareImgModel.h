//
//  CenterShareImgModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/22.
//  Copyright © 2018 币本. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CenterShareSingleModel.h"
#import "BYHomeNewsletterModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger ,ShareImgType) {
    ShareImgTypeOne,
};


@interface CenterShareImgModel : NSObject

-(UIImage *)createImgManagerWithShareType:(ShareImgType)type model:(CenterShareSingleModel *)model;

+(UIImage *)createImgManagerWithPlacehorderWithRect:(CGSize)size;

-(UIImage *)createErweimaWithInfo:(NSString *)infoUrl;

-(UIImage *)mainManager:(CenterShareSingleModel *)model;

-(UIImage *)createKuaixunImgWithModelWithModel:(BYHomeNewsletterModel *)model;
@end

NS_ASSUME_NONNULL_END
