//
//  CenterAboutRootViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/8.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterAboutRootViewController.h"
#import "CenterAboutLinkMeViewController.h"
#import "CenterAboutViewController.h"
#import "AboutTopTableViewCell.h"
#import "CenterLaboratoryViewController.h"

@interface CenterAboutRootViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *aboutRootTableView;
@property (nonatomic,strong)NSArray *aboutRootArr;
@end

@implementation CenterAboutRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];

}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"关于币本";
    self.view.backgroundColor = RGB(247, 247, 247, 1);
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
//    self.aboutRootArr = @[@[@"标题"],@[@"币本简介",@"联系我们",@"帮助中心",@"实验室"]];
    self.aboutRootArr = @[@[@"标题"],@[@"币本简介",@"联系我们",@"帮助中心"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.aboutRootTableView){
        self.aboutRootTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.aboutRootTableView.dataSource = self;
        self.aboutRootTableView.delegate = self;
        [self.view addSubview:self.aboutRootTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.aboutRootArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.aboutRootArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        AboutTopTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[AboutTopTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = [[self.aboutRootArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        cellWithRowTwo.transferHasArrow = YES;
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1){
        if (indexPath.row == 0){
            PDWebViewController *webViewController = [[PDWebViewController alloc]init];
            [webViewController webDirectedWebUrl:@"http://m.bitben.com/aboutbbt"];
            
//            CenterAboutViewController *aboutVC = [[CenterAboutViewController alloc]init];
            [self.navigationController pushViewController:webViewController animated:YES];
        } else if (indexPath.row == 2){
            PDWebViewController *webViewController = [[PDWebViewController alloc]init];
            [webViewController webDirectedWebUrl:help];
            [self.navigationController pushViewController:webViewController animated:YES];
        } else if (indexPath.row == 1){
            CenterAboutLinkMeViewController *linkVC = [[CenterAboutLinkMeViewController alloc]init];
            [self.navigationController pushViewController:linkVC animated:YES];
        } else if (indexPath.row == 3){
            CenterLaboratoryViewController *vc = [[CenterLaboratoryViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [AboutTopTableViewCell calculationCellHeight];
    } else {
        return [GWNormalTableViewCell calculationCellHeight] + 2 * LCFloat(5);
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(9);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.aboutRootTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.aboutRootArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.aboutRootArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}
@end
