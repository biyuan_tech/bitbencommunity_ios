//
//  CenterAboutLineTitleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/9.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterAboutLineTitleTableViewCell.h"

@interface CenterAboutLineTitleTableViewCell()
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UILabel *titleLabel;
@end

@implementation CenterAboutLineTitleTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.lineView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"13" textColor:@"393939"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    
     if ([transferTitle hasPrefix:@"!"]){
        NSArray *tempArr = [transferTitle componentsSeparatedByString:@"!"];
        self.titleLabel.text = [tempArr lastObject];
     }
    
    self.lineView.frame = CGRectMake(LCFloat(15), ([CenterAboutLineTitleTableViewCell calculationCellHeight] - LCFloat(15)) / 2., LCFloat(2), LCFloat(15));
    
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.lineView.frame) + LCFloat(13), 0, 100, [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.titleLabel.center_y = self.lineView.center_y;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(55);
}


@end
