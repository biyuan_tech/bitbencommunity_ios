//
//  CenterLaboratoryViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/3.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "CenterLaboratoryViewController.h"
#import "BYLiveHomeControllerV1.h"
#import "ArtivleNewRootViewController.h"
#import "MessageNewRootViewController.h"

@interface CenterLaboratoryViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *laboratoryTableView;
@property (nonatomic,strong)NSArray *laboratoryArray;


@end

@implementation CenterLaboratoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"实验室";
}

-(void)arrayWithInit{
    self.laboratoryArray = @[@[@"首页"],@[@"实验室"],@[@"新消息"]];
}

-(void)createTableView{
    if (!self.laboratoryTableView){
        self.laboratoryTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.laboratoryTableView.dataSource = self;
        self.laboratoryTableView.delegate = self;
        [self.view addSubview:self.laboratoryTableView];
    }
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.laboratoryArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.laboratoryArray objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowOne.transferTitle = [[self.laboratoryArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    cellWithRowOne.transferIcon = [UIImage imageNamed:@"icon_about_newHome"];
    cellWithRowOne.transferHasArrow = YES;
    return cellWithRowOne;
}

#pragma mark - UITableViewDeelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"实验室" sourceArr:self.laboratoryArray]){
        ArtivleNewRootViewController *articleVC = [[ArtivleNewRootViewController alloc]init];
        [self.navigationController pushViewController:articleVC animated:YES];
    }else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"新消息" sourceArr:self.laboratoryArray]){
        MessageNewRootViewController *articleVC = [[MessageNewRootViewController alloc]init];
        [self.navigationController pushViewController:articleVC animated:YES];
    }else {
        BYLiveHomeControllerV1 *liveHomeController = [[BYLiveHomeControllerV1 alloc] init];
        [self.navigationController pushViewController:liveHomeController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [GWNormalTableViewCell calculationCellHeight];
}


@end
