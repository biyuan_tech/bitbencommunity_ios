//
//  CenterAboutLinkMeViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/8.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterAboutLinkMeViewController.h"
#import "CenterLinkSingleTableViewCell.h"
#import "AboutTopTableViewCell.h"
#import "NetworkAdapter.h"
#import "CenterLinkMeModel.h"

@interface CenterAboutLinkMeViewController ()<UITableViewDelegate,UITableViewDataSource>{
    CenterLinkMeModel *linkModel;
}
@property (nonatomic,strong)UITableView *linkMeTableView;
@property (nonatomic,strong)NSArray *linkMeArr;

@end

@implementation CenterAboutLinkMeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"联系我们";
    self.view.backgroundColor = RGB(247, 247, 247, 1);
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.linkMeArr = @[@[@"标题"],@[@"商务合作",@"商Email",@"商微信"],@[@"官方社群",@"官微信"],@[@"官方微信客服号",@"客服微信"],@[@"官网",@"官网网址"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.linkMeTableView){
        self.linkMeTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.linkMeTableView.dataSource = self;
        self.linkMeTableView.delegate = self;
        [self.view addSubview:self.linkMeTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.linkMeArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.linkMeArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        AboutTopTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[AboutTopTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        return cellWithRowOne;
    } else {
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            cellWithRowTwo.transferTitle = [[self.linkMeArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            return cellWithRowTwo;
        } else {
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            CenterLinkSingleTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[CenterLinkSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            }
            if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商Email" sourceArr:self.linkMeArr]){
                if (indexPath.row == [self cellIndexPathRowWithcellData:@"商Email" sourceArr:self.linkMeArr]){
                    cellWithRowThr.transferTitle = [NSString stringWithFormat:@"E-Mail：%@",linkModel.business_email];
                    cellWithRowThr.transferCopyInfo = linkModel.business_email;
                } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"商微信" sourceArr:self.linkMeArr]){
                    cellWithRowThr.transferTitle = [NSString stringWithFormat:@"微信：%@",linkModel.business_wechat];
                    cellWithRowThr.transferCopyInfo = linkModel.business_wechat;
                }
            } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"官微信" sourceArr:self.linkMeArr]){
                cellWithRowThr.transferTitle = [NSString stringWithFormat:@"微信：%@",linkModel.community_wechat];
                cellWithRowThr.transferCopyInfo = linkModel.community_wechat;
            } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"客服微信" sourceArr:self.linkMeArr]){
                cellWithRowThr.transferTitle = [NSString stringWithFormat:@"微信：%@",linkModel.service_wechat];
                cellWithRowThr.transferCopyInfo = linkModel.service_wechat;
            } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"官网网址" sourceArr:self.linkMeArr]){
                cellWithRowThr.transferTitle = [NSString stringWithFormat:@"网址：%@",linkModel.official_website];
                cellWithRowThr.transferCopyInfo = linkModel.official_website;
            }
            return cellWithRowThr;
        }
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [AboutTopTableViewCell calculationCellHeight];
    } else {
        if (indexPath.row == 0){
            return [GWNormalTableViewCell calculationCellHeight];
        } else {
            return [CenterLinkSingleTableViewCell calculationCellHeight];
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.linkMeTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.linkMeArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.linkMeArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(9);
}

#pragma mark - Interface
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_base_data requestParams:@{@"base_key":@"contact_us"} responseObjectClass:[CenterLinkMeModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            CenterLinkMeModel *linkModel = (CenterLinkMeModel *)responseObject;
            strongSelf->linkModel = linkModel;
            [strongSelf.linkMeTableView reloadData];
        }
    }];
}
@end
