//
//  PowerHeaderView.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/25.
//  Copyright © 2018 币本. All rights reserved.
//

#import "PowerHeaderView.h"

@interface PowerHeaderView()
@property (nonatomic,strong)PDImageView *powerBgImgView;                // 背景
@property (nonatomic,strong)PDImageView *powerLayerView;
@property (nonatomic,strong)UILabel *powerNumberLabel;
@property (nonatomic,strong)UILabel *powerSubLabel;
@property (nonatomic,strong)PDImageView *powerBgView;
@property (nonatomic,strong)UILabel *titleLabel;

@end

@implementation PowerHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.powerBgImgView = [[PDImageView alloc]init];
    self.powerBgImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(398));
    self.powerBgImgView.image = [UIImage imageNamed:@"bg_center_power"];
    [self addSubview:self.powerBgImgView];
    
    self.powerLayerView = [[PDImageView alloc]init];
    self.powerLayerView.backgroundColor = [UIColor clearColor];
    self.powerLayerView.image = [UIImage imageNamed:@"icon_power_layer"];
    self.powerLayerView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(215)) / 2., LCFloat(91), LCFloat(215), LCFloat(195));
    [self addSubview:self.powerLayerView];
    
    self.powerNumberLabel = [GWViewTool createLabelFont:@"60" textColor:@"白"];
    self.powerNumberLabel.font = [self.powerNumberLabel.font boldFont];
    self.powerNumberLabel.text = @"000";
    self.powerNumberLabel.textAlignment = NSTextAlignmentCenter;
    CGSize powerNumberSize = [Tool makeSizeWithLabel:self.powerNumberLabel];
    self.powerNumberLabel.frame = CGRectMake(0, 0, powerNumberSize.width, powerNumberSize.height);
    self.powerNumberLabel.center = CGPointMake(self.powerLayerView.size_width / 2., self.powerLayerView.size_height / 2.);
    [self.powerLayerView addSubview:self.powerNumberLabel];

    self.powerSubLabel = [GWViewTool createLabelFont:@"12" textColor:@"白"];
    self.powerSubLabel.textAlignment = NSTextAlignmentCenter;
    self.powerSubLabel.text = @"能量值";
    self.powerSubLabel.frame = CGRectMake(0, self.powerLayerView.size_height / 2. + LCFloat(30), self.powerLayerView.size_width, [NSString contentofHeightWithFont:self.powerSubLabel.font]);
    [self.powerLayerView addSubview:self.powerSubLabel];
    
    
    self.powerBgView = [[PDImageView alloc]init];
    self.powerBgView.backgroundColor = [UIColor clearColor];
    self.powerBgView.image = [Tool stretchImageWithName:@"bg_power_title_alpha"];
    [self addSubview:self.powerBgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"13" textColor:@"白"];
    [self addSubview:self.titleLabel];
}

-(void)setTransferRuleModel:(PowerRuleModel *)transferRuleModel{
    _transferRuleModel = transferRuleModel;
    self.titleLabel.text = [NSString stringWithFormat:@"初始能量值为%li,次日00：00回复初始值",(long)transferRuleModel.total_energy];
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    CGFloat maxWidth = titleSize.width + 2 * LCFloat(15);
    CGFloat maxHeight = [NSString contentofHeightWithFont:self.titleLabel.font] + 2 * LCFloat(9);
    self.powerBgView.frame = CGRectMake((kScreenBounds.size.width - maxWidth) / 2., [PowerHeaderView calculationHeight] - LCFloat(33) - maxHeight, maxWidth, maxHeight);
    self.titleLabel.frame = CGRectMake(self.powerBgView.orgin_x + LCFloat(15),self.powerBgView.orgin_y + LCFloat(9), titleSize.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    [self.powerNumberLabel animationWithScrollToValue:transferRuleModel.user_energy];
}

+(CGFloat)calculationHeight{
    return LCFloat(398);
}

@end
