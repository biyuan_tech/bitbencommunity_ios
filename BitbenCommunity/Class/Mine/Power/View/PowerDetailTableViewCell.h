//
//  PowerDetailTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/25.
//  Copyright © 2018 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,PowerDetailTableViewCellType) {
    PowerDetailTableViewCellTypeTop,
    PowerDetailTableViewCellTypeBottom,
    PowerDetailTableViewCellTypeNormal,
};

@interface PowerDetailTableViewCell : PDBaseTableViewCell
@property (nonatomic,assign)NSInteger indexRow;
@property (nonatomic,copy)NSString *transferBgImg;
@property (nonatomic,copy)NSString *transferDetailInfo;
+(CGFloat)calculationCellHeightWithTitle:(NSString *)title type:(PowerDetailTableViewCellType)type;
@end

NS_ASSUME_NONNULL_END
