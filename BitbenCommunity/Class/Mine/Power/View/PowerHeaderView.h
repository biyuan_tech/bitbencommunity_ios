//
//  PowerHeaderView.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/25.
//  Copyright © 2018 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PowerRuleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PowerHeaderView : UIView

@property (nonatomic,strong)PowerRuleModel *transferRuleModel;

+(CGFloat)calculationHeight;

@end

NS_ASSUME_NONNULL_END
