//
//  PowerNormalTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/27.
//  Copyright © 2018 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "CenterBBTHistorySingleModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger ,PowerNormalTableViewCellType) {
    PowerNormalTableViewCellTypeTop,
    PowerNormalTableViewCellTypeBottom,
    PowerNormalTableViewCellTypeNormal,
};

@interface PowerNormalTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)CenterBBTHistorySingleModel *transferBBTModel;
@property (nonatomic,assign)NSInteger indexRow;
@property (nonatomic,copy)NSString *transferBgImg;

+(CGFloat)calculationCellHeightWithWithType:(PowerNormalTableViewCellType)type;
@end

NS_ASSUME_NONNULL_END
