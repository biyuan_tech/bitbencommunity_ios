//
//  PowerTitleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/24.
//  Copyright © 2018 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface PowerTitleTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;

@end

NS_ASSUME_NONNULL_END
