//
//  PowerZeroTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/24.
//  Copyright © 2018 币本. All rights reserved.
//

#import "PowerZeroTableViewCell.h"

#define powerZeroInfo @"能量值为0时，将不能进行顶踩等行为，可能影响您的收益与操作体验，且赞且珍惜哟"
@interface PowerZeroTableViewCell()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UILabel *titleLabel;
@end

@implementation PowerZeroTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor hexChangeFloat:@"F5F5F5"];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor hexChangeFloat:@"F6EFEA"];
    [self addSubview:self.bgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"12" textColor:@"D69066"];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.text = powerZeroInfo;
    [self.bgView addSubview:self.titleLabel];
    CGSize titleSize = [powerZeroInfo sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(49), CGFLOAT_MAX)];
    
    self.bgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), 2 * LCFloat(16) + titleSize.height);
    self.titleLabel.frame = CGRectMake(LCFloat(34), LCFloat(16), self.bgView.size_width - 2 * LCFloat(34), titleSize.height);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(16);
    CGSize titleSize = [powerZeroInfo sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"12"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(49), CGFLOAT_MAX)];
    cellHeight += titleSize.height;
    cellHeight += LCFloat(16);
    return cellHeight;
}

@end
