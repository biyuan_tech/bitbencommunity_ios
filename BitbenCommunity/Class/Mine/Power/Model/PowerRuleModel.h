//
//  PowerRuleModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/25.
//  Copyright © 2018 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PowerRuleModel : FetchModel

@property (nonatomic,assign)NSInteger total_energy ;    /**< 总能量*/
@property (nonatomic,assign)NSInteger user_energy ;     /**< 用户剩余能量*/
@property (nonatomic,strong)NSArray *rule ;             /**< 能量数组*/

@end

NS_ASSUME_NONNULL_END
