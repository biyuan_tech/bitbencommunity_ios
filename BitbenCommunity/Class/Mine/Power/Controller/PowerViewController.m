//
//  PowerViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/24.
//  Copyright © 2018 币本. All rights reserved.
//

#import "PowerViewController.h"
#import "PowerTitleTableViewCell.h"
#import "PowerZeroTableViewCell.h"
#import "PowerDetailTableViewCell.h"
#import "NetworkAdapter.h"
#import "PowerRuleModel.h"
#import "CenterBBTHistorySingleModel.h"
#import "CenterBBTSingleCell.h"
#import "KMScrollingHeaderView.h"
#import "PowerHeaderView.h"
#import "PowerNormalTableViewCell.h"

@interface PowerViewController ()<UITableViewDelegate,UITableViewDataSource,KMScrollingHeaderViewDelegate>
@property (nonatomic,strong)NSMutableArray *powerMutableArr;
@property (nonatomic,strong)NSMutableArray *kouchuMutableArr;
@property (nonatomic,strong)NSMutableArray *powerDetailMutableArr;
@property (nonatomic,strong)KMScrollingHeaderView *powerHeaderTableView;
@property (nonatomic,strong)PowerHeaderView *headerViewInfo;
@end

@implementation PowerViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self setupDetailsPageView];
    [self sendRequestToGetKouchuGuize];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.view.backgroundColor = [UIColor hexChangeFloat:@"F5F5F5"];
    self.barMainTitle = @"能量条";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.powerMutableArr = [NSMutableArray array];
    NSArray *powerTempArr = @[@[@"扣除规则"]];
    [self.powerMutableArr addObjectsFromArray:powerTempArr];

    self.kouchuMutableArr = [NSMutableArray array];
    self.powerDetailMutableArr = [NSMutableArray array];
}


#pragma mark - CreateBottomScrollingView
- (void)setupDetailsPageView {
    self.powerHeaderTableView = [[KMScrollingHeaderView alloc]initWithFrame:self.view.bounds imgHeight:LCFloat(400) scaling:LCFloat(200)];
    self.powerHeaderTableView.backgroundColor = [UIColor whiteColor];
    self.powerHeaderTableView.tableView.dataSource = self;
    self.powerHeaderTableView.tableView.delegate = self;
    self.powerHeaderTableView.tableView.backgroundColor = [UIColor whiteColor];
    self.powerHeaderTableView.delegate = self;
     
    self.powerHeaderTableView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.powerHeaderTableView.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.powerHeaderTableView.tableView.showsVerticalScrollIndicator = YES;
    self.powerHeaderTableView.tableView.backgroundColor = [UIColor whiteColor];
    
    self.powerHeaderTableView.tableView.separatorColor = [UIColor whiteColor];
    self.powerHeaderTableView.headerImageViewContentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:self.powerHeaderTableView];
    
    self.powerHeaderTableView.navbarViewFadingOffset = LCFloat(400);
    
    [self.powerHeaderTableView reloadScrollingHeader];
    
    [self createNavBarInfo];
    
    __weak typeof(self)weakSelf = self;
    [self.powerHeaderTableView.tableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetDetailWithReload:YES];
    }];
    [self.powerHeaderTableView.tableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetDetailWithReload:NO];
    }];
}

-(void)createNavBarInfo{
    UILabel *navBarLabel = [GWViewTool createLabelFont:@"15" textColor:@"白"];
    navBarLabel.textAlignment = NSTextAlignmentCenter;
    navBarLabel.text = @"能量值";
    navBarLabel.frame = CGRectMake(LCFloat(100), [BYTabbarViewController sharedController].statusHeight, kScreenBounds.size.width - LCFloat(100) * 2., 44);
    [self.view addSubview:navBarLabel];

    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, [BYTabbarViewController sharedController].statusHeight, 44, 44);
    [backButton setImage:[UIImage imageNamed:@"icon_center_bbt_back"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [backButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.view addSubview:backButton];
    
}


- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView headerImageView:(PDImageView *)imageView{
    self.headerViewInfo = [[PowerHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [PowerHeaderView calculationHeight])];
    [imageView addSubview:self.headerViewInfo];
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.powerMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"扣除规则" sourceArr:self.powerMutableArr]){
        return 1;
    } else if (section == [self cellIndexPathSectionWithcellData:@"扣除规则" sourceArr:self.powerMutableArr] + 1){
        return self.kouchuMutableArr.count;
    } else if (section == [self cellIndexPathSectionWithcellData:@"能量值0" sourceArr:self.powerMutableArr]){
        return 1;
    } else if (section == [self cellIndexPathSectionWithcellData:@"能量值明细" sourceArr:self.powerMutableArr]){
        return 1;
    } else {
        NSArray *sectionOfArr = [self.powerMutableArr objectAtIndex:section];
        return sectionOfArr.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger index =  [self cellIndexPathSectionWithcellData:@"扣除规则" sourceArr:self.powerMutableArr];
    NSInteger index2 =  [self cellIndexPathSectionWithcellData:@"能量值明细" sourceArr:self.powerMutableArr];
    if (indexPath.section == index || indexPath.section == index2){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PowerTitleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PowerTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        if((indexPath.section == [self cellIndexPathSectionWithcellData:@"扣除规则" sourceArr:self.powerMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"扣除规则" sourceArr:self.powerMutableArr])){
            cellWithRowOne.transferTitle = @"扣除规则";
        } else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"能量值明细" sourceArr:self.powerMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"能量值明细" sourceArr:self.powerMutableArr])){
            cellWithRowOne.transferTitle = @"能量值明细";
        }
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"能量值0" sourceArr:self.powerMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"能量值0" sourceArr:self.powerMutableArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PowerZeroTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[PowerZeroTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"扣除规则" sourceArr:self.powerMutableArr] + 1){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        PowerDetailTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[PowerDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.indexRow = indexPath.row;
        if (indexPath.row == 0){
            cellWithRowThr.transferBgImg = @"bg_center_top";
        } else if (indexPath.row == self.kouchuMutableArr.count - 1){
            cellWithRowThr.transferBgImg = @"bg_center_bottom";
        } else {
            cellWithRowThr.transferBgImg = @"bg_center_normal";
        }
        cellWithRowThr.transferDetailInfo = [self.kouchuMutableArr objectAtIndex:indexPath.row];
        return cellWithRowThr;
    } else {
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        PowerNormalTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[PowerNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
        }
        cellWithRowFour.indexRow = indexPath.row;
        cellWithRowFour.transferBBTModel = [self.powerDetailMutableArr objectAtIndex:indexPath.row];
        if (indexPath.row == 0){
            cellWithRowFour.transferBgImg = @"bg_center_top";
        } else if (indexPath.row == self.powerDetailMutableArr.count - 1){
            cellWithRowFour.transferBgImg = @"bg_center_bottom";
        } else {
            cellWithRowFour.transferBgImg = @"bg_center_normal";
        }
        return cellWithRowFour;
    }
}

#pragma mark -UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"扣除规则" sourceArr:self.powerMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"扣除规则" sourceArr:self.powerMutableArr]) || (indexPath.section == [self cellIndexPathSectionWithcellData:@"能量值明细" sourceArr:self.powerMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"能量值明细" sourceArr:self.powerMutableArr])){
        return [PowerTitleTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"能量值0" sourceArr:self.powerMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"能量值0" sourceArr:self.powerMutableArr]){
        return [PowerZeroTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"扣除规则" sourceArr:self.powerMutableArr] + 1){
        if (indexPath.row == 0){
            return [PowerDetailTableViewCell calculationCellHeightWithTitle:[self.kouchuMutableArr objectAtIndex:indexPath.row] type:PowerDetailTableViewCellTypeTop];
        } else if (indexPath.row == self.kouchuMutableArr.count - 1){
            return [PowerDetailTableViewCell calculationCellHeightWithTitle:[self.kouchuMutableArr objectAtIndex:indexPath.row] type:PowerDetailTableViewCellTypeBottom];
        } else {
            return [PowerDetailTableViewCell calculationCellHeightWithTitle:[self.kouchuMutableArr objectAtIndex:indexPath.row] type:PowerDetailTableViewCellTypeNormal];
        }
    } else {
        if (indexPath.row == 0){
            return [PowerNormalTableViewCell calculationCellHeightWithWithType:PowerNormalTableViewCellTypeTop];
        } else if (indexPath.row == self.powerDetailMutableArr.count - 1){
            return [PowerNormalTableViewCell calculationCellHeightWithWithType:PowerNormalTableViewCellTypeBottom];
        } else {
            return [PowerNormalTableViewCell calculationCellHeightWithWithType:PowerNormalTableViewCellTypeNormal];
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.powerHeaderTableView.tableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.powerMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.powerMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor =  [UIColor hexChangeFloat:@"F5F5F5"];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(0);
}

#pragma mark - UITableViewDataSource
-(void)sendRequestToGetKouchuGuize{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_energy requestParams:nil responseObjectClass:[PowerRuleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PowerRuleModel *ruleModel = (PowerRuleModel *)responseObject;
            [strongSelf.kouchuMutableArr removeAllObjects];
            [strongSelf.kouchuMutableArr addObjectsFromArray:ruleModel.rule];
            [strongSelf.powerMutableArr addObject:strongSelf.kouchuMutableArr];
            [strongSelf.powerMutableArr addObjectsFromArray:@[@[@"能量值0"],@[@"能量值明细"]]];
            [strongSelf.powerMutableArr addObject:strongSelf.powerDetailMutableArr];
            
            // 1. 修改能量条
            strongSelf.headerViewInfo.transferRuleModel = ruleModel;
            
            [strongSelf sendRequestToGetDetailWithReload:YES];
        }
    }];
}

-(void)sendRequestToGetDetailWithReload:(BOOL)hasReload{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"type":@"1",@"page_size":@"10",@"page_number":@(self.powerHeaderTableView.tableView.currentPage)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:page_user_history requestParams:params responseObjectClass:[CenterBBTHistoryListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (hasReload){
                [strongSelf.powerDetailMutableArr removeAllObjects];
            }
            CenterBBTHistoryListModel *listModel = (CenterBBTHistoryListModel *)responseObject;
            [strongSelf.powerDetailMutableArr addObjectsFromArray:listModel.content];
            
            [strongSelf.powerHeaderTableView.tableView reloadData];
            
            if (strongSelf.powerHeaderTableView.tableView.isXiaLa){
                [strongSelf.powerHeaderTableView.tableView stopPullToRefresh];
            } else {
                [strongSelf.powerHeaderTableView.tableView stopFinishScrollingRefresh];
            }

        }
    }];
}

@end
