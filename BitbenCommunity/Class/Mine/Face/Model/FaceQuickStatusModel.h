//
//  FaceQuickStatusModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/7.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,FaceQuickStatusModelType) {
    FaceQuickStatusModelTypeNone,               /**< 未认证*/
    FaceQuickStatusModelTypeSuccess,               /**< 成功*/
    FaceQuickStatusModelTypeFail,               /**< 失败*/
};
@interface FaceQuickStatusModel : FetchModel

@property (nonatomic,assign)FaceQuickStatusModelType status;


@end

NS_ASSUME_NONNULL_END
