//
//  FaceAlertViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/5.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FaceAlertViewController.h"
#import "FaceUploadViewController.h"
#import "FaceAlertImgTableViewCell.h"
#import "FaceQuickStatusModel.h"

@interface FaceAlertViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *faceTableView;
@property (nonatomic,strong)NSArray *faceArr;
@end

@implementation FaceAlertViewController

-(void)sendFaceImage:(UIImage *)faceImage
{
    [AccountModel sharedAccountModel].faceImg = faceImage;
}

- (void)sendFaceImageError {
    
    NSLog(@"图片上传失败");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self quicklyRenzhengStatusManager];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"快捷认证";
}

#pragma mark - ArrayWithInit
-(void)arrayWithInit{
    self.faceArr = @[@[@"标题"],@[@"图片"],@[@"1、请确认由本人亲自操作",@"2、请将脸置于提示框内，并且按提示完成动作"],@[@"按钮"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.faceTableView){
        self.faceTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.faceTableView.dataSource = self;
        self.faceTableView.delegate = self;
        [self.view addSubview:self.faceTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.faceArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr  = [self.faceArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.faceArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferTitle = @"请衣着整齐，平视屏幕，并正视光源";
        cellWithRowOne.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
        cellWithRowOne.titleLabel.textColor = [UIColor hexChangeFloat:@"545454"];
        cellWithRowOne.textAlignmentCenter = YES;
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"图片" sourceArr:self.faceArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        FaceAlertImgTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[FaceAlertImgTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"内容" sourceArr:self.faceArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"按钮" sourceArr:self.faceArr]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        GWButtonTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        cellWithRowFour.transferTitle = @"开始拍摄";
        __weak typeof(self)weakSelf = self;
        [cellWithRowFour buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            FaceUploadViewController *faceUploadVC = [[FaceUploadViewController alloc]init];
            [strongSelf.navigationController addChildViewController:faceUploadVC];
            
//            FaceStreamDetectorViewController *faceVC = [[FaceStreamDetectorViewController alloc]init];
//            faceVC.faceDelegate = strongSelf;
//            [faceUploadVC.navigationController pushViewController:faceVC animated:YES];
        }];
        return cellWithRowFour;
    } else {
        static NSString *cellIdentifyWithRowThr = @"GWNormalTableViewCell";
        GWNormalTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = [[self.faceArr objectAtIndex:indexPath.section ]objectAtIndex:indexPath.row];
        cellWithRowThr.titleLabel.textColor = [UIColor hexChangeFloat:@"545454"];
        return cellWithRowThr;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.faceArr]){
        return [GWNormalTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"图片" sourceArr:self.faceArr]){
        return [FaceAlertImgTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"内容" sourceArr:self.faceArr]){
        return 100;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"按钮" sourceArr:self.faceArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else {
        return [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]] + 2* LCFloat(5);
    }
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


#pragma mark - 判断当前是否快捷认证
-(void)quicklyRenzhengStatusManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_quick_auth_status requestParams:nil responseObjectClass:[FaceQuickStatusModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            FaceQuickStatusModel *model = (FaceQuickStatusModel *)responseObject;
            
            if (model.status == FaceQuickStatusModelTypeNone){
                [strongSelf.faceTableView dismissPrompt];
            } else if (model.status == FaceQuickStatusModelTypeSuccess){
                [strongSelf.faceTableView showPrompt:@"当前快捷认证通过" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            } else if (model.status == FaceQuickStatusModelTypeFail){
                [strongSelf.faceTableView showPrompt:@"当前快捷认证未通过,点击重新认证" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:^{
                    [strongSelf.faceTableView dismissPrompt];
                }];
            }
        }
    }];
}

@end
