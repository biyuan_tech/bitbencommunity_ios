//
//  FaceUploadViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/5.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FaceUploadViewController.h"
#import "FaceEndViewController.h"
#import "FaceQuickAuthModel.h"

@interface FaceUploadViewController (){
    UIImage *imageInfo;
}
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *imgView;
@property (nonatomic,strong)UILabel *descLabel;
@property (nonatomic,strong)UIButton *uploadButton;
@property (nonatomic,strong)UIButton *reUploadButton;
@end

@implementation FaceUploadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([AccountModel sharedAccountModel].faceImg){
        self.imgView.image = [AccountModel sharedAccountModel].faceImg;
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"快捷认证";
    
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"545454"];
    [self.view addSubview:self.titleLabel];
    self.titleLabel.text = @"照片上传";
    self.titleLabel.frame = CGRectMake(0, LCFloat(30), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.imgView = [[PDImageView alloc]init];
    self.imgView.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self.view addSubview:self.imgView];
    self.imgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(230)) / 2., CGRectGetMaxY(self.titleLabel.frame) + LCFloat(30), LCFloat(230), LCFloat(230));
    
    self.descLabel = [GWViewTool createLabelFont:@"12" textColor:@"545454"];
    [self.view addSubview:self.descLabel];
    self.descLabel.text = @"请放心上传，币本会确保您信息的安全";
    self.descLabel.frame = CGRectMake(0, CGRectGetMaxY(self.imgView.frame) + LCFloat(23), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.descLabel.font]);
    self.descLabel.textAlignment = NSTextAlignmentCenter;
    
    self.reUploadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.reUploadButton.backgroundColor = [UIColor whiteColor];
    [self.reUploadButton setTitle:@"重新上传" forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.reUploadButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf faceManager];
    }];
    self.reUploadButton.layer.borderColor = [UIColor hexChangeFloat:@"EA6441"].CGColor;
    self.reUploadButton.layer.borderWidth = 1.f;
    self.reUploadButton.layer.cornerRadius = LCFloat(4);
    self.reUploadButton.clipsToBounds = YES;
    [self.reUploadButton setTitle:@"重新上传" forState:UIControlStateNormal];
    [self.reUploadButton setTitleColor:[UIColor hexChangeFloat:@"EA6441"] forState:UIControlStateNormal];
    [self.view addSubview:self.reUploadButton];
    self.reUploadButton.frame = CGRectMake(LCFloat(15), kScreenBounds.size.height - LCFloat(26) - LCFloat(46) - [BYTabbarViewController sharedController].navBarHeight, kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(46));
    
    
    self.uploadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.uploadButton.backgroundColor = [UIColor hexChangeFloat:@"EA6441"];
    self.uploadButton.frame = CGRectMake(LCFloat(15), self.reUploadButton.orgin_y - LCFloat(20) - LCFloat(46), self.reUploadButton.size_width, self.reUploadButton.size_height);
    self.uploadButton.clipsToBounds = YES;
    [self.view addSubview:self.uploadButton];
    [self.uploadButton setTitle:@"确认上传" forState:UIControlStateNormal];
    [self.uploadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.uploadButton.layer.cornerRadius = LCFloat(4);
    
    [self.uploadButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToManager];
    }];
}


#pragma mark - Interface
-(void)sendRequestToManager{
    OSSFileModel *fileModel = [[OSSFileModel alloc]init];
    fileModel.objcImage = [AccountModel sharedAccountModel].faceImg;
    __weak typeof(self)weakSelf = self;
    [[OSSManager sharedUploadManager] uploadImageManagerWithImageList:[@[fileModel] copy]withUrlBlock:^(NSArray *imgUrlArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = self;
        if (imgUrlArr.count >= 1){
            [strongSelf interfaceManager:[imgUrlArr lastObject]];
        }
    }];
}


-(void)interfaceManager:(NSString *)url{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"url":url};
    [[NetworkAdapter sharedAdapter] fetchWithPath:quick_auth requestParams:params responseObjectClass:[FaceQuickAuthModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            FaceQuickAuthModel *authModel = (FaceQuickAuthModel *)responseObject;
            [strongSelf directManagerWithModel:authModel];
        }
    }];
}


-(void)directManagerWithModel:(FaceQuickAuthModel *)authModel{
    
    FaceEndViewController *faceEndVC = [[FaceEndViewController alloc]init];
    faceEndVC.hasSuccessed = authModel.status;
    if (authModel.status == NO){
        [[UIAlertView alertViewWithTitle:@"提示" message:authModel.fail_message buttonTitles:@[@"确定"] callBlock:NULL]show];
    }
    __weak typeof(self)weakSelf = self;
    [faceEndVC reFaceRenzhengManager:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [strongSelf faceManager];
        });
    }];
    [self.navigationController pushViewController:faceEndVC animated:YES];
}

-(void)faceManager{
//    FaceStreamDetectorViewController *faceVC = [[FaceStreamDetectorViewController alloc]init];
//    faceVC.faceDelegate = self;
//    [self.navigationController pushViewController:faceVC animated:YES];
}

-(void)sendFaceImage:(UIImage *)faceImage {
    [AccountModel sharedAccountModel].faceImg = faceImage;
}

- (void)sendFaceImageError {
    NSLog(@"图片上传失败");
}

@end
