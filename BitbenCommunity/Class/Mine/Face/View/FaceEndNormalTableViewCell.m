//
//  FaceEndNormalTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/6.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FaceEndNormalTableViewCell.h"

@interface FaceEndNormalTableViewCell()
@property (nonatomic,strong)UILabel *infoLabel;
@end

@implementation FaceEndNormalTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.infoLabel = [GWViewTool createLabelFont:@"13" textColor:@"545454"];
    self.infoLabel.numberOfLines = 3;
    self.infoLabel.textAlignment = NSTextAlignmentCenter;
    self.infoLabel.text = @"恭喜您已完成快捷认证，推荐您进一步\r完成实名认证（个人、产业人、机构），实名\r认证完成可享受更多权益";
    self.infoLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, 3 * [NSString contentofHeightWithFont:self.infoLabel.font]);
    [self addSubview:self.infoLabel];
}

+(CGFloat)calculationCellHeight{
    return 4 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]];
}

@end
