//
//  CenterUserInfoLivePlayerDetailView.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterUserInfoLivePlayerDetailView.h"

static char actionClickWithShowUserInfoBlockKey;
@interface CenterUserInfoLivePlayerDetailView()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nickLabel;
@property (nonatomic,strong)UIButton *actionButton;
@end

@implementation CenterUserInfoLivePlayerDetailView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.nickLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    [self addSubview:self.nickLabel];
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.actionButton];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithShowUserInfoBlockKey);
        if (block){
            block();
        }
    }];
    
}

-(void)actionClickWithShowUserInfoBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithShowUserInfoBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
