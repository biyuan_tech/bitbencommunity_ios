//
//  CenterUserInformationEvaluateTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterUserInformationEvaluateTableViewCell.h"
#import "CenterUserInformationArticleUserView.h"
#import "CenterUserInformationEvaluateShareView.h"

@interface CenterUserInformationEvaluateTableViewCell()
@property (nonatomic,strong)CenterUserInformationArticleUserView *userInfoView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)CenterUserInformationEvaluateShareView *shareView;
@property (nonatomic,strong)PDImageView *coinImgView;
@property (nonatomic,strong)UILabel *coinLabel;
@end

@implementation CenterUserInformationEvaluateTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.userInfoView = [[CenterUserInformationArticleUserView alloc]init];
    [self addSubview:self.userInfoView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    [self addSubview:self.titleLabel];
    
    self.shareView = [[CenterUserInformationEvaluateShareView alloc]init];
    [self addSubview:self.shareView];
    
    self.coinImgView = [[PDImageView alloc]init];
    [self addSubview:self.coinImgView];
    
    self.coinLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"灰"];
    [self addSubview:self.coinLabel];
}

@end
