//
//  CenterMessageZanTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/19.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageZanTableViewCell.h"

@interface CenterMessageZanTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;             /**< 头像*/
@property (nonatomic,strong)UILabel *nickLabel;                     /**< 名称*/
@property (nonatomic,strong)UIView *desInfoBgView;                  /**< 内容背景*/
@property (nonatomic,strong)UILabel *desLabel;                      /**< 内容label*/
@property (nonatomic,strong)UILabel *timeLabel;                     /**< 时间label*/
@property (nonatomic,strong)PDImageView *desConvertImgView;         /**< z内容遮罩层*/
@property (nonatomic,strong)PDImageView *desImgView;
@property (nonatomic,strong)PDImageView *zanImgView;
@property (nonatomic,strong)PDImageView *playerImgView;
/** 承载成就图标 */
@property (nonatomic ,strong) UIView *achievementView;

@end

@implementation CenterMessageZanTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    // 昵称
    self.nickLabel = [GWViewTool createLabelFont:@"14" textColor:@"545454"];
    [self addSubview:self.nickLabel];
    
    // 承载成就view
    self.achievementView = [[UIView alloc] init];
    [self.contentView addSubview:self.achievementView];

    // 内容背景
    self.desInfoBgView = [[UIView alloc]init];
    self.desInfoBgView.backgroundColor = [UIColor colorWithCustomerName:@"F5F5F5"];
    [self addSubview:self.desInfoBgView];
  
    // 内容
    self.desLabel = [GWViewTool createLabelFont:@"14" textColor:@"8F8F8F"];
    self.desLabel.numberOfLines = 0;
    [self.desInfoBgView addSubview:self.desLabel];
    
    
    // 赞按钮
    self.zanImgView = [[PDImageView alloc]init];
    self.zanImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.zanImgView];
    
    // 时间
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"A2A2A2"];
    [self addSubview:self.timeLabel];
    
    // desImgView
    self.desImgView = [[PDImageView alloc]init];
    self.desImgView.backgroundColor = [UIColor clearColor];
    self.desImgView.contentMode = UIViewContentModeScaleAspectFill;
    self.desImgView.clipsToBounds = YES;
    [self addSubview:self.desImgView];
    
    // player
    self.playerImgView = [[PDImageView alloc]init];
    self.playerImgView.backgroundColor = [UIColor clearColor];
    self.playerImgView.image = [UIImage imageNamed:@"icon_message_live_player_img"];
    [self addSubview:self.playerImgView];
    
    // 遮罩层
    self.desConvertImgView = [[PDImageView alloc]init];
    self.desConvertImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.desConvertImgView];
    
}

-(void)setTransferMessageModel:(CenterMessageRootSingleModel *)transferMessageModel{
    _transferMessageModel = transferMessageModel;
    
    self.avatarImgView.style = transferMessageModel.cert_badge;
    // 1. 头像
    self.avatarImgView.frame = CGRectMake(LCFloat(15), LCFloat(15), LCFloat(42), LCFloat(42));
    [self.avatarImgView uploadImageWithRoundURL:transferMessageModel.operate_pic placeholder:nil callback:NULL];
    
    // 2.昵称
    self.nickLabel.text = transferMessageModel.operate_name;
    CGSize nickSize = [Tool makeSizeWithLabel:self.nickLabel];
    CGFloat originX = CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11);
    CGFloat maxWidth = kCommonScreenWidth - originX - 68 - LCFloat(15) - LCFloat(76);
    CGFloat nickWidth = nickSize.width > maxWidth ? maxWidth : nickSize.width;
    self.nickLabel.frame = CGRectMake(originX, LCFloat(8), nickWidth, nickSize.height);
    
    // 添加成就
    self.achievementView.frame = CGRectMake(CGRectGetMaxX(_nickLabel.frame) + 5,
                                            0, 58, 16);
    self.achievementView.center = CGPointMake(self.achievementView.center_x, self.nickLabel.center_y);
    
    for (UIView *subView in self.achievementView.subviews) {
        [subView removeFromSuperview];
    }
    
    NSArray *achievementData = transferMessageModel.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [self.achievementView addSubview:imageView];
    }
    
    // 3. 赞按钮
    self.zanImgView.image = [UIImage imageNamed:@"icon_article_zan_nor"];
    self.zanImgView.frame = CGRectMake(self.nickLabel.orgin_x, CGRectGetMaxY(self.nickLabel.frame) + LCFloat(11), LCFloat(15), LCFloat(15));

    // 4. 时间
    self.timeLabel.text = [NSDate getTimeGap:self.transferMessageModel.create_time / 1000.];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(self.nickLabel.orgin_x, CGRectGetMaxY(self.zanImgView.frame) + LCFloat(20), timeSize.width, timeSize.height);
    
    // 5.convert
    self.desInfoBgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(76), LCFloat(8), LCFloat(76), LCFloat(76));
    
    self.desLabel.frame = self.desInfoBgView.bounds;
    
    // 6. des
    self.desImgView.frame = self.desInfoBgView.frame;
    
    self.playerImgView.hidden = YES;
    if (transferMessageModel.type_code == PushTypeArticleSupport){          // 文章被赞
        if (transferMessageModel.theme_pic.length){
            NSArray *companyArr = [transferMessageModel.theme_pic componentsSeparatedByString:@","];
            if (companyArr.count > 0){
                NSString *firstImg = [companyArr firstObject];
                [self.desImgView uploadHDImageWithURL:firstImg callback:NULL] ;
            } else {
                self.desImgView.image = [UIImage imageNamed:@"bg_article_detail_guandian_tuijian_nor"];
            }
        } else {
            self.desImgView.image = [UIImage imageNamed:@"bg_article_detail_guandian_tuijian_nor"];
        }
        
        self.desImgView.hidden = NO;
        self.desInfoBgView.hidden = YES;
    }else if (transferMessageModel.type_code == PushTypeLiveSuport || transferMessageModel.type_code == PushTypeLiveMyBeZan){
        if (transferMessageModel.theme_pic.length){
            NSArray *companyArr = [transferMessageModel.theme_pic componentsSeparatedByString:@","];
            if (companyArr.count > 0){
                NSString *firstImg = [companyArr firstObject];
                [self.desImgView uploadHDImageWithURL:firstImg callback:NULL] ;
            } else {
                self.desImgView.image = [UIImage imageNamed:@"bg_article_detail_guandian_tuijian_nor"];
            }
        } else {
            self.desImgView.image = [UIImage imageNamed:@"bg_article_detail_guandian_tuijian_nor"];
        }
        
        self.desImgView.hidden = NO;
        self.desInfoBgView.hidden = YES;
        self.playerImgView.hidden = NO;
        self.playerImgView.frame = CGRectMake(self.desInfoBgView.orgin_x + (self.desInfoBgView.size_width - LCFloat(40)) / 2., self.desInfoBgView.orgin_y + (self.desInfoBgView.size_height - LCFloat(40)) / 2., LCFloat(40), LCFloat(40));
    } else if (transferMessageModel.type_code == PushTypeArticleSubSupport || transferMessageModel.type_code == PushTypeLiveSubSuport || transferMessageModel.type_code == PushTypeLiveMyCommentBeZan){
        self.desImgView.hidden = YES;
        self.desInfoBgView.hidden = NO;
        self.desLabel.text = transferMessageModel.comment_content;
        self.desLabel.frame = CGRectMake(LCFloat(5), LCFloat(5), self.desInfoBgView.size_width - LCFloat(10), self.desInfoBgView.size_height - 2 * LCFloat(5));
    }
    self.desConvertImgView.image = [Tool stretchImageWithName:@"icon_message_live_img_convert"];
    self.desConvertImgView.frame = self.desInfoBgView.frame;
    
//    if (transferMessageModel.theme_pic.length){          //如果有图片的话显示图片
//        [self.desImgView uploadHDImageWithURL:transferMessageModel.theme_pic callback:NULL];
//
//        self.desImgView.hidden = NO;
//        self.desInfoBgView.hidden = YES;
//    } else {
//        self.desImgView.hidden = YES;
//        self.desInfoBgView.hidden = NO;
//        self.desLabel.text = transferMessageModel.comment_content;
//        self.desLabel.frame = CGRectMake(LCFloat(5), LCFloat(5), self.desInfoBgView.size_width - LCFloat(10), self.desInfoBgView.size_height - 2 * LCFloat(5));
//    }
    
}

#pragma mark - 文章赞
-(void)articleZanManager:(CenterMessageRootSingleModel *)transferMessageModel{
    if (transferMessageModel.theme_pic.length){
        [self.desImgView uploadHDImageWithURL:transferMessageModel.theme_pic callback:NULL];
        self.desImgView.hidden = NO;
        self.desInfoBgView.hidden = YES;
    } else {
        self.desImgView.hidden = YES;
        self.desInfoBgView.hidden = NO;
        self.desLabel.text = transferMessageModel.comment_content;
    }
}

#pragma mark - 文章评论赞
-(void)articleCommentZanManager:(CenterMessageRootSingleModel *)transferMessageModel{
   
}


+(CGFloat)calculationCellHeightWithModel:(CenterMessageRootSingleModel *)centerModel{
    CGFloat cellHeight = 0 ;
    cellHeight += LCFloat(8);
    cellHeight += LCFloat(76);
    cellHeight += LCFloat(8);
    return cellHeight;
}


@end
