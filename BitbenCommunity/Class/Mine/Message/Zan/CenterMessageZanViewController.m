//
//  CenterMessageZanViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/18.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageZanViewController.h"
#import "NetworkAdapter+Center.h"
#import "CenterMessageZanTableViewCell.h"

@interface CenterMessageZanViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *zanTableView;
@property (nonatomic,strong)NSMutableArray *zanMutableArr;
@end

@implementation CenterMessageZanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createView];
    [self sendRequestToGetMessageZanList];

}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"点赞";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.zanMutableArr = [NSMutableArray array];
}

-(void)createView{
    if (!self.zanTableView){
        self.zanTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.zanTableView.dataSource = self;
        self.zanTableView.delegate = self;
        [self.view addSubview:self.zanTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.zanTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetMessageZanList];
    }];
    
    [self.zanTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetMessageZanList];
    }];
    
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.zanMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    CenterMessageZanTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[CenterMessageZanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    CenterMessageRootSingleModel *messageSingleModel = [self.zanMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferMessageModel = messageSingleModel;
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CenterMessageRootSingleModel *messageSingleModel = [self.zanMutableArr objectAtIndex:indexPath.row];

    return [CenterMessageZanTableViewCell calculationCellHeightWithModel:messageSingleModel];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.zanTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.zanMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.zanMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}


#pragma mark - Interface
-(void)sendRequestToGetMessageZanList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerMessageGetListWithPage:self.zanTableView.currentPage type:MessageTypeZan block:^(CenterMessageRootListModel *listModel) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.zanTableView.isXiaLa){
            [strongSelf.zanMutableArr removeAllObjects];
        }
        
        [strongSelf.zanMutableArr addObjectsFromArray:listModel.content];
        [strongSelf.zanTableView reloadData];
        
        
        if (strongSelf.zanMutableArr.count){
            [strongSelf.zanTableView dismissPrompt];
        } else {
            [strongSelf.zanTableView showPrompt:@"当前没有信息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
        }
        
        if (strongSelf.zanTableView.isXiaLa){
            [strongSelf.zanTableView stopPullToRefresh];
        } else {
            [strongSelf.zanTableView stopFinishScrollingRefresh];
        }
    }];
}

@end
