//
//  CenterMessageZanTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/19.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "CenterMessageRootSingleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterMessageZanTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)CenterMessageRootSingleModel *transferMessageModel;

+(CGFloat)calculationCellHeightWithModel:(CenterMessageRootSingleModel *)centerModel;

@end

NS_ASSUME_NONNULL_END
