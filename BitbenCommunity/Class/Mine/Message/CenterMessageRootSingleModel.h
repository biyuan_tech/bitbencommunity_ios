//
//  CenterMessageRootSingleModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/18.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"
#import "CenterMessageLiveSingleModel.h"
#import "PushNotifManager.h"
#import "CenterMessageCommentFetchModel.h"
#import "BYLiveDefine.h"

NS_ASSUME_NONNULL_BEGIN

@protocol CenterMessageRootSingleModel <NSObject>

@end

@interface CenterMessageRootSingleModel : FetchModel


@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *picture;
@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,assign)BOOL is_read;
@property (nonatomic,copy)NSString *message_id;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,assign)MessageType type;
@property (nonatomic,copy)NSString *type_content;
@property (nonatomic,copy)NSString *user_id;
@property (nonatomic,copy)NSString *theme_content;
@property (nonatomic,copy)NSString *theme_pic;
@property (nonatomic,assign)NSInteger link_status;
/** vip认证 */
@property (nonatomic ,assign) NSInteger cert_badge;
/** 成就组 */
@property (nonatomic ,strong) NSArray <NSString *>*achievement_badge_list;

// 推送使用
@property (nonatomic,assign)PushType type_code;
@property (nonatomic,copy)NSString *theme_id;
@property (nonatomic,assign)BY_NEWLIVE_TYPE live_type;         /**< 如果是直播的话，返回直播类型*/
@property (nonatomic,copy)NSString *user_message_id;           /**< 用来转账进行跳转*/
@property (nonatomic,assign)BY_THEME_TYPE theme_type;          /**< 判断直播还是文章还是外链*/


// 原先的Remind
@property (nonatomic,copy)NSString *operate_name;
@property (nonatomic,copy)NSString *operate_pic;
@property (nonatomic,copy)NSString *operate_user_id;
@property (nonatomic,copy)NSString *comment_content;
@property (nonatomic,copy)NSString *comment_id;

@property (nonatomic,copy)NSString *reply_comment_content;
@property (nonatomic,copy)NSString *reply_comment_id;

@end


@interface CenterMessageRootListModel:FetchModel
@property (nonatomic,strong)NSArray<CenterMessageRootSingleModel > *content;
@end

NS_ASSUME_NONNULL_END
