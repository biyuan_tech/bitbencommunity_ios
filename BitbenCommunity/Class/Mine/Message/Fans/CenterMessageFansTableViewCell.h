//
//  CenterMessageFansTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/18.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "CenterFollowModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterMessageFansTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)CenterFollowContentModel *transferContentModel;

-(void)actionClickWithLinkWithBlock:(void(^)(BOOL status))block;
-(void)actionClickHeaderImgWithBlock:(void (^)(void))block;

-(void)btnStatusSelected:(BOOL)isSelected;
@end

NS_ASSUME_NONNULL_END
