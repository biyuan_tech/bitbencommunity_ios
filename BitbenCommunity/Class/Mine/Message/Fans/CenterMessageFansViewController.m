//
//  CenterMessageFansViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/18.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageFansViewController.h"
#import "CenterMessageFansTableViewCell.h"
#import "NetworkAdapter+Center.h"

@interface CenterMessageFansViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *fansTableView;
@property (nonatomic,strong)NSMutableArray *fansMutableArr;
@end

@implementation CenterMessageFansViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"新粉丝";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.fansMutableArr = [NSMutableArray array];
}

#pragma mark - createTableView
-(void)createTableView{
    if (!self.fansTableView){
        self.fansTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.fansTableView.dataSource = self;
        self.fansTableView.delegate = self;
        [self.view addSubview:self.fansTableView];
        __weak typeof(self)weakSelf = self;
        [self.fansTableView appendingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetInfo];
        }];
        [self.fansTableView appendingFiniteScrollingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetInfo];
        }];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.fansMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    CenterMessageFansTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[CenterMessageFansTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    CenterFollowContentModel *followModel = [self.fansMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferContentModel = followModel;

    __weak typeof(self)weakSelf = self;
    [cellWithRowOne actionClickWithLinkWithBlock:^(BOOL status) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToLinkManagerModel:cellWithRowOne link:status];
    }];
    
    [cellWithRowOne actionClickHeaderImgWithBlock:^{
        
    }];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CenterMessageFansTableViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.fansTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.fansMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.fansMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}

#pragma mark - Interface
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] sendRequestToGetMyFansManagerWithNumber:self.fansTableView.currentPage block:^(BOOL isSuccessed, CenterFollowModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(strongSelf.fansTableView.isXiaLa){
            [strongSelf.fansMutableArr removeAllObjects];
        }
        [strongSelf.fansMutableArr addObjectsFromArray:singleModel.content];
        [strongSelf.fansTableView reloadData];
        
        if (strongSelf.fansTableView.isXiaLa){
            [strongSelf.fansTableView stopPullToRefresh];
        } else {
            [strongSelf.fansTableView stopFinishScrollingRefresh];
        }

        
        if(strongSelf.fansMutableArr.count){
            [strongSelf.fansTableView dismissPrompt];
        } else {
            [strongSelf.fansTableView showPrompt:@"当前没有粉丝哦，快去直播吧" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
        }
        
    }];
}

#pragma mark - 关注
-(void)sendRequestToLinkManagerModel:(CenterMessageFansTableViewCell *)cell link:(BOOL)link{
    __weak typeof(self)weakSelf = self;
    
    NSString *userId = cell.transferContentModel.attention_user_id;
    NSString *otherId = cell.transferContentModel.user_id;
    NSString *mainId = [userId isEqualToString:[AccountModel sharedAccountModel].loginServerModel.user._id]?otherId:userId;
    [[NetworkAdapter sharedAdapter]centerSendRequestToLinkManagerWithUserId:mainId hasLink:link block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        if (isSuccessed){
            if (link){
                [AccountModel sharedAccountModel].loginServerModel.infoCount.attention += 1;
            } else {
                [AccountModel sharedAccountModel].loginServerModel.infoCount.attention -= 1;
            }
            
            [cell btnStatusSelected:link];
        }
    }];
    
}


@end
