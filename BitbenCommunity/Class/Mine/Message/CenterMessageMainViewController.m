//
//  CenterMessageMainViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/17.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageMainViewController.h"
#import "CenterNotifyRootViewController.h"
#import "CenterMessageRootViewController.h"

@interface CenterMessageMainViewController()<HTHorizontalSelectionListDelegate,HTHorizontalSelectionListDataSource>
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)NSArray *segmentArr;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)CenterNotifyRootViewController *centerNotifyVC;
@property (nonatomic,strong)CenterMessageRootViewController *centerMessageVC;


@end

@implementation CenterMessageMainViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createSegment];
    [self createMainScrollView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"通知";
    self.view.backgroundColor = RGB(243, 245, 249, 1);
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.segmentArr = @[@"通知",@"消息"];
}

#pragma mark - SegmentList
-(void)createSegment{
    PDSelectionListTitleView *segment = [[PDSelectionListTitleView alloc]initWithFrame: CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(40))];
    segment.delegate = self;
    segment.dataSource = self;
    [segment setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
    segment.selectionIndicatorColor = [UIColor colorWithCustomerName:@"红"];
    segment.bottomTrimColor = [UIColor clearColor];
    segment.isNotScroll = YES;
    [segment setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    segment.backgroundColor = [UIColor clearColor];
    segment.hasArticle = YES;
    self.segmentList = segment;
    self.navigationItem.titleView = self.segmentList;
}

- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList{
    return self.segmentArr.count;
}

- (nullable NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index{
    return [self.segmentArr objectAtIndex:index];
}


-(void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index{
    [self.mainScrollView setContentOffset:CGPointMake(index *kScreenBounds.size.width, 0) animated:YES];
}

#pragma mark - UIScrollView
-(void)createMainScrollView{
    __weak typeof(self)weakSelf = self;
    self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger page = scrollView.contentOffset.x / kScreenBounds.size.width;
        [strongSelf.segmentList setSelectedButtonIndex:page animated:YES];
    }];
    self.mainScrollView.frame = self.view.bounds;
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.size_width * 2,self.mainScrollView.size_height);
    [self.view addSubview:self.mainScrollView];
    [self createMessageVC];
    [self createNotifyVC];
}


#pragma mark - createView
-(void)createMessageVC{
    self.centerMessageVC = [CenterMessageRootViewController sharedController];
    self.centerMessageVC.view.frame = CGRectMake(kScreenBounds.size.width, 0, self.mainScrollView.size_width, self.mainScrollView.size_height);
    [self.mainScrollView addSubview:self.centerMessageVC.view];
    [self addChildViewController:self.centerMessageVC];
}

-(void)createNotifyVC{
    self.centerNotifyVC = [[CenterNotifyRootViewController alloc]init];;
    self.centerNotifyVC.view.backgroundColor = self.view.backgroundColor;
    self.centerNotifyVC.view.frame = CGRectMake(0, 0, self.mainScrollView.size_width, self.mainScrollView.size_height);
    [self.mainScrollView addSubview:self.centerNotifyVC.view];
    [self addChildViewController:self.centerNotifyVC];
}



@end
