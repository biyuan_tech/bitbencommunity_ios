//
//  CenterMessageLiveTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/18.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageLiveTableViewCell.h"
#import "PushNotifManager.h"

@interface CenterMessageLiveTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *desLabel;
@property (nonatomic,strong)UILabel *timeLabel;

@end

@implementation CenterMessageLiveTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.frame = CGRectMake(LCFloat(15), LCFloat(15), LCFloat(41), LCFloat(41));
    [self addSubview:self.avatarImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"000000"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(13), LCFloat(19), kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.avatarImgView.frame) - LCFloat(13), [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    self.desLabel = [GWViewTool createLabelFont:@"14" textColor:@"545454"];
    self.desLabel.numberOfLines = 0;
    [self addSubview:self.desLabel];

    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"A2A2A2"];
    [self addSubview:self.timeLabel];
}

#pragma mark -

-(void)setTransferMessageLiveModel:(CenterMessageRootSingleModel *)transferMessageLiveModel{
    _transferMessageLiveModel = transferMessageLiveModel;
    
    [self.avatarImgView uploadImageWithURL:transferMessageLiveModel.operate_pic placeholder:nil callback:NULL];
    
    if (transferMessageLiveModel.type_code == PushTypeLiveMyLink){
        NSString *info = [NSString stringWithFormat:@"你关注的主播 %@ 开播了",transferMessageLiveModel.operate_name];
        self.titleLabel.attributedText = [Tool rangeLabelWithContent:info hltContentArr:@[transferMessageLiveModel.operate_name] hltColor:[UIColor hexChangeFloat:@"5C92D5"] normolColor:[UIColor hexChangeFloat:@"000000"]];
    } else if (transferMessageLiveModel.type_code == PushTypeLiveMyAppointment){
        NSString *info = [NSString stringWithFormat:@"你预约的直播开播了"];
        self.titleLabel.text = info;
    }
    
    self.desLabel.text = transferMessageLiveModel.content;
    CGFloat width = kScreenBounds.size.width - CGRectGetMaxX(self.avatarImgView.frame) - LCFloat(13) - LCFloat(11);
    CGSize desSize = [self.desLabel.text sizeWithCalcFont:self.desLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.desLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), width, desSize.height);
    
    self.timeLabel.text = [NSDate getTimeGap:transferMessageLiveModel.create_time / 1000.];
    self.timeLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.desLabel.frame) + LCFloat(12), 200, [NSString contentofHeightWithFont:self.timeLabel.font]);
}


+(CGFloat)calculationCellHeightWithModel:(CenterMessageRootSingleModel *)model{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(19);
    cellHeight += [NSString contentofHeightWithFont:[[UIFont fontWithCustomerSizeName:@"15"] boldFont]];
    cellHeight += LCFloat(11);
    CGFloat width = kScreenBounds.size.width - LCFloat(68) - LCFloat(11);
    CGSize desSize = [model.content sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"14"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += desSize.height;
    cellHeight += LCFloat(14);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    cellHeight += LCFloat(16);
    return cellHeight;
}
@end
