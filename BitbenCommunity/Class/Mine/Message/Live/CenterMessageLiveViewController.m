//
//  CenterMessageLiveViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/18.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageLiveViewController.h"
#import "CenterMessageLiveTableViewCell.h"
#import "NetworkAdapter+Center.h"

@interface CenterMessageLiveViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *centerMessageTableView;
@property (nonatomic,strong)NSMutableArray *centerMessageMutableArr;
@end

@implementation CenterMessageLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfo];

}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"直播提醒";
}

-(void)arrayWithInit{
    self.centerMessageMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.centerMessageTableView){
        self.centerMessageTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.centerMessageTableView.delegate = self;
        self.centerMessageTableView.dataSource = self;
        [self.view addSubview:self.centerMessageTableView];
        __weak typeof(self)weakSelf = self;
        [self.centerMessageTableView appendingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetInfo];
        }];
        [self.centerMessageTableView appendingFiniteScrollingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetInfo];
        }];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.centerMessageMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellidentifyWithRowOne = @"cellidentifyWithRowOne";
    CenterMessageLiveTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellidentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[CenterMessageLiveTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifyWithRowOne];
    }
    CenterMessageRootSingleModel *singleModel = [self.centerMessageMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferMessageLiveModel = singleModel;
    return cellWithRowOne;
}

#pragma makr - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CenterMessageRootSingleModel *singleModel = [self.centerMessageMutableArr objectAtIndex:indexPath.row];

    return [CenterMessageLiveTableViewCell calculationCellHeightWithModel:singleModel];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.centerMessageTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.centerMessageMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.centerMessageMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}

-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerMessageGetListWithPage:self.centerMessageTableView.currentPage type:MessageTypeLive block:^(CenterMessageRootListModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.centerMessageTableView.isXiaLa){
            [strongSelf.centerMessageMutableArr removeAllObjects];
        }
        [strongSelf.centerMessageMutableArr addObjectsFromArray:listModel.content];
        [strongSelf.centerMessageTableView reloadData];
        
        if (strongSelf.centerMessageMutableArr.count){
            [strongSelf.centerMessageTableView dismissPrompt];
        } else {
            [strongSelf.centerMessageTableView showPrompt:@"没有消息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
        }

        if (strongSelf.centerMessageTableView.isXiaLa){
            [strongSelf.centerMessageTableView stopPullToRefresh];
        } else {
            [strongSelf.centerMessageTableView stopFinishScrollingRefresh];
        }

    }];
}

@end
