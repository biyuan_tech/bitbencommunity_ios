//
//  CenterMessageLiveSingleModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/18.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"
#import "BYLiveDefine.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterMessageLiveSingleModel : FetchModel

@property (nonatomic,copy)NSString *operate_name;
@property (nonatomic,copy)NSString *operate_pic;
@property (nonatomic,copy)NSString *operate_user_id;
@property (nonatomic,copy)NSString *theme_content;
@property (nonatomic,copy)NSString *theme_id;
@property (nonatomic,assign)BY_THEME_TYPE theme_type;
@property (nonatomic,assign)BY_NEWLIVE_TYPE live_type;

@end

NS_ASSUME_NONNULL_END
