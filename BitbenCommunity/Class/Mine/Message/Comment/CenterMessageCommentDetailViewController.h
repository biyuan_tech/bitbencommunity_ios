//
//  CenterMessageCommentDetailViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/22.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterMessageCommentDetailViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferArticleId;


@end

NS_ASSUME_NONNULL_END
