//
//  CenterMessageCommentTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/17.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "CenterMessageRootSingleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterMessageCommentTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)CenterMessageRootSingleModel *transferCommentModel;

-(void)actionClickWithAvatar:(void(^)(NSString *userId))block;

+(CGFloat)calculationCellHeightWithModel:(CenterMessageRootSingleModel *)model;
@end

NS_ASSUME_NONNULL_END
