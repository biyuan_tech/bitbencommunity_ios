//
//  CenterMessageCommentDetailViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/22.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterMessageCommentDetailViewController.h"
#import "ArticleDetailTitleTableViewCell.h"             // 标题头
#import "ArticleDetailAvatarTableViewCell.h"
#import "ArticleDetailTagsTableViewCell.h"              // 话题标签
#import "ArticleDetailItemsTableViewCell.h"
#import "NetworkAdapter+Article.h"
#import "LiveForumRootInputView.h"
#import "LiveForumRootSingleTableViewCell.h"
#import "LiveForumItemsTableViewCell.h"
#import "LiveForumRootSingleSubTableViewCell.h"
#import "LiveForumDetailViewController.h"
#import "ArticleDetailInfoTableViewCell.h"
#import "ArticleDetailNoneInfoTableViewCell.h"
#import "GWAssetsImgSelectedViewController.h"
#import "ShareRootViewController.h"
#import "NetworkAdapter+Center.h"
#import "BYPersonHomeController.h"
#import "ReportViewController.h"
#import "BYPersonHomeController.h"
#import "ReportViewController.h"
#import "CenterMessageCommentDetailHeaderTableViewCell.h"
#import "CenterMessageCommentDetailSubTableViewCell.h"

@interface CenterMessageCommentDetailViewController ()<UITableViewDelegate,UITableViewDataSource,WKNavigationDelegate,WKUIDelegate>{
    CGRect newKeyboardRect;
    LiveFourumRootListSingleModel *tempSingleModle;
    CGFloat webHeight;
}
@property (nonatomic,strong)UITableView *articleDetailTableView;
@property (nonatomic,strong)LiveForumRootInputView *inputView;                  // 输入框
@property (nonatomic,strong)NSMutableArray *articleDetailRootMutableArr;        /**< 整体*/
@property (nonatomic,strong)NSArray *infoArr;
@property (nonatomic,strong)NSMutableArray *articleNormalMutableArr;            /**< 全部*/

@end

@implementation CenterMessageCommentDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createInputView];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"观点详情";
    self.view.backgroundColor = [UIColor hexChangeFloat:@"EBEBEB"];
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_article_more"] barHltImage:[UIImage imageNamed:@"icon_article_more"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf shareManager];
    }];
}

-(void)arrayWithInit{
    
    self.articleDetailRootMutableArr = [NSMutableArray array];
    [self.articleDetailRootMutableArr addObject:@[@"头部"]];
    
    self.articleNormalMutableArr = [NSMutableArray array];
}

#pragma mark - 创建输入框
-(void)createInputView{
    if (!self.inputView){
        CGRect inputFrame = CGRectMake(0, kScreenBounds.size.height - [LiveForumRootInputView calculationHeight:@""] - [BYTabbarViewController sharedController].navBarHeight, kScreenBounds.size.width, [LiveForumRootInputView  calculationHeight:@""]);
        self.inputView = [[LiveForumRootInputView alloc]initWithFrame:inputFrame];
        self.inputView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        self.inputView.layer.shadowOpacity = .2f;
        self.inputView.layer.shadowOffset = CGSizeMake(.2f, .2f);
        
        [self.view addSubview:self.inputView];
    }
    __weak typeof(self)weakSelf = self;
    [self.inputView actionClickWithSendInfoBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToAddComment];
    }];
    [self.inputView actionTextInputChangeBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (IS_iPhoneX){
            strongSelf.inputView.orgin_y = strongSelf.view.size_height - strongSelf->newKeyboardRect.size.height - [LiveForumRootInputView calculationHeight:strongSelf.inputView.inputView.text] + LCFloat(20) ;
        } else {
            strongSelf.inputView.orgin_y = strongSelf.view.size_height - strongSelf->newKeyboardRect.size.height - [LiveForumRootInputView calculationHeight:strongSelf.inputView.inputView.text] ;
        }
    }];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.articleDetailTableView){
        self.articleDetailTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.articleDetailTableView.size_height -= [LiveForumRootInputView  calculationHeight:@""];
        self.articleDetailTableView.dataSource= self;
        self.articleDetailTableView.delegate = self;
        [self.view addSubview:self.articleDetailTableView];
    }
    
    [self.view addSubview:self.inputView];
    __weak typeof(self)weakSelf = self;
    [self.articleDetailTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetCommentListWithReload:YES];
    }];
    
    [self.articleDetailTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetCommentListWithReload:NO];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.articleDetailRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"头部" sourceArr:self.articleDetailRootMutableArr]){
        return self.infoArr.count;
    } else {
        id info = [self.articleDetailRootMutableArr objectAtIndex:section];
        if ([info isKindOfClass:[NSArray class]]){
            return 1;
        } else if ([info isKindOfClass:[LiveFourumRootListSingleModel class]]){
            LiveFourumRootListSingleModel *tempModel = (LiveFourumRootListSingleModel *)[self.articleDetailRootMutableArr objectAtIndex:section];
            return tempModel.child.count + 2;
        }
        return 1;
    }
    
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头部" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"头部" sourceArr:self.articleDetailRootMutableArr]){
//        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
//        CenterMessageCommentDetailHeaderTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
//        if (!cellWithRowOne){
//            cellWithRowOne = [[CenterMessageCommentDetailHeaderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
//        }
//
//        return cellWithRowOne;
//    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"头像" sourceArr:self.articleDetailRootMutableArr]){
//        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
//        ArticleDetailAvatarTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
//        if (!cellWithRowTwo){
//            cellWithRowTwo = [[ArticleDetailAvatarTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
//        }
//        cellWithRowTwo.transferArticleModel = self.transferArticleModel;
//        __weak typeof(self)weakSelf = self;
//        [cellWithRowTwo actionLinkButtonClick:^{
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            [strongSelf linkWIthCell:cellWithRowTwo hasLink:!strongSelf.transferArticleModel.isAttention];
//        }];
//        [cellWithRowTwo actionClickHeaderImgWithBlock:^{
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
//            personHomeController.user_id = cellWithRowTwo.transferArticleModel.author_id;
//            [strongSelf.navigationController pushViewController:personHomeController animated:YES];
//        }];
//
//        return cellWithRowTwo;
//    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"内容" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"内容" sourceArr:self.articleDetailRootMutableArr]){
//        static NSString *cellIdentifyWithRowTwo1 = @"cellIdentifyWithRowTwo1";
//        ArticleDetailInfoTableViewCell *cellWithRowTwo1 = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo1];
//        if (!cellWithRowTwo1){
//            cellWithRowTwo1 = [[ArticleDetailInfoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo1];
//        }
//        __weak typeof(self)weakSelf = self;
//        [cellWithRowTwo1 actionClickWithImgSelectedBlock:^(NSString *imgUrl) {
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            GWAssetsImgSelectedViewController *imgSelected = [[GWAssetsImgSelectedViewController alloc]init];
//            NSInteger index = [strongSelf.transferArticleModel.picture indexOfObject:imgUrl];
//            [imgSelected showInView:strongSelf.parentViewController imgArr:strongSelf.transferArticleModel.picture currentIndex:index cell:cellWithRowTwo1];
//        }];
//
//        [cellWithRowTwo1 actionWebViewDidLoad:^{
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            [strongSelf.articleDetailTableView reloadData];
//        }];
//
//        [cellWithRowTwo1 actionClickWithWebImgBlock:^(NSInteger index, NSString *imgUrl, CGRect rect, NSArray *imgArr) {
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            GWAssetsImgSelectedViewController *imgSelected = [[GWAssetsImgSelectedViewController alloc]init];
//            imgSelected.webViewSelectedRect = rect;
//            // 获取点击的图片
//            NSString *indexImgUrl = [imgArr objectAtIndex:index];
//            if (!cellWithRowTwo1.selectedImgView){
//                cellWithRowTwo1.selectedImgView = [[PDImageView alloc]init];
//            }
//
//            [cellWithRowTwo1.selectedImgView uploadImageWithURL:indexImgUrl placeholder:nil callback:^(UIImage *image) {
//                [imgSelected showInView:strongSelf.parentViewController imgArr:imgArr currentIndex:index cell:cellWithRowTwo1];
//            }];
//        }];
//
//        [cellWithRowTwo1 actionClickWithWebUrlDirectBlock:^(NSString *webUrl) {
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            PDWebViewController *webViewController = [[PDWebViewController alloc]init];
//            [webViewController webDirectedWebUrl:webUrl];
//            [strongSelf.navigationController pushViewController:webViewController animated:YES];
//        }];
//
//        cellWithRowTwo1.transferArticleModel = self.transferArticleModel;
//        return cellWithRowTwo1;
//    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr]){
//        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
//        ArticleDetailTagsTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
//        if (!cellWithRowThr){
//            cellWithRowThr = [[ArticleDetailTagsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
//        }
//        cellWithRowThr.transferArticleModel = self.transferArticleModel;
//        __weak typeof(self)weakSelf = self;
//        [cellWithRowThr actionItemClickWithBlock:^(NSString *info) {
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            ArticleRootViewController *articleRootVC = [[ArticleRootViewController alloc]init];
//            articleRootVC.transferType = ArticleRootViewControllerTypeTags;
//            articleRootVC.transferTags = info;
//            [strongSelf.navigationController pushViewController:articleRootVC animated:YES];
//        }];
//
//        return cellWithRowThr;
//    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标签" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标签" sourceArr:self.articleDetailRootMutableArr]){
//        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
//        ArticleDetailItemsTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
//        if (!cellWithRowFiv){
//            cellWithRowFiv = [[ArticleDetailItemsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
//        }
//        cellWithRowFiv.transferArticleModel = self.transferArticleModel;
//        return cellWithRowFiv;
//    } else {
//        id info = [self.articleDetailRootMutableArr objectAtIndex:indexPath.section];
//        if ([info isKindOfClass:[NSArray class]]){
//            NSArray *tempInfoArr = (NSArray *)info;
//            if ([tempInfoArr containsObject:@"热门评论"] || [tempInfoArr containsObject:@"全部回答"]){
//                static NSString *cellIdentifyWithRowSix = @"cellIdentifyWithRowSix";
//                GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSix];
//                if (!cellWithRowOne){
//                    cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSix];
//                }
//                CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
//                cellWithRowOne.transferCellHeight = cellHeight;
//                if ([tempInfoArr containsObject:@"热门评论"]){
//                    cellWithRowOne.transferTitle = @"热门评论";
//                } else if ([tempInfoArr containsObject:@"全部回答"]){
//                    cellWithRowOne.transferTitle = @"全部回答";
//                }
//                return cellWithRowOne;
//            } else if ([tempInfoArr containsObject:@"空"]){
//                static NSString *cellIdentifyWithRowNone1 = @"cellIdentifyWithRowNone1";
//                ArticleDetailNoneInfoTableViewCell *cellWithRowNone = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowNone1];
//                if (!cellWithRowNone){
//                    cellWithRowNone = [[ArticleDetailNoneInfoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowNone1];
//                }
//                return cellWithRowNone;
//            }
//
//        } else if ([info isKindOfClass:[LiveFourumRootListSingleModel class]]){
//            LiveFourumRootListSingleModel *tempSingleModel = (LiveFourumRootListSingleModel *)info;
//            tempSingleModel.transferViewType = self.transferPageType;
//            NSInteger rowCount = tempSingleModel.child.count + 2;
//            if (indexPath.row == 0){
//                static NSString *cellIdentifyWithRowSev = @"cellIdentifyWithRowSev";
//                LiveForumRootSingleTableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev];
//                if (!cellWithRowSev){
//                    cellWithRowSev = [[LiveForumRootSingleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev];
//                }
//                cellWithRowSev.transferInfoManager = tempSingleModel;
//                __weak typeof(self)weakSelf = self;
//                [cellWithRowSev actionClickMoreBlock:^{
//                    if (!weakSelf){
//                        return ;
//                    }
//                    __strong typeof(weakSelf)strongSelf = weakSelf;
//
//                    ReportViewController *reportVC = [[ReportViewController alloc]init];
//                    if (strongSelf.transferPageType == ArticleDetailRootViewControllerTypeLive){        // 直播里使用
//                        [reportVC showInView:strongSelf.parentViewController type:BY_THEME_TYPE_LIVE itemId:tempSingleModel._id];
//                    } else if (strongSelf.transferPageType == ArticleDetailRootViewControllerTypeArticle){  // 文章
//                        [reportVC showInView:strongSelf.parentViewController type:BY_THEME_TYPE_ARTICLE_COMMENT itemId:tempSingleModel._id];
//                    }
//
//                }];
//                [cellWithRowSev actionClickHeaderImgWithBlock:^{
//                    if (!weakSelf){
//                        return ;
//                    }
//                    __strong typeof(weakSelf)strongSelf = weakSelf;
//                    BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
//                    personHomeController.user_id = cellWithRowSev.transferInfoManager.user_id;
//                    [strongSelf.navigationController pushViewController:personHomeController animated:YES];
//                }];
//
//                return cellWithRowSev;
//            } else if (indexPath.row == rowCount - 1){
//                static NSString *cellIdentifyWithRowEig = @"cellIdentifyWithRowEig";
//                LiveForumItemsTableViewCell *cellWithRowEig = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowEig];
//                if (!cellWithRowEig){
//                    cellWithRowEig = [[LiveForumItemsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowEig];
//                }
//                cellWithRowEig.transferType = LiveForumDetailViewControllerTypeArticle;
//                cellWithRowEig.transferItemId = self.transferArticleModel._id;
//                cellWithRowEig.transferInfoManager = tempSingleModel;
//
//                __weak typeof(self)weakSelf = self;
//                [cellWithRowEig itemsReplyManagerWithModel:^(LiveFourumRootListSingleModel *singleModle1) {
//                    if (!weakSelf){
//                        return ;
//                    }
//                    __strong typeof(weakSelf)strongSelf = weakSelf;
//                    strongSelf->tempSingleModle = singleModle1;
//                    strongSelf.inputView.transferListModel = singleModle1;
//
//                    if (![strongSelf.inputView.inputView isFirstResponder]){
//                        [strongSelf.inputView.inputView becomeFirstResponder];
//                    }
//                    strongSelf.inputView.inputView.text = @"";
//                    strongSelf.inputView.inputView.placeholder = [NSString stringWithFormat:@"回复：%@",singleModle1.comment_name];
//                }];
//
//                return cellWithRowEig;
//            } else {
//                static NSString *cellIdentifyWithRowNig = @"cellIdentifyWithRowNig";
//                LiveForumRootSingleSubTableViewCell *cellWithRowNig = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowNig];
//                if (!cellWithRowNig){
//                    cellWithRowNig = [[LiveForumRootSingleSubTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowNig];
//                }
//                //                LiveFourumRootListSingleModel *singleModel = [self.articleNormalMutableArr objectAtIndex:indexPath.section - 1];
//                cellWithRowNig.transferCount = tempSingleModel.childCount <= 3 ? -1:tempSingleModel.childCount;
//                NSInteger index = indexPath.row - 1;
//                if (tempSingleModel.child.count > 1){
//                    if (index == 0){
//                        cellWithRowNig.transferSubType = LiveForumRootSingleSubTableViewCellTypeTop;
//                    } else if (indexPath.row == tempSingleModel.child.count){
//                        cellWithRowNig.transferSubType = LiveForumRootSingleSubTableViewCellTypeBottom;
//                    } else {
//                        cellWithRowNig.transferSubType = LiveForumRootSingleSubTableViewCellTypeNormal;
//                    }
//                } else {
//                    cellWithRowNig.transferSubType = LiveForumRootSingleSubTableViewCellTypeSingle;
//                }
//                if (tempSingleModel.child.count){
//                    cellWithRowNig.transferInfoManager = [tempSingleModel.child objectAtIndex:index];
//                }
//
//                return cellWithRowNig;
//            }
//        }
//    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    if(indexPath.section > 0){
//        id info = [self.articleDetailRootMutableArr objectAtIndex:indexPath.section];
//        if ([info isKindOfClass:[NSArray class]]){
//            NSArray *tempInfoArr = (NSArray *)info;
//            if ([tempInfoArr containsObject:@"热门评论"] || [tempInfoArr containsObject:@"全部回答"]){
//
//            }
//        } else if ([info isKindOfClass:[LiveFourumRootListSingleModel class]]){
//            LiveFourumRootListSingleModel *singleModel = (LiveFourumRootListSingleModel *)info;
//
//            NSInteger rowCount = singleModel.child.count + 2;
//            if (indexPath.row < rowCount - 2){
//                LiveForumDetailViewController *controller = [[LiveForumDetailViewController alloc]init];
//                controller.transferPageType = LiveForumDetailViewControllerTypeArticle;
//                controller.transferFourumRootModel = singleModel;
//                controller.transferRoomId = self.transferArticleModel._id;
//                [self.navigationController pushViewController:controller animated:YES];
//            }
//        }
//    }
//
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr]){
//        NSString *info = @"";
//        if (self.transferArticleModel.article_type == article_typeWeb){         // 长文
//            info = self.transferArticleModel.title;
//        } else if (self.transferArticleModel.article_type == article_typeNormal){       // 短文
//            info = self.transferArticleModel.content.length?self.transferArticleModel.content:self.transferArticleModel.title;
//        }
//
//
//        return [ArticleDetailTitleTableViewCell calculationCellHeightTitle:info];
//    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"头像" sourceArr:self.articleDetailRootMutableArr]){
//        return [ArticleDetailAvatarTableViewCell calculationCellHeight];
//    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr]){
//        return [ArticleDetailTagsTableViewCell calculationCellHeight:self.transferArticleModel];
//    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标签" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标签" sourceArr:self.articleDetailRootMutableArr]){
//        return [ArticleDetailItemsTableViewCell calculationCellHeight];
//    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"内容" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"内容" sourceArr:self.articleDetailRootMutableArr]){
//        if (self.transferArticleModel.article_type == article_typeNormal) {
//            return [ArticleDetailInfoTableViewCell calculationCellHeightWithModel:self.transferArticleModel];
//        } else {
//            return self.transferArticleModel.webLoadTempModel.transferCellHeight;
//        }
//    } else {
//        id info = [self.articleDetailRootMutableArr objectAtIndex:indexPath.section];
//        if ([info isKindOfClass:[NSArray class]]){
//            NSArray *tempInfoArr = (NSArray *)info;
//            if ([tempInfoArr containsObject:@"热门评论"] || [tempInfoArr containsObject:@"全部回答"]){
//                return [GWNormalTableViewCell calculationCellHeight];
//            } else if ([tempInfoArr containsObject:@"空"]){
//                return [ArticleDetailNoneInfoTableViewCell calculationCellHeight];
//            }
//        } else if ([info isKindOfClass:[LiveFourumRootListSingleModel class]]){
//            LiveFourumRootListSingleModel *tempSingleModel = (LiveFourumRootListSingleModel *)info;
//            NSInteger rowCount = tempSingleModel.child.count + 2;
//            if (indexPath.row == 0){
//                return [LiveForumRootSingleTableViewCell calculationCellHeightWithModel:tempSingleModel];
//            } else if (indexPath.row == rowCount - 1){
//                return [LiveForumItemsTableViewCell calculationCellHeight];
//            } else {
//                NSInteger index = indexPath.row - 1;
//                if (tempSingleModel.child.count > 1){
//                    if (index == 0){
//                        return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:tempSingleModel type:LiveForumRootSingleSubTableViewCellTypeTop];
//                    } else if (indexPath.row == tempSingleModel.child.count){
//                        return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:tempSingleModel type:LiveForumRootSingleSubTableViewCellTypeBottom];
//                    } else {
//                        return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:tempSingleModel type:LiveForumRootSingleSubTableViewCellTypeNormal];
//                    }
//                } else {
//                    return [LiveForumRootSingleSubTableViewCell calculationCellHeightWithModel:tempSingleModel type:LiveForumRootSingleSubTableViewCellTypeSingle];
//                }
//            }
//        }
//    }
//
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"全部回答" sourceArr:self.articleDetailRootMutableArr]){
        return 20.;
    } else if (section == [self cellIndexPathSectionWithcellData:@"热门评论" sourceArr:self.articleDetailRootMutableArr]){
        return 20.;
    } else {
        return 1.f;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.articleDetailTableView) {
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr]){
            SeparatorType separatorType = SeparatorTypeBottom;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"message"];
        }
    }
}



- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr]){
        return YES;
    } else {
        return NO;
    }
}

// 允许每一个Action

- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr]){
        if (action == @selector(copy:)) { // 支持复制和黏贴
            return YES;
        }
    }
    
    return NO;
    
}



// 对一个给定的行告诉代表执行复制或黏贴操作内容

- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr]){
        
        ArticleDetailTitleTableViewCell *cell = (ArticleDetailTitleTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        UIPasteboard *pasteBoard = [UIPasteboard generalPasteboard]; // 黏贴板
        
        [pasteBoard setString:cell.titleLabel.text];
        
    }
}





#pragma mark - 新增评论
-(void)sendRequestToAddComment{
//    __weak typeof(self)weakSelf = self;
//    [[NetworkAdapter sharedAdapter] articleCreateCommentWiththemeId:self.transferArticleModel._id ThemeInfo:self.inputView.transferListModel  content:self.inputView.inputView.text block:^(BOOL isSuccessed) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSuccessed){
//            strongSelf.inputView.transferListModel = nil;
//            [strongSelf sendRequestToGetHotComment];
//            [strongSelf.inputView cleanKeyboard];
//
//        }
//    }];
}

#pragma mark - 获取热门评论
-(void)sendRequestToGetHotComment{
//    __weak typeof(self)weakSelf = self;
//    [[NetworkAdapter sharedAdapter] articleGetHotCommentId:self.transferArticleModel._id actionBlock:^(BOOL isSuccessed, LiveFourumRootListModel *list) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSuccessed){
//            [strongSelf.articleHotMutableArr removeAllObjects];
//            if (list.content.count){
//                [strongSelf.articleHotMutableArr addObject:@[@"热门评论"]];
//                [strongSelf.articleHotMutableArr addObjectsFromArray:list.content];
//                [strongSelf.articleDetailRootMutableArr addObjectsFromArray:self.articleHotMutableArr];
//            }
//        }
//        [strongSelf sendRequestToGetCommentListWithReload:YES];
//    }];
}

#pragma mark - 获取评论列表
-(void)sendRequestToGetCommentListWithReload:(BOOL)rereload{
//    __weak typeof(self)weakSelf = self;
    
//    [[NetworkAdapter sharedAdapter] sendRequestToGetInfoWithCommentId:self.transferArticleModel._id reReload:rereload page:self.articleDetailTableView.currentPage actionBlock:^(BOOL isSuccessed, LiveFourumRootListModel *listModel) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if(isSuccessed){
//            if (strongSelf.articleDetailTableView.isXiaLa || rereload){
//                [strongSelf.articleNormalMutableArr removeAllObjects];
//                [strongSelf.articleNormalMutableArr addObject:@[@"全部回答"]];
//            }
//            // 添加数据
//            [strongSelf.articleNormalMutableArr addObjectsFromArray:listModel.content];
//
//            // 清理数据
//            [strongSelf.articleDetailRootMutableArr removeAllObjects];
//            [strongSelf.articleDetailRootMutableArr addObject:strongSelf.infoArr];
//            [strongSelf.articleDetailRootMutableArr addObjectsFromArray:strongSelf.articleHotMutableArr];
//
//
//            for (int i = 0; i <strongSelf.articleNormalMutableArr.count;i++){
//                id info = [strongSelf.articleNormalMutableArr objectAtIndex:i];
//                if ([info isKindOfClass:[LiveFourumRootListSingleModel class]]){
//                    LiveFourumRootListSingleModel *singleModel = (LiveFourumRootListSingleModel *)info;
//                    if (singleModel.childCount > singleModel.child.count){          // 表示有多余的内容
//                        LiveFourumRootListSingleModel *childModel = [[LiveFourumRootListSingleModel alloc]init];
//                        childModel.reply_comment_name = @"共同NNNNN条回复";
//                        childModel.childCount = singleModel.childCount;
//                        NSMutableArray *childRootArr = [NSMutableArray array];
//                        [childRootArr addObjectsFromArray:singleModel.child];
//                        [childRootArr addObject:childModel];
//                        singleModel.child = [childRootArr copy];
//                    }
//                }
//            }
//
//            if (strongSelf.articleNormalMutableArr.count == 1 && [strongSelf.articleNormalMutableArr containsObject:@[@"全部回答"]]){
//                if (![strongSelf.articleNormalMutableArr containsObject:@[@"空"]]){
//                    [strongSelf.articleNormalMutableArr addObject:@[@"空"]];
//                }
//            } else {
//            }
//
//            // 加入到数组
//            [strongSelf.articleDetailRootMutableArr addObjectsFromArray:strongSelf.articleNormalMutableArr];
//
//            if (listModel.content.count){
//                [strongSelf.articleDetailTableView reloadData];
//            }
//
//            [strongSelf.articleDetailTableView stopPullToRefresh];
//            [strongSelf.articleDetailTableView stopFinishScrollingRefresh];
//
//        }
//    }];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.inputView releaseKeyboard];
}



//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    // 判断webView所在的cell是否可见，如果可见就layout
//    NSArray *cells = self.articleDetailTableView.visibleCells;
//    for (UITableViewCell *cell in cells) {
//        if ([cell isKindOfClass:[ArticleDetailInfoTableViewCell class]]) {
//            ArticleDetailInfoTableViewCell *webCell = (ArticleDetailInfoTableViewCell *)cell;
//            [webCell.webView setNeedsLayout];
//        }
//    }
//}

#pragma mark - 分享方法
-(void)shareManager{
//    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
//    shareViewController.hasJubao = YES;
//    __weak typeof(self)weakSelf = self;
//    [shareViewController actionClickWithShareBlock:^(NSString *type) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//
//        thirdLoginType shareType;
//        if ([type isEqualToString:@"wechat"]){
//            shareType = thirdLoginTypeWechat;
//        } else if ([type isEqualToString:@"friendsCircle"]){
//            shareType = thirdLoginTypeWechatFirend;
//        } else {
//            shareType = thirdLoginTypeWechat;
//        }
//        id smartImgUrl;
//        if (strongSelf.transferArticleModel.picture.count){
//            smartImgUrl = [strongSelf.transferArticleModel.picture firstObject];
//        } else {
//            smartImgUrl = strongSelf.transferArticleModel.head_img;
//        }
//
//        id mainImgUrl;
//        if ([smartImgUrl isKindOfClass:[NSString class]]){
//            NSString *urlStr = (NSString *)smartImgUrl;
//            NSString *baseURL = [PDImageView getUploadBucket:urlStr];
//            NSString *nUrl = [[NSString stringWithFormat:@"%@%@",baseURL,urlStr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//            mainImgUrl = nUrl;
//        } else if ([smartImgUrl isKindOfClass:[UIImage class]]){
//            mainImgUrl = smartImgUrl;
//        }
//
//        NSString *title = @"";
//        NSString *content = @"";
//
//
//        if (strongSelf.transferArticleModel.article_type == article_typeWeb){
//            title = strongSelf.transferArticleModel.title;
//            if (strongSelf.transferArticleModel.subtitle.length && ![strongSelf.transferArticleModel.subtitle isEqualToString:@"null"]){
//                content = strongSelf.transferArticleModel.subtitle;
//            } else {
//                content = @"最新最热区块链内容，尽在币本社区";
//            }
//        } else if (strongSelf.transferArticleModel.article_type == article_typeNormal){
//            title = strongSelf.transferArticleModel.title.length?strongSelf.transferArticleModel.title:strongSelf.transferArticleModel.content;
//            content = @"最新最热区块链内容，尽在币本社区";
//        }
//        [ShareSDKManager shareManagerWithType:shareType title:title desc:content img:mainImgUrl url:strongSelf.transferArticleModel.article_url callBack:NULL];
//        [ShareSDKManager shareSuccessBack:shareTypeArticle block:NULL];
//    }];
//
//    [shareViewController actionClickWithJubaoBlock:^{
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        ReportViewController *reportVC = [[ReportViewController alloc]init];
//        [reportVC showInView:strongSelf.parentViewController type:BY_THEME_TYPE_ARTICLE itemId:strongSelf.transferArticleModel._id];
//    }];
//    [shareViewController showInView:self.parentViewController];
}

#pragma mark - 关注
-(void)linkWIthCell:(ArticleDetailAvatarTableViewCell *)cell hasLink:(BOOL)link{
//    NSString *userId = cell.transferArticleModel.author_id;
//    __weak typeof(self)weakSelf = self;
//    [[NetworkAdapter sharedAdapter]centerSendRequestToLinkManagerWithUserId:userId hasLink:link block:^(BOOL isSuccessed) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSuccessed){
//            if (link){
//                [AccountModel sharedAccountModel].loginServerModel.infoCount.attention += 1;
//            } else {
//                [AccountModel sharedAccountModel].loginServerModel.infoCount.attention -= 1;
//            }
//
//            [cell btnStatusSelected:link];
//        }
//
//        void(^block)(BOOL hasLink) = objc_getAssociatedObject(strongSelf, &actionReoloadFollowKey);
//        if (block){
//            block(link);
//        }
//    }];
}

-(void)actionReoloadFollow:(void(^)(BOOL link))block{
//    objc_setAssociatedObject(self, &actionReoloadFollowKey, block, OBJC_ASSOCIATION_COPY);
}


#pragma mark - 根据id获取j文章
-(void)sendRequestToGetArticleBlock:(void(^)(ArticleRootSingleModel *model))block{
//    NSString *articleId = @"";
//    if (self.transferPageType == ArticleDetailRootViewControllerTypeLive){
//        articleId = [AccountModel sharedAccountModel].serverModel.config.guide_publish_live;
//    } else if (self.transferPageType == ArticleDetailRootViewControllerTypeArticle){
//        articleId = [AccountModel sharedAccountModel].serverModel.config.guide_publish_long_article;
//    } else if (self.transferPageType == ArticleDetailRootViewControllerTypeBanner){
//        articleId = self.transferArticleId;
//    }
//    NSDictionary *params = @{@"article_id":articleId};
//    __weak typeof(self)weakSelf = self;
//    [[NetworkAdapter sharedAdapter] fetchWithPath:article_get_article requestParams:params responseObjectClass:[ArticleRootSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSucceeded){
//            ArticleRootSingleModel *model = (ArticleRootSingleModel *)responseObject;
//            strongSelf.transferArticleModel = model;
//            if (block){
//                block(model);
//            }
//        }
//    }];
}


@end
