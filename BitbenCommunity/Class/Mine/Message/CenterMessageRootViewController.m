//
//  CenterMessageRootViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/24.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterMessageRootViewController.h"
#import "CenterMessageSingleModel.h"
#import "CenterMessageTableViewCell.h"
#import "NetworkAdapter+Center.h"
#import "CenterMessageSiteMsgTableViewCell.h"
@interface CenterMessageRootViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *messageTableView;
@property (nonatomic,strong)NSMutableArray *messageMutableArr;
@end

@implementation CenterMessageRootViewController

+(instancetype)sharedController{
    static CenterMessageRootViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[CenterMessageRootViewController alloc] init];
    });
    return _sharedAccountModel;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self centerGetMessageInfoWithReload:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"我的消息";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"清除" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf centerCleanMyMessageManager];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.messageMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.messageTableView){
        self.messageTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.messageTableView.size_height -= LCFloat(10);
        self.messageTableView.dataSource = self;
        self.messageTableView.delegate = self;
        [self.view addSubview:self.messageTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.messageTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf centerGetMessageInfoWithReload:YES];
    }];
    [self.messageTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf centerGetMessageInfoWithReload:NO];
    }];
}

#pragma mark - UITableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.messageMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    CenterMessageSiteMsgTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[CenterMessageSiteMsgTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferMessageModel = [self.messageMutableArr objectAtIndex:indexPath.row];
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CenterMessageSingleModel *transferMessageModel = [self.messageMutableArr objectAtIndex:indexPath.row];
    if ([transferMessageModel.type_code isEqualToString:@"401"]){              // 站内信
        if ([transferMessageModel.type isEqualToString:@"0"]){              // main
            if ([transferMessageModel.theme_type isEqualToString:@"0"]){    // 跳转直播
                
            } else if ([transferMessageModel.theme_type isEqualToString:@"1"]){     // 1.跳转文章
                ArticleDetailRootViewController *articleVC = [[ArticleDetailRootViewController alloc]init];
                articleVC.transferPageType = ArticleDetailRootViewControllerTypeBanner;
                articleVC.transferArticleId = transferMessageModel.theme_id;
                [self.navigationController pushViewController:articleVC animated:YES];
            } else if ([transferMessageModel.theme_type isEqualToString:@"2"]){
                PDWebViewController *webViewController = [[PDWebViewController alloc]init];
                [webViewController webDirectedWebUrl:transferMessageModel.theme_id];
                [self.navigationController pushViewController:webViewController animated:YES];
            }
        }
    } else if ([transferMessageModel.type_code isEqualToString:@"1005"] || [transferMessageModel.type_code isEqualToString:@"1006"]){
        if ([transferMessageModel.type isEqualToString:@"1"]){
            if ([transferMessageModel.link_status isEqualToString:@"0"]){           // 不能跳转
                [StatusBarManager statusBarHidenWithText:@"当前不可跳转，消息已过期。"];
            } else if ([transferMessageModel.link_status isEqualToString:@"1"]){    // 跳转到修改密码
                [[UIAlertView alertViewWithTitle:@"忘记密码" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
            }
        }
    }
    [self centerSignHasReadWithModel:@[transferMessageModel]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CenterMessageSiteMsgTableViewCell calculationCellHeight:[self.messageMutableArr objectAtIndex:indexPath.row]];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.messageTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.messageMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.messageMutableArr count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

#pragma mark - interface
-(void)centerGetMessageInfoWithReload:(BOOL)reload{
    __weak typeof(self)weakSelf = self;
    MessageType type;
    if (self.transferType == CenterMessageRootViewControllerTypeMain){
        type = MessageTypeRoot;
    } else {
        type = MessageTypeBlock;
    }
    [[NetworkAdapter sharedAdapter] fetchMyMessageWithInfoWithPageNum:self.messageTableView.currentPage type:type block:^(CenterMessageListModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.messageTableView.isXiaLa || reload){
            [strongSelf.messageMutableArr removeAllObjects];
        }
        if (listModel.content.count){
            [strongSelf.messageMutableArr addObjectsFromArray:listModel.content];
        }
        
        // 审核
        if ([AccountModel sharedAccountModel].isShenhe){
            [strongSelf.messageMutableArr removeAllObjects];
            for (int i = 0 ; i < 2;i++){
                CenterMessageSingleModel *singleModel = [[CenterMessageSingleModel alloc]init];
                if (i == 0){
                    singleModel.create_time = 1542368328820;
                    singleModel.content = @"欢迎加入币本";
                } else if (i == 1){
                    singleModel.create_time = 1542368328820;
                    singleModel.content = @"币本赠送给你20元红包";
                }
                [strongSelf.messageMutableArr addObject:singleModel];
            }
        }
        
        // 全部标记已读
        if (listModel.content.count){
            NSMutableArray  *infoArr = [NSMutableArray array];
            for (int i = 0 ; i < listModel.content.count;i++){
                CenterMessageSingleModel *messageSingleModel = [listModel.content objectAtIndex:i];
                if (![messageSingleModel.type_code isEqualToString:@"401"]){
                    [infoArr addObject:messageSingleModel];
                }
            }
            
            [strongSelf centerSignHasReadWithModel:infoArr];
        } else {
        
            
            if (strongSelf.messageTableView.isXiaLa){
                [strongSelf.messageTableView stopPullToRefresh];
            } else {
                [strongSelf.messageTableView stopFinishScrollingRefresh];
            }

            
            [strongSelf.messageTableView reloadData];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if(strongSelf.messageMutableArr.count){
                    [strongSelf.messageTableView dismissPrompt];
                } else {
                    [strongSelf.messageTableView showPrompt:@"当前没有消息数据哦" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
                }
            });
        }
    }];
}

#pragma mark - 清除我的消息
-(void)centerCleanMyMessageManager{
    __weak typeof(self)weakSelf = self;
//    [[NetworkAdapter sharedAdapter] centerCleanMyMessagesWithBlock:^(BOOL isSuccessed) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSuccessed){
//            [strongSelf.messageMutableArr removeAllObjects];
//        }
//        [strongSelf.messageTableView reloadData];
//        if (strongSelf.messageMutableArr.count){
//            [strongSelf.messageTableView dismissPrompt];
//        } else {
//            [strongSelf.messageTableView showPrompt:@"当前没有消息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
//        }
//    }];
}

#pragma mark - 全部标记已读
-(void)centerSignHasReadWithModel:(NSArray *)infoArr{
    NSMutableArray *infoIdArr = [NSMutableArray array];
    for (int i = 0 ; i < infoArr.count;i++){
        CenterMessageSingleModel *singleModel = [infoArr objectAtIndex:i];
        [infoIdArr addObject:singleModel._id];
    }
    
    __weak typeof(self)weakSelf = self;
    if (infoArr.count){
        [[NetworkAdapter sharedAdapter] centerMessageReadList:infoIdArr block:^(BOOL isSuccessed) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (isSuccessed){
                if (infoArr.count){
                    for (int i = 0 ;i < infoArr.count;i++){
                        CenterMessageSingleModel *singleModel = [infoArr objectAtIndex:i];
                        singleModel.is_read = YES;
                    }
                }
            }
            
            [strongSelf.messageTableView reloadData];
            
            if (strongSelf.messageTableView.isXiaLa){
                [strongSelf.messageTableView stopPullToRefresh];
            } else {
                [strongSelf.messageTableView stopFinishScrollingRefresh];
            }

        }];
    }
}

@end
