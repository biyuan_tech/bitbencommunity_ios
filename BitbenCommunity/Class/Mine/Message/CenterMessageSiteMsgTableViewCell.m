//
//  CenterMessageSiteMsgTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/12.
//  Copyright © 2018 币本. All rights reserved.
//

#import "CenterMessageSiteMsgTableViewCell.h"

@interface CenterMessageSiteMsgTableViewCell()
@property (nonatomic,strong)PDImageView *messageIcon;
@property (nonatomic,strong)UILabel *messageLabel;
@property (nonatomic,strong)PDImageView *nImgView;
@property (nonatomic,strong)UILabel *nLabel;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *ablumImgView;
@property (nonatomic,strong)UILabel *contentLabel;

@end

@implementation CenterMessageSiteMsgTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.messageIcon = [[PDImageView alloc]init];
    self.messageIcon.backgroundColor = [UIColor clearColor];
    [self addSubview:self.messageIcon];
    
    self.messageLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    [self addSubview:self.messageLabel];
    
    self.nImgView = [[PDImageView alloc]init];
    self.nImgView.image = [UIImage imageNamed:@"icon_message_new"];
    self.nImgView.backgroundColor = [UIColor clearColor];
    self.nImgView.frame = CGRectMake(0, 0, LCFloat(33), LCFloat(17));
    [self addSubview:self.nImgView];
    
    self.nLabel = [GWViewTool createLabelFont:@"13" textColor:@"969696"];
    [self addSubview:self.nLabel];
    
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"313131"];
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];

    self.contentLabel = [GWViewTool createLabelFont:@"13" textColor:@"696969"];
    [self addSubview:self.contentLabel];
    
    self.ablumImgView = [[PDImageView alloc]init];
    self.ablumImgView.backgroundColor = [UIColor clearColor];
    _ablumImgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(160));
    [self addSubview:self.ablumImgView];
}

-(void)setTransferMessageModel:(CenterMessageSingleModel *)transferMessageModel{
    _transferMessageModel = transferMessageModel;
    // 图标
    self.messageIcon.frame = CGRectMake(LCFloat(11), LCFloat(11), LCFloat(15), LCFloat(15));
    self.messageIcon.image = [UIImage imageNamed:@"icon_message_sounds"];
    //
    self.messageLabel.text = transferMessageModel.type_content;
        
    CGSize messageSize = [Tool makeSizeWithLabel:self.messageLabel];
    self.messageLabel.frame = CGRectMake(CGRectGetMaxX(self.messageIcon.frame) + LCFloat(6), 0, messageSize.width, messageSize.height);
    self.messageLabel.center_y = self.messageIcon.center_y;
    
    // time
    self.nLabel.text = [NSDate getTimeWithString:transferMessageModel.create_time / 1000.];
    CGSize timeSize = [Tool makeSizeWithLabel:self.nLabel];
    self.nLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - timeSize.width, 0, timeSize.width, timeSize.height);
    self.nLabel.center_y = self.messageLabel.center_y;
    
    self.nImgView.frame = CGRectMake(self.nLabel.orgin_x - LCFloat(14) - LCFloat(33), 0, LCFloat(33), LCFloat(17));
    self.nImgView.center_y = self.nLabel.center_y;
    
    if (transferMessageModel.is_read){
        self.nImgView.hidden = YES;
    } else {
        self.nImgView.hidden = NO;
    }
    
    if ([transferMessageModel.type_code isEqualToString:@"401"]){     // 站内信
        self.ablumImgView.hidden = NO;
        self.contentLabel.hidden = NO;
        [self.ablumImgView uploadImageWithURL:transferMessageModel.picture placeholder:nil callback:NULL];
        self.ablumImgView.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.nImgView.frame) + LCFloat(13),kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(160));
        
        self.titleLabel.text = transferMessageModel.title;
        self.titleLabel.numberOfLines = 1;
        self.titleLabel.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.ablumImgView.frame) + LCFloat(15), kScreenBounds.size.width - 2* LCFloat(15), [NSString contentofHeightWithFont:self.titleLabel.font]);
        
        // content
        NSString *mainContentStr = @"";
        if (transferMessageModel.content.length > 40){
            NSString *infoStr = [transferMessageModel.content substringToIndex:40];
            mainContentStr = [NSString stringWithFormat:@"%@... 查看详情 >",infoStr];
        } else {
            mainContentStr = [NSString stringWithFormat:@"%@ 查看详情 >",transferMessageModel.content];
        }
        
        CGSize contentOfSize = [mainContentStr sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(15), CGFLOAT_MAX)];
        self.contentLabel.attributedText = [Tool rangeLabelWithContent:mainContentStr hltContentArr:@[@"查看详情 >"] hltColor:[UIColor hexChangeFloat:@"5A7FAC"] normolColor:[UIColor hexChangeFloat:@"696969"]];
        if (contentOfSize.height >= 2 * [NSString contentofHeightWithFont:self.contentLabel.font]){
            self.contentLabel.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(12), kScreenBounds.size.width - 2 * LCFloat(15), 2 * [NSString contentofHeightWithFont:self.contentLabel.font]);
            self.contentLabel.numberOfLines = 2;
        } else {
            self.contentLabel.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(12), kScreenBounds.size.width - 2 * LCFloat(15), 1 * [NSString contentofHeightWithFont:self.contentLabel.font]);
            self.contentLabel.numberOfLines = 1;
        }
        self.titleLabel.font = [[UIFont fontWithCustomerSizeName:@"14"] boldFont];
    } else {
        self.ablumImgView.hidden = YES;
        self.contentLabel.hidden = YES;
        
        self.titleLabel.text = transferMessageModel.content;
        self.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
        CGSize messageInfoSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(15), CGFLOAT_MAX)];
        self.titleLabel.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.messageIcon.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(15), messageInfoSize.height);
    }
}

+(CGFloat)calculationCellHeight:(CenterMessageSingleModel *)transferMessageModel{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(20);
    cellHeight += LCFloat(11);
    
    if ([transferMessageModel.type_code isEqualToString:@"401"]){
        cellHeight += LCFloat(160);
        cellHeight += LCFloat(13);
        cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]];
        cellHeight += LCFloat(12);
    }

    if (transferMessageModel.content.length){
        // content
        NSString *mainContentStr = @"";
        if (transferMessageModel.content.length > 40){
            NSString *infoStr = [transferMessageModel.content substringToIndex:40];
            mainContentStr = [NSString stringWithFormat:@"%@... 查看详情 >",infoStr];
        } else {
            mainContentStr = [NSString stringWithFormat:@"%@ 查看详情 >",transferMessageModel.content];
        }
        
        CGSize contentOfSize = [mainContentStr sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"13"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(15), CGFLOAT_MAX)];
        if (contentOfSize.height >= 2 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]]){
            cellHeight += 2 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]];
        } else {
            cellHeight += 1 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]];
        }
    }
    cellHeight += LCFloat(11);

    return cellHeight;
}

@end
