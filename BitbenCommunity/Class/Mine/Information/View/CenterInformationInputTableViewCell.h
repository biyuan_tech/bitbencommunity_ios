//
//  CenterInformationInputTableViewCell.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/28.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

typedef NS_ENUM(NSInteger,InformationInputType) {
    InformationInputTypeNick,
    InformationInputTypeGender,
    InformationInputTypeNormal,
    InformationInputTypeSign,
};

@interface CenterInformationInputTableViewCell : PDBaseTableViewCell

@property (nonatomic,assign)BOOL hasArrow;
@property (nonatomic,copy)NSString *transferTitle;
@property (nonatomic,copy)NSString *transferPlaceholder;
@property (nonatomic,copy)NSString *transferDymic;
@property (nonatomic,copy)NSString *transferBgImgName;
@property (nonatomic,strong)UITextField *inputTextField;

-(void)textFieldDidChangeBlock:(void(^)(NSString *info))block;

-(void)textFieldDidEndBlock:(void(^)(NSString *info))block;

@property (nonatomic,assign)InformationInputType transferType;

+(CGFloat)calculationCellHeightWithType:(InformationInputType)type;

@end
