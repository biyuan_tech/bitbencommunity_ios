//
//  CenterInformationLikeTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/29.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterInformationLikeTableViewCell.h"

@interface CenterInformationLikeTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *bgView;

@end

static char actionClickInformationWithSingleItemKey;
@implementation CenterInformationLikeTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"B1B1B1"];
    self.titleLabel.text = @"喜欢的话题";
    [self addSubview:self.titleLabel];
    self.titleLabel.frame = CGRectMake(LCFloat(30), 0, kScreenBounds.size.width - 2 * LCFloat(30), [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgView];
}

-(void)setTransferLikeArr:(NSArray<LoginServerModelAccountTopic> *)transferLikeArr{
    _transferLikeArr = transferLikeArr;
    self.titleLabel.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    if (self.bgView.subviews.count){
        [self.bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    CGFloat margin = LCFloat(9);
    CGFloat margin_h = LCFloat(0);
    CGFloat width = (kScreenBounds.size.width - 4 * margin) / 3.;
    CGFloat height = LCFloat(62);
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i < transferLikeArr.count;i++){
        LoginServerModelAccountTopic *model = [transferLikeArr objectAtIndex:i];
        
        CGFloat origin_x = margin + ( i % 3 ) *(width + margin);
        CGFloat origin_y = (i / 3) * (margin_h + height);
        
        CenterInformationLikeItemsView *singleItemView = [[CenterInformationLikeItemsView alloc]initWithFrame:CGRectMake(origin_x, origin_y, width, height) model:model actionBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(LoginServerModelAccountTopic *itemModel) = objc_getAssociatedObject(strongSelf, &actionClickInformationWithSingleItemKey);
            if(block){
                block(model);
            }
        }];
        [self.bgView addSubview:singleItemView];
    }
    self.bgView.userInteractionEnabled = YES;
    CGFloat bgView_height =  (margin_h + height) * (transferLikeArr.count % 3 == 0 ? transferLikeArr.count / 3 : transferLikeArr.count / 3 + 1);
    self.bgView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(9), kScreenBounds.size.width, bgView_height);
}

-(void)actionClickInformationWithSingleItem:(void(^)(LoginServerModelAccountTopic *singleModel))block{
    objc_setAssociatedObject(self, &actionClickInformationWithSingleItemKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeightWithArr:(NSArray *)likeArr{
    CGFloat cellHeight = 0;
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]];
    cellHeight += LCFloat(9);
    
    CGFloat margin = LCFloat(0);
    CGFloat height = LCFloat(62);
    
    CGFloat bgView_height =  (margin + height) * (likeArr.count % 3 == 0 ? likeArr.count / 3 : likeArr.count / 3 + 1);
    cellHeight += bgView_height;
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end



@interface CenterInformationLikeItemsView()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *actionButton;

@end

@implementation CenterInformationLikeItemsView

-(instancetype)initWithFrame:(CGRect)frame model:(LoginServerModelAccountTopic *)model actionBlock:(void(^)())block{
    self = [super initWithFrame:frame];
    if (self){
        self.userInteractionEnabled = YES;
        [self createViewModel:model actionBlock:block];
    }
    return self;
}

#pragma mark - createView
-(void)createViewModel:(LoginServerModelAccountTopic *)model actionBlock:(void(^)())block{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.frame = self.bounds;
    [self addSubview:self.bgImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"白"];
    self.titleLabel.text = model.content;
    
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.frame = CGRectMake(0, 0, self.size_width - LCFloat(21) /2. - LCFloat(38) / 2. - 1 * LCFloat(7), self.size_height - LCFloat(41) / 2. - LCFloat(18) / 2. - 1 * LCFloat(3));
    CGFloat center_x = (self.size_width - (LCFloat(38) /2. - LCFloat(21) /2.) / 2.) / 2. - LCFloat(1);
    CGFloat center_y = (self.size_height - (LCFloat(41) /2. - LCFloat(18) /2.) / 2.) / 2. - LCFloat(2);
    CGPoint center = CGPointMake(center_x, center_y);
    self.titleLabel.center = center;
    
    [self.bgImgView addSubview:self.titleLabel];
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.actionButton];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    self.actionButton.frame = self.bounds;
    
    if (model.isLike){
        self.bgImgView.image = [Tool stretchImageWithName:@"bg_center_like_hlt"];
        self.titleLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    } else {
        self.bgImgView.image = [Tool stretchImageWithName:@"bg_center_like_nor"];
        self.titleLabel.textColor = [UIColor colorWithCustomerName:@"B4B4B4"];
    }
}

@end
