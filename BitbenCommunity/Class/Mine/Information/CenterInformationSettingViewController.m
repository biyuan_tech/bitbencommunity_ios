//
//  CenterInformationSettingViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterInformationSettingViewController.h"
#import "CenterInformationInputTableViewCell.h"
#import "CenterInformationAvatarTableViewCell.h"
#import "CenterInformationLikeTableViewCell.h"              // 喜欢的话题
#import "GWAssetsLibraryViewController.h"
#import "NetworkAdapter+Login.h"
#import "NetworkAdapter+Center.h"
#import "InformationChooseShowController.h"
#import "AreaAnalysis.h"
#import "CenterInformationGenderTableViewCell.h"

@interface CenterInformationSettingViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITextField *nickTextField;
    UITextField *zhiyeTextField;
    UITextField *SignTextField;
}
@property (nonatomic,strong)UITableView *settingTableView;
@property (nonatomic,strong)NSArray *settingArr;

@end

@implementation CenterInformationSettingViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetMineAllInfo];
    [self createNotifi];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"修改信息";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    if ([AccountModel sharedAccountModel].loginServerModel.account.topic_list.count){
        self.settingArr = @[@[@"头像"],@[@"昵称"],@[@"性别",@"生日",@"城市",@"职业",@"签名"],@[@"喜欢的话题"]];
    } else {
        self.settingArr = @[@[@"头像"],@[@"昵称"],@[@"性别",@"生日",@"城市",@"职业",@"签名"]];
    }
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.settingTableView){
        self.settingTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.settingTableView.dataSource = self;
        self.settingTableView.delegate = self;
        [self.view addSubview:self.settingTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.settingArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.settingArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.settingArr]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        CenterInformationAvatarTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[CenterInformationAvatarTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
        }
        cellWithRowZero.transferAccountModel = [AccountModel sharedAccountModel];
        __weak typeof(self)weakSelf = self;
        [cellWithRowZero avatarButtonClickWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf choosePhotoAndUploadManager];
        }];
        return cellWithRowZero;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"喜欢的话题" sourceArr:self.settingArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        CenterInformationLikeTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[CenterInformationLikeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferLikeArr = [AccountModel sharedAccountModel].loginServerModel.account.topic_list;
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr actionClickInformationWithSingleItem:^(LoginServerModelAccountTopic *singleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            singleModel.isLike = !singleModel.isLike;
            [strongSelf sendRequestToChangeMyLike:singleModel];
        }];
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"性别" sourceArr:self.settingArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"性别" sourceArr:self.settingArr]){
        static NSString *cellIdentifyWithRowGender = @"cellIdentifyWithRowGender";
        CenterInformationGenderTableViewCell *cellWithGender = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowGender];
        if (!cellWithGender){
            cellWithGender = [[CenterInformationGenderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowGender];
            cellWithGender.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithGender.transferChooseGender = [AccountModel sharedAccountModel].loginServerModel.account.sex;
        __weak typeof(self)weakSelf = self;
        [cellWithGender actionSelectedGenderWithBlock:^(NSString *gender) {
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToUploadGender:gender];
        }];
        return cellWithGender;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        CenterInformationInputTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[CenterInformationInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferType = InformationInputTypeNormal;
        __weak typeof(self)weakSelf = self;
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"昵称" sourceArr:self.settingArr]){
            cellWithRowOne.transferTitle = @"昵称";
            cellWithRowOne.transferBgImgName = @"center_user_edit_single";
            cellWithRowOne.transferPlaceholder = @"请输入你的昵称";
            cellWithRowOne.transferDymic = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
            cellWithRowOne.transferType = InformationInputTypeNick;
            nickTextField = cellWithRowOne.inputTextField;
            [cellWithRowOne textFieldDidEndBlock:^(NSString *info) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (info.length >= 4 && info.length < 10){
                    [strongSelf sendRequestToUploadNickName:info];
                } else {
                    [[UIAlertView alertViewWithTitle:@"提示" message:@"昵称由4-10位汉字/字母/符号组成，首位不能是符号" buttonTitles:@[@"确定"] callBlock:NULL]show];
                }
            }];
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"性别" sourceArr:self.settingArr]){
            if (indexPath.row == [self cellIndexPathRowWithcellData:@"性别" sourceArr:self.settingArr]){
                cellWithRowOne.transferTitle = @"性别";
                cellWithRowOne.transferBgImgName = @"bg_center_top";
            } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"生日" sourceArr:self.settingArr]){
                cellWithRowOne.transferTitle = @"生日";
                cellWithRowOne.transferPlaceholder = @"请选择您的生日";
                cellWithRowOne.transferBgImgName = @"bg_center_normal";
                cellWithRowOne.inputTextField.userInteractionEnabled = NO;
                cellWithRowOne.transferDymic = [NSString stringWithFormat:@"%@",[AccountModel sharedAccountModel].loginServerModel.account.birthday.length?[AccountModel sharedAccountModel].loginServerModel.account.birthday:@""];
            } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"城市" sourceArr:self.settingArr]){
                cellWithRowOne.transferTitle = @"城市";
                cellWithRowOne.transferPlaceholder = @"请选择您的城市";
                cellWithRowOne.transferBgImgName = @"bg_center_normal";
                cellWithRowOne.inputTextField.userInteractionEnabled = NO;
                NSString *area = @"";
                if ([AccountModel sharedAccountModel].loginServerModel.account.province.length){
                    area = [area stringByAppendingString:[AccountModel sharedAccountModel].loginServerModel.account.province];
                }
                if ([AccountModel sharedAccountModel].loginServerModel.account.city.length){
                    area = [area stringByAppendingString:[AccountModel sharedAccountModel].loginServerModel.account.city];
                }
                cellWithRowOne.transferDymic = [NSString stringWithFormat:@"%@",area];
            } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"职业" sourceArr:self.settingArr]){
                cellWithRowOne.transferTitle = @"职业";
                 cellWithRowOne.transferBgImgName = @"bg_center_normal";
                cellWithRowOne.inputTextField.userInteractionEnabled = YES;
                zhiyeTextField = cellWithRowOne.inputTextField;
                cellWithRowOne.transferPlaceholder = @"请输入你的职业";
                cellWithRowOne.transferDymic = [AccountModel sharedAccountModel].loginServerModel.account.profession;
                [cellWithRowOne textFieldDidEndBlock:^(NSString *info) {
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [strongSelf sendRequestToUploadProfession:info];
                }];
                
            } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"签名" sourceArr:self.settingArr]){
                cellWithRowOne.transferTitle = @"签名";
                 cellWithRowOne.transferBgImgName = @"bg_center_bottom";
                cellWithRowOne.inputTextField.userInteractionEnabled = YES;
                cellWithRowOne.transferPlaceholder = @"请输入你的签名";
                SignTextField = cellWithRowOne.inputTextField;
                cellWithRowOne.transferDymic = [AccountModel sharedAccountModel].loginServerModel.user.self_introduction;
                [cellWithRowOne textFieldDidEndBlock:^(NSString *info) {
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [strongSelf sendRequestToChangeSign:info];
                }];
                
            }
        }
        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.settingArr]){
        return [CenterInformationAvatarTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"喜欢的话题" sourceArr:self.settingArr]){
        return [CenterInformationLikeTableViewCell calculationCellHeightWithArr:[AccountModel sharedAccountModel].loginServerModel.account.topic_list];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"昵称" sourceArr:self.settingArr] &&indexPath.row == [self cellIndexPathRowWithcellData:@"昵称" sourceArr:self.settingArr]){
        return [CenterInformationInputTableViewCell calculationCellHeightWithType:InformationInputTypeNick];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"性别" sourceArr:self.settingArr] &&indexPath.row == [self cellIndexPathRowWithcellData:@"性别" sourceArr:self.settingArr]){
        return [CenterInformationInputTableViewCell calculationCellHeightWithType:InformationInputTypeGender];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"签名" sourceArr:self.settingArr] &&indexPath.row == [self cellIndexPathRowWithcellData:@"签名" sourceArr:self.settingArr]){
        return [CenterInformationInputTableViewCell calculationCellHeightWithType:InformationInputTypeSign];
    } else {
        return [CenterInformationInputTableViewCell calculationCellHeightWithType:InformationInputTypeNormal];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 1){
        UIView *footerView = [[UIView alloc]init];
        PDImageView *waringIcon = [[PDImageView alloc]init];
        waringIcon.image = [UIImage imageNamed:@"icon_center_info_waring"];
        waringIcon.frame = CGRectMake(LCFloat(30), 0, LCFloat(14), LCFloat(14));
        [footerView addSubview:waringIcon];
        
        UILabel *titleLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
        titleLabel.text = @"昵称由4-10位汉字/字母/符号组成，首位不能是符号";
        [footerView addSubview:titleLabel];
        titleLabel.frame = CGRectMake(CGRectGetMaxX(waringIcon.frame) + LCFloat(3), 0, kScreenBounds.size.width - 2 * CGRectGetMaxX(waringIcon.frame) - 2 * LCFloat(3), [NSString contentofHeightWithFont:titleLabel.font]);
        return footerView;
    } else {
        return nil;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 1){
        CGFloat height = 0;
        height += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]];
        return height;
    } else {
        return 0;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.settingTableView) {
        NSArray *sectionOfArr = [self.settingArr objectAtIndex:indexPath.section];
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"性别" sourceArr:self.settingArr]){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType  = SeparatorTypeHead;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
            } else if ([indexPath row] == sectionOfArr.count - 1) {
                separatorType  = SeparatorTypeBottom;
            } else {
                separatorType  = SeparatorTypeMiddle;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
            }
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (nickTextField.isFirstResponder){
        [nickTextField resignFirstResponder];
    } else if (zhiyeTextField.isFirstResponder){
        [zhiyeTextField resignFirstResponder];
    } else if (SignTextField.isFirstResponder){
        [SignTextField resignFirstResponder];
    }
    
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"城市" sourceArr:self.settingArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"城市" sourceArr:self.settingArr]){
        [self sendRequestToChooseMyCity];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"生日" sourceArr:self.settingArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"生日" sourceArr:self.settingArr]){
        [self sendRequestToChooseBirthday];
    }
}


#pragma mark - 选择上传照片
-(void)choosePhotoAndUploadManager{
    GWAssetsLibraryViewController *assetVC = [[GWAssetsLibraryViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [assetVC selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        UIImage *image = [selectedImgArr lastObject];
        [strongSelf uploadAvatarWithImg:image];
    }];
    [self.navigationController pushViewController:assetVC animated:YES];
}

-(void)uploadAvatarWithImg:(UIImage *)avatar{
    OSSFileModel *ossFileModel = [[OSSFileModel alloc]init];
    ossFileModel.objcName = [NSString stringWithFormat:@"%@-%@",@"avatar",[NSDate getCurrentTimeWithFileName]];
    ossFileModel.objcImage = avatar;
    
    __weak typeof(self)weakSelf = self;
    [[OSSManager sharedUploadManager] uploadImageManagerWithImageList:[@[ossFileModel] copy] withUrlBlock:^(NSArray *imgUrlArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf interfaceManagerWithUploadAvarar:[imgUrlArr lastObject]];
    }];
}

-(void)interfaceManagerWithUploadAvarar:(NSString *)avatarUrl{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginWithUploadAvatar:avatarUrl block:^(BOOL isSuccessed, NSString *avararUrl) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [AccountModel sharedAccountModel].loginServerModel.account.head_img = avatarUrl;
            NSInteger section = [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.settingArr];
            NSInteger row = [self cellIndexPathRowWithcellData:@"头像" sourceArr:self.settingArr];
            [strongSelf.settingTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
}

#pragma mark - 获取我的所有信息
-(void)sendRequestToGetMineAllInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerGetMainUserInfoManagerBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf arrayWithInit];
        [strongSelf.settingTableView reloadData];
    }];
}

#pragma mark 修改昵称
-(void)sendRequestToUploadNickName:(NSString *)nickName{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"nickname":nickName};
    [[NetworkAdapter sharedAdapter] loginUpdateAccountInfoWithUserDic:params block:^{
        if (!weakSelf){
            return ;
        }
        [AccountModel sharedAccountModel].loginServerModel.user.nickname = nickName;
    }];
}


#pragma mark 修改职业
-(void)sendRequestToUploadProfession:(NSString *)profession{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"profession":profession};
    [[NetworkAdapter sharedAdapter] loginUpdateAccountInfoWithUserDic:params block:^{
        if (!weakSelf){
            return ;
        }
        [AccountModel sharedAccountModel].loginServerModel.account.profession = profession;
    }];
}


#pragma mark - 修改我的签名
-(void)sendRequestToChangeSign:(NSString *)sign{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"self_introduction":sign};
    [[NetworkAdapter sharedAdapter] loginUpdateAccountInfoWithUserDic:params block:^{
        if (!weakSelf){
            return ;
        }
        [AccountModel sharedAccountModel].loginServerModel.user.self_introduction = sign;
    }];
}

#pragma mark - 修改我的喜欢
-(void)sendRequestToChangeMyLike:(LoginServerModelAccountTopic *)singleModel{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerChangeMyLike:[AccountModel sharedAccountModel].loginServerModel.account.topic_list block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger section = [self cellIndexPathSectionWithcellData:@"喜欢的话题" sourceArr:self.settingArr];
        NSInteger row = [self cellIndexPathRowWithcellData:@"喜欢的话题" sourceArr:self.settingArr];
        [strongSelf.settingTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark - 修改我的城市
-(void)sendRequestToChooseMyCity{
    __weak typeof(self)weakSelf = self;
    InformationChooseShowController *chooseViewController = [[InformationChooseShowController alloc]init];
    chooseViewController.transferPageType = InfomationPageTypeCity;
    [chooseViewController actionCheckToChooseCity:^(NSString *provice, NSString *city) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToUploadProvice:provice city:city];
    }];
    
    [chooseViewController showInView:self.parentViewController];
}

#pragma mark 修改城市
-(void)sendRequestToUploadProvice:(NSString *)provice city:(NSString *)city{
    __weak typeof(self)weakSelf = self;
    if (!provice.length || !city.length){
        [StatusBarManager statusBarHidenWithText:@"请选择你的省或城市"];
        return;
    }
    
    NSDictionary *params = @{@"province":provice,@"city":city};
    [[NetworkAdapter sharedAdapter] loginUpdateAccountInfoWithUserDic:params block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [AccountModel sharedAccountModel].loginServerModel.account.province = provice;
        [AccountModel sharedAccountModel].loginServerModel.account.city = city;
        
        NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"城市" sourceArr:strongSelf.settingArr] ;
        NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"城市" sourceArr:strongSelf.settingArr];
        [strongSelf.settingTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark - 修改生日
-(void)sendRequestToChooseBirthday{
    __weak typeof(self)weakSelf = self;
    InformationChooseShowController *chooseViewController = [[InformationChooseShowController alloc]init];
    chooseViewController.transferPageType = InfomationPageTypeDate;
    [chooseViewController actionCheckToChooseDate:^(NSString *date) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToUploadBirthday:date];
    }];
    
    [chooseViewController showInView:self.parentViewController];
}

#pragma mark 修改城市
-(void)sendRequestToUploadBirthday:(NSString *)birthday{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"birthday":birthday};
    [[NetworkAdapter sharedAdapter] loginUpdateAccountInfoWithUserDic:params block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [AccountModel sharedAccountModel].loginServerModel.account.birthday =birthday;
        
        NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"生日" sourceArr:strongSelf.settingArr] ;
        NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"生日" sourceArr:strongSelf.settingArr];
        [strongSelf.settingTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark - 修改性别
-(void)sendRequestToUploadGender:(NSString *)gender{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"sex":gender};
    [[NetworkAdapter sharedAdapter] loginUpdateAccountInfoWithUserDic:params block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [AccountModel sharedAccountModel].loginServerModel.account.sex = gender;
        
        NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"性别" sourceArr:strongSelf.settingArr] ;
        NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"性别" sourceArr:strongSelf.settingArr];
        [strongSelf.settingTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:UITableViewRowAnimationNone];
    }];
}




#pragma mark - 键盘代理
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.settingTableView.frame = newTextViewFrame;
    [UIView commitAnimations];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.settingTableView.frame = self.view.bounds;
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

@end
