//
//  CenterInfomationViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterInfomationViewController.h"

@interface CenterInfomationViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *centerInformationTableView;
@property (nonatomic,strong)NSArray *infomationArr;
@end

@implementation CenterInfomationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"资料设置";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.infomationArr = @[@[@"头像"],@[@"基本资料",@"手机号码",@"登录密码"]];
}

#pragma mark - createTableView
-(void)createTableView{
    if (!self.centerInformationTableView){
        self.centerInformationTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.centerInformationTableView.dataSource = self;
        self.centerInformationTableView.delegate = self;
        [self.view addSubview:self.centerInformationTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.infomationArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.infomationArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"1");
}



@end
