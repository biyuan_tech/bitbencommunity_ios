//
//  NetworkAdapter+Article.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/10.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "NetworkAdapter.h"
#import "ArticleRootSingleModel.h"
#import "ArticleDetailHuatiListModel.h"
#import "LiveFourumOperateModel.h"
#import "ArticleRootRankModel.h"
#import "RankNormalModel.h"




typedef NS_ENUM(NSInteger ,ArticleListType) {
    ArticleListTypeTuijian = 1,             /**< 推荐*/
    ArticleListTypeHot = 2,                 /**< 最热*/
    ArticleListTypeNew = 0,                 /**< 最新*/
};

typedef NS_ENUM(NSInteger,ArticleType) {
    ArticleTypeShort,                   /**< 短文*/
    ArticleTypeLong,                   /**< 长文*/
};

typedef NS_ENUM(NSInteger,CollectionModelThemetype) {
    CollectionModelThemetype0,              /**< 直播*/
    CollectionModelThemetype1,              /**< 文章*/
};

@interface NetworkAdapter (Article)

#pragma mark 1.上传图片接口
-(void)articleAddWithImgList:(NSArray *)imgList articleType:(ArticleType)articleType topicArr:(NSArray *)topicArr title:(NSString *)title content:(NSString *)content subTitle:(NSString *)subTitle block:(void(^)())block;

-(void)articleAddWithImgList:(NSArray *)imgList articleType:(ArticleType)articleType topicArr:(NSArray *)topicArr title:(NSString *)title content:(NSString *)content subTitle:(NSString *)subTitle isAuthor:(BOOL)isAuthor block:(void(^)())block;

#pragma mark - 获取列表
-(void)articleListWithPage:(NSInteger)page topic:(NSString *)topic authorId:(NSString *)authorId block:(void(^)(NSArray <ArticleRootSingleModel>*articleList))block;
-(void)articleListWithPage:(NSInteger)page topic:(NSString *)topic type:(ArticleListType)type authorId:(NSString *)authorId block:(void(^)(NSArray <ArticleRootSingleModel>*articleList))block;
// 获取我关注的列表
-(void)articleListWithPage:(NSInteger)page block:(void(^)(NSArray <ArticleRootSingleModel>*articleList))block;

#pragma mark - 获取热门点评
-(void)articleGetHotCommentId:(NSString *)commentId actionBlock:(void(^)(BOOL isSuccessed,LiveFourumRootListModel *list))actionBlock;
#pragma mark - 添加评论
-(void)articleCreateCommentWiththemeId:(NSString *)themeId ThemeInfo:(LiveFourumRootListSingleModel *)themeinfo content:(NSString *)content block:(void(^)(BOOL isSuccessed,NSString *comment_id))block;
#pragma mark  - 获取评论列表
-(void)sendRequestToGetInfoWithCommentId:(NSString *)commentId reReload:(BOOL)rereload page:(NSInteger)page actionBlock:(void(^)(BOOL isSuccessed,LiveFourumRootListModel *list))actionBlock;
#pragma mark - 获取话题列表
-(void)sendRequestToGetHuatiListManagerBlock:(void(^)(BOOL isSuccessed, ArticleDetailHuatiListModel *listModel))block;
#pragma mark - 顶&踩
-(void)articleDingAndCai:(NSString *)commentId hasUp:(BOOL)up block:(void(^)(BOOL isSuccessed ,BOOL isOperate))block;
#pragma mark - 举报
-(void)jubaoManagerWithId:(NSString *)themeId type:(BY_THEME_TYPE)themeType jubaoInfo:(NSString *)jubaoInfo block:(void(^)())block;

-(void)articleListWithPage:(NSInteger)page topic:(NSString *)topic author_id:(NSString *)author_id status:(NSString *)status block:(void(^)(NSArray <ArticleRootSingleModel>*articleList))block;

#pragma mark - 取消定时发布
-(void)articleDraftCancelReleaseWithArticleId:(NSString *)articleId block:(void(^)())block;
#pragma mark - 立即发布
-(void)articleDraftReleaseNowWithArticleId:(NSString *)articleId block:(void(^)())block;
#pragma mark - 排行榜
-(void)articleRankInfoManagerSuccessBlock:(void(^)(ArticleRootRankListModel *rankListModel))block;
#pragma mark - 排行榜
-(void)rankRankListManagerPage:(NSInteger)page block:(void(^)(RankNormalModel *rankListModel))block;
#pragma mark - 添加快讯
-(void)addQuicklyInfoManagerWithTitle:(NSString *)title content:(NSString *)content img:(UIImage *)img block:(void(^)())block;
#pragma mark - 判断当前状态
-(void)autoGetUserTypeHasReleaseQuicklyInfoManagerblock:(void(^)(BOOL hasRelease,NSString *articleId))block;
#pragma mark - 收藏
-(void)collectionManager:(NSString *)theme_id themeType:(CollectionModelThemetype)type actionBlock:(void(^)(BOOL isSuccessed))block;
#pragma mark - 取消收藏
-(void)collectionCancelManager:(NSString *)theme_id themeType:(CollectionModelThemetype)type actionBlock:(void(^)(BOOL isSuccessed))block;
@end
