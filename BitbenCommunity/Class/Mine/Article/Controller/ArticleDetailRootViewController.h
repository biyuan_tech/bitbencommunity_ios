//
//  ArticleDetailRootViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/15.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "ArticleRootSingleModel.h"

typedef NS_ENUM(NSInteger,ArticleDetailRootViewControllerType) {
    ArticleDetailRootViewControllerTypeArticle = 1,
    ArticleDetailRootViewControllerTypeBanner = 2,
    ArticleDetailRootViewControllerTypeLive = 3,
    ArticleDetailRootViewControllerTypeInfomation = 4,                //资讯
};

@interface ArticleDetailRootViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferArticleId;
@property (nonatomic,assign)ArticleDetailRootViewControllerType transferPageType;

-(void)actionReoloadFollow:(void(^)(BOOL link))block;
-(void)actionReoloadZan:(void(^)(BOOL link))block;

@end
