//
//  ArticleMineRootViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ArticleMineRootViewController.h"
#import "ArticleRootTableViewCell.h"
#import "BYPersonHomeController.h"
#import "ArticleRootActionTableViewCell.h"
#import "ArticleDraftTitleTableViewCell.h"
#import "ShareRootViewController.h"


typedef NS_ENUM(NSInteger ,ArticleMineRootViewControllerType) {
    ArticleMineRootViewControllerTypeRelease,
    ArticleMineRootViewControllerTypeDeaft,
};

@interface ArticleMineRootViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)NSMutableArray *myArticleMutableArr;
@property (nonatomic,strong)UITableView *myArticleTableView;
@property (nonatomic,strong)UITableView *draftTableView;
@property (nonatomic,strong)NSMutableArray *draftMutableArr;
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@end

@implementation ArticleMineRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createSegmentList];
    [self createScrollView];
    [self createTableView];
    [self sendRequestToGetListManagerWithType:ArticleMineRootViewControllerTypeRelease hasLoad:YES];
    [self sendRequestToGetListManagerWithType:ArticleMineRootViewControllerTypeDeaft hasLoad:YES];
}


#pragma mark - pageSetting
-(void)pageSetting{
    self.view.backgroundColor = RGB(245, 245, 245, 1);
    self.barMainTitle = @"我的文章";
}

#pragma mark - segment
-(void)createSegmentList{
    if (!self.segmentList){
        __weak typeof(self)weakSelf = self;
        self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"已发布",@"草稿箱"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width * index, 0) animated:YES];
        }];
        self.segmentList.backgroundColor = [UIColor whiteColor];
        [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
        self.segmentList.selectionIndicatorColor = [UIColor colorWithCustomerName:@"红"];
        [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateSelected];
        self.segmentList.bottomTrimColor = [UIColor clearColor];
        self.segmentList.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(44));
        [self.view addSubview:self.segmentList];
    }
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.myArticleMutableArr = [NSMutableArray array];
    self.draftMutableArr = [NSMutableArray array];
}

#pragma mark - UIScrollView
-(void)createScrollView{
    if (!self.mainScrollView){
        __weak typeof(self)weakSelf = self;
        self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSInteger index = scrollView.contentOffset.x / kScreenBounds.size.width ;
            [strongSelf.segmentList setSelectedButtonIndex:index animated:YES];
        }];
        self.mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, self.view.size_height - self.segmentList.size_height - [BYTabbarViewController sharedController].navBarHeight);
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 2, self.mainScrollView.size_height);
        [self.view addSubview:self.mainScrollView];
    }
    [self createTableView];
}


#pragma mark - UITableView
-(void)createTableView{
    __weak typeof(self)weakSelf = self;
    if (!self.myArticleTableView){
        self.myArticleTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.myArticleTableView.dataSource = self;
        self.myArticleTableView.delegate = self;
        [self.mainScrollView addSubview:self.myArticleTableView];
        
        [self.myArticleTableView appendingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf= weakSelf;
            [strongSelf sendRequestToGetListManagerWithType:ArticleMineRootViewControllerTypeRelease hasLoad:YES];
        }];
        [self.myArticleTableView appendingFiniteScrollingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf= weakSelf;
            [strongSelf sendRequestToGetListManagerWithType:ArticleMineRootViewControllerTypeRelease hasLoad:NO];
        }];
    }
    if (!self.draftTableView){
        self.draftTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.draftTableView.dataSource = self;
        self.draftTableView.orgin_x = kScreenBounds.size.width;
        self.draftTableView.delegate = self;
        [self.mainScrollView addSubview:self.draftTableView];
        
        [self.draftTableView appendingPullToRefreshHandler:^{
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetListManagerWithType:ArticleMineRootViewControllerTypeDeaft hasLoad:YES];
        }];
        
        [self.draftTableView appendingFiniteScrollingPullToRefreshHandler:^{
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetListManagerWithType:ArticleMineRootViewControllerTypeDeaft hasLoad:NO];
        }];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.myArticleTableView){
        return self.myArticleMutableArr.count;
    } else if (tableView == self.draftTableView){
        return self.draftMutableArr.count;
    }return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.myArticleTableView){
        return 2;
    } else if (tableView == self.draftTableView){
        return 3;
    }
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0 && tableView == self.draftTableView){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        ArticleDraftTitleTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[ArticleDraftTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        
        ArticleRootSingleModel *singleModel = [self.draftMutableArr objectAtIndex:indexPath.section];
        cellWithRowThr.transferSingleModel = singleModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr dropCancelReleaseSuccessBlock:^(ArticleRootSingleModel * _Nonnull transferSingleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:indexPath.section];
            [strongSelf.draftTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
        }];
        [cellWithRowThr dropReleaseNowSuccessBlock:^(ArticleRootSingleModel * _Nonnull transferSingleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            // 移除当前cell
            NSInteger index = [strongSelf.draftMutableArr indexOfObject:transferSingleModel];
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
            [strongSelf.draftMutableArr removeObject:transferSingleModel];
            [strongSelf.draftTableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
        }];
        

        return cellWithRowThr;
    } else if ((indexPath.row == 0 && tableView == self.myArticleTableView) || (indexPath.row == 1 && tableView == self.draftTableView)){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        ArticleRootTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[ArticleRootTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.delBtn.hidden = NO;
        cellWithRowOne.moreButton.hidden = YES;
        cellWithRowOne.biLabel.hidden = YES;
        cellWithRowOne.biImgView.hidden = YES;
        ArticleRootSingleModel *singleModel ;
        if (tableView == self.myArticleTableView){
            singleModel = [self.myArticleMutableArr objectAtIndex:indexPath.section];
        } else if (tableView == self.draftTableView){
            singleModel = [self.draftMutableArr objectAtIndex:indexPath.section];
        }
        
        cellWithRowOne.transferSingleModel = singleModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickWithImgSelectedBlock:^(NSString *imgUrl) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf tableView:tableView didSelectRowAtIndexPath:indexPath];
        }];
        
        [cellWithRowOne actionClickWithTagsSelectedBlock:^(NSString *tag) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            ArticleRootViewController *articleRootVC = [[ArticleRootViewController alloc]init];
            articleRootVC.transferType = ArticleRootViewControllerTypeTags;
            articleRootVC.transferTags = tag;
            [strongSelf.navigationController pushViewController:articleRootVC animated:YES];
        }];
        
        [cellWithRowOne actionClickWithLinkButtonBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            
            [strongSelf authorizeWithCompletionHandler:^(BOOL successed) {
                if (successed){
//                        [strongSelf linkWIthCell:cellWithRowOne hasLink:!singleModel.isAttention];
                }
            }];
        }];
        
        [cellWithRowOne actionClickHeaderImgWithBlock:^{
            if (!weakSelf){
                return ;
            }
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = cellWithRowOne.transferSingleModel.author_id;
            [weakSelf.navigationController pushViewController:personHomeController animated:YES];
        }];
        
        @weakify(self);
        [cellWithRowOne actionClickWithDelMoreBtnBlock:^{
            @strongify(self);
            @weakify(self);
            [BYCommonTool showDelSheetViewInCurrentView:cellWithRowOne.transferSingleModel.article_id theme_type:BY_THEME_TYPE_ARTICLE cb:^{
                @strongify(self);
                [self.myArticleMutableArr removeObject:cellWithRowOne.transferSingleModel];
                [self.myArticleTableView reloadData];
            }];
        }];
        
        cellWithRowOne.linkButton.hidden = YES;
        return cellWithRowOne;
    } else if ((indexPath.row == 1 && tableView == self.myArticleTableView) ||(indexPath.row == 2 && tableView == self.draftTableView)){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        ArticleRootActionTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[ArticleRootActionTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        ArticleRootSingleModel *singleModel ;
        if (tableView == self.myArticleTableView){
            singleModel = [self.myArticleMutableArr objectAtIndex:indexPath.section];
        } else if (tableView == self.draftTableView){
            singleModel = [self.draftMutableArr objectAtIndex:indexPath.section];
        }

        cellWithRowTwo.transferSingleModel = singleModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo actionClickWithShareManagerWithArticleModelBlock:^(ArticleRootSingleModel *articleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf shareManager:articleModel];
        }];
        return cellWithRowTwo;
    }
    static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
    GWNormalTableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
    if (!cellWithRowOther){
        cellWithRowOther = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
    }
    
    return cellWithRowOther;
        
    
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        ArticleRootSingleModel *singleModel ;
        if (tableView == self.myArticleTableView){
            singleModel = [self.myArticleMutableArr objectAtIndex:indexPath.section];
        } else if (tableView == self.draftTableView){
            singleModel = [self.draftMutableArr objectAtIndex:indexPath.section];
        }
        
        ArticleDetailRootViewController *viewController = [[ArticleDetailRootViewController alloc]init];
        viewController.transferArticleId = singleModel._id;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.draftTableView){
        if (indexPath.row == 0){
            return [ArticleDraftTitleTableViewCell calculationCellHeight];
        } else if (indexPath.row == 1){
            ArticleRootSingleModel *singleModel;
            singleModel = [self.draftMutableArr objectAtIndex:indexPath.section];
            return [ArticleRootTableViewCell calculationCellHeightWithModel:singleModel];
        } else {
            return LCFloat(50);
        }
    } else if (tableView == self.myArticleTableView){
        if (indexPath.row == 0){
            ArticleRootSingleModel *singleModel;
            singleModel = [self.myArticleMutableArr objectAtIndex:indexPath.section];
            return [ArticleRootTableViewCell calculationCellHeightWithModel:singleModel];
        } else {
            return LCFloat(50);
        }
    }
    return 50;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    SeparatorType separatorType = SeparatorTypeMiddle;
    if (indexPath.row == 0 && tableView == self.myArticleTableView){
        separatorType  = SeparatorTypeBottom;
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"message"];
    } else if (tableView == self.draftTableView &&(indexPath.row == 0 || indexPath.row == 1)){
        separatorType  = SeparatorTypeBottom;
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"message"];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(10);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - 接口
-(void)sendRequestToGetListManagerWithType:(ArticleMineRootViewControllerType)type hasLoad:(BOOL)hasLoad{
    __weak typeof(self)weakSelf = self;
    NSString *authorId = [AccountModel sharedAccountModel].loginServerModel.user._id;
    NSString *status = @"";
    NSInteger page = 0;
    if (type == ArticleMineRootViewControllerTypeDeaft){            // 草稿箱
        status = @"0";
        page = self.draftTableView.currentPage;
    } else if (type == ArticleMineRootViewControllerTypeRelease){   // 已发布
        status = @"1";
        page = self.myArticleTableView.currentPage;
    }

    [[NetworkAdapter sharedAdapter] articleListWithPage:page topic:nil author_id:authorId status:status block:^(NSArray<ArticleRootSingleModel> *articleList) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        if (type == ArticleMineRootViewControllerTypeRelease){
            if (strongSelf.myArticleTableView.isXiaLa ||hasLoad){
                [strongSelf.myArticleMutableArr removeAllObjects];
            }
            [strongSelf.myArticleMutableArr addObjectsFromArray:articleList];
            
            if (articleList.count){
                [strongSelf.myArticleTableView reloadData];
            }
            
            if (strongSelf.myArticleTableView.isXiaLa){
                [strongSelf.myArticleTableView stopPullToRefresh];
            } else {
                [strongSelf.myArticleTableView stopFinishScrollingRefresh];
            }
            
            if (strongSelf.myArticleMutableArr.count){
                [strongSelf.myArticleTableView dismissPrompt];
            } else {
                [strongSelf.myArticleTableView showPrompt:@"当前没有文章" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
            
        } else if (type == ArticleMineRootViewControllerTypeDeaft){
            if (strongSelf.draftTableView.isXiaLa || hasLoad){
                [strongSelf.draftMutableArr removeAllObjects];
            }
            [strongSelf.draftMutableArr addObjectsFromArray:articleList];
            
            if (articleList.count){
                 [strongSelf.draftTableView reloadData];
            }

            if (strongSelf.draftTableView.isXiaLa){
                [strongSelf.draftTableView stopPullToRefresh];
            } else {
                [strongSelf.draftTableView stopFinishScrollingRefresh];
            }
            
            if (strongSelf.draftMutableArr.count){
                [strongSelf.draftTableView dismissPrompt];
            } else {
                [strongSelf.draftTableView showPrompt:@"当前没有文章" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
        }
    }];
}



#pragma mark 分享内容
-(void)shareManager:(ArticleRootSingleModel *)articleModel{
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.collection_status = articleModel.collection_status;
    shareViewController.transferCopyUrl = articleModel.article_url;
    shareViewController.hasJubao = NO;
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        } else {
            shareType = thirdLoginTypeWechat;
        }
        id smartImgUrl;
        if (articleModel.picture.count){
            smartImgUrl = [articleModel.picture firstObject];
        } else {
            smartImgUrl = articleModel.head_img;
        }
        
        id mainImgUrl;
        if ([smartImgUrl isKindOfClass:[NSString class]]){
            NSString *urlStr = (NSString *)smartImgUrl;
            NSString *baseURL = [PDImageView getUploadBucket:urlStr];
            NSString *nUrl = [[NSString stringWithFormat:@"%@%@",baseURL,urlStr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            mainImgUrl = nUrl;
        } else if ([smartImgUrl isKindOfClass:[UIImage class]]){
            mainImgUrl = smartImgUrl;
        }
        
        NSString *title = @"";
        NSString *content = @"";
        
        
        if (articleModel.article_type == article_typeWeb){
            title = articleModel.title;
            if (articleModel.subtitle.length && ![articleModel.subtitle isEqualToString:@"null"]){
                content = articleModel.subtitle;
            } else {
                content = @"最新最热区块链内容，尽在币本社区";
            }
        } else if (articleModel.article_type == article_typeNormal){
            title = articleModel.title.length?articleModel.title:articleModel.content;
            content = @"最新最热区块链内容，尽在币本社区";
        }
        [ShareSDKManager shareManagerWithType:shareType title:title desc:content img:mainImgUrl url:articleModel.article_url callBack:NULL];
        [ShareSDKManager shareSuccessBack:shareTypeArticle block:NULL];
    }];
    
//    [shareViewController actionClickWithJubaoBlock:^{
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        ReportViewController *reportVC = [[ReportViewController alloc]init];
//        [reportVC showInView:strongSelf.parentViewController type:BY_THEME_TYPE_ARTICLE itemId:articleModel._id];
//    }];
    
    [shareViewController actionClickWithCollectionBlock:^{
        articleModel.collection_status = !articleModel.collection_status;
    }];
    [shareViewController showInView:self.parentViewController];
}
@end
