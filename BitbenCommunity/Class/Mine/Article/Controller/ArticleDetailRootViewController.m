//
//  ArticleDetailRootViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/15.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleDetailRootViewController.h"
#import "ArticleDetailTitleTableViewCell.h"             // 标题头
#import "ArticleDetailAvatarTableViewCell.h"
#import "ArticleDetailTagsTableViewCell.h"              // 话题标签
#import "ArticleDetailItemsTableViewCell.h"
#import "NetworkAdapter+Article.h"
#import "ArticleNewDetailBottomInputView.h"
#import "LiveForumRootSingleTableViewCell.h"
#import "LiveForumItemsTableViewCell.h"
#import "LiveForumRootSingleSubTableViewCell.h"
#import "LiveForumDetailViewController.h"
#import "ArticleDetailInfoTableViewCell.h"
#import "ArticleDetailNoneInfoTableViewCell.h"
#import "GWAssetsImgSelectedViewController.h"
#import "ShareRootViewController.h"
#import "NetworkAdapter+Center.h"
#import "BYPersonHomeController.h"
#import "ReportViewController.h"
#import "BYPersonHomeController.h"
#import "ReportViewController.h"
#import "ArticlePercentDrivenTransition.h"
#import "ArticleNewReleaseViewController.h"
#import "ArticleDetailTuijianSingleTableViewCell.h"
#import "ArticleSubHuifuViewController.h"
#import "ArticleDetailTishiTableViewCell.h"
#import "BYArticleShareImgController.h"

static char actionReoloadFollowKey;
static char actionReoloadZanKey;
@interface ArticleDetailRootViewController ()<UITableViewDelegate,UITableViewDataSource,WKNavigationDelegate,WKUIDelegate>{
    CGRect newKeyboardRect;
    LiveFourumRootListSingleModel *pinglunSingleModel;
    CGFloat webHeight;
    ArticleDetailItemsTableViewCell *biaoqianCell;
    CGFloat navScrollMax;
    ArticleDetailAvatarTableViewCell *avatarCell;
}
@property (nonatomic,strong)ArticleNewDetailBottomInputView *inputView;                  /**< 输入框*/
@property (nonatomic,strong)UITableView *articleDetailTableView;
@property (nonatomic,strong)NSMutableArray *articleDetailRootMutableArr;        /**< 整体*/
@property (nonatomic,strong)NSArray *infoArr;                                   /**< 布局*/
@property (nonatomic,strong)NSMutableArray *articleHotMutableArr;               /**< 热门*/
@property (nonatomic,strong)NSMutableArray *articleNormalMutableArr;            /**< 全部*/
@property (nonatomic,strong)NSMutableArray *articleTuijianMutableArr;           /**< 推荐*/
@property (nonatomic,strong)ArticleRootSingleModel *articleRootModel;

// view
@property (nonatomic,strong)ArticlePercentDrivenTransition *transition;
@property (nonatomic,strong)UIView *navBarView;
@property (nonatomic,strong)PDImageView *navAvatarImgView;
@property (nonatomic,strong)UILabel *navTitleLabel;
@property (nonatomic,strong)UIButton *navLinkButton;
@property (nonatomic,strong)UILabel *navBarLabel;

@end

@implementation ArticleDetailRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];                         // 1. 页面初始化
    [self arrayWithInit];                       // 2. 数组初始化
    [self createNav];
    [self createInputView];                     // 3. 创建输入框
    [self createTableView];                     // 4. 创建列表
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self createInterfaceManager];              // 5. 创建接口
    });
}

#pragma mark - InterfaceManager
-(void)createInterfaceManager{
    __weak typeof(self)weakSelf = self;
    // 1. 获取
    [weakSelf sendRequestToGetArticleBlock:^(ArticleRootSingleModel *model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.articleDetailTableView reloadData];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self sendRequestToGetCommentListWithReload:YES block:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                dispatch_async(dispatch_get_main_queue(), ^{
                    // 更新界面
                    [strongSelf.articleDetailTableView reloadData];
                });
            }];
        });
    }];
}

-(void)actionArrayInfoManager{
    //如果是资讯的话
    if (self.transferPageType == ArticleDetailRootViewControllerTypeInfomation){
        self.articleRootModel.article_type = article_typeInfo;
    }
    
    
    // 进行数据内容变换为长图文
    if (self.articleRootModel.article_type == article_typeWeb || self.articleRootModel.article_type == article_typeInfo){
        TSHomeClassDetailJieshaoWebTableViewCellModel *webModel = [[TSHomeClassDetailJieshaoWebTableViewCellModel alloc]init];
        webModel.html = self.articleRootModel.content;
        self.articleRootModel.webLoadTempModel = webModel;
    }
    
    // 根据返回图文类型进行数据布局
    if (self.articleRootModel.article_type == article_typeWeb){
        if (self.articleRootModel.content.length){
            self.infoArr = @[@"标题",@"头像",@"内容",@"话题",@"提示",@"标签"];
        } else {
            self.infoArr = @[@"标题",@"头像",@"话题",@"提示",@"标签"];
        }
        self.navBarLabel.text = @"观点详情";
    } else if (self.articleRootModel.article_type == article_typeNormal){
        if (self.articleRootModel.picture.count){
            self.infoArr = @[@"头像",@"标题",@"内容",@"话题",@"提示",@"标签"];
        } else {
            self.infoArr = @[@"头像",@"标题",@"话题",@"提示",@"标签"];
        }
        self.navBarLabel.text = @"观点详情";
    } else if (self.articleRootModel.article_type == article_typeInfo){
         if (self.articleRootModel.content.length){
             self.infoArr = @[@"标题",@"头像",@"内容",@"话题",@"提示"];
         } else {
             self.infoArr = @[@"标题",@"头像",@"话题",@"提示"];
         }
        self.barMainTitle = @"资讯详情";
        self.navBarLabel.text = @"资讯详情";
    }
    [self.articleDetailRootMutableArr addObject:self.infoArr];
    [self.articleDetailRootMutableArr addObject:self.articleTuijianMutableArr];
    [self.articleDetailRootMutableArr addObject:self.articleNormalMutableArr];
    

}


#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"观点详情";
    self.view.backgroundColor = [UIColor hexChangeFloat:@"F2F4F5"];
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_article_more"] barHltImage:[UIImage imageNamed:@"icon_article_more"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf shareManager];
    }];
}

-(void)arrayWithInit{
    self.articleDetailRootMutableArr = [NSMutableArray array];                          // 文章详情
    self.articleNormalMutableArr = [NSMutableArray array];                              // 全部内容
    self.articleHotMutableArr = [NSMutableArray array];                                 // 热门内容
    self.articleTuijianMutableArr = [NSMutableArray array];                             // 推荐内容
}

#pragma mark - 创建输入框
-(void)createInputView{
    if (!self.inputView){
        CGRect inputFrame = CGRectMake(0, kScreenBounds.size.height - [ArticleNewDetailBottomInputView calculationHeight:@""], kScreenBounds.size.width, [ArticleNewDetailBottomInputView  calculationHeight:@""]);
        self.inputView = [[ArticleNewDetailBottomInputView alloc]initWithFrame:inputFrame];
        self.inputView.inputView.returnKeyType = UIReturnKeySend;
        self.inputView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        self.inputView.layer.shadowOpacity = .2f;
        self.inputView.layer.shadowOffset = CGSizeMake(.2f, .2f);
        
        [self.view addSubview:self.inputView];
    }
    __weak typeof(self)weakSelf = self;
    // 发送按钮
    [self.inputView actionClickWithSendInfoBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToAddComment];
    }];
    
    [self.inputView.inputView textViewSendActionBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToAddComment];
    }];
    // 内容变更
    [self.inputView actionTextInputChangeBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (IS_iPhoneX){
            strongSelf.inputView.orgin_y = strongSelf.view.size_height - strongSelf->newKeyboardRect.size.height - [ArticleNewDetailBottomInputView calculationHeight:strongSelf.inputView.inputView.text] + LCFloat(20) ;
        } else {
            strongSelf.inputView.orgin_y = strongSelf.view.size_height - strongSelf->newKeyboardRect.size.height - [ArticleNewDetailBottomInputView calculationHeight:strongSelf.inputView.inputView.text] ;
        }
    }];
    
    // 分享
    [self.inputView actionShareBtnClciManagerBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf shareManager];
    }];
    
    // 评论
    [self.inputView actionCommentBtnClciManagerBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger section = [self cellIndexPathSectionWithcellData:@"评论" sourceArr:self.articleDetailRootMutableArr];
        if (section != -1){
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
            [strongSelf.articleDetailTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }];
    
    // 攒
    [self.inputView actionZanBtnClciManagerBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf actionClickManagerWithArticle];
    }];
    
    // 收藏
    [self.inputView actionCollectionBtnClciManagerBlock:^{
        if (!weakSelf){
             return ;
         }
         __strong typeof(weakSelf)strongSelf = weakSelf;
    
         [strongSelf actionClickManagerWithCollection];
    }];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.articleDetailTableView){
        self.articleDetailTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.articleDetailTableView.orgin_y = CGRectGetMaxY(self.navBarView.frame);
        self.articleDetailTableView.size_height -= ([ArticleNewDetailBottomInputView  calculationHeight:@""] + [BYTabbarViewController sharedController].navBarHeight);
        self.articleDetailTableView.dataSource= self;
        self.articleDetailTableView.delegate = self;
        [self.view addSubview:self.articleDetailTableView];
    }
    
    [self.view addSubview:self.inputView];
    __weak typeof(self)weakSelf = self;
    [self.articleDetailTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetCommentListWithReload:YES block:^{
            [strongSelf.articleDetailTableView reloadData];
        }];
    }];
    
    [self.articleDetailTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetCommentListWithReload:NO block:^{
            [strongSelf.articleDetailTableView reloadData];
        }];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.articleDetailRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.articleDetailRootMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        ArticleDetailTitleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[ArticleDetailTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        if (self.articleRootModel.article_type == article_typeWeb || self.articleRootModel.article_type == article_typeInfo){         // 长文
            cellWithRowOne.transferTitle = self.articleRootModel.title;
        } else if (self.articleRootModel.article_type == article_typeNormal){       // 短文
            cellWithRowOne.transferSpacing = 10;
            cellWithRowOne.transferTitle = self.articleRootModel.content.length?self.articleRootModel.content:self.articleRootModel.title;
        }
        
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"头像" sourceArr:self.articleDetailRootMutableArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        ArticleDetailAvatarTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[ArticleDetailAvatarTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        avatarCell = cellWithRowTwo;
        if (!self.navAvatarImgView.image){
            self.navAvatarImgView.style = self.articleRootModel.cert_badge;
            self.navAvatarImgView.image = cellWithRowTwo.transferArticleModel.avatarRoundImg;
            [self.navAvatarImgView setNeedsLayout];
            self.navTitleLabel.text = self.articleRootModel.nickname;
            if (self.articleRootModel.isAttention){
                [self.navLinkButton setImage:[UIImage imageNamed:@"icon_article_link_nor"] forState:UIControlStateNormal];
            } else {
                [self.navLinkButton setImage:[UIImage imageNamed:@"icon_article_link_hlt"] forState:UIControlStateNormal];
            }
        }
        
        cellWithRowTwo.transferArticleModel = self.articleRootModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo actionLinkButtonClick:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf linkWIthCell:cellWithRowTwo hasLink:!strongSelf.articleRootModel.isAttention];
        }];
        [cellWithRowTwo actionClickHeaderImgWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = cellWithRowTwo.transferArticleModel.author_id;
            [strongSelf.navigationController pushViewController:personHomeController animated:YES];
        }];
        
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"内容" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"内容" sourceArr:self.articleDetailRootMutableArr]){
        static NSString *cellIdentifyWithRowTwo1 = @"cellIdentifyWithRowTwo1";
        ArticleDetailInfoTableViewCell *cellWithRowTwo1 = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo1];
        if (!cellWithRowTwo1){
            cellWithRowTwo1 = [[ArticleDetailInfoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo1];
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo1 actionClickWithImgSelectedBlock:^(NSString *imgUrl) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            GWAssetsImgSelectedViewController *imgSelected = [[GWAssetsImgSelectedViewController alloc]init];
            imgSelected.canSave = YES;
            NSInteger index = [strongSelf.articleRootModel.picture indexOfObject:imgUrl];
            [imgSelected showInView:strongSelf.parentViewController imgArr:strongSelf.articleRootModel.picture currentIndex:index cell:cellWithRowTwo1];
        }];
        
        [cellWithRowTwo1 actionWebViewDidLoad:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.articleDetailTableView reloadData];
        }];
        
        [cellWithRowTwo1 actionClickWithWebImgBlock:^(NSInteger index, NSString *imgUrl, CGRect rect, NSArray *imgArr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            GWAssetsImgSelectedViewController *imgSelected = [[GWAssetsImgSelectedViewController alloc]init];
            imgSelected.canSave = YES;
            imgSelected.webViewSelectedRect = rect;
            // 获取点击的图片
            NSString *indexImgUrl = [imgArr objectAtIndex:index];
            if (!cellWithRowTwo1.selectedImgView){
                cellWithRowTwo1.selectedImgView = [[PDImageView alloc]init];
            }
            
            [cellWithRowTwo1.selectedImgView uploadImageWithURL:indexImgUrl placeholder:nil callback:^(UIImage *image) {
                [imgSelected showInView:strongSelf.parentViewController imgArr:imgArr currentIndex:index cell:cellWithRowTwo1];
            }];
        }];
        
        [cellWithRowTwo1 actionClickWithWebUrlDirectBlock:^(NSString *webUrl) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDWebViewController *webViewController = [[PDWebViewController alloc]init];
            [webViewController webDirectedWebUrl:webUrl];
            [strongSelf.navigationController pushViewController:webViewController animated:YES];
        }];
        
        cellWithRowTwo1.transferArticleModel = self.articleRootModel;
        return cellWithRowTwo1;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        ArticleDetailTagsTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[ArticleDetailTagsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferArticleModel = self.articleRootModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr actionItemClickWithBlock:^(NSString *info) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            ArticleRootViewController *articleRootVC = [[ArticleRootViewController alloc]init];
            articleRootVC.transferType = ArticleRootViewControllerTypeTags;
            articleRootVC.transferTags = info;
            [strongSelf.navigationController pushViewController:articleRootVC animated:YES];
        }];
        
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提示" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"提示" sourceArr:self.articleDetailRootMutableArr]){
        static NSString *cellIdentifyWithRowTishi = @"cellIdentifyWithRowTishi";
        ArticleDetailTishiTableViewCell *cellWithRowTishi = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTishi];
        if (!cellWithRowTishi){
            cellWithRowTishi = [[ArticleDetailTishiTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTishi];
        }
        return cellWithRowTishi;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标签" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标签" sourceArr:self.articleDetailRootMutableArr]){
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        ArticleDetailItemsTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[ArticleDetailItemsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
        }
        cellWithRowFiv.transferArticleModel = self.articleRootModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowFiv actionZanManagerBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf actionClickManagerWithArticle];
        }];
        biaoqianCell = cellWithRowFiv;
        return cellWithRowFiv;
    } else {
        id info = [[self.articleDetailRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        if ([info isKindOfClass:[NSString class]] && ([info isEqualToString:@"热门评论"] || [info isEqualToString:@"评论"])){
            static NSString *cellIdentifyWithRowSix = @"cellIdentifyWithRowSix";
            GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSix];
            if (!cellWithRowOne){
                cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSix];
            }
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            cellWithRowOne.transferCellHeight = cellHeight;
            if ([info isEqualToString:@"热门评论"]){
                cellWithRowOne.transferTitle = @"热门评论";
            } else if ([info isEqualToString:@"评论"]){
                cellWithRowOne.transferTitle = @"评论";
            }
            return cellWithRowOne;
        } else if ([info isKindOfClass:[NSString class]] && ([info isEqualToString:@"空"])){
            static NSString *cellIdentifyWithRowNone1 = @"cellIdentifyWithRowNone1";
            ArticleDetailNoneInfoTableViewCell *cellWithRowNone = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowNone1];
            if (!cellWithRowNone){
                cellWithRowNone = [[ArticleDetailNoneInfoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowNone1];
            }
            return cellWithRowNone;
        } else if ([info isKindOfClass:[LiveFourumRootListSingleModel class]]){
            LiveFourumRootListSingleModel *tempSingleModel = (LiveFourumRootListSingleModel *)info;
            static NSString *cellIdentifyWithRowSev = @"cellIdentifyWithRowSev";
            LiveForumRootSingleTableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev];
            if (!cellWithRowSev){
                cellWithRowSev = [[LiveForumRootSingleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev];
            }
            cellWithRowSev.hasArticleDetail = YES;
            cellWithRowSev.transferItemId = self.articleRootModel._id;
            cellWithRowSev.transferInfoManager = tempSingleModel;
            
            __weak typeof(self)weakSelf = self;
            [cellWithRowSev actionClickMoreBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                
                ReportViewController *reportVC = [[ReportViewController alloc]init];
                if (strongSelf.transferPageType == ArticleDetailRootViewControllerTypeLive){        // 直播里使用
                    [reportVC showInView:strongSelf.parentViewController type:BY_THEME_TYPE_LIVE itemId:tempSingleModel._id];
                } else if (strongSelf.transferPageType == ArticleDetailRootViewControllerTypeArticle){  // 文章
                    [reportVC showInView:strongSelf.parentViewController type:BY_THEME_TYPE_ARTICLE_COMMENT itemId:tempSingleModel._id];
                }
                
            }];
            [cellWithRowSev actionClickHeaderImgWithBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
                personHomeController.user_id = cellWithRowSev.transferInfoManager.user_id;
                [strongSelf.navigationController pushViewController:personHomeController animated:YES];
            }];
            
            [cellWithRowSev actionClickZanManagerBlock:^{
                if(!weakSelf){
                    return ;
                }
                
            }];
            
            [cellWithRowSev actionClickReplaceManagerBlock:^{
                if(!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                strongSelf->pinglunSingleModel = tempSingleModel;
                strongSelf.inputView.transferListModel = tempSingleModel;
                if (![strongSelf.inputView.inputView isFirstResponder]){
                    [strongSelf.inputView.inputView becomeFirstResponder];
                }
                strongSelf.inputView.inputView.text = @"";
                strongSelf.inputView.inputView.placeholder = [NSString stringWithFormat:@"回复：%@",tempSingleModel.comment_name];
            }];
            
            return cellWithRowSev;
        } else if ([info isKindOfClass:[ArticleRootSingleModel class]]){
            static NSString *cellIdentifyWithRowTuijianArticle = @"cellIdentifyWithRowTuijianArticle";
            ArticleDetailTuijianSingleTableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTuijianArticle];
            if (!cellWithRowSev){
                cellWithRowSev = [[ArticleDetailTuijianSingleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTuijianArticle];
            }
            cellWithRowSev.transferCellHeight = cellHeight;
            NSInteger row = indexPath.row;
            ArticleRootSingleModel *singleModel = [self.articleTuijianMutableArr objectAtIndex:row];
            cellWithRowSev.transferArticleRootModel = singleModel;
            
            return cellWithRowSev;
        } else {
            static NSString *cellIdentifyWithRowSix = @"cellIdentifyWithRowSix";
            GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSix];
            if (!cellWithRowOne){
                cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSix];
            }
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferTitle = @"评论";
            
            return cellWithRowOne;
        }
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr]){
        
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"头像" sourceArr:self.articleDetailRootMutableArr]){
        
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"内容" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"内容" sourceArr:self.articleDetailRootMutableArr]){
        
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr]){
       
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标签" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标签" sourceArr:self.articleDetailRootMutableArr]){
       
    } else {
        id info = [[self.articleDetailRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        if ([info isKindOfClass:[NSString class]] && ([info isEqualToString:@"热门评论"] || [info isEqualToString:@"评论"])){
            
        } else if ([info isKindOfClass:[NSString class]] && ([info isEqualToString:@"空"])){
           
        } else if ([info isKindOfClass:[LiveFourumRootListSingleModel class]]){
            if ([self.inputView.inputView isFirstResponder]){
                [self.inputView.inputView resignFirstResponder];
            }
            
            LiveFourumRootListSingleModel *singleModel = (LiveFourumRootListSingleModel *)info;
            ArticleSubHuifuViewController *controller = [[ArticleSubHuifuViewController alloc]init];
            controller.transferPageType = LiveForumDetailViewControllerTypeArticle;
            controller.transferFourumRootModel = singleModel;
            controller.transferRoomId = self.articleRootModel._id;
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:controller];
            
            self.transition = [[ArticlePercentDrivenTransition alloc]initWithModalViewController:nav chiledController:controller];
            [self.transition actionLoadMoreManagerBLock:^{
                [controller loadInfoManager];
            }];
            self.transition.dragable = YES;//---是否可下拉收起
            nav.transitioningDelegate = self.transition;
            nav.modalPresentationStyle = UIModalPresentationCustom;
            // pop block
            [self presentViewController:nav animated:YES completion:nil];

        } else if ([info isKindOfClass:[ArticleRootSingleModel class]]){
           
            ArticleRootSingleModel *singleModel = (ArticleRootSingleModel *)info;
            ArticleDetailRootViewController *detailVC = [[ArticleDetailRootViewController alloc]init];
            detailVC.transferArticleId = singleModel._id;
            [self.navigationController pushViewController:detailVC animated:YES];
           
        } else {
           
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr]){
        NSString *info = @"";
        if (self.articleRootModel.article_type == article_typeWeb || self.articleRootModel.article_type == article_typeInfo){         // 长文
            info = self.articleRootModel.title;
            return [ArticleDetailTitleTableViewCell calculationCellHeightWithLongTextTitle:info];
        } else if (self.articleRootModel.article_type == article_typeNormal){       // 短文
            info = self.articleRootModel.content.length?self.articleRootModel.content:self.articleRootModel.title;
            return [ArticleDetailTitleTableViewCell calculationCellHeightTitle:info];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"头像" sourceArr:self.articleDetailRootMutableArr]){
        return [ArticleDetailAvatarTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr]){
        return [ArticleDetailTagsTableViewCell calculationCellHeight:self.articleRootModel];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标签" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标签" sourceArr:self.articleDetailRootMutableArr]){
        return [ArticleDetailItemsTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"内容" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"内容" sourceArr:self.articleDetailRootMutableArr]){
        if (self.articleRootModel.article_type == article_typeNormal) {
            return [ArticleDetailInfoTableViewCell calculationCellHeightWithModel:self.articleRootModel];
        } else {
            return self.articleRootModel.webLoadTempModel.transferCellHeight;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提示" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"提示" sourceArr:self.articleDetailRootMutableArr]){
        return [ArticleDetailTishiTableViewCell calculationCellHeight];
    } else {
        id info = [[self.articleDetailRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        if ([info isKindOfClass:[NSString class]] && ([info isEqualToString:@"热门评论"] || [info isEqualToString:@"评论"])){
            return [GWNormalTableViewCell calculationCellHeight];
        } else if ([info isKindOfClass:[NSString class]] && ([info isEqualToString:@"空"])){
            return [ArticleDetailNoneInfoTableViewCell calculationCellHeight];
        } else if ([info isKindOfClass:[LiveFourumRootListSingleModel class]]){
            return [LiveForumRootSingleTableViewCell calculationCellHeightWithModel:info];
        } else if ([info isKindOfClass:[ArticleRootSingleModel class]]){
            return [ArticleDetailTuijianSingleTableViewCell calculationCellHeight];
        } else {
            return [GWNormalTableViewCell calculationCellHeight];
        }
    }

    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return .1f;
    } else {
        return LCFloat(8);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.articleDetailTableView) {
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"话题" sourceArr:self.articleDetailRootMutableArr]){
            SeparatorType separatorType = SeparatorTypeBottom;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"message"];
        }
    }
}



- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr]){
        return YES;
    } else {
        return NO;
    }
}

// 允许每一个Action

- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr]){
        if (action == @selector(copy:)) { // 支持复制和黏贴
            return YES;
        }
    }
    
    return NO;
}



// 对一个给定的行告诉代表执行复制或黏贴操作内容

- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.articleDetailRootMutableArr]){
    
        ArticleDetailTitleTableViewCell *cell = (ArticleDetailTitleTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        UIPasteboard *pasteBoard = [UIPasteboard generalPasteboard]; // 黏贴板
        
        [pasteBoard setString:cell.titleLabel.text];
        
    }
}





#pragma mark - 接口
#pragma mark 根据id获取文章
-(void)sendRequestToGetArticleBlock:(void(^)(ArticleRootSingleModel *model))block{
    NSString *articleId = @"";
    if (self.transferPageType == ArticleDetailRootViewControllerTypeLive){
        articleId = [AccountModel sharedAccountModel].serverModel.config.guide_publish_live;

    } else if (self.transferPageType == ArticleDetailRootViewControllerTypeArticle){
        articleId = [AccountModel sharedAccountModel].serverModel.config.guide_publish_long_article;
    } else {
        articleId = self.transferArticleId;
    }

    NSDictionary *params = @{@"article_id":articleId.length?articleId:@""};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:article_get_article requestParams:params responseObjectClass:[ArticleRootSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            ArticleRootSingleModel *model = (ArticleRootSingleModel *)responseObject;
            strongSelf.articleRootModel = model;
            // 回复数量
            strongSelf.inputView.transferCommentCount = [NSString stringWithFormat:@"%li",model.comment_count];
            // 底部的赞按钮状态
            [strongSelf.inputView zanBtnStatus:model.isSupport];
            // 底部的收藏按钮
            [strongSelf.inputView collectionStatus:model.collection_status];
            
            // 插入推荐
            [strongSelf.articleTuijianMutableArr addObjectsFromArray:model.link_article_list];
            
            // 更新数据模板
            [strongSelf actionArrayInfoManager];
            NSString *string = [model.content removeH5Tag];
            PDLog(@"string:-------%@",string);
            if (block){
                block(model);
            }
        }
    }];
}

#pragma mark 热门评论
-(void)sendRequestToGetHotCommentWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] articleGetHotCommentId:self.articleRootModel._id actionBlock:^(BOOL isSuccessed, LiveFourumRootListModel *list) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [strongSelf.articleHotMutableArr removeAllObjects];
            if (list.content.count){
                [strongSelf.articleHotMutableArr addObject:@[@"热门评论"]];
                [strongSelf.articleHotMutableArr addObjectsFromArray:list.content];
                [strongSelf.articleDetailRootMutableArr addObjectsFromArray:strongSelf.articleHotMutableArr];
            }
        }
        if (block){
            block();
        }
    }];
}

#pragma mark 新增评论
-(void)sendRequestToAddComment{
    __weak typeof(self)weakSelf = self;
    NSString *string = [self.inputView.inputView.text removeBothEndsEmptyString:BYRemoveTypeAll];
    [[NetworkAdapter sharedAdapter] articleCreateCommentWiththemeId:self.articleRootModel._id ThemeInfo:self.inputView.transferListModel  content:string block:^(BOOL isSuccessed, NSString *comment_id) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            
            [strongSelf.inputView cleanKeyboard];
            
            LiveFourumRootListSingleModel *singleModel = [[LiveFourumRootListSingleModel alloc]init];
            singleModel.comment_id = comment_id;
            singleModel._id = comment_id;
            singleModel.comment_name = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
            singleModel.comment_pic = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
            singleModel.content = strongSelf.inputView.inputView.text;
            singleModel.count_reply = 0;
            singleModel.content = string;
            singleModel.count_support = 0;
            singleModel.count_tread = 0;
            singleModel.create_time = [NSDate getNSTimeIntervalWithCurrent] * 1000;
            singleModel.isAttention = 0;
            singleModel.isSupport = 0;
            singleModel.isTread = 0;
            singleModel.reply_comment_id = strongSelf.inputView.transferListModel.reply_comment_id;
            singleModel.reply_comment_name = strongSelf.inputView.transferListModel.reply_comment_name;
            singleModel.reply_comment_pic = strongSelf.inputView.transferListModel.reply_comment_pic;
            singleModel.reply_user_id = strongSelf.inputView.transferListModel.reply_user_id;
            singleModel.theme_id = self.articleRootModel._id;
            singleModel.theme_type = @"1";
            singleModel.reward = @"待揭晓";
            singleModel.user_id = [AccountModel sharedAccountModel].account_id;
            singleModel.cert_badge = [AccountModel sharedAccountModel].loginServerModel.user.cert_badge;
            singleModel.achievement_badge_list = [AccountModel sharedAccountModel].loginServerModel.user.achievement_badge_list;
            
            NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"评论" sourceArr:self.articleDetailRootMutableArr];
            if(strongSelf.inputView.transferListModel){
               strongSelf.inputView.transferListModel.count_reply += 1;
                
                // 刷新
                NSInteger row = [strongSelf.articleNormalMutableArr indexOfObject:strongSelf.inputView.transferListModel];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
                [strongSelf.articleDetailTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            } else {
                if([strongSelf.articleNormalMutableArr containsObject:@"空"]){
                    NSInteger index = [strongSelf. articleNormalMutableArr indexOfObject:@"空"];
                    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:index inSection:section];
                    [strongSelf.articleNormalMutableArr removeObject:@"空"];
                    [strongSelf.articleDetailTableView deleteRowsAtIndexPaths:@[indexPath1] withRowAnimation:UITableViewRowAnimationNone];
                }
                
                // 1. 找到m评论
                if ([strongSelf.articleNormalMutableArr containsObject:@"评论"]){
                    NSInteger pinglunIndex = [strongSelf.articleNormalMutableArr indexOfObject:@"评论"];
                    
                    [strongSelf.articleNormalMutableArr insertObject:singleModel atIndex:pinglunIndex + 1];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:pinglunIndex + 1 inSection:section];
                    [strongSelf.articleDetailTableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];

                }
                                
                
            }
            
            
            
            strongSelf.inputView.transferListModel = nil;
            
            
            
//            [strongSelf sendRequestToGetCommentListWithReload:YES block:^{
//                [strongSelf.articleDetailTableView reloadData];
//            }];
        }
    }];
}



#pragma mark - 获取评论列表
-(void)sendRequestToGetCommentListWithReload:(BOOL)rereload block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] sendRequestToGetInfoWithCommentId:self.articleRootModel._id reReload:rereload page:self.articleDetailTableView.currentPage actionBlock:^(BOOL isSuccessed, LiveFourumRootListModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(isSuccessed){
            if (strongSelf.articleDetailTableView.isXiaLa || rereload){
                [strongSelf.articleNormalMutableArr removeAllObjects];
                [strongSelf.articleNormalMutableArr addObject:@"评论"];
            }
            
            // 添加数据
            [strongSelf.articleNormalMutableArr addObjectsFromArray:listModel.content];
            
            if (strongSelf.articleNormalMutableArr.count == 1 && [strongSelf.articleNormalMutableArr containsObject:@"评论"]){
                if (![strongSelf.articleNormalMutableArr containsObject:@"空"]){
                    [strongSelf.articleNormalMutableArr addObject:@"空"];
                }
            }
            
            if (rereload || strongSelf.articleDetailTableView.isXiaLa){
                [strongSelf.articleDetailTableView stopPullToRefresh];
            } else {
                [strongSelf.articleDetailTableView stopFinishScrollingRefresh];
            }
            
            if (block){
                block();
            }
        }
    }];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.inputView releaseKeyboard];
}



//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    // 判断webView所在的cell是否可见，如果可见就layout
//    NSArray *cells = self.articleDetailTableView.visibleCells;
//    for (UITableViewCell *cell in cells) {
//        if ([cell isKindOfClass:[ArticleDetailInfoTableViewCell class]]) {
//            ArticleDetailInfoTableViewCell *webCell = (ArticleDetailInfoTableViewCell *)cell;
//            [webCell.webView setNeedsLayout];
//        }
//    }
//}

#pragma mark - 分享方法
-(void)shareManager{
//    BYArticleShareImgController *shareImgController = [[BYArticleShareImgController alloc] init];
//    [shareImgController loadWebContent:self.articleRootModel.content];
//    [self.parentViewController addChildViewController:shareImgController];
//    [shareImgController didMoveToParentViewController:self.parentViewController];
//    [self.parentViewController.view addSubview:shareImgController.view];
////    [kCommonWindow addSubview:shareImgController.view];
//    return;
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];

    shareViewController.transferCopyUrl = self.articleRootModel.article_url;
    shareViewController.collection_status = self.articleRootModel.collection_status;
    shareViewController.hasJubao = YES;
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        thirdLoginType shareType;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
            } else if ([type isEqualToString:@"sina"]){
                shareType = thirdLoginTypeWeibo;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        } else {
            shareType = thirdLoginTypeWechat;
        }
        id smartImgUrl;
        if (strongSelf.articleRootModel.picture.count){
            smartImgUrl = [strongSelf.articleRootModel.picture firstObject];
        } else {
            smartImgUrl = strongSelf.articleRootModel.head_img;
        }
        
        id mainImgUrl;
        if ([smartImgUrl isKindOfClass:[NSString class]]){
            NSString *urlStr = (NSString *)smartImgUrl;
            NSString *baseURL = [PDImageView getUploadBucket:urlStr];
            if(![smartImgUrl hasPrefix:baseURL]){
                mainImgUrl = smartImgUrl;
            } else {
                NSString *nUrl = [[NSString stringWithFormat:@"%@%@",baseURL,urlStr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                mainImgUrl = nUrl;
            }
        } else if ([smartImgUrl isKindOfClass:[UIImage class]]){
            mainImgUrl = smartImgUrl;
        }
        
        NSString *title = @"";
        NSString *content = @"";
        
        
        if (strongSelf.articleRootModel.article_type == article_typeWeb || strongSelf.articleRootModel.article_type == article_typeInfo){
            title = strongSelf.articleRootModel.title;
            if (strongSelf.articleRootModel.subtitle.length && ![strongSelf.articleRootModel.subtitle isEqualToString:@"null"]){
                content = strongSelf.articleRootModel.subtitle;
            } else {
                content = @"最新最热区块链内容，尽在币本社区";
            }
        } else if (strongSelf.articleRootModel.article_type == article_typeNormal){
            title = strongSelf.articleRootModel.title.length?strongSelf.articleRootModel.title:strongSelf.articleRootModel.content;
            content = @"最新最热区块链内容，尽在币本社区";
        }
        [ShareSDKManager shareManagerWithType:shareType title:title desc:content img:mainImgUrl url:strongSelf.articleRootModel.article_url callBack:NULL];
        [ShareSDKManager shareSuccessBack:shareTypeArticle block:NULL];
    }];
    
    [shareViewController actionClickWithJubaoBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        ReportViewController *reportVC = [[ReportViewController alloc]init];
        [reportVC showInView:strongSelf.parentViewController type:BY_THEME_TYPE_ARTICLE itemId:strongSelf.articleRootModel._id];
    }];
    
    [shareViewController actionClickWithCopyBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;

        [Tool copyWithString:strongSelf.articleRootModel.article_url callback:NULL];
        [StatusBarManager statusBarHidenWithText:@"复制成功"];
    }];
    
    [shareViewController actionClickWithCollectionBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.articleRootModel.collection_status = !strongSelf.articleRootModel.collection_status ;
        [strongSelf.inputView collectionStatus:strongSelf.articleRootModel.collection_status];
    }];
    
    [shareViewController showInView:self.parentViewController];
}

#pragma mark - 关注
-(void)linkWIthCell:(ArticleDetailAvatarTableViewCell *)cell hasLink:(BOOL)link{
    NSString *userId = cell.transferArticleModel.author_id;
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]centerSendRequestToLinkManagerWithUserId:userId hasLink:link block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (link){
                [StatusBarManager statusBarHidenWithText:@"已关注"];
                [AccountModel sharedAccountModel].loginServerModel.infoCount.attention += 1;
            } else {
                [AccountModel sharedAccountModel].loginServerModel.infoCount.attention -= 1;
            }
            
            [cell btnStatusSelected:link];
            if (link){
                [strongSelf.navLinkButton setImage:[UIImage imageNamed:@"icon_article_link_nor"] forState:UIControlStateNormal];
            } else {
                [strongSelf.navLinkButton setImage:[UIImage imageNamed:@"icon_article_link_hlt"] forState:UIControlStateNormal];
            }
        }
        
        void(^block)(BOOL hasLink) = objc_getAssociatedObject(strongSelf, &actionReoloadFollowKey);
        if (block){
            block(link);
        }
    }];
}

#pragma mark 文章的顶和踩
-(void)actionClickManagerWithArticle{
    __weak typeof(self)weakSelf = self;

    [[NetworkAdapter sharedAdapter] articleDingAndCai:self.transferArticleId hasUp:YES block:^(BOOL isSuccessed, BOOL isOperate) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (!isOperate){
                [strongSelf ->biaoqianCell buttonActionClickManager];
                [strongSelf.inputView zanBtnStatus:YES];
                
                void(^block)() = objc_getAssociatedObject(strongSelf, &actionReoloadZanKey);
                if (block){
                    block(YES);
                }
            } else {
                [StatusBarManager statusBarHidenWithText:@"你已经顶过该条评论"];
            }
        }
    }];
}

#pragma mark - 文章的收藏
-(void)actionClickManagerWithCollection{
    __weak typeof(self)weakSelf = self;
    if (self.articleRootModel.collection_status){           // 表示已收藏
        // 取消收藏
        [[NetworkAdapter sharedAdapter] collectionCancelManager:self.transferArticleId themeType:CollectionModelThemetype1 actionBlock:^(BOOL isSuccessed) {
            if (!weakSelf){
                 return ;
             }
             __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.articleRootModel.collection_status = NO;
            [strongSelf.inputView collectionStatus:NO];
        }];
    } else {
        [[NetworkAdapter sharedAdapter] collectionManager:self.transferArticleId themeType:CollectionModelThemetype1 actionBlock:^(BOOL isSuccessed) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.articleRootModel.collection_status = YES;
            [strongSelf.inputView collectionStatus:YES];
        }];
    }
}


-(void)actionReoloadFollow:(void(^)(BOOL link))block{
    objc_setAssociatedObject(self, &actionReoloadFollowKey, block, OBJC_ASSOCIATION_COPY);
}

-(void)actionReoloadZan:(void(^)(BOOL link))block{
    objc_setAssociatedObject(self, &actionReoloadZanKey, block, OBJC_ASSOCIATION_COPY);
}

#pragma mark - ShowManager
-(void)actionClickShowManager{
    ArticleNewReleaseViewController *articleShowVC = [[ArticleNewReleaseViewController alloc]init];
    self.transition = [[ArticlePercentDrivenTransition alloc]initWithModalViewController:articleShowVC];
    self.transition.dragable = YES;//---是否可下拉收起
    articleShowVC.transitioningDelegate = self.transition;
    articleShowVC.modalPresentationStyle = UIModalPresentationCustom;
    // pop block
    [self presentViewController:articleShowVC animated:YES completion:nil];
}

#pragma mark - 创建nav
-(void)createNav{
    self.navBarView = [[UIView alloc]init];
    self.navBarView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [BYTabbarViewController sharedController].navBarHeight);
    self.navBarView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.navBarView];
    
    // nav
    UILabel *navBarLabel = [GWViewTool createLabelFont:@"16" textColor:@"黑"];
    navBarLabel.text = @"";
    CGSize navSize = [@"一二三四五" sizeWithCalcFont:navBarLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 40)];
    
    navBarLabel.frame = CGRectMake((kScreenBounds.size.width - navSize.width) / 2., [BYTabbarViewController sharedController].statusHeight, navSize.width, 44);
    self.navBarLabel = navBarLabel;
    [self.navBarView addSubview:navBarLabel];
//
    UIButton *backButton = [[UIButton alloc]init];
    backButton.frame = CGRectMake(LCFloat(11), [BYTabbarViewController sharedController].statusHeight, LCFloat(44), 44);
    [backButton setImage:[UIImage imageNamed:@"icon_main_back"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [backButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.navBarView addSubview:backButton];
    
    // moreBtn
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(44), [BYTabbarViewController sharedController].statusHeight, LCFloat(44), 44);
    [moreButton setImage:[UIImage imageNamed:@"icon_article_more"] forState:UIControlStateNormal];
    [moreButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
       [strongSelf shareManager];
    }];
    [self.navBarView addSubview:moreButton];
    
    // navAvatar
    self.navAvatarImgView = [[PDImageView alloc]init];
    self.navAvatarImgView.backgroundColor = [UIColor clearColor];
    self.navAvatarImgView.frame = CGRectMake(CGRectGetMaxX(backButton.frame) , [BYTabbarViewController sharedController].statusHeight + (40 - 30) / 2., 30, 30);
    self.navAvatarImgView.hidden = YES;
    [self.navBarView addSubview:self.navAvatarImgView];
    
    self.navTitleLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.navTitleLabel.font = [self.navTitleLabel.font boldFont];
    self.navTitleLabel.frame = CGRectMake(CGRectGetMaxX(self.navAvatarImgView.frame) + LCFloat(12), [BYTabbarViewController sharedController].statusHeight, LCFloat(200), 40);
    self.navTitleLabel.hidden = YES;
    [self.navBarView addSubview:self.navTitleLabel];
    
    self.navLinkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.navLinkButton.backgroundColor = [UIColor clearColor];
    self.navLinkButton.frame = CGRectMake(moreButton.orgin_x - LCFloat(11) - LCFloat(55), [BYTabbarViewController sharedController].statusHeight + (40 - LCFloat(22)) / 2., LCFloat(55), LCFloat(22));
    self.navLinkButton.hidden = YES;
    [self.navBarView addSubview:self.navLinkButton];
    [self.navLinkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf linkWIthCell:avatarCell hasLink:!strongSelf.articleRootModel.isAttention];
    }];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat point_y = scrollView.contentOffset.y;
    
    CGRect mainRect = [avatarCell convertRect:avatarCell.frame toView:self.articleDetailTableView];
    
    if (point_y > mainRect.origin.y){
        self.navAvatarImgView.hidden = NO;
        self.navTitleLabel.hidden = NO;
        self.navLinkButton.hidden = NO;
        self.navBarLabel.hidden = YES;
    } else {
        self.navBarLabel.hidden = NO;
        self.navAvatarImgView.hidden = YES;
        self.navTitleLabel.hidden = YES;
        self.navLinkButton.hidden = YES;
    }
}

@end
