//
//  ArticleDetailHuatiListViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/18.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleDetailHuatiListViewController.h"
#import "NetworkAdapter+Article.h"
#import "ArticleNewTagsTableViewCell.h"

static char actionClickWithSelectedBlockKey;
@interface ArticleDetailHuatiListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *huatiListTableView;
@property (nonatomic,strong)NSMutableArray *huatiMutableArr;
@property (nonatomic,strong)NSMutableArray *selectedMutableArr;

@end

@implementation ArticleDetailHuatiListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetListInfo];
}

-(void)setMaxCount:(NSInteger)maxCount{
    _maxCount = maxCount;
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"话题列表";
    
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_nav_success"] barHltImage:[UIImage imageNamed:@"icon_nav_success"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf actionClickManager];
    }];
}

-(void)actionClickManager{
    NSMutableArray *tempMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < self.selectedMutableArr.count;i++){
        ArticleDetailHuatiListSingleModel *singleModel = [self.selectedMutableArr objectAtIndex:i];
        [tempMutableArr addObject:singleModel.content];
    }
    
    void(^block)(NSArray *infoArr) = objc_getAssociatedObject(self, &actionClickWithSelectedBlockKey);
    if (block){
        block(tempMutableArr);
    }
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.huatiMutableArr = [NSMutableArray array];
    self.selectedMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.huatiListTableView){
        self.huatiListTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.huatiListTableView.dataSource = self;
        self.huatiListTableView.delegate = self;
        [self.view addSubview:self.huatiListTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0){
        return 1;
    } else {
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        ArticleNewTagsTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[ArticleNewTagsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        cellWithRowOne.transferHuatiArr = self.huatiMutableArr;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionItemClickBlock:^(ArticleDetailHuatiListSingleModel * _Nonnull model) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if ([strongSelf.selectedMutableArr containsObject:model]){
                 [strongSelf.selectedMutableArr removeObject:model];
                model.isLike = NO;
            } else {
                if (strongSelf.selectedMutableArr.count >= self.maxCount){
                    [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"最多选择%li个话题",self.maxCount]];
                    return;
                }
                 [strongSelf.selectedMutableArr addObject:model];
                model.isLike = YES;
            }
            
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
            [strongSelf.huatiListTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
        }];
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        cellWithRowOne.transferTitle = @"完成";
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf actionClickManager];
        }];
        
        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [ArticleNewTagsTableViewCell calculationCellHeightWithArr:self.huatiMutableArr];
    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1){
        return LCFloat(40);
    }
    return 0;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


#pragma mark - Info
-(void)sendRequestToGetListInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] sendRequestToGetHuatiListManagerBlock:^(BOOL isSuccessed, ArticleDetailHuatiListModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (strongSelf.huatiMutableArr.count){
                [strongSelf.huatiMutableArr removeAllObjects];
            }
            [strongSelf.huatiMutableArr addObjectsFromArray:listModel.content];
            
            for (int i = 0 ; i < strongSelf.transferSelectedHuatiArr.count;i++){
                NSString *selectedStr = [strongSelf.transferSelectedHuatiArr objectAtIndex:i];
                for (int j = 0 ; j < listModel.content.count;j++){
                    ArticleDetailHuatiListSingleModel *singleModel = [listModel.content objectAtIndex:j];
                    if ([singleModel.content isEqualToString:selectedStr]){
                        singleModel.isLike = YES;
                        [strongSelf.selectedMutableArr addObject:singleModel];
                    }
                }
            }
            
            
            [strongSelf.huatiListTableView reloadData];
            
            if (strongSelf.huatiMutableArr.count){
                [strongSelf.huatiListTableView dismissPrompt];
            } else {
                [strongSelf.huatiListTableView showPrompt:@"当前没有话题" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }
        } else {
            
        }
    }];
}


-(void)actionClickWithSelectedBlock:(void(^)(NSArray *selectedArr))block{
    objc_setAssociatedObject(self, &actionClickWithSelectedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
