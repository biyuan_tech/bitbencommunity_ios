//
//  ArticleRootViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/9.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleRootViewController.h"
#import "ArticleRootTableViewCell.h"
#import "ArticleRootActionTableViewCell.h"
#import "ArticleReleaseRootViewController.h"
#import "NetworkAdapter+Article.h"
#import "GWAssetsImgSelectedViewController.h"
#import "ArticleDetailRootViewController.h"
#import "BYPersonHomeController.h"
#import "NetworkAdapter+Center.h"
#import "SegmentListTitleDropView.h"
#import "ArticleNewRootSubViewController.h"
#import "RankingRootViewController.h"


@interface ArticleRootViewController (){
    ArticleListType selectedArticleType;
}
@property (nonatomic,strong)PDSelectionListTitleView *segmentList;
@property (nonatomic,strong)NSArray *segmentArr;

@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)UITableView *linkArticleTableView;
@property (nonatomic,strong)NSMutableArray *linkArticleMutableArr;

@property (nonatomic,strong)UITableView *articleTableView;
@property (nonatomic,strong)NSMutableArray *articleMutableArr;
@property (nonatomic,strong)NSMutableArray *segmentListMutableArr;

@property (nonatomic,strong)ArticleNewRootSubViewController *tableView1;
@property (nonatomic,strong)ArticleNewRootSubViewController *tableView2;
@property (nonatomic,strong)ArticleNewRootSubViewController *tableView3;

@end


@implementation ArticleRootViewController

+(instancetype)sharedController{
    static ArticleRootViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[ArticleRootViewController alloc] init];
    });
    return _sharedAccountModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createScrollView];
    [self createTableView];
    if (self.transferType != ArticleRootViewControllerTypeMine){
        [self createSegment];
    } else {
        self.mainScrollView.scrollEnabled = NO;
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.view.backgroundColor = RGB(245, 245, 245, 1);
    if (self.transferType == ArticleRootViewControllerTypeMine){
        self.barMainTitle = @"我的文章";
    } else if (self.transferType == ArticleRootViewControllerTypeNormal){
        self.barMainTitle = @"观点";
    } else if (self.transferType == ArticleRootViewControllerTypeTags){
        if (self.transferTags.length){
            self.barMainTitle = self.transferTags;
        }
    }
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    selectedArticleType = ArticleListTypeTuijian;
    self.articleMutableArr = [NSMutableArray array];
    
    self.segmentArr = @[@"推荐",@"热门",@"新鲜"];
    
    self.linkArticleMutableArr = [NSMutableArray array];
    self.segmentListMutableArr = [NSMutableArray array];
}

#pragma mark - 创建segment
-(void)createSegment{
    __weak typeof(self)weakSelf = self;
    PDSelectionListTitleView *segment = (PDSelectionListTitleView *)[PDSelectionListTitleView createSegmentWithDataSource:self.segmentArr actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf segmentListSelectedIndex:index];
    }];
                                         
    self.segmentList = segment;
    self.navigationItem.titleView = self.segmentList;
}

-(void)segmentListSelectedIndex:(NSInteger)index{
    [self.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width * index, 0) animated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self pageReloadManager:index];
    });
}

#pragma mark - UIScrollView
-(void)createScrollView{
    __weak typeof(self)weakSelf = self;
    if (!self.mainScrollView){
        self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSInteger page = scrollView.contentOffset.x / kScreenBounds.size.width;
            [strongSelf.segmentList setSelectedButtonIndex:page animated:YES];
            [strongSelf pageReloadManager:page];
        }];

        self.mainScrollView.scrollEnabled = YES;
        self.mainScrollView.contentSize = CGSizeMake(self.segmentArr.count * kScreenBounds.size.width, self.mainScrollView.size_height);
        [self.view addSubview:self.mainScrollView];
        
    
        if(self.navigationController.childViewControllers.count == 1){
            self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - [BYTabbarViewController sharedController].tabBarHeight - [BYTabbarViewController sharedController].navBarHeight);
        } else {
            self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - [BYTabbarViewController sharedController].navBarHeight);
        }
        
        if (self.transferType == ArticleRootViewControllerTypeMine){
            self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - [BYTabbarViewController sharedController].navBarHeight);
        } else {
            self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - [BYTabbarViewController sharedController].tabBarHeight - [BYTabbarViewController sharedController].navBarHeight);
        }
    }
}

-(void)createTableView{
    self.tableView1 = [[ArticleNewRootSubViewController alloc]init];
    self.tableView1.transferType = ArticleListTypeTuijian;
    self.tableView1.view.frame = self.mainScrollView.bounds;
    self.tableView1.view.backgroundColor = [UIColor clearColor];
    [self.mainScrollView addSubview:self.tableView1.view];
    [self addChildViewController:self.tableView1];
    
    self.tableView2 = [[ArticleNewRootSubViewController alloc]init];
    self.tableView2.transferType = ArticleListTypeHot;
    self.tableView2.view.frame = self.mainScrollView.bounds;
    self.tableView2.view.orgin_x = kScreenBounds.size.width;
    self.tableView2.view.backgroundColor = [UIColor clearColor];
    [self.mainScrollView addSubview:self.tableView2.view];
    [self addChildViewController:self.tableView2];
    
    self.tableView3 = [[ArticleNewRootSubViewController alloc]init];
    self.tableView3.transferType = ArticleListTypeNew;
    self.tableView3.view.frame = self.mainScrollView.bounds;
    self.tableView3.view.orgin_x = 2 * kScreenBounds.size.width;
    self.tableView3.view.backgroundColor = [UIColor clearColor];
    [self.mainScrollView addSubview:self.tableView3.view];
    [self addChildViewController:self.tableView3];
    
    // 进行刷新
    [self.tableView1 startLoadInterfaceManager];
}

#pragma mark - 页面切换进行数据刷新
-(void)pageReloadManager:(NSInteger)page{
    if (page == 0){
        [self.tableView1 startLoadInterfaceManager];
    } else if (page == 1){
        [self.tableView2 startLoadInterfaceManager];
    } else if (page == 2){
        [self.tableView3 startLoadInterfaceManager];
    }
}




#pragma mark - 接口
-(void)sendRequestToGetListManager:(BOOL)hasLoad{
    __weak typeof(self)weakSelf = self;
    NSString *authorId = self.transferType == ArticleRootViewControllerTypeMine ? [AccountModel sharedAccountModel].account_id:@"";
    if(!self.articleMutableArr.count){
        [self.articleTableView showListLoadingAnimation];
    }
    [[NetworkAdapter sharedAdapter] articleListWithPage:self.articleTableView.currentPage topic:self.transferTags type:selectedArticleType authorId:authorId block:^(NSArray<ArticleRootSingleModel> *articleList) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.articleTableView.isXiaLa || hasLoad){       // 下拉
            [strongSelf.articleTableView dismissListAnimation];
            [strongSelf.articleMutableArr removeAllObjects];
        }
        
        [strongSelf.articleMutableArr addObjectsFromArray:articleList];
        
        
        if (strongSelf.articleTableView.isXiaLa){
            [strongSelf.articleTableView stopPullToRefresh];
        } else {
            [strongSelf.articleTableView stopFinishScrollingRefresh];
        }
        if (articleList.count){
            [strongSelf.articleTableView reloadData];
        }

        if (strongSelf.articleMutableArr.count){
            [strongSelf.articleTableView dismissPrompt];
            [strongSelf.articleTableView dismissListAnimation];

        } else {
            [strongSelf.articleTableView showPrompt:@"当前没有文章" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
        }
    }];
}


#pragma mark - 获取我关注的
-(void)sendRequestToGetMyLinkArticleWithReload:(BOOL)reload{
    __weak typeof(self)weakSelf = self;
    if (!self.linkArticleMutableArr.count){
        [self.linkArticleTableView showListLoadingAnimation];
    }

    [[NetworkAdapter sharedAdapter]articleListWithPage:self.linkArticleTableView.currentPage block:^(NSArray<ArticleRootSingleModel> *articleList) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.linkArticleTableView.isXiaLa || reload){
            [strongSelf.linkArticleMutableArr removeAllObjects];
        }

        if (strongSelf.linkArticleTableView.isXiaLa){
            [strongSelf.linkArticleTableView stopPullToRefresh];
        } else {
            [strongSelf.linkArticleTableView stopFinishScrollingRefresh];
        }

        
        if (articleList.count){
            [strongSelf.linkArticleMutableArr addObjectsFromArray:articleList];
            [strongSelf.linkArticleTableView reloadData];
        }
        
        if (strongSelf.linkArticleMutableArr.count){
            [strongSelf.linkArticleTableView dismissPrompt];
            [strongSelf.linkArticleTableView dismissListAnimation];

        } else {
            [strongSelf.linkArticleTableView showPrompt:@"当前没有文章信息" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
        }
    }];
}



//
//#pragma makr - UITableView
//-(void)createTableView{
//    if (!self.articleTableView){
//        self.articleTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
//        self.articleTableView.dataSource = self;
//        self.articleTableView.delegate = self;
//        [self.mainScrollView addSubview:self.articleTableView];
//    }
//
//    __weak typeof(self)weakSelf = self;
//    [self.articleTableView appendingPullToRefreshHandler:^{
//        if(!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        [strongSelf sendRequestToGetListManager:YES];
//    }];
//
//    [self.articleTableView appendingFiniteScrollingPullToRefreshHandler:^{
//        if(!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        [strongSelf sendRequestToGetListManager:NO];
//    }];
//
//    // 获取我的关注
//    if (!self.linkArticleTableView){
//        self.linkArticleTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
//        self.linkArticleTableView.orgin_x = kScreenBounds.size.width;
//        self.linkArticleTableView.dataSource = self;
//        self.linkArticleTableView.delegate = self;
//        [self.mainScrollView addSubview:self.linkArticleTableView];
//    }
//    [self.linkArticleTableView appendingPullToRefreshHandler:^{
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        [strongSelf sendRequestToGetMyLinkArticleWithReload:YES];
//    }];
//    [self.linkArticleTableView appendingFiniteScrollingPullToRefreshHandler:^{
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        [strongSelf sendRequestToGetMyLinkArticleWithReload:NO];
//    }];
//}
//
//#pragma mark - UITableViewDataSource
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    if (tableView == self.articleTableView){
//        return self.articleMutableArr.count;
//    } else if (tableView == self.linkArticleTableView){
//        return self.linkArticleMutableArr.count;
//    }
//    return self.articleMutableArr.count;
//}
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return 2;
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.row == 0){
//        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
//        ArticleRootTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
//        if (!cellWithRowOne){
//            cellWithRowOne = [[ArticleRootTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
//        }
//        ArticleRootSingleModel *singleModel;
//        if (tableView == self.articleTableView){
//            singleModel = [self.articleMutableArr objectAtIndex:indexPath.section];
//        } else if (tableView == self.linkArticleTableView){
//            singleModel = [self.linkArticleMutableArr objectAtIndex:indexPath.section];
//        }
//
//        cellWithRowOne.transferSingleModel = singleModel;
//        __weak typeof(self)weakSelf = self;
//        [cellWithRowOne actionClickWithImgSelectedBlock:^(NSString *imgUrl) {
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            [strongSelf tableView:tableView didSelectRowAtIndexPath:indexPath];
//        }];
//
//        [cellWithRowOne actionClickWithTagsSelectedBlock:^(NSString *tag) {
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            ArticleRootViewController *articleRootVC = [[ArticleRootViewController alloc]init];
//            articleRootVC.transferType = ArticleRootViewControllerTypeTags;
//            articleRootVC.transferTags = tag;
//            [strongSelf.navigationController pushViewController:articleRootVC animated:YES];
//        }];
//
//        [cellWithRowOne actionClickWithLinkButtonBlock:^{
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//
//            [strongSelf authorizeWithCompletionHandler:^(BOOL successed) {
//                if (successed){
//                    [strongSelf linkWIthCell:cellWithRowOne hasLink:!singleModel.isAttention];
//                }
//            }];
//        }];
//
//        [cellWithRowOne actionClickHeaderImgWithBlock:^{
//            if (!weakSelf){
//                return ;
//            }
//            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
//            personHomeController.user_id = cellWithRowOne.transferSingleModel.author_id;
//            [weakSelf.navigationController pushViewController:personHomeController animated:YES];
//        }];
//
//        return cellWithRowOne;
//    } else if (indexPath.row == 1){
//        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
//        ArticleRootActionTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
//        if (!cellWithRowTwo){
//            cellWithRowTwo = [[ArticleRootActionTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
//        }
//        ArticleRootSingleModel *singleModel;
//        if (tableView == self.articleTableView){
//            singleModel = [self.articleMutableArr objectAtIndex:indexPath.section];
//        } else if (tableView == self.linkArticleTableView){
//            singleModel = [self.linkArticleMutableArr objectAtIndex:indexPath.section];
//        }
//        cellWithRowTwo.transferSingleModel = singleModel;
//        return cellWithRowTwo;
//    }
//    return nil;
//}
//
//#pragma mark - UITableViewDelegate
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//
//    ArticleDetailRootViewController *viewController = [[ArticleDetailRootViewController alloc]init];
//    viewController.transferPageType = ArticleDetailRootViewControllerTypeArticle;
//    ArticleRootSingleModel *singleModel;
//    if (tableView == self.articleTableView){
//        singleModel = [self.articleMutableArr objectAtIndex:indexPath.section];
//    } else if (tableView == self.linkArticleTableView){
//        singleModel = [self.linkArticleMutableArr objectAtIndex:indexPath.section];
//    }
//
//    __weak typeof(self)weakSelf = self;
//    [viewController actionReoloadFollow:^(BOOL link) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        // 遍历所有的Model 只要是同一个作者的都备注已关注
//        for (int i = 0 ; i < strongSelf.articleMutableArr.count;i++){
//            ArticleRootSingleModel *tempSingleModel = [strongSelf.articleMutableArr objectAtIndex:i];
//            if ([tempSingleModel.author_id isEqualToString:singleModel.author_id]){
//                tempSingleModel.isAttention = link;
//            }
//        }
//        [strongSelf.articleTableView reloadData];
//    }];
//
//    viewController.transferArticleModel = singleModel;
//    [self.navigationController pushViewController:viewController animated:YES];
//}
//
//
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.row == 0){
//        ArticleRootSingleModel *singleModel;
//        if (tableView == self.articleTableView){
//            singleModel = [self.articleMutableArr objectAtIndex:indexPath.section];
//        } else if (tableView == self.linkArticleTableView){
//            singleModel = [self.linkArticleMutableArr objectAtIndex:indexPath.section];
//        }
//        return [ArticleRootTableViewCell calculationCellHeightWithModel:singleModel];
//    } else {
//        return LCFloat(50);
//    }
//}
//
//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (self.articleTableView) {
//        SeparatorType separatorType = SeparatorTypeMiddle;
//        if (indexPath.row == 0){
//            separatorType  = SeparatorTypeBottom;
//            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"message"];
//        }
//    }
//}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return LCFloat(10);
//}
//
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UIView *headerView = [[UIView alloc]init];
//    headerView.backgroundColor = [UIColor clearColor];
//    return headerView;
//}
@end
