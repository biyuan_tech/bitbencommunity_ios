//
//  ArticleRootViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/9.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,ArticleRootViewControllerType) {
    ArticleRootViewControllerTypeNormal,                /**< tabbar*/
    ArticleRootViewControllerTypeTags,                  /**< 标签*/
    ArticleRootViewControllerTypeMine,                  /**< 我的*/
};

@interface ArticleRootViewController : AbstractViewController

+(instancetype)sharedController;
@property (nonatomic,copy)NSString *transferTags;                                           /**< 传递的是关键字*/

@property (nonatomic,assign)ArticleRootViewControllerType transferType;                     /**< 传入的类型*/

@end
