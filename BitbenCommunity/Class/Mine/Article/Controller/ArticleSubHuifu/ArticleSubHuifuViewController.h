//
//  ArticleSubHuifuViewController.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveFourumRootListModel.h"
#import "LiveForumRootViewController.h"
#import "NetworkAdapter+PPTLive.h"

@class ArticleSubHuifuViewController;

NS_ASSUME_NONNULL_BEGIN

@interface ArticleSubHuifuViewController : AbstractViewController
@property (nonatomic,strong)LiveFourumRootListSingleModel *transferFourumRootModel;             /**< 上个页面传入的Model*/
@property (nonatomic,assign)LiveForumDetailViewControllerType transferPageType;
@property (nonatomic,copy)NSString *transferRoomId;
@property (nonatomic,strong)UITableView *dianpingDetailTableView;
@property (nonatomic,assign)BOOL loadHasEnd;
-(void)actionClickWithLinkBlock:(void(^)())block;
-(void)loadInfoManager;
@end

NS_ASSUME_NONNULL_END
