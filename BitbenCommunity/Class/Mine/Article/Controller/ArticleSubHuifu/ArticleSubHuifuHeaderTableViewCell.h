//
//  ArticleSubHuifuHeaderTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "LiveFourumRootListModel.h"
#import "LiveForumRootDingView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleSubHuifuHeaderTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)LiveFourumRootListSingleModel *transferInfoManager;
@property (nonatomic,strong)UIButton *zanButton;

+(CGFloat)calculationCellHeightWithModel:(LiveFourumRootListSingleModel *)model;
-(void)actionClickHeaderImgWithBlock:(void(^)(void))block;                  /**< 头像点击*/
-(void)actionClickLinkWithBlock:(void(^)(void))block;                  /**< 关注点击*/
-(void)actionClickZanWithBlock:(void(^)(void))block;                  /**< 赞点击*/

-(void)btnStatusSelected:(BOOL)selected;


@end

NS_ASSUME_NONNULL_END
