//
//  ArticleSubHuifuViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleSubHuifuViewController.h"
#import "LiveForumRootSingleTableViewCell.h"
#import "LiveForumItemsTableViewCell.h"
#import "LiveForumDetailTableViewCell.h"
#import "NetworkAdapter+PPTLive.h"
#import "LiveForumRootInputView.h"
#import "BYPersonHomeController.h"
#import "ArticleSubHuifuHeaderTableViewCell.h"
#import "LiveForumRootSingleTableViewCell.h"
#import "NetworkAdapter+Center.h"

@interface ArticleSubHuifuViewController()<UITableViewDelegate,UITableViewDataSource>{
    CGRect newKeyboardRect;
}
@property (nonatomic,strong)PDImageView *headerImgView;
@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)NSMutableArray *dianpingMutableArr;
@property (nonatomic,strong)NSMutableArray *dianpingRootMutableArr;
@property (nonatomic,strong)LiveForumRootInputView *inputView;
@end

@implementation ArticleSubHuifuViewController


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createHeaderView];
    [self createInputView];
    [self createTableView];
    [self sendRequestToGetInfoWithReReload:NO];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"点评详情";
    self.view.backgroundColor = [UIColor clearColor];
}

#pragma mark - createView
-(void)createHeaderView{
    self.headerImgView = [[PDImageView alloc]init];
    self.headerImgView.frame = CGRectMake(0, [BYTabbarViewController sharedController].statusHeight, kScreenBounds.size.width, LCFloat(44));
    self.headerImgView.image = [Tool stretchImageWithName:@"bg_comment_huifuHeader"];
    self.headerImgView.userInteractionEnabled = YES;
    [self.view addSubview:self.headerImgView];
    
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.frame = CGRectMake(0, 0, self.headerImgView.size_height, self.headerImgView.size_height);
    __weak typeof(self)weakSelf = self;
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
    }];
    [self.leftButton setImage:[UIImage imageNamed:@"icon_main_cancel"] forState:UIControlStateNormal];
    [self.headerImgView addSubview:self.leftButton];

    // title
    self.titleLabel = [GWViewTool createLabelFont:@"16" textColor:@"黑"];
    self.titleLabel.frame = CGRectMake(LCFloat(44), 0, kScreenBounds.size.width - 2 * LCFloat(44), self.headerImgView.size_height);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.text = @"0条回复";
    [self.headerImgView addSubview:self.titleLabel];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(0, self.headerImgView.size_height - .5f, kScreenBounds.size.width, .5f);
    [self.headerImgView addSubview:lineView];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.dianpingRootMutableArr = [NSMutableArray array];
    [self.dianpingRootMutableArr addObject:@[self.transferFourumRootModel]];
    
    self.dianpingMutableArr = [NSMutableArray array];
    [self.dianpingRootMutableArr addObject:self.dianpingMutableArr];
}

-(UITableView *)dianpingDetailTableView{
    if (!_dianpingDetailTableView){
        _dianpingDetailTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        _dianpingDetailTableView.backgroundColor = [UIColor whiteColor];
        _dianpingDetailTableView.dataSource = self;
        _dianpingDetailTableView.delegate = self;
        _dianpingDetailTableView.scrollEnabled = NO;
    }
    return _dianpingDetailTableView;
}

#pragma mark - UITableView
-(void)createTableView{
    self.dianpingDetailTableView.frame = self.view.bounds;
    self.dianpingDetailTableView.orgin_y = CGRectGetMaxY(self.headerImgView.frame);
    self.dianpingDetailTableView.size_height = self.inputView.orgin_y - CGRectGetMaxY(self.headerImgView.frame);
    [self.view addSubview:_dianpingDetailTableView];

    
    
    [self.view addSubview:self.inputView];
}

#pragma mark 上拉加载
-(void)loadInfoManager{
    self.dianpingDetailTableView.currentPage++;
    self.dianpingDetailTableView.isXiaLa = NO;
    [self sendRequestToGetInfoWithReReload:NO];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dianpingRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.dianpingRootMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0 && indexPath.row == 0){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        ArticleSubHuifuHeaderTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[ArticleSubHuifuHeaderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
        }
        LiveFourumRootListSingleModel *singleModel = self.transferFourumRootModel;
        cellWithRowTwo.transferInfoManager = singleModel;
        
        __weak typeof(self)weakSelf = self;
        // 1. 头像点击
        [cellWithRowTwo actionClickHeaderImgWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = singleModel.user_id;
            [strongSelf.navigationController pushViewController:personHomeController animated:YES];
        }];
        
        // 关注
        [cellWithRowTwo actionClickLinkWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf linkWIthCell:cellWithRowTwo hasLink:!strongSelf.transferFourumRootModel.isAttention];
        }];
        
        
        [cellWithRowTwo actionClickZanWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf actionClickManagerWithInterfaceWithCell:cellWithRowTwo];
        }];
        
        return cellWithRowTwo;
    } else {
        id info = [self.dianpingMutableArr objectAtIndex:indexPath.row];
        
        if ([info isKindOfClass:[NSString class]]){
            static NSString *cellIdentifyWithRowAllComment = @"cellIdentifyWithRowAllComment";
            GWNormalTableViewCell *cellWithRowComment = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowAllComment];
            if (!cellWithRowComment){
                cellWithRowComment = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowAllComment];
            }
            cellWithRowComment.transferCellHeight = cellHeight;
            cellWithRowComment.transferTitle = @"全部评论";
            
            return cellWithRowComment;
        } else if ([info isKindOfClass:[LiveFourumRootListSingleModel class]]){
            static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
            LiveForumRootSingleTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[LiveForumRootSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            cellWithRowTwo.transferItemId = self.transferRoomId;
            cellWithRowTwo.hasHuifu = YES;
            LiveFourumRootListSingleModel *singleModel = [[self.dianpingRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            cellWithRowTwo.transferInfoManager = singleModel;

            __weak typeof(self)weakSelf = self;
            // 赞
            [cellWithRowTwo actionClickZanManagerBlock:^{

            }];
            
            // 头像点击
            [cellWithRowTwo actionClickHeaderImgWithBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
                personHomeController.user_id = singleModel.user_id;
                [strongSelf.navigationController pushViewController:personHomeController animated:YES];
            }];
            
//            // 回复
            [cellWithRowTwo actionClickReplaceManagerBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                strongSelf.inputView.transferListModel = singleModel;
                strongSelf.inputView.transferListModel.transferTempCommentId = strongSelf.transferFourumRootModel.comment_id;

                if (![strongSelf.inputView.inputView isFirstResponder]){
                    [strongSelf.inputView.inputView becomeFirstResponder];
                }
//                strongSelf.inputView.inputView.text = @"";

//                strongSelf.inputView.inputView.placeholder = [NSString stringWithFormat:@"回复：%@",singleModel.comment_name];
            }];
            
            return cellWithRowTwo;
        } else {
            static NSString *cellIdentifyWithRowNormal = @"cellIdentifyWithRowNormal";
            GWNormalTableViewCell *cellWithRowNormal = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowNormal];
            if (!cellWithRowNormal){
                cellWithRowNormal = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowNormal];
            }
            return cellWithRowNormal;
        }
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0){
        LiveFourumRootListSingleModel *singleModel = [[self.dianpingRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        return [ArticleSubHuifuHeaderTableViewCell calculationCellHeightWithModel:singleModel];
    } else {
        id info = [self.dianpingMutableArr objectAtIndex:indexPath.row];
        
        if ([info isKindOfClass:[NSString class]]){
            return [GWNormalTableViewCell calculationCellHeight];
        } else {
            LiveFourumRootListSingleModel *singleModel = [[self.dianpingRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            return [LiveForumRootSingleTableViewCell calculationCellHeightWithModel:singleModel];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return LCFloat(0);
    } else {
        return .5f;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor hexChangeFloat:@"F4F4F4"];
    return headerView;
}

#pragma mark - 创建输入框
-(void)createInputView{
    if (!self.inputView){
        CGRect inputFrame = CGRectMake(0, kScreenBounds.size.height - [LiveForumRootInputView calculationHeight:@""] , kScreenBounds.size.width, [LiveForumRootInputView  calculationHeight:@""]);
        self.inputView = [[LiveForumRootInputView alloc]initWithFrame:inputFrame];
        self.inputView.transferListModel = self.transferFourumRootModel;
        self.inputView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        self.inputView.layer.shadowOpacity = .2f;
        self.inputView.layer.shadowOffset = CGSizeMake(.2f, .2f);
        
        [self.view addSubview:self.inputView];
    }
    __weak typeof(self)weakSelf = self;
    [self.inputView actionClickWithSendInfoBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToSendInfo];
    }];
    [self.inputView actionTextInputChangeBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.inputView.orgin_y = strongSelf.view.size_height - strongSelf->newKeyboardRect.size.height - [LiveForumRootInputView calculationHeight:strongSelf.inputView.inputView.text] ;
    }];
}


#pragma mark - Interface
-(void)sendRequestToGetInfoWithReReload:(BOOL)rereload{
    if (!self.inputView.transferListModel){
        self.inputView.transferListModel = self.transferFourumRootModel;
    }
    NSInteger pageNum = rereload ? 0 : self.dianpingDetailTableView.currentPage;
    
    NSDictionary *params = @{@"theme_id":self.transferRoomId,@"theme_type":@(self.transferPageType),@"reply_comment_id":self.inputView.transferListModel.comment_id,@"page_number":@(pageNum),@"page_size":@"10"};
    
    __weak typeof(self)weakSelf = self;
    
    if (rereload == NO){
        self.loadHasEnd = NO;
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:page_comment requestParams:params responseObjectClass:[LiveFourumRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            self.loadHasEnd = YES;
            LiveFourumRootListModel *singleModel = (LiveFourumRootListModel *)responseObject;
            if (strongSelf.dianpingDetailTableView.isXiaLa || rereload){
                [strongSelf.dianpingMutableArr removeAllObjects];
            }
            
            if (singleModel.content.count){
                if(![strongSelf.dianpingMutableArr containsObject:@"全部评论"]){
                    [strongSelf.dianpingMutableArr addObject:@"全部评论"];
                    [strongSelf.dianpingMutableArr addObjectsFromArray:singleModel.content];
                } else {
                    [strongSelf.dianpingMutableArr addObjectsFromArray:singleModel.content];
                }
            }

            
            [strongSelf.dianpingDetailTableView reloadData];
            
            if (strongSelf.dianpingDetailTableView.isXiaLa){
                [strongSelf.dianpingDetailTableView stopPullToRefresh];
            } else {
                [strongSelf.dianpingDetailTableView stopFinishScrollingRefresh];
            }
            
            
            if (strongSelf.dianpingRootMutableArr.count){
                [strongSelf.dianpingDetailTableView dismissPrompt];
            } else {
                [strongSelf.dianpingDetailTableView showPrompt:@"当前没有数据哦" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
            
            // 回复数量
            if ([strongSelf.dianpingMutableArr containsObject:@"全部回复"]){
                strongSelf.titleLabel.text = [NSString stringWithFormat:@"%li条回复",strongSelf.dianpingMutableArr.count - 1];
            } else {
                strongSelf.titleLabel.text = [NSString stringWithFormat:@"%li条回复",strongSelf.dianpingMutableArr.count];
            }
            
        }
    }];
}

#pragma mark - 发送信息
-(void)sendRequestToSendInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] liveCreateCommentWiththemeId:self.transferRoomId type:self.transferPageType ThemeInfo:self.inputView.transferListModel content:self.inputView.inputView.text block:^(BOOL isSuccessed ,NSString *commentId) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            strongSelf.inputView.transferListModel = strongSelf.transferFourumRootModel;

            // 插入信息
            LiveFourumRootListSingleModel *singleModel = [[LiveFourumRootListSingleModel alloc]init];
            singleModel.comment_name = [AccountModel sharedAccountModel].loginServerModel.user.nickname;
            singleModel.comment_id = commentId;
            singleModel.comment_pic = [AccountModel sharedAccountModel].loginServerModel.account.head_img;
            singleModel.content = strongSelf.inputView.inputView.text;
            singleModel.count_reply = 0;
            singleModel.count_support = 0;
            singleModel.count_tread = 0;
            singleModel.create_time = [NSDate getNSTimeIntervalWithCurrent] * 1000;
            singleModel.isAttention = 0;
            singleModel.isSupport = 0;
            singleModel.isTread = 0;
            singleModel.reply_comment_id = strongSelf.inputView.transferListModel.comment_id;
            singleModel.reply_comment_name = strongSelf.inputView.transferListModel.comment_name;
            singleModel.reply_comment_pic = strongSelf.inputView.transferListModel.comment_pic;
            singleModel.reply_user_id = strongSelf.inputView.transferListModel.reply_user_id;
            singleModel.theme_id = strongSelf.transferRoomId;
            singleModel.theme_type = @"1";
            singleModel.reward = @"待揭晓";
            singleModel.user_id = [AccountModel sharedAccountModel].account_id;
            if ([strongSelf.dianpingMutableArr containsObject:@"全部评论"]){
                NSInteger row = [strongSelf.dianpingMutableArr indexOfObject:@"全部评论"];
                [strongSelf.dianpingMutableArr insertObject:singleModel atIndex:row + 1];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row + 1 inSection:1];
                [strongSelf.dianpingDetailTableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
                // 修改回复数量
                strongSelf.titleLabel.text = [NSString stringWithFormat:@"%li条回复",(long)strongSelf.dianpingMutableArr.count - 1];
            } else {
                [strongSelf.dianpingMutableArr addObject:@"全部评论"];
                NSInteger row = [strongSelf.dianpingMutableArr indexOfObject:@"全部评论"];
                [strongSelf.dianpingMutableArr insertObject:singleModel atIndex:row + 1];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:1];
                [strongSelf.dianpingDetailTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
            }
            [strongSelf.inputView cleanKeyboard];
            strongSelf.inputView.inputView.placeholder = @"请输入评论";
        }
    }];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.inputView releaseKeyboard];
}

#pragma mark - 输入框释放
-(void)releaseKeyboard {
    [self.inputView cleanKeyboard];
}


#pragma mark - 关注
-(void)linkWIthCell:(ArticleSubHuifuHeaderTableViewCell *)cell hasLink:(BOOL)link{
    NSString *userId = self.transferFourumRootModel.user_id;
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]centerSendRequestToLinkManagerWithUserId:userId hasLink:link block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        if (isSuccessed){
            if (link){
                [StatusBarManager statusBarHidenWithText:@"已关注"];
                [AccountModel sharedAccountModel].loginServerModel.infoCount.attention += 1;
            } else {
                [AccountModel sharedAccountModel].loginServerModel.infoCount.attention -= 1;
            }
            
            [cell btnStatusSelected:link];
        }
    }];
}

#pragma mark - 点赞
-(void)actionClickManagerWithInterfaceWithCell:(ArticleSubHuifuHeaderTableViewCell *)cell{
    __weak typeof(self)weakSelf = self;
    BOOL hasUp = NO;
    if (!self.transferFourumRootModel.isSupport){
        hasUp = YES;
    } else {
        hasUp = NO;
    }
    
    
    [[NetworkAdapter sharedAdapter] liveComment_OperateWithCommentID:self.transferFourumRootModel._id itemId:self.transferRoomId theme_type:LiveForumDetailViewControllerTypeArticle hasUp:YES block:^(BOOL isSuccessed, BOOL isOperate) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (!isOperate){
                strongSelf.transferFourumRootModel.isSupport = YES;
                self.transferFourumRootModel.count_support +=1;
                [cell.zanButton setTitle:[NSString stringWithFormat:@"%li",strongSelf.transferFourumRootModel.count_support] forState:UIControlStateNormal];
                [cell.zanButton setImage:[UIImage imageNamed:@"icon_article_detail_tags_zan_hlt"] forState:UIControlStateNormal];
            } else {
                //                if (strongSelf.transferInfoManager.count_support - 1 <= 0){
                //                    [strongSelf.zanButton setTitle:@"赞" forState:UIControlStateNormal];
                //                } else {
                //                    [strongSelf.zanButton setTitle:[NSString stringWithFormat:@"%li",strongSelf.transferInfoManager.count_support - 1] forState:UIControlStateNormal];
                //                }
                //                [strongSelf.zanButton setImage:[UIImage imageNamed:@"icon_article_detail_bottom_zan_nor"] forState:UIControlStateNormal];
            }
        }
    }];
}



//-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
//CGPoint offset = scrollView.contentOffset;
//    NSLog(@"%.2f",offset.y);
//    if (scrollView == self.dianpingDetailTableView){
//
//        if (offset.y <= 0) {
//            offset.y = 0;
//        }
//        scrollView.contentOffset = offset;
//    }
//}
@end
