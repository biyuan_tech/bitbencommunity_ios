//
//  ArticleSubHuifuHeaderTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/15.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleSubHuifuHeaderTableViewCell.h"
#import "NSAttributedString_Encapsulation.h"

static char actionClickHeaderImgWithBlockKey;
static char actionClickLinkWithBlockKey;
static char actionClickZanWithBlockKey;
@interface ArticleSubHuifuHeaderTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)PDImageView *convertView;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *timeLabel;

@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UIButton *linkButton;
@property (nonatomic,strong)PDImageView *bpImgView;
@property (nonatomic,strong)UILabel *bpLabel;
/** 承载成就图标 */
@property (nonatomic ,strong) UIView *achievementView;


@end

@implementation ArticleSubHuifuHeaderTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.avatarImgView addTapGestureRecognizer:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickHeaderImgWithBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.avatarImgView];
    
    // 2. 头像遮罩
    self.convertView = [[PDImageView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.image = [UIImage imageNamed:@"icon_live_avatar_convert"];
    self.convertView.frame = CGRectMake(0, 0, LCFloat(67), LCFloat(67));
    [self addSubview:self.convertView];
    
    // 3. 昵称
    self.nickNameLabel = [GWViewTool createLabelFont:@"15" textColor:@"323232"];
    self.nickNameLabel.font = [self.nickNameLabel.font boldFont];
    [self addSubview:self.nickNameLabel];
    
    // 承载成就view
    self.achievementView = [[UIView alloc] init];
    [self.contentView addSubview:self.achievementView];
    
    // 4. 攒按钮
    self.zanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.zanButton setImage:[UIImage imageNamed:@"icon_article_detail_bottom_zan_nor"] forState:UIControlStateNormal];
    [self.zanButton setTitle:@"赞" forState:UIControlStateNormal];
    self.zanButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"12"];
    [self.zanButton setTitleColor:[UIColor hexChangeFloat:@"323232"] forState:UIControlStateNormal];
    [self.zanButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleLeft imageTitleSpace:LCFloat(3)];
    [self.zanButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickZanWithBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.zanButton];
    
    // 5. 评论内容
    self.dymicLabel = [GWViewTool createLabelFont:@"15" textColor:@"323232"];
    self.dymicLabel.numberOfLines = 0;
    [self addSubview:self.dymicLabel];
    
    // 6. 时间
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"A2A2A2"];
    [self addSubview:self.timeLabel];
    
    // 7. 回复按钮
    self.linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.linkButton setTitle:@"关注" forState:UIControlStateNormal];
    self.linkButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"12"];
    [self.linkButton setTitleColor:[UIColor hexChangeFloat:@"EB6947"] forState:UIControlStateNormal];
    [self.linkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickLinkWithBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.linkButton];
    
    // 币图片
    self.bpImgView = [[PDImageView alloc]init];
    self.bpImgView.backgroundColor = [UIColor clearColor];
    self.bpImgView.image = [UIImage imageNamed:@"icon_article_detail_items_bi"];
    [self addSubview:self.bpImgView];
    
    // 币名称
    self.bpLabel = [GWViewTool createLabelFont:@"12" textColor:@"323232"];
    [self addSubview:self.bpLabel];
    
}

-(void)setTransferInfoManager:(LiveFourumRootListSingleModel *)transferInfoManager{
    _transferInfoManager = transferInfoManager;

    // 1. 头像
    self.avatarImgView.style = transferInfoManager.cert_badge;
    self.avatarImgView.frame = CGRectMake(LCFloat(20), LCFloat(16), LCFloat(40), LCFloat(40));
    [self.avatarImgView uploadImageWithURL:transferInfoManager.comment_pic placeholder:nil callback:NULL];
    
    self.convertView.center = self.avatarImgView.center;
    
    // title
    self.nickNameLabel.text = transferInfoManager.comment_name;
    CGSize nickNameSize = [Tool makeSizeWithLabel:self.nickNameLabel];
    CGFloat originX = CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(13);
    CGFloat maxWidth = kCommonScreenWidth - originX - 68 - LCFloat(15) - LCFloat(50);
    CGFloat nickWidth = nickNameSize.width > maxWidth ? maxWidth : nickNameSize.width;
    self.nickNameLabel.frame = CGRectMake(originX, self.avatarImgView.orgin_y + LCFloat(6), nickWidth, nickNameSize.height);
    
    // 添加成就
    self.achievementView.frame = CGRectMake(CGRectGetMaxX(_nickNameLabel.frame) + 5,
                                            0, 58, 16);
    self.achievementView.center = CGPointMake(self.achievementView.center_x, self.nickNameLabel.center_y);
    
    for (UIView *subView in self.achievementView.subviews) {
        [subView removeFromSuperview];
    }
    
    NSArray *achievementData = transferInfoManager.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [self.achievementView addSubview:imageView];
    }
    
    // more
    self.linkButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(50), 0, LCFloat(50), LCFloat(30));
    self.linkButton.center_y = self.nickNameLabel.center_y;
    
    CGFloat width = kScreenBounds.size.width - LCFloat(73) - LCFloat(21);
    self.dymicLabel.text = transferInfoManager.content;
    CGSize dymicSize = [transferInfoManager.content sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.dymicLabel.frame = CGRectMake(LCFloat(73), CGRectGetMaxY(self.nickNameLabel.frame) + LCFloat(11), width, dymicSize.height);
    
    // time
    self.timeLabel.text = [NSDate getTimeGap:transferInfoManager.create_time];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(self.nickNameLabel.orgin_x, CGRectGetMaxY(self.dymicLabel.frame) + LCFloat(13), timeSize.width, timeSize.height);
    
    // 币按钮
    self.bpImgView.frame = CGRectMake(CGRectGetMaxX(self.timeLabel.frame) + LCFloat(30), 0, LCFloat(15), LCFloat(15));
    self.bpImgView.center_y = self.timeLabel.center_y;
    
    self.bpLabel.frame = CGRectMake(CGRectGetMaxX(self.bpImgView.frame) + LCFloat(9), 0, 100, [NSString contentofHeightWithFont:self.bpLabel.font]);
    self.bpLabel.text = transferInfoManager.reward;
    self.bpLabel.center_y = self.bpImgView.center_y;
    
    // 赞按钮
    self.zanButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(40) - LCFloat(20), 0, LCFloat(40), LCFloat(40));
    self.zanButton.center_y = self.timeLabel.center_y;
    if (self.transferInfoManager.isSupport){
        [self.zanButton setTitle:[NSString stringWithFormat:@"%li",self.transferInfoManager.count_support] forState:UIControlStateNormal];
        [self.zanButton setImage:[UIImage imageNamed:@"icon_article_detail_tags_zan_hlt"] forState:UIControlStateNormal];
    } else {
        if (self.transferInfoManager.count_support <= 0){
            [self.zanButton setTitle:@"赞" forState:UIControlStateNormal];
        } else {
            [self.zanButton setTitle:[NSString stringWithFormat:@"%li",self.transferInfoManager.count_support] forState:UIControlStateNormal];
        }
        [self.zanButton setImage:[UIImage imageNamed:@"icon_article_detail_bottom_zan_nor"] forState:UIControlStateNormal];
    }
    
    if (self.transferInfoManager.isAttention){
        [self.linkButton setTitle:@"已关注" forState:UIControlStateNormal];
        [self.linkButton setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
    } else {
        [self.linkButton setTitle:@"关注" forState:UIControlStateNormal];
        [self.linkButton setTitleColor:[UIColor hexChangeFloat:@"EB6947"] forState:UIControlStateNormal];
    }
}



+(CGFloat)calculationCellHeightWithModel:(LiveFourumRootListSingleModel *)model{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(21);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"15"]];
    cellHeight += LCFloat(11);
    CGFloat width = kScreenBounds.size.width - LCFloat(73) - LCFloat(21);
    NSString *newString = [NSString stringWithFormat:@"%@",model.content];
    CGSize contentOfSize = [newString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"15"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += contentOfSize.height;
    cellHeight += LCFloat(13);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    cellHeight+= LCFloat(15);
    return cellHeight;
}


// o头像点击
-(void)actionClickHeaderImgWithBlock:(void(^)(void))block{
    objc_setAssociatedObject(self, &actionClickHeaderImgWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

/**< 关注点击*/
-(void)actionClickLinkWithBlock:(void(^)(void))block{
    objc_setAssociatedObject(self, &actionClickLinkWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
/**< 赞点击*/
-(void)actionClickZanWithBlock:(void(^)(void))block{
    objc_setAssociatedObject(self, &actionClickZanWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


-(void)btnStatusSelected:(BOOL)isSelected{
    self.transferInfoManager.isAttention = isSelected;
    if (isSelected){
        [self.linkButton setTitle:@"已关注" forState:UIControlStateNormal];
        [self.linkButton setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
    } else {
        [self.linkButton setTitle:@"关注" forState:UIControlStateNormal];
        [self.linkButton setTitleColor:[UIColor hexChangeFloat:@"EB6947"] forState:UIControlStateNormal];
    }
}

@end
