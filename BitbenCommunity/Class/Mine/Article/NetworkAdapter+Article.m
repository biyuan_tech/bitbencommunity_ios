//
//  NetworkAdapter+Article.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/10.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "NetworkAdapter+Article.h"
#import "ArticleCommentCreateModel.h"
#import "ArticleQuicklyInfoModel.h"

@implementation NetworkAdapter (Article)

#pragma mark 1.上传图片接口
-(void)articleAddWithImgList:(NSArray *)imgList
                 articleType:(ArticleType)articleType
                    topicArr:(NSArray *)topicArr
                       title:(NSString *)title
                     content:(NSString *)content
                    subTitle:(NSString *)subTitle
                       block:(void(^)())block{
    [self articleAddWithImgList:imgList articleType:articleType topicArr:topicArr title:title content:content subTitle:subTitle isAuthor:NO block:block];
}

-(void)articleAddWithImgList:(NSArray *)imgList articleType:(ArticleType)articleType topicArr:(NSArray *)topicArr title:(NSString *)title content:(NSString *)content subTitle:(NSString *)subTitle isAuthor:(BOOL)isAuthor block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    if (imgList.count){
        NSArray *smallArray;
        if (imgList.count >= 9){
            smallArray = [imgList subarrayWithRange:NSMakeRange(0, 8)];
        } else {
            smallArray = imgList;
        }
         

        
        [self uploadImgs:smallArray block:^(NSArray *imgUrlArr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (articleType == ArticleTypeShort){       // 短文
                [strongSelf subArticleAddWithImgList:imgUrlArr articleType:articleType topicArr:topicArr title:title content:content subTitle:subTitle isAuthor:isAuthor block:block];
            } else if (articleType == ArticleTypeLong){                // 长文
                NSString *newContent = content;
                for (int i = 0 ; i < imgUrlArr.count;i++){
                    NSString *imgsss = [imgUrlArr objectAtIndex:i];
                    if (!imgsss.length){
                        NSLog(@"123");
                    }
                    NSString *imgMainUrl = [NSString stringWithFormat:@"%@%@",imageBaseUrl,[imgUrlArr objectAtIndex:i]];
                    NSString *htmlStr = [NSString stringWithFormat:@"src=\"%@\"",imgMainUrl];
                    NSString *targetItem = [NSString stringWithFormat:@"[%li]",(long)i];
                    newContent = [newContent stringByReplacingOccurrencesOfString:targetItem withString:htmlStr];
                }
                [strongSelf subArticleAddWithImgList:imgUrlArr articleType:articleType topicArr:topicArr title:title content:newContent subTitle:subTitle isAuthor:isAuthor block:block];
            }
        }];
    } else {
        [self subArticleAddWithImgList:nil articleType:articleType topicArr:topicArr title:title content:content subTitle:subTitle isAuthor:isAuthor block:block];
    }
}


-(void)subArticleAddWithImgList:(NSArray *)imgList articleType:(ArticleType)articleType topicArr:(NSArray *)topicArr title:(NSString *)title content:(NSString *)content subTitle:(NSString *)subTitle isAuthor:(BOOL)isAuthor block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (title.length){
        [params setObject:title forKey:@"title"];
    }
    
    if (subTitle.length){
        [params setObject:subTitle forKey:@"subtitle"];
    }
    
    NSInteger author = isAuthor ? 1 : 0;
    [params setObject:@(author) forKey:@"is_author"];
    
    [params setObject:@(articleType) forKey:@"article_type"];
    if (imgList.count){
        NSString *imgString = @"";
        for (int i = 0 ; i < imgList.count;i++){
            NSString *singleImgUrl = [imgList objectAtIndex:i];
            if (i == imgList.count - 1){
                imgString = [imgString stringByAppendingString:[NSString stringWithFormat:@"%@",singleImgUrl]];
            } else {
                imgString = [imgString stringByAppendingString:[NSString stringWithFormat:@"%@,",singleImgUrl]];
            }
        }
        [params setObject:imgString forKey:@"picture"];
    }
    if (topicArr.count){
        NSString *topicStr = @"";
        for (int i = 0 ; i < topicArr.count;i++){
            NSString *topic = [topicArr objectAtIndex:i];
            if (i == topicArr.count - 1){
                topicStr = [topicStr stringByAppendingString:[NSString stringWithFormat:@"%@",topic]];
            } else {
                topicStr = [topicStr stringByAppendingString:[NSString stringWithFormat:@"%@,",topic]];
            }
        }
        
        [params setObject:topicStr forKey:@"topic_content"];
    }
    
    if (content.length){
        [params setObject:content forKey:@"content"];
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:article_create_article requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}



#pragma mark - 上传多张图片
-(void)uploadImgs:(NSArray *)imgs block:(void(^)(NSArray *imgUrlArr))block{
    NSMutableArray *fileMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < imgs.count;i++){
        OSSFileModel *fileModle = [[OSSFileModel alloc]init];
        fileModle.objcImage = [imgs objectAtIndex:i];
        [fileMutableArr addObject:fileModle];
    }
    
    __weak typeof(self)weakSelf = self;
    [[OSSManager sharedUploadManager] uploadImageManagerWithImageList:[fileMutableArr copy] withUrlBlock:^(NSArray *imgUrlArr) {
        if (!weakSelf){
            return ;
        }
        if (imgUrlArr){
            if (block){
                block(imgUrlArr);
            }
        }
    }];
}

#pragma mark - 获取列表
-(void)articleListWithPage:(NSInteger)page topic:(NSString *)topic authorId:(NSString *)authorId block:(void(^)(NSArray <ArticleRootSingleModel>*articleList))block{
    [self articleListWithPage:page topic:topic type:ArticleListTypeNew authorId:authorId block:block];
}

-(void)articleListWithPage:(NSInteger)page topic:(NSString *)topic type:(ArticleListType)type authorId:(NSString *)authorId block:(void(^)(NSArray <ArticleRootSingleModel>*articleList))block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:@(page) forKey:@"page_number"];
    [params setValue:@"10" forKey:@"page_size"];
    if (authorId.length){
        [params setValue:authorId forKey:@"author_id"];
    }
    if (topic.length){
        [params setValue:topic forKey:@"topic_content"];
    }
    
    NSString *path = @"";
    if (authorId.length){
        path = page_author_article;
    } else {
        if (topic.length){
            path = page_topic_article;
        } else {
            path = article_page_article;
        }
    }
    
//    if ([AccountModel sharedAccountModel].isShenhe){
//        [params setValue:@"2" forKey:@"status"];
//    } else {
//        [params setValue:@"1" forKey:@"status"];
//    }
    if (!authorId.length){
        [params setValue:@(type) forKey:@"type"];
    }
    
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:path requestParams:params responseObjectClass:[ArticleRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ArticleRootListModel *model = (ArticleRootListModel *)responseObject;
            if (model){
                block(model.content);
            }
        }
    }];
}

// 获取我关注的列表
-(void)articleListWithPage:(NSInteger)page block:(void(^)(NSArray <ArticleRootSingleModel>*articleList))block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:@(page) forKey:@"page_number"];
    [params setValue:@"10" forKey:@"page_size"];
    
    NSString *path = page_attention_article;
    [[NetworkAdapter sharedAdapter] fetchWithPath:path requestParams:params responseObjectClass:[ArticleRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ArticleRootListModel *model = (ArticleRootListModel *)responseObject;
            if (model){
                block(model.content);
            }
        }
    }];
    
}


#pragma mark - 获取指定作者列表
-(void)articleListWithPage:(NSInteger)page topic:(NSString *)topic author_id:(NSString *)author_id status:(NSString *)status block:(void(^)(NSArray <ArticleRootSingleModel>*articleList))block{
    
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page_number":@(page),@"page_size":@"10",@"author_id":author_id,@"status":status};
    [[NetworkAdapter sharedAdapter] fetchWithPath:page_author_article requestParams:params responseObjectClass:[ArticleRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ArticleRootListModel *model = (ArticleRootListModel *)responseObject;
            if (model){
                block(model.content);
            }
        }
    }];
}



#pragma mark - 获取热门点评
-(void)articleGetHotCommentId:(NSString *)commentId actionBlock:(void(^)(BOOL isSuccessed,LiveFourumRootListModel *list))actionBlock{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"theme_id":commentId,@"theme_type":@"1",@"reply_comment_id":@"0",@"page_number":@(0),@"page_size":@"10"};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:page_hot_comment requestParams:params responseObjectClass:[LiveFourumRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
    
        if (isSucceeded){
            LiveFourumRootListModel *listModel = (LiveFourumRootListModel *)responseObject;
            if (actionBlock){
                actionBlock(YES,listModel);
            }
        } else {
            if (actionBlock){
                actionBlock(NO,nil);
            }
        }
    }];
}

#pragma mark - 添加评论
-(void)articleCreateCommentWiththemeId:(NSString *)themeId ThemeInfo:(LiveFourumRootListSingleModel *)themeinfo content:(NSString *)content block:(void(^)(BOOL isSuccessed,NSString *comment_id))block{
    
    if(!content.length){
        [StatusBarManager statusBarHidenWithText:@"请输入内容"];
        return;
    }
    
    
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:themeId forKey:@"theme_id"];
    [params setObject:@"1" forKey:@"theme_type"];
    if (!themeinfo){
        [params setObject:@"0" forKey:@"reply_comment_id"];
    } else {
        if (themeinfo.transferTempCommentId.length){
            [params setObject:themeinfo.transferTempCommentId forKey:@"reply_comment_id"];
        } else {
            [params setObject:themeinfo._id forKey:@"reply_comment_id"];
        }
        
        [params setObject:themeinfo.user_id forKey:@"reply_user_id"];
    }
    
    [params setObject:content forKey:@"content"];
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:create_comment requestParams:params responseObjectClass:[ArticleCommentCreateModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ArticleCommentCreateModel *model = (ArticleCommentCreateModel *)responseObject;
            
            if (block){
                block(YES,model.comment_id);
            }
        }
    }];
}

#pragma mark  - 获取评论列表
-(void)sendRequestToGetInfoWithCommentId:(NSString *)commentId reReload:(BOOL)rereload page:(NSInteger)page actionBlock:(void(^)(BOOL isSuccessed,LiveFourumRootListModel *list))actionBlock{
    if (!commentId.length){
        return;
    }
    page = rereload ? 0:page;
    NSDictionary *params = @{@"theme_id":commentId,@"theme_type":@"1",@"reply_comment_id":@"0",@"page_number":@(page),@"page_size":@"10"};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:page_comment requestParams:params responseObjectClass:[LiveFourumRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            LiveFourumRootListModel *listModel = (LiveFourumRootListModel *)responseObject;
            if (actionBlock){
                actionBlock(YES,listModel);
            }
        }
    }];
}


#pragma mark - 获取话题列表
-(void)sendRequestToGetHuatiListManagerBlock:(void(^)(BOOL isSuccessed, ArticleDetailHuatiListModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_topic_list requestParams:nil responseObjectClass:[ArticleDetailHuatiListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ArticleDetailHuatiListModel *listModel = (ArticleDetailHuatiListModel *)responseObject;
            if (block){
                block(YES,listModel);
            }
        } else {
            if (block){
                block(NO,nil);
            }
        }
    }];
}


#pragma mark - 顶&踩
-(void)articleDingAndCai:(NSString *)commentId hasUp:(BOOL)up block:(void(^)(BOOL isSuccessed ,BOOL isOperate))block{
    __weak typeof(self)weakSelf = self;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:commentId forKey:@"theme_id"];
    [params setValue:@"1" forKey:@"theme_type"];
    if (up){
        [params setValue:@"0" forKey:@"operate_type"];
    } else {
        [params setValue:@"1" forKey:@"operate_type"];
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:create_interaction requestParams:params responseObjectClass:[LiveFourumOperateModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            LiveFourumOperateModel *model = (LiveFourumOperateModel *)responseObject;
            
            if (block){
                block(isSucceeded, model.isOperate);
            }
        }
    }];
}

#pragma mark - 举报
-(void)jubaoManagerWithId:(NSString *)themeId type:(BY_THEME_TYPE)themeType jubaoInfo:(NSString *)jubaoInfo block:(void(^)())block{
    NSDictionary *param = @{@"theme_id":themeId,
                            @"theme_type":@(themeType),
                            @"report_type":jubaoInfo
                            };
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:create_report requestParams:param responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}


#pragma mark - 取消定时发布
-(void)articleDraftCancelReleaseWithArticleId:(NSString *)articleId block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"article_id":articleId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:cancel_timing_article requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}

#pragma mark - 立即发布
-(void)articleDraftReleaseNowWithArticleId:(NSString *)articleId block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"article_id":articleId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:publish_article requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            block();
        }
    }];
}


#pragma mark - 排行榜
-(void)articleRankInfoManagerSuccessBlock:(void(^)(ArticleRootRankListModel *rankListModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_article_rank requestParams:nil responseObjectClass:[ArticleRootRankListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ArticleRootRankListModel *rankListModel = (ArticleRootRankListModel *)responseObject;
            if (block){
                block(rankListModel);
            }
        }
    }];
}

#pragma mark - 排行榜
-(void)rankRankListManagerPage:(NSInteger)page block:(void(^)(RankNormalModel *rankListModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page_number":@(page),@"page_size":@"10"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_mine_rank requestParams:params responseObjectClass:[RankNormalModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            RankNormalModel *rankListModel = (RankNormalModel *)responseObject;
            if (block){
                block(rankListModel);
            }
        }
    }];
}

-(void)addQuicklyInfoManagerWithTitle:(NSString *)title content:(NSString *)content img:(UIImage *)img block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    
    if (img){
        [self uploadImgs:@[img] block:^(NSArray *imgUrlArr) {
           if (!weakSelf){
               return ;
           }
           __strong typeof(weakSelf)strongSelf = weakSelf;
           NSString *imgUrl = @"";
          if (imgUrlArr.count > 0){
              imgUrl = [imgUrlArr firstObject];
          }
           [strongSelf addQuicklyInfoSubManagerWithTitle:title content:content img:imgUrl block:block];
       }];
    } else {
        [self addQuicklyInfoSubManagerWithTitle:title content:content img:@"" block:block];
    }
}

-(void)addQuicklyInfoSubManagerWithTitle:(NSString *)title content:(NSString *)content img:(NSString *)imgUrl block:(void(^)())block{
    NSDictionary *params = @{@"title":title,@"content":content,@"picture":imgUrl};
    [[NetworkAdapter sharedAdapter] fetchWithPath:create_bulletin requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}


-(void)autoGetUserTypeHasReleaseQuicklyInfoManagerblock:(void(^)(BOOL hasRelease,NSString *articleId))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_user_cert_type requestParams:nil responseObjectClass:[ArticleQuicklyInfoModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ArticleQuicklyInfoModel *infoModel = (ArticleQuicklyInfoModel *)responseObject;
            if (infoModel.cert_type == 2){
                if (block){
                    block(YES,infoModel.article_id);
                }
            } else {
                if (block){
                    block(NO,infoModel.article_id);
                }
            }
        }
    }];
}



#pragma mark - 收藏
-(void)collectionManager:(NSString *)theme_id themeType:(CollectionModelThemetype)type actionBlock:(void(^)(BOOL isSuccessed))block{
    // 1. 根据url 获取是分享直播还是文章
      
   NSDictionary *params = @{@"theme_id":theme_id,@"theme_type":@(type),@"operate_type":@"2"};
   [[NetworkAdapter sharedAdapter] fetchWithPath:@"create_interaction" requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
       if (isSucceeded){
           [StatusBarManager statusBarShowWithText:@"已收藏"];
           if (block){
               block(YES);
           }
       } else {
           if (block){
               block(NO);
           }
           [StatusBarManager statusBarShowWithText:@"收藏出现异常，请稍后再试"];
       }
   }];
}

#pragma mark - 取消收藏
-(void)collectionCancelManager:(NSString *)theme_id themeType:(CollectionModelThemetype)type actionBlock:(void(^)(BOOL isSuccessed))block{
    NSDictionary *params = @{@"theme_id":theme_id,@"theme_type":@(type),@"operate_type":@"2"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"delete_interaction" requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (isSucceeded){
            [StatusBarManager statusBarShowWithText:@"已取消收藏"];
            if (block){
                block(YES);
            }
        } else {
            if (block){
                block(NO);
            }
            [StatusBarManager statusBarShowWithText:@"取消收藏出现异常，请稍后再试"];
        }
    }];
}


@end
