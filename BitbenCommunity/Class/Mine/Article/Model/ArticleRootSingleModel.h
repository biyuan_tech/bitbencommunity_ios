//
//  ArticleRootSingleModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/10.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"

@interface TSHomeClassDetailJieshaoWebTableViewCellModel : FetchModel
@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *html;
@property (nonatomic,assign)BOOL hasLoad;
@end


typedef NS_ENUM(NSInteger,article_type) {
    article_typeNormal,                 /**< 普通的文章*/
    article_typeWeb,                    /**< 网络文章*/
    article_typeInfo = 2,                    /**< 资讯*/
};

@protocol ArticleRootSingleModel <NSObject>
@end

@interface ArticleRootSingleModel : FetchModel

@property (nonatomic,copy)NSString *_id;                /**< id*/
@property (nonatomic,copy)NSString *article_id;                /**< id*/
@property (nonatomic,assign)article_type article_type;                /**< id*/
@property (nonatomic,copy)NSString *author_id;                /**< id*/
@property (nonatomic,assign)NSInteger comment_count;                /**< id*/
@property (nonatomic,assign)NSInteger count_support;                /**< id*/
@property (nonatomic,assign)NSInteger count_tread;                /**< id*/
@property (nonatomic,assign)NSTimeInterval create_time;                /**< id*/
@property (nonatomic,copy)NSString *head_img;                /**< id*/
@property (nonatomic,assign)BOOL isSupport;                /**< id*/
@property (nonatomic,assign)BOOL isTread;                /**< id*/
@property (nonatomic,assign)BOOL isAttention;                /**< id*/
@property (nonatomic,copy)NSString * nickname;                /**< id*/
@property (nonatomic,strong)NSArray *picture;                /**< id*/
@property (nonatomic,copy)NSString *reward;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *subtitle;
@property (nonatomic,strong)NSArray *topic_content;         /**< 话题*/
@property (nonatomic,copy)NSString *article_url;
@property (nonatomic,assign)NSInteger count_uv;
@property (nonatomic,copy)NSString *timing_status;
@property (nonatomic,copy)NSString *timing_datetime;
@property (nonatomic,assign)NSTimeInterval timing_time;

@property (nonatomic,assign)BOOL collection_status;                /**< id*/

@property (nonatomic,strong)UIImage *avatarRoundImg;

@property (nonatomic,strong) TSHomeClassDetailJieshaoWebTableViewCellModel *webLoadTempModel;
@property (nonatomic,strong) NSArray<ArticleRootSingleModel> *link_article_list;

/** vip认证 */
@property (nonatomic ,assign) NSInteger cert_badge;
/** 成就组 */
@property (nonatomic ,strong) NSArray <NSString *>*achievement_badge_list;
/** 是否为原创(0 否 1 是) */
@property (nonatomic ,assign) BOOL is_author;
@property (nonatomic ,assign) BOOL is_hot;


//
//public static final String ID = "_id";                                  // ID
//public static final String ARTICLE_ID = "article_id";                   // 文章ID
//public static final String TITLE = "title";                             // 标题
//public static final String CONTENT = "content";                         // 内容
//public static final String PICTURE = "picture";                         // 配图
//public static final String AUTHOR_ID = "author_id";                     // 作者的用户ID
//public static final String HEAD_IMG = "head_img";                       // 作者头像
//public static final String NICKNAME = "nickname";                       // 作者昵称
//public static final String ARTICLE_TYPE = "article_type";               // 文章类型[0：短图文，1：长文]
//public static final String TOPIC_CONTENT = "topic_content";             // 话题ID
//public static final String CREATE_TIME = "create_time";                 // 创建时间
//public static final String COUNT_SUPPORT = "count_support";             // 顶的数量
//public static final String COUNT_TREAD = "count_tread";                 // 踩的数量
//
//public static final String IS_ATTENTION = "is_attention";               // 是否已关注
//public static final String IS_SUPPORT = "is_support";                   // 是否已顶
//public static final String IS_TREAD = "is_tread";                       // 是否已踩
//public static final String COMMENT_COUNT = "comment_count";             // 评论量
//public static final String REWARD = "reward";                           // 获得的奖励


@end


@interface ArticleRootListModel : FetchModel

@property (nonatomic,strong)NSArray<ArticleRootSingleModel> *content;
@end
