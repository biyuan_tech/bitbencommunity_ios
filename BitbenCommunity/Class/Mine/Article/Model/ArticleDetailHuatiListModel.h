//
//  ArticleDetailHuatiListModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/19.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"

@protocol ArticleDetailHuatiListSingleModel <NSObject>
@end

@interface ArticleDetailHuatiListSingleModel:FetchModel
@property (nonatomic,copy)NSString *_id;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *topic_id;
@property (nonatomic,assign)NSInteger isLike;

@end

@interface ArticleDetailHuatiListModel : FetchModel

@property (nonatomic,strong)NSArray<ArticleDetailHuatiListSingleModel >*content;

@end


