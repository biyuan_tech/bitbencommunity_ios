//
//  ArticleCommentCreateModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/9/3.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleCommentCreateModel : FetchModel

@property (nonatomic,copy)NSString *comment_id;

@end

NS_ASSUME_NONNULL_END
