//
//  ArticleRootTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/9.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleRootTableViewCell.h"
#import "ArticleTagsView.h"

static char actionClickWithImgSelectedBlockKey;
static char actionClickWithTagsSelectedBlockKey;
static char actionClickWithLinkButtonBlockKey;
static char actionClickHeaderImgWithBlockKey;
static char actionClickWithMoreBtnClickBlockKey;
static char actionClickDelBtnBlockKey;
@interface ArticleRootTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;                     /**< 头像*/
@property (nonatomic,strong)PDImageView *avatarConvertImgView;              /**< 头像圆角*/
@property (nonatomic,strong)UILabel *nicknameLabel;                         /**< 昵称*/
@property (nonatomic,strong)UILabel *datetimeLabel;                         /**< 时间*/
@property (nonatomic,strong)UILabel *titleLabel;                            /**< 标题*/
@property (nonatomic,strong)UILabel *desclabel;                             /**< 内容*/
@property (nonatomic,strong)PDImageView *hotImgView;                        /**< 热门图片*/
@property (nonatomic,strong)ArticleTagsView *huatiDymicView;                /**< 话题标签*/
/** 更多按钮（删除） */
@property (nonatomic ,strong) UIButton *delBtn;
/** 承载成就图标 */
@property (nonatomic ,strong) UIView *achievementView;
@end

@implementation ArticleRootTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createTableView];
    }
    return self;
}

#pragma mark - createView
-(void)createTableView{
    // 1. 头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame = CGRectMake(LCFloat(15), LCFloat(20), LCFloat(44), LCFloat(44));
    [self addSubview:self.avatarImgView];
    // 2.mask
    self.avatarConvertImgView = [[PDImageView alloc]init];
    self.avatarConvertImgView.backgroundColor = [UIColor clearColor];
    self.avatarConvertImgView.image = [UIImage imageNamed:@"bg_avatar_convert"];
    self.avatarConvertImgView.frame = self.avatarImgView.frame;
    [self addSubview:self.avatarConvertImgView];
    __weak typeof(self)weakSelf = self;
    [self.avatarConvertImgView addTapGestureRecognizer:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickHeaderImgWithBlockKey);
        if (block){
            block();
        }
    }];
    
    
    // 删除按钮
    UIButton *delBtn = [UIButton by_buttonWithCustomType];
    delBtn.hidden = YES;
    [delBtn setBy_imageName:@"icon_live_more" forState:UIControlStateNormal];
    [delBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickDelBtnBlockKey);
        if (block){
            block();
        }
    }];
    [self.contentView addSubview:delBtn];
    self.delBtn = delBtn;
    @weakify(self);
    [delBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.width.mas_equalTo(52);
        make.height.mas_equalTo(23);
        make.right.mas_equalTo(-kCellRightSpace);
        make.centerY.mas_equalTo(self.avatarImgView.mas_centerY).mas_offset(0);
    }];
    
    // 昵称
    self.nicknameLabel = [GWViewTool createLabelFont:@"14" textColor:@"343434"];
    self.nicknameLabel.font = [self.nicknameLabel.font boldFont];
    [self addSubview: self.nicknameLabel];
    
    // 时间
    self.datetimeLabel = [GWViewTool createLabelFont:@"11" textColor:@"A2A2A2"];
    [self addSubview:self.datetimeLabel];
    
    // 承载成就
    self.achievementView = [[UIView alloc] init];
    [self addSubview:self.achievementView];
    
    // 热门
    self.hotImgView = [[PDImageView alloc]init];
    self.hotImgView.backgroundColor = [UIColor clearColor];
    self.hotImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(32), 0, LCFloat(32), LCFloat(32));
    self.hotImgView.image = [UIImage imageNamed:@"icon_article_root_hot"];
    [self addSubview:self.hotImgView];
    
    self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.moreButton.backgroundColor = [UIColor clearColor];
    self.moreButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(44), self.avatarImgView.center_y - LCFloat(44) / 2., LCFloat(44), LCFloat(44));
    [self.moreButton setImage:[UIImage imageNamed:@"icon_article_more"] forState:UIControlStateNormal];
    [self.moreButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithMoreBtnClickBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.moreButton];
    
    // 关注
    self.linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.linkButton.backgroundColor = [UIColor clearColor];
    self.linkButton.frame = CGRectMake(self.moreButton.orgin_x - LCFloat(54), self.avatarImgView.center_y - LCFloat(11), LCFloat(54), LCFloat(22));
    self.linkButton.center_y = self.avatarImgView.center_y;
    [self addSubview:self.linkButton];
    
    // 标题
    self.titleLabel = [GWViewTool createLabelFont:@"17" textColor:@"323232"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.numberOfLines = 2;
    [self addSubview:self.titleLabel];
    
    // 详情
    self.desclabel = [GWViewTool createLabelFont:@"15" textColor:@"303030"];
    self.desclabel.numberOfLines = 0;
    [self addSubview:self.desclabel];
    
    // 图片的背景
    self.imgBgView = [[ArticleRootSingleImgBgView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 10)];
    self.imgBgView.backgroundColor = [UIColor clearColor];
    [self.imgBgView actionImgClickBlock:^(NSString *imgUrl, PDImageView *imgView) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.selectedImgView = imgView;
        void(^block)(NSString *tempImgUrl) = objc_getAssociatedObject(strongSelf, &actionClickWithImgSelectedBlockKey);
        if (block){
            block(imgUrl);
        }
    }];
    
    [self addSubview:self.imgBgView];
    
    self.biImgView = [[PDImageView alloc]init];
    self.biImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.biImgView];
    
    self.biLabel = [GWViewTool createLabelFont:@"13" textColor:@"323232"];
    [self addSubview:self.biLabel];
}

-(void)setTransferSingleModel:(ArticleRootSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;

    self.avatarImgView.style = transferSingleModel.cert_badge;
    // avatar
    [self.avatarImgView uploadImageWithURL:transferSingleModel.head_img placeholder:nil callback:NULL];
    
    // title
    self.nicknameLabel.text = transferSingleModel.nickname;
    CGSize nickSize = [Tool makeSizeWithLabel:self.nicknameLabel];
    CGFloat originX = CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11);
    CGFloat maxWidth = kCommonScreenWidth - originX - 120;
    self.nicknameLabel.frame = CGRectMake(originX, 0, nickSize.width < maxWidth ? nickSize.width : maxWidth, nickSize.height);
    
    
    // time
    self.datetimeLabel.text = [NSDate getTimeGap:transferSingleModel.create_time / 1000.];
    CGSize dateSize = [Tool makeSizeWithLabel:self.datetimeLabel];
    self.datetimeLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), 0, dateSize.width, dateSize.height);
    
    CGFloat margin = (self.avatarImgView.size_height - [NSString contentofHeightWithFont:self.nicknameLabel.font] - [NSString contentofHeightWithFont:self.datetimeLabel.font]) / 3.;
    self.nicknameLabel.orgin_y = self.avatarImgView.orgin_y + margin;
    self.datetimeLabel.orgin_y = CGRectGetMaxY(self.nicknameLabel.frame) + margin;
    
    // 添加成就
    self.achievementView.frame = CGRectMake(CGRectGetMaxX(self.nicknameLabel.frame) + 5, 0, 58, 16);
    self.achievementView.center = CGPointMake(self.achievementView.center_x, self.nicknameLabel.center_y);
    
    for (UIView *subView in self.achievementView.subviews) {
        [subView removeFromSuperview];
    }
    
    NSArray *achievementData = transferSingleModel.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [self.achievementView addSubview:imageView];
    }
    
    // title
    self.titleLabel.text = transferSingleModel.title.length ? transferSingleModel.title : @"";
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(15), CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.avatarImgView.frame) + LCFloat(15), kScreenBounds.size.width - 2 * LCFloat(15), MIN(titleSize.height, 2 * [NSString contentofHeightWithFont:self.titleLabel.font]));

    // title
    NSString *desc = transferSingleModel.subtitle.length?transferSingleModel.subtitle:@"";
    [self.desclabel setText:desc lineSpacing:9];
    CGSize descSize = [desc sizeWithCalcFont:self.desclabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    NSInteger numberMax = MIN(descSize.height / [NSString contentofHeightWithFont:self.desclabel.font], 4);
    
    
    self.desclabel.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(14), kScreenBounds.size.width - 2 * LCFloat(15), MIN(descSize.height, 4 * [NSString contentofHeightWithFont:self.desclabel.font]) + (numberMax - 1) * 9);
    
    if (transferSingleModel.subtitle.length){
        self.desclabel.hidden = NO;
    } else {
        self.desclabel.hidden = YES;
        self.desclabel.size_height = 0;
    }
    
    
    // imgBgView
    self.imgBgView.orgin_y = CGRectGetMaxY(self.desclabel.frame) + LCFloat(11);
    self.imgBgView.transferImgArr = transferSingleModel.picture;
    self.imgBgView.size_height = [ArticleRootSingleImgBgView calculationHeight:transferSingleModel.picture];
    self.imgBgView.clipsToBounds = YES;

//     话题
    if (transferSingleModel.topic_content.count){
        if (!self.huatiDymicView){
            self.huatiDymicView = [[ArticleTagsView alloc]initWithFrame:CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15) - LCFloat(40), [ArticleTagsView calculationCellHeightWithArr:transferSingleModel.topic_content hasEdit:NO])];

            __weak typeof(self)weakSelf = self;
            [self.huatiDymicView actionClickWithTagsBlock:^(NSString *info) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void(^block)(NSString *info) = objc_getAssociatedObject(strongSelf, &actionClickWithTagsSelectedBlockKey);
                if (block){
                    block(info);
                }
            }];
            [self addSubview:self.huatiDymicView];
        }

        self.huatiDymicView.transferArr = transferSingleModel.topic_content;
        
        if (self.huatiDymicView){
            if (self.transferSingleModel.picture.count){
                self.huatiDymicView.orgin_y = CGRectGetMaxY(self.imgBgView.frame) + LCFloat(11);
            } else {
                if (self.desclabel.text.length){
                    self.huatiDymicView.orgin_y = CGRectGetMaxY(self.desclabel.frame) + LCFloat(11);
                } else {
                    self.huatiDymicView.orgin_y = CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11);
                }
            }
        }
        self.huatiDymicView.hidden = NO;
    } else {
        self.huatiDymicView.hidden = YES;
    }

    // 如果文章是我发的
    if ([transferSingleModel.author_id isEqualToString:[AccountModel sharedAccountModel].loginServerModel.user._id]){
        self.linkButton.hidden = YES;
    } else {
        // linkButton
        if (transferSingleModel.isAttention){
            [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_nor"] forState:UIControlStateNormal];
            self.linkButton.hidden = YES;
        } else {
            [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_hlt"] forState:UIControlStateNormal];
            self.linkButton.hidden = NO;
        }
    }

    __weak typeof(self)weakSelf = self;
    [self.linkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithLinkButtonBlockKey);
        if (block){
            block();
        }
    }];
    
    if (transferSingleModel.count_uv >= 30){
        self.hotImgView.hidden = NO;
    } else {
        self.hotImgView.hidden = YES;
    }
    
    
    // bi Label
    self.biLabel.text = transferSingleModel.reward;
    CGSize biSize = [Tool makeSizeWithLabel:_biLabel];
    self.biLabel.frame = CGRectMake(kScreenBounds.size.width - biSize.width - LCFloat(15), LCFloat(19), biSize.width, biSize.height);
    self.biLabel.center_y = self.huatiDymicView.center_y;
    
    self.biImgView.image = [UIImage imageNamed:@"icon_article_detail_items_bi"];
    self.biImgView.frame = CGRectMake(self.biLabel.orgin_x - LCFloat(18) - LCFloat(3), 0, LCFloat(18), LCFloat(18));
    self.biImgView.center_y = self.biLabel.center_y;
}

-(void)actionClickWithImgSelectedBlock:(void(^)(NSString *imgUrl))block{
    objc_setAssociatedObject(self, &actionClickWithImgSelectedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithTagsSelectedBlock:(void(^)(NSString *tag))block{
    objc_setAssociatedObject(self, &actionClickWithTagsSelectedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithLinkButtonBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithLinkButtonBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)actionClickHeaderImgWithBlock:(void (^)(void))block{
    objc_setAssociatedObject(self, &actionClickHeaderImgWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithMoreBtnClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithMoreBtnClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)actionClickWithDelMoreBtnBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickDelBtnBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)btnStatusSelected:(BOOL)isSelected{
    self.transferSingleModel.isAttention = isSelected;
    if (isSelected){
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_nor"] forState:UIControlStateNormal];
        self.linkButton.hidden = YES;
    } else {
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_hlt"] forState:UIControlStateNormal];
        self.linkButton.hidden = NO;
    }
}

+(CGFloat)calculationCellHeightWithModel:(ArticleRootSingleModel *)transferSingleModel{
    CGFloat cellHeight = 0;
    // 1. 头像
    cellHeight += LCFloat(20);
    cellHeight += LCFloat(44);
    // 内容
    cellHeight += LCFloat(15);
    NSString *title = transferSingleModel.title.length?transferSingleModel.title:@"";
    CGSize titleSize = [title sizeWithCalcFont:[[UIFont fontWithCustomerSizeName:@"17"] boldFont] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(15), CGFLOAT_MAX)];
    cellHeight += MIN(titleSize.height, 4 * [NSString contentofHeightWithFont:[[UIFont fontWithCustomerSizeName:@"17"]boldFont]]);

    // 副标题
    if (transferSingleModel.subtitle.length){
        cellHeight += LCFloat(14);
        NSString *des = transferSingleModel.subtitle.length?transferSingleModel.subtitle:@"";
        CGSize descSize = [des sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"15"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(15), CGFLOAT_MAX)];
        NSInteger numberMax = MIN(descSize.height / [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"15"]], 4);
        
        cellHeight += (MIN(descSize.height, 4 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"15"]]) + (numberMax - 1) * 9);
    }

    // 图片
    if (transferSingleModel.picture.count){
        cellHeight += LCFloat(11);
        CGFloat mainHeight = [ArticleRootSingleImgBgView calculationHeight:transferSingleModel.picture];
        cellHeight += mainHeight;
        cellHeight += LCFloat(11);
    }

    // 话题
    if (transferSingleModel.topic_content.count){
        cellHeight += LCFloat(11);
        cellHeight += [ArticleTagsView calculationCellHeightWithArr:transferSingleModel.topic_content hasEdit:NO];
    }
    
    cellHeight += LCFloat(11);
    return cellHeight;
}

- (void)hiddenLinkBtn{
    self.linkButton.layer.opacity = 0.0f;
}

-(void)linkButtonStatusManager{
    // 如果文章是我发的
    if ([self.transferSingleModel.author_id isEqualToString:[AccountModel sharedAccountModel].loginServerModel.user._id]){
        self.linkButton.hidden = YES;
    } else {
        // linkButton
        if (self.transferSingleModel.isAttention){
            [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_nor"] forState:UIControlStateNormal];
            self.linkButton.hidden = YES;
        } else {
            [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_hlt"] forState:UIControlStateNormal];
            self.linkButton.hidden = NO;
        }
    }
}


@end


static char actionImgClickBlockKey;
@interface ArticleRootSingleImgBgView()
@end

@implementation ArticleRootSingleImgBgView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    
}

-(void)setTransferImgArr:(NSArray *)transferImgArr{
    _transferImgArr = transferImgArr;
    if (self.subviews.count){
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    if (transferImgArr.count >= 3){
        for (int i = 0 ; i < transferImgArr.count;i++){
            NSString *currentImgUrl = [transferImgArr objectAtIndex:i];
            
            CGFloat margin = LCFloat(3);
            CGFloat top_margin = LCFloat(15);
            CGFloat single_width = (kScreenBounds.size.width - 2 * margin - 2 * top_margin) / 3.;
            // 1. origin_x
            CGFloat origin_x = top_margin + (i % 3) * single_width + (i % 3) * margin;
            // 2. origin_y
            CGFloat origin_y = LCFloat(11) + ( i / 3 ) * (single_width + margin);
            // 3 .frame
            CGRect tempRect = CGRectMake(origin_x, origin_y, single_width,single_width);
            
            PDImageView *imgView = [[PDImageView alloc]init];
            imgView.frame = tempRect;
            imgView.contentMode = UIViewContentModeScaleAspectFill;
            imgView.clipsToBounds = YES;
            [imgView uploadImageWithURL:currentImgUrl placeholder:nil callback:NULL];
            [self addSubview:imgView];
            
            UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
            actionButton.frame = tempRect;
            [self addSubview:actionButton];
            actionButton.userInteractionEnabled = YES;
            __weak typeof(self)weakSelf = self;
            [actionButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void(^block)(NSString *imgUrl,PDImageView *imgView) = objc_getAssociatedObject(strongSelf, &actionImgClickBlockKey);
                if (block){
                    block(currentImgUrl,imgView);
                }
            }];
        }
        self.size_height = [ArticleRootSingleImgBgView calculationHeight:transferImgArr];
    } else if (transferImgArr.count == 2){

        CGFloat margin = LCFloat(3);
        CGFloat top_margin = LCFloat(15);
        CGFloat single_width = (kScreenBounds.size.width - 1 * margin - 2 * top_margin) / 2.;
        for (int i = 0 ; i < transferImgArr.count;i++){
            NSString *currentImgUrl = [transferImgArr objectAtIndex:i];
            
            CGFloat origin_x = margin + i * (single_width + margin);
            
            PDImageView *imgView = [[PDImageView alloc]init];
            imgView.frame = CGRectMake(origin_x, LCFloat(11), single_width, single_width);
            [imgView uploadImageWithURL:currentImgUrl placeholder:nil callback:NULL];
            imgView.contentMode = UIViewContentModeScaleAspectFill;
            imgView.clipsToBounds = YES;
            [self addSubview:imgView];
            
            UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
            actionButton.frame = imgView.frame;
            [self addSubview:actionButton];
            actionButton.userInteractionEnabled = YES;
            __weak typeof(self)weakSelf = self;
            [actionButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void(^block)(NSString *imgUrl,PDImageView *imgView) = objc_getAssociatedObject(strongSelf, &actionImgClickBlockKey);
                if (block){
                    block(currentImgUrl,imgView);
                }
            }];
        }
        self.size_height = [ArticleRootSingleImgBgView calculationHeight:transferImgArr];
    } else if (transferImgArr.count == 1){
        
        // 1. 获取当前的imgURL
        NSString *currentImgUrl = [transferImgArr lastObject];
        NSArray *imgArr = [currentImgUrl componentsSeparatedByString:@"!!"];
        CGFloat width = 0;
        CGFloat height = 0;
        if (imgArr.count == 3){
            width = [[imgArr objectAtIndex:1] integerValue];
            height = [[imgArr objectAtIndex:2] integerValue];
        }
        
        PDImageView *imgView = [[PDImageView alloc]init];
        if (width != 0 && height != 0){
            CGFloat maxHeight = [ArticleRootSingleImgBgView calculationHeight:transferImgArr] - LCFloat(11);
            if (width < height){            // 图片高度大于宽度
                CGFloat currentWidth = maxHeight * width / height;
                imgView.frame = CGRectMake(LCFloat(15), LCFloat(11), currentWidth, maxHeight);
            } else {
                CGFloat currentHeight = width * maxHeight / width;
                imgView.frame = CGRectMake(LCFloat(15), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(15),  currentHeight);
            }
        } else {
            imgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), [ArticleRootSingleImgBgView calculationHeight:transferImgArr]);
        }

        __weak typeof(self)weakSelf = self;
        [imgView uploadImageWithURL:currentImgUrl placeholder:nil callback:NULL];
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.clipsToBounds = YES;
        [self addSubview:imgView];
        
        UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        actionButton.frame = imgView.frame;
        [self addSubview:actionButton];
        actionButton.userInteractionEnabled = YES;
        [actionButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(NSString *imgUrl,PDImageView *imgView) = objc_getAssociatedObject(strongSelf, &actionImgClickBlockKey);
            if (block){
                block(currentImgUrl,imgView);
            }
        }];
    }
}

-(void)actionImgClickBlock:(void(^)(NSString *imgUrl,PDImageView *imgView))block{
    objc_setAssociatedObject(self, &actionImgClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithTagsSelectedBlock:(void(^)(NSString *tag))block{
    objc_setAssociatedObject(self, &actionClickWithTagsSelectedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
+(CGFloat)calculationHeight:(NSArray *)imgArr{
    CGFloat height = 0;
    if (imgArr.count >= 3){
        CGFloat margin = LCFloat(3);
        CGFloat top_margin = LCFloat(15);
        CGFloat width = (kScreenBounds.size.width - 2 * top_margin - 2 * margin) / 3.;
        CGFloat numberOfRow = imgArr.count % 3 == 0? imgArr.count / 3 : imgArr.count / 3 + 1;
        height = margin + numberOfRow * (margin + width);
    } else if (imgArr.count == 2){
        CGFloat margin = LCFloat(3);
        CGFloat top_margin = LCFloat(15);
        CGFloat single_width = (kScreenBounds.size.width - 2 * top_margin - margin) / 2.;

        height += 2 * LCFloat(11) + single_width + LCFloat(11);
    } else if (imgArr.count == 1){
        height += LCFloat(200);
    }
    
    return height;
}

@end

