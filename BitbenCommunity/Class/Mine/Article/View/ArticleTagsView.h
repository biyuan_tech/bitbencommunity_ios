//
//  ArticleTagsView.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/13.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleTagsView : UIView

@property (nonatomic,strong)NSArray *transferArr;
@property (nonatomic,copy)NSString *barTitle;
@property (nonatomic,assign)BOOL hasEdit;
@property (nonatomic,strong)UIView *bgView;             /**< 背景图*/
@property (nonatomic,assign)BOOL hasRelease;            /**< 是否发布页使用*/

-(void)actionClickWithTagsBlock:(void(^)(NSString *info))block;

+(CGFloat)calculationCellHeightWithArr:(NSArray *)transferArr hasEdit:(BOOL)hasEdit;
+(CGFloat)calculationCellHeightWithArr:(NSArray *)transferArr hasEdit:(BOOL)hasEdit hasCreate:(BOOL)hasCreate;

@end
