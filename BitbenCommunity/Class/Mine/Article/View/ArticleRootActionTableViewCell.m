//
//  ArticleRootActionTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/9.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleRootActionTableViewCell.h"

static char actionClickWithShareManagerWithArticleModelBlockKey;
@interface ArticleRootActionTableViewCell()
@property (nonatomic,strong)LiveForumRootDingView *shareView;
@property (nonatomic,strong)LiveForumRootDingView *upView;
@property (nonatomic,strong)LiveForumRootDingView *downView;

@end

@implementation ArticleRootActionTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    for (int i = 0 ; i < 3 ;i++){
        LiveForumRootDingView *view = [[LiveForumRootDingView alloc]initWithFrame:CGRectMake(0, 0, [LiveForumRootDingView calculationSize].width, [LiveForumRootDingView calculationSize].height)];
        if (i == 0){
            self.shareView = view;
            self.shareView.transferType = LiveForumRootDingViewTypeRootShare;
            self.shareView.userInteractionEnabled = YES;
        } else if (i == 2){
            self.downView = view;
            self.downView.transferType = LiveForumRootDingViewTypeRootMsg;
            self.downView.userInteractionEnabled = NO;
        } else if (i == 1){
            self.upView = view;
            self.upView.transferType = LiveForumRootDingViewTypeRootUp;
            self.upView.userInteractionEnabled = YES;
        }
        [self addSubview:view];
    }
    
    CGFloat single_width = kScreenBounds.size.width / 6;
    self.shareView.center_x = single_width;
    self.downView.center_x = kScreenBounds.size.width / 2.;
    self.upView.center_x = kScreenBounds.size.width - single_width;
    
    __weak typeof(self)weakSelf = self;
    [self.upView actionButtonWithReplyBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf buttonActionClickManager];
    }];
    
    [self.shareView actionButtonWithShareBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(ArticleRootSingleModel *articleModel) = objc_getAssociatedObject(strongSelf, &actionClickWithShareManagerWithArticleModelBlockKey);
        if (block){
            block(strongSelf.transferSingleModel);
        }
    }];
}

-(void)setTransferSingleModel:(ArticleRootSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    self.shareView.transferArticleRootSingleModel = transferSingleModel;
    self.upView.transferArticleRootSingleModel = transferSingleModel;
    self.downView.transferArticleRootSingleModel = transferSingleModel;
}

-(void)buttonActionClickManager{
    __weak typeof(self)weakSelf = self;

    [[NetworkAdapter sharedAdapter] articleDingAndCai:self.transferSingleModel._id hasUp:YES block:^(BOOL isSuccessed, BOOL isOperate) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (!isOperate){
                [Tool clickZanWithView:strongSelf.upView block:^{
                    strongSelf.transferSingleModel.isSupport = YES;
                    strongSelf.transferSingleModel.count_support += 1;
                    strongSelf.upView.transferArticleRootSingleModel = strongSelf.transferSingleModel;
                }];
            } else {
                [StatusBarManager statusBarHidenWithText:@"你已经顶过该条评论"];
            }
        }
    }];
    
}

-(void)actionClickWithShareManagerWithArticleModelBlock:(void(^)(ArticleRootSingleModel *articleModel))block{
    objc_setAssociatedObject(self, &actionClickWithShareManagerWithArticleModelBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
