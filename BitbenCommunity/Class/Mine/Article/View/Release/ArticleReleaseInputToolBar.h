//
//  ArticleReleaseInputToolBar.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/17.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,ArticleReleaseInputToolBarType) {
    ArticleReleaseInputToolBarTypeShot,
    ArticleReleaseInputToolBarTypeLong,
    ArticleReleaseInputToolBarTypeQuicklyInfo,          // 快讯
};

@interface ArticleReleaseInputToolBar : UIView

@property (nonatomic,assign)NSInteger currentInputCount;
@property (nonatomic,assign)NSInteger maxInputCount;
@property (nonatomic,strong)UIButton *imgSelectedButton;
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,assign)ArticleReleaseInputToolBarType transferBarType;
/** 原创按钮 */
@property (nonatomic ,strong) UIButton *originalBtn;


-(void)actionClickBiaoqingblock:(void(^)())block;                   /**< 表情*/
-(void)actionClickJinghaoblock:(void(^)())block;                    /**< 井号*/
-(void)actionClickImgSelectedblock:(void(^)())block;                /**< 图片选择*/
-(void)actionClickFontSelectedblock:(void(^)())block;               /**< 文案编辑*/
@end
