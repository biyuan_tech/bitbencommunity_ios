//
//  ArticleRootTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/9.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ArticleRootSingleModel.h"

@interface ArticleRootSingleImgBgView : UIView

@property (nonatomic,strong)NSArray *transferImgArr;
-(void)actionImgClickBlock:(void(^)(NSString *imgUrl,PDImageView *imgView))block;
+(CGFloat)calculationHeight:(NSArray *)imgArr;

@end


@interface ArticleRootTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)ArticleRootSingleModel *transferSingleModel;
@property (nonatomic,strong)ArticleRootSingleImgBgView *imgBgView;              // temp
@property (nonatomic,strong)PDImageView *selectedImgView;
@property (nonatomic,strong)UIButton *linkButton;
@property (nonatomic,strong)UIButton *moreButton;                           /**< 更多按钮*/
@property (nonatomic,strong)PDImageView *biImgView;
@property (nonatomic,strong)UILabel *biLabel;
@property (nonatomic ,strong,readonly) UIButton *delBtn;

-(void)btnStatusSelected:(BOOL)isSelected;

-(void)actionClickWithImgSelectedBlock:(void(^)(NSString *imgUrl))block;                    /**< 图片点击*/
-(void)actionClickWithTagsSelectedBlock:(void(^)(NSString *tag))block;                      /**< 标签点击*/
-(void)actionClickWithLinkButtonBlock:(void(^)())block;                                     /**< 关注*/
-(void)actionClickHeaderImgWithBlock:(void(^)(void))block;                                  /**< 头像点击*/
-(void)actionClickWithMoreBtnClickBlock:(void(^)())block;                                   /**< 更多按钮点击*/
-(void)hiddenLinkBtn;                                                                       /**< 隐藏关注按钮*/
-(void)linkButtonStatusManager;                                                             /**< 关注按钮*/
- (void)actionClickWithDelMoreBtnBlock:(void(^)())block;                                    /** 删除按钮 */

+(CGFloat)calculationCellHeightWithModel:(ArticleRootSingleModel *)transferSingleModel;

@end
