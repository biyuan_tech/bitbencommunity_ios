//
//  ArticleDraftTitleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ArticleDraftTitleTableViewCell.h"
#import "NetworkAdapter+Article.h"

static char dropCancelReleaseSuccessBlockKey;
static char dropReleaseNowSuccessBlockKey;
@interface ArticleDraftTitleTableViewCell()
@property (nonatomic,strong)PDImageView *imgView;
@property (nonatomic,strong)UILabel *draftTitleLabel;
@property (nonatomic,strong)UIButton *dropButton;
@end


@implementation ArticleDraftTitleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.imgView = [[PDImageView alloc]init];
    self.imgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.imgView];
    
    self.draftTitleLabel = [GWViewTool createLabelFont:@"13" textColor:@"A9A9A9"];
    [self addSubview:self.draftTitleLabel];
    
    self.dropButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.dropButton.backgroundColor = [UIColor redColor];
    [self.dropButton setImage:[UIImage imageNamed:@"icon_article_draft_arrow"] forState:UIControlStateNormal];
    [self addSubview:self.dropButton];
    
    self.imgView.frame = CGRectMake(LCFloat(15), ([ArticleDraftTitleTableViewCell calculationCellHeight] - LCFloat(18)) / 2. , LCFloat(18), LCFloat(18));
    
    self.draftTitleLabel.frame = CGRectMake(CGRectGetMaxX(self.imgView.frame) + LCFloat(12), 0, LCFloat(200), [ArticleDraftTitleTableViewCell calculationCellHeight]);
    
    self.dropButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(12) - [ArticleDraftTitleTableViewCell calculationCellHeight] , 0, [ArticleDraftTitleTableViewCell calculationCellHeight] , [ArticleDraftTitleTableViewCell calculationCellHeight] );
}

-(void)setTransferSingleModel:(ArticleRootSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    NSArray *titlesArr;
    if ([transferSingleModel.timing_status isEqualToString:@"0"]){      // 为定时
        self.imgView.image = [UIImage imageNamed:@"icon_article_draft_clock_nor"];
        self.draftTitleLabel.text = @"未设置定时";
        titlesArr = @[@"取消",@"立即发布"];
    } else if ([transferSingleModel.timing_status isEqualToString:@"2"]){   // 已定时
        self.imgView.image = [UIImage imageNamed:@"icon_article_draft_clock"];
        self.draftTitleLabel.text = [NSString stringWithFormat:@"定时发布 %@",transferSingleModel.timing_datetime];
        titlesArr = @[@"取消",@"取消定时",@"立即发布"];
    } else if ([transferSingleModel.timing_status isEqualToString:@"3"]){   // 已取消
        self.imgView.image = [UIImage imageNamed:@"icon_article_draft_clock_nor"];
        self.draftTitleLabel.text = @"已取消";
        titlesArr = @[@"取消",@"立即发布"];
    }
    
    
    __weak typeof(self)weakSelf = self;
    [self.dropButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf dropButtonActionClickManagerWithType:transferSingleModel.timing_status titleArr:titlesArr];
    }];
}

-(void)dropButtonActionClickManagerWithType:(NSString *)type titleArr:(NSArray *)titleArr{
    __weak typeof(self)weakSelf = self;
    [[UIActionSheet actionSheetWithTitle:@"定时发布" buttonTitles:titleArr callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([type isEqualToString:@"2"]){
            if (buttonIndex == 0){
                [strongSelf articleCancelReleaseManager];
            } else if (buttonIndex == 1){
                [strongSelf articleReleaseNowManager];
            }
        } else {
            if (buttonIndex == 0){
                [strongSelf articleReleaseNowManager];
            }
        }
    }]showInView:self.superview];
}


+(CGFloat)calculationCellHeight{
    return LCFloat(44);
}


#pragma mark - 取消定时发布
-(void)articleCancelReleaseManager{
    __weak typeof(self)weakSelf = self;
    [[UIAlertView alertViewWithTitle:@"取消定时发布" message:@"取消定时发布后无法重新定时，请慎重操作" buttonTitles:@[@"确定撤销",@"取消撤销"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (buttonIndex == 0){
            [strongSelf articleCancelReleaseDetailManager];
        }
    }] show];
}

-(void)articleCancelReleaseDetailManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] articleDraftCancelReleaseWithArticleId:self.transferSingleModel._id block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"已取消定时发布" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            strongSelf.transferSingleModel.timing_status = @"3";
            void(^block)(ArticleRootSingleModel *transferSingleModel) = objc_getAssociatedObject(strongSelf, &dropCancelReleaseSuccessBlockKey);
            if (block){
                block(strongSelf.transferSingleModel);
            }
        }]show];
    }];
}

-(void)dropCancelReleaseSuccessBlock:(void(^)(ArticleRootSingleModel *transferSingleModel))block{
    objc_setAssociatedObject(self, &dropCancelReleaseSuccessBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


#pragma mark - 立即发布
-(void)articleReleaseNowManager{
    __weak typeof(self)weakSelf = self;
    [[UIAlertView alertViewWithTitle:@"立即发布" message:@"该文章是否立即发布" buttonTitles:@[@"立即发布",@"取消"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (buttonIndex == 0){
            [strongSelf articleReleaseNowDetailManager];
        }
    }]show];
}

-(void)articleReleaseNowDetailManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] articleDraftReleaseNowWithArticleId:self.transferSingleModel._id block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"文章已发布" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            void(^block)(ArticleRootSingleModel *transferSingleModel) = objc_getAssociatedObject(strongSelf, &dropReleaseNowSuccessBlockKey);
            if (block){
                block(strongSelf.transferSingleModel);
            }
        }]show];
    }];
}


-(void)dropReleaseNowSuccessBlock:(void(^)(ArticleRootSingleModel *transferSingleModel))block{
    objc_setAssociatedObject(self, &dropReleaseNowSuccessBlockKey, block, OBJC_ASSOCIATION_COPY);
}

@end
