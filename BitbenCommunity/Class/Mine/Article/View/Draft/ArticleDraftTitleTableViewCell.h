//
//  ArticleDraftTitleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ArticleRootSingleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleDraftTitleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)ArticleRootSingleModel *transferSingleModel;

-(void)dropCancelReleaseSuccessBlock:(void(^)(ArticleRootSingleModel *transferSingleModel))block;
-(void)dropReleaseNowSuccessBlock:(void(^)(ArticleRootSingleModel *transferSingleModel))block;


@end

NS_ASSUME_NONNULL_END
