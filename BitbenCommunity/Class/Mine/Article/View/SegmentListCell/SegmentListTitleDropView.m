//
//  SegmentListTitleDropView.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/2.
//  Copyright © 2019 币本. All rights reserved.
//

#import "SegmentListTitleDropView.h"

@interface SegmentListTitleDropView()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *arrowImgView;

@end

@implementation SegmentListTitleDropView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"EE4944"];
    [self addSubview:self.titleLabel];
    
    // arrow
    self.arrowImgView = [[PDImageView alloc]init];
    self.arrowImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.arrowImgView];
    self.arrowImgView.image = [UIImage imageNamed:@"icon_article_segment_arrow_nor1"];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake(0, (self.size_height - titleSize.height) / 2., titleSize.width, titleSize.height);
    
    self.arrowImgView.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame), 0, LCFloat(9), LCFloat(4));
    self.arrowImgView.center_y = self.titleLabel.center_y;
    
    if (self.arrowHasHidden){
        self.titleLabel.orgin_x = LCFloat(5);
    } else {
        self.titleLabel.orgin_x = 0;
    }
}

-(void)setArrowHasHidden:(BOOL)arrowHasHidden{
    _arrowHasHidden = arrowHasHidden;
    if (arrowHasHidden){
        self.arrowImgView.hidden = YES;
    } else {
        self.arrowImgView.hidden = NO;
    }
}

-(void)scrollToItemsWithSelected:(BOOL)selected{
    if (selected){
        self.titleLabel.textColor = [UIColor hexChangeFloat:@"EE4944"];
        self.arrowImgView.image = [UIImage imageNamed:@"icon_article_segment_arrow_nor1"];
    } else {
        self.titleLabel.textColor = [UIColor hexChangeFloat:@"353535"];
        self.arrowImgView.image = [UIImage imageNamed:@"icon_article_segment_arrow_nor1"];
    }
}

-(void)animationArrowTransHasShow:(BOOL)hasShow{
    if (hasShow){
//        self.arrowImgView.transform = CGAffineTransformRotate(self.arrowImgView.transform, M_PI);//旋转180
        self.arrowImgView.image = [UIImage imageNamed:@"icon_article_segment_arrow_hlt"];
    } else {
        self.arrowImgView.image = [UIImage imageNamed:@"icon_article_segment_arrow_nor1"];
//        self.arrowImgView.transform = CGAffineTransformIdentity;//还原
    }
}
@end
