//
//  SegmentListTitleDropView.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/2.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SegmentListTitleDropView : UIView

@property (nonatomic,copy)NSString *transferTitle;
@property (nonatomic,assign)BOOL arrowHasHidden;

-(void)animationArrowTransHasShow:(BOOL)hasShow;
-(void)scrollToItemsWithSelected:(BOOL)selected;
@end

NS_ASSUME_NONNULL_END
