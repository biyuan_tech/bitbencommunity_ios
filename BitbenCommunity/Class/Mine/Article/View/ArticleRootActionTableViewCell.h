//
//  ArticleRootActionTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/9.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "LiveForumRootDingView.h"
#import "ArticleRootSingleModel.h"

@interface ArticleRootActionTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)ArticleRootSingleModel *transferSingleModel;

-(void)actionClickWithShareManagerWithArticleModelBlock:(void(^)(ArticleRootSingleModel *articleModel))block;

@end

