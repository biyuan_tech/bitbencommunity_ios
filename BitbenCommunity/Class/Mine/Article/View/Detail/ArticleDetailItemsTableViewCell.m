//
//  ArticleDetailItemsTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/16.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleDetailItemsTableViewCell.h"
#import "LiveForumRootDingView.h"

static char actionZanManagerBlockKey;
@interface ArticleDetailItemsTableViewCell()
@property (nonatomic,strong)UIButton *biView;
@property (nonatomic,strong)UIButton *upView;
//@property (nonatomic,strong)UIButton *downView;
//@property (nonatomic,strong)UIButton *msgView;
@end

@implementation ArticleDetailItemsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma makr - createView
-(void)createView{
    CGFloat margin = (kScreenBounds.size.width - 2 * LCFloat(100) - LCFloat(44)) / 2.;
    self.biView = [UIButton buttonWithType:UIButtonTypeCustom];
    self.biView.backgroundColor = [UIColor clearColor];
    [self.biView setTitleColor:[UIColor hexChangeFloat:@"323232"] forState:UIControlStateNormal];
    self.biView.titleLabel.font = [UIFont fontWithCustomerSizeName:@"13"];
    self.biView.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.biView.frame = CGRectMake(margin, LCFloat(13), LCFloat(100),LCFloat(32));
    [self.biView setImage:[UIImage imageNamed:@"icon_article_tags_bp_btn"] forState:UIControlStateNormal];
    [self.biView setBackgroundImage:[UIImage imageNamed:@"icon_article_detail_btn_bg_nor"] forState:UIControlStateNormal];
    [self addSubview:self.biView];
    
    self.upView = [UIButton buttonWithType:UIButtonTypeCustom];
    self.upView.backgroundColor = [UIColor clearColor];
    [self.upView setTitleColor:[UIColor hexChangeFloat:@"323232"] forState:UIControlStateNormal];
    [self.upView setBackgroundImage:[UIImage imageNamed:@"icon_article_detail_btn_bg_nor"] forState:UIControlStateNormal];
    self.upView.titleLabel.font = [UIFont fontWithCustomerSizeName:@"13"];
    self.upView.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.upView.frame = CGRectMake(CGRectGetMaxX(self.biView.frame) + LCFloat(44),LCFloat(13), LCFloat(100),LCFloat(32));
    [self addSubview:self.upView];
}

-(void)setTransferArticleModel:(ArticleRootSingleModel *)transferArticleModel{
    _transferArticleModel = transferArticleModel;

    // 1. 币
    [self.biView setTitle:[NSString stringWithFormat:@"%@",transferArticleModel.reward] forState:UIControlStateNormal];
    [self.biView layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleLeft imageTitleSpace:LCFloat(7)];
    
    // 2. 赞
    [self.upView setTitle:[NSString stringWithFormat:@"%li",(long)transferArticleModel.count_support] forState:UIControlStateNormal];
    
    if (transferArticleModel.isSupport){
        [self.upView setImage:[UIImage imageNamed:@"icon_article_detail_tags_zan_hlt"] forState:UIControlStateNormal];
        [self.upView setBackgroundImage:[UIImage imageNamed:@"icon_article_detail_btn_bg_hlt"] forState:UIControlStateNormal];
        [self.upView setTitleColor:[UIColor hexChangeFloat:@"EA6441"] forState:UIControlStateNormal];
    } else {
        [self.upView setImage:[UIImage imageNamed:@"icon_article_detail_tags_zan_nor"] forState:UIControlStateNormal];
        [self.upView setBackgroundImage:[UIImage imageNamed:@"icon_article_detail_btn_bg_nor"] forState:UIControlStateNormal];
        [self.upView setTitleColor:[UIColor hexChangeFloat:@"323232"] forState:UIControlStateNormal];
    }
    [self.upView layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleLeft imageTitleSpace:LCFloat(7)];
    
   
    __weak typeof(self)weakSelf = self;
    [self.upView buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
       __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionZanManagerBlockKey);
        if (block){
            block();
        }
    }];
}

-(void)buttonActionClickManager{
        self.transferArticleModel.isSupport = YES;
        [self.upView setImage:[UIImage imageNamed:@"icon_article_detail_tags_zan_hlt"] forState:UIControlStateNormal];
        [self.upView setTitle:[NSString stringWithFormat:@"%li",(long)self.transferArticleModel.count_support + 1] forState:UIControlStateNormal];
        [self.upView setBackgroundImage:[UIImage imageNamed:@"icon_article_detail_btn_bg_hlt"] forState:UIControlStateNormal];
}


#pragma mark - 文章的顶和踩
-(void)actionClickManagerWithArticleInterface:(UIButton *)button block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    BOOL hasUp = NO;
    if (button == self.upView){
        hasUp = YES;
    }
    [[NetworkAdapter sharedAdapter] articleDingAndCai:self.transferArticleModel._id hasUp:hasUp block:^(BOOL isSuccessed, BOOL isOperate) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (block){
                block();
            }
            if (!isOperate){
                [strongSelf buttonActionClickManager];
            } else {
                if (button == self.upView){
                    [StatusBarManager statusBarHidenWithText:@"你已经顶过该条评论"];
                }
            }
        }
    }];
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(32);
    cellHeight += LCFloat(13) * 2;
    return cellHeight;
}

-(void)actionZanManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionZanManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
