//
//  ArticleDetailTuijianSingleTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleDetailTuijianSingleTableViewCell.h"

@interface ArticleDetailTuijianSingleTableViewCell()
@property (nonatomic,strong)PDImageView *infoImgView;
@property (nonatomic,strong)UILabel *infoLabel;

@end

@implementation ArticleDetailTuijianSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.infoLabel = [GWViewTool createLabelFont:@"16" textColor:@"323232"];
    [self addSubview:self.infoLabel];
    
    self.infoImgView = [[PDImageView alloc]init];
    self.infoImgView.backgroundColor = [UIColor clearColor];
    self.infoImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(20) - LCFloat(120), LCFloat(12), LCFloat(120), LCFloat(80));
    self.infoImgView.clipsToBounds = YES;
    self.infoImgView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.infoImgView];
}

-(void)setTransferArticleRootModel:(ArticleRootSingleModel *)transferArticleRootModel{
    _transferArticleRootModel = transferArticleRootModel;
    NSString *pic = @"";
    if (transferArticleRootModel.picture.count){
        pic = [transferArticleRootModel.picture firstObject];
    }
    __strong typeof(self)weakSelf = self;
    [self.infoImgView uploadMainImageWithURL:pic placeholder:[UIImage imageNamed:@"bg_article_detail_guandian_tuijian_nor"] imgType:PDImgTypeHD callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (!image){
            strongSelf.infoImgView.image = [UIImage imageNamed:@"bg_article_detail_guandian_tuijian_nor"];
        }
    }];
    
    CGFloat width = self.infoImgView.orgin_x - LCFloat(10) - LCFloat(20);
    CGSize titleSize = [transferArticleRootModel.title sizeWithCalcFont:self.infoLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (titleSize.height >= 2 * [NSString contentofHeightWithFont:self.infoLabel.font]){
        self.infoLabel.frame = CGRectMake(LCFloat(20), ([ArticleDetailTuijianSingleTableViewCell calculationCellHeight] - 2 * [NSString contentofHeightWithFont:self.infoLabel.font]) / 2., width, titleSize.height);
        self.infoLabel.numberOfLines = 2;
    } else {
        self.infoLabel.frame = CGRectMake(LCFloat(20), ([ArticleDetailTuijianSingleTableViewCell calculationCellHeight] - 2 * [NSString contentofHeightWithFont:self.infoLabel.font]) / 2., width, titleSize.height);
        self.infoLabel.numberOfLines = 1;
    }
    
    self.infoLabel.text = transferArticleRootModel.title;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(105);
}

@end
