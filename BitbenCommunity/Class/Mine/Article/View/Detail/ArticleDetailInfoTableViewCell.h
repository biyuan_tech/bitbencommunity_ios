//
//  ArticleDetailInfoTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/16.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ArticleRootTableViewCell.h"
#import "ArticleRootSingleModel.h"

@interface ArticleDetailInfoTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)WKWebView *webView;
@property (nonatomic,strong)ArticleRootSingleModel *transferArticleModel;

// 选中的图片
@property (nonatomic,strong)PDImageView *selectedImgView;
@property (nonatomic,strong)ArticleRootSingleImgBgView *imgBgView;

-(void)actionClickWithImgSelectedBlock:(void(^)(NSString *imgUrl))block;
-(void)actionWebViewDidLoad:(void(^)())block;

-(void)actionClickWithWebImgBlock:(void(^)(NSInteger index,NSString *imgUrl,CGRect rect,NSArray *imgArr))block;

-(void)actionClickWithWebUrlDirectBlock:(void(^)(NSString *webUrl))block;

+(CGFloat)calculationCellHeightWithModel:(ArticleRootSingleModel *)transferArticleModel;

@end
