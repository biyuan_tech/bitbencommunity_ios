//
//  ArticleDetailTitleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/15.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

@interface ArticleDetailTitleTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,assign)CGFloat transferSpacing;            /**< 行间距*/

+(CGFloat)calculationCellHeightTitle:(NSString *)title;
+(CGFloat)calculationCellHeightWithLongTextTitle:(NSString *)title;

@end
