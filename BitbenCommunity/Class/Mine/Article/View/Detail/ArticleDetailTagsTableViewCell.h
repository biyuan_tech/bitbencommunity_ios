//
//  ArticleDetailTagsTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/16.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ArticleRootSingleModel.h"

@interface ArticleDetailTagsTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)ArticleRootSingleModel *transferArticleModel;

+(CGFloat)calculationCellHeight:(ArticleRootSingleModel *)transferArticleModel;


// temp 2
@property (nonatomic,strong)NSArray *transferTagsArr;

+(CGFloat)calculationCellHeightWithArr:(NSArray *)tagsArr;

-(void)actionItemClickWithBlock:(void(^)(NSString *info))block;


@end
