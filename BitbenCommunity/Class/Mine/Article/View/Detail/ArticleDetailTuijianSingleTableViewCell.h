//
//  ArticleDetailTuijianSingleTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ArticleRootSingleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleDetailTuijianSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)ArticleRootSingleModel *transferArticleRootModel;

@end

NS_ASSUME_NONNULL_END
