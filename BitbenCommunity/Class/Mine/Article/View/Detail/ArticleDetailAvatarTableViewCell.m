//
//  ArticleDetailAvatarTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/16.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleDetailAvatarTableViewCell.h"

static char actionLinkButtonClickKey;
static char actionClickHeaderImgWithBlockKey;
@interface ArticleDetailAvatarTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)PDImageView *avatarConvertView;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *dateLabel;
@property (nonatomic,strong)UIButton *linkButton;
@property (nonatomic,strong)UILabel *huatiLabel;
/** 承载成就图标 */
@property (nonatomic ,strong) UIView *achievementView;
/** 原创标签 */
@property (nonatomic ,strong) UIImageView *originalImgView;
@end

@implementation ArticleDetailAvatarTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakself = self;
    [self.avatarImgView addTapGestureRecognizer:^{
        if (!weakself){
            return ;
        }
        __strong typeof(weakself)strongSelf = weakself;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickHeaderImgWithBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.avatarImgView];
    
    self.avatarConvertView = [[PDImageView alloc]init];
    self.avatarConvertView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarConvertView];
    
    self.nickNameLabel = [GWViewTool createLabelFont:@"14" textColor:@"343434"];
    [self addSubview:self.nickNameLabel];
    
    // 承载成就view
    self.achievementView = [[UIView alloc] init];
    [self.contentView addSubview:self.achievementView];
    
    self.dateLabel = [GWViewTool createLabelFont:@"11" textColor:@"A2A2A2"];
    [self addSubview:self.dateLabel];
    
    // 原创标签
    self.originalImgView = [UIImageView by_init];
    [self.originalImgView by_setImageName:@"livehome_article_original"];
    [self.contentView addSubview:self.originalImgView];
    
    self.linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.linkButton];
    [self.linkButton buttonWithBlock:^(UIButton *button) {
        if (!weakself){
            return ;
        }
        __strong typeof(weakself)strongSelf = weakself;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionLinkButtonClickKey);
        if (block){
            block();
        }
    }];
}

-(void)setTransferArticleModel:(ArticleRootSingleModel *)transferArticleModel{
    _transferArticleModel = transferArticleModel;
    
    self.avatarImgView.style = transferArticleModel.cert_badge;
    // avatar
    self.avatarImgView.frame = CGRectMake(LCFloat(17), LCFloat(7), LCFloat(37), LCFloat(37));
    __weak typeof(self)weakSelf = self;
    [self.avatarImgView uploadImageWithRoundURL:transferArticleModel.head_img placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.transferArticleModel.avatarRoundImg = image;
    }];
    
    // avatarConvert
    self.avatarConvertView.frame = self.avatarImgView.frame;
    
    self.nickNameLabel.text = transferArticleModel.nickname;
    CGSize nickNameSize = [Tool makeSizeWithLabel:self.nickNameLabel];
    CGFloat originX = CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11);
    CGFloat maxWidth = kCommonScreenWidth - originX - 68 - LCFloat(15) - LCFloat(54);
    CGFloat width = nickNameSize.width > maxWidth ? maxWidth : nickNameSize.width;
    self.nickNameLabel.frame = CGRectMake(originX, 0, width, nickNameSize.height);
    
    self.dateLabel.text = [NSDate getTimeGap:transferArticleModel.create_time / 1000.];
    CGSize dateSize = [Tool makeSizeWithLabel:self.dateLabel];
    self.dateLabel.frame = CGRectMake(self.nickNameLabel.orgin_x, 9, dateSize.width, dateSize.height);
    
    self.originalImgView.hidden = !transferArticleModel.is_author;
    self.originalImgView.frame = CGRectMake(CGRectGetMaxX(self.dateLabel.frame) + 8, 0, 24, 14);
    
    CGFloat margin = (self.avatarImgView.size_height - nickNameSize.height - dateSize.height) / 3.;
    self.nickNameLabel.orgin_y = self.avatarImgView.orgin_y + margin;
    self.dateLabel.orgin_y = CGRectGetMaxY(self.nickNameLabel.frame) + margin;
    self.originalImgView.center_y = self.dateLabel.center_y;
    
    // 添加成就
    self.achievementView.frame = CGRectMake(CGRectGetMaxX(_nickNameLabel.frame) + 5,
                                            0, 58, 16);
    self.achievementView.center = CGPointMake(self.achievementView.center_x, self.nickNameLabel.center_y);
    
    for (UIView *subView in self.achievementView.subviews) {
        [subView removeFromSuperview];
    }
    
    NSArray *achievementData = transferArticleModel.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [self.achievementView addSubview:imageView];
    }
    
    // linkButton
    self.linkButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(54), 0, LCFloat(54), LCFloat(26));
    self.linkButton.center_y = self.avatarImgView.center_y;
    if (transferArticleModel.isAttention){
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_nor"] forState:UIControlStateNormal];
    } else {
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_hlt"] forState:UIControlStateNormal];
    }
    
    if ([transferArticleModel.author_id isEqualToString:[AccountModel sharedAccountModel].loginServerModel.user._id]){
        self.linkButton.hidden = YES;
    } else {
        self.linkButton.hidden = NO;
    }
}


-(void)btnStatusSelected:(BOOL)isSelected{
    self.transferArticleModel.isAttention = isSelected;
    if (isSelected){
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_nor"] forState:UIControlStateNormal];
    } else {
        [self.linkButton setImage:[UIImage imageNamed:@"icon_article_link_hlt"] forState:UIControlStateNormal];
    }
}

-(void)actionLinkButtonClick:(void(^)())block{
    objc_setAssociatedObject(self, &actionLinkButtonClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)actionClickHeaderImgWithBlock:(void (^)(void))block{
    objc_setAssociatedObject(self, &actionClickHeaderImgWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(37);
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end
