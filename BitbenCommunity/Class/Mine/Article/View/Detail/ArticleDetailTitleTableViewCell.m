//
//  ArticleDetailTitleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/15.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleDetailTitleTableViewCell.h"
#define UILABEL_LINE_SPACE 6
#define HEIGHT [ [ UIScreen mainScreen ] bounds ].size.height

@interface ArticleDetailTitleTableViewCell()

@end

@implementation ArticleDetailTitleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"16" textColor:@"212121"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.titleLabel];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
    
    if (self.transferSpacing){          // 表示是短文
        [self setLabelSpace:self.titleLabel withValue:transferTitle withFont:self.titleLabel.font];
        CGFloat cellHeight = [ArticleDetailTitleTableViewCell getSpaceLabelHeight:self.titleLabel.text withFont:[UIFont fontWithCustomerSizeName:@"16"] withWidth:kScreenBounds.size.width - 2 * LCFloat(11)];
        
        self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), cellHeight);
        
    } else {                            // 表示是长文
        
        CGSize  titleSize = [transferTitle sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
         self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), titleSize.height);
    }
}

-(void)setTransferSpacing:(CGFloat)transferSpacing{
    _transferSpacing = transferSpacing;
}

+(CGFloat)calculationCellHeightTitle:(NSString *)title{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [ArticleDetailTitleTableViewCell getSpaceLabelHeight:title withFont:[UIFont fontWithCustomerSizeName:@"16"] withWidth:kScreenBounds.size.width - 2 * LCFloat(11)];
    cellHeight += LCFloat(11);
    return cellHeight;
}

+(CGFloat)calculationCellHeightWithLongTextTitle:(NSString *)title{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    CGSize titleSize = [title sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"16"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    cellHeight += titleSize.height;
    cellHeight += LCFloat(11);
    return cellHeight;
}



-(void)setLabelSpace:(UILabel*)label withValue:(NSString*)str withFont:(UIFont*)font {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = UILABEL_LINE_SPACE; //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f
};
    
    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
    label.attributedText = attributeStr;
}


+(CGFloat)getSpaceLabelHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = UILABEL_LINE_SPACE;
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f
};
    
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, HEIGHT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return size.height;
}

@end
