//
//  ArticleDetailInfoTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/16.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleDetailInfoTableViewCell.h"

static NSString *const DAWebViewDemoScheme = @"darkangel";
static NSString *const DAWebViewDemoHostSmsLogin = @"smsLogin";
static NSString *const DAServerSessionCookieName = @"DarkAngelCookie";
static NSString *const DAUserDefaultsCookieStorageKey = @"DAUserDefaultsCookieStorageKey";
static NSString *const DAURLProtocolHandledKey = @"DAURLProtocolHandledKey";

static char actionClickWithWebImgBlockKey;
static char actionClickWithImgSelectedBlockKey;
static char actionWebViewDidLoadKey;
static char actionClickWithWebUrlDirectBlockKey;
@interface ArticleDetailInfoTableViewCell()<WKUIDelegate,
WKNavigationDelegate,
WKScriptMessageHandler>

@end

@implementation ArticleDetailInfoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self creatSubviews];
    }
    return self;
}

- (void)creatSubviews {
    // 加载原生内容
    self.imgBgView = [[ArticleRootSingleImgBgView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 10)];
    self.imgBgView.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.imgBgView actionImgClickBlock:^(NSString *imgUrl, PDImageView *imgView) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.selectedImgView = imgView;
        void(^block)(NSString *tempImgUrl) = objc_getAssociatedObject(strongSelf, &actionClickWithImgSelectedBlockKey);
        if (block){
            block(imgUrl);
        }
    }];
    [self addSubview:self.imgBgView];
    
    // 加载webview
    [self initWKWebView];
}

-(void)actionClickWithImgSelectedBlock:(void(^)(NSString *imgUrl))block{
    objc_setAssociatedObject(self, &actionClickWithImgSelectedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


-(void)setTransferArticleModel:(ArticleRootSingleModel *)transferArticleModel{
    _transferArticleModel = transferArticleModel;
    
    if (transferArticleModel.article_type == article_typeWeb || transferArticleModel.article_type == article_typeInfo){
        [self loadWebWithModel:transferArticleModel];
        self.webView.hidden = NO;
        self.imgBgView.hidden = YES;
    } else if (transferArticleModel.article_type == article_typeNormal){
        [self loadNativeWithModel:transferArticleModel];
        self.webView.hidden = YES;
        self.imgBgView.hidden = NO;
    }
}


- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
    __weak typeof(self)weakSelf = self;
    if (self.transferArticleModel.webLoadTempModel.hasLoad == NO){
        [webView evaluateJavaScript:@"document.getElementById(\"testDiv\").offsetTop"completionHandler:^(id _Nullable result,NSError * _Nullable error) {
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            //获取页面高度，并重置webview的frame
            CGFloat lastHeight  = [result doubleValue] + 50;
            if (strongSelf.transferArticleModel.webLoadTempModel){
                strongSelf.transferArticleModel.webLoadTempModel.hasLoad = YES;
                strongSelf.transferArticleModel.webLoadTempModel.transferCellHeight = lastHeight;
            }

            void(^block)() = objc_getAssociatedObject(strongSelf, &actionWebViewDidLoadKey);
            if (block){
                block();
            }
        }];
    }
}


-(void)actionWebViewDidLoad:(void(^)())block{
    objc_setAssociatedObject(self, &actionWebViewDidLoadKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


#pragma mark - 1. 如果是网页加载
-(void)loadWebWithModel:(ArticleRootSingleModel *)transferArticleModel{
    TSHomeClassDetailJieshaoWebTableViewCellModel *transferModel = transferArticleModel.webLoadTempModel;
    if (transferModel.hasLoad){
        self.webView.size_height = transferModel.transferCellHeight;
    } else {
        
        NSString *documentDir = [[NSBundle mainBundle] pathForResource:@"layui.css" ofType:nil];//
        NSString *resultStr = [NSString stringWithContentsOfFile:documentDir encoding:NSUTF8StringEncoding error:nil];

        NSString *smartUrl = @"";
        smartUrl = @"<meta content=\"width=device-width, initial-scale=1.0, maximum-scale=3.0, user-scalable=0;\" name=\"viewport\" /> ";
        smartUrl = [smartUrl stringByAppendingString:resultStr];
        smartUrl = [smartUrl stringByAppendingString:@"<div class=\"ql-container ql-snow\"><div class=\"ql-editor\">"];
        smartUrl = [smartUrl stringByAppendingString:transferModel.html];
        smartUrl = [smartUrl stringByAppendingString:@"<div id=\"testDiv\" style = \"height:0px; width:1px\">"];
        smartUrl = [smartUrl stringByAppendingString:@"</div>"];
        smartUrl = [smartUrl stringByAppendingString:@"</div>"];
        smartUrl = [smartUrl stringByAppendingString:@"</div>"];

        [_webView loadHTMLString:smartUrl baseURL:[NSURL fileURLWithPath: [[NSBundle mainBundle]  bundlePath]]];
    }
}

#pragma mark - 2.如果是native
-(void)loadNativeWithModel:(ArticleRootSingleModel *)transferModel{
    // imgBgView
    if (![self.imgBgView.transferImgArr containsObject:[transferModel.picture firstObject]]){
        self.imgBgView.transferImgArr = transferModel.picture;
        self.imgBgView.size_height = [ArticleRootSingleImgBgView calculationHeight:transferModel.picture];
        self.imgBgView.clipsToBounds = YES;
    }
}

+(CGFloat)calculationCellHeightWithModel:(ArticleRootSingleModel *)transferArticleModel{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    CGFloat mainHeight = [ArticleRootSingleImgBgView calculationHeight:transferArticleModel.picture];
    cellHeight += mainHeight;
    cellHeight += LCFloat(11);
    return cellHeight;
}

#pragma mark - 初始化webview
- (void)initWKWebView {

    // Do any additional setup after loading the view.
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];

    //自定义脚本等
    WKUserContentController *controller = [[WKUserContentController alloc] init];
    //添加js全局变量
    WKUserScript *script = [[WKUserScript alloc] initWithSource:@"var interesting = 123;" injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
    //页面加载完成立刻回调，获取页面上的所有Cookie
    WKUserScript *cookieScript = [[WKUserScript alloc] initWithSource:@"                window.webkit.messageHandlers.currentCookies.postMessage(document.cookie);" injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
    //alert Cookie
    //    WKUserScript *alertCookieScript = [[WKUserScript alloc] initWithSource:@"alert(document.cookie);" injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
    //添加自定义的cookie
    WKUserScript *newCookieScript = [[WKUserScript alloc] initWithSource:@"                document.cookie = 'DarkAngelCookie=DarkAngel;'" injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
    
    //添加脚本
    [controller addUserScript:script];
    [controller addUserScript:cookieScript];
    //    [controller addUserScript:alertCookieScript];
    [controller addUserScript:newCookieScript];
    //注册回调
    [controller addScriptMessageHandler:self name:@"share"];
    [controller addScriptMessageHandler:self name:@"currentCookies"];
    [controller addScriptMessageHandler:self name:@"shareNew"];
    
    configuration.userContentController = controller;
    
    self.webView = [[WKWebView alloc] initWithFrame:CGRectMake(14, 0, kScreenBounds.size.width - 28, 1) configuration:configuration];

    self.webView.backgroundColor = [UIColor clearColor];

    _webView.userInteractionEnabled = YES;
    _webView.opaque = NO;
    _webView.scrollView.bounces=NO;
    _webView.scrollView.scrollEnabled = NO;
    _webView.scrollView.decelerationRate=UIScrollViewDecelerationRateNormal;
    _webView.UIDelegate = self;
    _webView.navigationDelegate = self;

    [self.contentView addSubview:self.webView];

    //图片添加点击事件
    [self imgAddClickEvent];
    // 添加内容
    [self addNativeApiToJS];
}

#pragma mark - Events

/**
 页面中的所有img标签添加点击事件
 */
- (void)imgAddClickEvent {
    //防止频繁IO操作，造成性能影响
    static NSString *jsSource;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        jsSource = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ImgAddClickEvent" ofType:@"js"] encoding:NSUTF8StringEncoding error:nil];
    });
    //添加自定义的脚本
    WKUserScript *js = [[WKUserScript alloc] initWithSource:jsSource injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
    [self.webView.configuration.userContentController addUserScript:js];
    //注册回调
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:@"imageDidClick"];
}

/**
 添加native端的api
 */
- (void)addNativeApiToJS
{
    //防止频繁IO操作，造成性能影响
    static NSString *nativejsSource;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        nativejsSource = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"NativeApi" ofType:@"js"] encoding:NSUTF8StringEncoding error:nil];
    });
    //添加自定义的脚本
    WKUserScript *js = [[WKUserScript alloc] initWithSource:nativejsSource injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
    [self.webView.configuration.userContentController addUserScript:js];
    //注册回调
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:@"nativeShare"];
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:@"nativeChoosePhoneContact"];
}

#pragma mark - WKScriptMessageHandler  js -> oc

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    if ([message.name isEqualToString:@"share"]) {
        id body = message.body;
        NSLog(@"share分享的内容为：%@", body);
    }
    else if ([message.name isEqualToString:@"shareNew"] || [message.name isEqualToString:@"nativeShare"]) {
        NSDictionary *shareData = message.body;
        NSLog(@"%@分享的数据为： %@", message.name, shareData);
        //模拟异步回调
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //读取js function的字符串
            NSString *jsFunctionString = shareData[@"result"];
            //拼接调用该方法的js字符串
            NSString *callbackJs = [NSString stringWithFormat:@"(%@)(%d);", jsFunctionString, NO];    //后面的参数NO为模拟分享失败
            //执行回调
            [self.webView evaluateJavaScript:callbackJs completionHandler:^(id _Nullable result, NSError * _Nullable error) {
                if (!error) {
                    NSLog(@"模拟回调，分享失败");
                }
            }];
        });
    }
    else if ([message.name isEqualToString:@"currentCookies"]) {
        NSString *cookiesStr = message.body;
        NSLog(@"当前的cookie为： %@", cookiesStr);
    }
    else if ([message.name isEqualToString:@"imageDidClick"]) {
        
        NSDictionary *dict = message.body;
        NSString *selectedImageUrl = dict[@"imgUrl"];
        CGFloat x = [dict[@"x"] floatValue] + + self.webView.scrollView.contentInset.left;
        CGFloat y = [dict[@"y"] floatValue] + self.webView.scrollView.contentInset.top;
        CGFloat width = [dict[@"width"] floatValue];
        CGFloat height = [dict[@"height"] floatValue];
        CGRect frame = CGRectMake(x, y, width, height);
        NSUInteger index = [dict[@"index"] integerValue];
        NSLog(@"点击了第%@个图片，\n链接为%@，\n在Screen中的绝对frame为%@，\n所有的图片数组为%@", @(index), selectedImageUrl, NSStringFromCGRect(frame), dict[@"imgUrls"]);
        NSArray *imgArr = dict[@"imgUrls"];
        
        void(^block)(NSInteger blockIndex,NSString *imgUrl,CGRect rect,NSArray *imgArr) = objc_getAssociatedObject(self, &actionClickWithWebImgBlockKey);
        if (block){
            block(index,selectedImageUrl,frame,imgArr);
        }
    }
    //选择联系人
    else if ([message.name isEqualToString:@"nativeChoosePhoneContact"]) {
        NSLog(@"正在选择联系人");
    
    }
}

-(void)actionClickWithWebImgBlock:(void(^)(NSInteger blockIndex,NSString *imgUrl,CGRect rect,NSArray *imgArr))block{
    objc_setAssociatedObject(self, &actionClickWithWebImgBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
















#pragma mark 选择联系人

- (void)selectContactCompletion:(void (^)(NSString *name, NSString *phone))completion
{
    NSLog(@"123");
}

#pragma mark oc -> js
/**
 测试evaluateJavaScript方法
 */
- (IBAction)testEvaluateJavaScript {
    
    [self.webView evaluateJavaScript:@"document.cookie" completionHandler:^(id _Nullable cookies, NSError * _Nullable error) {
        NSLog(@"调用evaluateJavaScript异步获取cookie：%@", cookies);
    }];
    
    // do not use dispatch_semaphore_t
    /*
     __block id cookies;
     dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
     [self.webView evaluateJavaScript:@"document.cookie" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
     cookies = result;
     dispatch_semaphore_signal(semaphore);
     }];
     //等待三秒，接收参数
     dispatch_semaphore_wait(semaphore, dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC));
     //打印cookie，肯定为空，因为足足等了3s，dispatch_semaphore_signal没有起作用
     NSLog(@"cookie的值为：%@", cookies);
     
     //还是老实的接受异步回调吧，不要用信号来搞成同步，会卡死的，不信可以试试
     */
}

#pragma mark - WKNavigationDelegate 方法按调用前后顺序排序

//针对一次action来决定是否允许跳转，允许与否都需要调用decisionHandler，比如decisionHandler(WKNavigationActionPolicyCancel);
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    //可以通过navigationAction.navigationType获取跳转类型，如新链接、后退等
    NSURL *URL = navigationAction.request.URL;
    //判断URL是否符合自定义的URL Scheme
    if ([URL.scheme isEqualToString:DAWebViewDemoScheme]) {
        //根据不同的业务，来执行对应的操作，且获取参数
        if ([URL.host isEqualToString:DAWebViewDemoHostSmsLogin]) {
            NSString *param = URL.query;
            NSLog(@"短信验证码登录, 参数为%@", param);
            decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }
    }
    
    if ([URL.absoluteString hasPrefix:@"http"]){
        void(^block)(NSString *urlStr) = objc_getAssociatedObject(self, &actionClickWithWebUrlDirectBlockKey);
        if (block){
            block([NSString stringWithFormat:@"%@",URL.absoluteString]);
        }
    }
    
#warning important 这里很重要
    //解决Cookie丢失问题
    NSURLRequest *originalRequest = navigationAction.request;
//    [self fixRequest:originalRequest];
    //如果originalRequest就是NSMutableURLRequest, originalRequest中已添加必要的Cookie，可以跳转
    //允许跳转
    decisionHandler(WKNavigationActionPolicyAllow);





}


-(void)actionClickWithWebUrlDirectBlock:(void(^)(NSString *webUrl))block{
    objc_setAssociatedObject(self, &actionClickWithWebUrlDirectBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

//根据response来决定，是否允许跳转，允许与否都需要调用decisionHandler，如decisionHandler(WKNavigationResponsePolicyAllow);
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
    NSLog(@"%@", NSStringFromSelector(_cmd));
    decisionHandler(WKNavigationResponsePolicyAllow);
}

//提交了一个跳转，早于 didStartProvisionalNavigation
- (void)webView:(WKWebView *)webView didCommitNavigation:(null_unspecified WKNavigation *)navigation
{
    NSLog(@"%@", NSStringFromSelector(_cmd));
}

//开始加载，对应UIWebView的- (void)webViewDidStartLoad:(UIWebView *)webView;
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSLog(@"%@", NSStringFromSelector(_cmd));
}


//页面加载失败或者跳转失败，对应UIWebView的- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error;
- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSLog(@"%@\nerror：%@", NSStringFromSelector(_cmd), error);
}

//页面加载数据时报错
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSLog(@"%@\nerror：%@", NSStringFromSelector(_cmd), error);
}

#pragma mark - WKUIDelegate

- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures
{
#warning important 这里也很重要
    //这里不打开新窗口
//    [self.webView loadRequest:[self fixRequest:navigationAction.request]];
    return nil;
}

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(nonnull void (^)(void))completionHandler
{
    //js 里面的alert实现，如果不实现，网页的alert函数无效
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }]];
//    [self presentViewController:alertController animated:YES completion:^{}];
}

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler {
    //  js 里面的alert实现，如果不实现，网页的alert函数无效  ,
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        completionHandler(NO);
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        completionHandler(YES);
    }]];
//    [self presentViewController:alertController animated:YES completion:^{}];
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString *))completionHandler
{
    //用于和JS交互，弹出输入框
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:prompt message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        completionHandler(nil);
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *textField = alertController.textFields.firstObject;
        completionHandler(textField.text);
    }]];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = defaultText;
    }];
//    [self presentViewController:alertController animated:YES completion:NULL];
}

@end


