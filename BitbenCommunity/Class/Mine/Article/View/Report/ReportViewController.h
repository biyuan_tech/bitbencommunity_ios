//
//  ReportViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/5.
//  Copyright © 2018 币本. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportViewController : AbstractViewController

// 相关属性
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势

- (void)showInView:(UIViewController *)viewController type:(BY_THEME_TYPE)type itemId:(NSString *)itemId;
- (void)dismissFromView:(UIViewController *)viewController;

@end

NS_ASSUME_NONNULL_END
