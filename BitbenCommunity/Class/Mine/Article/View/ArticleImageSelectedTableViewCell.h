//
//  ArticleImageSelectedTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/10.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

@class ArticleImageSelectedTableViewCell;

@protocol ArticleImageSelectedTableViewCellDelegate <NSObject>

-(void)imageSelectedButtonClick:(ArticleImageSelectedTableViewCell *)cell;
-(void)imageSelectedDeleteClick:(NSInteger)imgIndex imageView:(UIView *)imgView andSuperCell:(ArticleImageSelectedTableViewCell *)cell;
-(void)imageSelectedShowAll:(NSInteger)imgIndex imageView:(UIView *)imgView andSuperCell:(ArticleImageSelectedTableViewCell *)cell;

@end

@interface ArticleImageSelectedTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)NSArray *transferSelectedImgArr;
@property (nonatomic,weak)id<ArticleImageSelectedTableViewCellDelegate> delegate;
@property (nonatomic,assign)NSInteger selectedImgCount;
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)PDImageView *selectedImgView;

+(CGFloat)calculationCellHeightWithImgArr:(NSArray *)isSelectedImageArr;

@end
