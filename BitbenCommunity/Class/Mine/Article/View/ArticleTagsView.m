//
//  ArticleTagsView.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/10/13.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "ArticleTagsView.h"

@interface ArticleTagsView()
@end

static char actionClickWithTagsBlockKey;
@implementation ArticleTagsView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.userInteractionEnabled = YES;
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.userInteractionEnabled = YES;
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 100);
    [self addSubview:self.bgView];
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    self.bgView.size_height = transferCellHeight;
}

-(void)setHasEdit:(BOOL)hasEdit{
    _hasEdit = hasEdit;
}

-(void)setTransferArr:(NSArray *)transferArr{
    _transferArr = transferArr;
    
    NSString *huatiStr = @"话题";
//
    CGSize huatiSize = [huatiStr sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"]  constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]])];
    
    
    CGFloat flagOriginX = LCFloat(3);
    CGFloat flagOriginY = LCFloat(0);
    CGFloat flagMargin = LCFloat(10);
    
    CGFloat flagWidth = 0;
    if (self.hasEdit){          // 编辑状态
        flagWidth = kScreenBounds.size.width - 2 * LCFloat(11);
    } else {
        flagWidth = kScreenBounds.size.width - LCFloat(11) - huatiSize.width - 2 * LCFloat(11) - LCFloat(40);
    }
    if (self.hasRelease){
        flagWidth = kScreenBounds.size.width - LCFloat(11) - huatiSize.width - 2 * LCFloat(11) - LCFloat(40);
    }
    
    self.bgView.size_width = flagWidth;
    self.size_width = flagWidth;
    
    if (self.bgView.subviews.count){
        [self.bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    for (int i = 0 ; i < transferArr.count;i++){
        NSString *info = [transferArr objectAtIndex:i];
        UIView *flagImageView = [self createTagViewWithTitle:info];
        if (flagImageView.size_width < flagWidth){                      // 不换行
            flagImageView.frame = CGRectMake(flagOriginX, flagOriginY, flagImageView.size_width, flagImageView.size_height);
            flagWidth -= (flagImageView.size_width + flagMargin);
            flagOriginX += (flagImageView.size_width + flagMargin);
        } else {                                                        // 换行
            flagOriginY += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]] + 2 * LCFloat(4) + flagMargin;
            flagOriginX = LCFloat(3);
            flagWidth = kScreenBounds.size.width - 2 * flagMargin;
            flagImageView.frame = CGRectMake(flagOriginX, flagOriginY, flagImageView.size_width, flagImageView.size_height);
            flagWidth -= (flagImageView.size_width + flagMargin);
            flagOriginX += (flagImageView.size_width + flagMargin);
        }
        [self.bgView addSubview:flagImageView];
    }
    if (self.hasRelease){
        self.bgView.size_height = [ArticleTagsView calculationCellHeightWithArr:transferArr hasEdit:YES hasCreate:YES];
        self.size_height = self.bgView.size_height;
    } else {
        self.bgView.size_height = [ArticleTagsView calculationCellHeightWithArr:transferArr hasEdit:YES];
        self.size_height = self.bgView.size_height;
    }
}

#pragma mark - createTagView
-(UIView *)createTagViewWithTitle:(NSString *)info{
    PDImageView *tagView = [[PDImageView alloc]init];
//    tagView.backgroundColor = UURandomColor;
    tagView.userInteractionEnabled = YES;
    
    //1. 创建文字
    UILabel *flagLabel = [[UILabel alloc]init];
    flagLabel.backgroundColor = [UIColor clearColor];
    flagLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    flagLabel.userInteractionEnabled = YES;
    flagLabel.textColor = [UIColor colorWithCustomerName:@"5A7FAC"];
    flagLabel.text = info;
    CGSize flagLabelSize = [flagLabel.text sizeWithCalcFont:flagLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:flagLabel.font])];
    [tagView addSubview:flagLabel];
    
    if (self.hasEdit){
        [tagView setImage:[Tool stretchImageWithName:@"icon_tags_bg"]];
        flagLabel.frame = CGRectMake(LCFloat(7), 0, flagLabelSize.width, 2 * LCFloat(4) + [NSString contentofHeightWithFont:flagLabel.font]);
        tagView.bounds = CGRectMake(0, 0, flagLabel.size_width + 2 * LCFloat(7), 2 * LCFloat(4) + [NSString contentofHeightWithFont:flagLabel.font] );
    } else {
        tagView.image = [Tool stretchImageWithName:@"icon_article_huati_bg"];
        flagLabel.frame = CGRectMake(LCFloat(7), 0, flagLabelSize.width, 2 * LCFloat(4) + [NSString contentofHeightWithFont:flagLabel.font]);
        tagView.bounds = CGRectMake(0, 0, flagLabel.size_width + 2 * LCFloat(7), 2 * LCFloat(4) + [NSString contentofHeightWithFont:flagLabel.font] );
    }
    
    
    // 1. 添加取消a内容
    if ((![info isEqualToString:@"+ 选择话题"])){
        if (self.hasEdit){
            PDImageView *cancelImgView = [[PDImageView alloc]init];
            cancelImgView.image = [UIImage imageNamed:@"icon_tags_delete"];
            cancelImgView.frame = CGRectMake(CGRectGetMaxX(flagLabel.frame) + LCFloat(6), 0, LCFloat(8), LCFloat(8));
            cancelImgView.center_y = flagLabel.center_y;
            [tagView addSubview:cancelImgView];
            tagView.size_width += LCFloat(8) + LCFloat(6);
        }
    }
    
    // 2. 创建按钮
    UIButton *selectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    selectedButton.backgroundColor = [UIColor clearColor];
    selectedButton.userInteractionEnabled = YES;
    __weak typeof(self)weakSelf = self;
    [selectedButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSString *info) = objc_getAssociatedObject(strongSelf, &actionClickWithTagsBlockKey);
        if (block){
            block(info);
        }
    }];
    [tagView addSubview:selectedButton];
    
    selectedButton.frame = tagView.bounds;
    
    return tagView;
}


+(CGFloat)calculationCellHeightWithArr:(NSArray *)transferArr hasEdit:(BOOL)hasEdit hasCreate:(BOOL)hasCreate{
    CGFloat cellHeight = 0;
    
    NSString *huatiStr = @"话题";
    CGSize huatiSize = [huatiStr sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"]  constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]])];
    
    CGFloat flagOriginX = LCFloat(3);
    CGFloat flagOriginY = LCFloat(0);
    CGFloat flagMargin = LCFloat(10);
    
    
    CGFloat flagWidth = 0;
    if (hasEdit){          // 编辑
        flagWidth = kScreenBounds.size.width - 2 * LCFloat(11);
    } else {
        flagWidth = kScreenBounds.size.width - LCFloat(11) - huatiSize.width - 2 * LCFloat(11);
    }
    
    if (hasCreate){
         flagWidth = kScreenBounds.size.width - LCFloat(11) - huatiSize.width - 2 * LCFloat(11) - LCFloat(40);
    }
    
    
    for (int i = 0 ; i < transferArr.count;i++){
        NSString *info = [transferArr objectAtIndex:i];
        CGSize floagSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]])];
        CGFloat floagWidth = floagSize.width + 2 * LCFloat(11);
        
        if (hasEdit){
            floagWidth = floagSize.width + 2 * LCFloat(7);
        } else {
            floagWidth = floagSize.width;
        }
        if (hasCreate ){
            floagWidth =floagSize.width + 2 * LCFloat(7);
        }
        
        
        if (floagWidth < flagWidth){                      // 不换行
            flagWidth -= (floagWidth + flagMargin);
            flagOriginX += (floagWidth + flagMargin);
        } else {
            flagOriginY += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]] + 2 * LCFloat(4) + flagMargin;
            flagOriginX =  LCFloat(3);
            flagWidth = kScreenBounds.size.width - 2 * flagMargin;
            flagWidth -= (floagWidth + flagMargin);
            flagOriginX += (floagWidth + flagMargin);
            cellHeight = flagOriginY;
        }
        
    }
    
    
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]] + 2 * LCFloat(4) ;
    
    return cellHeight;
}

+(CGFloat)calculationCellHeightWithArr:(NSArray *)transferArr hasEdit:(BOOL)hasEdit{
    
    CGFloat cellHeight = 0;
    
    NSString *huatiStr = @"话题";
    CGSize huatiSize = [huatiStr sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"]  constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]])];
    
    CGFloat flagOriginX = LCFloat(3);
    CGFloat flagOriginY = LCFloat(0);
    CGFloat flagMargin = LCFloat(10);
    
    
    CGFloat flagWidth = 0;
    if (hasEdit){          // 编辑
        flagWidth = kScreenBounds.size.width - 2 * LCFloat(11);
    } else {
        flagWidth = kScreenBounds.size.width - LCFloat(11) - huatiSize.width - 2 * LCFloat(11);
    }

    
    for (int i = 0 ; i < transferArr.count;i++){
        NSString *info = [transferArr objectAtIndex:i];
        CGSize floagSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]])];
        CGFloat floagWidth = floagSize.width + 2 * LCFloat(11);
        
        if (hasEdit){
            floagWidth = floagSize.width + 2 * LCFloat(7);
        } else {
            floagWidth = floagSize.width;
        }
        
        
        if (floagWidth < flagWidth){                      // 不换行
            flagWidth -= (floagWidth + flagMargin);
            flagOriginX += (floagWidth + flagMargin);
        } else {
            flagOriginY += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]] + 2 * LCFloat(4) + flagMargin;
            flagOriginX =  LCFloat(3);
            flagWidth = kScreenBounds.size.width - 2 * flagMargin;
            flagWidth -= (floagWidth + flagMargin);
            flagOriginX += (floagWidth + flagMargin);
            cellHeight = flagOriginY;
        }
        
    }
    
    
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]] + 2 * LCFloat(4) ;
    
    return cellHeight;
}


-(void)actionClickWithTagsBlock:(void(^)(NSString *info))block{
    objc_setAssociatedObject(self, &actionClickWithTagsBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
