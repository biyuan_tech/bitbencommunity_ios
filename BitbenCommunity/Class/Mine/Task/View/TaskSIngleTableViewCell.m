//
//  TaskSIngleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/1.
//  Copyright © 2018 币本. All rights reserved.
//

#import "TaskSIngleTableViewCell.h"

static char actionClickWithBlockModelKey;

@interface TaskSIngleTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *shengwangLabel;
@property (nonatomic,strong)UILabel *bpLabel;
@property (nonatomic,strong)UIButton *actionButton;
@property (nonatomic,strong)UILabel *countLabel;

@end

@implementation TaskSIngleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"13" textColor:@"2F2F2F"];
    [self addSubview:self.titleLabel];
    
    self.shengwangLabel = [GWViewTool createLabelFont:@"11" textColor:@"919191"];
    [self addSubview:self.shengwangLabel];
    
    self.bpLabel = [GWViewTool createLabelFont:@"12" textColor:@"919191"];
    [self addSubview:self.bpLabel];
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"12"];
    [self addSubview:self.actionButton];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(TaskSingleInfoModel *transferModel) = objc_getAssociatedObject(strongSelf, &actionClickWithBlockModelKey);
        if (block){
            block(strongSelf.transferModel);
        }
    }];
    
    self.countLabel = [GWViewTool createLabelFont:@"9" textColor:@"989898"];
    self.countLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.countLabel];
}

-(void)setTransferModel:(TaskSingleInfoModel *)transferModel{
    _transferModel = transferModel;
    
    self.actionButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(60) - LCFloat(15), ([TaskSIngleTableViewCell calculationCellHeight] - LCFloat(27)) / 2., LCFloat(60), LCFloat(27));
    //    -2 所有任务已被抢完
    
    // 当前任务的状态 -2 所有任务已被抢完 -1 任务就绪待领取   0 已领取 进行中 1 任务完成
    if (transferModel.current_task_status == 0){        // 进行中
        [self.actionButton setTitle:@"进行中" forState:UIControlStateNormal];
        [self.actionButton setTitleColor:[UIColor hexChangeFloat:@"A8A8A8"] forState:UIControlStateNormal];
        [self.actionButton setBackgroundImage:[UIImage imageNamed:@"icon_task_nor"] forState:UIControlStateNormal];
    } else if (self.transferModel.current_task_status == 1){
        [self.actionButton setTitle:@"领取" forState:UIControlStateNormal];
        [self.actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.actionButton setBackgroundImage:[UIImage imageNamed:@"icon_task_lingqu"] forState:UIControlStateNormal];
    } else if(self.transferModel.current_task_status == -1){
        [self.actionButton setTitle:@"做任务" forState:UIControlStateNormal];
        [self.actionButton setTitleColor:[UIColor hexChangeFloat:@"EDB845"] forState:UIControlStateNormal];
        [self.actionButton setBackgroundImage:[UIImage imageNamed:@"icon_task_new"] forState:UIControlStateNormal];
    } else if (self.transferModel.current_task_status == -2){
        [self.actionButton setTitle:@"被抢完了" forState:UIControlStateNormal];
        [self.actionButton setTitleColor:[UIColor hexChangeFloat:@"A8A8A8"] forState:UIControlStateNormal];
        [self.actionButton setBackgroundImage:[UIImage imageNamed:@"icon_task_nor"] forState:UIControlStateNormal];
    }
    
    
    CGFloat width = self.actionButton.orgin_x - LCFloat(16) * 2;
    self.titleLabel.text = transferModel.task_name;
    self.titleLabel.frame = CGRectMake(LCFloat(16), LCFloat(12), width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    self.shengwangLabel.text = @"";
    CGSize shengwangSize = [Tool makeSizeWithLabel:self.shengwangLabel];
    self.shengwangLabel.frame = CGRectMake(LCFloat(16), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(8), shengwangSize.width, shengwangSize.height);
    
    self.bpLabel.text = [NSString stringWithFormat:@"BP + %li",(long)transferModel.task_fund];
    CGSize bpSize = [Tool makeSizeWithLabel:self.bpLabel];
    self.bpLabel.frame = CGRectMake(CGRectGetMaxX(self.shengwangLabel.frame) + LCFloat(10), self.shengwangLabel.orgin_y, bpSize.width, bpSize.height);
    
    self.countLabel.text = [NSString stringWithFormat:@"%li / %li",(long)transferModel.task_current_time,(long)transferModel.task_max_time];
    self.countLabel.frame = CGRectMake(self.actionButton.orgin_x, CGRectGetMaxY(self.actionButton.frame), self.actionButton.size_width, [TaskSIngleTableViewCell calculationCellHeight] - CGRectGetMaxY(self.actionButton.frame));
    
    if (transferModel.task_current_time == transferModel.task_max_time && transferModel.task_max_time == 1){
        self.countLabel.hidden = YES;
    } else {
        self.countLabel.hidden = NO;
    }
}

+(CGFloat)calculationCellHeight{
    return 58;
}

-(void)actionClickWithBlockModel:(void(^)(TaskSingleInfoModel *transferModel))block{
    objc_setAssociatedObject(self, &actionClickWithBlockModelKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
