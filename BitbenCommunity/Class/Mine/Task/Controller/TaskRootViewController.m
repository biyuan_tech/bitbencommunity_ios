//
//  TaskRootViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/1.
//  Copyright © 2018 币本. All rights reserved.
//

#import "TaskRootViewController.h"
#import "TaskSingleTitleTableViewCell.h"
#import "CenterInvitationNumberTableViewCell.h"
#import "TaskSIngleTableViewCell.h"
#import "NetworkAdapter+Task.h"
#import "JCAlertView.h"
#import "TaskSingleView.h"
#import "ArticleDetailRootViewController.h"
#import "BYLiveConfig.h"
#import "BYILiveRoomController.h"
#import "BYVideoRoomController.h"
#import "BYPPTRoomController.h"
#import "BYVODPlayController.h"
#import "BYVideoLiveController.h"
#import "BYLiveDetailController.h"

@interface TaskRootViewController ()<UITableViewDelegate,UITableViewDataSource>{
    TaskHeaderInfoModel *singleModel;
}
@property (nonatomic,strong)UITableView *taskRootTableView;
@property (nonatomic,strong)NSMutableArray *taskRootMutableArr;
@property (nonatomic,strong)NSMutableArray *taskListMutableArr;
@property (nonatomic,strong)NSMutableArray *bannerArr;
@property (nonatomic,strong)JCAlertView *alert;;
@property (nonatomic,strong)TaskSingleView *signView;

@end

@implementation TaskRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self interfaceManager];
    [self sendRequestToGetInfo];
}

#pragma mark -pageSetting
-(void)pageSetting{
    self.barMainTitle = @"任务中心";
    self.view.backgroundColor = [UIColor hexChangeFloat:@"F5F5F5"];
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_task_nav_right"] barHltImage:[UIImage imageNamed:@"icon_task_nav_right"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf->singleModel.article_id.length){
            ArticleDetailRootViewController *articleVC = [[ArticleDetailRootViewController alloc]init];
            articleVC.transferPageType = ArticleDetailRootViewControllerTypeBanner;
            articleVC.transferArticleId = strongSelf->singleModel.article_id;
            [strongSelf.navigationController pushViewController:articleVC animated:YES];
        } else {
            [StatusBarManager statusBarHidenWithText:@"请稍后再试"];
        }
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.taskRootMutableArr = [NSMutableArray array];
    self.taskListMutableArr = [NSMutableArray array];
    self.bannerArr = [NSMutableArray array];
    [self.taskRootMutableArr addObject:@[@"banner",@"bp"]];
}

#pragma mark - createTableView
-(void)createTableView{
    if (!self.taskRootTableView){
        self.taskRootTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.taskRootTableView.dataSource = self;
        self.taskRootTableView.delegate = self;
        [self.view addSubview:self.taskRootTableView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.taskRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"banner" sourceArr:self.taskRootMutableArr]){
        return 2;
    } else if (section == [self cellIndexPathSectionWithcellData:@"规则" sourceArr:self.taskRootMutableArr]){
        return 1;
    } else {
        TaskSingleInfoListModel *listModel = [self.taskRootMutableArr objectAtIndex:section];
        return listModel.taskList.count + 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"banner" sourceArr:self.taskRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"banner" sourceArr:self.taskRootMutableArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWBannerTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWBannerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;

        cellWithRowOne.transferImgArr = self.bannerArr;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne bannerActionClickBlock:^(GWScrollViewSingleModel *singleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf directManagerWithModel:singleModel.bannerModel];
        }];
        
        
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"bp" sourceArr:self.taskRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"bp" sourceArr:self.taskRootMutableArr]){
        static NSString *cellIdentifyWithRowBp = @"cellIdentifyWithRowBp";
        CenterInvitationNumberTableViewCell *cellWithRowBp = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowBp];
        if (!cellWithRowBp){
            cellWithRowBp = [[CenterInvitationNumberTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowBp];
        }
        cellWithRowBp.transferTaskModel = singleModel;
        return cellWithRowBp;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"规则" sourceArr:self.taskRootMutableArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferIcon = [UIImage imageNamed:@"icon_task_tishi"];
        cellWithRowTwo.transferTitle = @"相关奖励的详细规则请查看声望值和BP的帮助说明。";
        cellWithRowTwo.titleLabel.textColor = [UIColor colorWithCustomerName:@"CAB17A"];
        cellWithRowTwo.titleLabel.font = [UIFont fontWithCustomerSizeName:@"11"];
        return cellWithRowTwo;
    } else {
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            TaskSingleTitleTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[TaskSingleTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            }
            cellWithRowThr.transferCellHeight = cellHeight;
            TaskSingleInfoListModel *listModel = [self.taskRootMutableArr objectAtIndex:indexPath.section];
            cellWithRowThr.transferModel = listModel.model;

            return cellWithRowThr;
        } else {
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            TaskSIngleTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowThr){
                cellWithRowThr = [[TaskSIngleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            }
            cellWithRowThr.transferCellHeight = cellHeight;
            TaskSingleInfoListModel *listModel = [self.taskRootMutableArr objectAtIndex:indexPath.section];
            TaskSingleInfoModel *singleModel = [listModel.taskList objectAtIndex:indexPath.row - 1];
            cellWithRowThr.transferModel = singleModel;
            
            __weak typeof(self)weakSelf = self;
            [cellWithRowThr actionClickWithBlockModel:^(TaskSingleInfoModel * _Nonnull transferModel) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (transferModel.current_task_status == 0){            // 进行中
                    
                } else if (transferModel.current_task_status == 1){     // 已完成
                    [strongSelf receiveFundManagerWithTask:transferModel cell:indexPath];
                } else if (transferModel.current_task_status == 2){     // 已被抢完
                    
                } else if (transferModel.current_task_status == -1){    // 领取
                    [strongSelf receiveTaskManager:transferModel cell:indexPath];
                }
            }];
            return cellWithRowThr;
        }
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"banner" sourceArr:self.taskRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"banner" sourceArr:self.taskRootMutableArr]){
        return [GWBannerTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"bp" sourceArr:self.taskRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"bp" sourceArr:self.taskRootMutableArr]){
        return [CenterInvitationNumberTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"规则" sourceArr:self.taskRootMutableArr]){
        return [GWNormalTableViewCell calculationCellHeight];
    } else {
        if (indexPath.row == 0){
            TaskSingleInfoListModel *listModel = [self.taskRootMutableArr objectAtIndex:indexPath.section];
            return [TaskSingleTitleTableViewCell calculationCellHeightModel:listModel.model];
        } else {
            return [TaskSIngleTableViewCell calculationCellHeight];
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else {
        return LCFloat(9);
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    SeparatorType separatorType = SeparatorTypeMiddle;
    
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"banner" sourceArr:self.taskRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"banner" sourceArr:self.taskRootMutableArr]){
        separatorType  = SeparatorTypeHead;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"bp" sourceArr:self.taskRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"bp" sourceArr:self.taskRootMutableArr]){
        separatorType  = SeparatorTypeBottom;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"规则" sourceArr:self.taskRootMutableArr]){
        separatorType  = SeparatorTypeSingle;
    } else {
        if (indexPath.row == 0){
           separatorType  = SeparatorTypeHead;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
    }
    [cell addSeparatorLineWithType:separatorType];
}

#pragma mark - Interface
-(void)interfaceManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] taskListManagerblock:^(TaskSingleInfoListRootModel * _Nonnull listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.taskListMutableArr.count){
            [strongSelf.taskListMutableArr removeAllObjects];
        }
        
        // 判断是否有任务数量
        NSMutableArray *taskTempListArr = [NSMutableArray array];
        for (int i = 0 ; i < listModel.total.count;i++){
            TaskSingleInfoListModel *singleModel = [listModel.total objectAtIndex:i];
            if (singleModel.taskList.count){
                [taskTempListArr addObject:singleModel];
            }
        }
        [strongSelf.taskListMutableArr addObjectsFromArray:taskTempListArr];
        
        [strongSelf.taskRootMutableArr removeAllObjects];
        [strongSelf.taskRootMutableArr addObject:@[@"banner",@"bp"]];
        [strongSelf.taskRootMutableArr addObjectsFromArray:strongSelf.taskListMutableArr];
        [strongSelf.taskRootMutableArr addObject:@[@"规则"]];
        
        [strongSelf.taskRootTableView reloadData];
    }];
}

-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] taskGetMainInfo:^(TaskHeaderInfoModel * _Nonnull model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->singleModel = model;
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
        [strongSelf.bannerArr removeAllObjects];
        for (int i= 0 ; i < model.banner.count;i++){
            TaskHeaderInfoModelBanner *bannerModel = [model.banner objectAtIndex:i];
            [strongSelf.bannerArr addObject:bannerModel];
        }

        [strongSelf.taskRootTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark 领取任务
-(void)receiveTaskManager:(TaskSingleInfoModel *)taskModel cell:(NSIndexPath *)cellIndexPath{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] receiveTaskManagerWithTaskId:taskModel.user_task_id successBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        taskModel.current_task_status = 0;
        [strongSelf.taskRootTableView reloadRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark - 任务中心领取bp
-(void)receiveFundManagerWithTask:(TaskSingleInfoModel *)taskModel cell:(NSIndexPath *)cellIndexPath{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] receiveFundManagerWithTaskId:taskModel.user_task_id successBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        taskModel.current_task_status = -1;
        if (taskModel.task_current_time < taskModel.task_max_time){
            taskModel.task_current_time += 1;
        }
        [strongSelf.taskRootTableView reloadRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        [strongSelf showWithBpStr:[NSString stringWithFormat:@"BP + %li",(long)taskModel.task_fund]];
    }];
}

-(void)showWithBpStr:(NSString *)bpStr{
    if (!self.signView){
        self.signView = [[TaskSingleView alloc]initWithFrame:CGRectMake(0, 0, LCFloat(181), LCFloat(132))];
        self.signView.backgroundColor = [UIColor whiteColor];
    }
    if (!self.alert){
        self.alert = [[JCAlertView alloc]initWithCustomView:self.signView dismissWhenTouchedBackground:YES];

    }
    self.signView.transferBpStr = bpStr;
    self.signView.layer.cornerRadius = LCFloat(3);
    self.signView.clipsToBounds = YES;
    
    [self.alert show];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.alert dismissWithCompletion:NULL];
    });
}





-(void)directManagerWithModel:(TaskHeaderInfoModelBanner *)bannerModel{
    if (bannerModel.theme_type == BY_THEME_TYPE_ARTICLE) { // 跳文章
        ArticleDetailRootViewController *articleController = [[ArticleDetailRootViewController alloc] init];
        articleController.transferArticleId = bannerModel.theme_id;
        articleController.transferPageType = ArticleDetailRootViewControllerTypeBanner;
        [self.navigationController pushViewController:articleController animated:YES];
    }
    else if (bannerModel.theme_type == BY_THEME_TYPE_LIVE) { // 跳转直播
        switch (bannerModel.live_type) {
            case BY_NEWLIVE_TYPE_ILIVE:
            {
                BYLiveConfig *config = [[BYLiveConfig alloc] init];
                config.isHost = NO;
                BYILiveRoomController *ilivewRoomController = [[BYILiveRoomController alloc] initWithConfig:config];
                ilivewRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
                [self.navigationController pushViewController:ilivewRoomController animated:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_UPLOAD_VIDEO:
            {
                BYVideoRoomController *videoRoomController = [[BYVideoRoomController alloc] init];
                videoRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
                [self.navigationController pushViewController:videoRoomController animated:YES];

            }
                break;
            case BY_NEWLIVE_TYPE_AUDIO_PPT:
            case BY_NEWLIVE_TYPE_AUDIO_LECTURE:
            {
//                BYPPTRoomController *pptRoomController = [[BYPPTRoomController alloc] init];
//                pptRoomController.live_record_id = nullToEmpty(bannerModel.theme_id);
//                pptRoomController.isHost = NO;
                BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
                liveDetailController.live_record_id = nullToEmpty(bannerModel.theme_id);
                liveDetailController.isHost = NO;
                liveDetailController.navigationTitle = @"微直播";
                [self.navigationController pushViewController:liveDetailController animated:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_VOD_AUDIO:
            case BY_NEWLIVE_TYPE_VOD_VIDEO:
            {
                BYVODPlayController *vodPlayController = [[BYVODPlayController alloc] init];
                vodPlayController.live_record_id = nullToEmpty(bannerModel.theme_id);
                [self.navigationController pushViewController:vodPlayController animated:YES];
            }
                break;
            case BY_NEWLIVE_TYPE_VIDEO:
            {
//                BYVideoLiveController *videoLiveController = [[BYVideoLiveController alloc] init];
//                videoLiveController.live_record_id = nullToEmpty(bannerModel.theme_id);
//                videoLiveController.isHost = NO;
//                [self.navigationController pushViewController:videoLiveController animated:YES];
                BYLiveDetailController *liveDetailController = [[BYLiveDetailController alloc] init];
                liveDetailController.live_record_id = nullToEmpty(bannerModel.theme_id);
                liveDetailController.isHost = NO;
                liveDetailController.navigationTitle = @"推流直播";
                [self.navigationController pushViewController:liveDetailController animated:YES];

            }
                break;
            default:
                break;
        }
    }
    else if (bannerModel.theme_type == BY_THEME_TYPE_APP_H5) { // 跳转app内部h5
        PDWebViewController *webViewController = [[PDWebViewController alloc] init];
        [webViewController webDirectedWebUrl:bannerModel.theme_id];
        [self.navigationController pushViewController:webViewController animated:YES];
    }
}
@end


