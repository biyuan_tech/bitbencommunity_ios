//
//  TaskSingleInfoModel.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/1.
//  Copyright © 2018 币本. All rights reserved.
//

#import "TaskSingleInfoModel.h"

@implementation TaskSingleInfoModel



@end


@implementation TaskSingleInfoListModel

-(TaskSingleTitleTableViewCellModel *)model{
    if (!_model){
        _model = [[TaskSingleTitleTableViewCellModel alloc]init];
    }

    _model.title = _taskTypeName;
    if (![_taskTypeName isEqualToString:@"新手任务"]){
        _model.subTitle = @"每天做任务，额外获得奖励哦；每日释放总量有限，先做先得哟";
    }
    return _model;
}

@end

@implementation TaskSingleInfoListRootModel



@end
