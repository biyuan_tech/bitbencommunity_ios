//
//  NetworkAdapter+Task.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/5.
//  Copyright © 2018 币本. All rights reserved.
//

#import "NetworkAdapter+Task.h"


@implementation NetworkAdapter (Task)

-(void)taskListManagerblock:(void(^)(TaskSingleInfoListRootModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:personal_task_list requestParams:nil responseObjectClass:[TaskSingleInfoListRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TaskSingleInfoListRootModel *taskRootModel = (TaskSingleInfoListRootModel *)responseObject;
            if (block){
                block(taskRootModel);
            }
        }
    }];
}

-(void)taskGetMainInfo:(void (^)(TaskHeaderInfoModel *model))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_personal_fund requestParams:nil responseObjectClass:[TaskHeaderInfoModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TaskHeaderInfoModel *model = (TaskHeaderInfoModel *)responseObject;
            if (block){
                block(model);
            }
        }
    }];
}

#pragma mark - 领取任务
-(void)receiveTaskManagerWithTaskId:(NSString *)taskId successBlock:(void(^)())block{
    __weak typeof(self)weaKSelf = self;
    NSDictionary *params = @{@"user_task_id":taskId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:receive_task requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weaKSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
}

#pragma mark - 任务中心领取bp
-(void)receiveFundManagerWithTaskId:(NSString *)taskId successBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"user_task_id":taskId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:receive_fund requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}

#pragma mark - 分享直播
-(void)taskShareManager:(NSInteger)themeType block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"theme_type":@(themeType)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:share_theme requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}

@end
