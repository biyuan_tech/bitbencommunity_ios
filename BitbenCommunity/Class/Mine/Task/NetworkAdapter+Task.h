//
//  NetworkAdapter+Task.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/12/5.
//  Copyright © 2018 币本. All rights reserved.
//

#import "NetworkAdapter.h"
#import "TaskSingleInfoModel.h"
#import "TaskHeaderInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetworkAdapter (Task)
-(void)taskListManagerblock:(void(^)(TaskSingleInfoListRootModel *listModel))block;
-(void)taskGetMainInfo:(void (^)(TaskHeaderInfoModel *model))block;
#pragma mark - 领取任务
-(void)receiveTaskManagerWithTaskId:(NSString *)taskId successBlock:(void(^)())block;
#pragma mark - 任务中心领取bp
-(void)receiveFundManagerWithTaskId:(NSString *)taskId successBlock:(void(^)())block;
#pragma mark - 分享直播
-(void)taskShareManager:(NSInteger)themeType block:(void(^)())block;
@end

NS_ASSUME_NONNULL_END
