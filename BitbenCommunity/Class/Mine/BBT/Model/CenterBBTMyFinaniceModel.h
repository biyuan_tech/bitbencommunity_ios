//
//  CenterBBTMyFinaniceModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/24.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterBBTMyFinaniceModel : FetchModel

@property (nonatomic,copy)NSString *_id;                        /**< 编号*/
@property (nonatomic,copy)NSString *account_id;                 /**< 邀请人accountID*/
@property (nonatomic,assign)CGFloat coin;                       /**< bbt余额*/
@property (nonatomic,copy)NSString *diamond;
@property (nonatomic,assign)CGFloat fund;                        /**< BP余额*/
@property (nonatomic,assign)CGFloat lock_coin;                  /**< 锁定bbt*/
@property (nonatomic,assign)CGFloat lock_fund;                  /**< 锁定bp余额*/
@property (nonatomic,copy)NSString *power;
@property (nonatomic,assign)CGFloat unlock_coin;                /**< 已解锁BBt*/
@property (nonatomic,assign)CGFloat unlock_fund;                /**< 解锁bp余额*/
@property (nonatomic,copy)NSString *weighted_fund;
@property (nonatomic,assign)CGFloat unlocking_fund;             /**<bp解锁中*/

@end

NS_ASSUME_NONNULL_END
