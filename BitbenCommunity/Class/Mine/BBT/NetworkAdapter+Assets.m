//
//  NetworkAdapter+Assets.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/24.
//  Copyright © 2019 币本. All rights reserved.
//

#import "NetworkAdapter+Assets.h"

@implementation NetworkAdapter (Assets)

-(void)assetsGetFinanceHistoryManagerWithType:(BBTMyFinaniceType)type beginTime:(NSTimeInterval)beginTime endTime:(NSTimeInterval)endTime content:(NSString *)content Block:(void(^)(CenterBBTMyFinaniceHistoryModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    BBTMyFinaniceType tempType = 0;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    tempType = type;
    if (tempType == BBTMyFinaniceTypeBBTJiesuo){
        tempType = BBTMyFinaniceTypeBBTYue;
        content = @"解锁";
    } else if (tempType == BBTMyFinaniceTypeBpJiesuo){
        tempType = BBTMyFinaniceTypeBpYue;
        content = @"会员解锁";
    }
    
    [params setValue:@(tempType) forKey:@"finance_type"];
    [params setValue:content forKey:@"content"];
    if (beginTime != 0){
        [params setValue:@(beginTime * 1000) forKey:@"create_time_start"];
    }
    if (endTime != 0){
        [params setValue:@(endTime * 1000) forKey:@"create_time_end"];
    }
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_my_finance_history requestParams:params responseObjectClass:[CenterBBTMyFinaniceHistoryModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterBBTMyFinaniceHistoryModel *listModel = (CenterBBTMyFinaniceHistoryModel *)responseObject;
            if (block){
                block(listModel);
            }
        }
        
    }];
}

-(void)assetsGetMyFinanceInfoManagerWithBlcok:(void(^)(CenterBBTMyFinaniceModel *bbtMyFinaniceModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_my_finance requestParams:nil responseObjectClass:[CenterBBTMyFinaniceModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterBBTMyFinaniceModel *bbtMyFinaniceModel = (CenterBBTMyFinaniceModel *)responseObject;
            if (block){
                block(bbtMyFinaniceModel);
            }
        }
    }];
}

-(void)assetsTransferManagerWithConvertType:(NSString *)type coin:(NSString *)coin block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"convert_type":type,@"coin":coin};
    [[NetworkAdapter sharedAdapter] fetchWithPath:convert_finance requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}



// 获取提现记录
-(void)getTixianHistoryManagerBeginTime:(NSTimeInterval)beginTime endTime:(NSTimeInterval)endTime Block:(void(^)(CenterBBTMyFinaniceHistoryModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    if (beginTime != 0){
        [params setValue:@(beginTime * 1000) forKey:@"create_time_start"];
    }
    if (endTime != 0){
        [params setValue:@(endTime * 1000) forKey:@"create_time_end"];
    }
    [params setValue:[AccountModel sharedAccountModel].account_id forKey:@"account_id"];
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_my_cash_history requestParams:params responseObjectClass:[CenterBBTMyFinaniceHistoryModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            CenterBBTMyFinaniceHistoryModel *listModel = (CenterBBTMyFinaniceHistoryModel *)responseObject;
            if (block){
                block(listModel);
            }
        }
    }];
}

@end
