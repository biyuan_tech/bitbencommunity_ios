//
//  NetworkAdapter+Assets.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/24.
//  Copyright © 2019 币本. All rights reserved.
//

#import "NetworkAdapter.h"
#import "CenterBBTMyFinaniceModel.h"
#import "CenterBBTMyFinaniceHistoryModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,BBTMyFinaniceType) {
    BBTMyFinaniceTypeBBTYue = 101,
    BBTMyFinaniceTypeBBTSuoding = 102,
    BBTMyFinaniceTypeBBTJiesuo = 103,
    
    BBTMyFinaniceTypeBpYue = 301,
    BBTMyFinaniceTypeBpSuoding = 302,
    BBTMyFinaniceTypeBpJiesuo = 303,
};

typedef NS_ENUM(NSInteger,BBTMyFinaniceTransferType) {
    BBTMyFinaniceTransferTypeNormal = 0,
    BBTMyFinaniceTransferTypeToBBT = 1,
    BBTMyFinaniceTransferTypeToBP = 2,
};

@interface NetworkAdapter (Assets)

// 获取我的资产信息
-(void)assetsGetMyFinanceInfoManagerWithBlcok:(void(^)(CenterBBTMyFinaniceModel *bbtMyFinaniceModel))block;
// 获取历史数据
-(void)assetsGetFinanceHistoryManagerWithType:(BBTMyFinaniceType)type beginTime:(NSTimeInterval)beginTime endTime:(NSTimeInterval)endTime content:(NSString *)content Block:(void(^)(CenterBBTMyFinaniceHistoryModel *listModel))block;
// 进行转账
-(void)assetsTransferManagerWithConvertType:(NSString *)type coin:(NSString *)coin block:(void(^)())block;
// 获取提现记录
-(void)getTixianHistoryManagerBeginTime:(NSTimeInterval)beginTime endTime:(NSTimeInterval)endTime Block:(void(^)(CenterBBTMyFinaniceHistoryModel *listModel))block;
@end

NS_ASSUME_NONNULL_END
