//
//  CenterBBTTransferViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/25.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "CenterBBTMyFinaniceModel.h"

typedef NS_ENUM(NSInteger,CenterBBTTransferViewControllerType) {
    CenterBBTTransferViewControllerTypeToBBT,
    CenterBBTTransferViewControllerTypeToBP,
};

NS_ASSUME_NONNULL_BEGIN

@interface CenterBBTTransferViewController : AbstractViewController
@property (nonatomic,strong)CenterBBTMyFinaniceModel *transferFinaniceModel;
@property (nonatomic,assign)CenterBBTTransferViewControllerType transferPageType;

-(void)actionTransferManagerWithBlock:(void(^)())block;

@end

NS_ASSUME_NONNULL_END
