//
//  CenterBBTShowViewViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/23.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterBBTShowViewViewController.h"
#import "UIImage+ImageEffects.h"
#import "NetworkAdapter+Task.h"
#import "PGDatePicker.h"

static char actionClickWithChooseItemsBlockKey;
static char actionClickWithChooseTimeBlockKey;
#define SheetViewHeight       LCFloat(235 )
@interface CenterBBTShowViewViewController ()<PGDatePickerDelegate>{
    NSTimeInterval beginTime;
    NSTimeInterval endTime;
}

@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)PDImageView *actionBgimgView;
@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong)UIView *infoView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图片
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PGDatePicker *datePicker;
@property (nonatomic,strong)UIButton *chooseButton;
@end

@implementation CenterBBTShowViewViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
}

#pragma mark - 创建sheetView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    self.actionBgimgView = [[PDImageView alloc]init];
    self.actionBgimgView.backgroundColor = [UIColor clearColor];
    self.actionBgimgView.image = self.showImgBackgroundImage;
    self.actionBgimgView.frame = self.view.bounds;
    self.actionBgimgView.alpha = 0;
    [self.view addSubview:self.actionBgimgView];
    
    // 创建背景色
    self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35f];
    [self.view addSubview:self.backgrondView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    
    [self createSharetView];
}

#pragma mark - 创建view
-(void)createSharetView{
    CGFloat viewHeight = SheetViewHeight;

    viewHeight += LCFloat(51);
    
    CGRect screenRect = [[UIScreen mainScreen]bounds];
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, screenRect.size.height, screenRect.size.width, viewHeight)];
    _shareView.clipsToBounds = YES;
    _shareView.backgroundColor = UURandomColor;
    _shareView.image = self.backgroundImage;
    _shareView.userInteractionEnabled = YES;
    _shareView.contentMode = UIViewContentModeBottom;
    _shareView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_shareView];
    
    // 创建标题
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"353535"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(54));
    self.titleLabel.text = @"选择交易类型";
    [_shareView addSubview:self.titleLabel];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame), kScreenBounds.size.width, .5f);
    [_shareView addSubview:lineView];
    lineView.backgroundColor = [UIColor hexChangeFloat:@"EDEDED"];
    
    
    _shareView.size_height = LCFloat(282) - LCFloat(51);
    
    // 创建图标底部内容
    self.infoView = [[UIView alloc]init];
    self.infoView.backgroundColor = [UIColor whiteColor];
    self.infoView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame), kScreenBounds.size.width, 0);
    [self.shareView addSubview:self.infoView];
    
    self.chooseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.chooseButton.backgroundColor = [UIColor clearColor];
    [self.chooseButton setTitle:@"确定" forState:UIControlStateNormal];
    [self.chooseButton setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
    self.chooseButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
    self.chooseButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(100), 0, LCFloat(100), self.titleLabel.size_height);
    __weak typeof(self)weakSelf = self;
    [self.chooseButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.tranferType == CenterBBTShowViewViewControllerTypeItems){
            void(^block)(NSString *item) = objc_getAssociatedObject(strongSelf, &actionClickWithChooseItemsBlockKey);
            if (block){
                block(strongSelf.transferSelectedInfo);
            }
        } else if (strongSelf.tranferType == CenterBBTShowViewViewControllerTypeTimes){
            
            void(^block)(NSTimeInterval beginTime ,NSTimeInterval endTime) = objc_getAssociatedObject(strongSelf, &actionClickWithChooseTimeBlockKey);
            if (block){
                block(strongSelf->beginTime,strongSelf->endTime);
            }
        }
    }];
    [self.shareView addSubview:self.chooseButton];
}


-(void)btnActionClickManager:(NSString *)info{
    self.transferSelectedInfo = info;
    
    [self createShareButton];
}

#pragma mark 创建dismissButton
-(void)createDismissButton{
    UIView *lineView = [[UIView alloc]init];
    [_shareView addSubview:lineView];
    lineView.backgroundColor = [UIColor hexChangeFloat:@"EDEDED"];
    
    
    UIButton *dismissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissBtn.frame = CGRectMake(0, _shareView.frame.size.height - LCFloat(51), kScreenBounds.size.width, LCFloat(51));
    [dismissBtn setTitle:@"取消" forState:UIControlStateNormal];
    dismissBtn.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
    dismissBtn.backgroundColor = [UIColor whiteColor];
    [dismissBtn setTitleColor:[UIColor colorWithCustomerName:@"353535"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [dismissBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sheetViewDismiss];
    }];
    [_shareView addSubview:dismissBtn];
    
    // 举报按钮
    lineView.frame = CGRectMake(0, dismissBtn.orgin_y - .5f, kScreenBounds.size.width, .5f);
    
   // 手势
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.backgrondView addGestureRecognizer:tapGestureRecognizer];
}

#pragma mark - actionClick
-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}


#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak CenterBBTShowViewViewController *weakVC = self;
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.actionBgimgView.alpha = 1;
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.shareView.frame = CGRectMake(0, screenHeight, weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
        self.actionBgimgView.alpha = 0;
        self.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    [self createSheetView];                      // 加载Sheetview
    
    // 创建图标
    if (self.tranferType == CenterBBTShowViewViewControllerTypeItems){
        [self createShareButton];
    } else if (self.tranferType == CenterBBTShowViewViewControllerTypeTimes){
        [self createDatePicker];
    }
    [self createDismissButton];                  // 创建dismissButton
    
    __weak CenterBBTShowViewViewController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    
    [UIView animateWithDuration:.5f animations:^{
        weakVC.shareView.frame = CGRectMake(weakVC.shareView.frame.origin.x, screenHeight-_shareView.bounds.size.height-(IS_IOS7_LATER ? 0 : 20), weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
        
        self.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
        self.actionBgimgView.alpha = 1;
    } completion:^(BOOL finished) {
        self.actionBgimgView.alpha = 1;
    }];
}


- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}


-(void)actionClickWithChooseItemsBlock:(void(^)(NSString *chooseItems))block{
    objc_setAssociatedObject(self, &actionClickWithChooseItemsBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


#pragma mark - DatePicker
-(void)createDatePicker{
    self.datePicker = [[PGDatePicker alloc]init];
    self.datePicker.delegate = self;
    self.datePicker.datePickerMode = PGDatePickerModeYearAndMonth;
    self.datePicker.backgroundColor = [UIColor clearColor];
    self.datePicker.textColorOfSelectedRow = [UIColor hexChangeFloat:@"3E3E3E"];
    self.datePicker.textColorOfOtherRow = [UIColor hexChangeFloat:@"6F6F6F"];
    self.datePicker.autoSelected = YES;
    NSDate *date = [NSDate date];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];//设置为中文
    self.datePicker.locale = locale;
    [self.datePicker setMaximumDate:date];
    self.datePicker.frame = CGRectMake(LCFloat(20),0, kScreenBounds.size.width - 2 * LCFloat(22), LCFloat(300));
    [self.infoView addSubview:self.datePicker];
    self.infoView.size_height = LCFloat(200);
    _shareView.size_height = LCFloat(200) + self.titleLabel.size_height + LCFloat(51);
}

#pragma PGDatePickerDelegate
- (void)datePicker:(PGDatePicker *)datePicker didSelectDate:(NSDateComponents *)dateComponents {
    NSString *beginTimeStr = [NSString stringWithFormat:@"%li-%li-%li 00:00:00",(long)dateComponents.year,(long)dateComponents.month,(long)1];
    beginTime = [self UTCDateFromTimeStamap:beginTimeStr];
    
    NSInteger endYear = 0;
    NSInteger endMonth = 0;
    if (dateComponents.month == 12){
        endMonth = 1;
        endYear = dateComponents.year+1;
    } else {
        endMonth = dateComponents.month + 1;
        endYear = dateComponents.year;
    }
    
    NSString *endTimeStr = [NSString stringWithFormat:@"%li-%li-%li 00:00:00",(long)endYear,(long)endMonth,(long)1];
    endTime = [self UTCDateFromTimeStamap:endTimeStr];
}

//时间戳转UTCDate
-(NSTimeInterval )UTCDateFromTimeStamap:(NSString *)timeStamap{
    // 2.将时间转换为date
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *date2 = [formatter dateFromString:timeStamap];
    NSTimeInterval a=[date2 timeIntervalSince1970];
    return a;
}

#pragma mark 创建分享图标
-(void)createShareButton{
    if (self.infoView.subviews.count){
        [self.infoView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    CGFloat width = LCFloat(100);
    CGFloat margin = (kScreenBounds.size.width - 3 * width) / 4.;
    CGFloat height = LCFloat(45);
    CGFloat margin_top = LCFloat(23);
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i < self.transferItemsArr.count;i++){
        UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        actionButton.backgroundColor = [UIColor clearColor];
        [actionButton setTitle:[self.transferItemsArr objectAtIndex:i] forState:UIControlStateNormal];
        actionButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
        actionButton.layer.cornerRadius = LCFloat(3);
        actionButton.layer.borderWidth = 1;
        if ([[self.transferItemsArr objectAtIndex:i] isEqualToString:self.transferSelectedInfo]){
            [actionButton setTitleColor:[UIColor hexChangeFloat:@"E35B36"] forState:UIControlStateNormal];
            actionButton.layer.borderColor = [UIColor hexChangeFloat:@"E35B36"].CGColor;
        } else {
            [actionButton setTitleColor:[UIColor hexChangeFloat:@"3E3E3E"] forState:UIControlStateNormal];
            actionButton.layer.borderColor = [UIColor hexChangeFloat:@"DBDBDB"].CGColor;
        }
        [actionButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSString *info = [strongSelf.transferItemsArr objectAtIndex:i];
            [strongSelf btnActionClickManager:info];
        }];
        
        CGFloat origin_x = margin + (width + margin) * (i % 3);
        CGFloat origin_y = margin_top + (height + LCFloat(20)) * (i / 3);
        actionButton.frame = CGRectMake(origin_x, origin_y, width, height);
        [self.infoView addSubview:actionButton];
    }
    self.infoView.backgroundColor = [UIColor clearColor];
    NSInteger row = (self.transferItemsArr.count % 3 == 0 ? self.transferItemsArr.count / 3:self.transferItemsArr.count / 3 + 1);
    CGFloat mainHeight = row * (height + LCFloat(20)) + 2 * LCFloat(23);
    self.shareView.size_height = mainHeight + CGRectGetMaxY(self.titleLabel.frame) + LCFloat(51);
    self.infoView.size_height = mainHeight;
    self.infoView.userInteractionEnabled = YES;
    self.shareView.userInteractionEnabled = YES;
}

-(void)actionClickWithChooseTimeBlock:(void(^)(NSTimeInterval beginTime,NSTimeInterval endTime))block{
    objc_setAssociatedObject(self, &actionClickWithChooseTimeBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
