//
//  CenterBBTViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/31.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterBBTViewController.h"
#import "CenterBBTSingleCell.h"
#import "CenterBBTHeaderView.h"
#import "NetworkAdapter+Center.h"
#import "CenterBBTHistorySingleModel.h"
#import "CenterBBTDetailedViewController.h"
#import "CenterBBTQuestionViewController.h"
#import "NetworkAdapter+Assets.h"
#import "CenterBBTTransferViewController.h"
#import "CenterBBTTixianRootViewController.h"
#import "CenterJiesuoRootHistoryViewController.h"
#import "CenterJiesuoTransferRootViewController.h"
@interface CenterBBTViewController ()<UITableViewDataSource,UITableViewDelegate>{
    CenterBBTMyFinaniceModel * _Nonnull bbtMyFinaniceModel;
}
@property (nonatomic,strong)CenterBBTHeaderView *headerView;
@property (nonatomic,strong)NSMutableArray *bbtRootMutableArr;
@property (nonatomic,strong)UITableView *bbtTableView;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)UITableView *bpTableView;
@property (nonatomic,strong)NSMutableArray *bpMutableArr;


@property (nonatomic,strong)UIButton *chongzhiButton;
@property (nonatomic,strong)UIButton *transferBBTButton;

@end

@implementation CenterBBTViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createMainScrollView];
    [self sendRequestToGetFinanceManager];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.view.backgroundColor = RGB(243, 245, 249, 1);
    self.transferControllerType = CenterBBTViewControllerTypeBBT;
    self.headerView = [[CenterBBTHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [CenterBBTHeaderView calculationHeaderViewWithType:CenterBBTViewControllerTypeBBT])];
    self.headerView.clipsToBounds = YES;
    __weak typeof(self)weakSelf = self;
    [self.headerView actionBackButtonClick:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.headerView segmentListActionClickBlock:^(NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width * index, 0) animated:YES];
        strongSelf.headerView.transferControllerType = strongSelf.transferControllerType;
        [strongSelf mainViewScrollToPage:index];
    }];

    // 点击问题按钮
    [self.headerView actionClickWithQuestionManagerBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDWebViewController *webViewController = [[PDWebViewController alloc]init];
        [webViewController webDirectedWebUrl:page3];
        [strongSelf.navigationController pushViewController:webViewController animated:YES];
    }];
    // 点击跳转详情
    [self.headerView actionClickWithDetailManagerBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        CenterBBTDetailedViewController *bbtDetailVC = [[CenterBBTDetailedViewController alloc]init];
        if (strongSelf.transferControllerType == CenterBBTViewControllerTypeBP){
            bbtDetailVC.transferFinaniceType = BBTMyFinaniceTypeBpYue;
        } else if (strongSelf.transferControllerType == CenterBBTViewControllerTypeBBT){
            bbtDetailVC.transferFinaniceType = BBTMyFinaniceTypeBBTYue;
        }
        [strongSelf.navigationController pushViewController:bbtDetailVC animated:YES];
    }];
    
    [self.headerView actionSwapGestureManagerWithBlock:^(BOOL hasLeft) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger page = 0;
        if (hasLeft){
            page = 1;
        } else {
            page = 0;
        }
        [strongSelf.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width * page, 0) animated:YES];
        [strongSelf mainViewScrollToPage:page];
    }];
    
    [self.view addSubview:self.headerView];
    
    
    // 创建底部的按钮
    self.chongzhiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.chongzhiButton.backgroundColor = [UIColor whiteColor];
    [self.chongzhiButton setTitle:@"提现" forState:UIControlStateNormal];
    [self.chongzhiButton setTitleColor:[UIColor colorWithCustomerName:@"橙"] forState:UIControlStateNormal];
    self.chongzhiButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"18"];
    CGFloat btn_height = 0;
    btn_height = LCFloat(50);
    if (IS_iPhoneX){
        btn_height += 34;
    }
    [self.chongzhiButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if([strongSelf.chongzhiButton.titleLabel.text isEqualToString:@"提现"]){
            CenterBBTTixianRootViewController *tixianViewController = [[CenterBBTTixianRootViewController alloc]init];
            [strongSelf.navigationController pushViewController:tixianViewController animated:YES];
        } else {
//            [[UIAlertView alertViewWithTitle:@"提示" message:@"暂未开放，敬请期待" buttonTitles:@[@"确定"] callBlock:NULL]show];
//            return;
            CenterJiesuoTransferRootViewController *transferRootViewController = [[CenterJiesuoTransferRootViewController alloc]init];
            [transferRootViewController actionClickWithJiesuoSuccessedManager:^{
                [strongSelf.bpTableView reloadData];
            }];
            transferRootViewController.transferMuFinaniceModel = strongSelf->bbtMyFinaniceModel;
            [strongSelf.navigationController pushViewController:transferRootViewController animated:YES];
        }
    }];
    self.chongzhiButton.frame = CGRectMake(0, self.view.size_height - btn_height, kScreenBounds.size.width, btn_height);
    [self.view addSubview:self.chongzhiButton];
    
    self.transferBBTButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.transferBBTButton.backgroundColor = [UIColor colorWithCustomerName:@"橙"];
    self.transferBBTButton.frame = CGRectMake(CGRectGetMaxX(self.chongzhiButton.frame), self.chongzhiButton.orgin_y, self.chongzhiButton.size_width, self.chongzhiButton.size_height);
    [self.transferBBTButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.transferBBTButton setTitle:@"转BP" forState:UIControlStateNormal];
    self.transferBBTButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"18"];
    self.transferBBTButton.hidden = YES;
    [self.transferBBTButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        CenterBBTTransferViewController *bbtTransferVC = [[CenterBBTTransferViewController alloc]init];
        [bbtTransferVC actionTransferManagerWithBlock:^{
            strongSelf.headerView.transferBBTMyFinaniceModel = strongSelf -> bbtMyFinaniceModel;
            [strongSelf.headerView actionClickManagerWithChange];
            [strongSelf.bbtTableView reloadData];
        }];
        if ([strongSelf.transferBBTButton.titleLabel.text isEqualToString:@"转BP"]){
            bbtTransferVC.transferPageType = CenterBBTTransferViewControllerTypeToBP;
        } else if ([strongSelf.transferBBTButton.titleLabel.text isEqualToString:@"转BBT"]){
            bbtTransferVC.transferPageType = CenterBBTTransferViewControllerTypeToBBT;
        }
        bbtTransferVC.transferFinaniceModel = strongSelf->bbtMyFinaniceModel;
        [strongSelf.navigationController pushViewController:bbtTransferVC animated:YES];
    }];
    [self.view addSubview:self.transferBBTButton];
    
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.bbtRootMutableArr = [NSMutableArray array];
    [self.bbtRootMutableArr addObject:@[@"锁定",@"已解锁"]];
    
    self.bpMutableArr = [NSMutableArray array];
    [self.bpMutableArr addObject:@[@"锁定",@"解锁中",@"已解锁"]];
}

#pragma mark - createMainScrollView
-(void)createMainScrollView{
    __weak typeof(self)weakSelf = self;
    if(!self.mainScrollView){
        self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSInteger page = scrollView.contentOffset.x / scrollView.size_width;
            if (page == 0){
                strongSelf.transferControllerType = CenterBBTViewControllerTypeBBT;
            } else if (page == 1){
                strongSelf.transferControllerType = CenterBBTViewControllerTypeBP;
            }
            [strongSelf mainViewScrollToPage:page];
        }];
        self.mainScrollView.frame = CGRectMake(0,CGRectGetMaxY(self.headerView.frame),kScreenBounds.size.width,self.chongzhiButton.orgin_y - CGRectGetMaxY(self.headerView.frame));
        [self.view addSubview:self.mainScrollView];
        [self.mainScrollView setContentSize:CGSizeMake(2 * kScreenBounds.size.width, self.mainScrollView.size_height)];
    }
    [self createTableView];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.bbtTableView){
        self.bbtTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.bbtTableView.dataSource = self;
        self.bbtTableView.delegate = self;
        [self.mainScrollView addSubview:self.bbtTableView];
    }
    if (!self.bpTableView){
        self.bpTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.bpTableView.dataSource = self;
        self.bpTableView.orgin_x = self.mainScrollView.size_width;
        self.bpTableView.delegate = self;
        [self.mainScrollView addSubview:self.bpTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.bbtTableView){
        return self.bbtRootMutableArr.count;
    } else if (tableView == self.bpTableView){
        return self.bpMutableArr.count;
    }
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.bbtTableView){
        NSArray *sectionOfArr = [self.bbtRootMutableArr objectAtIndex:section];
        return sectionOfArr.count;
    } else if (tableView == self.bpTableView){
        NSArray *sectionOfArr = [self.bpMutableArr objectAtIndex:section];
        return sectionOfArr.count;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
   
    static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
    GWNormalTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
    if (!cellWithRowZero){
        cellWithRowZero = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
    }
    cellWithRowZero.transferCellHeight = cellHeight;
    cellWithRowZero.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
    cellWithRowZero.transferHasArrow = YES;
    
    if (tableView == self.bbtTableView){
        cellWithRowZero.transferTitle = [[self.bbtRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];

        if (indexPath.row == 0){
            cellWithRowZero.transferDesc = [NSString stringWithFormat:@"%.2f",bbtMyFinaniceModel.lock_coin];
        } else {
            cellWithRowZero.transferDesc = [NSString stringWithFormat:@"%.2f",bbtMyFinaniceModel.unlock_coin];
        }
    } else if (tableView == self.bpTableView){
        cellWithRowZero.transferTitle = [[self.bpMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];

        if (indexPath.row == 0){
            cellWithRowZero.transferDesc = [NSString stringWithFormat:@"%.2f",bbtMyFinaniceModel.lock_fund];
        } else if (indexPath.row == 1){
            cellWithRowZero.transferDesc = [NSString stringWithFormat:@"%.2f",bbtMyFinaniceModel.unlocking_fund];
        } else if (indexPath.row == 2){
            cellWithRowZero.transferDesc = [NSString stringWithFormat:@"%.2f",bbtMyFinaniceModel.unlock_fund];
        }
    }
    return cellWithRowZero;
   
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CenterBBTDetailedViewController *bbtVC = [[CenterBBTDetailedViewController alloc]init];
    if (tableView == self.bbtTableView){
        if (indexPath.row == 0){
            bbtVC.transferFinaniceType = BBTMyFinaniceTypeBBTSuoding;
        } else {
            bbtVC.transferFinaniceType = BBTMyFinaniceTypeBBTJiesuo;
        }
    } else if (tableView == self.bpTableView){
        if (indexPath.row == 0){
            bbtVC.transferFinaniceType = BBTMyFinaniceTypeBpSuoding;
        } else if (indexPath.row == 1){
//            [[UIAlertView alertViewWithTitle:@"提示" message:@"暂未开放，敬请期待" buttonTitles:@[@"确定"] callBlock:NULL]show];
//            return;
            
            CenterJiesuoRootHistoryViewController *jiesuoRootHistoryVC = [[CenterJiesuoRootHistoryViewController alloc]init];
            [self.navigationController pushViewController:jiesuoRootHistoryVC animated:YES];
            return;
        } else {
            bbtVC.transferFinaniceType = BBTMyFinaniceTypeBpJiesuo;
        }
    }
    [self.navigationController pushViewController:bbtVC animated:YES];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *sectionOfArr;
    if (self.bbtTableView) {
        sectionOfArr = [self.bbtRootMutableArr objectAtIndex:indexPath.section];
    } else {
        sectionOfArr = [self.bpMutableArr objectAtIndex:indexPath.section];
    }
    
    SeparatorType separatorType = SeparatorTypeMiddle;
    if ( [indexPath row] == 0) {
        separatorType  = SeparatorTypeHead;
    } else if ([indexPath row] == sectionOfArr.count - 1) {
        separatorType  = SeparatorTypeBottom;
    } else {
        separatorType  = SeparatorTypeMiddle;
    }
    if (sectionOfArr.count == 1) {
        separatorType  = SeparatorTypeSingle;
    }
    [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        return LCFloat(58);
    } else {
        return [CenterBBTSingleCell calculationCellHeight];
    }
}

#pragma mark - sendRequest

#pragma mark - 获取奖励金额
-(void)sendRequestToGetFinanceManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] assetsGetMyFinanceInfoManagerWithBlcok:^(CenterBBTMyFinaniceModel * _Nonnull bbtMyFinaniceModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->bbtMyFinaniceModel = bbtMyFinaniceModel;
        strongSelf.headerView.transferBBTMyFinaniceModel = bbtMyFinaniceModel;
        strongSelf.headerView.transferControllerType = CenterBBTViewControllerTypeBBT;
        [strongSelf.bbtTableView reloadData];
    }];
}

-(void)mainViewScrollToPage:(NSInteger)page{
    __weak typeof(self)weakSelf = self;
    [self.headerView.segmentList setSelectedButtonIndex:page animated:YES];
    if (page == 0){
        self.transferControllerType = CenterBBTViewControllerTypeBBT;
        [self.chongzhiButton setTitle:@"提现" forState:UIControlStateNormal];
        [self.transferBBTButton setTitle:@"转BP" forState:UIControlStateNormal];
        [UIView animateWithDuration:.5f animations:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.chongzhiButton.size_width = kScreenBounds.size.width;
            strongSelf.transferBBTButton.frame = CGRectMake(kScreenBounds.size.width, self.transferBBTButton.orgin_y, 0, self.transferBBTButton.size_height);
        } completion:^(BOOL finished) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.bbtTableView reloadData];
            strongSelf.transferBBTButton.hidden = YES;
        }];
        
    } else if (page == 1){
        self.transferControllerType = CenterBBTViewControllerTypeBP;
        [self.transferBBTButton setTitle:@"转BBT" forState:UIControlStateNormal];
        [self.chongzhiButton setTitle:@"解锁" forState:UIControlStateNormal];
        self.transferBBTButton.hidden = NO;
        [UIView animateWithDuration:.5f animations:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.chongzhiButton.size_width = kScreenBounds.size.width / 2.;
            strongSelf.transferBBTButton.frame = CGRectMake(kScreenBounds.size.width / 2., self.chongzhiButton.orgin_y, kScreenBounds.size.width / 2., self.transferBBTButton.size_height);
        } completion:^(BOOL finished) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.transferBBTButton.hidden = NO;
            [strongSelf.bpTableView reloadData];
        }];
    }
    
    self.headerView.transferControllerType = self.transferControllerType;
}


@end
