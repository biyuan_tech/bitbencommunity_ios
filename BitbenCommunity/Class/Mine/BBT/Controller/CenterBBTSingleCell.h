//
//  CenterBBTSingleCell.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/31.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"
#import "CenterBBTHistorySingleModel.h"

@interface CenterBBTSingleCell : PDBaseTableViewCell

@property (nonatomic,strong)CenterBBTHistorySingleModel *transferBBTModel;

@end
