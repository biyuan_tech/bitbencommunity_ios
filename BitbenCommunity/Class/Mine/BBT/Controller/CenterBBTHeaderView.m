//
//  CenterBBTHeaderView.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/31.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterBBTHeaderView.h"

static char actionBackButtonClickKey;
static char segmentListActionClickBlockKey;
static char actionClickWithQuestionManagerBlockKey;
static char actionClickWithDetailManagerBlockKey;
static char actionSwapGestureManagerWithBlockKey;
@interface CenterBBTHeaderView()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *yueLabel;
@property (nonatomic,strong)UIButton *backButton;
@property (nonatomic,strong)UIButton *detailButton;
@property (nonatomic,strong)UIButton *questionButton;
@property (nonatomic,strong)UISwipeGestureRecognizer *rightGesture;
@property (nonatomic,strong)UISwipeGestureRecognizer *leftGesture;
@end

@implementation CenterBBTHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgImgView];
    self.bgImgView.frame = self.bounds;
    self.bgImgView.image = [Tool stretchImageWithName:@"bg_center_bbt_header"];
    
    
    self.titleLabel = [GWViewTool createLabelFont:@"45" textColor:@"白"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    self.yueLabel = [GWViewTool createLabelFont:@"14" textColor:@"白"];
    [self addSubview:self.yueLabel];
    self.yueLabel.text = @"可用余额";
    self.yueLabel.textAlignment = NSTextAlignmentCenter;

    self.detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.detailButton.backgroundColor = [UIColor clearColor];
    [self.detailButton setTitle:@"查看明细" forState:UIControlStateNormal];
    [self.detailButton setImage:[UIImage imageNamed:@"icon_center_question_detail_header_arrow"] forState:UIControlStateNormal];
    [self.detailButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleRight imageTitleSpace:LCFloat(3)];
    self.detailButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"14"];
    __weak typeof(self)weakSelf = self;
    [self.detailButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithDetailManagerBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.detailButton];
    
    self.questionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.questionButton.backgroundColor = [UIColor clearColor];
    [self.questionButton setImage:[UIImage imageNamed:@"icon_center_bbt_detail_question"] forState:UIControlStateNormal];
    [self.questionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithQuestionManagerBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.questionButton];

    // 返回
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.backButton.backgroundColor = [UIColor clearColor];
    [self.backButton setImage:[UIImage imageNamed:@"icon_center_bbt_back"] forState:UIControlStateNormal];
    
    [self.backButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionBackButtonClickKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.backButton];

    // 1. 创建Segment
    [self createSegmentList];
    
    self.backButton.frame = CGRectMake(0, [BYTabbarViewController sharedController].statusHeight, 44, 44);
    self.questionButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(40), 0, LCFloat(40), LCFloat(40));
    self.questionButton.center_y = self.backButton.center_y;
    self.segmentList.frame = CGRectMake(CGRectGetMaxX(self.backButton.frame), 0, self.questionButton.orgin_x - CGRectGetMaxX(self.backButton.frame), self.backButton.size_height);
    self.segmentList.center_y = self.backButton.center_y;
    
    // 2. 创建标题
    self.yueLabel.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame) + LCFloat(25), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.yueLabel.font]);
    // 3. 创建内容
    self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.yueLabel.frame), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    // 4. 创建明细
    self.detailButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(130)) / 2., CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), LCFloat(130),LCFloat(40));
    
    [self addRightGesture];
}

#pragma mark - 添加右滑手势
-(void)addRightGesture{
    self.rightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
    [self.rightGesture setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self addGestureRecognizer:self.rightGesture];
    
    self.leftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
    [self.leftGesture setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self addGestureRecognizer:self.leftGesture];
}

- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer{
    void(^block)(BOOL hasSwap) = objc_getAssociatedObject(self, &actionSwapGestureManagerWithBlockKey);
    if(recognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        if (block){
            block(YES);
        }
    }
    if(recognizer.direction == UISwipeGestureRecognizerDirectionRight) {
        if (block){
            block(NO);
        }
    }
}


-(void)setTransferControllerType:(CenterBBTViewControllerType)transferControllerType{
    _transferControllerType = transferControllerType;
    [self actionClickManagerWithChange];
}

-(void)setTransferBBTMyFinaniceModel:(CenterBBTMyFinaniceModel *)transferBBTMyFinaniceModel{
    _transferBBTMyFinaniceModel = transferBBTMyFinaniceModel;
}

-(void)actionClickManagerWithChange{
    if (self.transferControllerType == CenterBBTViewControllerTypeBBT){
        self.titleLabel.text = [NSString stringWithFormat:@"%.2f",self.transferBBTMyFinaniceModel.coin];
        
    } else if (self.transferControllerType == CenterBBTViewControllerTypeBP){
        self.titleLabel.text = [NSString stringWithFormat:@"%.2f",self.transferBBTMyFinaniceModel.fund];
    }
}



+(CGFloat)calculationHeaderViewWithType:(CenterBBTViewControllerType) transferControllerType{
    CGFloat height = 0;
    if (IS_iPhoneX){
        height += 24;
    }
    if (transferControllerType == CenterBBTViewControllerTypeBBT){
        height += LCFloat(252);
    } else if (transferControllerType == CenterBBTViewControllerTypeBP){
        height += LCFloat(252) - LCFloat(60);
    }
    return height;
}

-(void)actionBackButtonClick:(void(^)())block{
    objc_setAssociatedObject(self, &actionBackButtonClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


#pragma mark - SegmentList
-(void)createSegmentList{
    if (!self.segmentList){
        __weak typeof(self)weakSelf = self;
        self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"BBT资产",@"BP资产"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(NSInteger index) = objc_getAssociatedObject(strongSelf, &segmentListActionClickBlockKey);
            if (block){
                block(index);
            }
        }];
        [self addSubview:self.segmentList];
        self.segmentList.frame = CGRectMake(kScreenBounds.size.width / 4., 0, kScreenBounds.size.width / 2., 44);
        [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
        [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateSelected];
        self.segmentList.selectionIndicatorColor = [UIColor colorWithCustomerName:@"白"];
        self.segmentList.center_y = self.backButton.center_y;
    }
}

-(void)segmentListActionClickBlock:(void(^)(NSInteger index))items{
    objc_setAssociatedObject(self, &segmentListActionClickBlockKey, items, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithQuestionManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithQuestionManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithDetailManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithDetailManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionSwapGestureManagerWithBlock:(void(^)(BOOL hasLeft))block{
    objc_setAssociatedObject(self, &actionSwapGestureManagerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end






@interface CenterBBTHeaderViewItems()

@end

@implementation CenterBBTHeaderViewItems

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.dymicLabel = [GWViewTool createLabelFont:@"17" textColor:@"434343"];
    self.dymicLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.dymicLabel];
    
    self.fixedLabel = [GWViewTool createLabelFont:@"13" textColor:@"434343"];
    [self addSubview:self.fixedLabel];
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    self.dymicLabel.adjustsFontSizeToFitWidth = YES;
    self.fixedLabel.adjustsFontSizeToFitWidth = YES;
    
    CGFloat margin = (self.size_height - LCFloat(6) - [NSString contentofHeightWithFont:self.fixedLabel.font] - [NSString contentofHeightWithFont:self.dymicLabel.font]) / 2.;
    self.dymicLabel.frame = CGRectMake(0, margin, self.size_width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
    self.fixedLabel.frame = CGRectMake(0, self.size_height - [NSString contentofHeightWithFont:self.fixedLabel.font] - margin, self.size_width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
}



@end


