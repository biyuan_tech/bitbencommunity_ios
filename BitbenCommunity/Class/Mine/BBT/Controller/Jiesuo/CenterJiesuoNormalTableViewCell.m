//
//  CenterJiesuoNormalTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/10/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "CenterJiesuoNormalTableViewCell.h"

@interface CenterJiesuoNormalTableViewCell()
@property (nonatomic,strong)PDImageView *lineImgView;
@property (nonatomic,strong)PDImageView *statusImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UILabel *timeLabel;


@end

@implementation CenterJiesuoNormalTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.lineImgView = [[PDImageView alloc]init];
    self.lineImgView.backgroundColor = [UIColor clearColor];
    self.lineImgView.image = [UIImage imageNamed:@"icon_bp_jiesuo_detail_line"];
    [self addSubview:self.lineImgView];
    
    self.statusImgView = [[PDImageView alloc]init];
    self.statusImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.statusImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    [self addSubview:self.dymicLabel];
    
    self.timeLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"黑"];
    [self addSubview:self.timeLabel];

    self.clipsToBounds = YES;
}

-(void)setTransferIndex:(NSInteger)transferIndex{
    _transferIndex = transferIndex;
}

-(void)setTransferHistoryModel:(CenterJiesuoRootHistoryDetailModel *)transferHistoryModel{
    _transferHistoryModel = transferHistoryModel;
    
    self.statusImgView.frame = CGRectMake(LCFloat(35), LCFloat(0), LCFloat(17), LCFloat(17));
    if (transferHistoryModel.is_unlock){
        self.statusImgView.image = [UIImage imageNamed:@"icon_bp_jiesuo_detail_hlt"];
    } else {
        self.statusImgView.image = [UIImage imageNamed:@"icon_bp_jiesuo_detail_nor"];
    }
    
    self.lineImgView.frame = CGRectMake(LCFloat(35) + LCFloat(17) / 2., CGRectGetMaxY(self.statusImgView.frame), 1, [CenterJiesuoNormalTableViewCell calculationCellHeight] - LCFloat(17));
    
    self.titleLabel.text = [NSString stringWithFormat:@"第%li批",self.transferIndex];
    CGSize titleLabelSize = [Tool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake(LCFloat(75), self.statusImgView.orgin_y, titleLabelSize.width, titleLabelSize.height);
    
    self.dymicLabel.text = [NSString stringWithFormat:@"%@ %@",transferHistoryModel.is_unlock?@"已解锁":@"未解锁",[NSString stringWithFormat:@"%@BP",transferHistoryModel.num]];
    CGSize dymicSize = [Tool makeSizeWithLabel:self.dymicLabel];
    self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + LCFloat(20),self.titleLabel.orgin_y ,dymicSize.width, dymicSize.height);
    
    self.timeLabel.text = [NSDate getTimeWithJiesuoString:transferHistoryModel.datetime];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), timeSize.width, timeSize.height);
}

+(CGFloat)calculationCellHeight{
    return 70;
}

@end
