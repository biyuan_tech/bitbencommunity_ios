//
//  CenterJiesuoTransferRootViewController.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/10/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AbstractViewController.h"
#import "CenterBBTMyFinaniceModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterJiesuoTransferRootViewController : AbstractViewController

@property (nonatomic,strong)CenterBBTMyFinaniceModel *transferMuFinaniceModel;

-(void)actionClickWithJiesuoSuccessedManager:(void(^)())block;
@end

NS_ASSUME_NONNULL_END
