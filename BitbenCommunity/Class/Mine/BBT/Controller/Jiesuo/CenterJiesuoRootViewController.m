//
//  CenterJiesuoRootViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/10/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "CenterJiesuoRootViewController.h"
#import "CenterJiesuoHeaderTableViewCell.h"
#import "CenterJiesuoNormalTableViewCell.h"
#import "CenterJiesuoTransferRootViewController.h"
#import "CenterJiesuoRootModel.h"
#import "CenterJiesuoRootHistoryViewController.h"

@interface CenterJiesuoRootViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *centerJiesuoTableView;
@property (nonatomic,strong)NSMutableArray *centerJiesuoMutableArr;
@end

@implementation CenterJiesuoRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
//    [self interfaceManager];
}


-(void)pageSetting{
    self.barMainTitle = @"BP解锁明细";
    self.view.backgroundColor = RGB(241, 243, 248, 1);
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.centerJiesuoMutableArr = [NSMutableArray array];
    [self.centerJiesuoMutableArr addObject:@[@"头部"]];
    if (self.transferHistoryModel.detail.count){
        NSMutableArray *infoTempArr = [NSMutableArray array];
        [infoTempArr addObject:@"标题"];
        [infoTempArr addObjectsFromArray:self.transferHistoryModel.detail];
        
        [self.centerJiesuoMutableArr addObject:infoTempArr];
    }
}

#pragma mark - createTableView
-(void)createTableView{
    if (!self.centerJiesuoTableView){
        self.centerJiesuoTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.centerJiesuoTableView.dataSource = self;
        self.centerJiesuoTableView.delegate = self;
        [self.view addSubview:self.centerJiesuoTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.centerJiesuoMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.centerJiesuoMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        CenterJiesuoHeaderTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[CenterJiesuoHeaderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferHistoryModel = self.transferHistoryModel;
        return cellWithRowOne;
    } else {
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            }
            cellWithRowTwo.transferCellHeight = [GWNormalTableViewCell calculationCellHeight];
            cellWithRowTwo.transferTitle = @"解锁详情";
            cellWithRowTwo.titleLabel.font = [[UIFont fontWithCustomerSizeName:@"16"]boldFont];
            cellWithRowTwo.titleLabel.size_width = 200;
            return cellWithRowTwo;
        } else {
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            CenterJiesuoNormalTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[CenterJiesuoNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            }
            CenterJiesuoRootHistoryDetailModel *detailModel = [[self.centerJiesuoMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            cellWithRowThr.transferIndex = indexPath.row;
            cellWithRowThr.transferHistoryModel = detailModel;
            return cellWithRowThr;
        }
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        return [CenterJiesuoHeaderTableViewCell calculationCellHeight];
    } else {
        if (indexPath.row == 0){
            return [GWNormalTableViewCell calculationCellHeight] + LCFloat(20);
        }
        return [CenterJiesuoNormalTableViewCell calculationCellHeight];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor =[ UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1){
        return LCFloat(20);
    } else {
        return 0;
    }
}

#pragma mark - Interface
-(void)interfaceManager{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"type":@"1"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_unlock_fund_history requestParams:params responseObjectClass:[CenterJiesuoRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = self;
        if (isSucceeded){
            CenterJiesuoRootModel *history = (CenterJiesuoRootModel *)responseObject;
        }
    }];
    
//    [self.centerJiesuoMutableArr addObjectsFromArray:@[@[@"1"],@[@"2",@"3",@"4",@"5"]]];
//    [self.centerJiesuoTableView reloadData];
}

@end
