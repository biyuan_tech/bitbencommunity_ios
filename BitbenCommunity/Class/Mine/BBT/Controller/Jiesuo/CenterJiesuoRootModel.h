//
//  CenterJiesuoRootModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/10/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol CenterJiesuoRootHistoryDetailModel <NSObject>

@end

@interface CenterJiesuoRootHistoryDetailModel:FetchModel

@property (nonatomic,assign)NSTimeInterval datetime;
@property (nonatomic,copy)NSString *num;
@property (nonatomic,assign)BOOL is_unlock;
@property (nonatomic,assign)NSInteger order;
@end


@protocol CenterJiesuoRootHistoryModel <NSObject>

@end


@interface CenterJiesuoRootHistoryModel:FetchModel
@property (nonatomic,copy)NSString * _id;
@property (nonatomic,copy)NSString * amount;
@property (nonatomic,assign)NSTimeInterval end_time;
@property (nonatomic,copy)NSString * count;
@property (nonatomic,copy)NSString * type;
@property (nonatomic,assign)NSTimeInterval start_time;
@property (nonatomic,assign)NSTimeInterval  datetime;
@property (nonatomic,copy)NSString * total;
@property (nonatomic,copy)NSString * user_id;
@property (nonatomic,copy)NSString * already_num;
@property (nonatomic,copy)NSString * once;
@property (nonatomic,copy)NSString * wait_num;
@property (nonatomic,copy)NSString * interval;
@property (nonatomic,assign)BOOL status;
@property (nonatomic,strong)NSArray<CenterJiesuoRootHistoryDetailModel> *detail;
@end


@interface CenterJiesuoRootModel : FetchModel

@property (nonatomic,strong)NSArray<CenterJiesuoRootHistoryModel> *history;

@end

NS_ASSUME_NONNULL_END
