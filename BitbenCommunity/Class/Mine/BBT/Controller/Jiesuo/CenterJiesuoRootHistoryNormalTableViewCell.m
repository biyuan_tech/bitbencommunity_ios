//
//  CenterJiesuoRootHistoryNormalTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/10/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "CenterJiesuoRootHistoryNormalTableViewCell.h"

@interface CenterJiesuoRootHistoryNormalTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)PDImageView *arrowImgView;

@end

@implementation CenterJiesuoRootHistoryNormalTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"393939"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
    
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"999999"];
    [self addSubview:self.timeLabel];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"15" textColor:@"3E3E3E"];
    [self addSubview:self.dymicLabel];
    
    self.arrowImgView = [[PDImageView alloc]init];
    [self addSubview:self.arrowImgView];
}

-(void)setTransferHistoryModel:(CenterJiesuoRootHistoryModel *)transferHistoryModel{
    _transferHistoryModel = transferHistoryModel;
    
    self.titleLabel.text = transferHistoryModel.status == 0?@"解锁中":@"已解锁";
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    
    self.timeLabel.text = [NSDate getTimeWithJiesuoString:transferHistoryModel.datetime];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    
    self.dymicLabel.text = transferHistoryModel.total;
    CGSize dymicSize = [Tool makeSizeWithLabel:self.dymicLabel];
    
    self.arrowImgView.image = [UIImage imageNamed:@"icon_wallet_detail_arrow"];
    self.arrowImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(6), ([CenterJiesuoRootHistoryNormalTableViewCell calculationCellHeight] - LCFloat(12)) / 2., LCFloat(6), LCFloat(12));
    
    self.dymicLabel.frame = CGRectMake(self.arrowImgView.orgin_x - LCFloat(11) - dymicSize.width, ([CenterJiesuoRootHistoryNormalTableViewCell calculationCellHeight] - dymicSize.height) / 2., dymicSize.width, dymicSize.height);
    
    CGFloat margin = ([CenterJiesuoRootHistoryNormalTableViewCell calculationCellHeight] - titleSize.height - timeSize.height - LCFloat(7)) / 2.;
    self.titleLabel.frame = CGRectMake(LCFloat(16), margin, titleSize.width, titleSize.height);
    
    self.timeLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(7), timeSize.width, timeSize.height);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(61);
}

@end
