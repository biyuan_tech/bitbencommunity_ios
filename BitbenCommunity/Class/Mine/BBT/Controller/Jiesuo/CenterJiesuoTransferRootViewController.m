//
//  CenterJiesuoTransferRootViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/10/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "CenterJiesuoTransferRootViewController.h"
#import "WalletTransferInputTableViewCell.h"
#import "CenterJiesuoTransferInfoTableViewCell.h"
#import "CenterJiesuoRootHistoryViewController.h"

static char actionClickWithJiesuoSuccessedManagerKey;
@interface CenterJiesuoTransferRootViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    WalletTransferInputTableViewCell *infoCell;
}
@property (nonatomic,strong)UITableView *centerJiesuoTransferTableView;
@property (nonatomic,strong)NSArray *centerJiesuoTransferArr;
@property (nonatomic,strong)UILabel *sloganLabel;
@end

@implementation CenterJiesuoTransferRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createSloganLabel];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"BP解锁";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"记录" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        CenterJiesuoRootHistoryViewController *historyViewController = [[CenterJiesuoRootHistoryViewController alloc]init];
        [strongSelf.navigationController pushViewController:historyViewController animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.centerJiesuoTransferArr = @[@[@"1"],@[@"2"],@[@"按钮"]];
}

#pragma mark - createTableVIew
-(void)createTableView{
    if(!self.centerJiesuoTransferTableView){
        self.centerJiesuoTransferTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.centerJiesuoTransferTableView.dataSource = self;
        self.centerJiesuoTransferTableView.delegate = self;
        [self.view addSubview:self.centerJiesuoTransferTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.centerJiesuoTransferArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.centerJiesuoTransferArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        WalletTransferInputTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[WalletTransferInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        infoCell = cellWithRowOne;
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferType = WalletTransferInputTableViewCellTypeJiesuo;
        cellWithRowOne.transferYue = self.transferMuFinaniceModel.lock_fund;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne transferButtonActionClickBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            cellWithRowOne.inputTextField.text = [NSString stringWithFormat:@"%.2f",strongSelf.transferMuFinaniceModel.lock_fund];
        }];
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"按钮" sourceArr:self.centerJiesuoTransferArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"立即解锁";
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToUnlockManager];
        }];
        return cellWithRowTwo;
    } else {
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        CenterJiesuoTransferInfoTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[CenterJiesuoTransferInfoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        
        return cellWithRowThr;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return LCFloat(100);
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"按钮" sourceArr:self.centerJiesuoTransferArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else {
        return [CenterJiesuoTransferInfoTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1){
        return LCFloat(20);
    } else if (section == 2){
        return LCFloat(100);
    } else {
        return LCFloat(11);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - createWalletTitle
-(void)createSloganLabel{
    self.sloganLabel = [GWViewTool createLabelFont:@"16" textColor:@"000000"];
    self.sloganLabel.textAlignment = NSTextAlignmentCenter;
    self.sloganLabel.font = [self.sloganLabel.font boldFont];
    self.sloganLabel.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(53) - [BYTabbarViewController sharedController].navBarHeight, kScreenBounds.size.width, LCFloat(53));
    if (IS_iPhoneX){
        self.sloganLabel.orgin_y -= 34;
    }
    self.sloganLabel.text = @"Bitben Wallet";
    [self.view addSubview:self.sloganLabel];
}

#pragma mark - InterfaceManager
-(void)sendRequestToUnlockManager{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"num":infoCell.inputTextField.text};
    [[NetworkAdapter sharedAdapter] fetchWithPath:unlock_fund requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            // 锁定的减少
            strongSelf.transferMuFinaniceModel.lock_fund -= [strongSelf->infoCell.inputTextField.text
                                                             floatValue];
            // 解锁中的增加
            strongSelf.transferMuFinaniceModel.unlocking_fund += [strongSelf->infoCell.inputTextField.text
            floatValue];
            
            void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithJiesuoSuccessedManagerKey);
            
            [[UIAlertView alertViewWithTitle:@"解锁成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (block){
                    block();
                }
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }]show];
        }
    }];
}

#pragma mark - 解锁成功方法
-(void)actionClickWithJiesuoSuccessedManager:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithJiesuoSuccessedManagerKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
