//
//  CenterJiesuoRootHistoryViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/10/29.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "CenterJiesuoRootHistoryViewController.h"
#import "CenterJiesuoRootModel.h"
#import "CenterJiesuoRootViewController.h"
#import "CenterJiesuoRootHistoryNormalTableViewCell.h"

@interface CenterJiesuoRootHistoryViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *historyTableView;
@property (nonatomic,strong)NSMutableArray *historyMutableArr;
@end

@implementation CenterJiesuoRootHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWthInit];
    [self createTableView];
    [self sendRequestTogetInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"BP解锁中";
}

#pragma mark - arrayWithInit
-(void)arrayWthInit{
    self.historyMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.historyTableView){
        self.historyTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.historyTableView.dataSource = self;
        self.historyTableView.delegate = self;
        [self.view addSubview:self.historyTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.historyMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    CenterJiesuoRootHistoryNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[CenterJiesuoRootHistoryNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    CenterJiesuoRootHistoryModel *singleModel = [self.historyMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferHistoryModel = singleModel;
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.historyTableView) {
          SeparatorType separatorType = SeparatorTypeMiddle;
          if ( [indexPath row] == 0) {
              separatorType  = SeparatorTypeHead;
          } else if ([indexPath row] == [self.historyMutableArr count] - 1) {
              separatorType  = SeparatorTypeBottom;
          } else {
              separatorType  = SeparatorTypeMiddle;
          }
          if ([self.historyMutableArr  count] == 1) {
              separatorType  = SeparatorTypeSingle;
          }
          [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
      }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CenterJiesuoRootHistoryModel *model = [self.historyMutableArr objectAtIndex:indexPath.row];
    CenterJiesuoRootViewController *rootViewController = [[CenterJiesuoRootViewController alloc]init];
    rootViewController.transferHistoryModel = model;
    [self.navigationController pushViewController:rootViewController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CenterJiesuoRootHistoryNormalTableViewCell calculationCellHeight];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

#pragma mark - Interface
-(void)sendRequestTogetInfo{
    __weak typeof(self)weakSelf = self;
   NSDictionary *params = @{@"type":@"1"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_unlock_fund_history requestParams:params responseObjectClass:[CenterJiesuoRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            CenterJiesuoRootModel *jiesuoModel = (CenterJiesuoRootModel *)responseObject;
            [strongSelf.historyMutableArr addObjectsFromArray:jiesuoModel.history];
            [strongSelf.historyTableView reloadData];
            if (strongSelf.historyMutableArr.count){
                [strongSelf.historyTableView dismissPrompt];
            } else {
                [strongSelf.historyTableView showPrompt:@"当前没有解锁中BP信息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
        }
    }];
}

@end
