//
//  CenterJiesuoTransferInfoTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/10/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "CenterJiesuoTransferInfoTableViewCell.h"

#define jiesuotishiArr @[@"1.锁定BP数量的多少会影响用户在社区内容挖矿的权重，锁定越多，点赞的权重将越高，从而影响挖矿的收益",@"2.锁定的BP数量将决定每日BBT分红的多少，锁定的BP越多，每日分红将会越多",@"3.解锁BP分为13批进行解锁，每周一批，解锁中的BP不计入权重与分红",@"4.解锁发起后无法取消解锁，请慎重选择"]

@interface CenterJiesuoTransferInfoTableViewCell()
@property (nonatomic,strong)UILabel *tishiLabel;
@property (nonatomic,strong)UIView *bgView;

@end

@implementation CenterJiesuoTransferInfoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    
    self.tishiLabel = [GWViewTool createLabelFont:@"14" textColor:@"A3A3A3"];
    self.tishiLabel.text = @"提示";
    self.tishiLabel.frame = CGRectMake(LCFloat(16), 0, kScreenBounds.size.width - 2 * LCFloat(16), [NSString contentofHeightWithFont:self.tishiLabel.font]);
    [self addSubview:self.tishiLabel];
    
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgView];
    
    [self actionManager];
}

-(void)actionManager{
    CGFloat origin_y = 0;
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(16);
    for (int i = 0;i<jiesuotishiArr.count;i++){
        UILabel *label = [GWViewTool createLabelFont:@"12" textColor:@"A3A3A3"];
        label.text = [jiesuotishiArr objectAtIndex:i];
        [self.bgView addSubview:label];
        label.numberOfLines = 0;
        CGSize labelSize = [label.text sizeWithCalcFont:label.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
        label.frame = CGRectMake(LCFloat(16), origin_y, kScreenBounds.size.width - 2 * LCFloat(16), labelSize.height);
        origin_y += (labelSize.height + LCFloat(14));
    }
    self.bgView.frame = CGRectMake(0, CGRectGetMaxY(self.tishiLabel.frame) + LCFloat(14), kScreenBounds.size.width, origin_y);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]];
    cellHeight += LCFloat(14);
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(16);
    
    for (int i = 0 ; i < jiesuotishiArr.count;i++){
        NSString *info = [jiesuotishiArr objectAtIndex:i];
        CGSize heightSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"12"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
        cellHeight += (heightSize.height + LCFloat(14));
    }
    return cellHeight;
}

@end
