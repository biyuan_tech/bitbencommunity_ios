//
//  CenterJiesuoHeaderTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/10/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "CenterJiesuoRootModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterJiesuoHeaderTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)CenterJiesuoRootHistoryModel *transferHistoryModel;

@end

NS_ASSUME_NONNULL_END
