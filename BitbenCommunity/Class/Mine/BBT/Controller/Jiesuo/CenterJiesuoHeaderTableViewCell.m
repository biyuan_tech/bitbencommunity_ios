//
//  CenterJiesuoHeaderTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/10/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "CenterJiesuoHeaderTableViewCell.h"

@interface CenterJiesuoHeaderTableViewCell()
@property (nonatomic,strong)PDImageView *lockImgView;
@property (nonatomic,strong)UILabel *lockLabel;
@property (nonatomic,strong)UILabel *titleLabel1;
@property (nonatomic,strong)UILabel *titleLabel2;
@property (nonatomic,strong)UILabel *titleLabel3;

@end

@implementation CenterJiesuoHeaderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1.
    self.lockImgView = [[PDImageView alloc]init];
    self.lockImgView.backgroundColor = [UIColor clearColor];
    self.lockImgView.image = [UIImage imageNamed:@"icon_bp_jiesuo_lock"];
    self.lockImgView.frame = CGRectMake(LCFloat(15), LCFloat(24), LCFloat(14), LCFloat(17));
    [self addSubview:self.lockImgView];
    
    // 2.
    self.lockLabel = [GWViewTool createLabelFont:@"副标题" textColor:@"黑"];
    self.lockLabel.textColor = [UIColor hexChangeFloat:@"377AFF"];
    [self addSubview:self.lockLabel];
    
    //
    self.titleLabel1 = [GWViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.titleLabel1.font = [UIFont fontWithCustomerSizeName:@"14"];
    self.titleLabel1.textColor = [UIColor hexChangeFloat:@"828282"];
    [self addSubview:self.titleLabel1];
    
    self.titleLabel2 = [GWViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.titleLabel2.font = [UIFont fontWithCustomerSizeName:@"14"];
    self.titleLabel2.textColor = [UIColor hexChangeFloat:@"393939"];
    [self addSubview:self.titleLabel2];
    
    self.titleLabel3 = [GWViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.titleLabel3.font = [UIFont fontWithCustomerSizeName:@"14"];
    self.titleLabel3.textColor = [UIColor hexChangeFloat:@"393939"];
    [self addSubview:self.titleLabel3];
    
}


-(void)setTransferHistoryModel:(CenterJiesuoRootHistoryModel *)transferHistoryModel{
    _transferHistoryModel = transferHistoryModel;
    
    self.lockLabel.text = @"解锁中";
    CGSize lockLabelSize = [Tool makeSizeWithLabel:self.lockLabel];
    self.lockLabel.frame = CGRectMake(CGRectGetMaxX(self.lockImgView.frame) + LCFloat(10), 0, lockLabelSize.width, lockLabelSize.height);
    self.lockLabel.center_y = self.lockImgView.center_y;
    
    self.titleLabel1.text = [NSString stringWithFormat:@"解锁总额：%@BP",transferHistoryModel.total];
    self.titleLabel1.frame = CGRectMake(LCFloat(16), CGRectGetMaxY(self.lockLabel.frame) + LCFloat(21), kScreenBounds.size.width - 2 * LCFloat(16), [NSString contentofHeightWithFont:self.titleLabel1.font]);
    
    self.titleLabel2.text = [NSString stringWithFormat:@"已解锁：%@BP   待解锁：%@BP",transferHistoryModel.already_num,transferHistoryModel.wait_num];
    self.titleLabel2.frame = CGRectMake(self.titleLabel1.orgin_x, CGRectGetMaxY(self.titleLabel1.frame) + LCFloat(21), kScreenBounds.size.width - 2 * LCFloat(16), [NSString contentofHeightWithFont:self.titleLabel2.font]);
    
    self.titleLabel3.text = [NSString stringWithFormat:@"%@ - %@",[NSDate getTimeWithJiesuoString:transferHistoryModel.start_time]   ,[NSDate getTimeWithJiesuoString:transferHistoryModel.end_time]];
    self.titleLabel3.frame = CGRectMake(LCFloat(16), CGRectGetMaxY(self.titleLabel2.frame) + LCFloat(21), kScreenBounds.size.width - 2 * LCFloat(16), [NSString contentofHeightWithFont:self.titleLabel3.font]);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(25);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"16"]];
    cellHeight += LCFloat(21);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]] * 3;
    cellHeight += LCFloat(21);
    cellHeight += 2 * LCFloat(16);
    return cellHeight;
}

@end
