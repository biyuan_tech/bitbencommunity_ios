//
//  CenterBBTSingleCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/31.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterBBTSingleCell.h"

@interface CenterBBTSingleCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *moneyLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *yueLabel;
@end

@implementation CenterBBTSingleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"393939"];
    [self addSubview:self.titleLabel];
    
    // money
    self.moneyLabel = [GWViewTool createLabelFont:@"15" textColor:@"393939"];
    [self addSubview:self.moneyLabel];
    
    // time
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"727272"];
    [self addSubview:self.timeLabel];
    
    // yue
    self.yueLabel = [GWViewTool createLabelFont:@"13" textColor:@"999999"];
    [self addSubview:self.yueLabel];
}

-(void)setTransferBBTModel:(CenterBBTHistorySingleModel *)transferBBTModel{
    _transferBBTModel = transferBBTModel;
 
    self.titleLabel.text = transferBBTModel.content;
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake(LCFloat(15), LCFloat(15), titleSize.width, titleSize.height);
    
    self.moneyLabel.text = [NSString stringWithFormat:@"%@%.2f",transferBBTModel.status == 0?@"+":@"-",transferBBTModel.transaction_amount];
    CGSize moneySize = [Tool makeSizeWithLabel:self.moneyLabel];
    self.moneyLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - moneySize.width, self.titleLabel.orgin_y, moneySize.width, moneySize.height);
    
    self.timeLabel.text = [NSDate getTimeWithString:transferBBTModel.create_time / 1000.];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(LCFloat(15),CGRectGetMaxY(self.titleLabel.frame) + LCFloat(8), timeSize.width, timeSize.height);
    
    self.yueLabel.text = [NSString stringWithFormat:@"余额:%.2f",transferBBTModel.coin];
    CGSize yueSize = [Tool makeSizeWithLabel:self.yueLabel];
    self.yueLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - yueSize.width, 0, yueSize.width, yueSize.height);
    self.yueLabel.center_y = self.timeLabel.center_y;
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(15);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]];
    cellHeight += LCFloat(8);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    cellHeight += LCFloat(15);
    return cellHeight;
}

@end
