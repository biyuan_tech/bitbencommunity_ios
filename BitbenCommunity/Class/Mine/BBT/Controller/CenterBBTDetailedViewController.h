//
//  CenterBBTDetailedViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/23.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "NetworkAdapter+Assets.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterBBTDetailedViewController : AbstractViewController

@property (nonatomic,assign)BBTMyFinaniceType transferFinaniceType;

@property (nonatomic,assign)BBTMyFinaniceTransferType transferType;

@end

NS_ASSUME_NONNULL_END
