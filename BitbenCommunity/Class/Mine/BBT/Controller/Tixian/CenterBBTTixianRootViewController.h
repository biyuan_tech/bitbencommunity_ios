//
//  CenterBBTTixianRootViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/20.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterBBTTixianRootViewController : AbstractViewController

-(void)actionTixianNoWalletManager:(void(^)())block;

@end

NS_ASSUME_NONNULL_END
