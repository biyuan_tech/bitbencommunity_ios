//
//  CenterBBTTixianDetailViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/22.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterBBTTixianDetailViewController.h"
#import "WalletTransferDetailSingleTableViewCell.h"
#import "WalletTransferDetailHeaderTableViewCell.h"
#import "NetworkAdapter+Wallet.h"

@interface CenterBBTTixianDetailViewController ()<UITableViewDelegate,UITableViewDataSource>{
    WalletTransferDetailModel *detailModel;
}
@property (nonatomic,strong)UITableView *transferDetailTableView;
@property (nonatomic,strong)NSMutableArray *transferDetailMutableArr;
@end

@implementation CenterBBTTixianDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"提现";
}

-(void)arrayWithInit{
    self.transferDetailMutableArr = [NSMutableArray array];
    if (self.transferModel){
        [self.transferDetailMutableArr addObjectsFromArray:@[@[@"标题"],@[@"接收地址",@"sdfadsfaosdfasdf"],@[@"手续费",@"sdfsdjflksjd"],@[@"到账数量",@"sdfsdjflksjd"],@[@"区块",@"sdlkfjaldsjasdf"],@[@"交易时间",@"klfjsd"],@[@"交易ID",@"sdalkfjdsfdsafadsfosd"]]];
    } else {
        [self.transferDetailMutableArr addObjectsFromArray:@[@[@"标题"],@[@"接收地址",@"sdfadsfaosdfasdf"],@[@"手续费",@"sdfsdjflksjd"],@[@"区块",@"sdlkfjaldsjasdf"],@[@"交易时间",@"klfjsd"],@[@"交易ID",@"sdalkfjdsfdsafadsfosd"]]];
    }

}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.transferDetailTableView){
        self.transferDetailTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.transferDetailTableView.dataSource = self;
        self.transferDetailTableView.delegate = self;
        [self.view addSubview:self.transferDetailTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.transferDetailMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.transferDetailMutableArr]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        WalletTransferDetailHeaderTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[WalletTransferDetailHeaderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
        }
        cellWithRowZero.transferTixianModel = self.transferModel;
        cellWithRowZero.transferDetailModel = detailModel;
        return cellWithRowZero;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        WalletTransferDetailSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[WalletTransferDetailSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        NSString *title = [[self.transferDetailMutableArr objectAtIndex:indexPath.section] objectAtIndex:0];
        cellWithRowOne.transferTitle = title;
        if ([title isEqualToString:@"发送地址"]){
            cellWithRowOne.transferDymic = detailModel.from;
        } else if ([title isEqualToString:@"接收地址"]){
            cellWithRowOne.transferDymic = detailModel.to;
        } else if ([title isEqualToString:@"手续费"]){
            NSString *shouxufei = @"";
            if (self.transferModel){
                shouxufei = detailModel.cash_service_charge;
            } else {
                shouxufei = detailModel.service_charge;
            }

            cellWithRowOne.transferDymic = shouxufei;
        } else if ([title isEqualToString:@"区块"]){

            cellWithRowOne.transferDymic = detailModel.block_number;
        } else if ([title isEqualToString:@"交易时间"]){
            cellWithRowOne.transferDymic = [NSDate getTimeWithDuobaoString:detailModel.create_time  / 1000].length? [NSDate getTimeWithDuobaoString:detailModel.create_time / 1000]:@"没有交易时间";
        } else if ([title isEqualToString:@"交易ID"]){
            cellWithRowOne.transferDymic = detailModel.hashs.length?detailModel.hashs:@"无";
        } else if ([title isEqualToString:@"到账数量"]){
            if (self.transferModel){
                cellWithRowOne.transferDymic = detailModel.value;
            }  else {
                cellWithRowOne.transferDymic = @"0";
            }
        }
        
        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [WalletTransferDetailHeaderTableViewCell calculationCellHeight];
    } else {
        NSString *title = [[self.transferDetailMutableArr objectAtIndex:indexPath.section] objectAtIndex:0];
        NSString *info = @"";
        if ([title isEqualToString:@"发送地址"]){
            info = detailModel.from;
        } else if ([title isEqualToString:@"接收地址"]){
            info = detailModel.to;
        } else if ([title isEqualToString:@"手续费"]){
            info = detailModel.service_charge;
        } else if ([title isEqualToString:@"区块"]){
            info = detailModel.block_number;
        } else if ([title isEqualToString:@"交易时间"]){
            info =  [NSDate getTimeWithDuobaoString:detailModel.create_time].length? [NSDate getTimeWithDuobaoString:detailModel.create_time]:@"没有交易时间";
        } else if ([title isEqualToString:@"交易ID"]){
            info = detailModel.hashs;
        }
        
        return [WalletTransferDetailSingleTableViewCell calculationCellHeightWithDymic:info];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.transferDetailTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.transferDetailMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.transferDetailMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}


#pragma mark - 获取接口信息
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] walletGetRechargeDetail:@"" transferId:self.transferModel.transaction_id block:^(WalletTransferDetailModel * _Nonnull detailModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->detailModel = detailModel;
        if (strongSelf->detailModel.create_time == 0){
            strongSelf->detailModel.create_time = strongSelf.transferModel.create_time;
        }
        
        [strongSelf.transferDetailTableView reloadData];
    }];
}


@end
