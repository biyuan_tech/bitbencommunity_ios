//
//  CenterBBTTixianRootViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/5/20.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterBBTTixianRootViewController.h"
#import "WalletTransferInputTableViewCell.h"
#import "WalletTongxunluViewController.h"
#import "CenterBBTTixianJiluViewController.h"
#import "WalletSecurityInputView.h"
#import "JCAlertView.h"
#import "CenterBBTTixianModel.h"

static char actionTixianNoWalletManagerKey;
@interface CenterBBTTixianRootViewController()<UITableViewDelegate,UITableViewDataSource>{
    UITextField *toUstdTextField;
    UITextField *valurUstdTextField;
    CenterBBTTixianModel *tixianRootModel;
    GWButtonTableViewCell *buttonCell;
    CGFloat daozhang;
    GWNormalTableViewCell *daozhangCell;
}
@property (nonatomic,strong)UITableView *tixianTableView;
@property (nonatomic,strong)NSArray *tixianArr;
@property (nonatomic,strong)UILabel *sloganLabel;
@property (nonatomic,strong)WalletSecurityInputView *inputView;
@property (nonatomic,strong)JCAlertView *alertView;

@end

@implementation CenterBBTTixianRootViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createSloganView];
    [self getCashInfoManager];
    [self createNotifi];
}

#pragma mark - PageSetting
-(void)pageSetting{
    self.barMainTitle = @"BBT提现";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"记录" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        CenterBBTTixianJiluViewController *tixianjiluVC = [[CenterBBTTixianJiluViewController alloc]init];
        [strongSelf.navigationController pushViewController:tixianjiluVC animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.tixianArr = @[@[@"提现金额"],@[@"提现地址"],@[@"手续费",@"到账数量"],@[@"立即提现"]];
    daozhang = 0;
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.tixianTableView){
        self.tixianTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.tixianTableView.dataSource = self;
        self.tixianTableView.delegate = self;
        [self.view addSubview:self.tixianTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.tixianArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.tixianArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提现金额" sourceArr:self.tixianArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"提现地址" sourceArr:self.tixianArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        WalletTransferInputTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[WalletTransferInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提现金额" sourceArr:self.tixianArr]){
            cellWithRowOne.transferType = WalletTransferInputTableViewCellTypeTixianMoney;
            [cellWithRowOne setTransferYue:tixianRootModel.balance];
            cellWithRowOne.inputTextField.keyboardType = UIKeyboardTypeDecimalPad;
            valurUstdTextField = cellWithRowOne.inputTextField;
            valurUstdTextField.placeholder = [NSString stringWithFormat:@"最小提现数量%@",tixianRootModel.cash_limit];
            __weak typeof(self)weakSelf = self;
            [cellWithRowOne transferButtonActionClickBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                cellWithRowOne.inputTextField.text = [NSString stringWithFormat:@"%.2f",strongSelf->tixianRootModel.balance];
                strongSelf-> daozhang = strongSelf->tixianRootModel.balance;
                NSIndexPath *tixianIndexPath = [strongSelf.tixianTableView indexPathForCell:strongSelf->daozhangCell];
                [strongSelf.tixianTableView reloadRowsAtIndexPaths:@[tixianIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                [strongSelf adjustInputFieldManager];
            }];
            
            [cellWithRowOne textFieldDidChangeBlock:^(NSString * _Nonnull info) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                strongSelf->daozhang = [info floatValue];
                NSIndexPath *tixianIndexPath = [strongSelf.tixianTableView indexPathForCell:strongSelf->daozhangCell];
                [strongSelf.tixianTableView reloadRowsAtIndexPaths:@[tixianIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                return;
            }];
            
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提现地址" sourceArr:self.tixianArr]){
            cellWithRowOne.transferType = WalletTransferInputTableViewCellTypeTixianAddress;
            cellWithRowOne.inputTextField.userInteractionEnabled = YES;
            __weak typeof(self)weakSelf = self;
            toUstdTextField = cellWithRowOne.inputTextField;
            
            [cellWithRowOne tongxunBookActionClickBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf chooseMailListManager];
            }];
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne textFieldTextEndBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            
            if(indexPath.section == [self cellIndexPathSectionWithcellData:@"提现金额" sourceArr:self.tixianArr]){
                if (![Tool validateFloatNumber:cellWithRowOne.inputTextField.text]){
                    [[UIAlertView alertViewWithTitle:@"提示" message:@"当前金额有误，请核对" buttonTitles:@[@"确定"] callBlock:NULL]show];
                    return;
                }
            }
            
            [strongSelf adjustInputFieldManager];
        }];
        
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"手续费" sourceArr:self.tixianArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if(!cellWithRowTwo){
            cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        if (indexPath.row == [self  cellIndexPathRowWithcellData:@"手续费" sourceArr:self.tixianArr]){
            cellWithRowTwo.transferTitle = @"手续费";
            cellWithRowTwo.transferDesc = tixianRootModel.cash_service_charge;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"到账数量" sourceArr:self.tixianArr]){
            cellWithRowTwo.transferTitle = @"到账数量";
            daozhangCell = cellWithRowTwo;
            CGFloat price = daozhang - [tixianRootModel.cash_service_charge floatValue];
            cellWithRowTwo.transferDesc = (daozhang == 0? @"--":[NSString stringWithFormat:@"%.2f",price]);
        }
        
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"立即提现" sourceArr:self.tixianArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if(!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"立即提现";
        buttonCell = cellWithRowThr;
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToCuowuPwdCountManager];
        }];
        
        [self adjustInputFieldManager];
        return cellWithRowThr;
    } else {
        static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
        WalletTransferInputTableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
        if (!cellWithRowOther){
            cellWithRowOther = [[WalletTransferInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
        }
        return cellWithRowOther;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提现金额" sourceArr:self.tixianArr]){
        return [WalletTransferInputTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提现地址" sourceArr:self.tixianArr]){
        return [WalletTransferInputTableViewCell calculationCellHeightWithType:WalletTransferInputTableViewCellTypeTixianAddress];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"手续费" sourceArr:self.tixianArr]){
        return [GWNormalTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"立即提现" sourceArr:self.tixianArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else {
        return [WalletTransferInputTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == [self cellIndexPathSectionWithcellData:@"立即提现" sourceArr:self.tixianArr]){
        return kScreenBounds.size.height - LCFloat(100) - [GWButtonTableViewCell calculationCellHeight] -  2 * [WalletTransferInputTableViewCell calculationCellHeight] - [GWNormalTableViewCell calculationCellHeight] - [BYTabbarViewController sharedController].navBarHeight;
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - createSloganView
-(void)createSloganView{
    self.sloganLabel = [GWViewTool createLabelFont:@"16" textColor:@"000000"];
    self.sloganLabel.textAlignment = NSTextAlignmentCenter;
    self.sloganLabel.font = [self.sloganLabel.font boldFont];
    self.sloganLabel.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(53) - [BYTabbarViewController sharedController].navBarHeight, kScreenBounds.size.width, LCFloat(53));
    self.sloganLabel.text = @"Bitben Wallet";
    [self.view addSubview:self.sloganLabel];
}

-(void)chooseMailListManager{
    WalletTongxunluViewController *tongxunVC = [[WalletTongxunluViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [tongxunVC actionClickChooseManagerWithBlock:^(WalletMailListSingleModel * _Nonnull singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->toUstdTextField.text = singleModel.friend_address;
        [strongSelf adjustInputFieldManager];
    }];
    [self.navigationController pushViewController:tongxunVC animated:YES];
}


-(void)adjustInputFieldManager{
    BOOL addressEnable = NO;
    BOOL moneyEnable = NO;
    if ([toUstdTextField.text hasPrefix:@"0x"] && toUstdTextField.text.length == 42){
        addressEnable = YES;
    } else {
        addressEnable = NO;
    }
    
    if ([Tool validateFloatNumber:valurUstdTextField.text]){
        moneyEnable = YES;
    } else {
        moneyEnable = NO;
    }
    
    
    if (valurUstdTextField.text.length && toUstdTextField.text.length && moneyEnable && addressEnable){
        [buttonCell setButtonStatus:YES];
    } else {
        [buttonCell setButtonStatus:NO];
    }
}





#pragma mark - 获取接口信息
#pragma mark 获取提现信息
-(void)getCashInfoManager{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"account_id":[AccountModel sharedAccountModel].account_id};
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_cash_info requestParams:params responseObjectClass:[CenterBBTTixianModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            CenterBBTTixianModel *tixianModel = (CenterBBTTixianModel *)responseObject;
            strongSelf->tixianRootModel = tixianModel;
            
            
            if (tixianModel.wallet_status && tixianModel.user_cer_status){
                [strongSelf.tixianTableView reloadData];
            } else {
                if (!tixianModel.wallet_status){
                    [[UIAlertView alertViewWithTitle:@"提示" message:@"请先开通钱包功能，才可以进行提现" buttonTitles:@[@"取消",@"立即开通"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                        if (buttonIndex == 1){
                            [strongSelf.navigationController popToRootViewControllerAnimated:YES];
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                [[CenterRootViewController sharedController] directToWallet];
                            });
                        }
                    }]show];
                }
            }
        }
    }];
}

#pragma mark 立即提现
-(void)sendRequestToCuowuPwdCountManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] walletPwdCountNumberWithHash:tixianRootModel.block_address block:^(NSInteger count) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (count <= 0){
            [[UIAlertView alertViewWithTitle:@"提示" message:@"密码输入次数超过限" buttonTitles:@[@"确定"] callBlock:NULL]show];
            return;
        } else {
            [strongSelf showAlertInfoManagerWithCount:count hasError:NO];
        }
    }];
}

-(void)showAlertInfoManagerWithCount:(NSInteger)count hasError:(BOOL)hasError{
    NSString *error = @"";
    if (!valurUstdTextField.text.length){
        error = @"请输入转账金额";
    } else if (!toUstdTextField.text.length){
        error = @"请输入转账地址";
    }
    if (hasError){
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }
    
    if (!self.inputView){
        self.inputView = [[WalletSecurityInputView alloc]initWithFrame:CGRectMake(0, 0, 0, 0 )];
        self.inputView.layer.cornerRadius = LCFloat(4);
        self.inputView.backgroundColor = [UIColor whiteColor];
    }
    __weak typeof(self)weakSelf = self;
    [self.inputView actionClickWithGoingPwd:^(NSString * _Nonnull pwd) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf tixianManagerWithPwd:pwd];
    }];
    self.inputView.inputTextField.text = @"";
    self.alertView = [JCAlertView initWithCustomView:self.inputView dismissWhenTouchedBackground:YES];
}

#pragma mark - 提现接口
-(void)tixianManagerWithPwd:(NSString *)pwd {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (valurUstdTextField.text.length){
        [params setValue:valurUstdTextField.text forKey:@"value"];
    }
    if (tixianRootModel.block_address.length){
        [params setValue:tixianRootModel.block_address forKey:@"cash_block_address"];
    }
    if (toUstdTextField.text.length){
        [params setValue:toUstdTextField.text forKey:@"to"];
    }
    [params setValue:pwd forKey:@"block_password"];
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:send_cash requestParams:params responseObjectClass:[CenterBBTTixianModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(isSucceeded){
            CenterBBTTixianModel *tixianEndModel = (CenterBBTTixianModel *)responseObject;
            if (tixianEndModel.password_status){                // 成功
                strongSelf->valurUstdTextField.text = @"";
                strongSelf ->daozhang = 0;
                NSIndexPath *tixianIndexPath = [strongSelf.tixianTableView indexPathForCell:strongSelf->daozhangCell];
                [strongSelf.tixianTableView reloadRowsAtIndexPaths:@[tixianIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                
                [strongSelf.alertView dismissWithCompletion:^{
                    [[UIAlertView alertViewWithTitle:@"提现成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                        CenterBBTTixianJiluViewController *tixianjiluVC = [[CenterBBTTixianJiluViewController alloc]init];
                        [strongSelf.navigationController pushViewController:tixianjiluVC animated:YES];
                    }]show];
                }];
            } else {                                            // 失败
                strongSelf.inputView.transferErrCount = tixianEndModel.count;
                strongSelf.inputView.inputTextField.text = @"";
            }
        } else {
            [strongSelf.alertView dismissWithCompletion:NULL];
        }
    }];
    
}

#pragma mark - 键盘通知
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.alertView.orgin_y =  (keyboardRect.size.height) / 3.;
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    CGRect mainRect = CGRectMake(0, 0, kScreenBounds.size.width,self.view.size_height );
    self.tixianTableView.frame = mainRect;
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


-(void)actionTixianNoWalletManager:(void(^)())block{
    objc_setAssociatedObject(self, &actionTixianNoWalletManagerKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
