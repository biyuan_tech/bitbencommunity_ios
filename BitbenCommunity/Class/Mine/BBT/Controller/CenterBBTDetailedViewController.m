//
//  CenterBBTDetailedViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/23.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterBBTDetailedViewController.h"
#import "CenterBBTDetailTableViewHeaderView.h"
#import "CenterBBTShowViewViewController.h"
#import "CenterBBTDetailInfoTableViewCell.h"

@interface CenterBBTDetailedViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSString *chooseType;
    NSTimeInterval beginTimeInterval;
    NSTimeInterval endTimeInterval;
}
@property (nonatomic,strong)UITableView *detailTableView;
@property (nonatomic,strong)NSMutableArray *detailMutableArr;
@property (nonatomic,strong)CenterBBTDetailTableViewHeaderView *headerView;
@property (nonatomic,strong)NSMutableArray *itemsMutableArr;

@end

@implementation CenterBBTDetailedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self InterfaceManagerWithReload:YES];
}


#pragma mark - pageSetting
-(void)pageSetting{
    if (self.transferFinaniceType == BBTMyFinaniceTypeBBTSuoding){                  // BBT锁定
        self.barMainTitle = @"BBT锁定明细";
    } else if (self.transferFinaniceType == BBTMyFinaniceTypeBBTYue){               // BBT余额
        self.barMainTitle = @"BBT余额明细";
    } else if (self.transferFinaniceType == BBTMyFinaniceTypeBpYue){                // BP 余额
        self.barMainTitle = @"BP余额明细";
    } else if (self.transferFinaniceType == BBTMyFinaniceTypeBpSuoding){            // BP锁定
        self.barMainTitle = @"BP锁定明细";
    } else if (self.transferFinaniceType == BBTMyFinaniceTypeBBTJiesuo){
        self.barMainTitle = @"已解锁明细";
        chooseType = @"解锁";
    } else if (self.transferFinaniceType == BBTMyFinaniceTypeBpJiesuo){
        self.barMainTitle = @"已解锁明细";
        chooseType = @"解锁";
    }
    
    if (self.transferType == BBTMyFinaniceTransferTypeToBBT){
        self.barMainTitle = @"转换记录";
        chooseType = @"BP转BBT";
        
        self.transferFinaniceType = BBTMyFinaniceTypeBpYue;
        
    } else if (self.transferType == BBTMyFinaniceTransferTypeToBP){
        self.barMainTitle = @"转换记录";
        chooseType = @"BBT转BP";
        self.transferFinaniceType = BBTMyFinaniceTypeBBTYue;
    }
    
    self.headerView = [[CenterBBTDetailTableViewHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [CenterBBTDetailTableViewHeaderView calculationViewHeight])];
   
    if (self.transferType == BBTMyFinaniceTransferTypeToBBT || self.transferType == BBTMyFinaniceTransferTypeToBP){
        self.headerView.transferTypeButton.hidden = YES;
    } else {
        if (self.transferFinaniceType == BBTMyFinaniceTypeBBTJiesuo || self.transferFinaniceType == BBTMyFinaniceTypeBpJiesuo){
            self.headerView.transferTypeButton.hidden = YES;
        } else {
            self.headerView.transferTypeButton.hidden = NO;
        }
    }
    
    
    __weak typeof(self)weakSelf = self;
    [self.headerView actionClickWithLeftChooseTime:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf itemsShowManagerWithType:CenterBBTShowViewViewControllerTypeTimes];
    }];
    
    [self.headerView actionClickWithRightChooseType:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf itemsShowManagerWithType:CenterBBTShowViewViewControllerTypeItems];
    }];
    
    [self.view addSubview:self.headerView];
}

-(void)itemsShowManagerWithType:(CenterBBTShowViewViewControllerType)type{
    __weak typeof(self)weakSelf = self;
    CenterBBTShowViewViewController *bbtShowViewCOntroller = [[CenterBBTShowViewViewController alloc]init];
    
    bbtShowViewCOntroller.transferSelectedInfo = chooseType;
    [bbtShowViewCOntroller actionClickWithChooseItemsBlock:^(NSString * _Nonnull chooseItems) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->chooseType = chooseItems;
        [strongSelf.headerView changeInfoType:strongSelf->chooseType];
        [bbtShowViewCOntroller sheetViewDismiss];
        
        // 进行请求数据
        [strongSelf InterfaceManagerWithReload:YES];
    }];
    
    [bbtShowViewCOntroller actionClickWithChooseTimeBlock:^(NSTimeInterval beginTime, NSTimeInterval endTime) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->beginTimeInterval = beginTime;
        strongSelf->endTimeInterval = endTime;
        [bbtShowViewCOntroller sheetViewDismiss];
        // 进行请求数据
        [strongSelf InterfaceManagerWithReload:YES];
    }];
    
    bbtShowViewCOntroller.tranferType = type;
    bbtShowViewCOntroller.transferItemsArr = [self.itemsMutableArr copy];
    
    [bbtShowViewCOntroller showInView:self.parentViewController];

}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    chooseType = @"全部";
    if (self.transferFinaniceType == BBTMyFinaniceTypeBBTJiesuo){
        chooseType = @"解锁";
    } else if (self.transferFinaniceType == BBTMyFinaniceTypeBpJiesuo){
        chooseType = @"解锁";
    }
    
    
    self.detailMutableArr = [NSMutableArray array];
    self.itemsMutableArr = [NSMutableArray array];
    
    if (self.transferType == BBTMyFinaniceTransferTypeToBP){
        chooseType = @"BBT转BP";
    } else if (self.transferType == BBTMyFinaniceTransferTypeToBBT){
        chooseType = @"BP转BBT";
    }
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.detailTableView){
        self.detailTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.detailTableView.orgin_y = CGRectGetMaxY(self.headerView.frame);
        self.detailTableView.size_height -= CGRectGetMaxY(self.headerView.frame);
        self.detailTableView.dataSource = self;
        self.detailTableView.delegate = self;
        [self.view addSubview:self.detailTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  self.detailMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    CenterBBTDetailInfoTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if(!cellWithRowOne){
        cellWithRowOne =[[CenterBBTDetailInfoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    CenterBBTMyFinaniceSubHistoryModel *singleModel = [self.detailMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.hasHiddenStatus = YES;
    cellWithRowOne.transferHistorySingleModel = singleModel;
    return cellWithRowOne;
}



#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CenterBBTDetailInfoTableViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.detailTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.itemsMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.itemsMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}


-(void)tapManager{

}

#pragma mark - Interface
-(void)InterfaceManagerWithReload:(BOOL)reload{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] assetsGetFinanceHistoryManagerWithType:self.transferFinaniceType beginTime:beginTimeInterval endTime:endTimeInterval content:chooseType Block:^(CenterBBTMyFinaniceHistoryModel * _Nonnull listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (reload || strongSelf.detailTableView.isXiaLa){
            [strongSelf.detailMutableArr removeAllObjects];
        }
        if (!strongSelf.itemsMutableArr.count){
            [strongSelf.itemsMutableArr addObjectsFromArray:listModel.type_list];
        }
        
        [strongSelf.detailMutableArr addObjectsFromArray:listModel.history_list];
        [strongSelf.detailTableView reloadData];
        
        if(listModel.history_list.count){
            [strongSelf.detailTableView dismissPrompt];
        } else {
            [strongSelf.detailTableView showPrompt:@"当前没有历史数据" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
        }
        
        strongSelf.headerView.transferHistoryModel = listModel;
    }];
}

@end
