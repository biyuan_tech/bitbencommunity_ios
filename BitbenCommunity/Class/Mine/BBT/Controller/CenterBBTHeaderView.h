//
//  CenterBBTHeaderView.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/31.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterBBTSingleModel.h"
#import "CenterBBTViewController.h"
#import "CenterBBTMyFinaniceModel.h"

@class CenterBBTViewController;

@interface CenterBBTHeaderViewItems:UIView

@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UILabel *fixedLabel;

@end

@interface CenterBBTHeaderView : UIView

@property (nonatomic,assign)CenterBBTViewControllerType transferControllerType;             /**< 传入当前类型*/
@property (nonatomic,strong)CenterBBTMyFinaniceModel *transferBBTMyFinaniceModel;
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;

-(void)actionClickManagerWithChange;

-(void)actionClickWithQuestionManagerBlock:(void(^)())block;
-(void)actionBackButtonClick:(void(^)())block;
-(void)segmentListActionClickBlock:(void(^)(NSInteger index))items;
+(CGFloat)calculationHeaderViewWithType:(CenterBBTViewControllerType) transferControllerType;
-(void)actionClickWithDetailManagerBlock:(void(^)())block;
-(void)actionSwapGestureManagerWithBlock:(void(^)(BOOL hasLeft))block;

@end

