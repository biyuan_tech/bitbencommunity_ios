//
//  CenterBBTQuestionViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/24.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterBBTQuestionViewController.h"

@interface CenterBBTQuestionViewController ()

@end

@implementation CenterBBTQuestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"问题";
}
@end
