//
//  CenterBBTDetailInfoTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/24.
//  Copyright © 2019 币本. All rights reserved.
//

#import "CenterBBTDetailInfoTableViewCell.h"

@interface CenterBBTDetailInfoTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *descLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *yueLabel;
@property (nonatomic,strong)UILabel *statusLabel;
@property (nonatomic,strong)PDImageView *arrowImgView;
@end

@implementation CenterBBTDetailInfoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"393939"];
    [self addSubview:self.titleLabel];
    
    self.descLabel = [GWViewTool createLabelFont:@"15" textColor:@"393939"];
    [self addSubview:self.descLabel];
    
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"999999"];
    [self addSubview:self.timeLabel];
    
    self.yueLabel = [GWViewTool createLabelFont:@"12" textColor:@"999999"];
    [self addSubview:self.yueLabel];
    
    self.statusLabel = [GWViewTool createLabelFont:@"12" textColor:@"999999"];
    [self addSubview:self.statusLabel];
    
    self.arrowImgView = [[PDImageView alloc]init];
    self.arrowImgView.backgroundColor = [UIColor clearColor];
    self.arrowImgView.image = [UIImage imageNamed:@"icon_wallet_detail_arrow"];
    self.arrowImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(6), ([CenterBBTDetailInfoTableViewCell calculationCellHeight] - LCFloat(12)) / 2., LCFloat(6), LCFloat(12));
    self.arrowImgView.hidden = YES;
    [self addSubview:self.arrowImgView];
    
}

-(void)setHasHiddenStatus:(BOOL)hasHiddenStatus{
    _hasHiddenStatus = hasHiddenStatus;
}

-(void)setTransferHistorySingleModel:(CenterBBTMyFinaniceSubHistoryModel *)transferHistorySingleModel{
    _transferHistorySingleModel = transferHistorySingleModel;
    
    self.titleLabel.text = transferHistorySingleModel.content;
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake(LCFloat(16), LCFloat(15), titleSize.width, titleSize.height);
    
    self.descLabel.text = [NSString stringWithFormat:@"%@%.2f",transferHistorySingleModel.status == 1?@"-":@"+",transferHistorySingleModel.transaction_amount];
    CGSize descSize = [Tool makeSizeWithLabel:self.descLabel];
    self.descLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(16) - descSize.width, self.titleLabel.orgin_y, descSize.width, descSize.height);
    
    self.timeLabel.text = transferHistorySingleModel.datetime;
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(8), timeSize.width, timeSize.height);
    
    self.yueLabel.text = [NSString stringWithFormat:@"余额:%.2f",transferHistorySingleModel.coin];
    CGSize yueSize = [Tool makeSizeWithLabel:self.yueLabel];
    self.yueLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(16) - yueSize.width, self.timeLabel.orgin_y, yueSize.width, yueSize.height);
    
    
    if (transferHistorySingleModel.transaction_id.length){
        if (self.transferHistorySingleModel.transaction_status == -1){
            self.statusLabel.text = @"返还";
            self.statusLabel.textColor = [UIColor hexChangeFloat:@"393939"];
        } else if (self.transferHistorySingleModel.transaction_status == 0){
            self.statusLabel.text = @"等待打包";
            self.statusLabel.textColor = [UIColor hexChangeFloat:@"377AFF"];
        } else if (self.transferHistorySingleModel.transaction_status == 1){
            self.statusLabel.text = @"打包中";
            self.statusLabel.textColor = [UIColor hexChangeFloat:@"377AFF"];
        } else if (self.transferHistorySingleModel.transaction_status == 2){
            self.statusLabel.text = @"完成";
            self.statusLabel.textColor = [UIColor hexChangeFloat:@"01CB4D"];
        } else if (self.transferHistorySingleModel.transaction_status == 3){
            self.statusLabel.text = @"失败";
            self.statusLabel.textColor = [UIColor hexChangeFloat:@"FF7C1C"];
        } else if (self.transferHistorySingleModel.transaction_status == 10){
            self.statusLabel.text = @"待审核";
            self.statusLabel.textColor = [UIColor hexChangeFloat:@"377AFF"];
        } else if (self.transferHistorySingleModel.transaction_status == 11){
            self.statusLabel.text = @"被驳回";
            self.statusLabel.textColor = [UIColor hexChangeFloat:@"FF7C1C"];
        } else if (self.transferHistorySingleModel.transaction_status == 12){
            self.statusLabel.text = @"被驳回";
            self.statusLabel.textColor = [UIColor hexChangeFloat:@"FF7C1C"];
        } else if (self.transferHistorySingleModel.transaction_status == 13){
            self.statusLabel.text = @"被驳回";
            self.statusLabel.textColor = [UIColor hexChangeFloat:@"FF7C1C"];
        }
    
        
        if (self.hasHiddenStatus){
            self.statusLabel.text = @"";
            self.arrowImgView.hidden = YES;
        } else {
            self.arrowImgView.hidden = NO;
        }
        
        CGSize statusSize = [Tool makeSizeWithLabel:self.statusLabel];
        self.descLabel.orgin_x = self.arrowImgView.orgin_x - self.descLabel.size_width - LCFloat(15);
        
        self.statusLabel.frame = CGRectMake(self.arrowImgView.orgin_x - statusSize.width - LCFloat(15), self.yueLabel.orgin_y, statusSize.width, statusSize.height);
        
        self.yueLabel.orgin_x = self.statusLabel.orgin_x - LCFloat(3) - self.yueLabel.size_width;
        self.arrowImgView.hidden = NO;
    } else {
        self.arrowImgView.hidden = YES;
    }
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(15);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"14"]];
    cellHeight += LCFloat(8);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    cellHeight += LCFloat(16);
    return cellHeight;
}

@end
