//
//  CenterBBTDetailTableViewHeaderView.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/4/23.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterBBTMyFinaniceHistoryModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CenterBBTDetailTableViewHeaderView : UIView

@property (nonatomic,strong)CenterBBTMyFinaniceHistoryModel *transferHistoryModel;
@property (nonatomic,strong)UIButton *transferTypeButton;


-(void)changeInfoType:(NSString *)infoType;
-(void)changeTime:(NSString *)time;

-(void)actionClickWithLeftChooseTime:(void(^)())block;
-(void)actionClickWithRightChooseType:(void(^)())block;

+(CGFloat)calculationViewHeight;

@end

NS_ASSUME_NONNULL_END
