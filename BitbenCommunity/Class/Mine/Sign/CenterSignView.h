//
//  CenterSignView.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/10.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CenterSignView : UIView

-(void)actionClickWithSignBlock:(void(^)())block;

@property (nonatomic,assign)CGFloat transferBBT;

+(CGSize)calculationSize ;



@end
