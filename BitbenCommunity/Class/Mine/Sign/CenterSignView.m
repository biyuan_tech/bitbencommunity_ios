//
//  CenterSignView.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/10.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterSignView.h"

static char actionClickWithSignBlockKey;
@interface CenterSignView()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UILabel *titleFixedLabel1;
@property (nonatomic,strong)UILabel *titleFixedLabel2;
@property (nonatomic,strong)UILabel *titleDymicLabel;
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UIButton *actionButton;
@end

@implementation CenterSignView

//[JCAlertView initWithCustomView:imgView dismissWhenTouchedBackground:NO];

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.bgImgView.image = [UIImage imageNamed:@"bg_center_sign"];
    self.bgImgView.frame = self.bounds;
    [self addSubview:self.bgImgView];
    
    // title
    self.titleFixedLabel1 = [GWViewTool createLabelFont:@"16" textColor:@"424242"];
    [self.bgImgView addSubview:self.titleFixedLabel1];
    
    self.titleFixedLabel2 = [GWViewTool createLabelFont:@"16" textColor:@"424242"];
    [self.bgImgView addSubview:self.titleFixedLabel2];
    
    self.titleDymicLabel = [GWViewTool createLabelFont:@"16" textColor:@"FAB800"];
    [self.bgImgView addSubview:self.titleDymicLabel];
    
    self.fixedLabel = [GWViewTool createLabelFont:@"13" textColor:@"B4B4B4"];
    [self.bgImgView addSubview:self.fixedLabel];
    
    self.bgImgView.userInteractionEnabled = YES;
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bgImgView addSubview:self.actionButton];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithSignBlockKey);
        if (block){
            block();
        }
    }];
}

-(void)setTransferBBT:(CGFloat)transferBBT{
    _transferBBT = transferBBT;
    // fixedLabel 1
    self.titleFixedLabel1.text = @"恭喜您今日获得";
    CGSize title1Size = [Tool makeSizeWithLabel:self.titleFixedLabel1];
    
    // fixedLabel2
    self.titleFixedLabel2.text = @"BP";
    CGSize title2Size = [Tool makeSizeWithLabel:self.titleFixedLabel2];
    
    // 3. dymic
    self.titleDymicLabel.text = [NSString stringWithFormat:@"%li",(long)transferBBT];
    CGSize dymicSize = [Tool makeSizeWithLabel:self.titleDymicLabel];
    
    // 4.fixedLabel
    self.fixedLabel.text = @"可在【我的钱包】中查看";
    
    // btn
    [self.actionButton setTitle:@"立即查看" forState:UIControlStateNormal];
    
    CGFloat margin = (self.size_width - title1Size.width - title2Size.width - dymicSize.width) / 2.;
    self.titleFixedLabel1.frame = CGRectMake(margin, self.size_height - LCFloat(120) - [NSString contentofHeightWithFont:self.titleFixedLabel1.font], title1Size.width, title1Size.height);
    
    self.titleDymicLabel.frame = CGRectMake(CGRectGetMaxX(self.titleFixedLabel1.frame), self.titleFixedLabel1.orgin_y, dymicSize.width, dymicSize.height);
    
    self.titleFixedLabel2.frame = CGRectMake(CGRectGetMaxX(self.titleDymicLabel.frame), self.titleFixedLabel1.orgin_y, title2Size.width, title2Size.height);
    
    self.fixedLabel.frame = CGRectMake(0, CGRectGetMaxY(self.titleFixedLabel1.frame) + LCFloat(11), self.size_width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    
    self.actionButton.frame = CGRectMake(0, self.size_height - LCFloat(38) - LCFloat(34), LCFloat(100), LCFloat(34));
    
    [self.actionButton setTitle:@"立即查看" forState:UIControlStateNormal];
    [self.actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.actionButton.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    self.actionButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"13"];
    self.actionButton.clipsToBounds = YES;
    self.actionButton.layer.cornerRadius = MIN(self.actionButton.size_height, self.actionButton.size_width) / 2.;
    self.actionButton.center_x = self.center_x;
}


-(void)actionClickWithSignBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithSignBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGSize)calculationSize {
    return  CGSizeMake(LCFloat(262), LCFloat(267));
}

@end
