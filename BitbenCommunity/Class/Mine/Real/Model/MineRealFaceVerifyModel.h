//
//  MineRealFaceVerifyModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/5/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MineRealFaceVerifyModel : FetchModel

@property (nonatomic,copy)NSString * verify_token;

@end

NS_ASSUME_NONNULL_END
