//
//  MineRealWalletModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/1.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MineRealWalletModel : FetchModel

@property (nonatomic,assign)NSInteger organ_type;
@property (nonatomic,copy)NSString *status;


@end

NS_ASSUME_NONNULL_END
