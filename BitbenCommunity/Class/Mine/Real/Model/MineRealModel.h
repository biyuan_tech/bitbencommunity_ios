//
//  MineRealModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/13.
//  Copyright © 2018 币本. All rights reserved.
//

#import "FetchModel.h"
#import "MineRealChooseTableViewCell.h"
@class MineRealChooseTableViewCell;
NS_ASSUME_NONNULL_BEGIN

@interface MineRealModel : FetchModel

@property (nonatomic,assign)MineRealChooseTableViewCellType chooseType;             /**< 选中的身份类型*/
@property (nonatomic,copy)NSString *infoStr;                /**< 主内容*/
@property (nonatomic,copy)NSString *infoSubStr;             /**< 临时调用*/
@property (nonatomic,copy)NSString *infoSubSubStr;             /**< 临时调用*/
@property (nonnull,copy)NSString *zhengmingStr;             /**< 证明*/

@property (nonatomic,strong)UIImage *image1;
@property (nonatomic,strong)UIImage *image2;
@property (nonatomic,strong)UIImage *image3;

@property (nonatomic,copy)NSString *imgUrl1;
@property (nonatomic,copy)NSString *imgUrl2;
@property (nonatomic,copy)NSString *imgUrl3;

@property (nonatomic,copy)NSString *status;//-3 图片不清晰未通过 -2 材料不全未通过 -1 材料不真实未通过 0 待审核 1 通过
@property (nonatomic,copy)NSString *inputStr;

@property (nonatomic,copy)NSString *handle_user_id;   //处理人ID
@property (nonatomic,copy)NSString *user_certification_id;   //认证记录ID
@property (nonatomic,copy)NSString *postscript;

@end

NS_ASSUME_NONNULL_END
