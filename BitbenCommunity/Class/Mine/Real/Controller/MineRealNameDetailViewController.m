//
//  MineRealNameDetailViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/13.
//  Copyright © 2018 币本. All rights reserved.
//

#import "MineRealNameDetailViewController.h"
#import "MineRealChoosePhotoTableViewCell.h"
#import "MineRealModel.h"
#import "GWAssetsImgSelectedViewController.h"
#import "NetworkAdapter+Center.h"
#import "ACEExpandableTextCell.h"

@interface MineRealNameDetailViewController ()<UITableViewDelegate,UITableViewDataSource>{
    GWButtonTableViewCell *btnCell;
    GWInputTextViewTableViewCell  *inputViewCell;
}
@property (nonatomic,strong)UITableView *realTableView;
@property (nonatomic,strong)NSArray *realArr;

@end

@implementation MineRealNameDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createNotifi];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"实名认证";
    self.view.backgroundColor = [UIColor whiteColor];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    if (self.transferType == MineRealChooseTableViewCellTypePerson){            // 个人
        self.realArr = @[@[@"身份证正面",@"身份证正面选择"],@[@"身份证反面",@"身份证反面选择"],@[@"手持身份证",@"手持身份证选择"],@[@"提交",@"提交文字"]];
    } else if (self.transferType == MineRealChooseTableViewCellTypeTeacher){    // 导师
        self.realArr = @[@[@"身份证正面",@"身份证正面选择"],@[@"身份证反面",@"身份证反面选择"],@[@"输入框"],@[@"提交",@"提交文字"]];
    } else if (self.transferType == MineRealChooseTableViewCellTypeLtd){        // 公司
        self.realArr = @[@[@"组织机构代码",@"组织机构代码选择"],@[@"提交",@"提交文字"]];
    }
}

#pragma mark - UITableVIew
-(void)createTableView{
    if (!self.realTableView){
        self.realTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.realTableView.dataSource = self;
        self.realTableView.delegate = self;
        [self.view addSubview:self.realTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.realArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.realArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提交" sourceArr:self.realArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"提交" sourceArr:self.realArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"提交";
        btnCell = cellWithRowThr;
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToUploadInfo];
        }];
        [self btnStatus];
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提交文字" sourceArr:self.realArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"提交文字" sourceArr:self.realArr]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        GWNormalTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        cellWithRowFour.transferTitle = @"实名后不可修改，完成认证后可领取奖励";
        cellWithRowFour.textAlignmentCenter = YES;
        cellWithRowFour.titleLabel.textColor = [UIColor colorWithCustomerName:@"红"];
        return cellWithRowFour;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"输入框" sourceArr:self.realArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"输入框" sourceArr:self.realArr]){
        static NSString *cellIdentifyWithRowFour1 = @"cellIdentifyWithRowFour1";
        GWInputTextViewTableViewCell  *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour1];
        if (!cellWithRowFour){
            cellWithRowFour = [[GWInputTextViewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour1];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        cellWithRowFour.transferPlaceholder = @"请输入微博、微信公众号名称或链接，或其他平台能证明自己影响力的连接。";
        inputViewCell = cellWithRowFour;
        __weak typeof(self)weakSelf = self;
        [cellWithRowFour textViewBeginChangeblock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.realTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
            });
        }];
        
        [cellWithRowFour textViewTextDidChangedBlock:^(NSString *info) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.transferRealModel.inputStr = info;
            [strongSelf btnStatus];
        }];
        cellWithRowFour.transferInputText = self.transferRealModel.inputStr;
        return cellWithRowFour;
        
    } else {
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            if (self.transferType == MineRealChooseTableViewCellTypePerson){           // 个人
                if (indexPath.section == 0){
                    cellWithRowOne.transferTitle = @"身份证正面(*)";
                } else if (indexPath.section == 1){
                    cellWithRowOne.transferTitle = @"身份证反面(*)";
                } else if (indexPath.section == 2){
                    cellWithRowOne.transferTitle = @"免冠手持身份证正面+手写币本和日期（*）";
                }
            } else if (self.transferType == MineRealChooseTableViewCellTypeTeacher){    // 讲师
                if (indexPath.section == 0){
                    cellWithRowOne.transferTitle = @"身份证正面(*)";
                } else if (indexPath.section == 1){
                    cellWithRowOne.transferTitle = @"身份证反面(*)";
                } else if (indexPath.section == 2){
                    cellWithRowOne.transferTitle = @"";
                }
            } else if (self.transferType == MineRealChooseTableViewCellTypeLtd){        // 公司
                if (indexPath.section == 0){
                    cellWithRowOne.transferTitle = @"组织机构代码证或统一社会信用代码（*）";
                }
            }
            cellWithRowOne.textAlignmentCenter = YES;
            return cellWithRowOne;
        } else {
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            MineRealChoosePhotoTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[MineRealChoosePhotoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            }
            __weak typeof(self)weakSelf = self;
            [cellWithRowTwo actionClickWithCloseImgBlock:^{
                if (!weakSelf){
                    return ;
                }
                cellWithRowTwo.transferImg = nil;
            }];
            
            if (self.transferType == MineRealChooseTableViewCellTypePerson){           // 个人
                if (indexPath.section == 0){
                    cellWithRowTwo.transferType = MineRealChoosePhotoTableViewCellType1;
                } else if (indexPath.section == 1){
                    cellWithRowTwo.transferType = MineRealChoosePhotoTableViewCellType2;
                } else if (indexPath.section == 2){
                    cellWithRowTwo.transferType = MineRealChoosePhotoTableViewCellType3;
                }
            } else if (self.transferType == MineRealChooseTableViewCellTypeTeacher){    // 讲师
                if (indexPath.section == 0){
                    cellWithRowTwo.transferType = MineRealChoosePhotoTableViewCellType1;
                } else if (indexPath.section == 1){
                    cellWithRowTwo.transferType = MineRealChoosePhotoTableViewCellType2;
                } else if (indexPath.section == 2){

                }
            } else if (self.transferType == MineRealChooseTableViewCellTypeLtd){        // 公司
                if (indexPath.section == 0){
                    cellWithRowTwo.transferType = MineRealChoosePhotoTableViewCellType4;
                }
            }
            
            if (indexPath.section == 0){
                cellWithRowTwo.transferImg = self.transferRealModel.image1;
            } else if (indexPath.section == 1){
                cellWithRowTwo.transferImg = self.transferRealModel.image2;
            } else if (indexPath.section == 2){
                cellWithRowTwo.transferImg = self.transferRealModel.image3;
            }
            
            return cellWithRowTwo;
        }
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0){
        
    } else {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if ([cell isKindOfClass:[MineRealChoosePhotoTableViewCell class]]){
            MineRealChoosePhotoTableViewCell *tempCell = (MineRealChoosePhotoTableViewCell *)cell;
            if (!tempCell.transferImg){
                GWAssetsLibraryViewController *assetViewController = [[GWAssetsLibraryViewController alloc]init];
                __weak typeof(self)weakSelf = self;
                [assetViewController selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    UIImage *image = [selectedImgArr lastObject];
                    if (strongSelf.transferType == MineRealChooseTableViewCellTypePerson){      // 个人
                        if (indexPath.section == 0){
                            strongSelf.transferRealModel.image1 = image;
                        } else if (indexPath.section == 1){
                            strongSelf.transferRealModel.image2 = image;
                        } else if (indexPath.section == 2){
                            strongSelf.transferRealModel.image3 = image;
                        }
                    } else if (strongSelf.transferType == MineRealChooseTableViewCellTypeTeacher){  // 导师
                        if (indexPath.section == 0){
                            strongSelf.transferRealModel.image1 = image;
                        } else if (indexPath.section == 1){
                            strongSelf.transferRealModel.image2 = image;
                        }
                    } else if (strongSelf.transferType == MineRealChooseTableViewCellTypeLtd){      // 公司
                        if (indexPath.section == 0){
                            strongSelf.transferRealModel.image1 = image;
                        }
                    }
                    
                    [strongSelf uploadImg:image index:indexPath.section block:NULL];
                    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                }];
                [self.navigationController pushViewController:assetViewController animated:YES];
            } else {
                GWAssetsImgSelectedViewController *assetViewController = [[GWAssetsImgSelectedViewController alloc]init];
                UIImage *img;
                if (indexPath.section == 0){
                    img = self.transferRealModel.image1;
                } else if (indexPath.section == 1){
                    img = self.transferRealModel.image2;
                } else if (indexPath.section == 2){
                    img = self.transferRealModel.image3;
                }
                [assetViewController showInView:self.parentViewController imgArr:@[img] currentIndex:0 cell:tempCell];
            }
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提交" sourceArr:self.realArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"提交" sourceArr:self.realArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提交文字" sourceArr:self.realArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"提交文字" sourceArr:self.realArr]){
        return [GWNormalTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"输入框" sourceArr:self.realArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"输入框" sourceArr:self.realArr]){
        return [GWInputTextViewTableViewCell calculationCellHeight];
    } else {
        if (indexPath.row == 0){
            return [GWNormalTableViewCell calculationCellHeight];
        } else {
            return [MineRealChoosePhotoTableViewCell calculationCellHeight];
        }
    }
}

#pragma mark - 上传图片
-(void)uploadImg:(UIImage *)img index:(NSInteger)index block:(void(^)(NSString *))block{
    OSSFileModel *fileModel = [[OSSFileModel alloc]init];
    fileModel.objcImage = img;
    __weak typeof(self)weakSelf = self;
    [[OSSManager sharedUploadManager] uploadImageManagerWithImageList:[@[fileModel] copy] withUrlBlock:^(NSArray *imgUrlArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSString *imgUrl = [imgUrlArr lastObject];
        if (index == 0){
            strongSelf.transferRealModel.imgUrl1 = imgUrl;
        } else if (index == 1){
            strongSelf.transferRealModel.imgUrl2 = imgUrl;
        } else if (index == 2){
            strongSelf.transferRealModel.imgUrl3 = imgUrl;
        }
        [strongSelf btnStatus];
    }];
}

#pragma mark - interface
-(void)sendRequestToUploadInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerShiminrenzhengManagerWithModel:self.transferRealModel block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [[UIAlertView alertViewWithTitle:@"实名认证" message:@"实名认证已提交到服务器，请耐心等待" buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {

                [strongSelf.navigationController popToRootViewControllerAnimated:YES];
            }]show];
        }
    }];
}



#pragma mark - 键盘代理
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.realTableView.frame = newTextViewFrame;
    [UIView commitAnimations];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.realTableView.frame = self.view.bounds;
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}



-(void)btnStatus{
    if (self.transferType == MineRealChooseTableViewCellTypePerson){            // 个人
        if (self.transferRealModel.imgUrl1.length && self.transferRealModel.imgUrl2.length && self.transferRealModel.imgUrl3.length){
            [btnCell setButtonStatus:YES];
        } else {
            [btnCell setButtonStatus:NO];
        }
    } else if (self.transferType == MineRealChooseTableViewCellTypeTeacher){    // 导师
        if (self.transferRealModel.imgUrl1.length && self.transferRealModel.imgUrl2.length && self.transferRealModel.inputStr.length){
            [btnCell setButtonStatus:YES];
        } else {
            [btnCell setButtonStatus:NO];
        }
    } else if (self.transferType == MineRealChooseTableViewCellTypeLtd){        // 企业
        if (self.transferRealModel.imgUrl1.length){
            [btnCell setButtonStatus:YES];
        } else {
            [btnCell setButtonStatus:NO];
        }
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if ([inputViewCell.inputTextView isFirstResponder]){
        [inputViewCell.inputTextView resignFirstResponder];
    }
}

@end
