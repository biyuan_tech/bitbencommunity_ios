//
//  MineRealNameViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/13.
//  Copyright © 2018 币本. All rights reserved.
//

#import "MineRealNameViewController.h"
#import "MineRealHeaderTableViewCell.h"
#import "MineRealNameDetailViewController.h"
#import "MineRealChooseTableViewCell.h"             // 身份选择
#import "NetworkAdapter+Center.h"
#import "WalletSecuritySettingPwdViewController.h"
#import "MineRealPersonWaringTableViewCell.h"
#import <RPSDK/RPSDK.h>
#import "PDHUD.h"

@interface MineRealNameViewController ()<UITableViewDelegate,UITableViewDataSource>{
    MineRealChooseTableViewCellType chooseType;
    GWInputTextFieldTableViewCell *inputCell1;
    GWInputTextFieldTableViewCell *inputCell2;
    GWButtonTableViewCell *buttonCell;
    
    MineRealChooseTableViewCell *realChooseCell;
}
@property (nonatomic,strong)UITableView *realTableView;
@property (nonatomic,strong)NSMutableArray *realMutableArr;
@property (nonatomic,strong)MineRealModel *moneRealModel;
@end

@implementation MineRealNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createNotifi];
    if (self.transferControllerType == MineRealNameViewControllerTypeWallet){           // 钱包进入
        [self adjustRealInfoManager];
    } else {
        [self sendShiMingStatus];                                                       // 获取实名认证状态
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"实名认证";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.realMutableArr = [NSMutableArray array];
    [self.realMutableArr addObjectsFromArray:@[@[@"实名认证标题"],@[@"身份标题",@"身份选择"]]];
    self.moneRealModel = [[MineRealModel alloc]init];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.realTableView){
        self.realTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.realTableView.dataSource = self;
        self.realTableView.delegate = self;
        [self.view addSubview:self.realTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.realMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.realMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"实名认证标题" sourceArr:self.realMutableArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        MineRealHeaderTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[MineRealHeaderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"身份标题" sourceArr:self.realMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"身份标题" sourceArr:self.realMutableArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"请选择您要认证的身份";
        cellWithRowTwo.titleLabel.orgin_x = LCFloat(15);
        cellWithRowTwo.titleLabel.font = [cellWithRowTwo.titleLabel.font boldFont];
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"身份选择" sourceArr:self.realMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"身份选择" sourceArr:self.realMutableArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        MineRealChooseTableViewCell *cellWithRowThr = [[MineRealChooseTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[MineRealChooseTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        __weak typeof(self)weakSelf = self;
        realChooseCell = cellWithRowThr;
        [cellWithRowThr actionClickWithChooseTypeBackBlock:^(MineRealChooseTableViewCellType type) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->chooseType = type;
            strongSelf.moneRealModel.chooseType = type;
            [strongSelf actionTypeManagerWithType:type];
        }];
        
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"主体信息登记" sourceArr:self.realMutableArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"个人提示" sourceArr:self.realMutableArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"个人提示" sourceArr:self.realMutableArr]){
            static NSString *cellIdentifyWithRowSev = @"cellIdentifyWithRowSev";
            MineRealPersonWaringTableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev];
            if (!cellWithRowSev){
                cellWithRowSev = [[MineRealPersonWaringTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev];
                cellWithRowSev.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowSev.transferType = chooseType;
            
            return cellWithRowSev;
        } else {
            if (indexPath.row == [self cellIndexPathRowWithcellData:@"主体信息登记" sourceArr:self.realMutableArr]){
                static NSString *cellIdentifyWithOther = @"cellIdentifyWithOther";
                GWNormalTableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithOther];
                if (!cellWithRowOther){
                    cellWithRowOther = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithOther];
                }
                cellWithRowOther.transferCellHeight = cellHeight;
                cellWithRowOther.transferTitle = @"主体信息登记";
                cellWithRowOther.titleLabel.orgin_x = LCFloat(15);
                cellWithRowOther.titleLabel.font = [cellWithRowOther.titleLabel.font boldFont];
                return cellWithRowOther;
            } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"下一步" sourceArr:self.realMutableArr]){
                static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
                GWButtonTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
                if (!cellWithRowFiv){
                    cellWithRowFiv = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                }
                cellWithRowFiv.transferCellHeight = cellHeight;
                buttonCell = cellWithRowFiv;
                [self buttonStatusManager];
                cellWithRowFiv.transferTitle = @"下一步";
                __weak typeof(self)weakSelf = self;
                [cellWithRowFiv buttonClickManager:^{
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    if (strongSelf.transferControllerType == MineRealNameViewControllerTypeRoot){
                        if(strongSelf->chooseType == MineRealChooseTableViewCellTypePerson){
                            [strongSelf faceVerifyInfoManager];
                        } else {
                            MineRealNameDetailViewController *mineRealVC = [[MineRealNameDetailViewController alloc]init];
                            mineRealVC.transferRealModel = strongSelf.moneRealModel;
                            mineRealVC.transferType = strongSelf->chooseType;
                            [strongSelf.navigationController pushViewController:mineRealVC animated:YES];
                        }
                    } else if (strongSelf.transferControllerType == MineRealNameViewControllerTypeWallet){
                        [strongSelf adjustToRealMineManager];
                    }
                    
                }];
                return cellWithRowFiv;
            } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"实名提示" sourceArr:self.realMutableArr]){
                static NSString *cellIdentifyWithRowSix = @"cellIdentifyWithRowSix";
                GWNormalTableViewCell *cellWithRowSix = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSix];
                if (!cellWithRowSix){
                    cellWithRowSix = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSix];
                    cellWithRowSix.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                cellWithRowSix.transferCellHeight = cellHeight;
                cellWithRowSix.transferTitle = @"实名后不可修改，完成认证后可领取奖励";
                cellWithRowSix.titleLabel.textAlignment = NSTextAlignmentCenter;
                cellWithRowSix.titleLabel.font = [UIFont fontWithCustomerSizeName:@"12"];
                cellWithRowSix.titleLabel.textColor = [UIColor colorWithCustomerName:@"红"];
                cellWithRowSix.textAlignmentCenter = YES;
                
                return cellWithRowSix;
            } else {
                static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
                GWInputTextFieldTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
                if (!cellWithRowFour){
                    cellWithRowFour = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
                }
                cellWithRowFour.transferCellHeight = cellHeight;
                cellWithRowFour.transferPlaceholeder = [[self.realMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
                cellWithRowFour.inputTextField.orgin_x = LCFloat(16);
                
                __weak typeof(self)weakSelf = self;
                [cellWithRowFour.inputTextField showBarCallback:^(UITextField *textField, NSInteger buttonIndex, UIButton *button) {
                    if (!weakSelf){
                        return ;
                    }
                    
                    if ([textField isFirstResponder]){
                        [textField resignFirstResponder];
                    }
                }];
                
                if (indexPath.row == 2){
                    inputCell1 = cellWithRowFour;
                } else if (indexPath.row == 3){
                    inputCell2 = cellWithRowFour;
                }
                
                
                [cellWithRowFour textFieldDidChangeBlock:^(NSString *info) {
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                   
                    
                    if (strongSelf->chooseType == MineRealChooseTableViewCellTypeTeacher){        // 讲师

                        if (indexPath.row == 2){
                           strongSelf.moneRealModel.infoStr = info;
                       } else if (indexPath.row == 3){
                           strongSelf.moneRealModel.infoSubStr = info;
                       }
                    } else if (strongSelf->chooseType == MineRealChooseTableViewCellTypePerson){      // 个人

                    } else if (strongSelf->chooseType == MineRealChooseTableViewCellTypeLtd){
                         if (indexPath.row == 2){
                            strongSelf.moneRealModel.infoStr = info;
                        } else if (indexPath.row == 3){
                            strongSelf.moneRealModel.infoSubStr = info;
                        }
                    }
                    
                    [strongSelf buttonStatusManager];
                }];
                
                if (chooseType == MineRealChooseTableViewCellTypeLtd){
                    if (indexPath.row == 1){
                        cellWithRowFour.inputTextField.keyboardType = UIKeyboardTypeEmailAddress;
                    } else if (indexPath.row == 2){
                        cellWithRowFour.inputTextField.keyboardType = UIKeyboardTypeEmailAddress;
                    }
                } else if (chooseType == MineRealChooseTableViewCellTypePerson || chooseType == MineRealChooseTableViewCellTypeTeacher){
                    if (indexPath.row == 1){
                        cellWithRowFour.inputTextField.keyboardType = UIKeyboardTypeDefault;
                    } else if (indexPath.row == 2){
                        cellWithRowFour.inputTextField.keyboardType = UIKeyboardTypeEmailAddress;
                    }
                }
                return cellWithRowFour;
            }
        }
    } else {
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        GWInputTextFieldTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
        }
        return cellWithRowFour;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"实名认证标题" sourceArr:self.realMutableArr]){
        return [MineRealHeaderTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"身份标题" sourceArr:self.realMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"身份标题" sourceArr:self.realMutableArr]){
        return [GWNormalTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"身份选择" sourceArr:self.realMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"身份选择" sourceArr:self.realMutableArr]){
        return [MineRealChooseTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"主体信息登记" sourceArr:self.realMutableArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"个人提示" sourceArr:self.realMutableArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"个人提示" sourceArr:self.realMutableArr]){
            return [MineRealPersonWaringTableViewCell calculationCellHeightWithType:chooseType];
        } else {
            if(indexPath.row == 0){
                return [GWNormalTableViewCell calculationCellHeight];
            } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"下一步" sourceArr:self.realMutableArr]){
                return [GWButtonTableViewCell calculationCellHeight];
            } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"实名提示" sourceArr:self.realMutableArr]){
                return [GWNormalTableViewCell calculationCellHeight];
            } else {
                return [GWInputTextFieldTableViewCell calculationCellHeight];
            }
        }
    } else {
        return [GWInputTextFieldTableViewCell calculationCellHeight];
    }
}


#pragma mark - 身份点击方法
-(void)actionTypeManagerWithType:(MineRealChooseTableViewCellType)type{
    inputCell1.inputTextField.text = @"";
    inputCell2.inputTextField.text = @"";
    
    BOOL hasInfo = NO;
    if (self.realMutableArr.count > 2){
        [self.realMutableArr removeAllObjects];
        [self.realMutableArr addObjectsFromArray:@[@[@"实名认证标题"],@[@"身份标题",@"身份选择"]]];
        hasInfo = YES;
    }
    
    NSInteger index = 0;
    if (type == MineRealChooseTableViewCellTypeTeacher){        // 讲师
        [self.realMutableArr addObjectsFromArray:@[@[@"个人提示",@"主体信息登记",@"请输入您的姓名",@"请输入18位身份证号码",@"下一步",@"实名提示"]]];
        index =  [self cellIndexPathSectionWithcellData:@"主体信息登记" sourceArr:self.realMutableArr];
    } else if (type == MineRealChooseTableViewCellTypePerson){      // 个人
        [self.realMutableArr addObjectsFromArray:@[@[@"个人提示",@"下一步",@"实名提示"]]];
        index =  [self cellIndexPathSectionWithcellData:@"个人提示" sourceArr:self.realMutableArr];
    } else if (type == MineRealChooseTableViewCellTypeLtd){
        [self.realMutableArr addObjectsFromArray:@[@[@"个人提示",@"主体信息登记",@"请输入企业或机构名称",@"请输入9位组织机构代码或18位的统一社会信用代码",@"下一步",@"实名提示"]]];
        index =  [self cellIndexPathSectionWithcellData:@"主体信息登记" sourceArr:self.realMutableArr];
    }
   
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
    if (hasInfo){
        [self.realTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    } else {
        [self.realTableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationBottom];
    }
}

-(void)buttonStatusManager{
    if (chooseType == MineRealChooseTableViewCellTypePerson){
        [buttonCell setButtonStatus:YES];
    } else {
        if (inputCell1.inputTextField.text.length && inputCell2.inputTextField.text.length){
            [buttonCell setButtonStatus:YES];
        } else {
            [buttonCell setButtonStatus:NO];
        }
    }
}

#pragma mark - 接口-获取实名认证状态
-(void)sendShiMingStatus{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] getShimingRenzhengStatusBlock:^(MineRealModel *model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        if (model.status.length){
            if ([model.status isEqualToString:@"-1"]){
                [strongSelf.realTableView showPrompt:@"您的实名认证审核失败,材料不真实未通过" withImage:[UIImage imageNamed:@"icon_real_img"] andImagePosition:PDPromptImagePositionNone tapBlock:^{
                    [strongSelf.realTableView dismissPrompt];
                }];
            } else if ([model.status isEqualToString:@"0"]){         // 审核中
                [strongSelf.realTableView showPrompt:@"您的实名认证正在审核中\r请耐心等待……" withImage:[UIImage imageNamed:@"icon_real_img"] andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            } else if ([model.status isEqualToString:@"1"]){          // 通过
                [strongSelf.realTableView showPrompt:@"您的实名认证已审核通过" withImage:[UIImage imageNamed:@"icon_real_img"] andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            } else if ([model.status isEqualToString:@"-2"]){
                [strongSelf.realTableView showPrompt:@"您的实名认证审核失败,材料不全未通过" withImage:[UIImage imageNamed:@"icon_real_img"] andImagePosition:PDPromptImagePositionNone tapBlock:^{
                    [strongSelf.realTableView dismissPrompt];
                }];
            } else if ([model.status isEqualToString:@"-3"]){
                [strongSelf.realTableView showPrompt:@"您的实名认证审核失败,图片不清晰" withImage:[UIImage imageNamed:@"icon_real_img"] andImagePosition:PDPromptImagePositionNone tapBlock:^{
                    [strongSelf.realTableView dismissPrompt];
                }];
            }
        } else {            // 表示没认证过
            [strongSelf.realTableView dismissPrompt];
        }
    }];
}


#pragma mark - 其他方法
#pragma mark 键盘代理
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.realTableView.frame = newTextViewFrame;
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.realTableView.frame = self.view.bounds;
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (inputCell1.inputTextField.isFirstResponder){
        [inputCell1.inputTextField resignFirstResponder];
    } else if (inputCell2.inputTextField.isFirstResponder){
        [inputCell2.inputTextField resignFirstResponder];
    }
}


#pragma mark - 获取判断实名认证信息
-(void)adjustRealInfoManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] adjustHasRealCertificationStatusManagerBlock:^(BOOL hasSuccessed, MineRealWalletModel * _Nonnull realModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (hasSuccessed){
            [strongSelf->realChooseCell walletChoosedType:realModel.organ_type hasSelected:NO];
            strongSelf->chooseType = realModel.organ_type;
            strongSelf.moneRealModel.chooseType = realModel.organ_type;
            [strongSelf actionTypeManagerWithType:realModel.organ_type];
        }
    }];
}

#pragma mark - 进行认证实名认证
-(void)adjustToRealMineManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] adjustRealInfoManagerWithName:inputCell1.inputTextField.text number:inputCell2.inputTextField.text block:^(BOOL hasSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (hasSuccessed){          // 【表示认证成功】
            strongSelf.transferFindBlockPwdModel.real_name = strongSelf->inputCell1.inputTextField.text;
            strongSelf.transferFindBlockPwdModel.ID_number = strongSelf->inputCell2.inputTextField.text;
            
            WalletSecuritySettingPwdViewController *settingPwdVC = [[WalletSecuritySettingPwdViewController alloc]init];
            settingPwdVC.transferType = WalletSecuritySettingPwdViewControllerTypeFind;             // 找回密码
            settingPwdVC.transferFindBlockPwdModel = strongSelf.transferFindBlockPwdModel;
            [strongSelf.navigationController pushViewController:settingPwdVC animated:YES];
        } else {
            [[UIAlertView alertViewWithTitle:@"认证失败" message:@"上传实名认证信息与最初信息不符，请重新再试" buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

#pragma mark - 个人-实名认证验证
-(void)faceVerifyInfoManager{
    [PDHUD showLongText:@"请等待……"];
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] faceVerifyManagerManagerBlock:^(NSString *info) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (info.length){
            [strongSelf faceInfo:info];
        } else {
            [PDHUD dismissManager];
        }
    }];
}

-(void)faceInfo:(NSString *)token{
    [PDHUD dismissManager];
    __weak typeof(self)weakSelf = self;
    [RPSDK start:token rpCompleted:^(AUDIT auditState) {
        if (!weakSelf){return ;}
        __strong typeof(weakSelf)strongSelf = weakSelf;

        NSLog(@"verifyResult = %ld",(unsigned long)auditState);
        if(auditState == AUDIT_PASS) { //认证通过。
            // 进行后端接口提交
            [strongSelf realInfoInsertManagerHasSuccessed:YES];
        }
        else if(auditState == AUDIT_FAIL) { //认证不通过。
            [strongSelf realInfoInsertManagerHasSuccessed:NO];
        } else if(auditState == AUDIT_IN_AUDIT) { //认证中，通常不会出现，只有在认证审核系统内部出现超时、未在限定时间内返回认证结果时出现。此时提示用户系统处理中，稍后查看认证结果即可。
        }
        else if(auditState == AUDIT_NOT) { //未认证，用户取消。
            
        }
        else if(auditState == AUDIT_EXCEPTION) { //系统异常。
//            [strongSelf realInfoInsertManagerHasSuccessed:NO];
        }
    }withVC:self.navigationController];
}

-(void)realInfoInsertManagerHasSuccessed:(BOOL)hasSuccessed{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] realVerifyManagerInsertSuccessedBlock:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (hasSuccessed){
            [strongSelf.realTableView showPrompt:@"您的实名认证已审核通过" withImage:[UIImage imageNamed:@"icon_real_img"] andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            } else {
                [strongSelf.realTableView showPrompt:@"您的实名认证审核失败，材料不真实未通过" withImage:[UIImage imageNamed:@"icon_real_img"] andImagePosition:PDPromptImagePositionTop tapBlock:^{
                    [strongSelf.realTableView dismissPrompt];
                }];
            }
        } else {
            [strongSelf.realTableView showPrompt:@"您的实名认证审核失败，材料不真实未通过" withImage:[UIImage imageNamed:@"icon_real_img"] andImagePosition:PDPromptImagePositionTop tapBlock:^{
                [strongSelf.realTableView dismissPrompt];
            }];
        }
    }];
}

@end
