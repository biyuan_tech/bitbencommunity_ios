//
//  MineRealNameViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/13.
//  Copyright © 2018 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "NetworkAdapter+Wallet.h"
#import "WalletFindPwdTempModel.h"              // 找回密码使用

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,MineRealNameViewControllerType) {
    MineRealNameViewControllerTypeRoot,
    MineRealNameViewControllerTypeWallet,
};

@interface MineRealNameViewController : AbstractViewController

@property (nonatomic,assign)MineRealNameViewControllerType transferControllerType;

@property (nonatomic,strong)WalletFindPwdTempModel *transferFindBlockPwdModel;
@end

NS_ASSUME_NONNULL_END
