//
//  MineRealNameDetailViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/13.
//  Copyright © 2018 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "MineRealChooseTableViewCell.h"
#import "MineRealModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MineRealNameDetailViewController : AbstractViewController

@property (nonatomic,assign)MineRealChooseTableViewCellType transferType;
@property (nonatomic,strong)MineRealModel *transferRealModel;

@end

NS_ASSUME_NONNULL_END
