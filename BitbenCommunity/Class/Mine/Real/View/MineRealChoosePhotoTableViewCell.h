//
//  MineRealChoosePhotoTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/13.
//  Copyright © 2018 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,MineRealChoosePhotoTableViewCellType) {
    MineRealChoosePhotoTableViewCellType1,              /**< 正面*/
    MineRealChoosePhotoTableViewCellType2,              /**< 反面*/
    MineRealChoosePhotoTableViewCellType3,              /**< 手持*/
    MineRealChoosePhotoTableViewCellType4,              /**< 营业执照*/
};

@interface MineRealChoosePhotoTableViewCell : PDBaseTableViewCell

@property (nonatomic,assign)MineRealChoosePhotoTableViewCellType transferType;
@property (nonatomic,strong)UIImage *__nullable transferImg;
@property (nonatomic,strong)UIView *convertView;
@property (nonatomic,strong)PDImageView *photoImgView;


-(void)actionClickWithCloseImgBlock:(void(^)())block;
@end

NS_ASSUME_NONNULL_END
