//
//  MineRealChoosePhotoTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/13.
//  Copyright © 2018 币本. All rights reserved.
//

#import "MineRealChoosePhotoTableViewCell.h"

static char actionClickWithCloseImgBlockKey;

@interface MineRealChoosePhotoTableViewCell()
@property (nonatomic,strong)UIImage *placeholderImg;
@property (nonatomic,strong)UIButton *closeButton;

@end

@implementation MineRealChoosePhotoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.convertView = [[UIView alloc]init];
    self.convertView.clipsToBounds = YES;
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.frame = CGRectMake(LCFloat(16), 0, kScreenBounds.size.width - 2 * LCFloat(16),LCFloat(197));
    [self addSubview:self.convertView];
    
    self.photoImgView = [[PDImageView alloc]init];
    self.photoImgView.backgroundColor = [UIColor clearColor];
    self.photoImgView.frame = self.convertView.bounds;
    [self.convertView addSubview:self.photoImgView];
    
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.closeButton.backgroundColor = [UIColor clearColor];
    [self.closeButton setImage:[UIImage imageNamed:@"icon_real_photo_close"] forState:UIControlStateNormal];
    self.closeButton.frame = CGRectMake(self.convertView.size_width - LCFloat(19) - 2 * LCFloat(9) ,0,  LCFloat(19) + 2 * LCFloat(9), LCFloat(19) + 2 * LCFloat(9));
    __weak typeof(self)weakSelf = self;
    [self.closeButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithCloseImgBlockKey);
        if (block){
            block();
        }
    }];
    [self.convertView addSubview:self.closeButton];
}

-(void)setTransferType:(MineRealChoosePhotoTableViewCellType)transferType{
    _transferType = transferType;
    if (transferType == MineRealChoosePhotoTableViewCellType1){                 // 正面
        self.placeholderImg = [UIImage imageNamed:@"icon_real_0_add"];
    } else if (self.transferType == MineRealChoosePhotoTableViewCellType2){     // 反面
        self.placeholderImg = [UIImage imageNamed:@"icon_real_1_add"];
    } else if (self.transferType == MineRealChoosePhotoTableViewCellType3){     // 手持
        self.placeholderImg = [UIImage imageNamed:@"icon_real_person"];
    } else if (self.transferType == MineRealChoosePhotoTableViewCellType4){
        self.placeholderImg = [UIImage imageNamed:@"icon_real_2_add"];
    }
    self.photoImgView.image = self.placeholderImg;
}

-(void)setTransferImg:(UIImage *__nullable)transferImg{
    _transferImg = transferImg;
    if (transferImg){
        [Tool setTransferSingleAsset:transferImg imgView:self.photoImgView convertView:self.convertView];
        self.closeButton.hidden = NO;
    } else {
        self.photoImgView.frame = self.convertView.bounds;
        self.photoImgView.image = self.placeholderImg;
        self.closeButton.hidden = YES;
    }
}

-(void)actionClickWithCloseImgBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithCloseImgBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(197);
}

@end
