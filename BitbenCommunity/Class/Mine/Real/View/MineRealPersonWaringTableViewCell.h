//
//  MineRealPersonWaringTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/5/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "MineRealChooseTableViewCell.h"
@class MineRealChooseTableViewCell;

NS_ASSUME_NONNULL_BEGIN

@interface MineRealPersonWaringTableViewCell : PDBaseTableViewCell

@property (nonatomic,assign)MineRealChooseTableViewCellType transferType;

+(CGFloat)calculationCellHeightWithType:(MineRealChooseTableViewCellType )type;

@end

NS_ASSUME_NONNULL_END
