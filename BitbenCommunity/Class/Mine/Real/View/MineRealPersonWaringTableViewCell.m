//
//  MineRealPersonWaringTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/5/30.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "MineRealPersonWaringTableViewCell.h"

@interface MineRealPersonWaringTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *mainLabel;
@end

@implementation MineRealPersonWaringTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.mainLabel = [GWViewTool createLabelFont:@"15" textColor:@"4C4C4C"];
    self.mainLabel.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(40));
    self.mainLabel.font = [self.mainLabel.font boldFont];
    [self addSubview:self.mainLabel];
    
    self.titleLabel = [GWViewTool createLabelFont:@"13" textColor:@"8F8F8F"];
    self.titleLabel.numberOfLines = 0;
    [self addSubview:self.titleLabel];

}

-(void)setTransferType:(MineRealChooseTableViewCellType)transferType{
    _transferType = transferType;
    if (transferType == MineRealChooseTableViewCellTypeLtd){        // 机构
        self.mainLabel.text = @"机构";
        self.titleLabel.text = @"区块链领域的媒体、项目、社区、资本、开发、协会、研究院、产业园、服务平台等等";
    } else if (transferType == MineRealChooseTableViewCellTypePerson){  // 个人
        self.mainLabel.text = @"个人";
        self.titleLabel.text = @"个人用户身份认证为系统自动认证，请您在确保是本人的前提下按照提示进行认证。另外系统可能存在一些识别误差，如果存在任何问题请添加微信：bibenwangluo";
    } else if (transferType == MineRealChooseTableViewCellTypeTeacher){ // 讲师
        self.mainLabel.text = @"产业人";
        self.titleLabel.text = @"从事区块链领域的作者、币圈大V、行情分析师、项目创始人、CEO、商务等等、或是第三方平台身份（微博、金色财经、巴比特等等）";
    }
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(15);
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.mainLabel.frame), width, titleSize.height);
}

+(CGFloat)calculationCellHeightWithType:(MineRealChooseTableViewCellType )type{
    CGFloat cellHeight = 0 ;
    NSString *info = @"";
    if (type == MineRealChooseTableViewCellTypeLtd){        // 机构
        info = @"区块链领域的媒体、项目、社区、资本、开发、协会、研究院、产业园、服务平台等等";
    } else if (type == MineRealChooseTableViewCellTypePerson){  // 个人
        info = @"个人用户身份认证为系统自动认证，请您在确保是本人的前提下按照提示进行认证。另外系统可能存在一些识别误差，如果存在任何问题请添加微信：bibenwangluo";
    } else if (type == MineRealChooseTableViewCellTypeTeacher){ // 讲师
        info = @"从事区块链领域的作者、币圈大V、行情分析师、项目创始人、CEO、商务等等、或是第三方平台身份（微博、金色财经、巴比特等等）";
    }

    CGSize infoSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"13"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(30), CGFLOAT_MAX)];
       
       cellHeight = infoSize.height;
    cellHeight+= LCFloat(40);
    cellHeight+= LCFloat(15);
       return cellHeight;
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0 ;
    NSString *info = @"个人用户身份认证为系统自动认证，请您在确保是本人的前提下按照提示进行认证。另外系统可能存在一些识别误差，如果存在任何问题请添加微信bibenwangluo";
    CGSize infoSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"13"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(30), CGFLOAT_MAX)];
    cellHeight = infoSize.height;
    cellHeight += LCFloat(40);
    return cellHeight;
}

@end
