//
//  MineRealChooseTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2018/11/13.
//  Copyright © 2018 币本. All rights reserved.
//

#import "MineRealChooseTableViewCell.h"

static char actionClickWithChooseTypeBackBlockKey;
@interface MineRealChooseTableViewCell()
@property (nonatomic,strong)MineRealChooseTableViewCellView *personView;
@property (nonatomic,strong)MineRealChooseTableViewCellView *teacherView;
@property (nonatomic,strong)MineRealChooseTableViewCellView *ltdView;
@end

@implementation MineRealChooseTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    
    __weak typeof(self)weakSelf = self;
    // 1. 个人
    self.personView = [[MineRealChooseTableViewCellView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width / 3., [MineRealChooseTableViewCellView calculationHeight])];
    [self.personView createSingleViewWithType:MineRealChooseTableViewCellTypePerson];
    [self.personView actionClickWithChooseTypeBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(MineRealChooseTableViewCellType type) = objc_getAssociatedObject(strongSelf, &actionClickWithChooseTypeBackBlockKey);
        if (block){
            block(MineRealChooseTableViewCellTypePerson);
        }
        
        // 其他移除
        self.teacherView.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
        self.ltdView.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
        [self.personView actionSelectBtnStatus:YES];
    }];
    [self addSubview:self.personView];
    
    // 2. 讲师
    self.teacherView = [[MineRealChooseTableViewCellView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.personView.frame), 0, self.personView.size_width, self.personView.size_height)];
    [self.teacherView createSingleViewWithType:MineRealChooseTableViewCellTypeTeacher];
    [self.teacherView actionClickWithChooseTypeBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(MineRealChooseTableViewCellType type) = objc_getAssociatedObject(strongSelf, &actionClickWithChooseTypeBackBlockKey);
        if (block){
            block(MineRealChooseTableViewCellTypeTeacher);
        }
        
        // 其他移除
        self.personView.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
        self.ltdView.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
        [self.teacherView actionSelectBtnStatus:YES];
    }];
    
    [self addSubview:self.teacherView];
    
    // 3. 群体
    self.ltdView = [[MineRealChooseTableViewCellView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.teacherView.frame), 0, self.personView.size_width, self.personView.size_height)];
    [self.ltdView createSingleViewWithType:MineRealChooseTableViewCellTypeLtd];
    [self.ltdView actionClickWithChooseTypeBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(MineRealChooseTableViewCellType type) = objc_getAssociatedObject(strongSelf, &actionClickWithChooseTypeBackBlockKey);
        if (block){
            block(MineRealChooseTableViewCellTypeLtd);
        }
        
        // 其他移除
        self.personView.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
        self.teacherView.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
        [self.ltdView actionSelectBtnStatus:YES];

    }];
    [self addSubview:self.ltdView];
}

-(void)actionClickWithChooseTypeBackBlock:(void (^)(MineRealChooseTableViewCellType))block{
    objc_setAssociatedObject(self, &actionClickWithChooseTypeBackBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    return 100;
}

-(void)walletChoosedType:(MineRealChooseTableViewCellType)type hasSelected:(BOOL)hasSelected{
    if (type == MineRealChooseTableViewCellTypeLtd){
        self.personView.userInteractionEnabled = NO;
        self.ltdView.userInteractionEnabled = YES;
        self.teacherView.userInteractionEnabled = NO;
        
        self.teacherView.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
        self.personView.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
        self.ltdView.chooseImgView.image = [UIImage imageNamed:@"creatroom_select"];
         
    } else if (type == MineRealChooseTableViewCellTypePerson){
        self.personView.userInteractionEnabled = YES;
        self.ltdView.userInteractionEnabled = NO;
        self.teacherView.userInteractionEnabled = NO;
        
        self.teacherView.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
        self.ltdView.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
        self.personView.chooseImgView.image = [UIImage imageNamed:@"creatroom_select"];
    } else if (type == MineRealChooseTableViewCellTypeTeacher){
        self.personView.userInteractionEnabled = NO;
        self.ltdView.userInteractionEnabled = NO;
        self.teacherView.userInteractionEnabled = YES;
        
        self.personView.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
        self.ltdView.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
        self.teacherView.chooseImgView.image = [UIImage imageNamed:@"creatroom_select"];
    }
}

@end

static char actionClickWithChooseTypeBlockKey;
@interface MineRealChooseTableViewCellView()
@property (nonatomic,strong)PDImageView *imgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *actionButton;

@end

@implementation MineRealChooseTableViewCellView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)actionSelectBtnStatus:(BOOL)status{
    [Tool clickZanWithView:self.chooseImgView block:^{
        if (status){
            self.chooseImgView.image = [UIImage imageNamed:@"creatroom_select"];
        } else {
            self.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
        }
    }];
}

-(void)createView{
    // 1. 创建头像
    self.imgView = [[PDImageView alloc]init];
    self.imgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.imgView];
    
    // 2. 创建标题
    self.titleLabel = [GWViewTool createLabelFont:@"13" textColor:@"4C4C4C"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    // 3. 创建选择框
    self.chooseImgView = [[PDImageView alloc]init];
    self.chooseImgView.image = [UIImage imageNamed:@"creatroom_unselect"];
    [self addSubview:self.chooseImgView];
    
    // 4. 创建button
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithChooseTypeBlockKey);
        if(block){
            block();
        }
    }];
    [self addSubview:self.actionButton];

    
}

#pragma mark - createSingleView;
-(void)createSingleViewWithType:(MineRealChooseTableViewCellType)type{
    
    if (type == MineRealChooseTableViewCellTypePerson){
        self.imgView.image = [UIImage imageNamed:@"icon_real_choose_person"];
        self.imgView.frame = CGRectMake((self.size_width - LCFloat(33)) / 2., 0, LCFloat(33), LCFloat(34));
        
        // title
        self.titleLabel.text = @"个人";
        self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.imgView.frame) + LCFloat(8), self.size_width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    } else if (type == MineRealChooseTableViewCellTypeTeacher){
        self.imgView.image = [UIImage imageNamed:@"icon_real_choose_teacher"];
        self.imgView.frame = CGRectMake((self.size_width - LCFloat(37)) / 2., 0, LCFloat(37), LCFloat(34));
        
        // title
        self.titleLabel.text = @"产业人";
        self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.imgView.frame) + LCFloat(8), self.size_width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    } else if (type == MineRealChooseTableViewCellTypeLtd){
        self.imgView.image = [UIImage imageNamed:@"icon_real_choose_ltd"];
        self.imgView.frame = CGRectMake((self.size_width - LCFloat(34)) / 2., 0, LCFloat(34), LCFloat(34));
        // title
        self.titleLabel.text = @"机构";
        self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.imgView.frame) + LCFloat(8), self.size_width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    }
    
    self.chooseImgView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(10), LCFloat(33), LCFloat(33));
    self.chooseImgView.center_x = self.size_width / 2.;
    
    self.actionButton.frame = self.bounds;
}

-(void)actionClickWithChooseTypeBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithChooseTypeBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationHeight{
    return 100;
}

@end
