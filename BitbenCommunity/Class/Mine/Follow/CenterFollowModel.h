//
//  CenterFollowModel.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/5.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "FetchModel.h"

@interface CenterFollowContentUserModel : FetchModel

@property (nonatomic,copy)NSString *head_img;
@property (nonatomic,assign)NSInteger level;
@property (nonatomic,copy)NSString *nickname;
@property (nonatomic,copy)NSString *self_introduction;
/** vip认证 */
@property (nonatomic ,assign) NSInteger cert_badge;
/** 成就组 */
@property (nonatomic ,strong) NSArray <NSString *>*achievement_badge_list;
@end


@protocol CenterFollowContentModel <NSObject>

@end

@interface CenterFollowContentModel : FetchModel
@property (nonatomic,copy)NSString *user_id;
@property (nonatomic,copy)NSString *attention_user_id;
@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,strong)CenterFollowContentUserModel *user;
@property (nonatomic,assign)BOOL isAttention;

@end




@interface CenterFollowModel : FetchModel

@property (nonatomic,assign)NSInteger first;
@property (nonatomic,assign)NSInteger last;
@property (nonatomic,assign)NSInteger number;
@property (nonatomic,assign)NSInteger numberOfElements;
@property (nonatomic,assign)NSInteger size;
@property (nonatomic,strong)NSArray<CenterFollowContentModel> *content;

@end



