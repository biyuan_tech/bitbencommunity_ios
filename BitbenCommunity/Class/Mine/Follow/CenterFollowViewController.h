//
//  CenterFollowViewController.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,CenterFansViewControllerType) {
    CenterFansViewControllerTypeFans,               /**< 我的粉丝*/
    CenterFansViewControllerTypeLink,               /**< 我的关注*/
};

@interface CenterFollowViewController : AbstractViewController

@property (nonatomic,assign)CenterFansViewControllerType transferType;

-(void)actionReoloadFollow:(void(^)())block;

@end
