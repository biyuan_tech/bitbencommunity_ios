//
//  CenterFollowSingleTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterFollowSingleTableViewCell.h"

@interface CenterFollowSingleTableViewCell()

@property (nonatomic,strong)PDImageView *avatarBgImgView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UIButton *linkButton;
/** 承载成就图标 */
@property (nonatomic ,strong) UIView *achievementView;
@end

static char actionClickWithLinkWithBlockKey;
static char actionClickHeaderImgWithBlockKey;
@implementation CenterFollowSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.avatarBgImgView = [[PDImageView alloc]init];
    self.avatarBgImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarBgImgView];
    
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    @weakify(self);
    [self.avatarImgView addTapGestureRecognizer:^{
        @strongify(self);
        void(^block)(void) = objc_getAssociatedObject(self, &actionClickHeaderImgWithBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.avatarImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"4C4C4C"];
    [self addSubview:self.titleLabel];
    
    // 承载成就view
    self.achievementView = [[UIView alloc] init];
    [self.contentView addSubview:self.achievementView];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"12" textColor:@"757474"];
    [self addSubview:self.dymicLabel];
    
    self.linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.linkButton];

}

-(void)setTransferContentModel:(CenterFollowContentModel *)transferContentModel{
    _transferContentModel = transferContentModel;

    self.avatarImgView.style = transferContentModel.user.cert_badge;
    self.avatarImgView.frame = CGRectMake(LCFloat(11), LCFloat(11), LCFloat(41), LCFloat(41));
    self.avatarImgView.layer.cornerRadius = self.avatarImgView.size_width / 2.;
    self.avatarImgView.clipsToBounds = YES;
    [self.avatarImgView uploadImageWithURL:transferContentModel.user.head_img placeholder:nil callback:NULL];

    
    // title
    self.titleLabel.text = transferContentModel.user.nickname.length?transferContentModel.user.nickname:transferContentModel.user_id;
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    CGFloat originX = CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11);
    CGFloat maxWidth = kCommonScreenWidth - originX - 68 - LCFloat(16) - LCFloat(54);
    CGFloat nickWidth = titleSize.width > maxWidth ? maxWidth : titleSize.width;
    self.titleLabel.frame = CGRectMake(originX, self.avatarImgView.orgin_y, nickWidth, titleSize.height);
    
    // 添加成就
    self.achievementView.frame = CGRectMake(CGRectGetMaxX(_titleLabel.frame) + 5,
                                            0, 58, 16);
    self.achievementView.center = CGPointMake(self.achievementView.center_x, self.titleLabel.center_y);
    
    for (UIView *subView in self.achievementView.subviews) {
        [subView removeFromSuperview];
    }
    
    NSArray *achievementData = transferContentModel.user.achievement_badge_list;
    for (NSString *bageId in achievementData) {
        NSInteger index = [achievementData indexOfObject:bageId];
        if (index > 2) {
            break;
        }
        ServerBadgeModel *model = [BYCommonTool getAchievementData:bageId];
        PDImageView *imageView = [[PDImageView alloc] init];
        [imageView uploadHDImageWithURL:model.picture callback:nil];
        imageView.frame = CGRectMake(21*index, 0, 16, 16);
        [self.achievementView addSubview:imageView];
    }
    
    // dymic
    self.dymicLabel.text = transferContentModel.user.self_introduction.length?transferContentModel.user.self_introduction:@"这个人很懒,什么都没有留下";
//    CGSize dymicSize = [Tool makeSizeWithLabel:self.dymicLabel];
    self.dymicLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(7), LCFloat(100), [NSString contentofHeightWithFont:self.dymicLabel.font]);
    
    self.linkButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(16) - LCFloat(54), 0, LCFloat(54), LCFloat(26));
    self.linkButton.center_y = [CenterFollowSingleTableViewCell calculationCellHeight] / 2.;
    
    if (transferContentModel.isAttention){
        [self.linkButton setBackgroundImage:[UIImage imageNamed:@"icon_center_link_nor"] forState:UIControlStateNormal];
    } else {
         [self.linkButton setBackgroundImage:[UIImage imageNamed:@"icon_center_link_hlt"] forState:UIControlStateNormal];
    }
    
    __weak typeof(self)weakSelf = self;
    [self.linkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(BOOL hasLink) = objc_getAssociatedObject(strongSelf, &actionClickWithLinkWithBlockKey);
        if (block){
            BOOL hasLink = transferContentModel.isAttention;
            block(!hasLink);
        }
    }];
}


-(void)actionClickWithLinkWithBlock:(void(^)(BOOL status))block{
    objc_setAssociatedObject(self, &actionClickWithLinkWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickHeaderImgWithBlock:(void (^)(void))block{
    objc_setAssociatedObject(self, &actionClickHeaderImgWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(77);
}

-(void)btnStatusSelected:(BOOL)isSelected{
    self.transferContentModel.isAttention = isSelected;
    if (isSelected){
        [self.linkButton setBackgroundImage:[UIImage imageNamed:@"icon_center_link_nor"] forState:UIControlStateNormal];
    } else {
        [self.linkButton setBackgroundImage:[UIImage imageNamed:@"icon_center_link_hlt"] forState:UIControlStateNormal];
    }
}

@end

