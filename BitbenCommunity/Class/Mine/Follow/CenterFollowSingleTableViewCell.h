//
//  CenterFollowSingleTableViewCell.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "CenterFollowViewController.h"
#import "CenterFollowModel.h"

@class  CenterFollowViewController;
@interface CenterFollowSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,assign)CenterFansViewControllerType transferType;
@property (nonatomic,strong)CenterFollowContentModel *transferContentModel;

-(void)actionClickWithLinkWithBlock:(void(^)(BOOL status))block;
-(void)actionClickHeaderImgWithBlock:(void(^)(void))block;
-(void)btnStatusSelected:(BOOL)isSelected;

@end
