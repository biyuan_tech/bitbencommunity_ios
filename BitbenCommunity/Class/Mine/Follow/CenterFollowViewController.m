//
//  CenterFollowViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/25.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterFollowViewController.h"
#import "CenterFollowSingleTableViewCell.h"
#import "NetworkAdapter+Center.h"
#import "BYPersonHomeController.h"

static char actionReoloadFollowKey;
@interface CenterFollowViewController ()<UITableViewDataSource,UITableViewDelegate,HTHorizontalSelectionListDelegate,HTHorizontalSelectionListDataSource>

@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)NSMutableArray *fansMutableArr;
@property (nonatomic,strong)UITableView *fansTableView;
@property (nonatomic,strong)NSMutableArray *linkMutableArr;
@property (nonatomic,strong)UITableView *linkTableView;


@end

@implementation CenterFollowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createMainScrollView];
    [self actinDirectManager];
    [self sendRequestToGetInfoWithType:CenterFansViewControllerTypeLink];
    [self sendRequestToGetInfoWithType:CenterFansViewControllerTypeFans];
}

-(void)actinDirectManager{
    if (self.transferType == CenterFansViewControllerTypeFans){
        [self.segmentList setSelectedButtonIndex:1 animated:NO];
        [self.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width, 0) animated:YES];
    } else {
        [self.segmentList setSelectedButtonIndex:0 animated:NO];
        [self.mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    // segment
    PDSelectionListTitleView *segment = [[PDSelectionListTitleView alloc]initWithFrame: CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(40))];
    segment.delegate = self;
    segment.dataSource = self;
    [segment setTitleColor:[UIColor colorWithCustomerName:@"353535"] forState:UIControlStateNormal];
    segment.selectionIndicatorColor = [UIColor colorWithCustomerName:@"EE4944"];
    segment.bottomTrimColor = [UIColor clearColor];
    segment.isNotScroll = YES;
    [segment setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    segment.backgroundColor = [UIColor clearColor];
    self.segmentList = segment;
    [self.view addSubview:self.segmentList];
    self.segmentList.size_width = 200;
    self.navigationItem.titleView = self.segmentList;
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return 2;
}

-(NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index{
    if (index == 0){
        return @"关注";
    } else if (index == 1){
        return @"粉丝";
    }
    return @"";
}

-(void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index{
    [self.mainScrollView setContentOffset:CGPointMake(index *kScreenBounds.size.width, 0) animated:YES];
}


-(void)arrayWithInit{
    self.fansMutableArr = [NSMutableArray array];
    self.linkMutableArr = [NSMutableArray array];
}

#pragma mark - createMainScrollView
-(void)createMainScrollView{
    __weak typeof(self)weakSelf = self;
    self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger index = scrollView.contentOffset.x / kScreenBounds.size.width;
        [strongSelf.segmentList setSelectedButtonIndex:index animated:YES];
    }];
//    if (IS_iPhoneX){
//        self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height);
//    } else {
//        self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height);
//    }
    self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - kNavigationHeight);
    self.mainScrollView.contentSize = CGSizeMake(2 * kScreenBounds.size.width, self.mainScrollView.size_height);
    [self.view addSubview:self.mainScrollView];
    [self createTableView];
}

-(void)createTableView{
    if (!self.linkTableView){
        self.linkTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.linkTableView.dataSource = self;
        self.linkTableView.delegate = self;
        [self.mainScrollView addSubview:self.linkTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.linkTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithType:CenterFansViewControllerTypeLink];
    }];
    [self.linkTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithType:CenterFansViewControllerTypeLink];
    }];
    
    
    if (!self.fansTableView){
        self.fansTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.fansTableView.orgin_x = kScreenBounds.size.width;
        self.fansTableView.dataSource = self;
        self.fansTableView.delegate = self;
        [self.mainScrollView addSubview:self.fansTableView];
    }
    [self.fansTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithType:CenterFansViewControllerTypeFans];
    }];
    [self.fansTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithType:CenterFansViewControllerTypeFans];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.fansTableView){
        return self.fansMutableArr.count;
    } else if (tableView == self.linkTableView){
        return self.linkMutableArr.count;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    CenterFollowSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[CenterFollowSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.backgroundColor = [UIColor clearColor];
    }
    cellWithRowOne.transferType = CenterFansViewControllerTypeFans;;
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (tableView == self.fansTableView){
        cellWithRowOne.transferContentModel = [self.fansMutableArr objectAtIndex:indexPath.row];
    } else if (tableView == self.linkTableView){
        cellWithRowOne.transferContentModel = [self.linkMutableArr objectAtIndex:indexPath.row];
    }
    
    __weak typeof(self)weakSelf = self;
    [cellWithRowOne actionClickWithLinkWithBlock:^(BOOL status) {
        if (!weakSelf){
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToLinkManagerModel:cellWithRowOne link:status];
    }];
    [cellWithRowOne actionClickHeaderImgWithBlock:^{
        if (!weakSelf){
            return;
        }
//        CenterFollowContentModel *model;
//        if (tableView == weakSelf.fansTableView){
//            model = [weakSelf.fansMutableArr objectAtIndex:indexPath.row];
//        }else if (tableView == weakSelf.linkTableView){
//            model = [weakSelf.linkMutableArr objectAtIndex:indexPath.row];
//        }
        BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
        if ([cellWithRowOne.transferContentModel.user_id isEqualToString:[AccountModel sharedAccountModel].loginServerModel.user._id]){
            personHomeController.user_id = cellWithRowOne.transferContentModel.attention_user_id;
        }
        else{
            personHomeController.user_id = cellWithRowOne.transferContentModel.user_id;
        }
        [weakSelf.navigationController pushViewController:personHomeController animated:YES];
    }];
    
    return cellWithRowOne;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SeparatorType separatorType = SeparatorTypeMiddle;
    if (indexPath.row == 0){
        separatorType = SeparatorTypeHead;
    } else if (indexPath.row == self.fansMutableArr.count){
        separatorType = SeparatorTypeBottom;
    } else {
        separatorType = SeparatorTypeMiddle;
    }
    [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"ranking"];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CenterFollowSingleTableViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    UserInfoViewController *userInfoViewController = [[UserInfoViewController alloc]init];
//    OtherSearchSingleModel *singleModel;
//
//    if (tableView == self.fansTableView){
//        singleModel = [self.fansMutableArr  objectAtIndex:indexPath.row];
//    } else if (tableView == self.linkTableView){
//        singleModel = [self.linkMutableArr  objectAtIndex:indexPath.row];
//    }
//    userInfoViewController.transferSearchSingleModel = singleModel;
//
//    __weak typeof(self)weakSelf = self;
//    [userInfoViewController actionLaheiBlock:^{             // 拉黑操作
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//
//        SearchSingleTableViewCell *cellWithRowOne;
//        if (tableView == strongSelf.fansTableView){
//            cellWithRowOne = (SearchSingleTableViewCell *)[strongSelf.fansTableView cellForRowAtIndexPath:indexPath];
//        } else if (tableView == self.linkTableView){
//            cellWithRowOne = (SearchSingleTableViewCell *)[strongSelf.linkTableView cellForRowAtIndexPath:indexPath];
//        }
//        [strongSelf sendRequestToLinkManagerModel:cellWithRowOne link:NO];
//    }];
//    [self.navigationController pushViewController:userInfoViewController animated:YES];
}


#pragma mark - 关注
-(void)sendRequestToLinkManagerModel:(CenterFollowSingleTableViewCell *)cell link:(BOOL)link{
    __weak typeof(self)weakSelf = self;

    NSString *userId = cell.transferContentModel.attention_user_id;
    NSString *otherId = cell.transferContentModel.user_id;
    NSString *mainId = [userId isEqualToString:[AccountModel sharedAccountModel].loginServerModel.user._id]?otherId:userId;
    [[NetworkAdapter sharedAdapter]centerSendRequestToLinkManagerWithUserId:mainId hasLink:link block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (link){
                [AccountModel sharedAccountModel].loginServerModel.infoCount.attention += 1;
            } else {
                [AccountModel sharedAccountModel].loginServerModel.infoCount.attention -= 1;
            }
            
            [cell btnStatusSelected:link];
        }
        
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionReoloadFollowKey);
        if (block){
            block();
        }
    }];
    
}

#pragma mark - sendRequestToGetInfo
-(void)sendRequestToGetInfoWithType:(CenterFansViewControllerType)type{
    __weak typeof(self)weakSelf = self;

    NSInteger pageNumber = 0;
    if (type == CenterFansViewControllerTypeFans){
        pageNumber = self.fansTableView.currentPage;
    } else if (type == CenterFansViewControllerTypeLink){
        pageNumber = self.linkTableView.currentPage;
    }
    
    if (type == CenterFansViewControllerTypeFans){
        [[NetworkAdapter sharedAdapter] sendRequestToGetMyFansManagerWithNumber:pageNumber block:^(BOOL isSuccessed, CenterFollowModel *singleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if(strongSelf.fansTableView.isXiaLa){
                [strongSelf.fansMutableArr removeAllObjects];
            }
            [strongSelf.fansMutableArr addObjectsFromArray:singleModel.content];
            [strongSelf.fansTableView reloadData];

            if (strongSelf.fansTableView.isXiaLa){
                [strongSelf.fansTableView stopPullToRefresh];
            } else {
                [strongSelf.fansTableView stopFinishScrollingRefresh];
            }

            
            if(strongSelf.fansMutableArr.count){
                [strongSelf.fansTableView dismissPrompt];
            } else {
                [strongSelf.fansTableView showPrompt:@"当前没有粉丝哦，快去直播吧" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }

        }];
    } else if (type == CenterFansViewControllerTypeLink){
        [[NetworkAdapter sharedAdapter] sendRequestToGetLinkListManagerWithNumber:pageNumber block:^(BOOL isSuccessed, CenterFollowModel *singleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if(strongSelf.linkTableView.isXiaLa){
                [strongSelf.linkMutableArr removeAllObjects];
            }
            [strongSelf.linkMutableArr addObjectsFromArray:singleModel.content];
            [strongSelf.linkTableView reloadData];

            if (strongSelf.linkTableView.isXiaLa){
                [strongSelf.linkTableView stopPullToRefresh];
            } else {
                [strongSelf.linkTableView stopFinishScrollingRefresh];
            }

            
            if(strongSelf.linkMutableArr.count){
                [strongSelf.linkTableView dismissPrompt];
            } else {
                [strongSelf.linkTableView showPrompt:@"当前没有关注哦，快去关注吧" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
            

        }];
    }
}


-(void)actionReoloadFollow:(void(^)())block{
    objc_setAssociatedObject(self, &actionReoloadFollowKey, block, OBJC_ASSOCIATION_COPY);
}
@end
