//
//  PDWelcomeViewController.m
//  PandaKing
//
//  Created by Cranz on 16/10/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDWelcomeViewController.h"

@interface PDWelcomeViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong) NSArray *lunchImages;

@property (nonatomic, strong) UIPageControl *pageControl;
@end

@implementation PDWelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self lunchScrollView];
    [self setPageView];
}

- (NSArray *)lunchImages {
    
    if ([AccountModel sharedAccountModel].isShenhe){
        if (IS_PhoneXAll){
            return @[@"guide_1_x_test",@"guide_2_x_test",@"guide_3_x_test"];
        } else {
            return @[@"guide_1_test",@"guide_2_test",@"guide_3_test"];
        }
    } else {
        if (IS_PhoneXAll){
            return @[@"guide_1_x",@"guide_2_x",@"guide_3_x",@"guide_4_x",@"guide_5_x"];
        } else {
            return @[@"guide_1",@"guide_2",@"guide_3",@"guide_4",@"guide_5"];
        }
    }
}

- (void)lunchScrollView {
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:kScreenBounds];
    scrollView.contentSize = CGSizeMake(kScreenBounds.size.width * self.lunchImages.count, kScreenBounds.size.height);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.bounces = NO;
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    for (int i = 0; i < self.lunchImages.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenBounds.size.width * i, 0, kScreenBounds.size.width, kScreenBounds.size.height)];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.image = [UIImage imageNamed:[self.lunchImages objectAtIndex:i]];
        [scrollView addSubview:imageView];
        if (i == (self.lunchImages.count - 1)) {
            imageView.userInteractionEnabled = YES;
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.backgroundColor = [UIColor clearColor];
            
            
            UIColor *btnColor = RGB(237, 74, 69, 1);
            [button setTitle:@"立即体验" forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            NSString *str = @"立即体验";
            CGSize strSize = [str sizeWithCalcFont:button.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:button.titleLabel.font])];
            CGFloat width = strSize.width + 2 * LCFloat(40);
            button.frame = CGRectMake((kScreenBounds.size.width - width) / 2., self.view.frame.size.height - LCFloat(100), width, 2 * LCFloat(11) + [NSString contentofHeightWithFont:button.titleLabel.font]);
            button.layer.borderWidth = 1.5;
            button.layer.borderColor = btnColor.CGColor;
            [button setTitleColor:btnColor forState:UIControlStateNormal];
            button.layer.cornerRadius = MIN(button.size_width, button.size_height) / 2.;
            __weak typeof(self)weakSelf = self;
            [button buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (strongSelf.startBlock){
                    strongSelf.startBlock();
                }
            }];
            
            [imageView addSubview:button];
        }
    }
}

- (void)setPageView {
    UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width * .4f, 30)];
    self.pageControl = pageControl;
    pageControl.center = CGPointMake(self.view.center.x, kScreenBounds.size.height - LCFloat(80));
    pageControl.numberOfPages = self.lunchImages.count;
    pageControl.currentPageIndicatorTintColor = RGB(111, 111, 111, 1);
    pageControl.pageIndicatorTintColor = RGB(224, 224, 224, 1);
    pageControl.userInteractionEnabled = NO;
    [pageControl setValue:[UIImage imageNamed:@"icon_guide_dot_nor"] forKeyPath:@"_pageImage"];

    [pageControl setValue:[UIImage imageNamed:@"icon_guide_dot_hlt"] forKeyPath:@"_currentPageImage"];
    
    [self.view addSubview:pageControl];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSInteger index = round(scrollView.contentOffset.x / kScreenBounds.size.width);
    self.pageControl.currentPage = index;
    if (index == self.lunchImages.count - 1) {
        self.pageControl.hidden = YES;
    } else {
        self.pageControl.hidden = NO;
    }
}

@end
