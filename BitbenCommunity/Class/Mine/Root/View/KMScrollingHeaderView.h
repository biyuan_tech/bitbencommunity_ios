//
//  KMScrollingHeaderView.h
//  TheMovieDB
//
//  Created by Kevin Mindeguia on 04/02/2014.
//  Copyright (c) 2014 iKode Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KMScrollingHeaderView;

@protocol KMScrollingHeaderViewDelegate <NSObject>

@required

- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView headerImageView:(PDImageView *)imageView;

@optional

- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView headerImageViewWasSelected:(UIImageView *)imageView;
- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView scrollViewWithScrollOffset:(CGFloat)scrollOffset;
@end

@interface KMScrollingHeaderView : UIView
-(instancetype)initWithFrame:(CGRect)frame imgHeight:(CGFloat)headerImgHeight scaling:(CGFloat)scaling;
@property (nonatomic, strong) PDImageView* imageView;
@property (nonatomic) CGFloat headerImageViewHeight;
@property (nonatomic) CGFloat headerImageViewScalingFactor;
@property (nonatomic) CGFloat navbarViewFadingOffset;
@property (nonatomic, strong) UITableView* tableView;
@property (nonatomic, strong) UIView* navbarView;
@property (nonatomic) UIViewContentMode headerImageViewContentMode;
@property (nonatomic, weak) id<KMScrollingHeaderViewDelegate> delegate;

- (void)reloadScrollingHeader;

@end
