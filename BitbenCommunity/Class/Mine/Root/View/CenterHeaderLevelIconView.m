//
//  CenterHeaderLevelIconView.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/6.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterHeaderLevelIconView.h"

@interface CenterHeaderLevelIconView()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)PDImageView *levelIcon;
@property (nonatomic,strong)UILabel *levelLabel;

@end

@implementation CenterHeaderLevelIconView

-(instancetype)initWithFrame:(CGRect)frame withLevel:(NSInteger)level{
    self = [super initWithFrame:frame];
    if (self){
        [self createViewWithLevel:level];
    }
    return self;
}

#pragma mark - createView
-(void)createViewWithLevel:(NSInteger)level{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = RGB(255, 197, 55, 1);
    self.bgView.frame = self.bounds;
    self.bgView.clipsToBounds = YES;
    self.bgView.layer.cornerRadius = MIN(self.size_height, self.size_width) / 2.;
    [self addSubview:self.bgView];
     
    // 2. icon
    self.levelIcon = [[PDImageView alloc]init];
    self.levelIcon.backgroundColor = [UIColor clearColor];
    self.levelIcon.image = [UIImage imageNamed:@"icon_center_level"];
    self.levelIcon.frame = CGRectMake(0, 0, LCFloat(13), LCFloat(10));
    [self.bgView addSubview:self.levelIcon];
    
    self.levelLabel = [GWViewTool createLabelFont:@"11" textColor:@"白"];
    self.levelLabel.text = [NSString stringWithFormat:@"Lv%li",(long)level];
    self.levelLabel.font = [self.levelLabel.font boldFont];
    [self.bgView addSubview:self.levelLabel];
    
    CGSize levelSize = [Tool makeSizeWithLabel:self.levelLabel];
    
    self.levelIcon.frame = CGRectMake(LCFloat(11), (self.size_height - LCFloat(10)) / 2., LCFloat(13), LCFloat(10));
    self.levelLabel.frame = CGRectMake(CGRectGetMaxX(self.levelIcon.frame) + LCFloat(3), 0, levelSize.width, levelSize.height);
    self.levelLabel.center_y = self.levelIcon.center_y;
}

+(CGSize)calculationSize {
    
    NSString *level = @"Lv4";
    CGSize levelSize = [level sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"11"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"11"]])];
    CGFloat width = LCFloat(11) + LCFloat(3) + levelSize.width + 2 * LCFloat(11);
    CGFloat height = [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"11"]] + 2 * LCFloat(7);
    return CGSizeMake(width, height);
}




@end
