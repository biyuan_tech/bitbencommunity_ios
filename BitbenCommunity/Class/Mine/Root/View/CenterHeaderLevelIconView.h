//
//  CenterHeaderLevelIconView.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/9/6.
//  Copyright © 2018年 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CenterHeaderLevelIconView : UIView

-(instancetype)initWithFrame:(CGRect)frame withLevel:(NSInteger)level;

+(CGSize)calculationSize ;

@end
