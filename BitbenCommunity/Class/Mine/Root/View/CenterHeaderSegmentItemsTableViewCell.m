//
//  CenterHeaderSegmentItemsTableViewCell.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/29.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterHeaderSegmentItemsTableViewCell.h"

static char segmentListItemClickWithBlockKey;
@interface CenterHeaderSegmentItemsTableViewCell()

@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UIView *bgView;

@property (nonatomic,strong)CenterHeaderSegmentSingleItemView *singleView1;
@property (nonatomic,strong)CenterHeaderSegmentSingleItemView *singleView2;
@property (nonatomic,strong)CenterHeaderSegmentSingleItemView *singleView3;
@property (nonatomic,strong)CenterHeaderSegmentSingleItemView *singleView4;
@property (nonatomic,strong)CenterHeaderSegmentSingleItemView *singleView5;
@end

@implementation CenterHeaderSegmentItemsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = self.bgImgView;
    self.bgImgView.image = [Tool stretchImageWithName:@"bg_center_single"];
    self.userInteractionEnabled = YES;
    
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.userInteractionEnabled = YES;
    [self addSubview:self.bgView];
    
    [self actionInfo];
    
}

-(void)actionInfo{
    if (self.bgView.subviews.count){
        [self.bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    NSInteger rootCount = 0;
//    if ([AccountModel sharedAccountModel].isShenhe){
//        rootCount = 4;
//    } else {
        rootCount = 5;
//    }
    
    CGFloat margin = LCFloat(21);
    CGFloat width = (kScreenBounds.size.width - 2 * margin) / rootCount;
    
    for (int i = 0 ; i < rootCount;i++){
        CGFloat origin_x = margin + width * i;
        CenterHeaderSegmentSingleItemView *singleView = [[CenterHeaderSegmentSingleItemView alloc]initWithFrame:CGRectMake(origin_x, 0, width, [CenterHeaderSegmentSingleItemView calculationHeight])];
        singleView.userInteractionEnabled = YES;
        [self.bgView addSubview:singleView];
        if (i == 0){
            self.singleView1 = singleView;
        } else if (i == 1){
            self.singleView2 = singleView;
        } else if (i == 2){
            self.singleView3 = singleView;
        } else if (i == 3){
            self.singleView4 = singleView;
        } else if (i == 4){
            self.singleView5 = singleView;
        }
        
//        if ([AccountModel sharedAccountModel].isShenhe){
//            self.singleView1.hidden = YES;
//            singleView.size_width = (kScreenBounds.size.width - 2 * margin) / 3;
//            self.singleView2.orgin_x = margin + singleView.size_width * 0;
//            self.singleView3.orgin_x = margin + singleView.size_width * 1;
//            self.singleView4.orgin_x = margin + singleView.size_width * 2;
//            self.singleView5.orgin_x = margin + singleView.size_width * 3;
//        }
        
        __weak typeof(self)weakSelf = self;
        [singleView actionClickWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(NSInteger index) = objc_getAssociatedObject(strongSelf, &segmentListItemClickWithBlockKey);
            if (block){
                block(i);
            }
        }];
    }
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [CenterHeaderSegmentSingleItemView calculationHeight]);
    self.bgView.center_y = [CenterHeaderSegmentItemsTableViewCell calculationCellHeight] / 2.;
}

-(void)segmentListItemClickWithBlock:(void(^)(NSInteger index))block{
    objc_setAssociatedObject(self, &segmentListItemClickWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(116);
}

-(void)reloadItems{
    self.singleView1.fixedLabel.text = @"能量值";
    self.singleView2.fixedLabel.text = @"关注";
    self.singleView3.fixedLabel.text = @"粉丝";
    self.singleView4.fixedLabel.text = @"文章";
    self.singleView5.fixedLabel.text = @"直播";
    if ([AccountModel sharedAccountModel].loginServerModel.infoCount){
        self.singleView1.dymicLabel.text = [NSString stringWithFormat:@"%li", (long)[AccountModel sharedAccountModel].loginServerModel.user.energy];
        self.singleView2.dymicLabel.text = [NSString stringWithFormat:@"%li", (long)[AccountModel sharedAccountModel].loginServerModel.infoCount.attention];
        self.singleView3.dymicLabel.text = [NSString stringWithFormat:@"%li", (long)[AccountModel sharedAccountModel].loginServerModel.infoCount.fans];
        self.singleView4.dymicLabel.text = [NSString stringWithFormat:@"%li", (long)[AccountModel sharedAccountModel].loginServerModel.infoCount.article];
        self.singleView5.dymicLabel.text = [NSString stringWithFormat:@"%li", (long)[AccountModel sharedAccountModel].loginServerModel.infoCount.live];
    } else {
        self.singleView1.dymicLabel.text = @"0";
        self.singleView2.dymicLabel.text = @"0";
        self.singleView3.dymicLabel.text = @"0";
        self.singleView4.dymicLabel.text = @"0";
        self.singleView5.dymicLabel.text = @"0";
    }
}

@end



static char actionClickWithBlockKey;
@interface CenterHeaderSegmentSingleItemView()
@property (nonatomic,strong)UIButton *actionButton;

@end

@implementation CenterHeaderSegmentSingleItemView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.dymicLabel = [GWViewTool createLabelFont:@"19" textColor:@"3F3F3F"];
    [self addSubview:self.dymicLabel];
    self.dymicLabel.font = [self.dymicLabel.font boldFont];
    self.dymicLabel.textAlignment = NSTextAlignmentCenter;
    
    self.fixedLabel = [GWViewTool createLabelFont:@"15" textColor:@"8C8C8C"];
    [self addSubview:self.fixedLabel];
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    
    self.dymicLabel.frame = CGRectMake(0, 0, self.size_width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
    self.fixedLabel.frame = CGRectMake(0, CGRectGetMaxY(self.dymicLabel.frame) + LCFloat(11), self.size_width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
    self.dymicLabel.text = @"123";
    self.fixedLabel.text = @"4123";
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.actionButton];
    self.actionButton.frame = self.bounds;
    self.actionButton.userInteractionEnabled = YES;
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithBlockKey);
        if (block){
            block();
        }
    }];
}

-(void)actionClickWithBlock:(void (^)())block{
    objc_setAssociatedObject(self, &actionClickWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationHeight{
    CGFloat height = 0;
    height += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"19"]];
    height += LCFloat(11);
    height += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"15"]];
    return height;
}

@end
