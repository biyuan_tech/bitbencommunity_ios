//
//  CenterRootViewController.h
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "AbstractViewController.h"

@interface CenterRootViewController : AbstractViewController

+(instancetype)sharedController;

-(void)logoutUIManager;

-(void)directToWallet;

// 退出登录以后的UI展示
-(void)logoutStatusManager;
@end
