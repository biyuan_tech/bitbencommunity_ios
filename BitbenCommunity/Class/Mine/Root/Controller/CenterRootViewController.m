//
//  CenterRootViewController.m
//  BiYuan
//
//  Created by 裴烨烽 on 2018/8/22.
//  Copyright © 2018年 币本. All rights reserved.
//

#import "CenterRootViewController.h"
#import "CenterMessageRootViewController.h"         // message
#import "NetworkAdapter+Login.h"
#import "NetworkAdapter+Center.h"
#import "KMScrollingHeaderView.h"
#import "CenterHeaderViewInfo.h"                    // 头部薪资
#import "ArticleRootViewController.h"               // 文章

#import "CenterNormalTableViewCell.h"
#import "CenterInformationSettingViewController.h"
#import "CenterInvitationViewController.h"
#import "CenterHeaderSegmentItemsTableViewCell.h"
#import "CenterFollowViewController.h"
#import "CenterUserInformationViewController.h"
#import "CenterBBTViewController.h"
#import "CenterAboutRootViewController.h"
#import "BYLiveRoomHomeController.h"
#import "BYCreatLiveRoomController.h"
#import "CenterSignView.h"
#import "MineRealNameViewController.h"

#import "TaskRootViewController.h"

#import "ReportViewController.h"
#import "PowerViewController.h"
#import "ArticleMineRootViewController.h"
#import "WalletRootViewController.h"
#import "WalletLaungchViewController.h"
#import "NetworkAdapter+Wallet.h"
#import "ProductVipViewController.h"
#import "FaceAlertViewController.h"
#import "VersionInvitationViewController.h"
#import "BYMyActiviryController.h"
#import "BYCollectionController.h"

@interface CenterRootViewController ()<UITableViewDelegate,UITableViewDataSource,KMScrollingHeaderViewDelegate>{
    GWButtonTableViewCell *btnCell;
}
@property (nonatomic,strong)NSArray *centerArr;
@property (nonatomic,strong)UIView *navBarView;
@property (nonatomic,strong)KMScrollingHeaderView *scrollingHeaderView;
@property (nonatomic,strong)CenterHeaderViewInfo *headerViewInfo;

@property (nonatomic,strong)UIView *rightNavView;
@property (nonatomic,strong)LOTAnimationView *settingAnimation;
@property (nonatomic,strong)UIButton *rightSettingButton;
@property (nonatomic,strong)UIButton *leftSettingButton;
@property (nonatomic,strong)UILabel *navLabel;

@end

@implementation CenterRootViewController

+(instancetype)sharedController{

    //
    static CenterRootViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[CenterRootViewController alloc] init];
    });
    return _sharedAccountModel;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){
        [self sendRequestToGetInfo];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self setupDetailsPageView];
    __weak typeof(self)weakSelf = self;
    [self authorizeWithCompletionHandler:^(BOOL successed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([[AccountModel sharedAccountModel] hasLoggedIn]){
            [strongSelf sendRequestToGetInfo];
        } else {
            strongSelf.headerViewInfo.transferAccountModel = nil;
            strongSelf->btnCell.transferTitle = @"登录";
            [strongSelf.scrollingHeaderView.tableView reloadData];
        }
    }];
}


#pragma mark - createNavBar
-(void)createNavBar{
    // 1. 创建本地的View
    self.rightNavView = [[UIView alloc]init];
    self.rightNavView.backgroundColor = [UIColor clearColor];
    self.rightNavView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - 44, [BYTabbarViewController sharedController].statusHeight, 44, 44);
    [self.view addSubview:self.rightNavView];
    
    self.navBarView = [[UIView alloc]init];
    self.navBarView.backgroundColor = [UIColor redColor];
    self.navBarView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [BYTabbarViewController sharedController].navBarHeight);
    [self.view addSubview:self.navBarView];
    
    UIButton *settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    settingButton.backgroundColor = [UIColor clearColor];
    [settingButton setImage:[UIImage imageNamed:@"icon_center_setting"] forState:UIControlStateNormal];
    settingButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(7) - LCFloat(44), 20, LCFloat(44), LCFloat(44));
    [self.navBarView addSubview:settingButton];
    __weak typeof(self)weakSelf = self;
    [settingButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfo];
    }];

    self.scrollingHeaderView.navbarView = self.navBarView;
}

#pragma mark - CreateBottomScrollingView
- (void)setupDetailsPageView {
    self.scrollingHeaderView = [[KMScrollingHeaderView alloc]initWithFrame:self.view.bounds imgHeight:LCFloat(230) scaling:LCFloat(230)];
    self.scrollingHeaderView.backgroundColor = [UIColor whiteColor];
    self.scrollingHeaderView.tableView.dataSource = self;
    self.scrollingHeaderView.tableView.delegate = self;
    self.scrollingHeaderView.tableView.backgroundColor = [UIColor whiteColor];
    self.scrollingHeaderView.delegate = self;
    
    self.scrollingHeaderView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.scrollingHeaderView.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.scrollingHeaderView.tableView.showsVerticalScrollIndicator = YES;
    self.scrollingHeaderView.tableView.backgroundColor = [UIColor whiteColor];
    
    self.scrollingHeaderView.tableView.separatorColor = [UIColor whiteColor];
    self.scrollingHeaderView.headerImageViewContentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:self.scrollingHeaderView];
    
    self.scrollingHeaderView.navbarViewFadingOffset = LCFloat(230);
    
    [self.scrollingHeaderView reloadScrollingHeader];
    
    [self createNavBarInfo];
}

-(void)createNavBarInfo{
    CGFloat origin_y = (self.scrollingHeaderView.headerImageViewHeight - [CenterHeaderViewInfo calculationHeight]) / 2. - 0;
    self.headerViewInfo = [[CenterHeaderViewInfo alloc]initWithFrame:CGRectMake(LCFloat(7), origin_y, kScreenBounds.size.width - 2 * LCFloat(7), [CenterHeaderViewInfo calculationHeight])];
    __weak typeof(self)weakSelf = self;
    [self.headerViewInfo actionClickWithHeaderAvatarBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf authorizeWithCompletionHandler:^(BOOL successed) {
            CenterInformationSettingViewController *informationViewController = [[CenterInformationSettingViewController alloc]init];
            [informationViewController hidesTabBarWhenPushed];
            [strongSelf.navigationController pushViewController:informationViewController animated:YES];
        }];
    }];
    self.headerViewInfo.transferAccountModel = [AccountModel sharedAccountModel];
    
    [self.scrollingHeaderView addSubview:self.headerViewInfo];
    
    // 设置按钮
    self.rightSettingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightSettingButton.backgroundColor = [UIColor clearColor];
    [self.rightSettingButton setImage:[UIImage imageNamed:@"icon_center_message"] forState:UIControlStateNormal];
    CGFloat margin = (self.headerViewInfo.orgin_y - LCFloat(44)) / 2.;
    
    self.rightSettingButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(7) - LCFloat(44), margin, LCFloat(44), LCFloat(44));
    [self.scrollingHeaderView addSubview:self.rightSettingButton];
    [self.rightSettingButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf authorizeWithCompletionHandler:^(BOOL successed) {

            CenterMessageRootViewController *messageViewController = [[CenterMessageRootViewController alloc]init];
            [messageViewController hidesTabBarWhenPushed];
            [strongSelf.navigationController pushViewController:messageViewController animated:YES];
        }];
    }];
    
    self.leftSettingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftSettingButton.backgroundColor = [UIColor clearColor];
    [self.leftSettingButton setImage:[UIImage imageNamed:@"icon_center_setting"] forState:UIControlStateNormal];
    self.leftSettingButton.frame = CGRectMake(LCFloat(7), margin, LCFloat(44), LCFloat(44));
    [self.scrollingHeaderView addSubview:self.leftSettingButton];
    [self.leftSettingButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf authorizeWithCompletionHandler:^(BOOL successed) {
            CenterInformationSettingViewController *informationViewController = [[CenterInformationSettingViewController alloc]init];
            [informationViewController hidesTabBarWhenPushed];
            [strongSelf.navigationController pushViewController:informationViewController animated:YES];
        }];
    }];
    
    self.navLabel = [GWViewTool createLabelFont:@"15" textColor:@"白"];
    self.navLabel.text = @"个人中心";
    CGSize navSize = [Tool makeSizeWithLabel:self.navLabel];
    self.navLabel.frame = CGRectMake((kScreenBounds.size.width - navSize.width) / 2., 0, navSize.width, navSize.height);
    self.navLabel.center_y = self.leftSettingButton.center_y;
    [self.scrollingHeaderView addSubview:self.navLabel];
}



#pragma mark - KMScrollingHeaderViewDelegate
- (void)detailsPage:(KMScrollingHeaderView *)detailsPageView headerImageView:(PDImageView *)imageView{
    imageView.image = [UIImage imageNamed:@"bg_center_alpha"];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"个人中心";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"设置" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfo];
    }];
    
    [self leftBarButtonWithTitle:@"消息" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        CenterMessageRootViewController *centerMessageViewController = [[CenterMessageRootViewController alloc]init];
        [centerMessageViewController hidesTabBarWhenPushed];
        [strongSelf.navigationController pushViewController:centerMessageViewController animated:YES];
    }];
}

-(void)arrayWithInit{
    if ([AccountModel sharedAccountModel].isShenhe){
        self.centerArr = @[@[@"信息"],@[@"每日签到",@"邀请好友",@"实名认证",@"关于币本"],@[@"退出登录"]];
    } else {
//        self.centerArr = @[@[@"信息"],@[@"我的资产",@"我的钱包",@"我的会议",@"我的收藏",@"会员中心",@"每日签到",@"邀请好友",@"实名认证",@"关于币本"],@[@"退出登录"]];
        self.centerArr = @[@[@"信息"],@[@"我的资产",@"我的钱包",@"我的会议",@"我的收藏",@"邀请好友",@"申请认证",@"关于币本"],@[@"退出登录"]];

    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.centerArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.centerArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"信息" sourceArr:self.centerArr]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        CenterHeaderSegmentItemsTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[CenterHeaderSegmentItemsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowZero segmentListItemClickWithBlock:^(NSInteger index) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf authorizeWithCompletionHandler:^(BOOL successed) {
                if (index == 0){ // BBT
                    PowerViewController *powerViewController = [[PowerViewController alloc]init];
                    [powerViewController hidesTabBarWhenPushed];
                    [strongSelf.navigationController pushViewController:powerViewController animated:YES];
                } else if (index == 1){         // 关注
                    CenterFollowViewController *followViewController = [[CenterFollowViewController alloc]init];
                    followViewController.transferType = CenterFansViewControllerTypeLink;
                    [followViewController hidesTabBarWhenPushed];
                    __weak typeof(self)weakSelf = self;
                    [followViewController actionReoloadFollow:^{
                        if (!weakSelf){
                            return ;
                        }
                        __strong typeof(weakSelf)strongSelf = weakSelf;
                        [strongSelf.scrollingHeaderView.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }];
                    [strongSelf.navigationController pushViewController:followViewController animated:YES];
                } else if (index == 2){         // 粉丝
                    CenterFollowViewController *followViewController = [[CenterFollowViewController alloc]init];
                    followViewController.transferType = CenterFansViewControllerTypeFans;
                    [followViewController hidesTabBarWhenPushed];
                    __weak typeof(self)weakSelf = self;
                    [followViewController actionReoloadFollow:^{
                        if (!weakSelf){
                            return ;
                        }
                        __strong typeof(weakSelf)strongSelf = weakSelf;
                        [strongSelf.scrollingHeaderView.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }];
                    [strongSelf.navigationController pushViewController:followViewController animated:YES];
                } else if (index == 3){         // 文章
                    ArticleMineRootViewController *articleRootViewController = [[ArticleMineRootViewController alloc]init];
                    [articleRootViewController hidesTabBarWhenPushed];
                    [strongSelf.navigationController pushViewController:articleRootViewController animated:YES];
                } else if (index == 4){         // 直播
                    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kiSHaveLiveRoomKey] isEqual:@YES]) {
                        BYLiveRoomHomeController *livRoomeHomeController = [[BYLiveRoomHomeController alloc] init];
                        [livRoomeHomeController hidesTabBarWhenPushed];
                        [strongSelf.navigationController pushViewController:livRoomeHomeController animated:YES];
                    } else{
                        BYCreatLiveRoomController *creatLiveRoomController = [[BYCreatLiveRoomController alloc] init];
                        [creatLiveRoomController hidesTabBarWhenPushed];
                        BYNavigationController *navigationController = [[BYNavigationController alloc] initWithRootViewController:creatLiveRoomController];
//                        [strongSelf.navigationController pushViewController:creatLiveRoomController animated:YES];
                        [strongSelf.navigationController presentViewController:navigationController animated:YES completion:nil];
                    }
                }
            }];
        }];
        
        [cellWithRowZero reloadItems];
        return cellWithRowZero;
        
    }  else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的钱包" sourceArr:self.centerArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"每日签到" sourceArr:self.centerArr] ){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        CenterNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[CenterNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的资产" sourceArr:self.centerArr]){
            cellWithRowOne.transferTitle = @"我的资产";
            cellWithRowOne.transferDymic = [NSString stringWithFormat:@"%li BBT",(long) [AccountModel sharedAccountModel].loginServerModel.finance.coin];
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_root_bp"];
            cellWithRowOne.transferBgImgName = @"bg_center_top";
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的钱包" sourceArr:self.centerArr]){
            cellWithRowOne.transferTitle = @"我的钱包";
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_root_wallet"];
            cellWithRowOne.transferBgImgName = @"bg_center_normal";
            cellWithRowOne.transferDymic = @"";
        }
        else if (indexPath.row == [self cellIndexPathRowWithcellData:@"每日签到" sourceArr:self.centerArr]){
            cellWithRowOne.transferTitle = @"每日签到";
            
            if ([AccountModel sharedAccountModel].isShenhe){
                cellWithRowOne.transferDymic = @"签到领取积分";
                cellWithRowOne.transferBgImgName = @"bg_center_top";
            } else {
                cellWithRowOne.transferDymic = @"签到领取BP";
                cellWithRowOne.transferBgImgName = @"bg_center_normal";
            }
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_sign"];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"任务中心" sourceArr:self.centerArr]){
            cellWithRowOne.transferTitle = @"任务中心";
            cellWithRowOne.transferDymic = @"做任务领奖励";
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_root_renwu"];
            cellWithRowOne.transferBgImgName = @"bg_center_normal";
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"邀请好友" sourceArr:self.centerArr]){
            cellWithRowOne.transferTitle = @"邀请好友";
            if ([AccountModel sharedAccountModel].isShenhe){
                cellWithRowOne.transferDymic = @"邀请送积分";
            } else {
                cellWithRowOne.transferDymic = @"邀请送BBT";
            }
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_inv"];
            cellWithRowOne.transferBgImgName = @"bg_center_normal";
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"申请认证" sourceArr:self.centerArr]){
            cellWithRowOne.transferTitle = @"申请认证";
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_root_real"];
            cellWithRowOne.transferBgImgName = @"bg_center_normal";
            cellWithRowOne.transferDymic = @"";
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"关于币本" sourceArr:self.centerArr]){
            cellWithRowOne.transferTitle = @"关于币本";
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_about"];
            cellWithRowOne.transferBgImgName = @"bg_center_bottom";
            cellWithRowOne.transferDymic = @"";
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"会员中心" sourceArr:self.centerArr]){
            cellWithRowOne.transferTitle = @"会员中心";
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_root_vip"];
            cellWithRowOne.transferBgImgName = @"bg_center_normal";
            cellWithRowOne.transferDymic = @"";
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"快捷认证" sourceArr:self.centerArr]){
            cellWithRowOne.transferTitle = @"快捷认证";
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_root_face"];
            cellWithRowOne.transferBgImgName = @"bg_center_normal";
            cellWithRowOne.transferDymic = @"";
        }else if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的会议" sourceArr:self.centerArr]){
            cellWithRowOne.transferTitle = @"我的会议";
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_root_activity"];
            cellWithRowOne.transferBgImgName = @"bg_center_normal";
            cellWithRowOne.transferDymic = @"";
        }else if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的收藏" sourceArr:self.centerArr]){
            cellWithRowOne.transferTitle = @"我的收藏";
            cellWithRowOne.transferIconImg = [UIImage imageNamed:@"icon_center_root_collection"];
            cellWithRowOne.transferBgImgName = @"bg_center_normal";
            cellWithRowOne.transferDymic = @"";
        }
        cellWithRowOne.hasArrow = YES;
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        if ([AccountModel sharedAccountModel].hasLoggedIn){
            cellWithRowTwo.transferTitle = @"退出登录";
        } else {
            cellWithRowTwo.transferTitle = @"登录";
        }
        
        btnCell = cellWithRowTwo;
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf logoutManager];
            return;
        }];
        return cellWithRowTwo;
    }
}


#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1){
        if (indexPath.row != [self cellIndexPathRowWithcellData:@"关于币本" sourceArr:self.centerArr]){
            __weak typeof(self)weakSelf = self;
            [self authorizeWithCompletionHandler:^(BOOL successed) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (successed){
                    if (indexPath.row == [strongSelf cellIndexPathRowWithcellData:@"我的资产" sourceArr:strongSelf.centerArr]){
                        CenterBBTViewController *bbtViewController = [[CenterBBTViewController alloc]init];
                        bbtViewController.transferControllerType = CenterBBTViewControllerTypeBP;
                        [bbtViewController hidesTabBarWhenPushed];
                        [strongSelf.navigationController pushViewController:bbtViewController animated:YES];
                    } else if (indexPath.row == [strongSelf cellIndexPathRowWithcellData:@"每日签到" sourceArr:strongSelf.centerArr]){
                        [strongSelf sendRequestToSign];
                    } else if (indexPath.row == [strongSelf cellIndexPathRowWithcellData:@"邀请好友" sourceArr:strongSelf.centerArr]){
                        VersionInvitationViewController *invitationViewController = [[VersionInvitationViewController alloc]init];
                        [invitationViewController hidesTabBarWhenPushed];
                        [strongSelf.navigationController pushViewController:invitationViewController animated:YES];
                    } else if (indexPath.row == [strongSelf cellIndexPathRowWithcellData:@"申请认证" sourceArr:strongSelf.centerArr]){
                        MineRealNameViewController *realVC = [[MineRealNameViewController alloc]init];
                        [strongSelf.navigationController pushViewController:realVC animated:YES];
                    } else if (indexPath.row == [strongSelf cellIndexPathRowWithcellData:@"任务中心" sourceArr:strongSelf.centerArr]){
                        TaskRootViewController *taskVC = [[TaskRootViewController alloc]init];
                        [taskVC hidesTabBarWhenPushed];
                        [strongSelf.navigationController pushViewController:taskVC animated:YES];
                    } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"快捷认证" sourceArr:self.centerArr]){
                        FaceAlertViewController *faceVC = [[FaceAlertViewController alloc]init];
                        [strongSelf.navigationController pushViewController:faceVC animated:YES];
                    } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的钱包" sourceArr:self.centerArr]){
                        [strongSelf getHasRealAndWallet];
                        
                        //                    UIAlertView *alertView = [UIAlertView alertViewWithTitle:@"暂未开放" message:@"敬请期待" buttonTitles:@[@"确定"] callBlock:NULL];
                        //                    [alertView show];
                        //                    return;
                        //                    if ([AccountModel sharedAccountModel].loginServerModel.account.block_address.length){
                        //                        WalletRootViewController *walletVC = [[WalletRootViewController alloc]init];
                        //                        [strongSelf.navigationController pushViewController:walletVC animated:YES];
                        //                    } else {
                        //                        WalletLaungchViewController *walletLaungchVC = [[WalletLaungchViewController alloc]init];
                        //                        [strongSelf.navigationController pushViewController:walletLaungchVC animated:YES];
                        //                    }
                    } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"会员中心" sourceArr:self.centerArr]){
                        //                    [[UIAlertView alertViewWithTitle:@"暂未开放" message:@"敬请期待" buttonTitles:@[@"确定"] callBlock:NULL]show];
                        [strongSelf memberVipProductMangaer];
                    } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"扫描" sourceArr:self.centerArr]){
//                        FaceRootViewController *vc = [[FaceRootViewController alloc]init];
//                        [self.navigationController pushViewController:vc animated:YES];
                        
                        
                    }else if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的会议" sourceArr:self.centerArr]){
                        BYMyActiviryController *myActivityController = [[BYMyActiviryController alloc] init];
                        [self.navigationController pushViewController:myActivityController animated:YES];

                    }else if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的收藏" sourceArr:self.centerArr]){
                        BYCollectionController *collectoinController = [[BYCollectionController alloc] init];
                        [self.navigationController pushViewController:collectoinController animated:YES];

//                        BYMyActiviryController *myActivityController = [[BYMyActiviryController alloc] init];
//                        [self.navigationController pushViewController:myActivityController animated:YES];
                        
                    }
                }
                
            }];
        } else {
            CenterAboutRootViewController *aboutViewController = [[CenterAboutRootViewController alloc]init];
            [aboutViewController hidesTabBarWhenPushed];
            [self.navigationController pushViewController:aboutViewController animated:YES];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return LCFloat(0);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *sectionOfArr = [self.centerArr objectAtIndex:indexPath.section];
    
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"信息" sourceArr:self.centerArr]){
        return [CenterHeaderSegmentItemsTableViewCell calculationCellHeight];
    }  else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的钱包" sourceArr:self.centerArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"每日签到" sourceArr:self.centerArr]){
        if (indexPath.row == 0){
            return [CenterNormalTableViewCell calculationCellHeightWithLocation:cellLocationTop];
        } else if (indexPath.row == sectionOfArr.count - 1){
            return [CenterNormalTableViewCell calculationCellHeightWithLocation:cellLocationBottom];
        } else {
            return [CenterNormalTableViewCell calculationCellHeightWithLocation:cellLocationNormal];
        }
    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *sectionOfArr = [self.centerArr objectAtIndex:indexPath.section];

    if (self.scrollingHeaderView.tableView) {
        if (!(indexPath.section == [self cellIndexPathSectionWithcellData:@"信息" sourceArr:self.centerArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"退出登录" sourceArr:self.centerArr])){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType  = SeparatorTypeHead;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
            } else if (indexPath.row == sectionOfArr.count - 1) {
                separatorType  = SeparatorTypeBottom;
            } else {
                separatorType  = SeparatorTypeMiddle;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
            }
        }
    }
}


#pragma mark - Interface
#pragma mark 签到
-(void)sendRequestToSign{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerSignGetBBTManagerBlock:^(BOOL isSuccessed, NSInteger reward) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [[BYAlertView sharedAlertView] showAlertSignWithTitle:reward block:^{
                // 刷新
                [AccountModel sharedAccountModel].loginServerModel.finance.coin += reward;
                NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"信息" sourceArr:strongSelf.centerArr];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
                [strongSelf.scrollingHeaderView.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                
                if ([AccountModel sharedAccountModel].isShenhe){
                    return ;
                }
                // 跳转到我的钱包
                CenterBBTViewController *bbtViewController = [[CenterBBTViewController alloc]init];
                bbtViewController.transferControllerType = CenterBBTViewControllerTypeBP;
                [bbtViewController hidesTabBarWhenPushed];
                [strongSelf.navigationController pushViewController:bbtViewController animated:YES];
            }];
        } else {
            [StatusBarManager statusBarHidenWithText:@"签到失败"];
        }
    }];
}

#pragma mark 获取个人中心用户信息
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]centerGetUserCountInfoManagerBlock:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.headerViewInfo.transferAccountModel = [AccountModel sharedAccountModel];
        [strongSelf.scrollingHeaderView.tableView reloadData];
    }];
}

-(void)logoutManager{
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] logoutManagerBlock:^{
        if (!weakSelf){
            return ;
        }
        // 进行UI退出登录
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.headerViewInfo notLogStatus];
        [[CenterRootViewController sharedController] logoutUIManager];
        
        NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"信息" sourceArr:strongSelf.centerArr];
        NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"信息" sourceArr:strongSelf.centerArr];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
        CenterHeaderSegmentItemsTableViewCell *cell = (CenterHeaderSegmentItemsTableViewCell *)[strongSelf.scrollingHeaderView.tableView cellForRowAtIndexPath:indexPath];
        [cell reloadItems];
        btnCell.transferTitle = @"登录";
    }];
}

-(void)logoutUIManager{
    __weak typeof(self)weakSelf = self;
    [self authorizeWithCompletionHandler:^(BOOL successed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (successed){
            strongSelf->btnCell.transferTitle = @"退出登录";
            [strongSelf sendRequestToGetInfo];
        }
    }];
}


-(void)getHasRealAndWallet{
    __weak typeof(self)weakSelf = self;
    [StatusBarManager statusBarShowWithText:@"正在读取区块信息……"];
    [[NetworkAdapter sharedAdapter] getHasRealInfoAndWalletWithBlock:^(WalletHasRealModel * _Nonnull walletHasRealModel) {
        if (!weakSelf){
            return ;
        }
        
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (!walletHasRealModel.user_cer_status){               // 没有实名认证
            [StatusBarManager statusBarHidenWithText:@"请先实名认证"];
            MineRealNameViewController *realVC = [[MineRealNameViewController alloc]init];
            [strongSelf.navigationController pushViewController:realVC animated:YES];
            return;
        }
        
        if (walletHasRealModel.wallet_status){              // 已经有钱包
            if (walletHasRealModel.block_address.length){
                [AccountModel sharedAccountModel].block_Address = walletHasRealModel.block_address;;
                [AccountModel sharedAccountModel].loginServerModel.account.block_address = walletHasRealModel.block_address;
                WalletRootViewController *walletVC = [[WalletRootViewController alloc]init];
                [strongSelf.navigationController pushViewController:walletVC animated:YES];
                return;
            } else {
                WalletLaungchViewController *walletLaungchVC = [[WalletLaungchViewController alloc]init];
                [strongSelf.navigationController pushViewController:walletLaungchVC animated:YES];
                return;
            }
        } else {
            WalletLaungchViewController *walletLaungchVC = [[WalletLaungchViewController alloc]init];
            [strongSelf.navigationController pushViewController:walletLaungchVC animated:YES];
            return;
        }
    }];
}

-(void)memberVipProductMangaer{
    __weak typeof(self)weakSelf = self;
    [StatusBarManager statusBarShowWithText:@"正在读取区块信息……"];
    [[NetworkAdapter sharedAdapter] getHasRealInfoAndWalletWithBlock:^(WalletHasRealModel * _Nonnull walletHasRealModel) {
        if (!weakSelf){
            return ;
        }
        
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (!walletHasRealModel.user_cer_status){               // 没有实名认证
            [StatusBarManager statusBarHidenWithText:@"请先实名认证"];
            MineRealNameViewController *realVC = [[MineRealNameViewController alloc]init];
            [strongSelf.navigationController pushViewController:realVC animated:YES];
            return;
        }
        
        if (walletHasRealModel.wallet_status){              // 已经有钱包
            if (walletHasRealModel.block_address.length){
                [AccountModel sharedAccountModel].block_Address = walletHasRealModel.block_address;;
                [AccountModel sharedAccountModel].loginServerModel.account.block_address = walletHasRealModel.block_address;
                ProductVipViewController *walletVC = [[ProductVipViewController alloc]init];
                [strongSelf.navigationController pushViewController:walletVC animated:YES];
                return;
            } else {
                WalletLaungchViewController *walletLaungchVC = [[WalletLaungchViewController alloc]init];
                [strongSelf.navigationController pushViewController:walletLaungchVC animated:YES];
                return;
            }
        } else {
            WalletLaungchViewController *walletLaungchVC = [[WalletLaungchViewController alloc]init];
            [strongSelf.navigationController pushViewController:walletLaungchVC animated:YES];
            return;
        }
    }];
}



#pragma mark - 跳转到钱包
-(void)directToWallet{
    [self getHasRealAndWallet];
}

-(void)logoutStatusManager{
    // 1. 清空头部
    self.headerViewInfo.transferAccountModel = [AccountModel sharedAccountModel];
    // 2. a清空
    [self.scrollingHeaderView.tableView reloadData];
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat point = scrollView.contentOffset.y;
    self.headerViewInfo.orgin_y = (self.scrollingHeaderView.headerImageViewHeight - self.headerViewInfo.size_height) / 2.- point;
    CGFloat margin = (self.headerViewInfo.orgin_y - LCFloat(44)) / 2.;
    self.rightSettingButton.orgin_y = self.headerViewInfo.orgin_y - margin - LCFloat(44);
    self.leftSettingButton.orgin_y = self.headerViewInfo.orgin_y - margin - LCFloat(44);
    self.navLabel.center_y = self.leftSettingButton.center_y;
}



@end


