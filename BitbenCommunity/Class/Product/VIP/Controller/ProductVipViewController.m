//
//  ProductVipViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ProductVipViewController.h"
#import "NetworkAdapter+MemberVIP.h"
#import "ProductVipEquityTableViewCell.h"
#import "ProductVipMemberListTableViewCell.h"
#import "ProductVipEquityTableViewCell.h"
#import "ProductVipMemberTitleTableViewCell.h"
#import "ProductVipMainCardTableViewCell.h"
#import "ProductVipSegmentListTableViewCell.h"
#import "ProductComfirmViewController.h"
#import "KMScrollingHeaderView.h"
#import "ProductVipSuperMemberTableViewCell.h"
#import "WalletRootViewController.h"
#import "WebSocketTestViewController.h"
#import "VersionInvitationInputWindow.h"
#import "JCAlertView.h"
#import "NetworkAdapter+VersionInvitation.h"
#import "CenterBBTDetailedViewController.h"

@interface ProductVipViewController ()<UITableViewDelegate,UITableViewDataSource,KMScrollingHeaderViewDelegate>{
    ProductVipMemberListTableViewCell *cell;
    ProductVipMemberListTableViewCell *cell1;
    ProductVipMemberModel * _Nonnull infoProductModel;
    ProductVipMemberProductsListSingleModel * _Nonnull memberSelectProductSingleModel;
    ProductVipMemberProductsListSingleModel * _Nonnull poolSelectProductSingleModel;
    NSInteger segmentSelectedIndex;
    JCAlertView *alert;
}
@property (nonatomic,strong)KMScrollingHeaderView *productScrollHeaderView;
@property (nonatomic,strong)UIButton *rightSettingButton;
@property (nonatomic,strong)UIButton *leftSettingButton;
@property (nonatomic,strong)UILabel *navLabel;

@property (nonatomic,strong)NSMutableArray *vipRootMutableArr;
@property (nonatomic,strong)UIView *navBgView;
@property (nonatomic,strong)NSMutableArray *poolMutableArr;
@end

@implementation ProductVipViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfo];
    [self createNotifi];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [cell animationScrollIndex:3];
    if (cell1){
        [cell1 animationScrollIndex:3];
    }
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"会员中心";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.vipRootMutableArr = [NSMutableArray array];
    [self.vipRootMutableArr addObjectsFromArray:@[@[@"卡"],@[@"会员权益Segment"],@[@"会员列表"],@[@"会员标题",@"会员权益详情"],@[@"立即开通"],@[@"sm"]]];
    self.poolMutableArr = [NSMutableArray array];
    segmentSelectedIndex = 0;
}

#pragma mark - UITableView
-(void)createTableView{
    self.productScrollHeaderView = [[KMScrollingHeaderView alloc]initWithFrame:self.view.bounds imgHeight:LCFloat(100) scaling:LCFloat(100)];
    self.productScrollHeaderView.tableView.dataSource = self;
    self.productScrollHeaderView.tableView.delegate = self;
    self.productScrollHeaderView.tableView.backgroundColor = [UIColor whiteColor];
    self.productScrollHeaderView.delegate = self;
    
    self.productScrollHeaderView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.productScrollHeaderView.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.productScrollHeaderView.tableView.showsVerticalScrollIndicator = YES;
    self.productScrollHeaderView.tableView.backgroundColor = [UIColor whiteColor];
    
    self.productScrollHeaderView.tableView.separatorColor = [UIColor whiteColor];
    self.productScrollHeaderView.headerImageViewContentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:self.productScrollHeaderView];
    
    self.productScrollHeaderView.navbarViewFadingOffset = LCFloat(100);
    
    [self.productScrollHeaderView reloadScrollingHeader];

    [self createNavBarInfo];
}

-(void)createNavBarInfo{
    self.navBgView = [[UIView alloc]init];
    self.navBgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [BYTabbarViewController sharedController].navBarHeight);
    self.navBgView.alpha = 0;
    self.navBgView.backgroundColor = [UIColor hexChangeFloat:@"ED5845"];
    [self.view addSubview:self.navBgView];
    __weak typeof(self)weakSelf = self;
    
    // 设置按钮
    self.rightSettingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightSettingButton.backgroundColor = [UIColor clearColor];
    [self.rightSettingButton setImage:[UIImage imageNamed:@"icon_vip_nav_question"] forState:UIControlStateNormal];
    
    self.rightSettingButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(7) - LCFloat(44), [BYTabbarViewController sharedController].statusHeight, LCFloat(44), LCFloat(44));
    [self.view addSubview:self.rightSettingButton];
    [self.rightSettingButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDWebViewController *webViewController = [[PDWebViewController alloc]init];
        [webViewController webDirectedWebUrl:page2];
        [strongSelf.navigationController pushViewController:webViewController animated:YES];
    }];
    
    self.leftSettingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftSettingButton.backgroundColor = [UIColor clearColor];
    [self.leftSettingButton setImage:[UIImage imageNamed:@"icon_center_bbt_back"] forState:UIControlStateNormal];
    self.leftSettingButton.frame = CGRectMake(LCFloat(7), self.rightSettingButton.orgin_y, LCFloat(44), LCFloat(44));
    [self.view addSubview:self.leftSettingButton];
    [self.leftSettingButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    self.navLabel = [GWViewTool createLabelFont:@"15" textColor:@"白"];
    self.navLabel.text = @"会员中心";
    CGSize navSize = [Tool makeSizeWithLabel:self.navLabel];
    self.navLabel.frame = CGRectMake((kScreenBounds.size.width - navSize.width) / 2., 0, navSize.width, navSize.height);
    self.navLabel.center_y = self.rightSettingButton.center_y;
    [self.view addSubview:self.navLabel];
}

#pragma mark - KMScrollingHeaderViewDelegate
- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView headerImageView:(PDImageView *)imageView{
    imageView.backgroundColor = [UIColor hexChangeFloat:@"ED5845"];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.vipRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.vipRootMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"卡" sourceArr:self.vipRootMutableArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        ProductVipMainCardTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[ProductVipMainCardTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferMemberModel = infoProductModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickWithLeftBtn:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            CenterBBTDetailedViewController *bbtVC = [[CenterBBTDetailedViewController alloc]init];
            bbtVC.transferFinaniceType = BBTMyFinaniceTypeBpSuoding;
            [strongSelf.navigationController pushViewController:bbtVC animated:YES];
        }];
        [cellWithRowOne actionClickWithRightBtn:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            CenterBBTDetailedViewController *bbtVC = [[CenterBBTDetailedViewController alloc]init];
            bbtVC.transferFinaniceType = BBTMyFinaniceTypeBpYue;
            [strongSelf.navigationController pushViewController:bbtVC animated:YES];
        }];
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"会员权益Segment" sourceArr:self.vipRootMutableArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        ProductVipSegmentListTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[ProductVipSegmentListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo actionClickWithItemsBlock:^(NSInteger index) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->segmentSelectedIndex = index;
            [strongSelf segmentListSelectedManager];
        }];
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"会员列表" sourceArr:self.vipRootMutableArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        ProductVipMemberListTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[ProductVipMemberListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cell = cellWithRowThr;
        
        if (segmentSelectedIndex == 0){
            cell.transferListModel = infoProductModel.member_products;
        }
        
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr scrollViewScrollMangaerWithBlock:^(ProductVipMemberProductsListSingleModel * _Nonnull singleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->memberSelectProductSingleModel = singleModel;
            strongSelf->memberSelectProductSingleModel.member_days = strongSelf->infoProductModel.member_days;
            [strongSelf.productScrollHeaderView.tableView reloadData];
        }];
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"池主列表" sourceArr:self.vipRootMutableArr]){
        static NSString *cellIdentifyWithRowThr1 = @"cellIdentifyWithRowThr1";
        ProductVipMemberListTableViewCell *cellWithRowThr1 = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr1];
        if (!cellWithRowThr1){
            cellWithRowThr1 = [[ProductVipMemberListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr1];
        }
        cell1 = cellWithRowThr1;
        
        if (segmentSelectedIndex == 1){
            cell1.transferListModel = infoProductModel.pool_products;
        }
        
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr1 scrollViewScrollMangaerWithBlock:^(ProductVipMemberProductsListSingleModel * _Nonnull singleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
//            strongSelf->selectProductSingleModel = singleModel;
            
            [strongSelf.productScrollHeaderView.tableView reloadData];
        }];
        return cellWithRowThr1;
    } else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"会员标题" sourceArr:self.vipRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"会员标题" sourceArr:self.vipRootMutableArr]) || (indexPath.section == [self cellIndexPathSectionWithcellData:@"池主标题" sourceArr:self.vipRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"池主标题" sourceArr:self.vipRootMutableArr])){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        ProductVipMemberTitleTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[ProductVipMemberTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
        }
        
        if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"会员标题" sourceArr:self.vipRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"会员标题" sourceArr:self.vipRootMutableArr])){
            cellWithRowFour.transferTitle = [NSString stringWithFormat:@"%@权益",memberSelectProductSingleModel.product_name];
        } else {
            cellWithRowFour.transferTitle = @"池主通用说明";
        }
        
        return cellWithRowFour;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"会员权益详情" sourceArr:self.vipRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"会员权益详情" sourceArr:self.vipRootMutableArr]){
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        ProductVipEquityTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[ProductVipEquityTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
        }
        cellWithRowFiv.transferProductSingleModel = memberSelectProductSingleModel;
        
        return cellWithRowFiv;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"立即开通" sourceArr:self.vipRootMutableArr]){
        static NSString *cellIdentifyWithRowSex = @"cellIdentifyWithRowSex";
        GWButtonTableViewCell *cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSex];
        if (!cellWithRowSex){
            cellWithRowSex = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSex];
        }
        cellWithRowSex.backgroundColor = [UIColor whiteColor];
        cellWithRowSex.transferCellHeight = cellHeight;
        if (segmentSelectedIndex == 0){
            if (infoProductModel.member_level == 1){
                cellWithRowSex.transferTitle = @"立即开通";
                [cellWithRowSex setButtonStatus:YES];
            } else {
                if (infoProductModel.member_level < memberSelectProductSingleModel.member_level){
                    cellWithRowSex.transferTitle = @"立即升级";
                    [cellWithRowSex setButtonStatus:YES];
                } else if (infoProductModel.member_level > memberSelectProductSingleModel.member_level){
                    cellWithRowSex.transferTitle = @"无法降级";
                    [cellWithRowSex setButtonStatus:NO];
                } else if (infoProductModel.member_level == memberSelectProductSingleModel.member_level){
                    cellWithRowSex.transferTitle = @"立即续费";
                    [cellWithRowSex setButtonStatus:YES];
                }
            }
            if(memberSelectProductSingleModel.member_level == 1){
                cellWithRowSex.transferTitle = @"无需购买";
                [cellWithRowSex setButtonStatus:NO];
            }
        } else {
            cellWithRowSex.transferTitle = @"敬请等待";
            [cellWithRowSex setButtonStatus:NO];
        }
        if(!memberSelectProductSingleModel.available){
            cellWithRowSex.transferTitle = @"暂不可使用";
            [cellWithRowSex setButtonStatus:NO];
        }
        
//        cellWithRowSex.transferTitle = @"暂不可开通";
//        [cellWithRowSex setButtonStatus:NO];

        __weak typeof(self)weakSelf = self;
        [cellWithRowSex buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if(strongSelf->infoProductModel.invitation_code.length){
                [strongSelf sendRequestToMakeOrder:strongSelf->memberSelectProductSingleModel];
                return;
            } else {
                [strongSelf actionShowInvitationManager];
            }
            
            
        }];
        return cellWithRowSex;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"sm" sourceArr:self.vipRootMutableArr]){
        static NSString *cellIdentifyWithRowSev = @"cellIdentifyWithRowSev";
        GWNormalTableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev];
        if (!cellWithRowSev){
            cellWithRowSev = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev];
        }
        return cellWithRowSev;
    } else {
        static NSString *cellIdentifyWithRowEig = @"cellIdentifyWithRowEig";
        ProductVipSuperMemberTableViewCell *cellWithRowEig = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowEig];
        if (!cellWithRowEig){
            cellWithRowEig = [[ProductVipSuperMemberTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowEig];
        }
        cellWithRowEig.transferTitle = [[self.vipRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        return cellWithRowEig;
    }
    
    
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"卡" sourceArr:self.vipRootMutableArr]){
        return [ProductVipMainCardTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"会员权益Segment" sourceArr:self.vipRootMutableArr]){
        return [ProductVipSegmentListTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"会员列表" sourceArr:self.vipRootMutableArr] || indexPath.section == [self cellIndexPathSectionWithcellData:@"池主列表" sourceArr:self.vipRootMutableArr] ){
        return [ProductVipMemberListTableViewCell calculationCellHeight];
    } else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"会员标题" sourceArr:self.vipRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"会员标题" sourceArr:self.vipRootMutableArr]) || (indexPath.section == [self cellIndexPathSectionWithcellData:@"池主标题" sourceArr:self.vipRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"池主标题" sourceArr:self.vipRootMutableArr])){
        return [ProductVipMemberTitleTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"会员权益详情" sourceArr:self.vipRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"会员权益详情" sourceArr:self.vipRootMutableArr]){
        return [ProductVipEquityTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"立即开通" sourceArr:self.vipRootMutableArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"sm" sourceArr:self.vipRootMutableArr]){
        return [GWNormalTableViewCell calculationCellHeight];
    } else {
        return [ProductVipSuperMemberTableViewCell calculationCellHeightWithTitle: [[self.vipRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

-(void)segmentListSelectedManager{
    if(segmentSelectedIndex == 1){
        //移除
        NSInteger has = -1;
        has = [self cellIndexPathSectionWithcellData:@"会员权益详情" sourceArr:self.vipRootMutableArr];
        if (has != -1){         // 表示有会员权益Segment - 移除
            [self.vipRootMutableArr removeObjectAtIndex:has];
            [self.vipRootMutableArr insertObject:self.poolMutableArr atIndex:has];
        }
        // 2. 添加池主
        if (![self.vipRootMutableArr containsObject:self.poolMutableArr]){
            NSInteger index = [self cellIndexPathSectionWithcellData:@"会员权益Segment" sourceArr:self.vipRootMutableArr];
            [self.vipRootMutableArr insertObject:self.poolMutableArr atIndex:index];
        }
        
        // 3. 移除会员列表
        NSInteger section = [self cellIndexPathSectionWithcellData:@"会员列表" sourceArr:self.vipRootMutableArr];
        if ([self.vipRootMutableArr containsObject:@[@"会员列表"]]){
            [self.vipRootMutableArr replaceObjectAtIndex:section withObject:@[@"池主列表"]];
            [self.productScrollHeaderView.tableView reloadData];
        }
    } else {
        NSInteger has = - 1;
        has = [self cellIndexPathSectionWithcellData:@"会员权益详情" sourceArr:self.vipRootMutableArr];
        if (has == -1){             // 表示没有会员权益详情
            NSInteger index = 0;
            if ([self.vipRootMutableArr containsObject:self.poolMutableArr]){
                index = [self.vipRootMutableArr indexOfObject:self.poolMutableArr];
                [self.vipRootMutableArr removeObject:self.poolMutableArr];
            }
            index = [self cellIndexPathSectionWithcellData:@"会员列表" sourceArr:self.vipRootMutableArr];
            if (index == - 1){
                index = [self cellIndexPathSectionWithcellData:@"池主列表" sourceArr:self.vipRootMutableArr];
            }
            [self.vipRootMutableArr insertObject:@[@"会员标题",@"会员权益详情"] atIndex:index + 1];
            
            // 判断是否有池主
            // 3. 移除会员列表
            if ([self.vipRootMutableArr containsObject:@[@"池主列表"]]){
                NSInteger section = [self cellIndexPathSectionWithcellData:@"池主列表" sourceArr:self.vipRootMutableArr];
                [self.vipRootMutableArr replaceObjectAtIndex:section withObject:@[@"会员列表"]];
            }
        } else {                    // 表示有会员权益详情
            NSInteger section = [self cellIndexPathSectionWithcellData:@"会员列表" sourceArr:self.vipRootMutableArr];
            NSInteger row = [self cellIndexPathRowWithcellData:@"会员列表" sourceArr:self.vipRootMutableArr];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            [self.productScrollHeaderView.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            [cell animationScrollIndex:3];
        }
    }
    [self.productScrollHeaderView.tableView reloadData];
}

#pragma mark - InterfaceManager
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] sendRequestToGetMemberCenterInfoWithBlock:^(ProductVipMemberModel * _Nonnull productModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->infoProductModel = productModel;

        [strongSelf.poolMutableArr addObject:@"池主标题"];
        [strongSelf.poolMutableArr addObjectsFromArray:productModel.pool_describe];
        if (productModel.member_products.count > 1){
            strongSelf-> memberSelectProductSingleModel = [productModel.member_products objectAtIndex:1];
            strongSelf->memberSelectProductSingleModel.member_days = strongSelf->infoProductModel.member_days;
        }

        [strongSelf.productScrollHeaderView.tableView reloadData];
    }];
}
//member_level

-(void)sendRequestToMakeOrder:(ProductVipMemberProductsListSingleModel *)orderModel{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] sendRequestToMakeOrderWithProductId:orderModel.product_id amount:1 block:^(ProductVipConfirmModel * _Nonnull productModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (productModel){
            ProductComfirmViewController *productConfirmVC = [[ProductComfirmViewController alloc]init];
            
            NSMutableArray *infoMutableArr = [NSMutableArray array];
            [infoMutableArr addObject:@[@"订单信息",[NSString stringWithFormat:@"%@ %@天",orderModel.product_name,strongSelf->infoProductModel.member_days]]];
            [infoMutableArr addObject:@[@"支付币种",productModel.name.length?productModel.name:@"USDT"]];
            [infoMutableArr addObject:@[@"矿工费 ",[NSString stringWithFormat:@"0 %@",productModel.name]]];
            
            [infoMutableArr addObject:@[@"应付金额",[NSString stringWithFormat:@"%@ %@",productModel.initial_value,productModel.name]]];
            [infoMutableArr addObject:@[@"现有等级可抵消金额",[NSString stringWithFormat:@"%@ %@",productModel.discount_value,productModel.name]]];
            [infoMutableArr addObject:@[@"实付金额",[NSString stringWithFormat:@"%@ %@",productModel.value,productModel.name]]];
            
            productConfirmVC.transferOrderInfo = [infoMutableArr copy];
            productModel.product_name = orderModel.product_name;
            productConfirmVC.transferProductModel = productModel;
            [productConfirmVC showInView:strongSelf.parentViewController];
        }
    }];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"%.2f",scrollView.contentOffset.y);
    CGFloat alpha = scrollView.contentOffset.y / [BYTabbarViewController sharedController].navBarHeight;
    self.navBgView.alpha = alpha;
}





#pragma mark- Info
-(void)actionShowInvitationManager{
    VersionInvitationInputWindow *versionInvitationView = [[VersionInvitationInputWindow alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(38), LCFloat(244))];
    __weak typeof(self)weakSelf = self;
    [versionInvitationView actionCheckManagerWithBlock:^(NSString * _Nonnull info) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->infoProductModel.invitation_code = info;
        [strongSelf interfaceWithBindingMemberInvitationCode:info Block:^{
            [JCAlertView dismissAllCompletion:^{
                
            }];
            [strongSelf sendRequestToMakeOrder:strongSelf->memberSelectProductSingleModel];
        }];
    }];
    [versionInvitationView actionCancelManagerWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [JCAlertView dismissAllCompletion:^{
            [strongSelf sendRequestToMakeOrder:strongSelf->memberSelectProductSingleModel];
        }];
    }];
    
    versionInvitationView.clipsToBounds = YES;
    versionInvitationView.layer.cornerRadius = LCFloat(4);
    alert = [JCAlertView initWithCustomView:versionInvitationView dismissWhenTouchedBackground:YES];
}

-(void)interfaceWithBindingMemberInvitationCode:(NSString *)code Block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] InvitationBandingMemberWithCode:code Block:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
}



#pragma mark - 键盘通知
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    alert.orgin_y =  (keyboardRect.size.height) / 3.;
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    alert.center_y = kScreenBounds.size.height / 2.;
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
@end
