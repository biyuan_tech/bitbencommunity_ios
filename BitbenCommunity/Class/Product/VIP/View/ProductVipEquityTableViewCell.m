//
//  ProductVipEquityTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ProductVipEquityTableViewCell.h"


@interface ProductVipEquityTableViewCellSingleView()


@end

@implementation ProductVipEquityTableViewCellSingleView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.frame = CGRectMake((self.size_width - LCFloat(55)) / 2., 0, LCFloat(55), LCFloat(55));
    [self addSubview:self.iconImgView];
    
    self.fixedLabel = [GWViewTool createLabelFont:@"12" textColor:@"565656"];
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    self.fixedLabel.frame = CGRectMake(0, CGRectGetMaxY(self.iconImgView.frame) + LCFloat(18), self.size_width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
    [self addSubview:self.fixedLabel];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"12" textColor:@"9E6410"];
    self.dymicLabel.textAlignment = NSTextAlignmentCenter;
    self.dymicLabel.frame = CGRectMake(0, CGRectGetMaxY(self.fixedLabel.frame) + LCFloat(10), self.size_width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
    [self addSubview:self.dymicLabel];
}

+(CGFloat)calculationSizeHeight{
    CGFloat height = 0;
    height += LCFloat(55);
    height += LCFloat(18);
    height += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    height += LCFloat(10);
    height += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    height += LCFloat(11);
    return height;
}

@end


@interface ProductVipEquityTableViewCell()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)ProductVipEquityTableViewCellSingleView *singleView1;
@property (nonatomic,strong)ProductVipEquityTableViewCellSingleView *singleView2;
@property (nonatomic,strong)ProductVipEquityTableViewCellSingleView *singleView3;
@property (nonatomic,strong)ProductVipEquityTableViewCellSingleView *singleView4;

@end

@implementation ProductVipEquityTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgView];

    CGFloat width = kScreenBounds.size.width / 4.;
    CGFloat height = [ProductVipEquityTableViewCellSingleView calculationSizeHeight];
    
    for (int i = 0 ; i < 4;i++){
        CGFloat origin_x = i * width;
        ProductVipEquityTableViewCellSingleView *singleView = [[ProductVipEquityTableViewCellSingleView alloc]initWithFrame:CGRectMake(origin_x, 0, width, height)];
        [self.bgView addSubview:singleView];
        
        if (i == 0){
            singleView.iconImgView.image = [UIImage imageNamed:@"icon_vip_jiasu"];
            singleView.fixedLabel.text = @"每日固定解锁";
            singleView.dymicLabel.text = @"计算中";
            self.singleView1 = singleView;
        } else if (i == 1){
            singleView.iconImgView.image = [UIImage imageNamed:@"icon_vip_jiesuo"];
            singleView.fixedLabel.text = @"每日解锁量上限";
            singleView.dymicLabel.text = @"计算中";
            self.singleView2 = singleView;
        } else if (i == 2){
            singleView.iconImgView.image = [UIImage imageNamed:@"icon_vip_fenhong"];
            singleView.fixedLabel.text = @"分红权重";
            singleView.dymicLabel.text = @"计算中";
            self.singleView3 = singleView;
        } else if (i == 3){
            singleView.iconImgView.image = [UIImage imageNamed:@"icon_vip_gongjiesuo"];
            singleView.fixedLabel.text = @"180日解锁总量";
            singleView.dymicLabel.text = @"计算中";
            self.singleView4 = singleView;
        }
    }
}

-(void)setTransferProductSingleModel:(ProductVipMemberProductsListSingleModel *)transferProductSingleModel{
    _transferProductSingleModel = transferProductSingleModel;
    
    self.singleView1.dymicLabel.text = [NSString stringWithFormat:@"%@",transferProductSingleModel.product_remark.daily_speed_profit];
    self.singleView2.dymicLabel.text = [NSString stringWithFormat:@"%@",transferProductSingleModel.product_remark.daily_unlock];
    self.singleView3.dymicLabel.text = [NSString stringWithFormat:@"%@",transferProductSingleModel.product_remark.dividend_ratio];
    self.singleView4.dymicLabel.text = [NSString stringWithFormat:@"%@",transferProductSingleModel.product_remark.month_unlock];
    self.singleView4.fixedLabel.text = [NSString stringWithFormat:@"%@日解锁总量",transferProductSingleModel.member_days];
    
}

+(CGFloat)calculationCellHeight{
    return [ProductVipEquityTableViewCellSingleView calculationSizeHeight];
}

@end
