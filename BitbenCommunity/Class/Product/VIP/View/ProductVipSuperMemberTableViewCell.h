//
//  ProductVipSuperMemberTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductVipSuperMemberTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;

+(CGFloat)calculationCellHeightWithTitle:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
