//
//  ProductLineCollectionViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/19.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductVipMemberModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductLineCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)ProductVipMemberProductsListSingleModel *transferVipProductModel;

@end

NS_ASSUME_NONNULL_END
