//
//  ProductVipSegmentListTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductVipSegmentListTableViewCell : PDBaseTableViewCell

-(void)actionClickWithItemsBlock:(void(^)(NSInteger index))block;

@end

NS_ASSUME_NONNULL_END
