//
//  ProductVipMemberListTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ProductVipMemberModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductVipMemberListSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)UIImage *transferImg;

@end

@interface ProductVipMemberListTableViewCell : PDBaseTableViewCell
@property (nonatomic,strong)NSArray<ProductVipMemberProductsListSingleModel> *transferListModel;

-(void)animationScrollIndex:(NSInteger )index;

-(void)scrollViewScrollMangaerWithBlock:(void(^)(ProductVipMemberProductsListSingleModel *singleModel))block;
@end

NS_ASSUME_NONNULL_END
