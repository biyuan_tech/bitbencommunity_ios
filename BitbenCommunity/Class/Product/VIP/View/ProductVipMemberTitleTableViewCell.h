//
//  ProductVipMemberTitleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductVipMemberTitleTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;

@end

NS_ASSUME_NONNULL_END
