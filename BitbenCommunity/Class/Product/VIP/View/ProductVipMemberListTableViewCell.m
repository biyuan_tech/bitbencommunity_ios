//
//  ProductVipMemberListTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ProductVipMemberListTableViewCell.h"
#import "LineLayout.h"
#import "ProductLineCollectionViewCell.h"

@interface ProductVipMemberListSingleTableViewCell()
@property (nonatomic,strong)PDImageView *mainImgView;

@end

@implementation ProductVipMemberListSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.mainImgView = [[PDImageView alloc]init];
    self.mainImgView.backgroundColor = [UIColor clearColor];
    self.mainImgView.frame = CGRectMake(0, 0, LCFloat(210), LCFloat(210));
    [self addSubview:self.mainImgView];
    self.mainImgView.transform = CGAffineTransformMakeRotation(M_PI/2);
}

-(void)setTransferImg:(UIImage *)transferImg{
    _transferImg = transferImg;
    self.mainImgView.image = transferImg;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(210);
}
@end


static char scrollViewScrollMangaerWithBlockKey;
@interface ProductVipMemberListTableViewCell()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong)UICollectionView *productCollectionView;           /**< 首页collectionView*/
@property (nonatomic,strong)NSMutableArray *productMutableArr;
@property (nonatomic,strong)LineLayout *lineLayout;

@end

@implementation ProductVipMemberListTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self arrayWithInit];
        [self createView];
    }
    return self;
}


#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.productMutableArr = [NSMutableArray array];
}

#pragma mark - createView
-(void)createView{
    self.lineLayout = [[LineLayout alloc] init];
    
    self.productCollectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:self.lineLayout];
    self.productCollectionView.backgroundColor = [UIColor clearColor];
    self.productCollectionView.showsVerticalScrollIndicator = NO;
    self.productCollectionView.delegate = self;
    self.productCollectionView.dataSource = self;
    self.productCollectionView.showsHorizontalScrollIndicator = NO;
    self.productCollectionView.scrollsToTop = YES;
    self.productCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self addSubview:self.productCollectionView];
    
    [self.productCollectionView registerClass:[ProductLineCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
}


#pragma mark - UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.productMutableArr.count;
}

#pragma mark - UICollectionViewDelegate
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ProductLineCollectionViewCell *cell = (ProductLineCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectionIdentify"  forIndexPath:indexPath];
    ProductVipMemberProductsListSingleModel *singleModel = [self.transferListModel objectAtIndex:indexPath.row];
    cell.transferVipProductModel = singleModel;
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(LCFloat(120),LCFloat(120));
}

#pragma mark cell的点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ProductLineCollectionViewCell *cell = (ProductLineCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    CGRect rectInTableView=[collectionView convertRect:cell.frame toView:collectionView];
    CGPoint newPoint = CGPointMake(rectInTableView.origin.x - LCFloat(120), 0);
    [collectionView setContentOffset:newPoint animated:YES];
    
    void(^block)(ProductVipMemberProductsListSingleModel *singleModel) = objc_getAssociatedObject(self, &scrollViewScrollMangaerWithBlockKey);
    ProductVipMemberProductsListSingleModel *mainModel = [self.transferListModel objectAtIndex:indexPath.row];
    if (block){
        block(mainModel);
    }
    
}


// 滚动视图减速完成，滚动将停止时，调用该方法。一次有效滑动，只执行一次。
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGPoint point = scrollView.contentOffset;
    NSInteger index = (point.x - 150) / (kScreenBounds.size.width / 3.33);

    NSInteger mainIndex = 0;
    if (index > self.transferListModel.count - 1){
        mainIndex = self.transferListModel.count - 1;
    } else     if (index < 0){
        mainIndex = 0;
    } else {
        mainIndex = index;
    }

    void(^block)(ProductVipMemberProductsListSingleModel *singleModel) = objc_getAssociatedObject(self, &scrollViewScrollMangaerWithBlockKey);
    ProductVipMemberProductsListSingleModel *mainModel = [self.transferListModel objectAtIndex:mainIndex];
    if (block){
        block(mainModel);
    }
}

-(void)scrollViewScrollMangaerWithBlock:(void(^)(ProductVipMemberProductsListSingleModel *singleModel))block{
    objc_setAssociatedObject(self, &scrollViewScrollMangaerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


-(void)animationScrollIndex:(NSInteger)index{
    if (self.productMutableArr.count){
        NSIndexPath *indexPath  = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.productCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
//        [self.productCollectionView setContentOffset:CGPointMake(LCFloat(240), 0)];
    }
}



-(void)setTransferListModel:(NSArray<ProductVipMemberProductsListSingleModel> *)transferListModel{
    _transferListModel = transferListModel;
    [self.productMutableArr removeAllObjects];
    [self.productMutableArr addObjectsFromArray:transferListModel];
    [self.productCollectionView reloadData];
}

+(CGFloat)calculationCellHeight{
    return 200;
}


@end
