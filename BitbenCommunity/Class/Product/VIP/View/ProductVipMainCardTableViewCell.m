//
//  ProductVipMainCardTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ProductVipMainCardTableViewCell.h"

static char actionClickWithLeftBtnKey;
static char actionClickWithRightBtnKey;
@interface ProductVipMainCardTableViewCell()
@property (nonatomic,strong)PDImageView *cardBgRootView;;
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UIView *itemsView;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UILabel *youxiaoqiLabel;
@property (nonatomic,strong)ProductVipEquityView *zuoriFenhongView;
@property (nonatomic,strong)UIButton *actionButton1;
@property (nonatomic,strong)ProductVipEquityView *gudingShouyiView;
@property (nonatomic,strong)UIButton *actionButton2;
@property (nonatomic,strong)ProductVipEquityView *zuoriwakuangView;
@property (nonatomic,strong)UIButton *actionButton3;

@end

@implementation ProductVipMainCardTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.backgroundColor = [UIColor hexChangeFloat:@"ED5845"];
    
    self.cardBgRootView = [[PDImageView alloc]init];
    self.cardBgRootView.backgroundColor = [UIColor hexChangeFloat:@"ED5845"];
    self.cardBgRootView.frame = CGRectMake(0, LCFloat(70), kScreenBounds.size.width, LCFloat(100));
    self.cardBgRootView.image = [Tool stretchImageWithName:@"bg_product_card_root"];
    [self addSubview:self.cardBgRootView];
    
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor hexChangeFloat:@"F6EADF"];
    self.bgImgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(170));
    self.bgImgView.layer.cornerRadius = LCFloat(4);
    self.bgImgView.clipsToBounds = YES;
    [self addSubview:self.bgImgView];
    
    self.itemsView = [[UIView alloc]init];
    self.itemsView.backgroundColor = [UIColor clearColor];
    [self.bgImgView addSubview:self.itemsView];
    
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame = CGRectMake(LCFloat(23), LCFloat(23), LCFloat(45), LCFloat(45));
    self.avatarImgView.layer.cornerRadius = self.avatarImgView.size_width / 2.;
    self.avatarImgView.clipsToBounds = YES;
    self.avatarImgView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.avatarImgView.layer.borderWidth = 2;
    [self.bgImgView addSubview:self.avatarImgView];
    
    // 昵称
    self.nickNameLabel = [GWViewTool createLabelFont:@"16" textColor:@"2D1A08"];
    [self.bgImgView addSubview:self.nickNameLabel];
    
    // id
    self.numberLabel = [GWViewTool createLabelFont:@"13" textColor:@"2D1A08"];
    [self.bgImgView  addSubview:self.numberLabel];
    
    // 有效期
    self.youxiaoqiLabel = [GWViewTool createLabelFont:@"12" textColor:@"B49979"];
    [self.bgImgView addSubview:self.youxiaoqiLabel];
    
    self.zuoriFenhongView = [[ProductVipEquityView alloc]initWithFrame:CGRectMake(0, 0, [ProductVipEquityView calculationSize].width, [ProductVipEquityView calculationSize].height)];
    [self.bgImgView addSubview:self.zuoriFenhongView];
    
    self.actionButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton1.userInteractionEnabled = YES;
    __weak typeof(self)weakSelf = self;
    [self.actionButton1 buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithLeftBtnKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.actionButton1];
    
    self.gudingShouyiView = [[ProductVipEquityView alloc]initWithFrame:CGRectMake(0, 0, [ProductVipEquityView calculationSize].width, [ProductVipEquityView calculationSize].height)];
    [self.bgImgView addSubview:self.gudingShouyiView];
    
    self.actionButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton2.userInteractionEnabled = YES;
    [self.actionButton2 buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithRightBtnKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.actionButton2];
    
    self.zuoriwakuangView = [[ProductVipEquityView alloc]initWithFrame:CGRectMake(0, 0, [ProductVipEquityView calculationSize].width, [ProductVipEquityView calculationSize].height)];
    [self.bgImgView addSubview:self.zuoriwakuangView];
    
    self.actionButton3 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton3.userInteractionEnabled = YES;
    [self.actionButton3 buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithRightBtnKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.actionButton3];
}

-(void)setTransferMemberModel:(ProductVipMemberModel *)transferMemberModel{
    _transferMemberModel = transferMemberModel;
    
    // avatar
    self.avatarImgView.style = [AccountModel sharedAccountModel].loginServerModel.user.cert_badge;
    [self.avatarImgView uploadImageWithURL:[AccountModel sharedAccountModel].loginServerModel.account.head_img placeholder:nil callback:NULL];

    // nick
    LoginServerModel *sm = [AccountModel sharedAccountModel].loginServerModel;
    self.nickNameLabel.text = sm.user.nickname;
    CGSize nickSize = [Tool makeSizeWithLabel:self.nickNameLabel];
    self.nickNameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(12), LCFloat(20), nickSize.width, nickSize.height);
    self.nickNameLabel.center_y = LCFloat(40);
    
    // ID
    self.numberLabel.text = [NSString stringWithFormat:@"ID:%@", [AccountModel sharedAccountModel].loginServerModel.account._id];
    CGSize numberSize = [Tool makeSizeWithLabel:self.numberLabel];
    self.numberLabel.frame = CGRectMake(self.nickNameLabel.orgin_x, CGRectGetMaxY(self.nickNameLabel.frame) + LCFloat(3), numberSize.width, numberSize.height);
    
    // 有效期
    NSArray *dateArr = [transferMemberModel.member_validity componentsSeparatedByString:@" "];
    self.youxiaoqiLabel.text = [NSString stringWithFormat:@"会员有效期至:%@ 00:00",[dateArr firstObject]];
    CGSize youxiaoSize = [Tool makeSizeWithLabel:self.youxiaoqiLabel];
    self.youxiaoqiLabel.frame = CGRectMake(self.numberLabel.orgin_x, CGRectGetMaxY(self.numberLabel.frame) + LCFloat(3), youxiaoSize.width, youxiaoSize.height);
    
    if (transferMemberModel.member_level == 1){
        self.youxiaoqiLabel.hidden = YES;
    } else {
        self.youxiaoqiLabel.hidden = NO;
    }
    

    self.zuoriFenhongView.frame = CGRectMake(self.avatarImgView.orgin_x, CGRectGetMaxY(self.youxiaoqiLabel.frame) + LCFloat(25), self.zuoriFenhongView.size_width, self.zuoriFenhongView.size_height);
    self.zuoriFenhongView.center_y = LCFloat(125);
    [self.zuoriFenhongView infoStr:[NSString stringWithFormat:@"%@",transferMemberModel.bp_dividend] name:@"BP" fixedName:@"昨日分红 >"];
    self.actionButton1.frame = self.zuoriFenhongView.frame;

    CGFloat margin = (kScreenBounds.size.width - 2 * LCFloat(15) - 3 * self.zuoriFenhongView.size_width) / 2.;
    
    
    self.gudingShouyiView.frame = CGRectMake(CGRectGetMaxX(self.zuoriFenhongView.frame) + margin / 3., self.zuoriFenhongView.orgin_y, self.gudingShouyiView.size_width, self.gudingShouyiView.size_height);
    [self.gudingShouyiView infoStr:[NSString stringWithFormat:@"%@",transferMemberModel.bp_fixed] name:@"BP" fixedName:@"昨日固定收益解锁 >"];
    self.actionButton2.frame = self.gudingShouyiView.frame;

    self.zuoriwakuangView.frame = CGRectMake(CGRectGetMaxX(self.gudingShouyiView.frame) + margin, self.zuoriFenhongView.orgin_y, self.zuoriwakuangView.size_width, self.zuoriwakuangView.size_height);
    [self.zuoriwakuangView infoStr:[NSString stringWithFormat:@"%@",transferMemberModel.bp_mining] name:@"BP" fixedName:@"昨日挖矿解锁 >"];
    self.actionButton3.frame = self.zuoriwakuangView.frame;

    // 计算
    CGFloat itemsWidth = self.bgImgView.size_width - CGRectGetMaxX(self.nickNameLabel.frame) - 2 * LCFloat(11);
    self.itemsView.frame = CGRectMake(CGRectGetMaxX(self.nickNameLabel.frame) + LCFloat(11), 0, itemsWidth, LCFloat(16));
    self.itemsView.center_y = self.nickNameLabel.center_y;
    
    NSArray *smar = transferMemberModel.member_tags;
    if (self.itemsView.subviews.count){
        [self.itemsView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    CGFloat origin_x = 0;
    for (int i = 0 ; i < smar.count;i++){
        NSString *info = [smar objectAtIndex:i];
        if (info.length && ![info isEqualToString:@" "]){
            UILabel *titleLabel = [GWViewTool createLabelFont:@"9" textColor:@"88663E"];
            titleLabel.text = info;
            CGSize titleSize = [Tool makeSizeWithLabel:titleLabel];
            titleLabel.frame = CGRectMake(origin_x, 0, titleSize.width + LCFloat(5) * 2, LCFloat(16));
            origin_x += titleLabel.size_width + LCFloat(8);
            titleLabel.layer.cornerRadius = 3;
            titleLabel.layer.borderColor = [UIColor hexChangeFloat:@"88663E"].CGColor;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.layer.borderWidth = 1;
            [self.itemsView addSubview:titleLabel];
        }
    }
}


+(CGFloat)calculationCellHeight{
    return LCFloat(170);
}

-(void)actionClickWithLeftBtn:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithLeftBtnKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
-(void)actionClickWithRightBtn:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithRightBtnKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end


@interface ProductVipEquityView()
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *fixedLabel;

@end

@implementation ProductVipEquityView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.numberLabel = [GWViewTool createLabelFont:@"24" textColor:@"2D1A08"];
    [self addSubview:self.numberLabel];
    
    self.nameLabel = [GWViewTool createLabelFont:@"13" textColor:@"2D1A08"];
    [self addSubview:self.nameLabel];
    
    self.fixedLabel = [GWViewTool createLabelFont:@"12" textColor:@"88663E"];
    [self addSubview:self.fixedLabel];
}

-(void)infoStr:(NSString *)infoStr name:(NSString *)name fixedName:(NSString *)fixedName{
    self.numberLabel.text = infoStr;
    CGSize numberSize = [Tool makeSizeWithLabel:self.numberLabel];
    self.numberLabel.frame = CGRectMake(0, 0, numberSize.width, numberSize.height);
    
    // name
    self.nameLabel.text = name;
    CGSize nameSize = [Tool makeSizeWithLabel:self.nameLabel];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.numberLabel.frame) + LCFloat(5), self.numberLabel.orgin_y + (numberSize.height - nameSize.height) - LCFloat(3), nameSize.width, nameSize.height);
    
    // fixed
    self.fixedLabel.text = fixedName;
    CGSize fixedSize = [Tool makeSizeWithLabel:self.fixedLabel];
    self.fixedLabel.frame = CGRectMake(0, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(5), fixedSize.width, fixedSize.height);
}

+(CGSize)calculationSize{
    CGFloat height = [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"24"]];
    height += LCFloat(8);
    height += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]];
    
    NSString *info = @"昨日获得总量 >";
    CGSize infoSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"12"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"12"]])];
    CGFloat width = infoSize.width + LCFloat(11);
    return CGSizeMake(width, height);
}

@end
