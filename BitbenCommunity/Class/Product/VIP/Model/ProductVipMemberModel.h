//
//  ProductVipMemberModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN


@interface ProductVipProductRemark:FetchModel

@property (nonatomic,copy)NSString *daily_speed_profit;
@property (nonatomic,copy)NSString *daily_unlock;
@property (nonatomic,copy)NSString *dividend_ratio;
@property (nonatomic,copy)NSString *month_unlock;

@end

@protocol ProductVipMemberProductsListSingleModel <NSObject>

@end

@interface ProductVipMemberProductsListSingleModel:FetchModel

@property (nonatomic,copy)NSString *product_id;
@property (nonatomic,copy)NSString *product_name;
@property (nonatomic,strong)ProductVipProductRemark *product_remark;
@property (nonatomic,assign)NSInteger product_amount;
@property (nonatomic,assign)BOOL available;
@property (nonatomic,assign)NSInteger member_level;
@property (nonatomic,assign)CGFloat price;
@property (nonatomic,copy)NSString *tag;
@property (nonatomic,copy)NSString *product_img;
@property (nonatomic,copy)NSString *member_days;


@end



#pragma mark - Main
@interface ProductVipMemberModel : FetchModel

@property (nonatomic,assign)NSInteger pool_master_level;            /**< 池主等级*/
@property (nonatomic,strong)NSArray * member_tags;                  /**< 会员标签*/
@property (nonatomic,copy)NSString * bbt_amount;
@property (nonatomic,copy)NSString *member_validity;
@property (nonatomic,copy)NSString *bp_amount;
@property (nonatomic,assign)BOOL buy_status;
@property (nonatomic,copy)NSString *invitation_code;
@property (nonatomic,assign)NSInteger invitation_total_reward;
@property (nonatomic,copy)NSString *member_days;
@property (nonatomic,copy)NSString *bp_dividend;                /**< 分红*/
@property (nonatomic,copy)NSString *bp_fixed;                   /**< 固定*/
@property (nonatomic,copy)NSString *bp_mining;                   /**< 挖矿*/


@property (nonatomic,strong)NSArray<ProductVipMemberProductsListSingleModel> *member_products;
@property (nonatomic,strong)NSArray<ProductVipMemberProductsListSingleModel> *pool_products;

@property (nonatomic,assign)NSInteger member_level;
@property (nonatomic,strong)NSArray *pool_describe;


@end






NS_ASSUME_NONNULL_END
