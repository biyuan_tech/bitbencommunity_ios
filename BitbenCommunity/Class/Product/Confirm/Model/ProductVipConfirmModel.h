//
//  ProductVipConfirmModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/21.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductVipConfirmModel : FetchModel

@property (nonatomic,copy)NSString *product_name;               /**< 商品名字*/
@property (nonatomic,assign)NSInteger buy_amount;               /**< 购买数量*/
@property (nonatomic,copy)NSString *datetime;                   /**< 购买时间*/
@property (nonatomic,copy)NSString *name;                       /**< 支付币种*/
@property (nonatomic,copy)NSString *order_id;                       /**< 订单编号*/
@property (nonatomic,copy)NSString *product_id;                       /**< 商品编号*/
@property (nonatomic,copy)NSString *value;                       /**< 商品价格*/
@property (nonatomic,copy)NSString *discount_value;                       /**< 抵消金额*/
@property (nonatomic,copy)NSString *initial_value;                       /**< 抵消金额*/


@end

NS_ASSUME_NONNULL_END
