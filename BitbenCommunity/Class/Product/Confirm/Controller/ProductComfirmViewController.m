//
//  ProductComfirmViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ProductComfirmViewController.h"
#import "ProductComfirmTitleTableViewCell.h"
#import "ProductConfirmButtonTableViewCell.h"
#import "ProductConfirmSubInfoTableViewCell.h"
#import "ProductConfirmInputTableViewCell.h"
#import "NetworkAdapter+MemberVIP.h"

@interface ProductComfirmViewController ()<UITableViewDataSource,UITableViewDelegate>{
    UITextField *inputTextField;
    WalletPwdErrorCountModel * _Nonnull pwdStatusCountModel;
}
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)PDImageView *actionBgimgView;
@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图
// 1. 订单tableview
@property (nonatomic,strong)UITableView *confirmTableView;
@property (nonatomic,strong)NSMutableArray *confirmMutableArr;
@property (nonatomic,strong)UIScrollView *mainScrollView;
// 2.支付tableview
@property (nonatomic,strong)UITableView *payPwdTableView;
@property (nonatomic,strong)NSArray *payArr;

@end

@implementation ProductComfirmViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    [self createSheetView];
    [self createShareView];
    [self arrayWithInit];
    [self createTableView];
    [self createNotifi];
}


#pragma mark - 创建sheetView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    self.actionBgimgView = [[PDImageView alloc]init];
    self.actionBgimgView.backgroundColor = [UIColor clearColor];
    self.actionBgimgView.image = self.showImgBackgroundImage;
    self.actionBgimgView.frame = self.view.bounds;
    self.actionBgimgView.alpha = 0;
    [self.view addSubview:self.actionBgimgView];
    
    // 创建背景色
    self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35f];
    [self.view addSubview:self.backgrondView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    
//    [self createSharetView];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.backgrondView addGestureRecognizer:tapGestureRecognizer];

}

#pragma mark - createShareView
-(void)createShareView{
    CGRect screenRect = [[UIScreen mainScreen]bounds];
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, screenRect.size.height, screenRect.size.width, [self calculationConfirmHeight])];
    _shareView.clipsToBounds = YES;
    _shareView.backgroundColor = UURandomColor;
    _shareView.image = self.backgroundImage;
    _shareView.userInteractionEnabled = YES;
    _shareView.contentMode = UIViewContentModeBottom;
    _shareView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_shareView];
    [self createMainScrollView];
}

-(void)arrayWithInit{
    self.confirmMutableArr = [NSMutableArray array];
    [self.confirmMutableArr addObject:@[@"&&标题&&"]];
    [self.confirmMutableArr addObject:self.transferOrderInfo];
    [self.confirmMutableArr addObject:@[@"**按钮**"]];
    
    // 支付
    self.payArr = @[@[@"&&请输入安全密码&&"],@[@"支付密码",@"密码错误"],@[@"确定"]];
}

#pragma mark - createMainScrollView
-(void)createMainScrollView{
    if (!self.mainScrollView){
        self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
            
        }];
        self.mainScrollView.scrollEnabled = NO;
        self.mainScrollView.frame = _shareView.bounds;
        self.mainScrollView.contentSize = CGSizeMake(2 * self.shareView.size_width, self.shareView.size_height);
        [self.shareView addSubview:self.mainScrollView];
    }
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.confirmTableView){
        self.confirmTableView = [GWViewTool gwCreateTableViewRect:CGRectMake(0, 0, kScreenBounds.size.width, self.shareView.size_height)];
        self.confirmTableView.dataSource = self;
        self.confirmTableView.delegate = self;
        self.confirmTableView.scrollEnabled = NO;
        [self.mainScrollView addSubview:self.confirmTableView];
    }
    
    if (!self.payPwdTableView){
        self.payPwdTableView = [GWViewTool gwCreateTableViewRect:CGRectMake(self.shareView.size_width, 0, kScreenBounds.size.width, self.shareView.size_height)];
        self.payPwdTableView.dataSource = self;
        self.payPwdTableView.delegate = self;
        self.payPwdTableView.scrollEnabled = NO;
        [self.mainScrollView addSubview:self.payPwdTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.confirmTableView){
        return self.confirmMutableArr.count;
    } else {
        return self.payArr.count;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.confirmTableView){
        NSArray *sectionOfArr = [self.confirmMutableArr objectAtIndex:section];
        return sectionOfArr.count;
    } else {
        NSArray *sectionOfArr = [self.payArr objectAtIndex:section];
        return sectionOfArr.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (tableView == self.confirmTableView){
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"&&标题&&" sourceArr:self.confirmMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"&&标题&&" sourceArr:self.confirmMutableArr]){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            ProductComfirmTitleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[ProductComfirmTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferTitle = @"币本钱包";
            __weak typeof(self)weakSelf = self;
            [cellWithRowOne actionClickWithCancelButton:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf sheetViewDismiss];
            }];
            return cellWithRowOne;
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"**按钮**" sourceArr:self.confirmMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"**按钮**" sourceArr:self.confirmMutableArr]){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            ProductConfirmButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[ProductConfirmButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            }
            __weak typeof(self)weakSelf = self;
            [cellWithRowTwo buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width, 0) animated:YES];
                
            }];
            return cellWithRowTwo;
        } else {
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            ProductConfirmSubInfoTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[ProductConfirmSubInfoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            }
            cellWithRowThr.transferTitle = [[self.transferOrderInfo objectAtIndex:indexPath.row] objectAtIndex:0];
            cellWithRowThr.transferSubTitle = [[self.transferOrderInfo objectAtIndex:indexPath.row] objectAtIndex:1];
            if (indexPath.row == self.transferOrderInfo.count - 1){
                cellWithRowThr.transferIndexRow = 999;
            } else {
                cellWithRowThr.transferIndexRow = indexPath.row;
            }
            return cellWithRowThr;
        }
    } else {
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"&&请输入安全密码&&" sourceArr:self.payArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"&&请输入安全密码&&" sourceArr:self.payArr]){
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            ProductComfirmTitleTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowFour){
                cellWithRowFour = [[ProductComfirmTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            }
            cellWithRowFour.transferCellHeight = cellHeight;
            cellWithRowFour.transferTitle = @"请输入安全密码";
            __weak typeof(self)weakSelf = self;
            [cellWithRowFour actionClickWithCancelButton:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf sheetViewDismiss];
            }];
            return cellWithRowFour;
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"支付密码" sourceArr:self.payArr]){
            if (indexPath.row == 0){
                static NSString *cellIdentifyWithRowSex = @"cellIdentifyWithRowSex";
                ProductConfirmInputTableViewCell *cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSex];
                if (!cellWithRowSex){
                    cellWithRowSex = [[ProductConfirmInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSex];
                }
                inputTextField = cellWithRowSex.inputTextField;
                return cellWithRowSex;
            } else {
                static NSString *cellIdentifyWithRowSev = @"cellIdentifyWithRowSev";
                GWNormalTableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev];
                if (!cellWithRowSev){
                    cellWithRowSev = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev];
                }
                cellWithRowSev.transferCellHeight = cellHeight;
                if (pwdStatusCountModel && pwdStatusCountModel.password_status == NO){
                    cellWithRowSev.transferTitle = [NSString stringWithFormat:@"密码错误，还有%li次机会",(long)pwdStatusCountModel.count];
                } else {
                    cellWithRowSev.transferTitle = @"";
                }
                
                cellWithRowSev.titleLabel.textColor = [UIColor redColor];
                return cellWithRowSev;
            }
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"确定" sourceArr:self.payArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"确定" sourceArr:self.payArr]){
            static NSString *cellIdentifyWithRowEig = @"cellIdentifyWithRowEig";
            ProductConfirmButtonTableViewCell *cellWithRowEig = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowEig];
            if (!cellWithRowEig){
                cellWithRowEig = [[ProductConfirmButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowEig];
            }
            __weak typeof(self)weakSelf = self;
            [cellWithRowEig buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf payManager];
            }];

            return cellWithRowEig;
        } else {
            static NSString *cellIdentifyWithRowNie = @"cellIdentifyWithRowNie";
            GWNormalTableViewCell *cellWithRowNie = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowNie];
            if (!cellWithRowNie){
                cellWithRowNie = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowNie];
            }
            return cellWithRowNie;
        }
        return nil;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    
    if (tableView == self.confirmTableView){
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"&&标题&&" sourceArr:self.confirmMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"&&标题&&" sourceArr:self.confirmMutableArr]){
            return [ProductComfirmTitleTableViewCell calculationCellHeight];
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"**按钮**" sourceArr:self.confirmMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"**按钮**" sourceArr:self.confirmMutableArr]){
            return [ProductConfirmButtonTableViewCell calculationCellHeight];
        } else {
            if (indexPath.row == self.transferOrderInfo.count - 1){
                [ProductConfirmSubInfoTableViewCell calculationCellHeightWithIndex:999];
            }
            return [ProductConfirmSubInfoTableViewCell calculationCellHeightWithIndex:indexPath.row];
        }
    } else {
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"&&请输入安全密码&&" sourceArr:self.payArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"&&请输入安全密码&&" sourceArr:self.payArr]){
            return [ProductComfirmTitleTableViewCell calculationCellHeight];
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"支付密码" sourceArr:self.payArr]){
            if (indexPath.row == 0){
                return [ProductConfirmInputTableViewCell calculationCellHeight];
            } else {
                return [GWNormalTableViewCell calculationCellHeight];
            }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"确定" sourceArr:self.payArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"确定" sourceArr:self.payArr]){
            return [ProductConfirmButtonTableViewCell calculationCellHeight];
        }
        return 44;
    }
}






#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak ProductComfirmViewController *weakVC = self;
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.actionBgimgView.alpha = 1;
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.shareView.frame = CGRectMake(0, screenHeight, weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
        self.actionBgimgView.alpha = 0;
        self.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    __weak ProductComfirmViewController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    
    [UIView animateWithDuration:.5f animations:^{
        weakVC.shareView.frame = CGRectMake(weakVC.shareView.frame.origin.x, screenHeight-_shareView.bounds.size.height-(IS_IOS7_LATER ? 0 : 20), weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
        
        self.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
        self.actionBgimgView.alpha = 1;
    } completion:^(BOOL finished) {
        self.actionBgimgView.alpha = 1;
    }];
}

#pragma mark - actionClick
-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}

-(CGFloat)calculationConfirmHeight{
    CGFloat viewHeight = 0;
    viewHeight += [ProductComfirmTitleTableViewCell calculationCellHeight];
    viewHeight += [ProductConfirmButtonTableViewCell calculationCellHeight];
    viewHeight += self.transferOrderInfo.count * [ProductConfirmSubInfoTableViewCell calculationCellHeightWithIndex:0];
    if (IS_iPhoneX){
        viewHeight += 34;
    }
    return viewHeight;
}

#pragma mark - 接口
-(void)payManager{
    __weak typeof(self)weakSelf = self;
    
    if (!inputTextField.text.length){
        [StatusBarManager statusBarHidenWithText:@"请输入密码"];
        return;
    }
    
    [[NetworkAdapter sharedAdapter] sendRequestToPayMemberProductManager:self.transferProductModel.order_id blockPassword:inputTextField.text block:^(WalletPwdErrorCountModel * _Nonnull countModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->pwdStatusCountModel = countModel;
        if (!countModel.password_status){
            NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"密码错误" sourceArr:strongSelf.payArr];
            NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"密码错误" sourceArr:strongSelf.payArr];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            [strongSelf.payPwdTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        } else {
            [strongSelf sheetViewDismiss];
            NSString *title = [NSString stringWithFormat:@"%@会员申请成功",strongSelf.transferProductModel.product_name];
            [[UIAlertView alertViewWithTitle:title message:@"USDT链上付款进行中，期间请勿重复操作！扣款成功后，相关会员权益将在次日生效！充值、提现、交易问题请加微信：bibenwangluo" buttonTitles:@[@"复制微信号",@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex == 0){
                    [Tool copyWithString:@"bibenwangluo" callback:NULL];
                    [StatusBarManager statusBarShowWithText:@"微信号复制成功"];
                }
            }]show];
        }
    }];
}




#pragma mark - 键盘通知
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.shareView.orgin_y = kScreenBounds.size.height - keyboardRect.size.height - self.shareView.size_height;
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.shareView.orgin_y = kScreenBounds.size.height - self.shareView.size_height;
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


@end




