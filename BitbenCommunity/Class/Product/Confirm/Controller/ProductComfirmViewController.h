//
//  ProductComfirmViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "ProductVipConfirmModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductComfirmViewController : AbstractViewController


// 相关属性
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势
@property (nonatomic,strong)UIImage *showImgBackgroundImage;                // 背景图片
@property (nonatomic,strong)NSArray *transferOrderInfo;
@property (nonatomic,strong)ProductVipConfirmModel * _Nonnull transferProductModel;


- (void)showInView:(UIViewController *)viewController;
- (void)dismissFromView:(UIViewController *)viewController;


@end

NS_ASSUME_NONNULL_END
