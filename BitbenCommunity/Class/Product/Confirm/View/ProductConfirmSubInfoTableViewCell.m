//
//  ProductConfirmSubInfoTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ProductConfirmSubInfoTableViewCell.h"

@interface ProductConfirmSubInfoTableViewCell()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *dymicLabel;

@end

@implementation ProductConfirmSubInfoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"656565"];
    [self.bgImgView addSubview:self.titleLabel];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"15" textColor:@"2B2B2B"];
    self.dymicLabel.textAlignment = NSTextAlignmentRight;
    [self.bgImgView addSubview:self.dymicLabel];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    // 标题
    self.titleLabel.text = self.transferTitle;
}

-(void)setTransferIndexRow:(NSInteger)transferIndexRow{
    _transferIndexRow = transferIndexRow;
    
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    self.bgImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [ProductConfirmSubInfoTableViewCell calculationCellHeightWithIndex:transferIndexRow]);
    if (self.transferIndexRow == 999){
        self.bgImgView.image = [Tool stretchImageWithName:@"bg_product_order_bottom"];
        self.titleLabel.frame = CGRectMake(LCFloat(20),([ProductConfirmSubInfoTableViewCell calculationCellHeightWithIndex:transferIndexRow] - titleSize.height) / 2., titleSize.width, titleSize.height);
    } else if (self.transferIndexRow == 0){
        self.bgImgView.image = [Tool stretchImageWithName:@"bg_product_order_top"];
        self.titleLabel.frame = CGRectMake(LCFloat(20), ([ProductConfirmSubInfoTableViewCell calculationCellHeightWithIndex:transferIndexRow] - titleSize.height) / 2., titleSize.width, titleSize.height);
    } else {
        self.bgImgView.image = [Tool stretchImageWithName:@"bg_product_order_nor"];
        self.titleLabel.frame = CGRectMake(LCFloat(20), ([ProductConfirmSubInfoTableViewCell calculationCellHeightWithIndex:transferIndexRow] - titleSize.height) / 2., titleSize.width, titleSize.height);
    }
    CGSize subTitleSize = [Tool makeSizeWithLabel:self.dymicLabel];
    self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + LCFloat(20) , self.titleLabel.orgin_y, kScreenBounds.size.width - 2 * LCFloat(15) - LCFloat(20) - CGRectGetMaxX(self.titleLabel.frame), subTitleSize.height);
}

-(void)setTransferSubTitle:(NSString *)transferSubTitle{
    _transferSubTitle = transferSubTitle;
    
    // sub标题
    self.dymicLabel.text = self.transferSubTitle;
}

+(CGFloat)calculationCellHeightWithIndex:(NSInteger)index{
    if (index == 0){
        return 40;
    } else {
        return 40;
    }
}

@end
