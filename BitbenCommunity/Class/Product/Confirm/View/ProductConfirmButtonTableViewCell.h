//
//  ProductConfirmButtonTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductConfirmButtonTableViewCell : PDBaseTableViewCell

-(void)buttonClickManager:(void(^)())block;

@end

NS_ASSUME_NONNULL_END
