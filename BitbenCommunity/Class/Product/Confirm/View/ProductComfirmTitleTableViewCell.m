//
//  ProductComfirmTitleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ProductComfirmTitleTableViewCell.h"

static char actionClickWithCancelKey;
@interface ProductComfirmTitleTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *cancelButton;
@end

@implementation ProductComfirmTitleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"3C3B3B"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cancelButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.cancelButton];
    __weak typeof(self)weakSelf = self;
    [self.cancelButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithCancelKey);
        if (block){
            block();
        }
    }];
    
    self.cancelButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(55), 0, [ProductComfirmTitleTableViewCell calculationCellHeight], [ProductComfirmTitleTableViewCell calculationCellHeight]);
    [self.cancelButton setImage:[UIImage imageNamed:@"icon_productConfirm_cancel"] forState:UIControlStateNormal];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
    self.titleLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, [ProductComfirmTitleTableViewCell calculationCellHeight]);
}

-(void)actionClickWithCancelButton:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithCancelKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}



+(CGFloat)calculationCellHeight{
    return LCFloat(55);
}

@end
