//
//  ProductConfirmInputTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/22.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductConfirmInputTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)UITextField *inputTextField;

@end

NS_ASSUME_NONNULL_END
