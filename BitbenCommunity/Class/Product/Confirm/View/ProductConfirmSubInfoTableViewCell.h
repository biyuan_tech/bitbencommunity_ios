//
//  ProductConfirmSubInfoTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/16.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductConfirmSubInfoTableViewCell : PDBaseTableViewCell

@property (nonatomic,assign)NSInteger transferIndexRow;
@property (nonatomic,copy)NSString *transferTitle;
@property (nonatomic,copy)NSString *transferSubTitle;

+(CGFloat)calculationCellHeightWithIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
