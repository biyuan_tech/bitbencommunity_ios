//
//  ProductConfirmInputTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/22.
//  Copyright © 2019 币本. All rights reserved.
//

#import "ProductConfirmInputTableViewCell.h"

@interface ProductConfirmInputTableViewCell()
@property (nonatomic,strong)UIView *infoBgView;

@end

@implementation ProductConfirmInputTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.infoBgView = [[UIView alloc]init];
    self.infoBgView.backgroundColor = [UIColor hexChangeFloat:@"F5F5F5"];
    self.infoBgView.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - 2 * LCFloat(11), [ProductConfirmInputTableViewCell calculationCellHeight]);
    self.infoBgView.layer.cornerRadius = LCFloat(4);
    self.infoBgView.clipsToBounds = YES;
    [self addSubview:self.infoBgView];
    
    self.inputTextField = [GWViewTool inputTextField];
    self.inputTextField.placeholder = @"请输入安全密码";
    self.inputTextField.secureTextEntry = YES;
    self.inputTextField.font = [UIFont fontWithCustomerSizeName:@"14"];
    self.inputTextField.frame = CGRectMake(LCFloat(11), 0, self.infoBgView.size_width - 2 * LCFloat(11), self.infoBgView.size_height);
    [self.infoBgView addSubview:self.inputTextField];
}

+(CGFloat)calculationCellHeight{
    return LCFloat(55);
}

@end
