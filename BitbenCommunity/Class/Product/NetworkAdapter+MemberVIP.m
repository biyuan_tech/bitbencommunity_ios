//
//  NetworkAdapter+MemberVIP.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "NetworkAdapter+MemberVIP.h"

@implementation NetworkAdapter (MemberVIP)

-(void)sendRequestToGetMemberCenterInfoWithBlock:(void(^)(ProductVipMemberModel *productModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:user_member_center requestParams:nil responseObjectClass:[ProductVipMemberModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ProductVipMemberModel *vipModel = (ProductVipMemberModel *)responseObject;
            if (block){
                block(vipModel);
            }
        }
    }];
}


#pragma mark - 下订单
-(void)sendRequestToMakeOrderWithProductId:(NSString *)productId amount:(NSInteger)amount block:(void(^)(ProductVipConfirmModel *productModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"product_id":productId,@"buy_amount":@(amount),@"name":@"USDT",@"from":[AccountModel sharedAccountModel].block_Address};
    [[NetworkAdapter sharedAdapter] fetchWithPath:order_member_product requestParams:params responseObjectClass:[ProductVipConfirmModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ProductVipConfirmModel *singleModel = (ProductVipConfirmModel *)responseObject;
            if (block){
                block(singleModel);
            }
        }
    }];
}


#pragma mark - 产品支付
-(void)sendRequestToPayMemberProductManager:(NSString *)orderId blockPassword:(NSString *)blockPassword block:(void(^)(WalletPwdErrorCountModel *countModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"order_id":orderId,@"block_password":blockPassword,@"from":[AccountModel sharedAccountModel].block_Address};
    [[NetworkAdapter sharedAdapter] fetchWithPath:pay_member_product requestParams:params responseObjectClass:[WalletPwdErrorCountModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        
        if (isSucceeded){
            WalletPwdErrorCountModel *pwdErrorModel = (WalletPwdErrorCountModel *)responseObject;
            if (block){
                block(pwdErrorModel);
            }
        }
    }];
}
@end
