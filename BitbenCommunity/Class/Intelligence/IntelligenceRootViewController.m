//
//  IntelligenceRootViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "IntelligenceRootViewController.h"
#import "IntelligenceRootTableViewCell.h"
#import "IntelligenceTitleTableViewCell.h"
#import "NetworkAdapter+Center.h"
#import "IntelligenceViewCalendarShowController.h"

@interface IntelligenceRootViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSString *titleDateTimeStr;
    NSTimeInterval timeInt;
}
@property (nonatomic,strong)UITableView *intelligenceTableView;
@property (nonatomic,strong)NSMutableArray *intelligenceMutableArr;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *riliButton;
@property (nonatomic,strong)UIView *bgView;

@end

@implementation IntelligenceRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createView];
    [self createTableView];
    [self sendRequestToGetInfo];
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor hexChangeFloat:@"F2F4F5"];
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(45));
    [self.view addSubview:self.bgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"323232"];
    self.titleLabel.text = titleDateTimeStr;
    self.titleLabel.frame = CGRectMake(LCFloat(15), 0, LCFloat(200), self.bgView.size_height);
    [self.view addSubview:self.titleLabel];
    
    self.riliButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.riliButton.backgroundColor = [UIColor clearColor];
    [self.riliButton setImage:[UIImage imageNamed:@"icon_calendar"] forState:UIControlStateNormal];
    self.riliButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) * 2 - LCFloat(20), 0, LCFloat(15) * 2 + LCFloat(20), self.bgView.size_height);
    [self.view addSubview:self.riliButton];
    __weak typeof(self)weakSelf = self;
    [self.riliButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf calendarShowManager];
    }];
}

-(void)calendarShowManager{
    IntelligenceViewCalendarShowController *VC = [[IntelligenceViewCalendarShowController alloc]init];
    __weak typeof(self)weakSelf = self;
    [VC actionClickWithDidSelected:^(NSDate * _Nonnull date) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->titleDateTimeStr = [NSDate getTimeWithString1:[NSDate getTimeIntWithDate:date]];
        strongSelf.titleLabel.text = strongSelf->titleDateTimeStr;
        strongSelf->timeInt = [NSDate getTimeIntWithDate:date];
        // 清空tableview数据
        strongSelf.intelligenceTableView.currentPage = 0;
        [strongSelf.intelligenceMutableArr removeAllObjects];
        [strongSelf sendRequestToGetInfo];
    }];
    
    VC.transferShowOrigin_Y = [BYTabbarViewController sharedController].navBarHeight + self.bgView.size_height;
    [VC showInView:self.parentViewController];
 
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"情报";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.intelligenceMutableArr = [NSMutableArray array];
    titleDateTimeStr = [NSDate getCurrentTime1];
    timeInt = 0;
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.intelligenceTableView){
        self.intelligenceTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.intelligenceTableView.orgin_y = CGRectGetMaxY(self.bgView.frame);
        self.intelligenceTableView.size_height -= self.bgView.size_height;
        self.intelligenceTableView.dataSource = self;
        self.intelligenceTableView.delegate = self;
        [self.view addSubview:self.intelligenceTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.intelligenceTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfo];
    }];
    
    [self.intelligenceTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfo];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.intelligenceMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    IntelligenceSubModel *singleModel = [self.intelligenceMutableArr objectAtIndex:section];
    return singleModel.sub_list.count + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        IntelligenceTitleTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[IntelligenceTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
        }
        IntelligenceSubModel *singleModel = [self.intelligenceMutableArr objectAtIndex:indexPath.section];
        cellWithRowZero.transferDateTitle = [NSDate getTimeWithString1:singleModel.information_date / 1000.];
        
        if (indexPath.section == 0){
            cellWithRowZero.hasTop = YES;
        } else {
            cellWithRowZero.hasTop = NO;
        }
        return cellWithRowZero;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        IntelligenceRootTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[IntelligenceRootTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        IntelligenceSubModel *singleModel = [self.intelligenceMutableArr objectAtIndex:indexPath.section];
        IntelligenceSubSingleModel *singleInfoModel = [singleModel.sub_list objectAtIndex:indexPath.row - 1];
        cellWithRowOne.transferSingleModel = singleInfoModel;
        return cellWithRowOne;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        return [IntelligenceTitleTableViewCell calculationCellHeight];
    } else {
        IntelligenceSubModel *singleModel = [self.intelligenceMutableArr objectAtIndex:indexPath.section];
        IntelligenceSubSingleModel *singleInfoModel = [singleModel.sub_list objectAtIndex:indexPath.row - 1];
        return [IntelligenceRootTableViewCell calculationCellHeightWithModel:singleInfoModel];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) return;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    IntelligenceSubModel *singleModel = [self.intelligenceMutableArr objectAtIndex:indexPath.section];
    IntelligenceSubSingleModel *singleInfoModel = [singleModel.sub_list objectAtIndex:indexPath.row - 1];
    [DirectManager directMaxWithModel:singleInfoModel block:NULL];
}

#pragma mark - Interface
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] getQingbaoManagerWithDate:timeInt page:self.intelligenceTableView.currentPage block:^(IntelligenceModel *model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.intelligenceTableView.isXiaLa){
            [strongSelf.intelligenceMutableArr removeAllObjects];
        }
        
        [strongSelf.intelligenceMutableArr addObjectsFromArray:model.content];
        [strongSelf.intelligenceTableView reloadData];
        
        if (strongSelf.intelligenceTableView.isXiaLa){
            [strongSelf.intelligenceTableView stopPullToRefresh];
        } else {
            [strongSelf.intelligenceTableView stopFinishScrollingRefresh];
        }
    }];
}
@end
