//
//  IntelligenceViewCalendarShowController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "IntelligenceViewCalendarShowController.h"
#import "FSCalendar.h"
#import "IntelligenceCalendarModel.h"

#define SheetViewHeight       LCFloat(334 )
static char actionClickWithDidSelectedKey;
@interface IntelligenceViewCalendarShowController ()<FSCalendarDelegate,FSCalendarDataSource>{
    NSDate *currentDate;
}

@property (nonatomic,strong)FSCalendar *calendar;
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)PDImageView *actionBgimgView;
@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图片
@property (nonatomic,strong)NSArray *shareButtonArray;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)NSMutableArray *dateMutableArr;


@end

@implementation IntelligenceViewCalendarShowController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dateMutableArr = [NSMutableArray array];
    self.view.backgroundColor = [UIColor clearColor];
    [self createSheetView];
    [self createView];
    
    // 获取当前的日期
    NSTimeInterval time = [NSDate getNSTimeIntervalWithCurrent];
    [self sendRequestToGetCalendarListManager:time];
    
}

#pragma mark - 创建sheetView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    self.actionBgimgView = [[PDImageView alloc]init];
    self.actionBgimgView.backgroundColor = [UIColor clearColor];
    self.actionBgimgView.image = self.showImgBackgroundImage;
    self.actionBgimgView.frame = self.view.bounds;
    self.actionBgimgView.alpha = 0;
    [self.view addSubview:self.actionBgimgView];
    
    // 创建背景色
    self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.3f];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sheetViewDismiss)];
    [self.backgrondView addGestureRecognizer:tapGesture];
    
    [self.view addSubview:self.backgrondView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    
    [self createSharetView];
}

#pragma mark - 创建view
-(void)createSharetView{
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, self.transferShowOrigin_Y, kScreenBounds.size.width, 0)];
    _shareView.clipsToBounds = YES;
    _shareView.image = self.backgroundImage;
    _shareView.userInteractionEnabled = YES;
    _shareView.contentMode = UIViewContentModeBottom;
    _shareView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_shareView];

    [self createView];
}




#pragma mark - 第一次进入获取本月事件
-(void)eventListWithMoon{
    NSDate *currentDate = [NSDate date];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"事件日历";
}

#pragma mark - createView
-(void)createView{
    if (!self.calendar){
        self.calendar = [[FSCalendar alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, SheetViewHeight)];
        self.calendar.backgroundColor = [UIColor whiteColor];
        self.calendar.dataSource = self;
        self.calendar.delegate = self;
        self.calendar.firstWeekday = 2;                                                             // 设置周一为第一天
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];                    // 设置为中文
        self.calendar.locale = locale;                                                              // 设置周次是中文显示
        self.calendar.appearance.weekdayTextColor = [UIColor hexChangeFloat:@"323232"];             //  周标题
        self.calendar.appearance.weekdayFont = [UIFont fontWithCustomerSizeName:@"14"];             // 设置周文字大小
        self.calendar.appearance.headerTitleColor = [UIColor darkGrayColor];
        self.calendar.appearance.headerDateFormat = @"yyyy年MM月";                                   // 设置标题日期显示类型
        self.calendar.appearance.titleDefaultColor = c4;                                            // 设置下面的文字颜色
        self.calendar.appearance.titleFont = [[UIFont fontWithCustomerSizeName:@"标题"]boldFont];
        self.calendar.appearance.eventDefaultColor = [UIColor colorWithCustomerName:@"红"];
        self.calendar.appearance.eventSelectionColor = [UIColor lightGrayColor];
        self.calendar.appearance.selectionColor = [UIColor hexChangeFloat:@"F2F4F5"];               // 设置选中时的颜色
        self.calendar.appearance.todayColor = UULightBlue;
        self.calendar.appearance.titleTodayColor = UUCloudWhite;
        self.calendar.appearance.caseOptions = FSCalendarCaseOptionsWeekdayUsesSingleUpperCase;     // 设置周次为一,二
        [self.calendar selectDate:[NSDate date]];                                                   // 设置默认选中日期是今天
        self.calendar.placeholderType = FSCalendarPlaceholderTypeNone;                              // 月份模式时，只显示当前月份
        [self.shareView addSubview:self.calendar];
    }
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar{
    NSInteger moon = calendar.currentPage.month;
    NSInteger year = calendar.currentPage.year;
    NSString *date = [NSString stringWithFormat:@"%li-%li-1",year,moon];
    NSTimeInterval dateTimeInt = [NSDate timeSwitchTimestamp:date];
    [self sendRequestToGetCalendarListManager:dateTimeInt];
}

//-(NSString *)calendar:(FSCalendar *)calendar subtitleForDate:(NSDate *)date{
//    return @"";
//}

// 设置五行显示时的calendar布局
- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date{

    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"d"];
    NSString *currentDay=[formatter stringFromDate:date];
    
    if ([self.dateMutableArr containsObject:currentDay] || [self.dateMutableArr containsObject:@([currentDay integerValue])]){
        return 1;
    } else {
        return 0;
    }
//
//    for (int i = 0 ;i < self.dateMutableArr.count;i++){
//        NSString *info = [self.dateMutableArr objectAtIndex:i];
//        if ([info isEqualToString:[NSString stringWithFormat:@"%li",calendar.currentPage.day]]){
//            return 1;
//        } else {
//            return 0;
//        }
//    }


}

-(void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition {
    currentDate = date;
    void(^block)(NSDate *date) = objc_getAssociatedObject(self, &actionClickWithDidSelectedKey);
    if(block){
        block(date);
    }
    [self sheetViewDismiss];
}









#pragma mark - actionClick
-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}

#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak IntelligenceViewCalendarShowController *weakVC = self;
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.actionBgimgView.alpha = 1;
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.shareView.frame = CGRectMake(0, self.transferShowOrigin_Y, weakVC.shareView.bounds.size.width, 0);
        self.actionBgimgView.alpha = 0;
        self.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    __weak IntelligenceViewCalendarShowController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    self.actionBgimgView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1);
    
    [UIView animateWithDuration:.5f animations:^{
        weakVC.shareView.frame = CGRectMake(0, self.transferShowOrigin_Y, weakVC.shareView.bounds.size.width, SheetViewHeight);
        
        self.actionBgimgView.layer.transform = CATransform3DMakeScale(1, 1, 1);
        self.actionBgimgView.alpha = 1;
    } completion:^(BOOL finished) {
        self.actionBgimgView.alpha = 1;
    }];
}



#pragma mark - 获取当前这个月的时间戳
-(void)sendRequestToGetCalendarListManager:(NSTimeInterval)time{
    NSInteger timeInt = (NSInteger)time * 1000;
    NSDictionary *params = @{@"information_date":@(timeInt)};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_information_date requestParams:params responseObjectClass:[IntelligenceCalendarModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            IntelligenceCalendarModel *calendarModel = (IntelligenceCalendarModel *)responseObject;
            [strongSelf.dateMutableArr removeAllObjects];
            [strongSelf.dateMutableArr addObjectsFromArray:calendarModel.days];
            [strongSelf.calendar reloadData];
        }
    }];
}

-(void)actionClickWithDidSelected:(void(^)(NSDate *date))block{
    objc_setAssociatedObject(self, &actionClickWithDidSelectedKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
