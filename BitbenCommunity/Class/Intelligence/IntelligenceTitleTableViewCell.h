//
//  IntelligenceTitleTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface IntelligenceTitleTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferDateTitle;
@property (nonatomic,assign)BOOL hasTop;
@end

NS_ASSUME_NONNULL_END
