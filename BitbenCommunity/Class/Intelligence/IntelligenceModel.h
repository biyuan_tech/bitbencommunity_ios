//
//  IntelligenceModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/14.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"
#import "BYLiveDefine.h"
NS_ASSUME_NONNULL_BEGIN

@protocol IntelligenceSubSingleModel <NSObject>


@end

@interface IntelligenceSubSingleModel : FetchModel

@property (nonatomic,copy)NSString *_id;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,assign)NSTimeInterval information_date;
@property (nonatomic,copy)NSString *information_id;
@property (nonatomic,assign)NSInteger live_type;
@property (nonatomic,copy)NSString *picture;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,copy)NSString *theme_id;
@property (nonatomic,assign)BY_THEME_TYPE theme_type;


@end

@protocol IntelligenceSubModel <NSObject>

@end

@interface IntelligenceSubModel : FetchModel
@property (nonatomic,strong)NSArray<IntelligenceSubSingleModel> *sub_list;
@property (nonatomic,assign)NSTimeInterval information_date;
@end


@interface IntelligenceModel : FetchModel

@property (nonatomic,strong)NSArray<IntelligenceSubModel> *content;

@end


NS_ASSUME_NONNULL_END
