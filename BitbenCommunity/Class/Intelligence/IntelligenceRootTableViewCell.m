//
//  IntelligenceRootTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "IntelligenceRootTableViewCell.h"

@interface IntelligenceRootTableViewCell()
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *iconImgView;
@end

@implementation IntelligenceRootTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor hexChangeFloat:@"D2D2D2"];
    [self addSubview:self.lineView];
    
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"323232"];
    self.titleLabel.numberOfLines = 0;
    [self.bgImgView addSubview:self.titleLabel];
    
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self.bgImgView addSubview:self.iconImgView];
}

-(void)setTransferSingleModel:(IntelligenceSubSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    // line
    self.lineView.frame = CGRectMake(LCFloat(21), 0, 1.f, [IntelligenceRootTableViewCell calculationCellHeightWithModel:transferSingleModel]);
    
    self.bgImgView.frame = CGRectMake(LCFloat(47), LCFloat(6), kScreenBounds.size.width - LCFloat(47) - LCFloat(15), [IntelligenceRootTableViewCell calculationCellHeightWithModel:transferSingleModel] - 2 * LCFloat(6));
    self.bgImgView.image = [Tool stretchImageWithName:@"bg_Intelligence_single_bg"];
    
    self.titleLabel.text = transferSingleModel.content;
    CGFloat width = self.bgImgView.size_width - 2 * LCFloat(17);
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(LCFloat(17), LCFloat(12), width, titleSize.height);
    
    self.iconImgView.frame = CGRectMake(LCFloat(16), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(10), LCFloat(24), LCFloat(24));
    [self.iconImgView uploadHDImageWithURL:transferSingleModel.picture callback:NULL];
}


+(CGFloat)calculationCellHeightWithModel:(IntelligenceSubSingleModel *)model{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(6);
    cellHeight += LCFloat(12);
    CGFloat width = kScreenBounds.size.width - LCFloat(47) - LCFloat(15) - 2 * LCFloat(17);
    CGSize contentSize = [model.content sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"15"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += contentSize.height;
    cellHeight += LCFloat(10);
    cellHeight += LCFloat(24);
    cellHeight += LCFloat(12);
    cellHeight += LCFloat(6);
    return cellHeight;
}
@end
