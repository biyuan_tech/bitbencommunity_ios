//
//  IntelligenceRootTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "IntelligenceModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface IntelligenceRootTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)IntelligenceSubSingleModel *transferSingleModel;

+(CGFloat)calculationCellHeightWithModel:(IntelligenceSubSingleModel *)model;

@end

NS_ASSUME_NONNULL_END
