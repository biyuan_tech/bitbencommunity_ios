//
//  IntelligenceCalendarModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/20.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface IntelligenceCalendarModel : FetchModel

@property (nonatomic,strong)NSArray *days;

@end

NS_ASSUME_NONNULL_END
