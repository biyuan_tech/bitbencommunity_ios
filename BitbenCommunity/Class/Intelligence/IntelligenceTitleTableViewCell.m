//
//  IntelligenceTitleTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/8/13.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "IntelligenceTitleTableViewCell.h"

@interface IntelligenceTitleTableViewCell()
@property (nonatomic,strong)PDImageView *calendarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *lineView;

@end

@implementation IntelligenceTitleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"323232"];
    [self addSubview:self.titleLabel];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor hexChangeFloat:@"D2D2D2"];
    self.lineView.frame = CGRectMake(LCFloat(21), CGRectGetMaxY(self.calendarImgView.frame),1.f , 40);
    [self addSubview:self.lineView];
    
    self.calendarImgView = [[PDImageView alloc]init];
    self.calendarImgView.backgroundColor = [UIColor clearColor];
    self.calendarImgView.image = [UIImage imageNamed:@"icon_intelligence_dot"];
    self.calendarImgView.frame = CGRectMake(LCFloat(15), ([IntelligenceTitleTableViewCell calculationCellHeight] - LCFloat(12)) / 2., LCFloat(12), LCFloat(12));
    [self addSubview:self.calendarImgView];
}

-(void)setTransferDateTitle:(NSString *)transferDateTitle{
    _transferDateTitle = transferDateTitle;
    self.titleLabel.text = transferDateTitle;
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake(LCFloat(48), 0, titleSize.width, [IntelligenceTitleTableViewCell calculationCellHeight]);
    
}

-(void)setHasTop:(BOOL)hasTop{
    _hasTop = hasTop;
    
    if (hasTop){
        self.lineView.frame = CGRectMake(LCFloat(21), LCFloat(22), 1.f, LCFloat(22));
    } else {
        self.lineView.frame = CGRectMake(LCFloat(21), 0, 1.f, LCFloat(44));
    }
}


+(CGFloat)calculationCellHeight{
    return LCFloat(44);
}

@end
