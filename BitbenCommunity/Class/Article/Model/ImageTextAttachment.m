//
//  ImageTextAttachment.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ImageTextAttachment.h"

@implementation ImageTextAttachment

- (CGRect)attachmentBoundsForTextContainer:(NSTextContainer *)textContainer proposedLineFragment:(CGRect)lineFrag glyphPosition:(CGPoint)position characterIndex:(NSUInteger)charIndex {
    return CGRectMake(0, 0, _imageSize.width, _imageSize.height);
    
    //    //调整图片大小
    //    return CGRectMake( 0 , 0 , [[UIScreen mainScreen] bounds].size.width , 80);
    
}
- (UIImage *)scaleImage:(UIImage *)image withSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

@end
