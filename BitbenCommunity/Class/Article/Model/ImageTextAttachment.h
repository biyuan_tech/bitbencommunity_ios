//
//  ImageTextAttachment.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageTextAttachment : NSTextAttachment
@property(strong, nonatomic) NSString *imageTag;
@property(assign, nonatomic) CGSize imageSize;  //For emoji image size

- (UIImage *)scaleImage:(UIImage *)image withSize:(CGSize)size;

@end

NS_ASSUME_NONNULL_END
