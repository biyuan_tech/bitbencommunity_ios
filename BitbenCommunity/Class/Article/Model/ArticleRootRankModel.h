//
//  ArticleRootRankModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ArticleRootRankModel <NSObject>

@end

@interface ArticleRootRankModel : FetchModel

@property (nonatomic,copy)NSString *user_id;
@property (nonatomic,copy)NSString *head_img;
@property (nonatomic,copy)NSString *nickname;
@property (nonatomic,copy)NSString *mine_fund;
@property (nonatomic,assign)NSInteger rank;
@end

@interface ArticleRootRankListModel : FetchModel

@property (nonatomic,strong)NSArray<ArticleRootRankModel > *rank_list;

@end


NS_ASSUME_NONNULL_END
