//
//  ArticleRootRankModel.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleRootRankModel.h"

@implementation ArticleRootRankModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"mine_fund":@"last_week_mine_fund"};
}

@end

@implementation ArticleRootRankListModel

@end

