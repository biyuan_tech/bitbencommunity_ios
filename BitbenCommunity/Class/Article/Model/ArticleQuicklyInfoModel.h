//
//  ArticleQuicklyInfoModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/12/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleQuicklyInfoModel : FetchModel

@property (nonatomic,assign)NSInteger cert_type;
@property (nonatomic,copy)NSString *article_id;
@end

NS_ASSUME_NONNULL_END
