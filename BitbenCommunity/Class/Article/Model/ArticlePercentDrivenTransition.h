//
//  ArticlePercentDrivenTransition.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleSubHuifuViewController.h"
@class ArticleSubHuifuViewController;
NS_ASSUME_NONNULL_BEGIN

// 文章通知
static NSString *const ArticleNotification = @"ArticleNotification";                            /**< 日记通知*/

@interface ArticlePercentDrivenTransition : UIPercentDrivenInteractiveTransition<UIViewControllerAnimatedTransitioning,UIViewControllerTransitioningDelegate,UIGestureRecognizerDelegate>
// 设置是否可以手势
@property (nonatomic, assign, getter=isDragable) BOOL dragable;
//---初始化 model视图控制器
- (id)initWithModalViewController:(UIViewController *)modalViewController;
-(instancetype)initWithModalViewController:(UIViewController *)modalViewController chiledController:(UIViewController *)controller;
@property (nonatomic,assign)CGFloat transferOrigin;
@property (nonatomic,strong)UITableView *mainTableView;

-(void)actionLoadMoreManagerBLock:(void(^)())block;
@end

NS_ASSUME_NONNULL_END
