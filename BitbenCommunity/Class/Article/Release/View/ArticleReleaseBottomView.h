//
//  ArticleReleaseBottomView.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ArticleReleaseBottomView : UIView

-(void)actionClickWithAddImgManagerBlock:(void(^)())block;
-(void)actionClickWithAddTxtManagerBlock:(void(^)())block;

@end

NS_ASSUME_NONNULL_END
