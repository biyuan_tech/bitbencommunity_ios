//
//  ArticleNewReleaseTextViewCollectionViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class ArticleNewReleaseTextViewCollectionViewCell;

@protocol ArticleNewReleaseTextViewCollectionViewCellDelegate <NSObject>

- (void)textViewCellEndEditing:(NSString *)text indexPath:(NSIndexPath *)indexPath;

- (void)textViewCellBeginEditingWithIndexPath:(NSIndexPath *)indexPath;

- (void)textViewCellIsEditing:(UITextView *)textView indexPath:(NSIndexPath *)indexPath;

@end

@interface ArticleNewReleaseTextViewCollectionViewCell : UICollectionViewCell<UITextViewDelegate>

@property (nonatomic, strong)UITextView *textView;/**< 文*/

//@property (nonatomic, strong) CAShapeLayer *borderLayer;/**< 虚线*/

@property (nonatomic, strong) id<ArticleNewReleaseTextViewCollectionViewCellDelegate>delegate;

- (void)setCellTextView:(NSIndexPath *)indexPath;

- (void)setCellTextView:(NSString *)text indexPath:(NSIndexPath *)indexPath ;

@end

NS_ASSUME_NONNULL_END
