//
//  ArticleReleaseBottomView.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleReleaseBottomView.h"

static char actionClickWithAddImgManagerBlockKey;
static char actionClickWithAddTxtManagerBlockKey;
@interface ArticleReleaseBottomView()
@property (nonatomic,strong)UIButton *addImgButton;
@property (nonatomic,strong)UIButton *addTextButton;
@end

@implementation ArticleReleaseBottomView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - 创建添加按钮
-(void)createView{
    // 1. 添加按钮
    self.addImgButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.addImgButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.addImgButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithAddImgManagerBlockKey);
        if (block){
            block();
        }
    }];
    self.addImgButton.frame = CGRectMake(0, 0, kScreenBounds.size.width / 2., LCFloat(44));
    [self.addImgButton setTitle:@"添加图片" forState:UIControlStateNormal];
    [self addSubview:self.addImgButton];
    
    // 2. 添加文字按钮
    self.addTextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.addTextButton.backgroundColor = [UIColor clearColor];
    [self.addTextButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithAddTxtManagerBlockKey);
        if (block){
            block();
        }
    }];
    self.addTextButton.frame = CGRectMake(kScreenBounds.size.width / 2., 0, kScreenBounds.size.width / 2., LCFloat(44));
    [self.addTextButton setTitle:@"添加文字" forState:UIControlStateNormal];
    [self addSubview:self.addTextButton];
}

-(void)actionClickWithAddImgManagerBlock:(void (^)())block{
    objc_setAssociatedObject(self, &actionClickWithAddImgManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithAddTxtManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithAddTxtManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


@end
