//
//  ArticleNewReleaseImgCellCollectionViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ArticleNewReleaseImgCellCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) PDImageView *imageView;/**< 图*/

- (void)setCellImage:(NSArray *)array indexPath:(NSIndexPath *)indePath;


@end

NS_ASSUME_NONNULL_END
