//
//  ArticleNewReleaseImgCellCollectionViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleNewReleaseImgCellCollectionViewCell.h"

@implementation ArticleNewReleaseImgCellCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[PDImageView alloc]init];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
        self.backgroundView = self.imageView;
    }
    return self;
}


-(void)setCellImage:(NSArray *)array indexPath:(NSIndexPath *)indePath{
    
    //    if ([[array objectAtIndex:indePath.item]isKindOfClass:[NSDictionary class]]) {
    //        NSDictionary *tampDict = [array objectAtIndex:indePath.item];
    //        NSString *URLstr = [tampDict stringValueForKey:@"imageUrl"];
    //        [_imageView sd_setImageWithURL:[NSURL URLWithString:URLstr] placeholderImage:[UIImage imageNamed:@"load_bg1.png"]];
    //        return;
    //    }
    self.imageView.image = [array objectAtIndex:indePath.item];
    
}


- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    if (highlighted) {
        self.imageView.alpha = .7f;
    }else {
        self.imageView.alpha = 1.f;
    }
}

@end
