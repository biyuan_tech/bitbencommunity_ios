//
//  CollectionViewTripletLayout.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/24.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class CollectionViewTripletLayout;

#define CollectionViewTripletLayoutStyleSquare CGSizeZero

@protocol CollectionViewDelegateTripletLayout <UICollectionViewDelegateFlowLayout>

@optional

- (CGSize)collectionView:(UICollectionView *)collectionView sizeForLargeItemsInSection:(NSInteger)section; //Default to automaticaly grow square !
- (UIEdgeInsets)insetsForCollectionView:(UICollectionView *)collectionView;
- (CGFloat)sectionSpacingForCollectionView:(UICollectionView *)collectionView;
- (CGFloat)minimumInteritemSpacingForCollectionView:(UICollectionView *)collectionView;
- (CGFloat)minimumLineSpacingForCollectionView:(UICollectionView *)collectionView;

@end


@protocol CollectionViewTripletLayoutDatasource <UICollectionViewDataSource>

@end


@interface CollectionViewTripletLayout : UICollectionViewFlowLayout


@property (nonatomic, weak) id<CollectionViewDelegateTripletLayout> delegate;
@property (nonatomic, weak) id<CollectionViewTripletLayoutDatasource> datasource;


//@property (nonatomic, assign, readonly) CGSize largeCellSize;
//@property (nonatomic, assign, readonly) CGSize smallCellSize;
@property (nonatomic, assign, readonly) CGSize largeCellSize;
@property (nonatomic, assign, readonly) CGSize smallCellSize;
@property (nonatomic, strong) NSMutableArray *largeCellSizeArray;
@property (nonatomic, strong) NSMutableArray *smallCellSizeArray;

@property (nonatomic, strong) NSMutableArray *allCellSizeArr;/**< size数组 */
@property (nonatomic, strong) NSMutableArray *allCellFrameArr;

- (CGFloat)contentHeight;

@end

NS_ASSUME_NONNULL_END
