//
//  ArticleNewReleaseTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleNewReleaseTableViewCell : PDBaseTableViewCell

@end

NS_ASSUME_NONNULL_END
