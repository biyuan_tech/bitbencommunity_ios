//
//  ArticleNewReleaseHuatiCollectionViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleRootSingleModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ArticleNewReleaseHuatiCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)ArticleRootSingleModel *transferArticleModel;

+(CGFloat)calculationCellHeight:(ArticleRootSingleModel *)transferArticleModel;


// temp 2
@property (nonatomic,strong)NSArray *transferTagsArr;

+(CGFloat)calculationCellHeightWithArr:(NSArray *)tagsArr;

-(void)actionItemClickWithBlock:(void(^)(NSString *info))block;
@end

NS_ASSUME_NONNULL_END
