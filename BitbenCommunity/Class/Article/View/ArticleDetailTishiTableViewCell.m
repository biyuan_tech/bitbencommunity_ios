//
//  ArticleDetailTishiTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/11/28.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleDetailTishiTableViewCell.h"

@interface ArticleDetailTishiTableViewCell()
@property (nonatomic,strong)UILabel *tishiLabel;
@end

@implementation ArticleDetailTishiTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.tishiLabel = [GWViewTool createLabelFont:@"13" textColor:@"AAAAAA"];
    self.tishiLabel.text = @"免责声明：本文旨在传递更多市场信息，不构成任何投资建议。文章仅代表作者观点，不代表币本官方立场。";
    self.tishiLabel.numberOfLines = 0;
    CGFloat main_width = kScreenBounds.size.width - 2 * LCFloat(21);
    CGSize tishiSize = [self.tishiLabel.text sizeWithCalcFont:self.tishiLabel.font constrainedToSize:CGSizeMake(main_width, CGFLOAT_MAX)];
    self.tishiLabel.frame = CGRectMake(LCFloat(21), LCFloat(11), main_width, tishiSize.height);
    [self addSubview:self.tishiLabel];
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    NSString *info = @"免责声明：本文旨在传递更多市场信息，不构成任何投资建议。文章仅代表作者观点，不代表币本官方立场。";
    CGFloat main_width = kScreenBounds.size.width - 2 * LCFloat(21);
    CGSize size = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"13"] constrainedToSize:CGSizeMake(main_width, CGFLOAT_MAX)];
    cellHeight += size.height;
    cellHeight += 2 * LCFloat(11);
    return cellHeight;
}

@end
