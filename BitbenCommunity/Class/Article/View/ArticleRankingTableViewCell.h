//
//  ArticleRankingTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ArticleRootRankModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ArticleRankingTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)NSArray *transferRankListArr;

-(void)actionClickRankItemsBlock:(void(^)(ArticleRootRankModel *model))block;
@end

NS_ASSUME_NONNULL_END
