//
//  ArticleNewTagsTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleNewTagsTableViewCell.h"

static char actionClickInformationWithSingleItemKey;
@interface ArticleNewTagsTableViewCell()
@property (nonatomic,strong)UIView *bgView;
@end

@implementation ArticleNewTagsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgView];
}

-(void)setTransferHuatiArr:(NSArray *)transferHuatiArr{
    _transferHuatiArr = transferHuatiArr;
    
    if (transferHuatiArr.count){
        if (self.bgView.subviews.count){
            [self.bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        }
    }
    
    CGFloat margin = LCFloat(15);
    CGFloat margin_h = LCFloat(15);
    CGFloat width = (kScreenBounds.size.width - 5 * margin) / 4.;
    CGFloat height = LCFloat(34);
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i < transferHuatiArr.count;i++){
        ArticleDetailHuatiListSingleModel *model = [self.transferHuatiArr objectAtIndex:i];
        
        CGFloat origin_x = margin + ( i % 4 ) *(width + margin);
        CGFloat origin_y = (i / 4) * (margin_h + height);
        
        ArticleNewTagsSingleTableViewItems *singleItemView = [[ArticleNewTagsSingleTableViewItems alloc] initWithFrame:CGRectMake(origin_x, origin_y, width, height) model:model actionBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(ArticleDetailHuatiListSingleModel *model) = objc_getAssociatedObject(strongSelf, &actionClickInformationWithSingleItemKey);
            if(block){
                block(model);
            }
        }];
        [self.bgView addSubview:singleItemView];
    }
    self.bgView.userInteractionEnabled = YES;
    CGFloat bgView_height =  (margin_h + height) * (transferHuatiArr.count % 4 == 0 ? transferHuatiArr.count / 4 : transferHuatiArr.count / 4 + 1);
    self.bgView.frame = CGRectMake(0, LCFloat(9), kScreenBounds.size.width, bgView_height);
}


-(void)actionItemClickBlock:(void(^)(ArticleDetailHuatiListSingleModel *model))block{
    objc_setAssociatedObject(self, &actionClickInformationWithSingleItemKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeightWithArr:(NSArray *)huatiArr{
    CGFloat cellHeight = 0;
    NSInteger numberOfRow = huatiArr.count % 4 == 0 ? huatiArr.count / 4: huatiArr.count / 4 + 1;
    cellHeight += numberOfRow * (LCFloat(16) + LCFloat(34));
    return cellHeight;
}
@end


@interface ArticleNewTagsSingleTableViewItems()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *actionButton;

@end

@implementation ArticleNewTagsSingleTableViewItems

-(instancetype)initWithFrame:(CGRect)frame model:(ArticleDetailHuatiListSingleModel *)model actionBlock:(void(^)())block{
    self = [super initWithFrame:frame];
    if (self){
        self.userInteractionEnabled = YES;
        [self createItems:model actionBlock:block];
    }
    return self;
}

-(void)createItems:(ArticleDetailHuatiListSingleModel *)model actionBlock:(void(^)())block{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.frame = self.bounds;
    [self addSubview:self.bgImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"323232"];
    self.titleLabel.text = model.content;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.frame = self.bgImgView.bounds;
    [self.bgImgView addSubview:self.titleLabel];
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.actionButton];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    self.actionButton.frame = self.bounds;
    
    if (model.isLike){
        self.bgImgView.image = [Tool stretchImageWithName:@"icon_article_tags_bg_hlt"];
        self.titleLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    } else {
        self.bgImgView.image = [Tool stretchImageWithName:@"icon_article_tags_bg_nor"];
        self.titleLabel.textColor = [UIColor colorWithCustomerName:@"B4B4B4"];
    }
}

@end
