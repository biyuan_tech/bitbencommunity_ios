//
//  ArticleNewRankingCollectionViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleNewRankingCollectionViewCell.h"

@interface ArticleNewRankingCollectionViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)PDImageView *convertImgView;
@property (nonatomic,strong)UILabel *titleLabel;

@end

@implementation ArticleNewRankingCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame = CGRectMake(0, 0, LCFloat(60), LCFloat(60));
    self.avatarImgView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarImgView.clipsToBounds = YES;
    [self addSubview:self.avatarImgView];
    
    self.convertImgView = [[PDImageView alloc]init];
    self.convertImgView.backgroundColor = [UIColor clearColor];
    self.convertImgView.frame = self.avatarImgView.bounds;
    self.convertImgView.image = [UIImage imageNamed:@"icon_article_rank_convert"];
    [self addSubview:self.convertImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"12" textColor:@"FFFFFF"];
    [self addSubview:self.titleLabel];
}

-(void)setTransferRankModel:(ArticleRootRankModel *)transferRankModel{
    _transferRankModel = transferRankModel;
    
    if ([transferRankModel.nickname isEqualToString:@"排行榜"]){
        self.avatarImgView.image = [UIImage imageNamed:transferRankModel.head_img];
        self.titleLabel.text = @"";
        self.convertImgView.hidden = YES;
    } else {
        [self.avatarImgView uploadMainImageWithURL:transferRankModel.head_img placeholder:nil imgType:PDImgTypeHD callback:NULL];
        self.titleLabel.text = transferRankModel.nickname;
        self.convertImgView.hidden = NO;
    }
    
    
    CGSize titleSize = [transferRankModel.nickname sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(self.size_width - 2 * LCFloat(6), CGFLOAT_MAX)];
    if (titleSize.height >= 2 * [NSString contentofHeightWithFont:self.titleLabel.font]){
        self.titleLabel.frame = CGRectMake(LCFloat(6), LCFloat(8), self.size_width - 2 * LCFloat(6), 2 * [NSString contentofHeightWithFont:self.titleLabel.font]);
        self.titleLabel.numberOfLines = 2;
    } else {
        self.titleLabel.frame = CGRectMake(LCFloat(6), LCFloat(8), self.size_width - 2 * LCFloat(6), 1 * [NSString contentofHeightWithFont:self.titleLabel.font]);
        self.titleLabel.numberOfLines = 1;
    }
}
@end
