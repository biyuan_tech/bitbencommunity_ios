//
//  ArticleNewDetailBottomInputView.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleNewDetailBottomInputView.h"

static char actionClickWithSendInfoBlockKey;
static char actionTextInputChangeBlockKey;
static char actionReportBlockKey;
static char actionShareBtnClciManagerBlockKey;
static char actionCommentBtnClciManagerBlockKey;
static char actionZanBtnClciManagerBlockKey;
static char actionCollectionBtnClciManagerBlockKey;
#define kInputBgTopMargin LCFloat(8)                        // 输入背景距上部&下部
#define kInputBgTopLeftMargin LCFloat(15)                   // 输入背景距左边边上
#define kInputMargin (LCFloat(35) - [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]]) / 2.       // 输入框与输入背景的高度
#define kInputLeftMargin LCFloat(15)                        // 输入框与输入背景的距左


@interface ArticleNewDetailBottomInputView(){
    CGRect originalRect;
    CGRect keyboardShowRect;
}
@property (nonatomic,strong)PDImageView *bgInputImgView;            /**< 背景图片*/
@property (nonatomic,strong)PDImageView *inputDrawImgView;          /**< 输入内容*/
@property (nonatomic,strong)UIButton *sendButton;                   /**< 发送按钮*/
@property (nonatomic,strong)UIButton *shareButton;                  /**< 分享按钮*/
@property (nonatomic,strong)UIButton *commentButton;                /**< 分享按钮*/
@property (nonatomic,strong)UIButton *upButton;                     /**< 赞按钮*/
@property (nonatomic,strong)UIButton *collectionButton;                     /**< 赞按钮*/
@end
@implementation ArticleNewDetailBottomInputView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        originalRect = frame;
        self.backgroundColor = [UIColor hexChangeFloat:@"FFFFFF"];
        [self createView];
        
        //  添加观察者，监听键盘弹出
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardWillShowNotification object:nil];
        
        //  添加观察者，监听键盘收起
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

-(void)setTransferCommentCount:(NSString *)transferCommentCount{
    _transferCommentCount = transferCommentCount;
    [self.commentButton setBadge:transferCommentCount];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - createView
-(void)createView{
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 1.);
    [self addSubview:lineView];
    
    self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sendButton.backgroundColor = [UIColor hexChangeFloat:@"424242"];
    [self.sendButton setTitle:@"发送" forState:UIControlStateNormal];
    self.sendButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"15"];
    self.sendButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - kInputSendBtnWidth, LCFloat(9), kInputSendBtnWidth, LCFloat(32));
    self.sendButton.layer.cornerRadius = self.sendButton.size_height / 2.;
    self.sendButton.clipsToBounds = YES;
    [self addSubview:self.sendButton];
    __weak typeof(self)weakSelf = self;
    [self.sendButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithSendInfoBlockKey);
        if (block){
            block();
        }
    }];
    
    self.bgInputImgView = [[PDImageView alloc]init];
    self.bgInputImgView.backgroundColor = [UIColor hexChangeFloat:@"EEEEEE"];
    self.bgInputImgView.layer.cornerRadius = LCFloat(3);
    self.bgInputImgView.clipsToBounds = YES;
    self.bgInputImgView.userInteractionEnabled = YES;
    [self addSubview:self.bgInputImgView];
    
    self.inputView = [[UITextView alloc]init];
    self.inputView.backgroundColor = [UIColor clearColor];
    self.inputView.font = [UIFont fontWithCustomerSizeName:@"13"];
    self.inputView.userInteractionEnabled = YES;
    [self.bgInputImgView addSubview:self.inputView];
    
    // 输入内容
    self.inputDrawImgView = [[PDImageView alloc]init];
    self.inputDrawImgView.backgroundColor = [UIColor clearColor];
    self.inputDrawImgView.image = [UIImage imageNamed:@"icon_article_detail_draw_input"];
    [self.bgInputImgView addSubview:self.inputDrawImgView];

    
    // 创建收藏按钮
    self.collectionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.collectionButton.backgroundColor = [UIColor clearColor];
   [self.collectionButton setImage:[UIImage imageNamed:@"icon_article_detail_bottom_collection_nor"] forState:UIControlStateNormal];
   [self.collectionButton buttonWithBlock:^(UIButton *button) {
       if (!weakSelf){
           return ;
       }
       __strong typeof(weakSelf)strongSelf = weakSelf;
       void(^block)() = objc_getAssociatedObject(strongSelf, &actionCollectionBtnClciManagerBlockKey);
       if (block){
           block();
       }
   }];
   [self addSubview:self.collectionButton];
    
    // 创建分享按钮
    self.shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.shareButton.backgroundColor = [UIColor clearColor];
    [self.shareButton setImage:[UIImage imageNamed:@"icon_article_detail_bottom_share_nor"] forState:UIControlStateNormal];
    [self.shareButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionShareBtnClciManagerBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.shareButton];

    // 创建内容按钮
    self.commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.commentButton.backgroundColor = [UIColor clearColor];
    [self.commentButton setImage:[UIImage imageNamed:@"icon_article_detail_bottom_comment_nor"] forState:UIControlStateNormal];
    [self.commentButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionCommentBtnClciManagerBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.commentButton];
    
    // 创建点赞按钮
    self.upButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.upButton.backgroundColor = [UIColor clearColor];
    [self.upButton setImage:[UIImage imageNamed:@"icon_article_detail_bottom_zan_nor"] forState:UIControlStateNormal];
    [self.upButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionZanBtnClciManagerBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.upButton];
    
    [self.inputView textViewDidChangeWithBlock:^(NSInteger currentCount) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (currentCount > 0){
            self.inputDrawImgView.hidden = YES;
        } else {
            self.inputDrawImgView.hidden = NO;
        }
        
        if (currentCount>= 140){
            strongSelf.inputView.text = [strongSelf.inputView.text substringToIndex:140];
        }
        [strongSelf calculationTextViewHeight];
    }];
    
    [self.inputView textViewSendActionBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithSendInfoBlockKey);
        if (block){
            block();
        }
    }];
    
    [self frameInfoMangerWithStatus:NO];
}


-(void)frameInfoMangerWithStatus:(BOOL)keyStatus{
    if (keyStatus == YES){              // 表示键盘弹起
        self.sendButton.hidden = NO;
        self.shareButton.hidden = YES;
        self.commentButton.hidden = YES;
        self.collectionButton.hidden = YES;
        self.upButton.hidden = YES;
        self.bgInputImgView.frame = CGRectMake(LCFloat(15), LCFloat(7), self.sendButton.orgin_x - LCFloat(14) - LCFloat(15), 2 * LCFloat(9) + [NSString contentofHeightWithFont:self.inputView.font]);
        self.inputView.frame = CGRectMake(LCFloat(3), 0, self.bgInputImgView.size_width - 2 * LCFloat(3), self.bgInputImgView.size_height);
    } else {                            // 表示键盘收拢
        self.sendButton.hidden = YES;
        self.shareButton.hidden = NO;
        self.commentButton.hidden = NO;
        self.upButton.hidden = NO;
        self.collectionButton.hidden = NO;
        
        CGFloat comment_height =  2 * LCFloat(9) + [NSString contentofHeightWithFont:self.inputView.font];
        self.collectionButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(30), 0, LCFloat(30), comment_height);
        self.upButton.frame = CGRectMake(self.collectionButton.orgin_x - LCFloat(30), 0, LCFloat(30), comment_height);
        self.commentButton.frame = CGRectMake(self.upButton.orgin_x - LCFloat(30), 0, LCFloat(30), comment_height);
        self.shareButton.frame = CGRectMake(self.commentButton.orgin_x - LCFloat(30), 0, LCFloat(30), comment_height);
        
        
        self.bgInputImgView.frame = CGRectMake(LCFloat(15), LCFloat(7), self.shareButton.orgin_x - LCFloat(7) * 2,comment_height);
        self.inputDrawImgView.frame = CGRectMake(LCFloat(12), self.bgInputImgView.center_y - LCFloat(9), LCFloat(16), LCFloat(18));
        
        self.inputView.frame = CGRectMake(LCFloat(3), 0, self.bgInputImgView.size_width - 2 * LCFloat(3) , self.bgInputImgView.size_height);
        self.inputDrawImgView.center_y = self.inputView.center_y;
        
        self.upButton.center_y = self.bgInputImgView.center_y;
        self.commentButton.center_y = self.bgInputImgView.center_y;
        self.shareButton.center_y = self.bgInputImgView.center_y;
        self.collectionButton.center_y = self.bgInputImgView.center_y;
        
        self.inputView.placeholder = @"写评论";
        self.inputView.placeholderLabel.orgin_x = CGRectGetMaxX(self.inputDrawImgView.frame) + LCFloat(4);
        self.inputView.limitMax = 300;
    }
    
    if (self.inputView.text.length){
        self.inputDrawImgView.hidden = YES;
    } else {
        self.inputDrawImgView.hidden = NO;
    }
}


-(void)setTransferListModel:(LiveFourumRootListSingleModel *)transferListModel{
    _transferListModel = transferListModel;
    
    self.inputView.placeholder = [NSString stringWithFormat:@"回复%@:",transferListModel.reply_comment_name];
}

-(void)actionClickWithSendInfoBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithSendInfoBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionReportBlock:(void (^)())block{
    objc_setAssociatedObject(self, &actionReportBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}

-(void)actionTextInputChangeBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionTextInputChangeBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionShareBtnClciManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionShareBtnClciManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionCommentBtnClciManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionCommentBtnClciManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionZanBtnClciManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionZanBtnClciManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionCollectionBtnClciManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionCollectionBtnClciManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)zanBtnStatus:(BOOL)status{
    [Tool clickZanWithView:self.upButton block:^{
        if (status){
            [self.upButton setImage:[UIImage imageNamed:@"icon_article_detail_tags_zan_hlt"] forState:UIControlStateNormal];
        } else {
            [self.upButton setImage:[UIImage imageNamed:@"icon_article_detail_bottom_zan_nor"] forState:UIControlStateNormal];
        }
    }];
}

-(void)collectionStatus:(BOOL)status{
    [Tool clickZanWithView:self.collectionButton block:^{
        if (status){
            [self.collectionButton setImage:[UIImage imageNamed:@"icon_article_detail_bottom_collection_hlt"] forState:UIControlStateNormal];
        } else {
            [self.collectionButton setImage:[UIImage imageNamed:@"icon_article_detail_bottom_collection_nor"] forState:UIControlStateNormal];
        }
    }];
}

#pragma mark - 计算文字高度
+(CGFloat)calculationHeight:(NSString *)inputText{
    // 2. 获取输入内容的高度
    CGSize fontSize = [ArticleNewDetailBottomInputView inputStrSize:inputText];
    
    // 3. 获取输入内容的高度
    CGFloat cellHeight = 0;
    cellHeight += kInputBgTopMargin * 2;            // 输入背景距上部
    cellHeight += kInputMargin * 2;                 // 输入框
    cellHeight += MAX(fontSize.height,[NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]]);
    
    // 如果是iphoneX 就加入底部的段落。否则就不加
    if (IS_PhoneXAll){
        cellHeight += LCFloat(20);
    }
    
    return cellHeight;
}

+(CGSize)inputStrSize:(NSString *)inputText{
    if ([inputText isEqualToString:@""]){
        inputText = @"1";
    }
    
    // 1. 计算输入框实际宽度
    CGFloat inputWidth = kScreenBounds.size.width;
    inputWidth -= kInputBgTopLeftMargin - kInputSendBtnWidth - kInputBgTopLeftMargin * 2;
    inputWidth -= kInputLeftMargin * 2;
    
    // 2. 获取输入内容的高度
    CGSize fontSize = [inputText sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"13"] constrainedToSize:CGSizeMake(inputWidth, CGFLOAT_MAX)];
    
    // 2.1 限制输入内容的高度，最高不超过3行
    CGFloat masonrySizeHeight = MIN(3 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]], fontSize.height);
    
    CGSize newSize = CGSizeMake(fontSize.width, masonrySizeHeight);
    return newSize;
}

-(void)calculationTextViewHeight{
    // 1. 进行block 输出
    void(^block)() = objc_getAssociatedObject(self, &actionTextInputChangeBlockKey);
    if (block){
        block();
    }
    
    // 2. 计算位置
    // 2.1 计算输入内容高度
    CGFloat inputSizeHeight = [ArticleNewDetailBottomInputView inputStrSize:self.inputView.text].height;
    
    // 2. 计算 输入框背景内容
    self.bgInputImgView.frame = CGRectMake(kInputLeftMargin, kInputBgTopMargin, self.bgInputImgView.size_width,inputSizeHeight + 2 * kInputMargin);
    
    self.inputView.size_height = self.bgInputImgView.size_height;
    
    // 3. 计算位置
    self.size_height = self.bgInputImgView.size_height + 2 * kInputBgTopMargin;
    
    self.orgin_y = keyboardShowRect.origin.y - self.size_height;
    
    // 如果是iphoneX 就加入底部的段落。否则就不加
    if (IS_PhoneXAll){
        self.size_height += LCFloat(20);
    }
}


-(void)releaseKeyboard{
    if (self.inputView.isFirstResponder){
        [self.inputView resignFirstResponder];
    }
}

-(void)cleanKeyboard{
    self.inputView.text = @"";
    self.inputView.placeholder = @"写评论";
    [self releaseKeyboard];
}


- (void)keyBoardDidShow:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.superview convertRect:keyboardRect fromView:nil];
    keyboardShowRect = keyboardRect;
    
    if (IS_PhoneXAll){
        self.orgin_y = keyboardRect.origin.y - self.size_height + LCFloat(20);
    } else {
        self.orgin_y = keyboardRect.origin.y - self.size_height;
    }
    [self frameInfoMangerWithStatus:YES];
    
}

- (void)keyBoardDidHide:(NSNotification *)notification {
    self.frame = CGRectMake(0, originalRect.origin.y + originalRect.size.height - self.size_height, kScreenBounds.size.width, self.size_height);
    [self frameInfoMangerWithStatus:NO];
}

@end
