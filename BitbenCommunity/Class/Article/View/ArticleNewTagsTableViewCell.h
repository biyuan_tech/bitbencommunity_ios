//
//  ArticleNewTagsTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ArticleDetailHuatiListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleNewTagsTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)NSArray *transferHuatiArr;
+(CGFloat)calculationCellHeightWithArr:(NSArray *)huatiArr;
-(void)actionItemClickBlock:(void(^)(ArticleDetailHuatiListSingleModel *model))block;
@end

@interface ArticleNewTagsSingleTableViewItems : UIView
-(instancetype)initWithFrame:(CGRect)frame model:(ArticleDetailHuatiListSingleModel *)model actionBlock:(void(^)())block;

@end

NS_ASSUME_NONNULL_END
