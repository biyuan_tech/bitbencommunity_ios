//
//  ArticleNewDetailBottomInputView.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/19.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveFourumRootListModel.h"

#define kInputSendBtnWidth LCFloat(63)                              // 输入框按钮

NS_ASSUME_NONNULL_BEGIN

@interface ArticleNewDetailBottomInputView : UIView

@property (nonatomic,strong)LiveFourumRootListSingleModel *transferListModel;
@property (nonatomic,strong)UITextView *inputView;                      // 输入框

+(CGFloat)calculationHeight:(NSString *)inputText;
+(CGSize)inputStrSize:(NSString *)inputText;                            // 计算输入内容的高度

-(void)actionClickWithSendInfoBlock:(void(^)())block;                   // 点击发送按钮
-(void)actionTextInputChangeBlock:(void(^)())block;                     // 内容变更信息
-(void)actionShareBtnClciManagerBlock:(void(^)())block;                 // 分享按钮
-(void)actionCommentBtnClciManagerBlock:(void(^)())block;               // 评论按钮
-(void)actionZanBtnClciManagerBlock:(void(^)())block;                   // 赞按钮
-(void)actionCollectionBtnClciManagerBlock:(void(^)())block;                   // 收藏
-(void)zanBtnStatus:(BOOL)status;
-(void)collectionStatus:(BOOL)status;
@property (nonatomic,copy)NSString *transferCommentCount;


-(void)releaseKeyboard;                                                // 释放键盘
-(void)cleanKeyboard;                                                  // 释放键盘并且清空文字

@end

NS_ASSUME_NONNULL_END
