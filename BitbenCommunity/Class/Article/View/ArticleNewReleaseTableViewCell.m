//
//  ArticleNewReleaseTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleNewReleaseTableViewCell.h"

@interface ArticleNewReleaseTableViewCell()<UITextViewDelegate>
@property (nonatomic,strong)UITextView *inputTextView;                               // 输入框
@property (nonatomic,strong) NSMutableAttributedString * locationStr;                //
@property (nonatomic,assign) NSUInteger location;                                    // 纪录变化的起始位置

@end

@implementation ArticleNewReleaseTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.inputTextView = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(200))];
    self.inputTextView.placeholder = @"请输入内容";
    self.inputTextView.delegate = self;
    self.inputTextView.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.inputTextView.editable = YES;
    [self addSubview:self.inputTextView];
    
     NSRange wholeRange = NSMakeRange(0, self.inputTextView.textStorage.length);
    [self.inputTextView.textStorage removeAttribute:NSFontAttributeName range:wholeRange];
    [self.inputTextView.textStorage removeAttribute:NSForegroundColorAttributeName range:wholeRange];
    //字体颜色
    [self.inputTextView.textStorage addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:wholeRange];
    //字体
    [self.inputTextView.textStorage addAttribute:NSFontAttributeName value:[UIFont fontWithCustomerSizeName:@"小正文"] range:wholeRange];
    
    
}



@end
