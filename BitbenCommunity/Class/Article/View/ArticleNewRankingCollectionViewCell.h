//
//  ArticleNewRankingCollectionViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleRootRankModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleNewRankingCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)ArticleRootRankModel *transferRankModel;

@end

NS_ASSUME_NONNULL_END
