//
//  ArticleNewReleaseHuatiCollectionViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/27.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleNewReleaseHuatiCollectionViewCell.h"
#import "ArticleTagsView.h"

static char actionItemClickWithBlockKey;
@interface ArticleNewReleaseHuatiCollectionViewCell()

@property (nonatomic,strong)ArticleTagsView *huatiDymicView;

@end

@implementation ArticleNewReleaseHuatiCollectionViewCell

-(instancetype )initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;

}

#pragma mark - createView
-(void)createView{
    self.huatiDymicView = [[ArticleTagsView alloc]initWithFrame:CGRectMake( LCFloat(20), 0, kScreenBounds.size.width - 2 * LCFloat(20),[ArticleTagsView calculationCellHeightWithArr:@[@""] hasEdit:NO])];
    [self addSubview:self.huatiDymicView];
    __weak typeof(self)weakSelf = self;
    [self.huatiDymicView actionClickWithTagsBlock:^(NSString *info) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSString *itemInfo) = objc_getAssociatedObject(strongSelf, &actionItemClickWithBlockKey);
        if (block){
            block(info);
        }
    }];
}

-(void)setTransferArticleModel:(ArticleRootSingleModel *)transferArticleModel{
    _transferArticleModel = transferArticleModel;
    self.huatiDymicView.hasEdit = NO;
    if (![self.huatiDymicView.transferArr containsObject:[transferArticleModel.topic_content firstObject]]){
        self.huatiDymicView.transferArr = transferArticleModel.topic_content;
    }
}

+(CGFloat)calculationCellHeight:(ArticleRootSingleModel *)transferArticleModel{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(7);
    cellHeight += [ArticleTagsView calculationCellHeightWithArr:transferArticleModel.topic_content hasEdit:NO];
    cellHeight += LCFloat(7);
    return cellHeight;
}

-(void)setTransferTagsArr:(NSArray *)transferTagsArr {
    _transferTagsArr = transferTagsArr;
    self.huatiDymicView.hasEdit = YES;
    self.huatiDymicView.transferArr = transferTagsArr;
    self.huatiDymicView.orgin_x = LCFloat(11);
    self.huatiDymicView.size_height = [ArticleTagsView calculationCellHeightWithArr:transferTagsArr hasEdit:NO];
    
}

-(void)actionItemClickWithBlock:(void(^)(NSString *info))block{
    objc_setAssociatedObject(self, &actionItemClickWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeightWithArr:(NSArray *)tagsArr{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(7);
    cellHeight += [ArticleTagsView calculationCellHeightWithArr:tagsArr hasEdit:NO];
    cellHeight += LCFloat(7);
    return cellHeight;
}
@end
