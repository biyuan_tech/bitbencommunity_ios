//
//  ArticleRankingTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleRankingTableViewCell.h"
#import "ArticleNewRankingCollectionViewCell.h"

static char actionClickRankItemsBlockKey;
@interface ArticleRankingTableViewCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)UICollectionView *ablumCollectionView;
@property (nonatomic,strong)NSMutableArray *rankMutableArr;

@end

@implementation ArticleRankingTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.rankMutableArr = [NSMutableArray array];
        [self createView];
    }
    return self;
}

-(void)setTransferRankListArr:(NSArray *)transferRankListArr{
    _transferRankListArr = transferRankListArr;
    [self.rankMutableArr removeAllObjects];
    ArticleRootRankModel *articleRankModel = [[ArticleRootRankModel alloc]init];
    articleRankModel.nickname = @"排行榜";
    articleRankModel.head_img = @"icon_article_root_rank_btn";

    [self.rankMutableArr addObject:articleRankModel];
    [self.rankMutableArr addObjectsFromArray:transferRankListArr];
    [self.ablumCollectionView reloadData];
}

#pragma mark - createView
-(void)createView{
    if (!self.ablumCollectionView){
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = CGSizeMake(LCFloat(60), LCFloat(60));
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 2;

        
        self.ablumCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, LCFloat(12), kScreenBounds.size.width, LCFloat(60)) collectionViewLayout:layout];
        self.ablumCollectionView.backgroundColor = [UIColor clearColor];
        self.ablumCollectionView.showsVerticalScrollIndicator = NO;
        [self.ablumCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
        self.ablumCollectionView.delegate = self;
        self.ablumCollectionView.dataSource = self;
        self.ablumCollectionView.showsHorizontalScrollIndicator = NO;
        self.ablumCollectionView.scrollsToTop = YES;
        [self addSubview:self.ablumCollectionView];
        
        [self.ablumCollectionView registerClass:[ArticleNewRankingCollectionViewCell class] forCellWithReuseIdentifier:@"GWAblumCollectionViewCell"];
    }
}

#pragma mark - CollectionDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.rankMutableArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ArticleNewRankingCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GWAblumCollectionViewCell" forIndexPath:indexPath];
    ArticleRootRankModel *model = [self.rankMutableArr objectAtIndex:indexPath.row];
    cell.transferRankModel = model;
    return cell;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 0;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(0, LCFloat(15), 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat cellHeight = LCFloat(60);
    return CGSizeMake(cellHeight , cellHeight);
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    return nil;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    return CGSizeZero;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    
    return CGSizeZero;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ArticleRootRankModel *model = [self.rankMutableArr objectAtIndex:indexPath.row];
    void(^block)(ArticleRootRankModel *model) = objc_getAssociatedObject(self, &actionClickRankItemsBlockKey);
    if (block){
        block(model);
    }
}

-(void)actionClickRankItemsBlock:(void(^)(ArticleRootRankModel *model))block{
    objc_setAssociatedObject(self, &actionClickRankItemsBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


@end
