//
//  ArticleInfoListViewController.m
//  
//
//  Created by 裴烨烽 on 2019/12/31.
//

#import "ArticleInfoListViewController.h"
#import "ArticleInfoListSingleTableViewCell.h"
#import <MJRefresh.h>

@interface ArticleInfoListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *articleInfoTableView;
@property (nonatomic,strong)NSMutableArray *articleMutableArr;

@end

@implementation ArticleInfoListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfoManager];
    
}

- (void)autoReload{
    [self.articleInfoTableView.mj_header beginRefreshing];
//    [self reloadData];
}

// 界面刷新接口
- (void)reloadData{
    [self sendRequestToGetInfoManager];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"资讯";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.articleMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.articleInfoTableView){
        self.articleInfoTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.articleInfoTableView.dataSource =self;
        self.articleInfoTableView.delegate = self;
        [self.view addSubview:self.articleInfoTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.articleInfoTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoManager];
    }];
    [self.articleInfoTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoManager];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.articleMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    ArticleInfoListSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[ArticleInfoListSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    ArticleRootSingleModel *listModel = [self.articleMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferModel  = listModel;
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ArticleRootSingleModel *listModel = [self.articleMutableArr objectAtIndex:indexPath.row];
    if (listModel.is_hot){
        return [ArticleInfoListSingleTableViewCell calculationCellHeightWithType:AtricleInfoListSingleCellType2];
    } else {
        return [ArticleInfoListSingleTableViewCell calculationCellHeightWithType:AtricleInfoListSingleCellType1];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ArticleRootSingleModel *listModel = [self.articleMutableArr objectAtIndex:indexPath.row];
    ArticleDetailRootViewController *viewController = [[ArticleDetailRootViewController alloc]init];
    viewController.transferPageType = ArticleDetailRootViewControllerTypeInfomation;
    viewController.transferArticleId = listModel._id;
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.articleInfoTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.articleMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.articleMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"ranking"];
    }
}

-(void)sendRequestToGetInfoManager{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"type":@"3",@"page_size":@"10",@"page_number":@(self.articleInfoTableView.currentPage)};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:@"article/page_index_article" requestParams:params responseObjectClass:[ArticleRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        ArticleRootListModel *listModel = (ArticleRootListModel *)responseObject;
        if (strongSelf.articleInfoTableView.isXiaLa){
            [strongSelf.articleMutableArr removeAllObjects];
        }
        [strongSelf.articleMutableArr addObjectsFromArray:listModel.content];
        [strongSelf.articleInfoTableView reloadData];

        [strongSelf.articleInfoTableView stopPullToRefresh];
        [strongSelf.articleInfoTableView stopFinishScrollingRefresh];

        if (strongSelf.articleMutableArr.count){
            [strongSelf.articleInfoTableView dismissPrompt];
        } else {
            [strongSelf.articleInfoTableView showPrompt:@"当前没有资讯信息" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
        }
    }];
}
@end


