//
//  ArticleInfoListViewController.h
//  
//
//  Created by 裴烨烽 on 2019/12/31.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleInfoListViewController : AbstractViewController

- (void)autoReload;
@end

NS_ASSUME_NONNULL_END
