//
//  ArticleInfoListSingleTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/12/31.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleInfoListSingleTableViewCell.h"

@interface ArticleInfoListSingleTableViewCell()
@property (nonatomic,strong)UILabel *titlelabel;
@property (nonatomic,strong)UILabel *sinceLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)PDImageView *ablumImg;
@property (nonatomic,strong)PDImageView *convertImgView;
@property (nonatomic,assign)AtricleInfoListSingleCellType transferCellType;

@end

@implementation ArticleInfoListSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.ablumImg = [[PDImageView alloc]init];
    self.ablumImg.backgroundColor = [UIColor clearColor];
    [self addSubview:self.ablumImg];
    
    self.convertImgView = [[PDImageView alloc]init];
    self.convertImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.convertImgView];
    
    self.titlelabel = [GWViewTool createLabelFont:@"16" textColor:@"黑"];
    [self addSubview:self.titlelabel];
    
    self.timeLabel = [GWViewTool createLabelFont:@"11" textColor:@"8F8F8F"];
    [self addSubview:self.timeLabel];
    
    self.sinceLabel = [GWViewTool createLabelFont:@"11" textColor:@"8F8F8F"];
    [self addSubview:self.sinceLabel];
}

-(void)setTransferModel:(ArticleRootSingleModel *)transferModel{
    _transferModel = transferModel;
    
    self.titlelabel.text = transferModel.title;
    self.sinceLabel.text = transferModel.nickname;
    self.timeLabel.text = [NSString stringWithFormat:@"· %@",[NSDate getTimeGap:transferModel.create_time / 1000.]];
    NSString *ablumUrl = @"";
    if (transferModel.picture.count){
        ablumUrl = [transferModel.picture firstObject];
    }
    if (transferModel.is_hot){
        self.transferCellType = AtricleInfoListSingleCellType2;
    } else {
        self.transferCellType = AtricleInfoListSingleCellType1;
    }
    
    if (self.transferCellType == AtricleInfoListSingleCellType1){
        self.titlelabel.textColor = [UIColor colorWithCustomerName:@"黑"];
        self.sinceLabel.textColor = [UIColor colorWithCustomerName:@"8F8F8F"];
        self.timeLabel.textColor = [UIColor colorWithCustomerName:@"8F8F8F"];
        
        CGFloat imgWidth =  0;
        // 如果没有图片
        if (!ablumUrl.length || [ablumUrl hasSuffix:@"gif"]){
            imgWidth = 0;
        } else {
            imgWidth = LCFloat(120);
        }
        
        CGFloat titleWidth = kScreenBounds.size.width - imgWidth - 3 * LCFloat(15) ;
        // title
        CGSize titleSize = [self.titlelabel.text sizeWithCalcFont:self.titlelabel.font constrainedToSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
        if (titleSize.height >= 2 *  [NSString contentofHeightWithFont:self.titlelabel.font]){
            self.titlelabel.frame = CGRectMake(LCFloat(15), LCFloat(15), titleWidth, MIN(titleSize.height, 2 * [NSString contentofHeightWithFont:self.titlelabel.font]));
            self.titlelabel.numberOfLines = 2;
        } else {
            self.titlelabel.frame = CGRectMake(LCFloat(15), LCFloat(15), titleWidth, MIN(titleSize.height, 1 * [NSString contentofHeightWithFont:self.titlelabel.font]));
            self.titlelabel.numberOfLines = 1;
        }

        CGSize sinceSize = [Tool makeSizeWithLabel:self.sinceLabel];
        self.sinceLabel.frame = CGRectMake(LCFloat(15), [ArticleInfoListSingleTableViewCell calculationCellHeightWithType:self.transferCellType] - LCFloat(15) - sinceSize.height, sinceSize.width, sinceSize.height);
        
        CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
        self.timeLabel.frame = CGRectMake(CGRectGetMaxX(self.sinceLabel.frame), self.sinceLabel.orgin_y, timeSize.width, timeSize.height);
        
        self.ablumImg.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - imgWidth, self.titlelabel.orgin_y, imgWidth, LCFloat(80));
        __weak typeof(self)weakSelf = self;
        [self.ablumImg uploadHDImageWithURL:ablumUrl callback:^(UIImage *image) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            
            if (!image){
                CGFloat imgWidth = 0;
                CGFloat titleWidth = kScreenBounds.size.width - imgWidth - 3 * LCFloat(15) ;
                // title
                CGSize titleSize = [strongSelf.titlelabel.text sizeWithCalcFont:strongSelf.titlelabel.font constrainedToSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
                if (titleSize.height >= 2 *  [NSString contentofHeightWithFont:strongSelf.titlelabel.font]){
                    strongSelf.titlelabel.frame = CGRectMake(LCFloat(15), LCFloat(15), titleWidth, MIN(titleSize.height, 2 * [NSString contentofHeightWithFont:strongSelf.titlelabel.font]));
                    strongSelf.titlelabel.numberOfLines = 2;
                } else {
                    strongSelf.titlelabel.frame = CGRectMake(LCFloat(15), LCFloat(15), titleWidth, MIN(titleSize.height, 1 * [NSString contentofHeightWithFont:strongSelf.titlelabel.font]));
                    strongSelf.titlelabel.numberOfLines = 1;
                }

                CGSize sinceSize = [Tool makeSizeWithLabel:strongSelf.sinceLabel];
                strongSelf.sinceLabel.frame = CGRectMake(LCFloat(15), [ArticleInfoListSingleTableViewCell calculationCellHeightWithType:strongSelf.transferCellType] - LCFloat(15) - sinceSize.height, sinceSize.width, sinceSize.height);
                           
                CGSize timeSize = [Tool makeSizeWithLabel:strongSelf.timeLabel];
                self.timeLabel.frame = CGRectMake(CGRectGetMaxX(strongSelf.sinceLabel.frame), strongSelf.sinceLabel.orgin_y, timeSize.width, timeSize.height);
                           
                self.ablumImg.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - imgWidth, strongSelf.titlelabel.orgin_y, imgWidth, LCFloat(80));
            }
        }];
    } else if (self.transferCellType == AtricleInfoListSingleCellType2){
        CGFloat imgWidth = kScreenBounds.size.width - 2 * LCFloat(15);
        CGFloat titleWidth = imgWidth - 2 * LCFloat(15);
        
        self.ablumImg.frame = CGRectMake(LCFloat(15), LCFloat(15), imgWidth, LCFloat(180));
        self.titlelabel.textColor = [UIColor whiteColor];
        self.sinceLabel.textColor = [UIColor whiteColor];
        self.timeLabel.textColor = [UIColor whiteColor];
        CGSize titleSize = [self.titlelabel.text sizeWithCalcFont:self.titlelabel.font constrainedToSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
        self.titlelabel.frame = CGRectMake(self.ablumImg.orgin_x + LCFloat(15), CGRectGetMaxY(self.ablumImg.frame) - LCFloat(10) - MIN(2 * [NSString contentofHeightWithFont:self.titlelabel.font], titleSize.height), titleWidth, MIN(2 * [NSString contentofHeightWithFont:self.titlelabel.font], titleSize.height));
        
        CGSize sinceSize = [Tool makeSizeWithLabel:self.sinceLabel];
        self.sinceLabel.frame = CGRectMake(self.titlelabel.orgin_x, self.titlelabel.orgin_y - LCFloat(8) - sinceSize.height, sinceSize.width, sinceSize.height);
        
        CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
        self.timeLabel.frame = CGRectMake(CGRectGetMaxX(self.sinceLabel.frame), self.sinceLabel.orgin_y, timeSize.width, timeSize.height);
        
        [self.ablumImg uploadHDImageWithURL:ablumUrl callback:NULL];
    }
    self.convertImgView.frame = self.ablumImg.frame;
    self.convertImgView.image = [Tool stretchImageWithName:@"icon_message_live_img_convert"];
}


+(CGFloat)calculationCellHeightWithType:(AtricleInfoListSingleCellType)transferCellType{
    if(transferCellType == AtricleInfoListSingleCellType1){
        return LCFloat(80) + 2 * LCFloat(12);
    } else {
        return LCFloat(180) + 2 * LCFloat(15);
    }
}
@end
