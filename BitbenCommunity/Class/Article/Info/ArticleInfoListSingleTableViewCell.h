//
//  ArticleInfoListSingleTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/12/31.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ArticleRootSingleModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,AtricleInfoListSingleCellType) {
    AtricleInfoListSingleCellType1,
    AtricleInfoListSingleCellType2,
};

@interface ArticleInfoListSingleTableViewCell : PDBaseTableViewCell
@property (nonatomic,strong)ArticleRootSingleModel *transferModel;

+(CGFloat)calculationCellHeightWithType:(AtricleInfoListSingleCellType)transferCellType;;
@end

NS_ASSUME_NONNULL_END
