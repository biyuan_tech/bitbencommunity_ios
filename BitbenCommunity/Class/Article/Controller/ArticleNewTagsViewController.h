//
//  ArticleNewTagsViewController.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleNewTagsViewController : AbstractViewController

@property (nonatomic,strong)NSArray *transferSelectedHuatiArr;

@end

NS_ASSUME_NONNULL_END
