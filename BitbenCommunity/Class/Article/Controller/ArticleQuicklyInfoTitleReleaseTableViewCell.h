//
//  ArticleQuicklyInfoTitleReleaseTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/12/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleQuicklyInfoTitleReleaseTableViewCell : PDBaseTableViewCell
@property (nonatomic,strong) UITextView *inputTextView;

+(CGFloat)calculationCellHeight;

-(void)textViewDidPasteChangeWithBlock:(void(^)(NSInteger currentCount))block;

@end

NS_ASSUME_NONNULL_END
