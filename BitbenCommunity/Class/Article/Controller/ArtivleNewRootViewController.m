//
//  ArtivleNewRootViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArtivleNewRootViewController.h"
#import "ArticleNewTagsViewController.h"
#import "ArticleNewReleaseViewController.h"
#import "ArticleNewRootSubViewController.h"

@interface ArtivleNewRootViewController ()<HTHorizontalSelectionListDelegate,HTHorizontalSelectionListDataSource>
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)NSArray *segmentArr;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)ArticleNewRootSubViewController *tableView1Controller;

@end

@implementation ArtivleNewRootViewController

+(instancetype)sharedController{
    static ArtivleNewRootViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[ArtivleNewRootViewController alloc] init];
    });
    return _sharedAccountModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];
    [self createSegmentList];
    [self createScrollView];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.segmentArr = @[@"推荐",@"热门",@"新鲜"];
    [self rightBarButtonWithTitle:@"123" barNorImage:nil barHltImage:nil action:^{
        ArticleNewReleaseViewController *sm = [[ArticleNewReleaseViewController alloc]init];
        [self.navigationController pushViewController:sm animated:YES];
    }];
}

#pragma mark - createSegment
-(void)createSegmentList{
    self.segmentList = [PDSelectionListTitleView createSegmentWithDataSource:self.segmentArr actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        
    }];
    self.segmentList.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(60), LCFloat(40));
    self.segmentList.isNotScroll = YES;
    self.segmentList.backgroundColor = [UIColor clearColor];
    [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.navigationItem.titleView = self.segmentList;
}

#pragma mark - scrollView
-(void)createScrollView{
    if (!self.mainScrollView){
        __weak typeof(self)weakSelf = self;
        self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
            if (!weakSelf){
                return ;
            }
            NSInteger page = scrollView.contentOffset.x / kScreenBounds.size.width;
        }];
        
        self.mainScrollView.scrollEnabled = YES;
        self.mainScrollView.contentSize = CGSizeMake(self.segmentArr.count * kScreenBounds.size.width, self.mainScrollView.size_height);
        self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.view.size_height);
        [self.view addSubview:self.mainScrollView];
    }
    
    [self createMainTableView];
}

#pragma mark - createTableView
-(void)createMainTableView{
    self.tableView1Controller = [[ArticleNewRootSubViewController alloc]init];
    self.tableView1Controller.view.backgroundColor = UURandomColor;
    self.tableView1Controller.view.frame = self.mainScrollView.bounds;
    [self.mainScrollView addSubview:self.tableView1Controller.view];
    [self addChildViewController:self.tableView1Controller];
}

@end
