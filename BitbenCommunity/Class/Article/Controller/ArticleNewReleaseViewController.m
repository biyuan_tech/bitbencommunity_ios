//
//  ArticleNewReleaseViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleNewReleaseViewController.h"
#import "CollectionViewReorderableTripletLayout.h"
#import "ReleaseUtils.h"
#import "ArticleDetailHuatiListViewController.h"

// cell
#import "ArticleNewReleaseTextViewCollectionViewCell.h"
#import "ArticleNewReleaseImgCellCollectionViewCell.h"
#import "XZPostTipsView.h"
#import "XZTextCellFakeView.h"
#import "ArticleReleaseBottomView.h"
#import "ArticleReleaseInputToolBar.h"

#import "NetworkAdapter+Article.h"
#import "ArticleTagsView.h"


// 宏定义
#define XZCollectionInsetVertical 15.f
#define XZCollectionItemSpace 15.f
#define BottomViewHeight 52
#define CELLID @"cellIdentify"
#define TextCellID @"textViewCellIdentify"
#define HEADERID @"headerViewIdentify"

@interface ArticleNewReleaseViewController ()<CollectionViewDelegateReorderableTripletLayout, CollectionViewReorderableTripletLayoutDataSource,XZTextCellFakeViewDelegate,UITextViewDelegate>
@property (nonatomic,strong)UICollectionView *mainCollectionView;
@property (nonatomic,strong)CollectionViewReorderableTripletLayout *mainLayout;
@property (nonatomic,strong)UITextView *inputTextView;
@property (nonatomic,strong)NSMutableArray *imageMutableArr;
@property (nonatomic,assign)BOOL isShowKeyboard;                        /**< 判断键盘是否弹起*/
@property (nonatomic,assign)NSInteger seletedItem;                      /**< 用户选择的item*/
@property (nonatomic,strong)XZTextCellFakeView *cellTextFakeView;       /**< cell临时使用*/
@property (nonatomic,assign)BOOL isBeginEdit;
@property (nonatomic,assign)CGFloat textCellmaxY;
@property (nonatomic,strong)XZPostTipsView *tipView;
@property (nonatomic,strong)ArticleReleaseInputToolBar *inputTooBar;
@property (nonatomic,strong)UIButton *navReleasebtn;
@property (nonatomic,strong)UIButton *leftNavReleasebtn;
@property (nonatomic,strong)ArticleTagsView *huatiDymicView;
@property (nonatomic,strong)NSMutableArray *huatiMutableArr;

@end

@implementation ArticleNewReleaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createCollectionView];
    [self createTextViewToView:nil];
    [self createInputTooBar];
    [self createHuatiView];
    
    
    self.tipView = [[XZPostTipsView alloc]initWithFrame:[XZPostTipsView getTipsViewFrame]];
    [self.tipView showViewOnMainWindow];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(throwAwayTextViewFakeView)];
    [self.mainCollectionView addGestureRecognizer:tap];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self createNotifi];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"发布文章";
    __weak typeof(self)weakSelf = self;
    self.navReleasebtn = [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_article_release_nav_release_nor"] barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf throwAwayTextViewFakeView];
        [strongSelf createArticleReleaseManager];
    }];
    
    self.leftNavReleasebtn = [self leftBarButtonWithTitle:@"取消" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.imageMutableArr.count || strongSelf.inputTextView.text.length || strongSelf.huatiMutableArr.count){
            [[UIActionSheet actionSheetWithTitle:@"要放弃发布吗？" buttonTitles:@[@"取消",@"放弃发布"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
                if (buttonIndex == 0){
                    [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
                }
            }]showInView:strongSelf.view];
        }
    }];
}

#pragma mark - createInputToolBar
-(ArticleReleaseInputToolBar *)createInputTooBar{
    if (!self.inputTooBar){
        self.inputTooBar = [[ArticleReleaseInputToolBar alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [BYTabbarViewController sharedController].tabBarHeight)];
        self.inputTooBar.frame = CGRectMake(0, kScreenBounds.size.height - [BYTabbarViewController sharedController].navBarHeight - self.inputTooBar.size_height, kScreenBounds.size.width, self.inputTooBar.size_height);
        __weak typeof(self)weakSelf = self;
        self.inputTooBar.transferBarType = ArticleReleaseInputToolBarTypeLong;
        self.inputTooBar.maxInputCount = 30;
        [self.inputTooBar actionClickJinghaoblock:^{                // 井号
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            // 埋点
            [strongSelf insertTextManager];
        }];
        
        // 表情
        [self.inputTooBar actionClickBiaoqingblock:^{
            if (!weakSelf){
                return ;
            }
        }];
        
        // 图片
        [self.inputTooBar actionClickImgSelectedblock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf insertImgManager];
        }];
        
        // 文案编辑
        [self.inputTooBar actionClickFontSelectedblock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            // 埋点
            [strongSelf insertTextManager];
        }];
        [self.view addSubview:self.inputTooBar];
    }
    
    return self.inputTooBar;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.imageMutableArr = [NSMutableArray array];
    self.huatiMutableArr = [NSMutableArray array];
    [self.huatiMutableArr addObject:@"+ 选择话题"];
}

-(void)createHuatiView{
    self.huatiDymicView = [[ArticleTagsView alloc]initWithFrame:CGRectMake( LCFloat(20), 0, kScreenBounds.size.width - 2 * LCFloat(20) - LCFloat(40),[ArticleTagsView calculationCellHeightWithArr:@[@""] hasEdit:NO hasCreate:YES])];
    self.huatiDymicView.backgroundColor = [UIColor clearColor];
    self.huatiDymicView.hasEdit = NO;
    self.huatiDymicView.hasRelease = YES;
    self.huatiDymicView.orgin_y = self.inputTooBar.orgin_y - self.huatiDymicView.size_height - LCFloat(20);
    self.huatiDymicView.transferArr = self.huatiMutableArr;
    [self.view addSubview:self.huatiDymicView];
    __weak typeof(self)weakSelf = self;
    [self.huatiDymicView actionClickWithTagsBlock:^(NSString *info) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([info isEqualToString:@"+ 选择话题"]){
            [strongSelf addHuatiManager];
//
//            [strongSelf.huatiMutableArr addObject:info];
//            strongSelf.huatiDymicView.transferArr = strongSelf.huatiMutableArr;
//            strongSelf.huatiDymicView.size_height = [ArticleTagsView calculationCellHeightWithArr:strongSelf.huatiMutableArr hasEdit:NO];
        }
    }];
}

-(void)addHuatiManager{
    
    ArticleDetailHuatiListViewController *huatiListVC = [[ArticleDetailHuatiListViewController alloc]init];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:huatiListVC];
    huatiListVC.transferSelectedHuatiArr = self.huatiMutableArr;
    huatiListVC.maxCount = 3;
    __weak typeof(self)weakSelf = self;
    [huatiListVC actionClickWithSelectedBlock:^(NSArray *selectedArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf throwAwayTextViewFakeView];
        
        [strongSelf.huatiMutableArr removeAllObjects];
        [strongSelf.huatiMutableArr addObjectsFromArray:selectedArr];
        [strongSelf.huatiMutableArr addObject:@"+ 选择话题"];
        strongSelf.huatiDymicView.transferArr = strongSelf.huatiMutableArr;
        strongSelf.huatiDymicView.orgin_y = strongSelf.inputTooBar.orgin_y - LCFloat(20) - [ArticleTagsView calculationCellHeightWithArr:strongSelf.huatiMutableArr hasEdit:NO hasCreate:YES];
        
        [strongSelf actionClickManagerStatus];
    }];
    [self.navigationController presentViewController:nav animated:YES completion:NULL];
}


-(void)createTextViewToView:(UIView *)view{
    if (!self.inputTextView) {
        self.inputTextView = [[UITextView alloc]initWithFrame:CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(80))];
        self.inputTextView.backgroundColor = [UIColor clearColor];
        self.inputTextView.textColor = [UIColor blackColor];
        self.inputTextView.font = [[UIFont systemFontOfCustomeSize:21]boldFont];
        __weak typeof(self)weakSelf = self;
        [self.inputTextView textViewDidChangeWithBlock:^(NSInteger currentCount) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.inputTooBar.currentInputCount = currentCount;
            [strongSelf actionClickManagerStatus];
        }];
        [self.inputTextView textViewDidPasteChangeWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf actionClickManagerStatus];
            if (strongSelf.inputTextView.text.length){
                strongSelf.inputTooBar.currentInputCount = strongSelf.inputTextView.text.length;
                strongSelf.inputTextView.placeholderLabel.hidden = YES;
            } else {
                strongSelf.inputTextView.placeholderLabel.hidden = NO;
            }
        }];
        
        self.inputTextView.placeholder = @"请输入标题";
        self.inputTextView.limitMax = 30;
        [self.mainCollectionView addSubview:self.inputTextView];
        
        UIView *lineView = [[UIView alloc]init];
        lineView.backgroundColor = [UIColor hexChangeFloat:@"E7E7EA"];
        lineView.frame = CGRectMake(0, CGRectGetMaxY(self.inputTextView.frame) + 3.f, kScreenBounds.size.width, .5f);
        [self.mainCollectionView addSubview:lineView];
    }
}

#pragma mark - UICollectionView
-(void)createCollectionView{
    if (!self.mainCollectionView){
        self.mainLayout = [[CollectionViewReorderableTripletLayout alloc]init];
        self.mainLayout.delegate = self;
        self.mainLayout.datasource = self;
        
        self.mainCollectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:self.mainLayout];
        self.mainCollectionView.backgroundColor = [UIColor whiteColor];
        self.mainCollectionView.dataSource = self;
        self.mainCollectionView.delegate = self;
        self.mainCollectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        [self.mainCollectionView registerClass:[ArticleNewReleaseImgCellCollectionViewCell class] forCellWithReuseIdentifier:CELLID];
        [self.mainCollectionView registerClass:[ArticleNewReleaseTextViewCollectionViewCell class] forCellWithReuseIdentifier:TextCellID];
        [self.mainCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:HEADERID];
        [self.view addSubview:self.mainCollectionView];
        
        self.seletedItem = -1;
        self.mainCollectionView.tag = -5;
        self.isBeginEdit = NO;
    }
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.imageMutableArr.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isTextView = [[self.imageMutableArr objectAtIndex:indexPath.item]isKindOfClass:[NSString class]];
    if (isTextView){
        ArticleNewReleaseTextViewCollectionViewCell *cell = (ArticleNewReleaseTextViewCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:TextCellID forIndexPath:indexPath];
        [cell setCellTextView:(NSString *)[self.imageMutableArr objectAtIndex:indexPath.item] indexPath:indexPath];
        cell.tag = 999;
        if (self.isShowKeyboard && indexPath.item == self.seletedItem) {
            self.isShowKeyboard = NO;                   // 表示键盘已经弹起
            // 移除临时使用TextViewCell
            [self throwAwayTextViewFakeView];
            NSString *cellStr = cell.textView.text;
            if (!self.cellTextFakeView){
                self.cellTextFakeView = [[XZTextCellFakeView alloc]initWithFrame:cell.frame indexPath:indexPath];
                self.cellTextFakeView.cellTextViewDelegate = self;
                self.cellTextFakeView.text = cellStr;
                [collectionView addSubview:self.cellTextFakeView];
                
                for (UIGestureRecognizer *gestureRecognizer in _cellTextFakeView.gestureRecognizers) {
                    if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]) {
                        [self.mainLayout.longPressGesture requireGestureRecognizerToFail:gestureRecognizer];
                    }
                }
                
                self.mainLayout.panGesture.enabled = NO;
                self.mainLayout.tapGesture.enabled = NO;
                self.mainLayout.longPressGesture.enabled = NO;
            }
            if (!self.cellTextFakeView.isFirstResponder) {
                [self.cellTextFakeView becomeFirstResponder];
            }
        }
        return cell;
    } else {
        ArticleNewReleaseImgCellCollectionViewCell *cellWithRowTwo = (ArticleNewReleaseImgCellCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CELLID forIndexPath:indexPath];
        [cellWithRowTwo setCellImage:self.imageMutableArr indexPath:indexPath];
        cellWithRowTwo.tag = 888;
        return cellWithRowTwo;
    }
}

#pragma mark - 第三方框架的代理
- (CGFloat)sectionSpacingForCollectionView:(UICollectionView *)collectionView {
    return 5.f;
}

- (CGFloat)minimumInteritemSpacingForCollectionView:(UICollectionView *)collectionView {
    return XZCollectionItemSpace;
}

- (CGFloat)minimumLineSpacingForCollectionView:(UICollectionView *)collectionView {
    return 15.f;
}

- (UIEdgeInsets)insetsForCollectionView:(UICollectionView *)collectionView {
    return UIEdgeInsetsMake(5.f, XZCollectionInsetVertical, BottomViewHeight + 15.f, XZCollectionInsetVertical);
}

- (CGSize)collectionView:(UICollectionView *)collectionView sizeForLargeItemsInSection:(NSInteger)section {
    return CollectionViewTripletLayoutStyleSquare; //same as default !
}

- (UIEdgeInsets)autoScrollTrigerEdgeInsets:(UICollectionView *)collectionView {
    return UIEdgeInsetsMake(50.f, 0, 50.f, 0); //Sorry, horizontal scroll is not supported now.
}

- (UIEdgeInsets)autoScrollTrigerPadding:(UICollectionView *)collectionView {
    return UIEdgeInsetsMake(64.f, 0, BottomViewHeight + 100, 0);
}

- (CGFloat)reorderingItemAlpha:(UICollectionView *)collectionview {
    return .3f;
}


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath didMoveToIndexPath:(NSIndexPath *)toIndexPath {
    
    NSLog(@"fromIndexPath = %zd toIndexPath =%zd",fromIndexPath.item,toIndexPath.item);
    //    NSLog(@"照片数组 == %@",self.imageArray);
    NSInteger fromItem = fromIndexPath.item;
    
    UIImage *image = [self.imageMutableArr objectAtIndex:fromItem];
    [self.imageMutableArr removeObjectAtIndex:fromItem];
    [self.imageMutableArr insertObject:image atIndex:toIndexPath.item];
    
    CGSize cellSize = [[self.mainLayout.allCellSizeArr objectAtIndex:fromItem] CGSizeValue];
    [self.mainLayout.allCellSizeArr removeObjectAtIndex:fromItem];
    [self.mainLayout.allCellSizeArr insertObject:[NSValue valueWithCGSize:cellSize] atIndex:toIndexPath.item];
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
#pragma mark 是否允许cell移动到这个indexPath
- (BOOL)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath canMoveToIndexPath:(NSIndexPath *)toIndexPath {
    return YES;
}
#pragma mark 是否允许cell移动
- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
#pragma mark 全部是否允许移动
- (BOOL)canMoveCollectionView:(UICollectionView *)collectionView {
    return YES;
}

#pragma mark 将要开始拖动
- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout willBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    [self throwAwayTextViewFakeView];
}

#pragma mark - XZPostPhotoGroupViewControllerDelegate(全新选择图片回来)
- (void)didSelectPhotos:(NSMutableArray *)photos{
    self.imageMutableArr = [NSMutableArray arrayWithArray:photos];
    self.mainLayout.allCellSizeArr = [NSMutableArray array];
    self.mainLayout.allCellFrameArr = [NSMutableArray array];
    _seletedItem = 0;
    self.mainCollectionView.tag = 1;
    [self.mainCollectionView reloadData];
}

#pragma mark - XZOneDelegate赋值给单独的cell(选择一张图片回来)
- (void)didSelectOnePhotos:(UIImage *)image item:(NSInteger)item {
    NSLog(@"image = %@\nitem =%zd",image,item);
    [self.imageMutableArr replaceObjectAtIndex:item withObject:image];
    [self.mainCollectionView reloadData];
}

#pragma mark - XZEditPhotoGroupViewControllerDelegate(再次去选择图片回来)
- (void)addEditSelectPhotos:(NSMutableArray *)photos {
    [self.self.imageMutableArr addObjectsFromArray:photos];
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width - 30.f;
    CGFloat CellHeightPart = 2;
    CGSize imageSize = CGSizeMake(width, width/CellHeightPart);
    for (int iii = 0; iii < photos.count; iii ++) {
        [self.mainLayout.allCellSizeArr addObject:[NSValue valueWithCGSize:imageSize]];
    }
    
    [self.mainCollectionView reloadData];
}



#pragma mark - layout代理事件
#pragma mark cell点击事件，系统的点击事件被我拦截了（我不是故意的）
- (void)customCollectionView:(UICollectionView *)collectionView didSeleted:(NSIndexPath *)indexPath {
//    if (collectionView.tag == -1) {
//        [self openNewPhotoListView];
//        return;
//    }
    _seletedItem = indexPath.item;
    //    DLog(@"_seletedItem == %zd", _seletedItem);
    [self throwAwayTextViewFakeView];
    //    DLog(@"editText %zd",indexPath.item);
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    if ([cell isKindOfClass:[ArticleNewReleaseTextViewCollectionViewCell class]]) {//cell上的mune是在手势layout里创建
        NSString *cellStr = [self.imageMutableArr objectAtIndex:indexPath.item];
        if (!_cellTextFakeView) {
            _cellTextFakeView = [[XZTextCellFakeView alloc]initWithFrame:cell.frame indexPath:indexPath];
            _cellTextFakeView.cellTextViewDelegate = self;
            _cellTextFakeView.text = cellStr;
            [collectionView addSubview:_cellTextFakeView];
        }
    } else {//此时在手势layout创建了一个image类型的fakecellview(细胞假象视图)
        [self.view endEditing:YES];
    }
    
}
#pragma mark 改变输入框对齐方式（暂时不实现）
- (void)customCollectionView:(UICollectionView *)collectionView changeAlignment:(NSIndexPath *)indexPath {
    NSLog(@"changeAlignment");
}
#pragma mark 换图
- (void)customCollectionView:(UICollectionView *)collectionView changeImage:(NSIndexPath *)indexPath {
    [self throwAwayTextViewFakeView];
    self.isShowKeyboard = NO;
    
    
    GWAssetsLibraryViewController *assetLibiaryVC = [[GWAssetsLibraryViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [assetLibiaryVC selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        UIImage *selectedImg = [selectedImgArr lastObject];
        [strongSelf.imageMutableArr replaceObjectAtIndex:indexPath.row withObject:selectedImg];
        [strongSelf.mainCollectionView reloadData];
    }];
    [self.navigationController pushViewController:assetLibiaryVC animated:YES];
}

#pragma mark 删除cell
- (void)customCollectionView:(UICollectionView *)collectionView deleteCell:(NSIndexPath *)indexPath {
    //删除cell用的
    if (self.imageMutableArr.count == 0) {
        return;
    }
    
    [self throwAwayTextViewFakeView];
    
    NSInteger item = indexPath.item;
    __weak typeof(self)weakSelf = self;
    [self.mainCollectionView performBatchUpdates:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.imageMutableArr removeObjectAtIndex:item];
        
        [strongSelf.mainLayout.allCellSizeArr removeObjectAtIndex:item];
        
        [strongSelf.mainCollectionView deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.mainCollectionView reloadData];
    }];
  
}
#pragma mark 输入
- (void)customCollectionView:(UICollectionView *)collectionView editText:(NSIndexPath *)indexPath {
    self.mainLayout.panGesture.enabled = NO;
    self.mainLayout.tapGesture.enabled = NO;
    self.mainLayout.longPressGesture.enabled = NO;
    [_cellTextFakeView becomeFirstResponder];
    self.inputTooBar.numberLabel.hidden = YES;
}
#pragma mark - 供调用
#pragma mark 打开新相册
- (void)openNewPhotoListView {
  
}

#pragma mark - textFakeViewCell代理
#pragma mark 结束编辑(编辑完的时候把得到的文字存数组)
- (void)cellTextFakeViewEndEdit:(UITextView *)textView indexPath:(NSIndexPath *)indexPath {
    if (![[self.imageMutableArr objectAtIndex:indexPath.item]isEqualToString:textView.text]) {
        
        [self.imageMutableArr replaceObjectAtIndex:indexPath.item withObject:textView.text];
    }
    
    [self throwAwayTextViewFakeView];
    //    [_collectionView reloadItemsAtIndexPaths:@[indexPath]];
    self.isShowKeyboard = NO;
    
    [self.mainCollectionView reloadData];
}

#pragma mark 开始编辑(改变导航栏右键、上升屏幕)
- (void)cellTextFakeViewBeginEdit:(UITextView *)textView indexPath:(NSIndexPath *)indexPath {
    
    
    UICollectionViewCell *cell = [self.mainCollectionView cellForItemAtIndexPath:indexPath];
    
    //    _textCellmaxY = FMGetMaxY(cell);
    self.textCellmaxY = CGRectGetMaxY(cell.frame);
    //    DLog(@"开始编辑 = %f", _textCellmaxY);
}
#pragma mark 正在编辑（改变textViewCell的高度）
- (void)cellTextFakeViewIsEditing:(UITextView *)textView indexPath:(NSIndexPath *)indexPath {
    // 1.修改右上角的按钮问题
    [self actionClickManagerStatus];
    self.inputTooBar.numberLabel.hidden = YES;
    
    CGSize cellSize = [[self.mainLayout.allCellSizeArr objectAtIndex:indexPath.item] CGSizeValue];
    
    NSLog(@"%@",NSStringFromCGSize(cellSize));
    
    CGFloat textWidth = cellSize.width - 10.f;
    CGFloat textHeight = [NSString heightFromString:textView.text withLabelWidth:textWidth andLabelFont:[UIFont systemFontOfSize:16]];
    textHeight += 15.f;
    NSLog(@"cellSizeH1:%lf--textWidth1:%lf--textHeight1:%lf",cellSize.height,textWidth,textHeight);
    NSLog(@"cellTextFake1===%@",NSStringFromCGSize(self.cellTextFakeView.frame.size));
    //编辑文字，删除或增加换行，虚拟文本框的大小调整
    if (textHeight > cellSize.height) {
        //        DLog(@"增加高度");
        //        textView.h = textHeight;
        _cellTextFakeView.size_height = textHeight;
        cellSize.height = textHeight;
        [self.mainLayout.allCellSizeArr replaceObjectAtIndex:_seletedItem withObject:[NSValue valueWithCGSize:cellSize]];
        [self.mainLayout invalidateLayout];
    }else if (textHeight < cellSize.height && textHeight > 49 ){
        
        _cellTextFakeView.size_height = textHeight-15;
        cellSize.height = textHeight-15;
        if (cellSize.height <44) {
            cellSize.height = 44;
            self.cellTextFakeView.size_height = 53;
        }
        
        [self.mainLayout.allCellSizeArr replaceObjectAtIndex:_seletedItem withObject:[NSValue valueWithCGSize:cellSize]];
        [self.mainLayout invalidateLayout];
    }
    NSLog(@"cellTextFake===%@",NSStringFromCGSize(_cellTextFakeView.frame.size));
    NSLog(@"cellSizeH:%lf--textWidth:%lf--textHeight:%lf",cellSize.height,textWidth,textHeight);
}

#pragma mark - 插入图片方法
-(void)insertImgManager{
    [self throwAwayTextViewFakeView];
    self.isShowKeyboard = NO;
    
    GWAssetsLibraryViewController *assetLibraryVC = [[GWAssetsLibraryViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [assetLibraryVC selectImageArrayFromImagePickerWithMaxSelected:10 andBlock:^(NSArray *selectedImgArr) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        [strongSelf.imageMutableArr addObjectsFromArray:selectedImgArr];
        
        CGFloat width = [UIScreen mainScreen].bounds.size.width - 30.f;
        CGFloat CellHeightPart = 2;
        CGSize imageSize = CGSizeMake(width, width/CellHeightPart);
        for (int iii = 0; iii < selectedImgArr.count; iii ++) {
            [strongSelf.mainLayout.allCellSizeArr addObject:[NSValue valueWithCGSize:imageSize]];
        }
        
        [strongSelf.mainCollectionView reloadData];
    }];
    [self.navigationController pushViewController:assetLibraryVC animated:YES];
}

#pragma mark - 插入text
-(void)insertTextManager{
    NSInteger wantInsertItem = _seletedItem + 1;//插在选中的cell的下面
    NSLog(@"插入文字之前self.imageArray = %@",self.imageMutableArr);
    
    NSString *stringTamp = @"";
    if (self.imageMutableArr.count <= wantInsertItem) {//如果是最后一个，就不插入，用添加
        wantInsertItem = self.imageMutableArr.count;
        [self.imageMutableArr addObject:stringTamp];
    } else {
        [self.imageMutableArr insertObject:stringTamp atIndex:wantInsertItem];
    }
    NSLog(@"插入文字之后self.imageArray = %@",self.imageMutableArr);
    CGFloat cellWidth = [UIScreen mainScreen].bounds.size.width - XZCollectionInsetVertical*2;
    CGSize cellSize = CGSizeMake(cellWidth, 44);
    
    if (self.mainLayout.allCellSizeArr.count <= wantInsertItem) {//如果是最后一个，就不插入，用添加
        
        [self.mainLayout.allCellSizeArr addObject:[NSValue valueWithCGSize:cellSize]];//添加
    } else {
        
        [self.mainLayout.allCellSizeArr insertObject:[NSValue valueWithCGSize:cellSize] atIndex:wantInsertItem];//插入
    }
    
    [self.mainCollectionView reloadData];
    
    //让刚插入的那个东西becomeFirstResponder
    _seletedItem = wantInsertItem;
    self.isShowKeyboard = YES;
}

#pragma mark - 移除临时使用的textFakeView
- (void)throwAwayTextViewFakeView {
    if (_cellTextFakeView) {
        [_cellTextFakeView removeFromSuperview];
        _cellTextFakeView = nil;
    }
    self.mainLayout.panGesture.enabled = YES;
    self.mainLayout.tapGesture.enabled = YES;
    self.mainLayout.longPressGesture.enabled = YES;
}

#pragma mark - 键盘代理
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postMainViewkeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postMainViewKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}


-(void)postMainViewkeyboardWillShow:(NSNotification *)notification {
    self.isBeginEdit = YES;
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.mainCollectionView.frame = newTextViewFrame;
    
    self.inputTooBar.orgin_y = CGRectGetMaxY(self.mainCollectionView.frame) - self.inputTooBar.size_height;
    self.huatiDymicView.orgin_y = self.inputTooBar.orgin_y - self.huatiDymicView.size_height - LCFloat(20);
    [UIView commitAnimations];
    if (self.imageMutableArr.count){
        [self.mainCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:_seletedItem inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    }
}

-(void)postMainViewKeyboardWillHide:(NSNotification *)notification {
    self.isBeginEdit = NO;
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.mainCollectionView.frame = self.view.bounds;
    self.inputTooBar.orgin_y = CGRectGetMaxY(self.mainCollectionView.frame) - self.inputTooBar.size_height;
    self.huatiDymicView.orgin_y = self.inputTooBar.orgin_y - self.huatiDymicView.size_height - LCFloat(20);

    [UIView commitAnimations];
}


-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - articleRelease
-(void)createArticleReleaseManager{
    NSMutableArray *articleReleaseImgArr = [NSMutableArray array];
    NSMutableArray *articleReleaseTextArr = [NSMutableArray array];
    NSString *mainInfoStr = @"";
    NSString *subTitle = @"";
    for (int i = 0 ; i < self.imageMutableArr.count;i++){
        id info = [self.imageMutableArr objectAtIndex:i];
        if ([info isKindOfClass:[UIImage class]]){
            [articleReleaseImgArr addObject:info];
            mainInfoStr = [mainInfoStr stringByAppendingString:[NSString stringWithFormat:@"<img [%li]>",articleReleaseImgArr.count - 1]];
        } else if ([info isKindOfClass:[NSString class]]){
            [articleReleaseTextArr addObject:info];
            info = [info stringByReplacingOccurrencesOfString:@" " withString:@"&nbsp;"];
            mainInfoStr = [mainInfoStr stringByAppendingString:[NSString stringWithFormat:@"<p>%@</p>",info]];
            
            if (i == self.imageMutableArr.count - 1){
                subTitle = [subTitle stringByAppendingString:[NSString stringWithFormat:@"%@",info]];
            } else {
                subTitle = [subTitle stringByAppendingString:[NSString stringWithFormat:@"%@\r",info]];
            }
        }
    }
    
    __weak typeof(self)weakSelf = self;
    NSMutableArray *tagTempMutableArr = [NSMutableArray array];
    [tagTempMutableArr addObjectsFromArray: [self.huatiMutableArr copy]];
    if ([tagTempMutableArr containsObject:@"+ 选择话题"]){
        [tagTempMutableArr removeObject:@"+ 选择话题"];
    }
    
    [[NetworkAdapter sharedAdapter] articleAddWithImgList:articleReleaseImgArr articleType:ArticleTypeLong topicArr:tagTempMutableArr title:self.inputTextView.text content:mainInfoStr subTitle:subTitle isAuthor:_inputTooBar.originalBtn.selected block:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
    }];
}

-(void)actionClickManagerStatus{
    if (self.inputTextView.text.length && self.imageMutableArr.count && self.huatiMutableArr.count > 1){
        [self.navReleasebtn setImage:[UIImage imageNamed:@"icon_article_release_nav_release_hlt"] forState:UIControlStateNormal];
        self.navReleasebtn.enabled = YES;
    } else {
        [self.navReleasebtn setImage:[UIImage imageNamed:@"icon_article_release_nav_release_nor"] forState:UIControlStateNormal];
        self.navReleasebtn.enabled = NO;
    }
}


@end
