//
//  ArticleNewReleaseViewController.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleNewReleaseViewController : AbstractViewController

@end

NS_ASSUME_NONNULL_END
