//
//  ArticleQuicklyInfoTitleReleaseTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/12/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleQuicklyInfoTitleReleaseTableViewCell.h"

static char textViewDidPasteChangeWithBlockKey;
@interface ArticleQuicklyInfoTitleReleaseTableViewCell()


@end

@implementation ArticleQuicklyInfoTitleReleaseTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    if (!self.inputTextView) {
        self.inputTextView = [[UITextView alloc]initWithFrame:CGRectMake(LCFloat(5), 0, kScreenBounds.size.width - 2 * LCFloat(5), LCFloat(80))];
    }
       self.inputTextView.backgroundColor = [UIColor clearColor];
       self.inputTextView.textColor = [UIColor blackColor];
       self.inputTextView.font = [[UIFont systemFontOfCustomeSize:21] boldFont];
       __weak typeof(self)weakSelf = self;
       [self.inputTextView textViewDidChangeWithBlock:^(NSInteger currentCount) {
           if (!weakSelf){
               return ;
           }
           __strong typeof(weakSelf)strongSelf = weakSelf;
           void(^block)(NSInteger currentCount) = objc_getAssociatedObject(strongSelf, &textViewDidPasteChangeWithBlockKey);
           if (block){
               block(currentCount);
           }
       }];
    
       [self.inputTextView textViewDidPasteChangeWithBlock:^{
           if (!weakSelf){
               return ;
           }
           __strong typeof(weakSelf)strongSelf = weakSelf;
           if (strongSelf.inputTextView.text.length){
               strongSelf.inputTextView.placeholderLabel.hidden = YES;
           } else {
               strongSelf.inputTextView.placeholderLabel.hidden = NO;
           }
       }];
       
       self.inputTextView.placeholder = @"请输入快讯标题";
       self.inputTextView.limitMax = 30;
       [self addSubview:self.inputTextView];
       
       UIView *lineView = [[UIView alloc]init];
       lineView.backgroundColor = [UIColor hexChangeFloat:@"E7E7EA"];
       lineView.frame = CGRectMake(0, CGRectGetMaxY(self.inputTextView.frame) + 3.f, kScreenBounds.size.width, .5f);
       [self addSubview:lineView];
}

-(void)textViewDidPasteChangeWithBlock:(void(^)(NSInteger currentCount))block{
    objc_setAssociatedObject(self, &textViewDidPasteChangeWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(80);
    cellHeight += LCFloat(5);
    return cellHeight;
}

@end
