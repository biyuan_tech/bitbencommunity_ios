//
//  ArticleNewRootSubViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleNewRootSubViewController.h"
#import "ArticleRankingTableViewCell.h"
#import "ArticleRootTableViewCell.h"
#import "ArticleRootActionTableViewCell.h"
#import "NetworkAdapter+Article.h"
#import "BYPersonHomeController.h"
#import "ShareRootViewController.h"
#import "ReportViewController.h"
#import "NetworkAdapter+Center.h"
#import "GWAssetsImgSelectedViewController.h"
#import "RankingRootViewController.h"

@interface ArticleNewRootSubViewController ()<UITableViewDataSource,UITableViewDelegate>{
    ArticleRootRankListModel *rankListModel;
}
@property (nonatomic,strong)UITableView *articleTableView;
@property (nonatomic,strong)NSMutableArray *articleMutableArr;
@property (nonatomic,strong)NSMutableArray *rankListMutableArr;

@end

@implementation ArticleNewRootSubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    if(self.transferTags.length){
        [self sendRequestToGetListManager:YES block:NULL];
    }
}

#pragma pageSetting
-(void)pageSetting{
    self.view.backgroundColor = RGB(245, 245, 245, 1);

    if (self.transferTags.length){
        self.barMainTitle = self.transferTags;
    } else {
        self.barMainTitle = @"标题";
    }
}

#pragma mark - arrayWithIit
-(void)arrayWithInit{
    // 排行榜
    self.rankListMutableArr = [NSMutableArray array];
    self.articleMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if(!self.articleTableView){
        self.articleTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.articleTableView.dataSource = self;
        self.articleTableView.delegate = self;

        [self.view addSubview:self.articleTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.articleTableView appendingPullToRefreshHandler:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetListManager:YES block:NULL];
    }];
    
    [self.articleTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetListManager:NO block:NULL];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.articleMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"排行" sourceArr:self.articleMutableArr]){
        return 1;
    } else {
        return 2;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"排行" sourceArr:self.articleMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"排行" sourceArr:self.articleMutableArr]){
        static NSString *cellIdentifyWithRowRank = @"cellIdentifyWithRowRank";
        ArticleRankingTableViewCell *cellWithRowRank = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowRank];
        if (!cellWithRowRank){
            cellWithRowRank = [[ArticleRankingTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowRank];
        }
        cellWithRowRank.transferRankListArr = self.rankListMutableArr;
        __weak typeof(self)weakSelf = self;
        [cellWithRowRank actionClickRankItemsBlock:^(ArticleRootRankModel * _Nonnull model) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if ([model.nickname isEqualToString:@"排行榜"]){
                RankingRootViewController *rankingRootVC = [[RankingRootViewController alloc]init];
                [strongSelf.navigationController pushViewController:rankingRootVC animated:YES];
            } else {
                BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
                personHomeController.user_id = model.user_id;
                [strongSelf.navigationController pushViewController:personHomeController animated:YES];
            }
        }];
        return cellWithRowRank;
    } else {
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            ArticleRootTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[ArticleRootTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            }
            ArticleRootSingleModel *singleModel = [self.articleMutableArr objectAtIndex:indexPath.section];
            cellWithRowOne.transferSingleModel = singleModel;
            
            __weak typeof(self)weakSelf = self;
            // 1. 图片点击
            [cellWithRowOne actionClickWithImgSelectedBlock:^(NSString *imgUrl) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                GWAssetsImgSelectedViewController *imgSelected = [[GWAssetsImgSelectedViewController alloc]init];
                NSInteger index = [singleModel.picture indexOfObject:imgUrl];
                [imgSelected showInView:strongSelf.parentViewController imgArr:singleModel.picture currentIndex:index cell:cellWithRowOne];
            }];
            
            // 2. 标签点击
            [cellWithRowOne actionClickWithTagsSelectedBlock:^(NSString *tag) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                ArticleNewRootSubViewController *articleRootVC = [[ArticleNewRootSubViewController alloc]init];
                articleRootVC.transferType = ArticleRootViewControllerTypeTags;
                articleRootVC.transferTags = tag;
                [strongSelf.navigationController pushViewController:articleRootVC animated:YES];
            }];
            
            // 3. 关注
            [cellWithRowOne actionClickWithLinkButtonBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                
                [strongSelf authorizeWithCompletionHandler:^(BOOL successed) {
                    if (successed){
                        [strongSelf linkWIthCell:cellWithRowOne hasLink:YES];
                    }
                }];
            }];
            
            // 4. 头像点击
            [cellWithRowOne actionClickHeaderImgWithBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
                personHomeController.user_id = cellWithRowOne.transferSingleModel.author_id;
                [strongSelf.navigationController pushViewController:personHomeController animated:YES];
            }];
            
            // 5. 举报
            [cellWithRowOne actionClickWithMoreBtnClickBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf jubaoManager:singleModel];
            }];
            
            
            return cellWithRowOne;
        } else if (indexPath.row == 1){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            ArticleRootActionTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[ArticleRootActionTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            }
            ArticleRootSingleModel *singleModel = [self.articleMutableArr objectAtIndex:indexPath.section];
            cellWithRowTwo.transferSingleModel = singleModel;
            __weak typeof(self)weakSelf = self;
            [cellWithRowTwo actionClickWithShareManagerWithArticleModelBlock:^(ArticleRootSingleModel *articleModel) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf shareManager:articleModel];
            }];
            return cellWithRowTwo;
        }
        return nil;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section != [self cellIndexPathSectionWithcellData:@"排行" sourceArr:self.articleMutableArr]){
        ArticleDetailRootViewController *viewController = [[ArticleDetailRootViewController alloc]init];
        ArticleRootSingleModel *singleModel = [self.articleMutableArr objectAtIndex:indexPath.section];
        
        __weak typeof(self)weakSelf = self;
        [viewController actionReoloadFollow:^(BOOL link) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            // 遍历所有的Model 只要是同一个作者的都备注已关注
            for (int i = 0 ; i < strongSelf.articleMutableArr.count;i++){
                id singleModel1 = [strongSelf.articleMutableArr objectAtIndex:i];
                if ([singleModel1 isKindOfClass:[ArticleRootSingleModel class]]){
                    ArticleRootSingleModel *tempSingleModel = (ArticleRootSingleModel *)singleModel1;
                    if ([tempSingleModel.author_id isEqualToString:singleModel.author_id]){
                        tempSingleModel.isAttention = link;
                    }
                }
            }
            [strongSelf.articleTableView reloadData];
        }];
        
        [viewController actionReoloadZan:^(BOOL link) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            // 遍历所有的Model 只要是同一个作者的都备注已关注
            for (int i = 0 ; i < strongSelf.articleMutableArr.count;i++){
                id singleModel1 = [strongSelf.articleMutableArr objectAtIndex:i];
                if ([singleModel1 isKindOfClass:[ArticleRootSingleModel class]]){
                    ArticleRootSingleModel *tempSingleModel = (ArticleRootSingleModel *)singleModel1;
                    if ([tempSingleModel.author_id isEqualToString:singleModel.author_id]){
                        tempSingleModel.isSupport = link;
                    }
                }
            }
            [strongSelf.articleTableView reloadData];
        }];
        viewController.transferArticleId = singleModel._id;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"排行" sourceArr:self.articleMutableArr]){
        return LCFloat(80);
    } else {
        if (indexPath.row == 0){
            ArticleRootSingleModel *singleModel = [self.articleMutableArr objectAtIndex:indexPath.section];
            return [ArticleRootTableViewCell calculationCellHeightWithModel:singleModel];
        } else {
            return LCFloat(50);
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.articleTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if (indexPath.row == 0){
            separatorType  = SeparatorTypeBottom;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"message"];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(10);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)sendRequestToGetListManager:(BOOL)hasLoad block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    if(!self.articleMutableArr.count){
        [self.articleTableView showListLoadingAnimation];
    }
    
    // 更新类型
    [[NetworkAdapter sharedAdapter] articleListWithPage:self.articleTableView.currentPage topic:self.transferTags type:self.transferType authorId:@"" block:^(NSArray<ArticleRootSingleModel> *articleList) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        if (hasLoad || strongSelf.articleTableView.isXiaLa){
            [strongSelf.articleTableView stopPullToRefresh];
        } else {
            [strongSelf.articleTableView stopFinishScrollingRefresh];
        }

        
        
        if (strongSelf.articleTableView.isXiaLa || hasLoad){       // 下拉
            [strongSelf.articleTableView dismissListAnimation];
            [strongSelf.articleMutableArr removeAllObjects];
        }
  
        
        if (strongSelf.transferType == ArticleListTypeTuijian && !strongSelf.transferTags.length){
            if (![strongSelf.articleMutableArr containsObject:@[@"排行"]]){
                [strongSelf.articleMutableArr addObject:@[@"排行"]];
            }
        }

        [strongSelf.articleMutableArr addObjectsFromArray:articleList];
        
        if (articleList.count){
            [strongSelf.articleTableView reloadData];
        }


        if (strongSelf.articleMutableArr.count){
            [strongSelf.articleTableView dismissPrompt];
            [strongSelf.articleTableView dismissListAnimation];
        } else {
            [strongSelf.articleTableView showPrompt:@"当前没有文章" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
        }
        
        if (block){
            block();
        }
    }];
}



#pragma mark - 接口请求
// 获取排行榜
-(void)getRankListManagerWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] articleRankInfoManagerSuccessBlock:^(ArticleRootRankListModel *rankListModel) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(strongSelf.rankListMutableArr.count){
            [strongSelf.rankListMutableArr removeAllObjects];
        }
        [strongSelf.rankListMutableArr addObjectsFromArray:rankListModel.rank_list];
        if (block){
            block();
        }
    }];
}


#pragma mark - 关注
-(void)linkWIthCell:(ArticleRootTableViewCell *)targetCell hasLink:(BOOL)link{
    NSString *userId = targetCell.transferSingleModel.author_id;
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerSendRequestToLinkManagerWithUserId:userId hasLink:link block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        if (isSuccessed){
            [StatusBarManager statusBarHidenWithText:@"已关注"];
            for (UITableViewCell *cell in strongSelf.articleTableView.visibleCells){
                if ([cell isKindOfClass:[ArticleRootTableViewCell class]]){
                    ArticleRootTableViewCell *smartCell = (ArticleRootTableViewCell *)cell;
                    if ([smartCell.transferSingleModel.author_id isEqualToString:targetCell.transferSingleModel.author_id]){
                        smartCell.transferSingleModel.isAttention = YES;
                        [smartCell linkButtonStatusManager];
                    }
                }
            }
        }
    }];
}

#pragma mark 开始加载内容
-(void)startLoadInterfaceManager{
    // 1. 进行判断
    __weak typeof(self)weakSelf = self;
    if (self.transferType == ArticleListTypeTuijian){
        if (!self.rankListMutableArr.count){
            [self getRankListManagerWithBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (!strongSelf.articleMutableArr.count){
                    [strongSelf sendRequestToGetListManager:YES block:NULL];
                }
            }];
        }
    } else {
        if (!self.articleMutableArr.count){
            [self sendRequestToGetListManager:YES block:NULL];
        }
    }
}

























#pragma mark 举报内容
-(void)jubaoManager:(ArticleRootSingleModel *)singleModel{
    ReportViewController *reportVC = [[ReportViewController alloc]init];
    [reportVC showInView:self.parentViewController type:BY_THEME_TYPE_ARTICLE itemId:singleModel._id];
}

#pragma mark 分享内容
-(void)shareManager:(ArticleRootSingleModel *)articleModel{
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.transferCopyUrl = articleModel.article_url;
    shareViewController.collection_status = shareViewController.collection_status;
    shareViewController.hasJubao = NO;
    __weak typeof(self)weakSelf = self;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        thirdLoginType shareType;
        if ([type isEqualToString:@"wechat"]){
            shareType = thirdLoginTypeWechat;
        } else if ([type isEqualToString:@"friendsCircle"]){
            shareType = thirdLoginTypeWechatFirend;
        } else if ([type isEqualToString:@"sina"]){
            shareType = thirdLoginTypeWeibo;
        } else {
            shareType = thirdLoginTypeWechat;
        }
        id smartImgUrl;
        if (articleModel.picture.count){
            smartImgUrl = [articleModel.picture firstObject];
        } else {
            smartImgUrl = articleModel.head_img;
        }
        
        id mainImgUrl;
        if ([smartImgUrl isKindOfClass:[NSString class]]){
            NSString *urlStr = (NSString *)smartImgUrl;
            NSString *baseURL = [PDImageView getUploadBucket:urlStr];
            NSString *nUrl = [[NSString stringWithFormat:@"%@%@",baseURL,urlStr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            mainImgUrl = nUrl;
        } else if ([smartImgUrl isKindOfClass:[UIImage class]]){
            mainImgUrl = smartImgUrl;
        }
        
        NSString *title = @"";
        NSString *content = @"";
        
        
        if (articleModel.article_type == article_typeWeb){
            title = articleModel.title;
            if (articleModel.subtitle.length && ![articleModel.subtitle isEqualToString:@"null"]){
                content = articleModel.subtitle;
            } else {
                content = @"最新最热区块链内容，尽在币本社区";
            }
        } else if (articleModel.article_type == article_typeNormal){
            title = articleModel.title.length?articleModel.title:articleModel.content;
            content = @"最新最热区块链内容，尽在币本社区";
        }
        [ShareSDKManager shareManagerWithType:shareType title:title desc:content img:mainImgUrl url:articleModel.article_url callBack:NULL];
        [ShareSDKManager shareSuccessBack:shareTypeArticle block:NULL];
    }];
    
    [shareViewController actionClickWithJubaoBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        ReportViewController *reportVC = [[ReportViewController alloc]init];
        [reportVC showInView:strongSelf.parentViewController type:BY_THEME_TYPE_ARTICLE itemId:articleModel._id];
    }];
    
    [shareViewController actionClickWithCollectionBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        articleModel.collection_status = !articleModel.collection_status;
    }];
    [shareViewController showInView:self.parentViewController];
}

@end
