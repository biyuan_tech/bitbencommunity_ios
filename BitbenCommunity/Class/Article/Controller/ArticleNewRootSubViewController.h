//
//  ArticleNewRootSubViewController.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/18.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "AbstractViewController.h"
#import "NetworkAdapter+Article.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleNewRootSubViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferTags;
@property (nonatomic,assign)ArticleListType transferType;

-(void)startLoadInterfaceManager;                                   /**< 开始加载内容*/

@end

NS_ASSUME_NONNULL_END
