//
//  ArticleQuicklyInfoReleaseViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/12/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleQuicklyInfoReleaseViewController.h"
#import "ArticleImageSelectedTableViewCell.h"
#import "NetworkAdapter+Article.h"
#import "GWAssetsImgSelectedViewController.h"
#import "ArticleReleaseInputToolBar.h"                          // ToolBar
#import "ACEExpandableTextCell.h"
#import "ArticleDetailHuatiListViewController.h"
#import "ArticleDetailTagsTableViewCell.h"
#import "ArticleQuicklyInfoTitleReleaseTableViewCell.h"

static char actionWithReleaseSuccessedBlockKey;
@interface ArticleQuicklyInfoReleaseViewController ()<UITableViewDelegate,UITableViewDataSource,ACEExpandableTableViewDelegate,ArticleImageSelectedTableViewCellDelegate>{
    NSString *inputTextStr;
    CGFloat _cellHeight[2];
    
}
@property (nonatomic,strong)NSMutableArray *photoMutableArr;
@property (nonatomic,strong)UITableView *releaseTableView;
@property (nonatomic,strong)NSMutableArray *releaseMutableArr;
@property (nonatomic,strong)NSMutableArray *tagsMutableArr;
@property (nonatomic,strong)ArticleReleaseInputToolBar *inputTooBar;
@property (nonatomic,strong)ArticleImageSelectedTableViewCell *selectedViewCell;
@property (nonatomic,strong)ArticleQuicklyInfoTitleReleaseTableViewCell *titleCell;
@property (nonatomic,strong)ACEExpandableTextCell *textInputCell;
@property (nonatomic,strong)UIButton *rightButton;
@end

@implementation ArticleQuicklyInfoReleaseViewController


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createInputTooBar];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"发布快讯";
    __weak typeof(self)weakSelf = self;
    
    self.rightButton = [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_article_release_nav_release_nor"] barHltImage:[UIImage imageNamed:@"icon_article_release_nav_release_nor"] action:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 埋点
        [MTAManager event:MTATypeShortRelease params:nil];
        
        [strongSelf sendRequestToAddInfo];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.photoMutableArr = [NSMutableArray array];
    self.releaseMutableArr = [NSMutableArray array];
    [self.releaseMutableArr addObject:@[@"标题"]];
    [self.releaseMutableArr addObject:@[@"输入"]];
    inputTextStr = @"";
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.releaseTableView){
        self.releaseTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.releaseTableView.dataSource = self;
        self.releaseTableView.delegate = self;
        [self.view addSubview:self.releaseTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.releaseMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.releaseMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.releaseMutableArr]){
         static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
         ArticleQuicklyInfoTitleReleaseTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
         if (!cellWithRowZero){
             cellWithRowZero = [[ArticleQuicklyInfoTitleReleaseTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
         }
         __weak typeof(self)weakSelf = self;
         [cellWithRowZero textViewDidPasteChangeWithBlock:^(NSInteger currentCount) {
             if (!weakSelf){
                 return ;
             }
             __strong typeof(weakSelf)strongSelf = weakSelf;
             [strongSelf buttonManager];
         }];
         self.titleCell = cellWithRowZero;
         return cellWithRowZero;
     }else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"输入" sourceArr:self.releaseMutableArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        ACEExpandableTextCell *cellWithRowOne = (ACEExpandableTextCell *)[tableView expandableTextCellWithId:cellIdentifyWithRowOne];
        cellWithRowOne.text = inputTextStr;
        [cellWithRowOne.textView setInputAccessoryView:self.inputTooBar];
        cellWithRowOne.textView.placeholder = @"请输入快讯内容…";
        cellWithRowOne.textView.font = [UIFont fontWithCustomerSizeName:@"13"];
        self.textInputCell = cellWithRowOne;
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"图片" sourceArr:self.releaseMutableArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        ArticleImageSelectedTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[ArticleImageSelectedTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.delegate = self;
        }
        self.selectedViewCell = cellWithRowTwo;
        cellWithRowTwo.selectedImgCount = 1;
        cellWithRowTwo.transferSelectedImgArr = self.photoMutableArr;
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"话题" sourceArr:self.releaseMutableArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        ArticleDetailTagsTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[ArticleDetailTagsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferTagsArr = self.tagsMutableArr;
        __weak typeof(self)weakSelf = self;
        
        [cellWithRowThr actionItemClickWithBlock:^(NSString *info) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if ([info isEqualToString:@"+ 选择话题"]){
                // 埋点
                [MTAManager event:MTATypeShortChooseHuati params:nil];
                
                [strongSelf jinghaoManager];
            } else {
                // 埋点
                NSDictionary *params = @{@"删除话题":info};
                [MTAManager event:MTATypeShortDelHuati params:params];
                
                [strongSelf.tagsMutableArr removeObject:info];
                NSInteger index = [strongSelf cellIndexPathSectionWithcellData:@"话题" sourceArr:strongSelf.releaseMutableArr];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
                [strongSelf.releaseTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
            }
        }];
        return cellWithRowThr;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.releaseMutableArr]){
    return [ArticleQuicklyInfoTitleReleaseTableViewCell calculationCellHeight];
  } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"输入" sourceArr:self.releaseMutableArr]){
    return MAX(3 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]], _cellHeight[indexPath.section]);
  } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"话题" sourceArr:self.releaseMutableArr]){
    return [ArticleDetailTagsTableViewCell calculationCellHeightWithArr:self.tagsMutableArr];
  } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"图片" sourceArr:self.releaseMutableArr]){
    return [ArticleImageSelectedTableViewCell calculationCellHeightWithImgArr:self.photoMutableArr];
  }
    return 44;
}

- (void)tableView:(UITableView *)tableView updatedHeight:(CGFloat)height atIndexPath:(NSIndexPath *)indexPath {
    _cellHeight[indexPath.section] = height;
}

- (void)tableView:(UITableView *)tableView updatedText:(NSString *)text atIndexPath:(NSIndexPath *)indexPath {
    inputTextStr = text;
    [self buttonManager];
    self.inputTooBar.currentInputCount = text.length;
}


#pragma mark - createInputToolBar
-(ArticleReleaseInputToolBar *)createInputTooBar{
    if (!self.inputTooBar){
        self.inputTooBar = [[ArticleReleaseInputToolBar alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(44))];
        self.inputTooBar.transferBarType = ArticleReleaseInputToolBarTypeQuicklyInfo;
        // 图片
        __weak typeof(self)weakSelf = self;
        [self.inputTooBar actionClickImgSelectedblock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            // 埋点
            [MTAManager event:MTATypeShortChooseImg params:nil];
            
            [strongSelf imageSelectedButtonClick];
        }];
    }
    return self.inputTooBar;
}


#pragma mark - 井号Manager
-(void)jinghaoManager{
    ArticleDetailHuatiListViewController *huatiListVC = [[ArticleDetailHuatiListViewController alloc]init];
    huatiListVC.transferSelectedHuatiArr = self.tagsMutableArr;
    huatiListVC.maxCount = 3;
    __weak typeof(self)weakSelf = self;
    [huatiListVC actionClickWithSelectedBlock:^(NSArray *selectedArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.tagsMutableArr removeAllObjects];
        [strongSelf.tagsMutableArr addObjectsFromArray:selectedArr];
        [strongSelf.tagsMutableArr addObject:@"+ 选择话题"];
        
        if (strongSelf.tagsMutableArr.count){
            if (![strongSelf.releaseMutableArr containsObject:@[@"话题"]]){
                NSInteger insertIndex = [self cellIndexPathSectionWithcellData:@"输入" sourceArr:self.releaseMutableArr] + 1;
                [strongSelf.releaseMutableArr insertObject:@[@"话题"] atIndex:insertIndex];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:insertIndex];
                [strongSelf.releaseTableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
            } else {
                NSInteger index = [self cellIndexPathSectionWithcellData:@"话题" sourceArr:self.releaseMutableArr];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
                [strongSelf.releaseTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
            }
        } else {            // 没有话题
            if ([strongSelf.releaseMutableArr containsObject:@[@"话题"]]){
                NSInteger insertIndex = [self cellIndexPathSectionWithcellData:@"话题" sourceArr:self.releaseMutableArr];
                [strongSelf.releaseMutableArr removeObjectAtIndex:insertIndex];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:insertIndex];
                [strongSelf.releaseTableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
    }];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:huatiListVC];
    [self.navigationController presentViewController:nav animated:YES completion:NULL];
}

#pragma mark - ImgSelectedDelegate
-(void)imageSelectedButtonClick{
    GWAssetsLibraryViewController *assetViewController = [[GWAssetsLibraryViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [assetViewController selectImageArrayFromImagePickerWithMaxSelected: 1 - self.photoMutableArr.count andBlock:^(NSArray *selectedImgArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.photoMutableArr addObjectsFromArray:selectedImgArr];
        strongSelf.selectedViewCell.transferSelectedImgArr = strongSelf.photoMutableArr;
        strongSelf.selectedViewCell.size_height = [ArticleImageSelectedTableViewCell calculationCellHeightWithImgArr:strongSelf.photoMutableArr];
        
        if (strongSelf.photoMutableArr.count){          // 显示出cell
            strongSelf.inputTooBar.imgSelectedButton.userInteractionEnabled = NO;
            if (![strongSelf.releaseMutableArr containsObject:@[@"图片"]]){
                // 1. 判断是否有标签
                NSInteger index = [self cellIndexPathSectionWithcellData:@"话题" sourceArr:self.releaseMutableArr];
                if (index != - 1){      // 表示有话题
                    [strongSelf.releaseMutableArr insertObject:@[@"图片"] atIndex:index + 1];
                    [strongSelf.releaseTableView insertSections:[NSIndexSet indexSetWithIndex:index + 1] withRowAnimation:UITableViewRowAnimationNone];
                } else {                // 没有话题
                    NSInteger indexInput = [self cellIndexPathSectionWithcellData:@"输入" sourceArr:self.releaseMutableArr] + 1;
                    [strongSelf.releaseMutableArr insertObject:@[@"图片"] atIndex:indexInput];
                    [strongSelf.releaseTableView insertSections:[NSIndexSet indexSetWithIndex:indexInput] withRowAnimation:UITableViewRowAnimationNone];
                }
            }
        } else {
            strongSelf.inputTooBar.imgSelectedButton.userInteractionEnabled = YES;
        }
    }];
    [self.navigationController pushViewController:assetViewController animated:YES];
}

-(void)imageSelectedButtonClick:(ArticleImageSelectedTableViewCell *)cell{
    [self imageSelectedButtonClick];
}

-(void)imageSelectedDeleteClick:(NSInteger)imgIndex imageView:(UIView *)imgView andSuperCell:(ArticleImageSelectedTableViewCell *)cell{
    [self.photoMutableArr removeObjectAtIndex:imgIndex];
    self.selectedViewCell.transferSelectedImgArr = self.photoMutableArr;
    self.selectedViewCell.size_height = [ArticleImageSelectedTableViewCell calculationCellHeightWithImgArr:self.photoMutableArr];
    
    if (!self.photoMutableArr.count){
        self.inputTooBar.imgSelectedButton.userInteractionEnabled = YES;
        if ([self.releaseMutableArr containsObject:@[@"图片"]]){
            NSInteger index = [self.releaseMutableArr indexOfObject:@[@"图片"]];
            [self.releaseMutableArr removeObject:@[@"图片"]];
            [self.releaseTableView deleteSections:[NSIndexSet indexSetWithIndex:index] withRowAnimation:UITableViewRowAnimationLeft];
        }
    } else {
        self.inputTooBar.imgSelectedButton.userInteractionEnabled = NO;
    }
}

-(void)imageSelectedShowAll:(NSInteger)imgIndex imageView:(UIView *)imgView andSuperCell:(ArticleImageSelectedTableViewCell *)cell{
    GWAssetsImgSelectedViewController *imgSelected = [[GWAssetsImgSelectedViewController alloc]init];
    [imgSelected showInView:self.parentViewController imgArr:self.photoMutableArr currentIndex:imgIndex cell:cell];
}

#pragma mark - 接口
-(void)sendRequestToReleaseInfo{
    __weak typeof(self)weakSelf = self;
    if (!inputTextStr.length){
        [StatusBarManager statusBarHidenWithText:@"请输入图文内容"];
        return;
    }
    
    NSMutableArray *tagTempsMutableArr = [NSMutableArray array];
    [tagTempsMutableArr addObjectsFromArray:self.tagsMutableArr];
    if ([tagTempsMutableArr containsObject:@"+ 选择话题"]){
        [tagTempsMutableArr removeObject:@"+ 选择话题"];
    }
    
    if (tagTempsMutableArr.count <= 0){
        [StatusBarManager statusBarHidenWithText:@"请选择话题"];
        return;
    }
    
    [[NetworkAdapter sharedAdapter] articleAddWithImgList:self.photoMutableArr articleType:ArticleTypeShort topicArr:tagTempsMutableArr title:inputTextStr content:inputTextStr subTitle:@"" block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        [strongSelf.navigationController dismissViewControllerAnimated:YES completion:^{
            void(^block)() = objc_getAssociatedObject(strongSelf, &actionWithReleaseSuccessedBlockKey);
            if (block){
                block();
            }
        }];
    }];
}

-(void)actionWithReleaseSuccessedBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionWithReleaseSuccessedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if ([self.textInputCell.textView isFirstResponder]){
        [self.textInputCell.textView resignFirstResponder];
    }
}

-(void)buttonManager{
    if (self.textInputCell.textView.text.length > 0 && self.titleCell.inputTextView.text.length > 0){
        [self.rightButton setImage:[UIImage imageNamed:@"icon_article_release_nav_release_hlt"] forState:UIControlStateNormal];
        self.rightButton.enabled = YES;
    } else {
        [self.rightButton setImage:[UIImage imageNamed:@"icon_article_release_nav_release_nor"] forState:UIControlStateNormal];
        self.rightButton.enabled = NO;
    }
}

-(void)sendRequestToAddInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] addQuicklyInfoManagerWithTitle:self.titleCell.inputTextView.text content:inputTextStr img:[self.photoMutableArr firstObject] block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        [strongSelf.navigationController dismissViewControllerAnimated:YES completion:^{
            void(^block)() = objc_getAssociatedObject(strongSelf, &actionWithReleaseSuccessedBlockKey);
            if (block){
                block();
            }
        }];
    }];

    

}
@end
