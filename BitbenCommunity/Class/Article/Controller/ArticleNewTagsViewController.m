//
//  ArticleNewTagsViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/6/17.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "ArticleNewTagsViewController.h"
#import "ArticleNewTagsTableViewCell.h"
#import "ArticleNewTagsTableViewCell.h"
#import "NetworkAdapter+Article.h"

@interface ArticleNewTagsViewController ()<UITableViewDelegate,UITableViewDataSource>{
    ArticleDetailHuatiListModel *tempListModel;
}
@property (nonatomic,strong)UITableView *huatiListTableView;
@property (nonatomic,strong)NSMutableArray *huatiMutableArr;
@property (nonatomic,strong)NSMutableArray *selectedMutableArr;
@property (nonatomic,strong)UIButton *succesButton;

@end

@implementation ArticleNewTagsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetListInfo];
}


#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"选择话题";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.huatiMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.huatiListTableView){
        self.huatiListTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.huatiListTableView.dataSource = self;
        self.huatiListTableView.delegate = self;
        [self.view addSubview:self.huatiListTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        ArticleNewTagsTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[ArticleNewTagsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
//        cellWithRowOne.transferHuatiArr = self.;
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            
            
        }];
        
        return cellWithRowOne;
    }
}


#pragma mark - Info
-(void)sendRequestToGetListInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] sendRequestToGetHuatiListManagerBlock:^(BOOL isSuccessed, ArticleDetailHuatiListModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            strongSelf->tempListModel = listModel;
            if (strongSelf.huatiMutableArr.count){
                [strongSelf.huatiMutableArr removeAllObjects];
            }
            [strongSelf.huatiMutableArr addObjectsFromArray:listModel.content];
            
            for (int i = 0 ; i < strongSelf.transferSelectedHuatiArr.count;i++){
                NSString *selectedStr = [strongSelf.transferSelectedHuatiArr objectAtIndex:i];
                for (int j = 0 ; j < listModel.content.count;j++){
                    ArticleDetailHuatiListSingleModel *singleModel = [listModel.content objectAtIndex:j];
                    if ([singleModel.content isEqualToString:selectedStr]){
                        [strongSelf.selectedMutableArr addObject:singleModel];
                    }
                }
            }
            
            [strongSelf.huatiListTableView reloadData];
            
            if (strongSelf.huatiMutableArr.count){
                [strongSelf.huatiListTableView dismissPrompt];
            } else {
                [strongSelf.huatiListTableView showPrompt:@"当前没有话题" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }
        } else {
            
        }
    }];
}

@end
