//
//  RankingRootViewController.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/22.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "RankingRootViewController.h"
#import "RankHeaderTableViewCell.h"
#import "RankNormalModel.h"
#import "NetworkAdapter+Article.h"
#import "RankNormalTableViewCell.h"
#import "BYPersonHomeController.h"

@interface RankingRootViewController ()<UITableViewDelegate,UITableViewDataSource>{
    RankNormalModel *mainModel;
    ArticleRootRankModel *myRank;
}
@property (nonatomic,strong)UITableView *rankTableView;
@property (nonatomic,strong)NSMutableArray *rankRootMutableArr;
@property (nonatomic,strong)NSMutableArray *rankTopMutableArr;
@property (nonatomic,strong)NSMutableArray *rankSubMutableArr;
@property (nonatomic,strong)UIView *navView;
@property (nonatomic,strong)UIButton *navButton;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)RankNormalTableViewCell *mineView;

@end

@implementation RankingRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)createNavView{
    if (!self.navView){
        self.navView = [[UIView alloc]init];
        self.navView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [BYTabbarViewController sharedController].navBarHeight);
        self.navView.backgroundColor = [UIColor whiteColor];
        self.navView.alpha = 0;
        [self.view addSubview:self.navView];
        
        self.navButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.navButton.frame = CGRectMake(LCFloat(11), [BYTabbarViewController sharedController].statusHeight, 44, 44);
        __weak typeof(self)weakSelf = self;
        [self.navButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }];
        [self.navButton setImage:[UIImage imageNamed:@"icon_rank_nav_back"] forState:UIControlStateNormal];
        [self.view addSubview:self.navButton];
        
        self.titleLabel = [GWViewTool createLabelFont:@"16" textColor:@"白"];
        [self.view addSubview:self.titleLabel];
        self.titleLabel.frame = CGRectMake(LCFloat(70), [BYTabbarViewController sharedController].statusHeight, kScreenBounds.size.width - 2 * LCFloat(70), 44);
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.text = @"观点达人排行榜";
    }
}

#pragma mark - createMineView
-(void)createMineView{
    self.mineView = [[RankNormalTableViewCell alloc]init];
    self.mineView.frame = CGRectMake(0, kScreenBounds.size.height, kScreenBounds.size.width, [RankNormalTableViewCell calculationCellHeight]);
    self.mineView.hasMine = YES;
    [self.view addSubview:self.mineView];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createView];
    [self getRankListInfoWithReload:YES];
    [self createNavView];
    [self createMineView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"币本达人周排行榜";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.rankRootMutableArr = [NSMutableArray array];
    self.rankSubMutableArr = [NSMutableArray array];
    self.rankTopMutableArr = [NSMutableArray array];
    [self.rankRootMutableArr addObject:@[@"头部"]];
    [self.rankRootMutableArr addObject:self.rankSubMutableArr];
}

#pragma mark - createView
-(void)createView{
    if (!self.rankTableView){
        self.rankTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.rankTableView.dataSource = self;
        self.rankTableView.delegate = self;
        [self.view addSubview:self.rankTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.rankTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf getRankListInfoWithReload:NO];
    }];
    [self.rankTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf getRankListInfoWithReload:YES];
    }];
    
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.rankRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.rankRootMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        RankHeaderTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[RankHeaderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferPersonArr = [self.rankTopMutableArr copy];
        cellWithRowOne.transferUploadTime = mainModel.timestamp / 1000.;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickWithAvatar:^(NSString * _Nonnull userId) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = userId;
            [strongSelf.navigationController pushViewController:personHomeController animated:YES];
        }];
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        RankNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[RankNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        ArticleRootRankModel *model = [self.rankSubMutableArr objectAtIndex:indexPath.row];
        cellWithRowTwo.transferRankModel = model;
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo actionClickWithAvatarBlock:^(NSString * _Nonnull userId) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
            personHomeController.user_id = model.user_id;
            [strongSelf.navigationController pushViewController:personHomeController animated:YES];
        }];
        
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return LCFloat(537);
    } else {
        return LCFloat(69);
    }
}

#pragma mark - Interface
-(void)getRankListInfoWithReload:(BOOL)reload{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] rankRankListManagerPage:self.rankTableView.currentPage block:^(RankNormalModel *rankListModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (reload || strongSelf.rankTableView.isXiaLa){
            [strongSelf.rankSubMutableArr removeAllObjects];
            [strongSelf.rankTopMutableArr removeAllObjects];
        }
        
        if (rankListModel.my_rank){
            strongSelf->myRank = rankListModel.my_rank;
        }
        
        strongSelf->mainModel = rankListModel;
        for (int i = 0 ; i < rankListModel.page_rank.content.count;i++){
            ArticleRootRankModel *model = [rankListModel.page_rank.content objectAtIndex:i];
            if (model.rank < 4){
                [strongSelf.rankTopMutableArr addObject:model];
            } else {
                [strongSelf.rankSubMutableArr addObject:model];
            }
        }
        [strongSelf.rankTableView reloadData];
        
        if (reload){
            [strongSelf.rankTableView stopPullToRefresh];
        } else {
            [strongSelf.rankTableView stopFinishScrollingRefresh];
        }
        
        [self rankMineFrameInfoSetting];
    }];
}

-(void)rankMineFrameInfoSetting{
    if (myRank.user_id.length){
        self.mineView.transferRankModel = myRank;
        self.mineView.frame = CGRectMake(0, self.view.size_height - self.mineView.size_height, kScreenBounds.size.width, self.mineView.size_height);
        self.rankTableView.frame = CGRectMake(0,0, kScreenBounds.size.width, kScreenBounds.size.height - [RankNormalTableViewCell calculationCellHeight]);
    } else {
        self.mineView.orgin_y = kScreenBounds.size.height;
        self.rankTableView.size_height = self.view.size_height;
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.rankTableView){
        CGPoint point = scrollView.contentOffset;
        if (point.y > self.navView.size_height){
            self.navView.alpha = 1;
            self.titleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            
            [self.navButton setImage:[UIImage imageNamed:@"common_barItem_back"] forState:UIControlStateNormal];
        } else {
            self.navView.alpha = 0;
            self.titleLabel.textColor = [UIColor colorWithCustomerName:@"白"];
            [self.navButton setImage:[UIImage imageNamed:@"icon_rank_nav_back"] forState:UIControlStateNormal];
        }
    }
}

@end
