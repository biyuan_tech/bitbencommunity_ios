//
//  RankTopView.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/22.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "RankTopView.h"

@interface RankTopView()
@property (nonatomic,strong)PDImageView *lightImgView;
@property (nonatomic,strong)PDImageView *topImgView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)PDImageView *biImgView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *biLabel;

@end

@implementation RankTopView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark -
-(void)createView{
    self.lightImgView = [[PDImageView alloc]init];
    self.lightImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.lightImgView];
    
    self.topImgView = [[PDImageView alloc]init];
    self.topImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.topImgView];
    
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.biImgView = [[PDImageView alloc]init];
    self.biImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.biImgView];
    
    self.biLabel = [GWViewTool createLabelFont:@"13" textColor:@"FFFFFF"];
    [self addSubview:self.biLabel];
    
    self.nameLabel = [GWViewTool createLabelFont:@"15" textColor:@"FFFFFF"];
    self.nameLabel.font = [self.nameLabel.font boldFont];
    [self addSubview:self.nameLabel];
}

-(void)setTransferType:(RankTopViewType)transferType{
    _transferType = transferType;
}

-(void)setTransferRankModel:(ArticleRootRankModel *)transferRankModel{
    _transferRankModel = transferRankModel;
//
    if (self.transferType == RankTopViewTypeOne){
        self.lightImgView.image = [UIImage imageNamed:@"bg_rank_top_light"];
        self.lightImgView.frame = self.bounds;
        self.topImgView.image = [UIImage imageNamed:@"bg_rank_top_number_1"];
        
        self.topImgView.frame = CGRectMake(([RankTopView calculationSizeWithType:RankTopViewTypeOne].width - LCFloat(80)) / 2., LCFloat(32), LCFloat(80), LCFloat(80));
        self.avatarImgView.frame = CGRectMake(0, 0, LCFloat(65), LCFloat(65));
    } else if (self.transferType == RankTopViewTypeTwo){
        self.lightImgView.image = nil;
        self.topImgView.image = [UIImage imageNamed:@"bg_rank_top_number_2"];
        
        self.topImgView.frame = CGRectMake((self.size_width - LCFloat(73)) / 2., 0, LCFloat(73), LCFloat(73));
        self.avatarImgView.frame = CGRectMake(0, 0, LCFloat(58), LCFloat(58));
    } else if (self.transferType == RankTopViewTypeThr){
        self.lightImgView.image = nil;
        self.topImgView.image = [UIImage imageNamed:@"bg_rank_top_number_3"];
        
        self.topImgView.frame = CGRectMake((self.size_width - LCFloat(73)) / 2., 0, LCFloat(73), LCFloat(73));
        self.avatarImgView.frame = CGRectMake(0, 0, LCFloat(58), LCFloat(58));

    }
    
    
    self.avatarImgView.center = self.topImgView.center;
    [self.avatarImgView uploadMainImageWithURL:transferRankModel.head_img placeholder:nil imgType:PDImgTypeRound callback:NULL];
    
    self.nameLabel.text = transferRankModel.nickname;
    CGSize nameSize = [Tool makeSizeWithLabel:self.nameLabel];
    self.nameLabel.frame = CGRectMake((self.size_width - MIN(nameSize.width, self.size_width)) / 2., CGRectGetMaxY(self.topImgView.frame) + LCFloat(12), MIN(nameSize.width, self.size_width), [NSString contentofHeightWithFont:self.nameLabel.font]);
    
    self.biImgView.image = [UIImage imageNamed:@"icon_article_detail_items_bi"];
    
    self.biLabel.text = transferRankModel.mine_fund;
    CGSize biSize = [Tool makeSizeWithLabel:self.biLabel];
    CGFloat margin = (self.size_width - biSize.width - LCFloat(18) - LCFloat(3)) / 2.;
    self.biImgView.frame = CGRectMake(margin, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(10), LCFloat(18), LCFloat(18));
    self.biLabel.frame = CGRectMake(CGRectGetMaxX(self.biImgView.frame) + LCFloat(3), 0, biSize.width, biSize.height);
    self.biLabel.center_y = self.biImgView.center_y;

}


+(CGSize)calculationSizeWithType:(RankTopViewType)type{
    if (type == RankTopViewTypeOne){
        return CGSizeMake(LCFloat(143),LCFloat(166));
    } else {
        CGFloat height = 0;
        height += LCFloat(73);
        height += LCFloat(12);
        height += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"15"]];
        height += LCFloat(13);
        height += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]];
        height += LCFloat(5);
        return CGSizeMake(LCFloat(90),height);
    }
    
}


@end
