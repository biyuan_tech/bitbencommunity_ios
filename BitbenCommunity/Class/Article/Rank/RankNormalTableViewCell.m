
//
//  RankNormalTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/22.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "RankNormalTableViewCell.h"

static char actionClickWithAvatarBlockKey;
@interface RankNormalTableViewCell()
@property (nonatomic,strong)PDImageView *bgView;
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)PDImageView *avatarConverImgView;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)PDImageView *biImgView;
@property (nonatomic,strong)UILabel *biLabel;
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UIButton *avatarButton;

@end

@implementation RankNormalTableViewCell

-(instancetype)init{
    self = [super init];
    if (self){
        [self createView];
    }
    return self;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    
    self.bgView = [[PDImageView alloc]init];
    self.bgView.image = [UIImage imageNamed:@"bg_rank_my"];
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [RankNormalTableViewCell calculationCellHeight]);
    self.bgView.alpha = 0;
    [self addSubview:self.bgView];
    
    
    self.numberLabel = [GWViewTool createLabelFont:@"15" textColor:@"8F8F8F"];
    [self addSubview:self.numberLabel];
    
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.frame = CGRectMake(LCFloat(50), ([RankNormalTableViewCell calculationCellHeight] - LCFloat(44)) / 2., LCFloat(44), LCFloat(44));
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.avatarConverImgView = [[PDImageView alloc]init];
    self.avatarConverImgView.backgroundColor = [UIColor clearColor];
    self.avatarConverImgView.image = [UIImage imageNamed:@"bg_avatar_convert"];
    [self addSubview:self.avatarConverImgView];
    
    self.avatarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.avatarButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarButton];
    
    
    self.nickNameLabel = [GWViewTool createLabelFont:@"14" textColor:@"323232"];
    [self addSubview:self.nickNameLabel];
    
    self.biImgView = [[PDImageView alloc]init];
    self.biImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.biImgView];
    
    self.biLabel = [GWViewTool createLabelFont:@"13" textColor:@"323232"];
    [self addSubview:self.biLabel];
    
    self.fixedLabel = [GWViewTool createLabelFont:@"13" textColor:@"323232"];
    [self addSubview:self.fixedLabel];
}

-(void)setHasMine:(BOOL)hasMine{
    _hasMine = hasMine;
}

-(void)setTransferRankModel:(ArticleRootRankModel *)transferRankModel{
    _transferRankModel = transferRankModel;
    
    // 1. avatar
    self.avatarConverImgView.frame = self.avatarImgView.frame;
    
    // button
    self.avatarButton.frame = self.avatarImgView.frame;
    __weak typeof(self)weakSelf = self;
    [self.avatarButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSString *userId) = objc_getAssociatedObject(strongSelf, &actionClickWithAvatarBlockKey);
        if (block){
            block(transferRankModel.user_id);
        }
    }];
    
    
    // 2. number
    self.numberLabel.frame = CGRectMake(0, 0, self.avatarImgView.orgin_x, [RankNormalTableViewCell calculationCellHeight]);
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    if (transferRankModel.rank < 10){
        self.numberLabel.text = [NSString stringWithFormat:@"0%li",(long)transferRankModel.rank];
    } else if (transferRankModel.rank > 100 && self.hasMine){
        self.numberLabel.text = @"100+";
    } else {
        self.numberLabel.text = [NSString stringWithFormat:@"%li",(long)transferRankModel.rank];
    }
    
    // 3. name
    self.nickNameLabel.text = transferRankModel.nickname;
    CGSize nickSize = [Tool makeSizeWithLabel:self.nickNameLabel];
    self.nickNameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(13), 0, nickSize.width, [RankNormalTableViewCell calculationCellHeight]);
    
    // bi Label
    self.biLabel.text = transferRankModel.mine_fund;
    CGSize biSize = [Tool makeSizeWithLabel:_biLabel];
    self.biLabel.frame = CGRectMake(kScreenBounds.size.width - biSize.width - LCFloat(15), LCFloat(19), biSize.width, biSize.height);
    
    self.biImgView.image = [UIImage imageNamed:@"icon_article_detail_items_bi"];
    self.biImgView.frame = CGRectMake(self.biLabel.orgin_x - LCFloat(18) - LCFloat(3), 0, LCFloat(18), LCFloat(18));
    self.biImgView.center_y = self.biLabel.center_y;
    
    self.fixedLabel.text =  @"累计获得BP奖励";
    CGSize fixedSize = [Tool makeSizeWithLabel:self.fixedLabel];
    self.fixedLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - fixedSize.width, [RankNormalTableViewCell calculationCellHeight] - LCFloat(15) - fixedSize.height, fixedSize.width, fixedSize.height);
    
    
    if (self.hasMine){
        self.bgView.alpha = 1;
        
        self.avatarConverImgView.hidden = YES;
        __weak typeof(self)weakSelf = self;
        [self.avatarImgView uploadHDImageWithURL:transferRankModel.head_img callback:^(UIImage *image) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.avatarImgView.image =  [Tool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round.png"]];
        }];
        

        self.numberLabel.textColor = [UIColor whiteColor];
        self.nickNameLabel.textColor = [UIColor whiteColor];
        self.biLabel.textColor = [UIColor whiteColor];
        self.fixedLabel.textColor = [UIColor whiteColor];
    } else {
        self.bgView.alpha = 0;
        [self.avatarImgView uploadHDImageWithURL:transferRankModel.head_img callback:NULL];
        
        self.avatarConverImgView.hidden = NO;
        self.numberLabel.textColor = [UIColor hexChangeFloat:@"8F8F8F"];
        self.nickNameLabel.textColor = [UIColor hexChangeFloat:@"8F8F8F"];
        self.biLabel.textColor = [UIColor  hexChangeFloat:@"8F8F8F"];
        self.fixedLabel.textColor = [UIColor hexChangeFloat:@"8F8F8F"];
    }
}

-(void)actionClickWithAvatarBlock:(void(^)(NSString *userId))block{
    objc_setAssociatedObject(self, &actionClickWithAvatarBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


+(CGFloat)calculationCellHeight{
    return LCFloat(68);
}
@end
