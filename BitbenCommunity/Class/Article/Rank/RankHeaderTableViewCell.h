//
//  RankHeaderTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/22.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface RankHeaderTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)NSArray *transferPersonArr;
@property (nonatomic,assign)NSTimeInterval transferUploadTime;


-(void)actionClickWithAvatar:(void(^)(NSString *userId))block;

@end

NS_ASSUME_NONNULL_END
