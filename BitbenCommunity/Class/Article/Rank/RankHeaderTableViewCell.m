//
//  RankHeaderTableViewCell.m
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/22.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "RankHeaderTableViewCell.h"
#import "RankTopView.h"

static char actionClickWithAvatarKey;
@interface RankHeaderTableViewCell()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)PDImageView *paihangbangImgView;
@property (nonatomic,strong)UILabel *reloadLabel;

@property (nonatomic,strong)PDImageView *oneImgView;
@property (nonatomic,strong)PDImageView *twoImgView;
@property (nonatomic,strong)PDImageView *thrImgView;

@property (nonatomic,strong)RankTopView *oneTopView;
@property (nonatomic,strong)RankTopView *twoTopView;
@property (nonatomic,strong)RankTopView *thrTopView;

@property (nonatomic,strong)UIButton *oneBtn;
@property (nonatomic,strong)UIButton *twoBtn;
@property (nonatomic,strong)UIButton *thrBtn;


@end

@implementation RankHeaderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.bgImgView.image = [UIImage imageNamed:@"bg_rank_list"];
    self.backgroundView = self.bgImgView;
    
    self.paihangbangImgView = [[PDImageView alloc]init];
    self.paihangbangImgView.backgroundColor = [UIColor clearColor];
    self.paihangbangImgView.image = [UIImage imageNamed:@"icon_rank_top_guandiandaren"];
    self.paihangbangImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(255)) / 2., LCFloat(83), LCFloat(255), LCFloat(76));
    [self addSubview:self.paihangbangImgView];
    
    self.reloadLabel = [GWViewTool createLabelFont:@"13" textColor:@"CCB1FF"];
    self.reloadLabel.textAlignment = NSTextAlignmentCenter;
    self.reloadLabel.text = @"本期排行更新时间：";
    self.reloadLabel.frame = CGRectMake(0, CGRectGetMaxY(self.paihangbangImgView.frame) + LCFloat(20), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.reloadLabel.font]);
    [self addSubview:self.reloadLabel];
    
    
    self.twoImgView = [[PDImageView alloc]init];
    self.twoImgView.image = [UIImage imageNamed:@"rank_top_number_2"];
    self.twoImgView.frame = CGRectMake(LCFloat(15), [RankHeaderTableViewCell calculationCellHeight] - LCFloat(120), LCFloat(120), LCFloat(120));
    [self addSubview:self.twoImgView];
    
    
    self.thrImgView = [[PDImageView alloc]init];
    self.thrImgView.image = [UIImage imageNamed:@"rank_top_number_3"];
    self.thrImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(120), [RankHeaderTableViewCell calculationCellHeight] - LCFloat(94), LCFloat(120), LCFloat(94));
    [self addSubview:self.thrImgView];
    
    self.oneImgView = [[PDImageView alloc]init];
    self.oneImgView.image = [UIImage imageNamed:@"rank_top_number_1"];
    self.oneImgView.frame = CGRectMake(0, [RankHeaderTableViewCell calculationCellHeight] - LCFloat(180), LCFloat(135), LCFloat(180));
    self.oneImgView.center_x = kScreenBounds.size.width / 2.;
    [self addSubview:self.oneImgView];
    
    self.oneTopView = [[RankTopView alloc]initWithFrame:CGRectMake((kScreenBounds.size.width - [RankTopView calculationSizeWithType:RankTopViewTypeOne].width) / 2., self.oneImgView.orgin_y - [RankTopView calculationSizeWithType:RankTopViewTypeOne].height, [RankTopView calculationSizeWithType:RankTopViewTypeOne].width, [RankTopView calculationSizeWithType:RankTopViewTypeOne].height)];
    self.oneTopView.transferType = RankTopViewTypeOne;
    [self addSubview:self.oneTopView];
    
    self.twoTopView = [[RankTopView alloc] initWithFrame:CGRectMake(LCFloat(15), self.twoImgView.orgin_y - [RankTopView calculationSizeWithType:RankTopViewTypeTwo].height, LCFloat(105), [RankTopView calculationSizeWithType:RankTopViewTypeTwo].height)];
    self.twoTopView.transferType = RankTopViewTypeTwo;
    [self addSubview:self.twoTopView];
    
    self.thrTopView = [[RankTopView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(105), self.thrImgView.orgin_y - [RankTopView calculationSizeWithType:RankTopViewTypeThr].height, LCFloat(105), [RankTopView calculationSizeWithType:RankTopViewTypeThr].height)];
    self.thrTopView.transferType = RankTopViewTypeThr;
    [self addSubview:self.thrTopView];

    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i < 3 ; i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        if (i == 0){
            self.oneBtn = button;
            self.oneBtn.frame = self.oneTopView.frame;
        } else if (i == 1){
            self.twoBtn = button;
            self.twoBtn.frame = self.twoTopView.frame;
        } else if (i == 2){
            self.thrBtn = button;
            self.thrBtn.frame = self.thrTopView.frame;
        }
        [button buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(NSString *avatar) = objc_getAssociatedObject(strongSelf, &actionClickWithAvatarKey);
            if (block){
                if (button == strongSelf.oneBtn){
                    block(strongSelf.oneTopView.transferRankModel.user_id);
                } else if (button == strongSelf.twoBtn){
                    block(strongSelf.twoTopView.transferRankModel.user_id);
                } else if (button == strongSelf.thrBtn){
                    block(strongSelf.thrTopView.transferRankModel.user_id);
                }
            }
        }];
        
        [self addSubview:button];
    }
}

-(void)setTransferPersonArr:(NSArray *)transferPersonArr{
    _transferPersonArr = transferPersonArr;
    if(transferPersonArr.count >= 1){
        self.oneTopView.transferRankModel = [transferPersonArr objectAtIndex:0];
    }
    if (transferPersonArr.count >= 2){
        self.twoTopView.transferRankModel = [transferPersonArr objectAtIndex:1];
    }
    
    if (transferPersonArr.count >= 3){
        self.thrTopView.transferRankModel = [transferPersonArr objectAtIndex:2];
    }
    
}

-(void)setTransferUploadTime:(NSTimeInterval)transferUploadTime{
    _transferUploadTime = transferUploadTime;
    self.reloadLabel.text = [NSString stringWithFormat:@"本期排行更新时间：%@",[NSDate getTimeWithDuobaoString:transferUploadTime]];
    
}

+(CGFloat)calculationCellHeight{
    return LCFloat(537);
}

-(void)actionClickWithAvatar:(void(^)(NSString *userId))block{
    objc_setAssociatedObject(self, &actionClickWithAvatarKey, block, OBJC_ASSOCIATION_COPY);
}

@end
