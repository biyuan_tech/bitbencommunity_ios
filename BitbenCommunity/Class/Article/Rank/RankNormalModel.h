//
//  RankNormalModel.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/22.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "FetchModel.h"
#import "ArticleRootRankModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RankNormalPageRankModel : FetchModel

@property (nonatomic,strong)NSArray<ArticleRootRankModel> *content;



@end

@interface RankNormalModel : FetchModel

@property (nonatomic,strong)ArticleRootRankModel *my_rank;
@property (nonatomic,strong)RankNormalPageRankModel *page_rank;
@property (nonatomic,assign)NSTimeInterval timestamp;

@end





NS_ASSUME_NONNULL_END
