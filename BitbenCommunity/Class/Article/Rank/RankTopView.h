//
//  RankTopView.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/22.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleRootRankModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,RankTopViewType) {
    RankTopViewTypeOne,
    RankTopViewTypeTwo,
    RankTopViewTypeThr,
};

@interface RankTopView : UIView

@property (nonatomic,assign)RankTopViewType transferType;

@property (nonatomic,strong)ArticleRootRankModel *transferRankModel;

+(CGSize)calculationSizeWithType:(RankTopViewType)type;

@end

NS_ASSUME_NONNULL_END
