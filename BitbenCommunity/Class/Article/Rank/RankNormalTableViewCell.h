//
//  RankNormalTableViewCell.h
//  BitbenCommunity
//
//  Created by 裴烨烽 on 2019/7/22.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "ArticleRootRankModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RankNormalTableViewCell : PDBaseTableViewCell

@property (nonatomic,assign)BOOL hasMine;

@property (nonatomic,strong)ArticleRootRankModel *transferRankModel;
-(void)actionClickWithAvatarBlock:(void(^)(NSString *userId))block;


@end

NS_ASSUME_NONNULL_END
