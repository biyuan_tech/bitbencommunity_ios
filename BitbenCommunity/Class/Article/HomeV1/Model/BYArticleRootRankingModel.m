//
//  BYArticleRootRankingModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYArticleRootRankingModel.h"

@implementation BYArticleRootRankingSingleModel


@end

@implementation BYArticleRootRankingModel

+ (BYArticleRootRankingModel *)getRankingData:(NSArray *)respondArr{
    BYArticleRootRankingModel *model = [[BYArticleRootRankingModel alloc] init];
    NSMutableArray *array = [NSMutableArray array];
    BYArticleRootRankingSingleModel *topModel = [[BYArticleRootRankingSingleModel alloc] init];
    topModel.head_img = @"icon_article_root_rank_btn";
    [array addObject:topModel];
    [array addObjectsFromArray:[BYArticleRootRankingSingleModel mj_objectArrayWithKeyValuesArray:respondArr]];
    model.rank_list = [array copy];
    return model;
}

@end
