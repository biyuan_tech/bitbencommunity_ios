//
//  BYArticleRootRankingModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYArticleRootRankingSingleModel : BYCommonModel

/** user_id */
@property (nonatomic ,copy) NSString *user_id;
/** 头像 */
@property (nonatomic ,copy) NSString *head_img;
/** 昵称 */
@property (nonatomic ,copy) NSString *nickname;

@end

@interface BYArticleRootRankingModel : BYCommonModel

/** data */
@property (nonatomic ,strong) NSArray <BYArticleRootRankingSingleModel *>*rank_list;

+ (BYArticleRootRankingModel *)getRankingData:(NSArray *)respondArr;

@end

NS_ASSUME_NONNULL_END
