//
//  BYArticleRootViewController.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/9.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYArticleRootViewController.h"
#import "BYArticleRootViewModel.h"

@interface BYArticleRootViewController ()

@end

@implementation BYArticleRootViewController

- (Class)getViewModelClass{
    return [BYArticleRootViewModel class];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)autoReload{
    [(BYArticleRootViewModel *)self.viewModel autoReload];
}

@end
