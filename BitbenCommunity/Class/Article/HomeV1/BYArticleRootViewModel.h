//
//  BYArticleRootViewModel.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/9.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYCommonViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYArticleRootViewModel : BYCommonViewModel

- (void)autoReload;

@end

NS_ASSUME_NONNULL_END
