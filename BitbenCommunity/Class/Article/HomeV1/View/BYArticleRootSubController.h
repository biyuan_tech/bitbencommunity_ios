//
//  BYArticleRootSubController.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/25.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "HGPageViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYArticleRootSubController : HGPageViewController

/** 话题 */
@property (nonatomic ,copy) NSString *topic;
/** sortType */
@property (nonatomic ,assign) BY_ARTICLE_SORT_TYPE type;

@end

NS_ASSUME_NONNULL_END
