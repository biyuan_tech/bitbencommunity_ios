//
//  BYArticleRootRankingCell.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYArticleRootRankingCell.h"

#import "BYArticleRootRankingModel.h"

@interface BYArticleRootRankingCell ()

/** imageView */
@property (nonatomic ,strong) PDImageView *coverImgView;
/** 遮罩 */
@property (nonatomic ,strong) UIView *maskView;
/** title */
@property (nonatomic ,strong) UILabel *titleLab;


@end

@implementation BYArticleRootRankingCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentView];
    }
    return self;
}

- (void)setContentWithObject:(id)object indexPath:(NSIndexPath *)indexPath{
    BYArticleRootRankingSingleModel *model = object;
    if (indexPath.row == 0) {
        [self.coverImgView setImage:[UIImage imageNamed:model.head_img]];
        self.maskView.hidden = YES;
    }else{
        [self.coverImgView uploadHDImageWithURL:model.head_img callback:nil];
        self.maskView.hidden = NO;
    }
    self.titleLab.text = model.nickname;
}

- (void)setContentView{
    self.coverImgView = [[PDImageView alloc] init];
    self.coverImgView.contentMode = UIViewContentModeScaleAspectFill;
    self.coverImgView.clipsToBounds = YES;
    self.coverImgView.layer.cornerRadius = 5.0f;
    [self.contentView addSubview:self.coverImgView];
    [self.coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    self.maskView = [UIView by_init];
    self.maskView.layer.cornerRadius = 5.0f;
    [self.maskView setBackgroundColor:kColorRGB(69, 69, 69, 0.5)];
    [self.contentView addSubview:self.maskView];
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    self.titleLab = [UILabel by_init];
    [self.titleLab setBy_font:12];
    self.titleLab.textColor = [UIColor whiteColor];
    self.titleLab.numberOfLines = 2.0f;
    [self.contentView addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(5);
        make.top.mas_equalTo(10);
        make.right.mas_equalTo(-5);
        make.height.mas_greaterThanOrEqualTo(0);
    }];
}

@end
