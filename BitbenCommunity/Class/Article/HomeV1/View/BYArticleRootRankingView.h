//
//  BYArticleRootRankingView.h
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BYArticleRootRankingModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BYArticleRootRankingView : UIView

- (void)reloadData:(BYArticleRootRankingModel *)data;

@end

NS_ASSUME_NONNULL_END
