//
//  BYArticleRootRankingView.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/10.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYArticleRootRankingView.h"
#import "BYArticleRootRankingCell.h"

#import "RankingRootViewController.h"
#import "BYPersonHomeController.h"

@interface BYArticleRootRankingView ()<UICollectionViewDelegate,UICollectionViewDataSource>

/** UICollection */
@property (nonatomic ,strong) UICollectionView *collectionView;
/** model */
@property (nonatomic ,strong) BYArticleRootRankingModel *model;


@end

@implementation BYArticleRootRankingView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setContentView];
    }
    return self;
}

- (void)reloadData:(BYArticleRootRankingModel *)data{
    self.model = data;
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDelegate/DataScoure
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.model.rank_list.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BYArticleRootRankingCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    BYArticleRootRankingSingleModel *model = self.model.rank_list[indexPath.row];
    [cell setContentWithObject:model indexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    BYCommonViewController *currentController = (BYCommonViewController *)[BYTabbarViewController sharedController].currentController;
    BYArticleRootRankingSingleModel *model = self.model.rank_list[indexPath.row];
    if (indexPath.row == 0) {
        RankingRootViewController *rankingRootController = [[RankingRootViewController alloc] init];
        [currentController authorizePush:rankingRootController animation:YES];
    }else{
        BYPersonHomeController *personHomeController = [[BYPersonHomeController alloc] init];
        personHomeController.user_id = model.user_id;
        [currentController authorizePush:personHomeController animation:YES];
    }
}

#pragma mark - configUI

- (void)setContentView{
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(60, 60);
    layout.minimumInteritemSpacing = 10.0f;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.headerReferenceSize = CGSizeMake(15, 60);
    layout.footerReferenceSize = CGSizeMake(15, 60);
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [collectionView registerClass:[BYArticleRootRankingCell class] forCellWithReuseIdentifier:@"cell"];
    [collectionView setBackgroundColor:[UIColor whiteColor]];
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.delegate = self;
    collectionView.dataSource = self;
    [self addSubview:collectionView];
    self.collectionView = collectionView;
    [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(15);
        make.height.mas_equalTo(60);
    }];
    
    UIView *lineView = [UIView by_init];
    [lineView setBackgroundColor:kColorRGBValue(0xe7e7ea)];
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
    }];
}
@end
