//
//  BYArticleRootViewModel.m
//  BitbenCommunity
//
//  Created by 随风 on 2019/9/9.
//  Copyright © 2019 币源网络. All rights reserved.
//

#import "BYArticleRootViewModel.h"
#import "BYHomeSearchController.h"
#import "BYArticleRootSubController.h"

#import "BYVideoHomeTopicSelView.h"
#import "BYHomeSearchNavigationView.h"
#import "BYArticleRootRankingView.h"
#import "HGSegmentedPageViewController.h"
#import "HGCenterBaseTableView.h"


#import "BYHomeArticleModel.h"

@interface BYArticleRootViewModel ()<HGSegmentedPageViewControllerDelegate, HGPageViewControllerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    // 话题
    NSString *_topic;
}
/** 下拉话题按钮 */
@property (nonatomic ,strong) UIButton *downBtn;
/** selectView */
@property (nonatomic ,strong) UIView *selectView;
/** selectLab */
@property (nonatomic ,strong) UILabel *selectLab;
/** topicView */
@property (nonatomic ,strong) BYVideoHomeTopicSelView *topicSelView;
/** searchView */
@property (nonatomic ,strong) BYHomeSearchNavigationView *searchView;
/** 排行榜 */
@property (nonatomic ,strong) BYArticleRootRankingView *rankingView;
/** 热门 */
@property (nonatomic ,strong) BYArticleRootSubController *hotArticleController;
/** 微文 */
@property (nonatomic ,strong) BYArticleRootSubController *shortArticleController;
/** 新鲜 */
@property (nonatomic ,strong) BYArticleRootSubController *freshArticleController;
/** tableView */
@property (nonatomic ,strong) HGCenterBaseTableView *tableView;
/** segmentControllView */
@property (nonatomic ,strong) HGSegmentedPageViewController *segmentedPageViewController;
/** 是否可滑动 */
@property (nonatomic ,assign) BOOL cannotScroll;
/** tableFooterView */
@property (nonatomic ,strong) UIView *tableFooterView;
@end

@implementation BYArticleRootViewModel

- (void)setContentView{
    [self addSearchView];
    [self addTableView];
    [self loadRequestRanking];
}

- (void)viewWillAppear{
    [self.searchView reloadRedDot];
}

#pragma mark - custom

- (void)autoReload{
    [self.segmentedPageViewController.currentPageViewController autoReload];
}

- (void)reloadData{
    self.hotArticleController.topic = _topic;
    self.shortArticleController.topic = _topic;
    self.freshArticleController.topic = _topic;
}

- (void)selectTopic:(NSString *)topic{
    _topic = topic;
    _selectLab.text = topic.length ? topic : _selectLab.text;
    _selectView.hidden = topic.length ? NO : YES;
    [_selectView layoutIfNeeded];
    [self reloadData];
}

#pragma mark - action

// 删除选择话题
- (void)delBtnAction:(UIButton *)sender{
    _topic = @"";
    _selectView.hidden = YES;
    [self.topicSelView reset];
    [self reloadData];
}

// 展开条件视图
- (void)downBtnAction{
    if (!self.topicSelView.isShow) {
        [self.segmentedPageViewController.view bringSubviewToFront:self.segmentedPageViewController.categoryView];
        [self.topicSelView showAnimationInView:self.segmentedPageViewController.view];
        [self.segmentedPageViewController.view insertSubview:self.topicSelView atIndex:1];
    }else{
        [self.topicSelView hiddenAnimation];
    }
}

// 下拉图片动画
- (void)downBtnAnimation:(BOOL)isOpen{
    [UIView beginAnimations:@"animation" context:nil];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    CGFloat angle = isOpen ? M_PI : 0;
    self.downBtn.transform = CGAffineTransformMakeRotation(angle);
    [UIView commitAnimations];
}

#pragma mark - BYCommonTableViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat contentOffsetY = scrollView.contentOffset.y;
    CGFloat criticalPointOffsetY = 84;
    
    //利用contentOffset处理内外层scrollView的滑动冲突问题
    if (contentOffsetY >= criticalPointOffsetY) {
        
        self.cannotScroll = YES;
        scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        [self.segmentedPageViewController.currentPageViewController makePageViewControllerScroll:YES];
    }else if (contentOffsetY <= 0) {
        self.cannotScroll = NO;
        self.segmentedPageViewController.currentPageViewController.canDownUpdate = YES;
        scrollView.contentOffset = CGPointMake(0, 0);
        [self.segmentedPageViewController.currentPageViewController makePageViewControllerScroll:YES];
    }else {
        self.segmentedPageViewController.currentPageViewController.canDownUpdate = NO;
        if (self.cannotScroll) {
            //“维持吸顶状态”
            scrollView.contentOffset = CGPointMake(0, criticalPointOffsetY);
        } else {
            /* 吸顶状态 -> 不吸顶状态
             * categoryView的子控制器的tableView或collectionView在竖直方向上的contentOffsetY小于等于0时，会通过代理的方式改变当前控制器self.canScroll的值；
             */
        }
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    [self.segmentedPageViewController.currentPageViewController makePageViewControllerScrollToTop];
    return YES;
}

#pragma mark - HGSegmentedPageViewControllerDelegate
- (void)segmentedPageViewControllerWillBeginDragging {
    self.tableView.scrollEnabled = NO;
}

- (void)segmentedPageViewControllerDidEndDragging {
    self.tableView.scrollEnabled = YES;
}

#pragma mark - HGPageViewControllerDelegate
- (void)pageViewControllerLeaveTop {
    self.cannotScroll = NO;
}

#pragma mark - request
- (void)loadRequestRanking{
    @weakify(self);
    [[BYVideoRequest alloc] loadRequestGetArticleRank:^(id  _Nonnull object) {
        @strongify(self);
        if (!object) return ;
        [self.rankingView reloadData:object];
    } faileBlock:^(NSError * _Nonnull error) {
        
    }];
}

#pragma mark - configUI
- (void)addSearchView{
    self.searchView = [[BYHomeSearchNavigationView alloc] init];
    [S_V_VIEW addSubview:self.searchView];
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(kSafe_Mas_Top(60));
    }];
}

- (void)addTableView{
    HGCenterBaseTableView *tableView = [[HGCenterBaseTableView alloc] init];
    tableView.categoryViewHeight = 47;
    tableView.delegate = self;
    tableView.tableFooterView = self.tableFooterView;
    tableView.tableHeaderView = self.rankingView;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.showsHorizontalScrollIndicator = NO;
    [S_V_VIEW addSubview:tableView];
    [S_V_VIEW sendSubviewToBack:tableView];
    self.tableView = tableView;
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(kSafe_Mas_Top(60), 0, 0, 0));
    }];
}

- (void)addSelectView{
    UIView *selectView = [UIView by_init];
    [selectView setBackgroundColor:[UIColor whiteColor]];
    selectView.hidden = YES;
    [self.segmentedPageViewController.categoryView addSubview:selectView];
    selectView.layer.cornerRadius = 2.0f;
    self.selectView = selectView;
    
    CGFloat fontSize = 14;
    CGFloat width = 28;
    UILabel *selectLab = [UILabel by_init];
    selectLab.text = @"快讯";
    [selectLab setBy_font:fontSize];
    selectLab.textColor = kColorRGBValue(0xea6441);
    [selectView addSubview:selectLab];
    self.selectLab = selectLab;
    [selectLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(13);
        make.centerY.mas_equalTo(0);
        make.height.mas_equalTo(selectLab.font.pointSize);
        make.width.mas_greaterThanOrEqualTo(width);
    }];
    
    UIButton *delBtn = [UIButton by_buttonWithCustomType];
    [delBtn setBy_imageName:@"videohome_del_icon" forState:UIControlStateNormal];
    [delBtn addTarget:self action:@selector(delBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [selectView addSubview:delBtn];
    [delBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(selectLab.mas_right).mas_offset(5);
        make.top.bottom.right.mas_equalTo(0);
    }];
    
    [selectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(165);
        make.centerY.mas_equalTo(0);
        make.height.mas_equalTo(27);
        make.right.mas_equalTo(selectLab.mas_right).mas_offset(20);
    }];
    
    // 下拉按钮
    UIButton *downBtn = [UIButton by_buttonWithCustomType];
    [downBtn setBy_imageName:@"videohome_down" forState:UIControlStateNormal];
    [downBtn addTarget:self action:@selector(downBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.segmentedPageViewController.categoryView addSubview:downBtn];
    self.downBtn = downBtn;
    [downBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(43);
        make.height.mas_equalTo(27);
        make.centerY.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    
}

- (UIView *)tableFooterView{
    if (!_tableFooterView) {
        _tableFooterView = [[UIView alloc] init];
        _tableFooterView.frame = CGRectMake(0, 0, kCommonScreenWidth, kCommonScreenHeight - kSafe_Mas_Top(60 + 49));
        [S_VC addChildViewController:self.segmentedPageViewController];
        [_tableFooterView addSubview:self.segmentedPageViewController.view];
        [self.segmentedPageViewController didMoveToParentViewController:S_VC];
        @weakify(self);
        [self.segmentedPageViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.edges.equalTo(self.tableFooterView);
        }];
        [self addSelectView];
    }
    return _tableFooterView;
}

- (HGSegmentedPageViewController *)segmentedPageViewController {
    if (!_segmentedPageViewController) {
        NSMutableArray *controllers = [NSMutableArray array];
        NSArray *titles = @[@"热门",@"微文",@"新鲜"];
        for (int i = 0; i < titles.count; i++) {
            HGPageViewController *controller;
            if (i == 0) {
                controller = self.hotArticleController;
            } else if (i == 1) {
                controller = self.shortArticleController;
            }else{
                controller = self.freshArticleController;
            }
            controller.delegate = self;
            [controllers addObject:controller];
        }
        _segmentedPageViewController = [[HGSegmentedPageViewController alloc] init];
        _segmentedPageViewController.pageViewControllers = controllers;
        _segmentedPageViewController.categoryView.titles = titles;
        _segmentedPageViewController.categoryView.height = 47;
        _segmentedPageViewController.categoryView.alignment = HGCategoryViewAlignmentLeft;
        _segmentedPageViewController.categoryView.originalIndex = 0;
        _segmentedPageViewController.categoryView.leftAndRightMargin = 15.0f;
        _segmentedPageViewController.categoryView.itemSpacing = 20;
        _segmentedPageViewController.categoryView.titleNomalFont = [UIFont systemFontOfSize:14];
        _segmentedPageViewController.categoryView.titleSelectedFont = [UIFont boldSystemFontOfSize:16];
        _segmentedPageViewController.categoryView.titleNormalColor = kColorRGBValue(0x8f8f8f);
        _segmentedPageViewController.categoryView.titleSelectedColor = kColorRGBValue(0x323232);
        _segmentedPageViewController.categoryView.backgroundColor = [UIColor whiteColor];
        _segmentedPageViewController.categoryView.vernierHeight = 3;
        _segmentedPageViewController.categoryView.vernierWidth = 20;
        _segmentedPageViewController.categoryView.vernierBottomSapce = 6.0f;
        _segmentedPageViewController.categoryView.vernier.backgroundColor = kColorRGBValue(0xea6441);
        _segmentedPageViewController.categoryView.topBorder.hidden = YES;
        _segmentedPageViewController.categoryView.bottomBorder.hidden = YES;
        _segmentedPageViewController.delegate = self;
    }
    return _segmentedPageViewController;
}



- (BYVideoHomeTopicSelView *)topicSelView{
    if (!_topicSelView) {
        _topicSelView = [[BYVideoHomeTopicSelView alloc] init];
        _topicSelView.animationY = 55;
        @weakify(self);
        _topicSelView.didSelectTopicHandle = ^(NSString * _Nonnull topic) {
            @strongify(self);
            [self selectTopic:topic];
        };
        _topicSelView.showStatusHandle = ^(BOOL isShow) {
            @strongify(self);
            [self downBtnAnimation:isShow];
        };
    }
    return _topicSelView;
}

- (BYArticleRootRankingView *)rankingView{
    if (!_rankingView) {
        _rankingView = [[BYArticleRootRankingView alloc] init];
        _rankingView.frame = CGRectMake(0, 0, kCommonScreenWidth, 84);
    }
    return _rankingView;
}

- (BYArticleRootSubController *)hotArticleController{
    if (!_hotArticleController) {
        _hotArticleController = [[BYArticleRootSubController alloc] init];
        _hotArticleController.type = BY_ARTICLE_SORT_TYPE_HOT;
    }
    return _hotArticleController;
}

- (BYArticleRootSubController *)shortArticleController{
    if (!_shortArticleController) {
        _shortArticleController = [[BYArticleRootSubController alloc] init];
        _shortArticleController.type = BY_ARTICLE_SORT_TYPE_SOFTTEXT;
    }
    return _shortArticleController;
}

- (BYArticleRootSubController *)freshArticleController{
    if (!_freshArticleController) {
        _freshArticleController = [[BYArticleRootSubController alloc] init];
        _freshArticleController.type = BY_ARTICLE_SORT_TYPE_TIME;
    }
    return _freshArticleController;
}

@end
