//
//  WalletDetailSingleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/25.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletDetailSingleTableViewCell.h"

@interface WalletDetailSingleTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *descLabel;
@property (nonatomic,strong)UILabel *statusLabel;
@property (nonatomic,strong)PDImageView *arrotImgView;

@end

@implementation WalletDetailSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - CreateView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.frame = CGRectMake(LCFloat(20), ([WalletDetailSingleTableViewCell calculationCellHeight] - LCFloat(20)) / 2.,  LCFloat(20), LCFloat(20));
    [self addSubview:self.iconImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"3E3E3E"];
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    [self addSubview:self.titleLabel];
    
    self.timeLabel = [GWViewTool createLabelFont:@"12" textColor:@"A4A4A4"];
    [self addSubview:self.timeLabel];
    
    self.descLabel = [GWViewTool createLabelFont:@"15" textColor:@"3E3E3E"];
    [self addSubview:self.descLabel];
    
    self.statusLabel = [GWViewTool createLabelFont:@"12" textColor:@"3E3E3E"];
    [self addSubview:self.statusLabel];

    self.arrotImgView = [[PDImageView alloc]init];
    self.arrotImgView.backgroundColor = [UIColor clearColor];
    self.arrotImgView.image = [UIImage imageNamed:@"icon_wallet_detail_arrow"];
    [self addSubview:self.arrotImgView];
    
}

-(void)setTransferDetailModel:(WalletTransferDetailModel *)transferDetailModel{
    _transferDetailModel = transferDetailModel;
    
    // margin
    CGFloat margin = ([WalletDetailSingleTableViewCell calculationCellHeight] - [NSString contentofHeightWithFont:self.titleLabel.font] - [NSString contentofHeightWithFont:self.timeLabel.font] - LCFloat(9)) / 2.;
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(12), margin, LCFloat(115), [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.titleLabel.text = transferDetailModel.hashs.length ? transferDetailModel.hashs : @"- -";
    
    // time
    self.timeLabel.text = [NSDate getTimeWithDuobaoString:transferDetailModel.create_time / 1000.];
    self.timeLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(9), self.titleLabel.size_width + 50, [NSString contentofHeightWithFont:self.timeLabel.font]);
    
    //arrow
    self.arrotImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(20) - LCFloat(6), 0, LCFloat(6), LCFloat(12));
    self.arrotImgView.center_y = self.iconImgView.center_y;
    
    self.descLabel.text = [NSString stringWithFormat:@"%@ %@",transferDetailModel.value,transferDetailModel.coinName];
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.descLabel.font])];
    self.descLabel.frame = CGRectMake(self.arrotImgView.orgin_x - LCFloat(13) - descSize.width, self.titleLabel.orgin_y, descSize.width, [NSString contentofHeightWithFont:self.descLabel.font]);
    
    
    if ([transferDetailModel.status isEqualToString:@"0"]){
        self.statusLabel.text = @"等待打包中……";
    } else if ([transferDetailModel.status isEqualToString:@"1"]){
        self.statusLabel.text = @"打包中……";
    } else if ([transferDetailModel.status isEqualToString:@"2"]){
        self.statusLabel.text = @"成功";
    } else if ([transferDetailModel.status isEqualToString:@"3"]){
        self.statusLabel.text = @"失败";
    } else if ([transferDetailModel.status isEqualToString:@"10"]){
        self.statusLabel.text = @"待审核";
    } else if ([transferDetailModel.status isEqualToString:@"11"] || [transferDetailModel.status isEqualToString:@"12"] || [transferDetailModel.status isEqualToString:@"13"]){
        self.statusLabel.text = @"被驳回";
    }
    
    CGSize statusSize = [self.statusLabel.text sizeWithCalcFont:self.statusLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.statusLabel.font])];
    self.statusLabel.frame = CGRectMake(self.arrotImgView.orgin_x - LCFloat(13) - statusSize.width, self.timeLabel.orgin_y, statusSize.width, [NSString contentofHeightWithFont:self.statusLabel.font]);
    
    
    if ([transferDetailModel.status isEqualToString:@"3"]){
        self.descLabel.textColor = [UIColor hexChangeFloat:@"FF7C1C"];
        self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_detail_error"];
    } else {
        if (transferDetailModel.type == 0){
            self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_detail_shouru"];
            self.descLabel.textColor = [UIColor hexChangeFloat:@"01CB4D"];
        } else if (transferDetailModel.type == 1){
            self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_detail_zhichu"];
            self.descLabel.textColor = [UIColor hexChangeFloat:@"377AFF"];
        }
    }
}


+(CGFloat)calculationCellHeight{
    return LCFloat(66);
}

@end
