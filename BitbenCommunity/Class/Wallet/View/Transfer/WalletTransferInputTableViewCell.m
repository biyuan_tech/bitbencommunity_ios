//
//  WalletTransferInputTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletTransferInputTableViewCell.h"

static char tongxunBookActionClickBlockKey;
static char textFieldTextEndBlockKey;
static char textFieldKey;
static char transferButtonActionClickBlockKey;
@interface WalletTransferInputTableViewCell()<UITextFieldDelegate>

@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UIView *inputBgView;
@property (nonatomic,strong)UIButton *allTransferButton;
@end

@implementation WalletTransferInputTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // title
    self.titleLabel = [GWViewTool createLabelFont:@"13" textColor:@"3C3B3B"];
    NSString *tempTitleStr = @"转账金额转账金额转账金额";
    CGSize titleSize = [tempTitleStr sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(LCFloat(15), LCFloat(4), titleSize.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    // dymicLabel
    self.dymicLabel = [GWViewTool createLabelFont:@"13" textColor:@"3C3B3B"];
    [self addSubview:self.dymicLabel];
    
    self.dymicSubLabel = [GWViewTool createLabelFont:@"13" textColor:@"A3A3A3"];
    [self addSubview:self.dymicSubLabel];
    
    self.inputBgView = [[UIView alloc]init];
    self.inputBgView.backgroundColor = [UIColor hexChangeFloat:@"F5F5F5"];
    self.inputBgView.frame = CGRectMake(LCFloat(15), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(15), kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(46));
    [self addSubview:self.inputBgView];
    
    // inputTextField;
    self.inputTextField = [GWViewTool inputTextField];
    self.inputTextField.frame =  CGRectMake(self.inputBgView.orgin_x + LCFloat(18), self.inputBgView.orgin_y, self.inputBgView.size_width - 2 * LCFloat(18), self.inputBgView.size_height);
    self.inputTextField.adjustsFontSizeToFitWidth = YES;
    self.inputTextField.delegate = self;
    [self.inputTextField addTarget:self action:@selector(textFieldDidChanged) forControlEvents:UIControlEventEditingChanged];

    __weak typeof(self)weakSelf = self;
    [self.inputTextField showBarCallback:^(UITextField *textField, NSInteger buttonIndex, UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if ([textField isFirstResponder]){
            [textField resignFirstResponder];
        }
    }];
    
    [self addSubview:self.inputTextField];
    
    
    self.notifyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.notifyButton.backgroundColor = [UIColor clearColor];
    [self.notifyButton setImage:[UIImage imageNamed:@"icon_wallet_book"] forState:UIControlStateNormal];
    self.notifyButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(22), 0, LCFloat(22), LCFloat(22));
    self.notifyButton.center_y = self.titleLabel.center_y;
    [self addSubview:self.notifyButton];
    
    [self.notifyButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &tongxunBookActionClickBlockKey);
        if (block){
            block();
        }
    }];
    
    self.allTransferButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.allTransferButton setTitle:@"全部转换" forState:UIControlStateNormal];
    self.allTransferButton.backgroundColor = [UIColor clearColor];
    self.allTransferButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"13"];
    [self.allTransferButton setTitleColor:[UIColor hexChangeFloat:@"377AFF"] forState:UIControlStateNormal];
    CGSize allTransferSize = [Tool makeSizeWithLabel:self.allTransferButton.titleLabel];
    CGSize mainSize = CGSizeMake(allTransferSize.width + 2 * LCFloat(11), self.inputBgView.size_height);
    self.allTransferButton.frame = CGRectMake(CGRectGetMaxX(self.inputBgView.frame) - mainSize.width, self.inputBgView.orgin_y, mainSize.width, mainSize.height);
    [self.allTransferButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &transferButtonActionClickBlockKey);
        if (block){
            block();
        }
    }];
    self.allTransferButton.hidden = YES;
    [self addSubview:self.allTransferButton];
    
}

-(void)setTransferType:(WalletTransferInputTableViewCellType)transferType{
    _transferType = transferType;
    if (transferType == WalletTransferInputTableViewCellTypeMoney){
        self.titleLabel.text = @"转账金额";
        self.inputTextField.placeholder = @"输入金额";
        self.inputTextField.userInteractionEnabled = YES;
        self.inputTextField.keyboardType = UIKeyboardTypeDecimalPad;
        self.inputTextField.text = @"";
        self.notifyButton.hidden = YES;
        self.dymicLabel.hidden = NO;
        self.dymicSubLabel.hidden = NO;
        self.allTransferButton.hidden = YES;
    } else if (transferType == WalletTransferInputTableViewCellTypeToLocation){
        self.titleLabel.text = @"接收地址";
        self.inputTextField.placeholder = @"请输入地址";
        self.inputTextField.userInteractionEnabled = YES;
        self.inputTextField.keyboardType = UIKeyboardTypeASCIICapable;
        self.inputTextField.text = @"";
        self.notifyButton.hidden = NO;
        self.dymicLabel.hidden = YES;
        self.dymicSubLabel.hidden = YES;
        self.allTransferButton.hidden = YES;
    } else if (transferType == WalletTransferInputTableViewCellTypeFromLocation){
        self.titleLabel.text = @"发送地址";
        self.inputTextField.placeholder = @"请输入地址";
        self.inputTextField.text = [AccountModel sharedAccountModel].block_Address;
        self.inputTextField.userInteractionEnabled = NO;
        self.notifyButton.hidden = YES;
        self.dymicLabel.hidden = YES;
        self.dymicSubLabel.hidden = YES;
        self.allTransferButton.hidden = YES;
    } else if (transferType == WalletTransferInputTableViewCellTypeMailListName){
        self.titleLabel.text = @"姓名";
        self.inputTextField.placeholder = @"请输入姓名";
        self.inputTextField.userInteractionEnabled = YES;
        self.notifyButton.hidden = YES;
        self.dymicLabel.hidden = YES;
        self.dymicSubLabel.hidden = YES;
        self.allTransferButton.hidden = YES;
    } else if (transferType == WalletTransferInputTableViewCellTypeMailListBeizhu){
        self.titleLabel.text = @"备注";
        self.inputTextField.placeholder = @"请输入备注";
        self.inputTextField.userInteractionEnabled = YES;
        self.notifyButton.hidden = YES;
        self.dymicLabel.hidden = YES;
        self.dymicSubLabel.hidden = YES;
        self.allTransferButton.hidden = YES;
     } else if (transferType == WalletTransferInputTableViewCellTypeMailListWalletAddress){
         self.titleLabel.text = @"钱包地址";
         self.inputTextField.placeholder = @"请输入钱包地址";
         self.inputTextField.userInteractionEnabled = YES;
         self.notifyButton.hidden = NO;
         [self.notifyButton setImage:[UIImage imageNamed:@"icon_wallet_copy"] forState:UIControlStateNormal];
         self.dymicLabel.hidden = YES;
         self.dymicSubLabel.hidden = YES;
         self.allTransferButton.hidden = YES;
     } else if (transferType == WalletTransferInputTableViewCellTypeOldPwd){
         self.titleLabel.text = @"请输入您的安全密码";
         self.inputTextField.placeholder = @"请输入您的旧密码";
         self.inputTextField.userInteractionEnabled = YES;
         self.notifyButton.hidden = YES;
         self.dymicLabel.hidden = YES;
         self.dymicSubLabel.hidden = YES;
         self.allTransferButton.hidden = YES;
     } else if (transferType == WalletTransferInputTableViewCellTypeTransferBBT){
         self.titleLabel.text = @"转账成BBT数量";
         self.inputTextField.placeholder = @"请输入转换数量";
         self.inputTextField.userInteractionEnabled = YES;
         self.notifyButton.hidden = YES;
         self.dymicSubLabel.text = @"可转换BP数量";
         self.dymicLabel.hidden = NO;
         self.dymicSubLabel.hidden = NO;
         self.allTransferButton.hidden = NO;
     } else if (transferType == WalletTransferInputTableViewCellTypeTixianMoney){
         self.titleLabel.text = @"提现金额";
         self.inputTextField.placeholder = @"最小提现数量2000";
         self.inputTextField.userInteractionEnabled = YES;
         self.allTransferButton.hidden = NO;
         [self.allTransferButton setTitle:@"全部提现" forState:UIControlStateNormal];
         self.notifyButton.hidden = YES;
         self.dymicSubLabel.hidden = NO;
         self.dymicSubLabel.text = @"可提现BBT数量";
         self.dymicLabel.text = @"12.33";
         self.dymicLabel.hidden = NO;
     } else if (transferType == WalletTransferInputTableViewCellTypeTixianAddress){
         self.titleLabel.text = @"提现地址";
         self.inputTextField.placeholder = @"请输入提现地址";
         self.inputTextField.userInteractionEnabled = YES;
         self.allTransferButton.hidden = YES;
         self.notifyButton.hidden = NO;
         self.dymicSubLabel.hidden = YES;
         self.dymicLabel.hidden = YES;
     } else if (transferType == WalletTransferInputTableViewCellTypeJiesuo){
         self.titleLabel.text = @"解锁BP数量";
         self.inputTextField.placeholder = @"最小解锁数量100BP";
         self.inputTextField.userInteractionEnabled = YES;
         [self.allTransferButton setTitle:@"全部解锁" forState:UIControlStateNormal];
         self.dymicSubLabel.text = @"可解锁BP数量";
         self.dymicLabel.text = @"123123";
         self.dymicLabel.hidden = NO;
         self.dymicSubLabel.hidden = NO;
         self.allTransferButton.hidden = NO;
         self.notifyButton.hidden = YES;
     }
}

-(void)setTransferYue:(CGFloat)transferYue{
    _transferYue = transferYue;
    self.dymicLabel.text = [NSString stringWithFormat:@"%.6f",transferYue];
    CGSize dymicSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.dymicLabel.font])];
    self.dymicLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - dymicSize.width,0 , dymicSize.width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
    self.dymicLabel.center_y = self.titleLabel.center_y;
    
    CGSize dymicSubSize = [self.dymicSubLabel.text sizeWithCalcFont:self.dymicSubLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.dymicSubLabel.font])];
    self.dymicSubLabel.frame = CGRectMake(self.dymicLabel.orgin_x - LCFloat(17) - dymicSubSize.width, 0, dymicSubSize.width, [NSString contentofHeightWithFont:self.dymicSubLabel.font]);
    self.dymicSubLabel.center_y = self.dymicLabel.center_y;
}

-(void)tongxunBookActionClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &tongxunBookActionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)textFieldTextEndBlock:(void(^)())block{
    objc_setAssociatedObject(self, &textFieldTextEndBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    void(^block)() = objc_getAssociatedObject(self, &textFieldTextEndBlockKey);
    if (block){
        block();
    }
}

+(CGFloat)calculationCellHeight{
    return LCFloat(90);
}

+(CGFloat)calculationCellHeightWithType:(WalletTransferInputTableViewCellType)type{
    CGFloat cellHeight = 0;
    if (type == WalletTransferInputTableViewCellTypeTixianAddress){
        cellHeight += 100;
    }
    return cellHeight;
}

-(void)textFieldDidChanged{
    void(^block)(NSString *info) = objc_getAssociatedObject(self, &textFieldKey);
    if (block){
        block(self.inputTextField.text);
    }
}

-(void)textFieldDidChangeBlock:(void (^)(NSString *info))block{
    objc_setAssociatedObject(self, &textFieldKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)transferButtonActionClickBlock:(void(^)())block{
 objc_setAssociatedObject(self, &transferButtonActionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
