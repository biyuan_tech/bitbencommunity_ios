//
//  WalletTransferDetailSingleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletTransferDetailSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;
@property (nonatomic,copy)NSString *transferDymic;

+(CGFloat)calculationCellHeightWithDymic:(NSString *)dymic;

@end

NS_ASSUME_NONNULL_END
