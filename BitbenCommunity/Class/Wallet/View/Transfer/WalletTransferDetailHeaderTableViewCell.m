//
//  WalletTransferDetailHeaderTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletTransferDetailHeaderTableViewCell.h"

@interface WalletTransferDetailHeaderTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UILabel *shoukuanLabel;
@end

@implementation WalletTransferDetailHeaderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImgView];
    
    self.numberLabel = [GWViewTool createLabelFont:@"24" textColor:@"3C3C3C"];
    self.numberLabel.font = [self.numberLabel.font boldFont];
    [self addSubview:self.numberLabel];

    self.shoukuanLabel = [GWViewTool createLabelFont:@"14" textColor:@"01CB4D"];
    [self addSubview:self.shoukuanLabel];
    
}

-(void)setTransferSingleModel:(WalletYuESingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
}

-(void)setTransferTixianModel:(CenterBBTMyFinaniceSubHistoryModel *)transferTixianModel{
    _transferTixianModel = transferTixianModel;
}

-(void)setTransferDetailModel:(WalletTransferDetailModel *)transferDetailModel{
    _transferDetailModel = transferDetailModel;

    self.iconImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(42)) / 2., LCFloat(33), LCFloat(42),LCFloat(42));

    
    NSString *sign = @"";
    if (self.transferTixianModel){
        if (self.transferTixianModel && self.transferTixianModel.status == 1){
            sign = @"-";
        } else {
            sign = @"+";
        }
    } else {
        if (self.transferDetailModel.type == 0){
            sign = @"+";
        } else if (self.transferDetailModel.type == 1){
            sign = @"-";
        } else if (self.transferDetailModel.type == 2){
            sign = @"-";
        } else if (self.transferDetailModel.type == 3){
            sign = @"-";
        } else if (self.transferDetailModel.type == 4){
            sign = @"+";
        }
    }

    if (self.transferTixianModel){
        self.numberLabel.text = [NSString stringWithFormat:@"%@ %@",sign ,transferDetailModel.total_value];
    } else {
        self.numberLabel.text = [NSString stringWithFormat:@"%@ %@",sign ,transferDetailModel.value];
    }
    
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    self.numberLabel.frame = CGRectMake(0, CGRectGetMaxY(self.iconImgView.frame) + LCFloat(19), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.numberLabel.font]);
    
    NSString *shoukuanText = @"";
    if ([transferDetailModel.status isEqualToString:@"0"]){
        shoukuanText = @"等待打包";
        self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_wait"];
    } else if ([transferDetailModel.status isEqualToString:@"1"]){
        shoukuanText = @"打包中";
        self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_wait"];
    } else if ([transferDetailModel.status isEqualToString:@"2"]){
        shoukuanText = transferDetailModel.type == 0 ? @"收款成功" : @"转账成功";
        self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_iconSuccess"];
    } else if ([transferDetailModel.status isEqualToString:@"3"]){
        shoukuanText = transferDetailModel.type == 0 ? @"收款失败" : @"转账失败";
        self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_iconError"];
    }  else if ([transferDetailModel.status isEqualToString:@"10"]){
        shoukuanText = @"转账额度较大，审核中";
        self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_wait"];
    } else if ([transferDetailModel.status isEqualToString:@"11"]){
        shoukuanText = @"账号异常";
        self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_iconError"];
    } else if ([transferDetailModel.status isEqualToString:@"12"]){
        shoukuanText = @"账号存在安全隐患";
        self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_iconError"];
    } else if ([transferDetailModel.status isEqualToString:@"13"]){
        shoukuanText = @"转入非ERC20地址";
        self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_iconError"];
    }
    
    NSString *tixianName = @"";
    if (self.transferSingleModel.name.length){
        tixianName = self.transferSingleModel.name;
    } else if (self.transferDetailModel.name.length){
        tixianName = self.transferDetailModel.name;
    } else {
        tixianName = transferDetailModel.name;
    }
    self.shoukuanLabel.text = [NSString stringWithFormat:@"%@ %@",tixianName,shoukuanText];
    self.shoukuanLabel.textAlignment = NSTextAlignmentCenter;
    self.shoukuanLabel.frame = CGRectMake(0, CGRectGetMaxY(self.numberLabel.frame) + LCFloat(14), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.shoukuanLabel.font]);
    self.shoukuanLabel.textColor = transferDetailModel.type == 0 ? kColorRGBValue(0x01CB4D) : kColorRGBValue(0x377AFF);
}


+(CGFloat)calculationCellHeight{
    return LCFloat(191) - LCFloat(15);
}


@end
