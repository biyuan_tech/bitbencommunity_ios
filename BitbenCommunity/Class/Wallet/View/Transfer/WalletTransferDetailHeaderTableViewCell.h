//
//  WalletTransferDetailHeaderTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "WalletYuEModel.h"
#import "WalletTransferDetailModel.h"
#import "CenterBBTMyFinaniceHistoryModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletTransferDetailHeaderTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)WalletYuESingleModel *transferSingleModel;
@property (nonatomic,strong)WalletTransferDetailModel *transferDetailModel;
@property (nonatomic,strong)CenterBBTMyFinaniceSubHistoryModel *transferTixianModel;            /**< 提现*/

@end

NS_ASSUME_NONNULL_END
