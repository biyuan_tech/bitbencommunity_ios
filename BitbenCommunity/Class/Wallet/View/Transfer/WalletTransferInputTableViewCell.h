//
//  WalletTransferInputTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger ,WalletTransferInputTableViewCellType) {
    WalletTransferInputTableViewCellTypeMoney,                       /**< 转账金额*/
    WalletTransferInputTableViewCellTypeToLocation,                  /**< 接收地址*/
    WalletTransferInputTableViewCellTypeFromLocation,                /**< 发送地址地址*/
    WalletTransferInputTableViewCellTypeMailListName,                /**< 通讯录名字*/
    WalletTransferInputTableViewCellTypeMailListBeizhu,              /**< 通讯录备注*/
    WalletTransferInputTableViewCellTypeMailListWalletAddress,       /**< 通讯录钱包地址*/
    WalletTransferInputTableViewCellTypeOldPwd,                      /**< 旧安全密码*/
    WalletTransferInputTableViewCellTypeTransferBBT,                 /**< 转账*/
    WalletTransferInputTableViewCellTypeTransferBP,                  /**< 转账为BP*/
    // 提现
    WalletTransferInputTableViewCellTypeTixianMoney,                 /**< 提现金额*/
    WalletTransferInputTableViewCellTypeTixianAddress,               /**< 提现地址*/
    // 解锁
    WalletTransferInputTableViewCellTypeJiesuo,                      /**< 解锁*/
};

@interface WalletTransferInputTableViewCell : PDBaseTableViewCell

@property (nonatomic,assign)WalletTransferInputTableViewCellType transferType;
@property (nonatomic,strong)UITextField *inputTextField;
@property (nonatomic,strong)UILabel *dymicSubLabel;
@property (nonatomic,assign)CGFloat transferYue;
@property (nonatomic,strong)UIButton *notifyButton;
@property (nonatomic,strong)UILabel *titleLabel;

-(void)tongxunBookActionClickBlock:(void(^)())block;
-(void)textFieldTextEndBlock:(void(^)())block;
-(void)textFieldDidChangeBlock:(void (^)(NSString *info))block;
-(void)transferButtonActionClickBlock:(void(^)())block;
+(CGFloat)calculationCellHeightWithType:(WalletTransferInputTableViewCellType)type;
@end

NS_ASSUME_NONNULL_END
