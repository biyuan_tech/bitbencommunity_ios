//
//  WalletTransferDetailSingleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletTransferDetailSingleTableViewCell.h"

@interface WalletTransferDetailSingleTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic ,strong) UIButton *copButton;

@end

@implementation WalletTransferDetailSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"3C3C3C"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.frame = CGRectMake(LCFloat(20), LCFloat(15), kScreenBounds.size.width - 2 * LCFloat(20), [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"12" textColor:@"8F8F8F"];
    self.dymicLabel.numberOfLines = 0;
    
    [self addSubview:self.dymicLabel];
    
    self.copButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.copButton setImage:[UIImage imageNamed:@"icon_wallet_copy"] forState:UIControlStateNormal];
    self.copButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(54), 0, LCFloat(40), LCFloat(40));
    [self addSubview:self.copButton];
    self.copButton.hidden = YES;
    @weakify(self);
    [self.copButton buttonWithBlock:^(UIButton *button) {
        @strongify(self);
        [Tool copyWithString:self.dymicLabel.text callback:^{
            [StatusBarManager statusBarHidenWithText:@"地址复制成功"];
        }];
    }];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
}

-(void)setTransferDymic:(NSString *)transferDymic{
    if ([_transferTitle isEqualToString:@"发送地址"] || [_transferTitle isEqualToString:@"接收地址"]) {
        self.copButton.hidden = NO;
    }
    else{
        self.copButton.hidden = YES;
    }
    _transferDymic = transferDymic;
    self.dymicLabel.text = transferDymic;
    CGSize dymicSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(20), CGFLOAT_MAX)];
    self.dymicLabel.frame = CGRectMake(LCFloat(20), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(20), dymicSize.height);
    self.copButton.center = CGPointMake(self.copButton.center.x, self.dymicLabel.center.y);
}


+(CGFloat)calculationCellHeightWithDymic:(NSString *)dymic{
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(20);
    CGFloat height = 0;
    CGSize textSize = [dymic sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"12"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    height += LCFloat(15);
    height += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"15"]];
    height += LCFloat(11);
    height += textSize.height;
    height += LCFloat(14);
    return height;
}

@end
