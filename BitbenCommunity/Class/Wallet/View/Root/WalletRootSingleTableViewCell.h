//
//  WalletRootSingleTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "WalletYuEModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletRootSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)WalletYuESingleModel *transferSingleModel;
@end

NS_ASSUME_NONNULL_END
