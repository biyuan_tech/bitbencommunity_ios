//
//  WalletRootSingleTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletRootSingleTableViewCell.h"

@interface WalletRootSingleTableViewCell()
@property (nonatomic,strong)PDImageView *backImgView;
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *descLabel;

@end

@implementation WalletRootSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.backImgView = [[PDImageView alloc]init];
    self.backImgView.backgroundColor = [UIColor clearColor];
    self.backImgView.image = [Tool stretchImageWithName:@"bg_wallet_root_bi_info"];
    self.backImgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), [WalletRootSingleTableViewCell calculationCellHeight]);
    [self addSubview:self.backImgView];
    
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.frame = CGRectMake(LCFloat(39), ([WalletRootSingleTableViewCell calculationCellHeight] - LCFloat(30)) / 2., LCFloat(30), LCFloat(30));
    [self addSubview:self.iconImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"2B2B2B"];
    NSString *info = @"USTDAS";
    CGSize titleSize = [info sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(14), 0, titleSize.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.titleLabel.center_y = self.iconImgView.center_y;
    [self addSubview:self.titleLabel];
    
    self.descLabel = [GWViewTool createLabelFont:@"16" textColor:@"3E3E3E"];
    CGFloat descWidth = kScreenBounds.size.width - LCFloat(39) - CGRectGetMaxX(self.titleLabel.frame) - LCFloat(30);
    self.descLabel.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + LCFloat(30), 0, descWidth, [NSString contentofHeightWithFont:self.descLabel.font]);
    self.descLabel.textAlignment = NSTextAlignmentRight;
    self.descLabel.center_y = self.titleLabel.center_y;
    [self addSubview:self.descLabel];
}

-(void)setTransferSingleModel:(WalletYuESingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    [self.iconImgView uploadMainImageWithURL:transferSingleModel.pic placeholder:nil imgType:PDImgTypeHD callback:NULL];
    
    self.titleLabel.text = transferSingleModel.name;
    
    self.descLabel.text = transferSingleModel.balance;
}


+(CGFloat)calculationCellHeight{
    return LCFloat(65);
}

@end
