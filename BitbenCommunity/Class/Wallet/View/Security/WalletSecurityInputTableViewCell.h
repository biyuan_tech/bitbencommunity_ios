//
//  WalletSecurityInputTableViewCell.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletSecurityInputTableViewCell : PDBaseTableViewCell
@property (nonatomic,strong)UITextField *inputTextField;

-(void)inputTextFiledTextDidChanged:(void(^)(NSString *info))block;
@end

NS_ASSUME_NONNULL_END
