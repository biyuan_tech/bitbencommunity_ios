//
//  WalletSecurityInputView.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletSecurityInputView.h"

static char actionClickWithGoingPwdKey;
@interface WalletSecurityInputView()

@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *inputBgView;
@property (nonatomic,strong)UILabel *errorLabel;
@property (nonatomic,strong)UIButton *actionClickButton;

@end

@implementation WalletSecurityInputView

-(instancetype)initWithFrame:(CGRect)frame{
    frame = CGRectMake(frame.origin.x, frame.origin.y, kScreenBounds.size.width - 2 * LCFloat(38), LCFloat(224));
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"14" textColor:@"3C3B3B"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.text = @"请输入安全密码";
    self.titleLabel.frame = CGRectMake(0, LCFloat(30), self.size_width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    // inputBg
    self.inputBgView = [[UIView alloc]init];
    self.inputBgView.backgroundColor = [UIColor hexChangeFloat:@"F5F5F5"];;
    self.inputBgView.frame = CGRectMake(LCFloat(21), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(19), self.size_width - 2 * LCFloat(21), LCFloat(46));
    [self addSubview:self.inputBgView];
    
    self.inputTextField = [GWViewTool inputTextField];
    self.inputTextField.placeholder = @"请输入安全密码";
    self.inputTextField.font = [UIFont fontWithCustomerSizeName:@"15"];
    self.inputTextField.frame = self.inputBgView.frame;
    self.inputTextField.clearButtonMode = UITextFieldViewModeAlways;
    self.inputTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.inputTextField.secureTextEntry = YES;
    [self addSubview:self.inputTextField];

    self.errorLabel = [GWViewTool createLabelFont:@"12" textColor:@"FF525A"];
    self.errorLabel.frame = CGRectMake(self.inputBgView.orgin_x, CGRectGetMaxY(self.inputBgView.frame) + LCFloat(13), self.inputBgView.size_width, [NSString contentofHeightWithFont:self.errorLabel.font]);
    self.errorLabel.hidden = YES;
    [self addSubview:self.errorLabel];
    
    self.actionClickButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.actionClickButton setTitle:@"确定" forState:UIControlStateNormal];
    self.actionClickButton.backgroundColor = [UIColor hexChangeFloat:@"377AFF"];
    [self.actionClickButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.actionClickButton.layer.cornerRadius = LCFloat(4);
    self.actionClickButton.clipsToBounds = YES;
    __weak typeof(self)weakSelf = self;
    [self.actionClickButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSString *info) = objc_getAssociatedObject(strongSelf, &actionClickWithGoingPwdKey);
        if (block){
            block(strongSelf.inputTextField.text);
        }
    }];
    self.actionClickButton.frame = CGRectMake((self.size_width - LCFloat(115)) / 2.,CGRectGetMaxY(self.inputBgView.frame) + LCFloat(54) , LCFloat(115), LCFloat(40));
    [self addSubview:self.actionClickButton];
}

-(void)setTransferErrCount:(NSInteger)transferErrCount{
    _transferErrCount = transferErrCount;
    if (transferErrCount != 0){
        self.errorLabel.text = [NSString stringWithFormat:@"密码错误，你还有%li次机会",(long)transferErrCount];
        self.errorLabel.hidden = NO;
    }
}

-(void)actionClickWithGoingPwd:(void(^)(NSString *pwd))block{
    objc_setAssociatedObject(self, &actionClickWithGoingPwdKey,block , OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
