//
//  WalletSecurityInputView.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WalletSecurityInputView : UIView
@property (nonatomic,strong)UITextField *inputTextField;
@property (nonatomic,assign)NSInteger transferErrCount;

-(void)actionClickWithGoingPwd:(void(^)(NSString *pwd))block;

@end

NS_ASSUME_NONNULL_END
