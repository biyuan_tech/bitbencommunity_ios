//
//  WalletSecurityInputTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletSecurityInputTableViewCell.h"

static char inputTextFiledTextDidChangedKey;
@interface WalletSecurityInputTableViewCell()
@property (nonatomic,strong)UIView *inputBgView;

@end

@implementation WalletSecurityInputTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.inputBgView = [[UIView alloc]init];
    self.inputBgView.backgroundColor = RGB(245, 245, 245, 1);
    self.inputBgView.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(46));
    self.inputBgView.layer.cornerRadius = LCFloat(5);
    self.inputBgView.clipsToBounds = YES;
    [self addSubview:self.inputBgView];
    
    self.inputTextField = [GWViewTool inputTextField];
    self.inputTextField.frame = CGRectMake(LCFloat(33), 0, kScreenBounds.size.width - 2 * LCFloat(33), self.inputBgView.size_height);
    self.inputTextField.placeholder = @"请输入";
    [self addSubview:self.inputTextField];
    [self.inputTextField addTarget:self action:@selector(textFieldDidChanged) forControlEvents:UIControlEventEditingChanged];

}

-(void)textFieldDidChanged{
    void(^block)(NSString *info) = objc_getAssociatedObject(self, &inputTextFiledTextDidChangedKey);
    if (block){
        block(self.inputTextField.text);
    }
}

-(void)inputTextFiledTextDidChanged:(void(^)(NSString *info))block{
    objc_setAssociatedObject(self, &inputTextFiledTextDidChangedKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
