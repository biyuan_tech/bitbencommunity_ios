//
//  WalletSecurityMenuTableViewCell.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletSecurityMenuTableViewCell.h"

@implementation WalletSecurityInputTableViewCellModel

@end


@interface WalletSecurityMenuTableViewCell()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@end

@implementation WalletSecurityMenuTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = self.bgImgView;
    [self addSubview:self.bgImgView];
    
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.frame = CGRectMake(LCFloat(35), 0, LCFloat(15), LCFloat(16));
    [self addSubview:self.iconImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"12" textColor:@"FF525A"];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(8), 0, kScreenBounds.size.width - 2 * (CGRectGetMaxX(self.iconImgView.frame) + LCFloat(8)), [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
}

-(void)setTransferModel:(WalletSecurityInputTableViewCellModel *)transferModel{
    _transferModel = transferModel;
    
    if (transferModel.index == 0){
        self.bgImgView.image = [Tool stretchImageWithName:@"bg_center_top"];
        self.iconImgView.orgin_y = LCFloat(24) + (LCFloat(30) - LCFloat(16)) / 2.;
    } else if (transferModel.index == 4){
        self.bgImgView.image = [Tool stretchImageWithName:@"bg_center_bottom"];
        self.iconImgView.orgin_y = (LCFloat(30) - LCFloat(16)) / 2.;
    } else {
        self.bgImgView.image = [Tool stretchImageWithName:@"bg_center_normal"];
        self.iconImgView.orgin_y = (LCFloat(30) - LCFloat(16)) / 2.;
    }
    self.titleLabel.center_y = self.iconImgView.center_y;
    
    
    if (transferModel.successed){
        self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_security_success"];
    } else {
        self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_security_fail"];
    }
    self.titleLabel.text = transferModel.title;
    
    if (transferModel.hasTitle){
        self.iconImgView.hidden = YES;
        self.titleLabel.orgin_x = LCFloat(33);
        self.titleLabel.textColor = [UIColor hexChangeFloat:@"3C3B3B"];
    } else {
        self.iconImgView.hidden = NO;
        self.titleLabel.orgin_x = CGRectGetMaxX(self.iconImgView.frame) + LCFloat(8);
        
        [self iconImgStatus:transferModel.successed];
    }
}

+(CGFloat)calculationCellHeightWithIndex:(NSInteger)index{
    if (index == 0){
        return LCFloat(24) + LCFloat(30);
    } else if (index == 4){
        return LCFloat(24) + LCFloat(30);
    } else {
        return LCFloat(30);
    }
    return 44;
}

-(void)iconImgStatus:(BOOL)status{
    if (status){
        self.titleLabel.textColor = [UIColor hexChangeFloat:@"676666"];
        self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_security_success"];
    } else {
        self.titleLabel.textColor = [UIColor hexChangeFloat:@"FF525A"];
        self.iconImgView.image = [UIImage imageNamed:@"icon_wallet_security_fail"];
    }
}
@end
