//
//  WalletDetailHeaderView.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WalletYuEModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletDetailHeaderView : UIView

@property (nonatomic,strong)WalletYuESingleModel *transferBiSingleModel;

-(void)transferButtonActionClickBlock:(void(^)())block;
-(void)shoukuanButtonActionClickBlock:(void(^)())block;

+(CGFloat)calculationHeight;
@end

NS_ASSUME_NONNULL_END
