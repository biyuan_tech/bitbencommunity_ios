//
//  WalletDetailHeaderView.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletDetailHeaderView.h"

static char transferButtonActionClickBlockKey;
static char shoukuanButtonActionClickBlockKey;
@interface WalletDetailHeaderView()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UILabel *codeLabel;
@property (nonatomic,strong)UIButton *copButton;
@property (nonatomic,strong)UIButton *transferButton;
@property (nonatomic,strong)UIButton *shoukuanButton;
@end

@implementation WalletDetailHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"2B2B2B"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.frame = CGRectMake(0, LCFloat(25), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(38)) / 2., CGRectGetMaxY(self.titleLabel.frame) + LCFloat(12), LCFloat(38), LCFloat(38));
    [self addSubview:self.iconImgView];
    
    self.numberLabel = [GWViewTool createLabelFont:@"18" textColor:@"3E3E3E"];
    self.numberLabel.frame = CGRectMake(0, CGRectGetMaxY(self.iconImgView.frame) + LCFloat(19), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.numberLabel.font]);
    self.numberLabel.font = [self.numberLabel.font boldFont];
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.numberLabel];
    
    self.codeLabel = [GWViewTool createLabelFont:@"13" textColor:@"3E3E3E"];
    [self addSubview:self.codeLabel];
    
    self.copButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.copButton setImage:[UIImage imageNamed:@"icon_wallet_copy"] forState:UIControlStateNormal];
    self.copButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(54) - LCFloat(13), CGRectGetMaxY(self.numberLabel.frame) + LCFloat(20), LCFloat(40), LCFloat(40));
    [self addSubview:self.copButton];
    
    self.codeLabel.frame = CGRectMake(LCFloat(59), 0, self.copButton.orgin_x - LCFloat(13) - LCFloat(59), [NSString contentofHeightWithFont:self.codeLabel.font]);
    self.codeLabel.center_y = self.copButton.center_y;
    
    self.transferButton = [UIButton buttonWithType:UIButtonTypeCustom];
    CGFloat btn_width = (kScreenBounds.size.width - 3 * LCFloat(20)) / 2.;
    self.transferButton.frame = CGRectMake(LCFloat(20), CGRectGetMaxY(self.codeLabel.frame) + LCFloat(40) , btn_width, LCFloat(46));
    [self addSubview:self.transferButton];
    [self.transferButton setBackgroundImage:[UIImage imageNamed:@"bg_wallet_transfer"] forState:UIControlStateNormal];
    [self.transferButton setImage:[UIImage imageNamed:@"icon_wallet_btn_top"] forState:UIControlStateNormal];
    [self.transferButton setTitle:@"转账" forState:UIControlStateNormal];
    [self.transferButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.transferButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleRight imageTitleSpace:LCFloat(10)];
    
    self.shoukuanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.shoukuanButton.frame = CGRectMake(CGRectGetMaxX(self.transferButton.frame) + LCFloat(20), CGRectGetMaxY(self.codeLabel.frame) + LCFloat(40) , btn_width, LCFloat(46));
    [self addSubview:self.shoukuanButton];
    [self.shoukuanButton setImage:[UIImage imageNamed:@"icon_wallet_btn_bottom"] forState:UIControlStateNormal];
    [self.shoukuanButton setBackgroundImage:[UIImage imageNamed:@"bg_wallet_shoukuan"] forState:UIControlStateNormal];
    [self.shoukuanButton setTitle:@"收款" forState:UIControlStateNormal];
    [self.shoukuanButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.shoukuanButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleRight imageTitleSpace:LCFloat(10)];
    
    __weak typeof(self)weakSelf = self;
    [self.transferButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &transferButtonActionClickBlockKey);
        if (block){
            block();
        }
    }];
    
    [self.shoukuanButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &shoukuanButtonActionClickBlockKey);
        if (block){
            block();
        }
    }];
    
}


-(void)setTransferBiSingleModel:(WalletYuESingleModel *)transferBiSingleModel{
    _transferBiSingleModel = transferBiSingleModel;

    // 标题
    self.titleLabel.text = transferBiSingleModel.name;
    // 图标
    [self.iconImgView uploadMainImageWithURL:transferBiSingleModel.pic placeholder:nil imgType:PDImgTypeHD callback:NULL];
    // name
    self.numberLabel.text = transferBiSingleModel.balance;
    // address
    self.codeLabel.text = [AccountModel sharedAccountModel].block_Address;
    CGSize codeSize= [self.codeLabel.text sizeWithCalcFont:self.codeLabel.font constrainedToSize:CGSizeMake(self.codeLabel.size_width, CGFLOAT_MAX)];
    if (codeSize.height >= 2 * [NSString contentofHeightWithFont:self.codeLabel.font]){
        self.codeLabel.size_height = 2 * [NSString contentofHeightWithFont:self.codeLabel.font];
        self.codeLabel.numberOfLines = 2;
    } else {
        self.codeLabel.size_height = 1 * [NSString contentofHeightWithFont:self.codeLabel.font];
        self.codeLabel.numberOfLines = 1;
    }
    
    self.copButton.center_y = self.codeLabel.center_y;
    
    __weak typeof(self)weakSelf = self;
    [self.copButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        [Tool copyWithString:[AccountModel sharedAccountModel].block_Address callback:^{
            [StatusBarManager statusBarHidenWithText:@"地址复制成功"];
        }];
    }];
}


-(void)transferButtonActionClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &transferButtonActionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
-(void)shoukuanButtonActionClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &shoukuanButtonActionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


+(CGFloat)calculationHeight{
    return LCFloat(278);
}

@end
