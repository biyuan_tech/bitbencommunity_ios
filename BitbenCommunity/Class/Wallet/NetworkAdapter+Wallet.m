//
//  NetworkAdapter+Wallet.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/29.
//  Copyright © 2019 币本. All rights reserved.
//

#import "NetworkAdapter+Wallet.h"
#import "WalletAddressModel.h"
#import "WalletTransferShouxufeiModel.h"
#import "WalletCheckResultModel.h"


@implementation NetworkAdapter (Wallet)

-(void)walletCreateWalletWithPwd:(NSString *)pwd block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"block_password":pwd};
    [[NetworkAdapter sharedAdapter] fetchWithPath:create_account requestParams:params responseObjectClass:[WalletAddressModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            WalletAddressModel *walletAddressModel = (WalletAddressModel *)responseObject;
            [AccountModel sharedAccountModel].block_Address = walletAddressModel.block_address;
            if (block){
                block(isSucceeded);
            }
        }
    }];
}


#pragma mark - 获取余额列表
-(void)walletGetYueListManagerWithBlock:(void(^)(BOOL isSuccessed,WalletYuEModel *yueModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params;
    if ([AccountModel sharedAccountModel].block_Address.length){
        params = @{@"block_address":[AccountModel sharedAccountModel].block_Address};
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_balance requestParams:params responseObjectClass:[WalletYuEModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        if (isSucceeded){
            WalletYuEModel *yueModle = (WalletYuEModel *)responseObject;
            if (block){
                block(isSucceeded,yueModle);
            }
        }
    }];
}

// 3. 进行转账
-(void)walletTransferMoneyWithFrom:(NSString *)from to:(NSString *)to value:(NSString *)vale pwd:(NSString *)pwd coinType:(NSString *)coinType block:(void(^)(BOOL isSuccessed ,WalletTransferYanzhengPwdModel *model))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"from":from,@"to":to,@"value":vale,@"block_password":pwd,@"name":coinType};
    [[NetworkAdapter sharedAdapter] fetchWithPath:offline_transaction requestParams:params responseObjectClass:[WalletTransferYanzhengPwdModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            WalletTransferYanzhengPwdModel *model = (WalletTransferYanzhengPwdModel *)responseObject;
            if (block){
                block(YES,model);
            }
        }
    }];
}



// 4. 获取交易记录
-(void)walletRechargeWithType:(WalletRechargeType)type coinType:(id)coinType listBlock:(void(^)(WalletTransferListModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:[AccountModel sharedAccountModel].block_Address forKey:@"block_address"];
    [params setValue:@(type) forKey:@"type"];
    
    if ([coinType isKindOfClass:[NSString class]]){
           [params setValue:coinType forKey:@"name"];
    } else {
        WalletCoinType walletCoinType = (WalletCoinType)coinType;
        if (walletCoinType == WalletCoinTypeBBT){
            [params setValue:@"BBT" forKey:@"name"];
        } else if (walletCoinType == WalletCoinTypeETH){
            [params setValue:@"ETH" forKey:@"name"];
        } else if (walletCoinType == WalletCoinTypeUSDT){
            [params setValue:@"USDT" forKey:@"name"];
        }
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_recharge_list requestParams:params responseObjectClass:[WalletTransferListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            WalletTransferListModel *listModel = (WalletTransferListModel *)responseObject;
            if (block){
                block(listModel);
            }
        }
    }];
}

-(void)walletGetRechargeDetail:(NSString *)hash transferId:(NSString *)transferId block:(void(^)(WalletTransferDetailModel *detailModel))block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (hash.length){
        [params setObject:hash forKey:@"hash"];
    }
    if (transferId.length){
        [params setObject:transferId forKey:@"transaction_id"];
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_transaction requestParams:params responseObjectClass:[WalletTransferDetailModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            WalletTransferDetailModel *detailModel = (WalletTransferDetailModel *)responseObject;
            if (block){
                block(detailModel);
            }
        }
    }];
}


#pragma mark - 获取当前密码错误获取次数
-(void)walletPwdCountNumberWithHash:(NSString *)hash block:(void(^)(NSInteger count))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"block_address":hash};
    [[NetworkAdapter sharedAdapter] fetchWithPath:count_block_password requestParams:params responseObjectClass:[WalletPwdErrorCountModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            WalletPwdErrorCountModel *countModel = (WalletPwdErrorCountModel *)responseObject;
            NSInteger count = countModel.count;
            if (block){
                block(count);
            }
        }
    }];
}

#pragma mark - 计算当前的手续费
-(void)walletTransferMoneyCalculationShouxuWithName:(NSString *)name block:(void(^)(NSString *shouxu))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"name":name};
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_service_charge requestParams:params responseObjectClass:[WalletTransferShouxufeiModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        if (isSucceeded){
            
            WalletTransferShouxufeiModel *model = (WalletTransferShouxufeiModel *)responseObject;
            if(block){
                block(model.service_charge);
            }
        }
    }];
}


#pragma mark - 获取当前通讯录
-(void)walletMailListManagerWithPage:(NSInteger)page block:(void(^)(WalletMailListModel *list))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page_size":@"10",@"page_number":@(page)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:address_book_list requestParams:params responseObjectClass:[WalletMailListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            WalletMailListModel *listModel = (WalletMailListModel *)responseObject;
            if (block){
                block(listModel);
            }
        }
    }];
}

#pragma mark - 添加通讯录
-(void)walletMailListWithAddName:(NSString *)name blockId:(NSString *)blockId address:(NSString *)address remark:(NSString *)remark block:(void(^)(BOOL hasSuccessed ,WalletCheckResultModel *resultModel))block{
    __weak typeof(self)weakSelf = self;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (blockId.length){
        [params setObject:blockId forKey:@"address_book_id"];
    }
    [params setObject:name.length?name:@"" forKey:@"friend_name"];
    [params setObject:address.length?address:@"" forKey:@"block_address"];
    [params setObject:remark.length?remark:@"" forKey:@"remark"];
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:insert_address_book requestParams:params responseObjectClass:[WalletCheckResultModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            WalletCheckResultModel *resultModel = (WalletCheckResultModel *)responseObject;
            if (block){
                block(YES,resultModel);
            }
        }
    }];
}

#pragma mark - 删除通讯录
-(void)walletDelWithAddress:(NSString *)address block:(void(^)(NSString *status))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"block_address":address};
    [[NetworkAdapter sharedAdapter] fetchWithPath:delete_address_book requestParams:params responseObjectClass:[WalletPwdErrorCountModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            WalletPwdErrorCountModel *singleModel = (WalletPwdErrorCountModel *)responseObject;
            if (block){
                block(singleModel.status);
            }
        }
    }];
}

// 11. 判断地址是否成功
-(void)adjustWithAddressHasSuccssed:(NSString *)address block:(void(^)(NSString *error))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"block_address":address};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:check_address_legal requestParams:params responseObjectClass:[WalletCheckResultModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            WalletCheckResultModel *model = (WalletCheckResultModel *)responseObject;
            if (!model.checkResult){
                if (block){
                    block(@"地址验证失败");
                }
            } else {
                if (block){
                    block(@"");
                }
            }
        }
    }];
}

// 12. 校验密码是否正确
-(void)validWalletPassword:(NSString *)password block:(void(^)(WalletPwdErrorCountModel *pwdErrorModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"block_password":password};
    [[NetworkAdapter sharedAdapter] fetchWithPath:valid_block_password requestParams:params responseObjectClass:[WalletPwdErrorCountModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            WalletPwdErrorCountModel *pwdErrorModel = (WalletPwdErrorCountModel *)responseObject;
            if (block){
                block (pwdErrorModel);
            }
        }
    }];
}

// 13.修改密码
-(void)walletChangePwd:(NSString *)oldPwd newPwd:(NSString *)newPwd successBlock:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"block_password":oldPwd,@"new_block_password":newPwd};
    [[NetworkAdapter sharedAdapter] fetchWithPath:update_block_password requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if(block){
                block(YES);
            }
        } else {
            if (block){
                block(NO);
            }
        }
    }];
}


// 14. 判断是否实名认证
-(void)adjustHasRealCertificationStatusManagerBlock:(void(^)(BOOL hasSuccessed,MineRealWalletModel *realModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_certification_status requestParams:nil responseObjectClass:[MineRealWalletModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            MineRealWalletModel *realWalletModel = (MineRealWalletModel *)responseObject;
            if (block){
                block(YES,realWalletModel);
            }
        } else {
            if (block){
                block(NO,nil);
            }
        }
    }];
}

// 15. 验证实名认证信息
-(void)adjustRealInfoManagerWithName:(NSString *)name number:(NSString *)number block:(void(^)(BOOL hasSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"real_name":name,@"ID_number":number};
    [[NetworkAdapter sharedAdapter] fetchWithPath:password_certification_confirm requestParams:params responseObjectClass:[MineRealWalletModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            MineRealWalletModel *model = (MineRealWalletModel *)responseObject;
            if (block){
                if ([model.status isEqualToString:@"1"]){
                    block(YES);
                } else if ([model.status isEqualToString:@"0"]){
                    block(NO);
                }
            }
        }
    }];
}

-(void)walletFindPwdInfoManagerWithNewPwd:(NSString *)newPwd name:(NSString *)name number:(NSString *)number phone:(NSString *)phone smsCode:(NSString *)smsCode block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (newPwd.length){
        [params setValue:newPwd forKey:@"new_block_password"];
    }
    if (name.length){
        [params setValue:name forKey:@"real_name"];
    }
    if (number.length){
        [params setValue:number forKey:@"ID_number"];
    }
    if (phone.length){
        [params setValue:phone forKey:@"phone"];
    }
    if (smsCode.length){
        [params setValue:smsCode forKey:@"sms_code"];
    }
    [[NetworkAdapter sharedAdapter] fetchWithPath:retrieve_block_password requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        if(isSucceeded){
            if (block){
                block(YES);
            }
        }
    }];
}


#pragma mark - 查询是否有钱包和实名认证
-(void)getHasRealInfoAndWalletWithBlock:(void(^)(WalletHasRealModel *walletHasRealModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:get_wallet_status requestParams:nil responseObjectClass:[WalletHasRealModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        
        if (isSucceeded){
            WalletHasRealModel *walletHasRealModel = (WalletHasRealModel *)responseObject;
            if (block){
                block(walletHasRealModel);
            }
        }
    }];
}

#pragma mark - 获取消息信息
-(void)messageGetMessageInfoManagerWithId:(NSString *)messageId block:(void(^)(WalletTransferDetailModel *model))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"user_message_id":messageId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:message_get_message requestParams:params responseObjectClass:[WalletTransferDetailModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            WalletTransferDetailModel *model = (WalletTransferDetailModel *)responseObject;
            if (block){
                block(model);
            }
        }
    }];
}


@end
