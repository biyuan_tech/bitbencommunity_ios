//
//  NetworkAdapter+Wallet.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/29.
//  Copyright © 2019 币本. All rights reserved.
//

#import "NetworkAdapter.h"
#import "WalletYuEModel.h"
#import "WalletTransferDetailModel.h"
#import "WalletPwdErrorCountModel.h"
#import "WalletTransferYanzhengPwdModel.h"
#import "WalletMailListModel.h"
#import "WalletPwdErrorCountModel.h"
#import "MineRealWalletModel.h"
#import "WalletCheckResultModel.h"
#import "WalletHasRealModel.h"

typedef NS_ENUM(NSInteger,WalletRechargeType) {
    WalletRechargeTypeAll = 2,                 /**< 全部收入*/
    WalletRechargeTypeZhichu = 1,              /**< 支出*/
    WalletRechargeTypeShouru = 0,              /**< 收入*/
};

typedef NS_ENUM(NSInteger,WalletCoinType) {
    WalletCoinTypeUSDT = 0,                     /**< USDT*/
    WalletCoinTypeBBT = 1,                      /**< BBT*/
    WalletCoinTypeETH = 2,                      /**< ETH*/
};

NS_ASSUME_NONNULL_BEGIN

@interface NetworkAdapter (Wallet)
// 1. 创建钱包
-(void)walletCreateWalletWithPwd:(NSString *)pwd block:(void(^)(BOOL isSuccessed))block;
// 2. 获取余额
-(void)walletGetYueListManagerWithBlock:(void(^)(BOOL isSuccessed,WalletYuEModel *yueModel))block;
// 3. 进行转账
-(void)walletTransferMoneyWithFrom:(NSString *)from to:(NSString *)to value:(NSString *)vale pwd:(NSString *)pwd coinType:(NSString *)coinType block:(void(^)(BOOL isSuccessed ,WalletTransferYanzhengPwdModel *model))block;
// 4. 获取交易记录
-(void)walletRechargeWithType:(WalletRechargeType)type coinType:(id)coinType listBlock:(void(^)(WalletTransferListModel *listModel))block;
// 5.获取交易详情
-(void)walletGetRechargeDetail:(NSString *)hash transferId:(NSString *)transferId block:(void(^)(WalletTransferDetailModel *detailModel))block;
// 6.获取当前密码错误获取次数
-(void)walletPwdCountNumberWithHash:(NSString *)hash block:(void(^)(NSInteger count))block;
// 7. 获取当前手续费
-(void)walletTransferMoneyCalculationShouxuWithName:(NSString *)name block:(void(^)(NSString *shouxu))block;
// 8.获取当前通讯录
-(void)walletMailListManagerWithPage:(NSInteger)page block:(void(^)(WalletMailListModel *list))block;
// 9.添加x通讯录
-(void)walletMailListWithAddName:(NSString *)name blockId:(NSString *)blockId address:(NSString *)address remark:(NSString *)remark block:(void(^)(BOOL hasSuccessed ,WalletCheckResultModel *resultModel))block;
// 10.删除通讯录
-(void)walletDelWithAddress:(NSString *)address block:(void(^)(NSString *status))block;
// 11. 判断地址是否成功
-(void)adjustWithAddressHasSuccssed:(NSString *)address block:(void(^)(NSString *error))block;

#pragma mark - 密码
// 12. 校验密码是否正确
-(void)validWalletPassword:(NSString *)password block:(void(^)(WalletPwdErrorCountModel *pwdErrorModel))block;
// 13.修改密码
-(void)walletChangePwd:(NSString *)oldPwd newPwd:(NSString *)newPwd successBlock:(void(^)(BOOL isSuccessed))block;
// 14. 判断是否实名认证
-(void)adjustHasRealCertificationStatusManagerBlock:(void(^)(BOOL hasSuccessed,MineRealWalletModel *realModel))block;
// 15. 验证实名认证信息
-(void)adjustRealInfoManagerWithName:(NSString *)name number:(NSString *)number block:(void(^)(BOOL hasSuccessed))block;
-(void)walletFindPwdInfoManagerWithNewPwd:(NSString *)newPwd name:(NSString *)name number:(NSString *)number phone:(NSString *)phone smsCode:(NSString *)smsCode block:(void(^)(BOOL isSuccessed))block;

#pragma mark - 查询是否有钱包和实名认证
-(void)getHasRealInfoAndWalletWithBlock:(void(^)(WalletHasRealModel *walletHasRealModel))block;
#pragma mark - 获取消息信息(钱包)
-(void)messageGetMessageInfoManagerWithId:(NSString *)messageId block:(void(^)(WalletTransferDetailModel *model))block;
@end

NS_ASSUME_NONNULL_END
