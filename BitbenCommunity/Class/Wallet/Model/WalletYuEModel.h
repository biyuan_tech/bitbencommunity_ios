//
//  WalletYuEModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/29.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN
@protocol WalletYuESingleModel <NSObject>

@end

@interface WalletYuESingleModel : FetchModel

@property (nonatomic,copy)NSString *balance;
@property (nonatomic,copy)NSString *currency_id;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *pic;

@end

@interface WalletYuEModel : FetchModel

@property (nonatomic,strong)NSArray *banner;
@property (nonatomic,strong)NSArray<WalletYuESingleModel> *currency;

@end


NS_ASSUME_NONNULL_END
