//
//  WalletTransferShouxufeiModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/15.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletTransferShouxufeiModel : FetchModel

@property (nonatomic,copy)NSString *service_charge;

@end

NS_ASSUME_NONNULL_END
