//
//  WalletTransferDetailModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/29.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol WalletTransferDetailModel <NSObject>

@end

@interface WalletTransferDetailModel : FetchModel

@property (nonatomic,copy)NSString *block_number;
@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,copy)NSString *from;
@property (nonatomic,copy)NSString *hashs;
@property (nonatomic,copy)NSString *service_charge;
@property (nonatomic,copy)NSString *cash_service_charge;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *to;
@property (nonatomic,assign)NSInteger type;
@property (nonatomic,copy)NSString *value;
@property (nonatomic,copy)NSString *coinName;
@property (nonatomic,copy)NSString *transaction_id;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,assign)NSInteger transaction_type;
@property (nonatomic,copy)NSString *total_value;



@end

@interface WalletTransferListModel : FetchModel

@property (nonatomic,copy)NSArray<WalletTransferDetailModel> *recharge_list;
@end

NS_ASSUME_NONNULL_END
