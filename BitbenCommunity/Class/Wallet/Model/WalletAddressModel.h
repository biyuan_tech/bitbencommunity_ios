//
//  WalletAddressModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/29.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletAddressModel : FetchModel

@property (nonatomic,copy)NSString *block_address;

@end

NS_ASSUME_NONNULL_END
