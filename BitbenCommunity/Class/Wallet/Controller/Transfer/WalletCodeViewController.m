//
//  WalletCodeViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletCodeViewController.h"
#import "CenterShareImgModel.h"

@interface WalletCodeViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *codeTableView;
@property (nonatomic,strong)NSArray *codeArr;
@property (nonatomic,strong)UILabel *sloganLabel;
@property (nonatomic,strong)UIImage *codeImg;
@end

@implementation WalletCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createLabel];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"二维码";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.codeArr = @[@[@"icon"]];
    CenterShareImgModel *centerShareImgManager = [[CenterShareImgModel alloc]init];
    self.codeImg = [centerShareImgManager createErweimaWithInfo:[AccountModel sharedAccountModel].block_Address];
    
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.codeTableView){
        self.codeTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.codeTableView.dataSource = self;
        self.codeTableView.delegate = self;
        [self.view addSubview:self.codeTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.codeArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.codeArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"icon" sourceArr:self.codeArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            
            // icon
            PDImageView *iconImgView = [[PDImageView alloc]init];
            iconImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(38)) / 2., 38, LCFloat(38), LCFloat(38));
            iconImgView.stringTag = @"iconImgView";
            [cellWithRowOne addSubview:iconImgView];
            
            // 二维码
            PDImageView *erweimaImg = [[PDImageView alloc]init];
            erweimaImg.frame = CGRectMake((kScreenBounds.size.width - LCFloat(180)) / 2., CGRectGetMaxY(iconImgView.frame) + LCFloat(31), LCFloat(180), LCFloat(180));
            erweimaImg.stringTag = @"erweimaImg";
            [cellWithRowOne addSubview:erweimaImg];
            
            // 3. codeStr
            UILabel *erweimaLabel = [GWViewTool createLabelFont:@"13" textColor:@"3E3E3E"];
            erweimaLabel.stringTag = @"erweimaLabel";
            [cellWithRowOne addSubview:erweimaLabel];
            
            // 4. btn
            UIButton *copButton = [UIButton buttonWithType:UIButtonTypeCustom];
            copButton.backgroundColor = [UIColor clearColor];
            [copButton setImage:[UIImage imageNamed:@"icon_wallet_copy"] forState:UIControlStateNormal];
            copButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(40) - LCFloat(40), 0, LCFloat(40), LCFloat(40));
            [cellWithRowOne addSubview:copButton];
            copButton.stringTag = @"copButton";
            
            // 5. 注意label
            UILabel *zhuyiLabel = [GWViewTool createLabelFont:@"13" textColor:@"A1A1A1"];
            zhuyiLabel.stringTag = @"zhuyiLabel";
            [cellWithRowOne addSubview:zhuyiLabel];
            zhuyiLabel.text = @"注意：该地址仅支持基于以太坊ERC20协议标准的币种，无法用于接收其他协议币种！";
            zhuyiLabel.numberOfLines = 2;
        }
        
        // icon
        PDImageView *iconImg = (PDImageView *)[cellWithRowOne viewWithStringTag:@"iconImgView"];
        [iconImg uploadHDImageWithURL:self.transferSingleModel.pic callback:NULL];
        
        // 二维码
        PDImageView *erweimaImg = (PDImageView *)[cellWithRowOne viewWithStringTag:@"erweimaImg"];
        erweimaImg.image = self.codeImg;
        
        // 二维码label
        UILabel *erweimaLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"erweimaLabel"];
        erweimaLabel.text = [AccountModel sharedAccountModel].block_Address;
        CGFloat width = kScreenBounds.size.width - LCFloat(92) - LCFloat(60);
        CGSize erweiSize =[ erweimaLabel.text sizeWithCalcFont:erweimaLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
        if (erweiSize.height >= 2 * [NSString contentofHeightWithFont:erweimaLabel.font]){
            erweimaLabel.frame = CGRectMake(LCFloat(60), CGRectGetMaxY(erweimaImg.frame) + LCFloat(53), width, 2 * [NSString contentofHeightWithFont:erweimaLabel.font]);
            erweimaLabel.numberOfLines = 2;
        } else {
            erweimaLabel.frame = CGRectMake(LCFloat(60), CGRectGetMaxY(erweimaImg.frame) + LCFloat(53), width, 1 * [NSString contentofHeightWithFont:erweimaLabel.font]);
            erweimaLabel.numberOfLines = 1;
        }
        
        // copButton
        UIButton *copButton = (UIButton *)[cellWithRowOne viewWithStringTag:@"copButton"];
        copButton.center_y = erweimaLabel.center_y;
        __weak typeof(self)weakSelf = self;
        [copButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            [Tool copyWithString:[AccountModel sharedAccountModel].block_Address callback:^{
                [StatusBarManager statusBarHidenWithText:@"地址复制成功"];
            }];
        }];
        
        
        // zhuyiLabel
        UILabel *zhuyiLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"zhuyiLabel"];
        zhuyiLabel.frame = CGRectMake(LCFloat(60), CGRectGetMaxY(erweimaLabel.frame) + LCFloat(35), kScreenBounds.size.width - 2 * LCFloat(60), 2 * [NSString contentofHeightWithFont:zhuyiLabel.font]);
        return cellWithRowOne;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 400;
}

#pragma mark - createLabel
-(void)createLabel{
    self.sloganLabel = [GWViewTool createLabelFont:@"16" textColor:@"000000"];
    self.sloganLabel.text = @"Bitben Wallet";
    self.sloganLabel.font = [self.sloganLabel.font boldFont];
    self.sloganLabel.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(40) - [NSString contentofHeightWithFont:self.sloganLabel.font] - [BYTabbarViewController sharedController].navBarHeight, kScreenBounds.size.width, [NSString contentofHeightWithFont:self.sloganLabel.font]);
    self.sloganLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.sloganLabel];
}


@end
