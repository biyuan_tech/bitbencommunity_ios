//
//  WalletTransferViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletTransferViewController.h"
#import "WalletTransferInputTableViewCell.h"
#import "NetworkAdapter+Wallet.h"
#import "WalletSecurityInputView.h"
#import "JCAlertView.h"
#import "WalletTongxunluViewController.h"

@interface WalletTransferViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UITextField *moneyTextField;
    UITextField *toUstdTextField;
    NSString *shouxufei;
    
    BOOL moneyEnable;
    BOOL addressEnable;
    
    GWButtonTableViewCell *buttonCell;
}
@property (nonatomic,strong)UITableView *transferTableView;
@property (nonatomic,strong)NSArray *transferArr;
@property (nonatomic,strong)UILabel *sloganLabel;
@property (nonatomic,strong)JCAlertView *alertView;
@property (nonatomic,strong)WalletSecurityInputView *inputView;
@end

@implementation WalletTransferViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createSloganView];
    [self arrayWithInit];
    [self createTableView];
    [self createNotifi];
    [self getCurrentShouxufei];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [self.view addGestureRecognizer:tapGesture];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self tapManager];
}

-(void)tapManager{
    if ([moneyTextField isFirstResponder]){
        [moneyTextField resignFirstResponder];
    }
    if ([toUstdTextField isFirstResponder]){
        [toUstdTextField resignFirstResponder];
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"转账";
}


#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.transferArr = @[@[@"转账金额"],@[@"接收地址"],@[@"发送地址"],@[@"手续费"],@[@"下一步"]];
}

#pragma mark - createSloganView
-(void)createSloganView{
    self.sloganLabel = [GWViewTool createLabelFont:@"16" textColor:@"000000"];
    self.sloganLabel.textAlignment = NSTextAlignmentCenter;
    self.sloganLabel.font = [self.sloganLabel.font boldFont];
    self.sloganLabel.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(53) - [BYTabbarViewController sharedController].navBarHeight, kScreenBounds.size.width, LCFloat(53));
    self.sloganLabel.text = @"Bitben Wallet";
    [self.view addSubview:self.sloganLabel];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.transferTableView){
        self.transferTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.transferTableView.dataSource = self;
        self.transferTableView.delegate = self;
        [self.view addSubview:self.transferTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.transferArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.transferArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section < 3){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        WalletTransferInputTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[WalletTransferInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne textFieldDidChangeBlock:^(NSString * _Nonnull info) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf adjustWithYanzhengInfoManager];
        }];
        
        if (indexPath.section == 0){
            cellWithRowOne.transferType = WalletTransferInputTableViewCellTypeMoney;
            moneyTextField = cellWithRowOne.inputTextField;
            [cellWithRowOne textFieldTextEndBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if ([strongSelf-> moneyTextField.text hasPrefix:@"."]){
                    NSString *tempStr = strongSelf-> moneyTextField.text;
                    strongSelf->moneyTextField.text = [NSString stringWithFormat:@"0%@",tempStr];
                }
                
                BOOL isSuccess = [Tool validateFloatNumber:strongSelf->moneyTextField.text];
                strongSelf-> moneyEnable = isSuccess;
                if (!isSuccess){
                    [[UIAlertView alertViewWithTitle:@"提示" message:@"您输入的转账金额有误，请核对后输入" buttonTitles:@[@"确定"] callBlock:NULL]show];
                    return ;
                }
                [strongSelf adjustWithYanzhengInfoManager];
            }];
            cellWithRowOne.dymicSubLabel.text = self.transferYueModel.name;
        } else if (indexPath.section == 1){
            cellWithRowOne.transferType = WalletTransferInputTableViewCellTypeToLocation;
            toUstdTextField = cellWithRowOne.inputTextField;
            cellWithRowOne.dymicSubLabel.text = @"";
            
            [cellWithRowOne textFieldTextEndBlock:^{
                if(!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if ([strongSelf->toUstdTextField.text isEqualToString:[AccountModel sharedAccountModel].block_Address]){
                    strongSelf-> addressEnable = NO;
                    [[UIAlertView alertViewWithTitle:@"提示" message:@"接收地址不得与发送地址相同,请核对后再试" buttonTitles:@[@"确定"] callBlock:NULL]show];
                    return;
                } else {
                    strongSelf-> addressEnable = YES;
                }
                
                [strongSelf adjustWithYanzhengInfoManager];
            }];
            
            [cellWithRowOne tongxunBookActionClickBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf chooseMailListManager];
            }];
        } else if (indexPath.section == 2){
            cellWithRowOne.transferType = WalletTransferInputTableViewCellTypeFromLocation;
            cellWithRowOne.dymicSubLabel.text = @"";
        }
        
        cellWithRowOne.transferYue = [self.transferYueModel.balance floatValue];
        return cellWithRowOne;
    } else if (indexPath.section == 3){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"手续费";
        cellWithRowTwo.transferDesc = [NSString stringWithFormat:@"%@",shouxufei];
        return cellWithRowTwo;
    } else if (indexPath.section == 4){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        buttonCell = cellWithRowThr;
        cellWithRowThr.transferTitle = @"下一步";
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToCuowuPwdCountManager];
        }];
        return cellWithRowThr;
    } else {
        return nil;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section < 3){
        return [WalletTransferInputTableViewCell calculationCellHeight];
    } else if (indexPath.section == 3){
        return [GWNormalTableViewCell calculationCellHeight];
    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - 接口信息
// 进行转账
-(void)sendRequestToTransferManagerWithPwd:(NSString *)pwd toAddress:(NSString *)toAddress value:(NSString *)value coinType:(NSString *)coinType block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] walletTransferMoneyWithFrom:[AccountModel sharedAccountModel].block_Address to:toAddress value:value pwd:pwd coinType:coinType block:^(BOOL isSuccessed, WalletTransferYanzhengPwdModel * _Nonnull model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(isSuccessed && model.password_status){
            [strongSelf transferSuccessedManager];
            if (block){
                block(YES);
            }
        } else {
            strongSelf.inputView.transferErrCount = model.count;
            if (block){
                block(NO);
            }
        }
    }];
}

-(void)transferSuccessedManager{
    __weak typeof(self)weakSelf = self;
    [[UIAlertView alertViewWithTitle:@"转账成功" message:@"转账成功，请等待转账结果" buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.alertView dismissWithCompletion:^{
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }];
    }]show];
}

-(void)sendRequestToCuowuPwdCountManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] walletPwdCountNumberWithHash:[AccountModel sharedAccountModel].block_Address block:^(NSInteger count) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf showAlertInfoManagerWithCount:count hasError:NO];
    }];
}

#pragma mark - showAlertInfoManager
-(void)showAlertInfoManagerWithCount:(NSInteger)count hasError:(BOOL)hasError{
    NSString *error = @"";
    if (!moneyTextField.text.length){
        error = @"请输入转账金额";
    } else if (!toUstdTextField.text.length){
        error = @"请输入转账地址";
    }
    if (hasError){
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }

    if (!self.inputView){
        self.inputView = [[WalletSecurityInputView alloc]initWithFrame:CGRectMake(0, 0, 0, 0 )];
        self.inputView.layer.cornerRadius = LCFloat(4);
        self.inputView.backgroundColor = [UIColor whiteColor];
    }
    __weak typeof(self)weakSelf = self;
    [self.inputView actionClickWithGoingPwd:^(NSString * _Nonnull pwd) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        [strongSelf sendRequestToTransferManagerWithPwd:pwd toAddress:toUstdTextField.text value:moneyTextField.text coinType:strongSelf.transferYueModel.name block:^(BOOL isSuccessed) {
            if (isSuccessed){
                [strongSelf.alertView dismissWithCompletion:NULL];
            }
        }];
    }];
    self.inputView.inputTextField.text = @"";
    self.alertView = [JCAlertView initWithCustomView:self.inputView dismissWhenTouchedBackground:YES];
}



#pragma mark - 键盘通知
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.alertView.orgin_y =  (keyboardRect.size.height) / 3.;
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    CGRect mainRect = CGRectMake(0, 0, kScreenBounds.size.width,self.view.size_height );
    self.transferTableView.frame = mainRect;
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)chooseMailListManager{
    WalletTongxunluViewController *tongxunVC = [[WalletTongxunluViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [tongxunVC actionClickChooseManagerWithBlock:^(WalletMailListSingleModel * _Nonnull singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->toUstdTextField.text = singleModel.friend_address;
        [strongSelf adjustWithYanzhengInfoManager];
    }];
    [self.navigationController pushViewController:tongxunVC animated:YES];
}

#pragma mark - 获取当前手续费
-(void)getCurrentShouxufei{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] walletTransferMoneyCalculationShouxuWithName:self.transferYueModel.name block:^(NSString *shouxu) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->shouxufei = shouxu;
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:3];
        [strongSelf.transferTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark - 验证管理
-(void)adjustWithYanzhengInfoManager{
    if ([toUstdTextField.text hasPrefix:@"0x"] && toUstdTextField.text.length == 42){
        addressEnable = YES;
    } else {
        addressEnable = NO;
    }
    
    if (moneyTextField.text.length && toUstdTextField.text.length && moneyEnable && addressEnable){
        [buttonCell setButtonStatus:YES];
    } else {
        [buttonCell setButtonStatus:NO];
    }
}
@end
