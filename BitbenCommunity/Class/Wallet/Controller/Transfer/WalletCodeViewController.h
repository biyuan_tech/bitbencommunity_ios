//
//  WalletCodeViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "WalletBiSingleModel.h"
#import "WalletYuEModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface WalletCodeViewController : AbstractViewController

@property (nonatomic,strong)WalletBiSingleModel *transferBiSingleModel;
@property (nonatomic,strong)WalletYuESingleModel *transferSingleModel;


@end

NS_ASSUME_NONNULL_END
