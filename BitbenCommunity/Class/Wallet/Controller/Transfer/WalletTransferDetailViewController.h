//
//  WalletTransferDetailViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "WalletYuEModel.h"
#import "WalletTransferDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletTransferDetailViewController : AbstractViewController

@property (nonatomic,strong)WalletYuESingleModel *transferSingleModel;
@property (nonatomic,strong)WalletTransferDetailModel *transferDetailSingleModel;

@end

NS_ASSUME_NONNULL_END
