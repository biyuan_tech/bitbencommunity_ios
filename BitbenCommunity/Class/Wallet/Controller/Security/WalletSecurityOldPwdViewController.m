//
//  WalletSecurityOldPwdViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/27.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletSecurityOldPwdViewController.h"
#import "WalletTransferInputTableViewCell.h"
#import "WalletSecuritySettingPwdViewController.h"
#import "NetworkAdapter+Wallet.h"

@interface WalletSecurityOldPwdViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UITextField *inputTextFiled;
}
@property (nonatomic,strong)UITableView *oldPwdTableView;
@property (nonatomic,strong)NSArray *oldPwdArr;
@end

@implementation WalletSecurityOldPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

-(void)pageSetting{
    self.barMainTitle = @"修改密码";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.oldPwdArr = @[@[@"密码"],@[@"下一步"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.oldPwdTableView){
        self.oldPwdTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.oldPwdTableView.dataSource = self;
        self.oldPwdTableView.delegate = self;
        [self.view addSubview:self.oldPwdTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.oldPwdArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.oldPwdArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        WalletTransferInputTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[WalletTransferInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferType = WalletTransferInputTableViewCellTypeOldPwd;
        inputTextFiled = cellWithRowOne.inputTextField;
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = [GWButtonTableViewCell calculationCellHeight];
        cellWithRowTwo.transferTitle = @"下一步";
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf adjustPwdHasSuccessedManager];
        }];
        return cellWithRowTwo;
    }
    
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return LCFloat(20);
    } else {
        return LCFloat(200);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [WalletTransferInputTableViewCell calculationCellHeight];
    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}


-(void)adjustPwdHasSuccessedManager{
    if (inputTextFiled.text.length){
        __weak typeof(self)weakSelf = self;
        [[NetworkAdapter sharedAdapter] validWalletPassword:inputTextFiled.text block:^(WalletPwdErrorCountModel * _Nonnull pwdErrorModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (pwdErrorModel.password_status){
                [StatusBarManager statusBarHidenWithText:@"密码校验成功"];
                WalletSecuritySettingPwdViewController *securityVC = [[WalletSecuritySettingPwdViewController alloc]init];
                securityVC.transferType = WalletSecuritySettingPwdViewControllerTypeUpdate;
                securityVC.transferOldPwd = strongSelf->inputTextFiled.text;
                [strongSelf.navigationController pushViewController:securityVC animated:YES];
            } else {
                NSString *error = [NSString stringWithFormat:@"密码校验次数:%li",(long)pwdErrorModel.count];
                [[UIAlertView alertViewWithTitle:@"密码校验失败" message:error buttonTitles:@[@"确定"] callBlock:NULL]show];
            }
        }];
    }
}

@end
