//
//  WalletFindPwdTempModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/3/1.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletFindPwdTempModel : FetchModel

@property (nonatomic,copy)NSString *n_block_password;
@property (nonatomic,copy)NSString *real_name;
@property (nonatomic,copy)NSString *ID_number;
@property (nonatomic,copy)NSString *phone;
@property (nonatomic,copy)NSString *sms_code;
@end

NS_ASSUME_NONNULL_END
