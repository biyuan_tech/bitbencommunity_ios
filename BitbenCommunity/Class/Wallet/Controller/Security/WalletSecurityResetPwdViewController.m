//
//  WalletSecurityResetPwdViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/28.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletSecurityResetPwdViewController.h"
#import "LoginInputTableViewCell.h"
#import "NetworkAdapter+Login.h"
#import "MineRealNameViewController.h"
#import "WalletSecuritySettingPwdViewController.h"

@interface WalletSecurityResetPwdViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UITextField *phoneTextField;
    UITextField *smsTextField;
    LoginInputTableViewCell *smsCell;
}
@property (nonatomic,strong)UITableView *findTableView;
@property (nonatomic,strong)NSArray *findArr;
@end

@implementation WalletSecurityResetPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    phoneTextField.text = [AccountModel sharedAccountModel].loginServerModel.account.phone;
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"找回安全密码";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.findArr = @[@[@"手机",@"验证码"],@[@"下一步"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.findTableView){
        self.findTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.findTableView.dataSource = self;
        self.findTableView.delegate = self;
        [self.view addSubview:self.findTableView];
    }
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.findArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.findArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"手机" sourceArr:self.findArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        LoginInputTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[LoginInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"手机" sourceArr:self.findArr]){
            cellWithRowOne.transferType = LoginInputTypePhone;
            phoneTextField = cellWithRowOne.inputTextField;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"验证码" sourceArr:self.findArr]){
            cellWithRowOne.transferType = LoginInputTypeSMS;
            smsTextField = cellWithRowOne.inputTextField;
            smsCell = cellWithRowOne;
            __weak typeof(self)weakSelf = self;
            [cellWithRowOne actionClickWithSMSBtn:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf sendRequestToGetSMSCode];
            }];
        }
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"下一步";
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf loginValidateSMSCodeWithPhone];
        }];
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [LoginInputTableViewCell calculationCellHeight];
    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1){
        return LCFloat(60);
    } else {
        return LCFloat(20);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - Interface
-(void)sendRequestToGetSMSCode{
    NSString *error = @"";
    if (!phoneTextField.text.length){
        error = @"请输入手机号码";
    }
    if (error.length){
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginSendSMSCodeWithFindPwd:phoneTextField.text block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [StatusBarManager statusBarHidenWithText:@"验证码发送成功"];
            [strongSelf->smsCell startTimer];
        } else {
            [StatusBarManager statusBarHidenWithText:@"验证码发送失败，请稍后再试"];
        }
    }];
}

#pragma mark - 校验验证码
-(void)loginValidateSMSCodeWithPhone{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginValidateSMSCodeWithPhone:phoneTextField.text smsCode:smsTextField.text block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){           // 表示校验成功
            WalletFindPwdTempModel *tempModel = [[WalletFindPwdTempModel alloc]init];
            tempModel.phone = strongSelf->phoneTextField.text;
            tempModel.sms_code = strongSelf->smsTextField.text;
            
            WalletSecuritySettingPwdViewController *settingPwdVC = [[WalletSecuritySettingPwdViewController alloc]init];
            settingPwdVC.transferType = WalletSecuritySettingPwdViewControllerTypeFind;             // 找回密码
            settingPwdVC.transferFindBlockPwdModel = tempModel;
            [strongSelf.navigationController pushViewController:settingPwdVC animated:YES];
        }
    }];
}


@end
