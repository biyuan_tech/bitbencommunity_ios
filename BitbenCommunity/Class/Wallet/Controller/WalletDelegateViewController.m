//
//  WalletDelegateViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/25.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletDelegateViewController.h"
#import "CenterAboutViewController.h"
#import "WalletSecuritySettingPwdViewController.h"

@interface WalletDelegateViewController (){
    BOOL delegateButtonHasSelected;
}
@property (nonatomic,strong)UIButton *createButton;
@property (nonatomic,strong)UIButton *delegateButton;
@property (nonatomic,strong)PDWebViewController *aboutVC;
@end

@implementation WalletDelegateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"用户协议";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    delegateButtonHasSelected = NO;
}

#pragma mark - createView
-(void)createView{
    self.createButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.createButton setTitle:@"创建钱包" forState:UIControlStateNormal];
    [self.createButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.createButton.frame = CGRectMake(LCFloat(15), kScreenBounds.size.height - LCFloat(33) - [BYTabbarViewController sharedController].navBarHeight - LCFloat(46), kScreenBounds.size.width - 2 * LCFloat(15), LCFloat(46));
    [self.view addSubview:self.createButton];
    self.createButton.clipsToBounds = YES;
    self.createButton.layer.cornerRadius = LCFloat(4);
    [self createButtonStatus:NO];
    
    // delegateButton
    self.delegateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.delegateButton setImage:[UIImage imageNamed:@"icon_wallet_delegate_nor"] forState:UIControlStateNormal];
    [self.view addSubview:self.delegateButton];

    [self.delegateButton setTitleColor:[UIColor hexChangeFloat:@"2B4BED"] forState:UIControlStateNormal];
    [self.delegateButton setTitle:@"我已仔细阅读并同意以上条款" forState:UIControlStateNormal];
    self.delegateButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"13"];
    [self.delegateButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleLeft imageTitleSpace:LCFloat(10)];
    
    CGSize delegateSize = [self.delegateButton.titleLabel.text sizeWithCalcFont:self.delegateButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.delegateButton.titleLabel.font])];
    CGFloat width = LCFloat(17) + LCFloat(11) + delegateSize.width;
    self.delegateButton.frame = CGRectMake(LCFloat(15), self.createButton.orgin_y - LCFloat(40),width, LCFloat(40));
    
    __weak typeof(self)weakSelf = self;
    [self.delegateButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf delegateButtonManager];
    }];
    
    self.aboutVC = [[PDWebViewController alloc]init];
    [self.aboutVC webDirectedWebUrl:wallet_user_delegate];
    self.aboutVC.view.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.delegateButton.orgin_y + [BYTabbarViewController sharedController].navBarHeight);
    [self addChildViewController:self.aboutVC];
    [self.view addSubview:self.aboutVC.view];
    
    
    [self.createButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        WalletSecuritySettingPwdViewController *settingPwdVC = [[WalletSecuritySettingPwdViewController alloc]init];
        settingPwdVC.transferType = WalletSecuritySettingPwdViewControllerTypeCreate;
        [strongSelf.navigationController pushViewController:settingPwdVC animated:YES];
    }];
}

-(void)createButtonStatus:(BOOL)status{
    if(status){
        self.createButton.backgroundColor = [UIColor hexChangeFloat:@"377AFF"];
        self.createButton.userInteractionEnabled = YES;
    } else {
        self.createButton.backgroundColor = [UIColor hexChangeFloat:@"BCBCBC"];
        self.createButton.userInteractionEnabled = NO;
    }
}

-(void)delegateButtonManager{
    if (!delegateButtonHasSelected){
        [self.delegateButton setImage:[UIImage imageNamed:@"icon_wallet_delegate_hlt"] forState:UIControlStateNormal];
        [self createButtonStatus:YES];
    } else {
        [self.delegateButton setImage:[UIImage imageNamed:@"icon_wallet_delegate_nor"] forState:UIControlStateNormal];
        [self createButtonStatus:NO];
    }
    delegateButtonHasSelected = !delegateButtonHasSelected;
}


@end
