//
//  WalletDetailViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/25.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletDetailViewController.h"
#import "WalletDetailHeaderView.h"
#import "WalletCodeViewController.h"
#import "WalletTransferViewController.h"
#import "WalletDetailSingleTableViewCell.h"
#import "WalletTransferDetailViewController.h"
#import "NetworkAdapter+Wallet.h"

@interface WalletDetailViewController ()<UITableViewDataSource,UITableViewDelegate>{
    
}
@property (nonatomic,strong)UITableView *walletDetailTableView;
@property (nonatomic,strong)NSMutableArray *walletMutableArr;
@property (nonatomic,strong)WalletDetailHeaderView *headerView;
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@end

@implementation WalletDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createHeaderView];
    [self createSegmentList];
    [self createTableView];
    [self sendRequestToGetRechargeListWithType:WalletRechargeTypeAll];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"Bitben Wallet";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.walletMutableArr = [NSMutableArray array];
}

#pragma mark - createHeaderView
-(void)createHeaderView{
    __weak typeof(self)weakSelf = self;

    self.headerView = [[WalletDetailHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [WalletDetailHeaderView calculationHeight])];
    self.headerView.transferBiSingleModel = self.transferSingleModel;
    [self.headerView shoukuanButtonActionClickBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        WalletCodeViewController *shoukuanVC = [[WalletCodeViewController alloc]init];
        shoukuanVC.transferSingleModel = strongSelf.transferSingleModel;
        [strongSelf.navigationController pushViewController:shoukuanVC animated:YES];
    }];
    [self.headerView transferButtonActionClickBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        WalletTransferViewController *transferVC = [[WalletTransferViewController alloc]init];
        transferVC.transferYueModel = strongSelf.transferSingleModel;
        [strongSelf.navigationController pushViewController:transferVC animated:YES];
    }];
    
    [self.view addSubview:self.headerView];
}

#pragma mark - createSegment
-(void)createSegmentList{
    if (!self.segmentList){
        __weak typeof(self)weakSelf = self;
        self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"全部",@"转账",@"收款"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (index == 0){
                [strongSelf sendRequestToGetRechargeListWithType:WalletRechargeTypeAll];
            } else if (index == 1){
                [strongSelf sendRequestToGetRechargeListWithType:WalletRechargeTypeZhichu];
            } else {
                [strongSelf sendRequestToGetRechargeListWithType:WalletRechargeTypeShouru];
            }
        }];
        self.segmentList.frame = CGRectMake(0, CGRectGetMaxY(self.headerView.frame), kScreenBounds.size.width, LCFloat(48));
        [self.view addSubview:self.segmentList];
    }
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.walletDetailTableView){
        self.walletDetailTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.walletDetailTableView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetMaxY(self.segmentList.frame));
        self.walletDetailTableView.dataSource = self;
        self.walletDetailTableView.delegate = self;
        [self.view addSubview:self.walletDetailTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.walletMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    WalletDetailSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[WalletDetailSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    WalletTransferDetailModel *detailSingleModel = [self.walletMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferDetailModel = detailSingleModel;
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WalletTransferDetailViewController *transferDetailVC = [[WalletTransferDetailViewController alloc]init];
    WalletTransferDetailModel *detailSingleModel = [self.walletMutableArr objectAtIndex:indexPath.row];
    transferDetailVC.transferDetailSingleModel = detailSingleModel;
    transferDetailVC.transferSingleModel = self.transferSingleModel;
    [self.navigationController pushViewController:transferDetailVC animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [WalletDetailSingleTableViewCell calculationCellHeight];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.walletDetailTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.walletMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.walletMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}

#pragma mark - Interface
-(void)sendRequestToGetRechargeListWithType:(WalletRechargeType)type{
    __weak typeof(self)weakSelf = self;
    
    
    [[NetworkAdapter sharedAdapter] walletRechargeWithType:type coinType:self.transferSingleModel.name listBlock:^(WalletTransferListModel * _Nonnull listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.walletMutableArr removeAllObjects];
        for (int i = 0 ; i < listModel.recharge_list.count; i++) {
            WalletTransferDetailModel *detailModel = [listModel.recharge_list objectAtIndex:i];
            detailModel.coinName = strongSelf.transferSingleModel.name;
            [strongSelf.walletMutableArr addObject:detailModel];
        }
        
        [strongSelf.walletDetailTableView reloadData];
        
        if (strongSelf.walletMutableArr.count){
            [strongSelf.walletDetailTableView dismissPrompt];
        } else {
            [strongSelf.walletDetailTableView showPrompt:@"当前没有收支记录" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
        }
    }];
}
@end
