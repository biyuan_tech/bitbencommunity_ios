//
//  WalletDetailViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/25.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "WalletYuEModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletDetailViewController : AbstractViewController

@property (nonatomic,strong)WalletYuESingleModel *transferSingleModel;

@end

NS_ASSUME_NONNULL_END
