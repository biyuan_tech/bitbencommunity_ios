//
//  WalletSettingNotifyCenterViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletSettingNotifyCenterViewController.h"

@interface WalletSettingNotifyCenterViewController ()

@end

@implementation WalletSettingNotifyCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self viewInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"通知中心";
}

#pragma mark - viewInfo
-(void)viewInfo{
    [self.view showPrompt:@"当前没有通知中心内容" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
}


@end
