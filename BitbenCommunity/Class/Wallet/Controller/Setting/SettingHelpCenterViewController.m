//
//  SettingHelpCenterViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/1/30.
//  Copyright © 2019 币本. All rights reserved.
//

#import "SettingHelpCenterViewController.h"

@interface SettingHelpCenterViewController ()

@end

@implementation SettingHelpCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self viewInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"帮助中心";
}

#pragma mark - view
-(void)viewInfo{
    [self.view showPrompt:@"帮助中心没有信息" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
}


@end
