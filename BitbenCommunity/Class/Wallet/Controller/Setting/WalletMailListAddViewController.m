//
//  WalletMailListAddViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletMailListAddViewController.h"
#import "WalletTransferInputTableViewCell.h"
#import "NetworkAdapter+Wallet.h"
#import "PDHUD.h"

static char deleteAddressBlockKey;
static char addAddressBlockKey;
@interface WalletMailListAddViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UITextField *nameTextField;
    UITextField *addressTextField;
    UITextField *beizhuTextField;
    BOOL hasEdit;
    NSString *errorInfo;
    GWButtonTableViewCell *buttonCell;
}
@property (nonatomic,strong)UITableView *mailListAddTableView;
@property (nonatomic,strong)NSArray *mailListAddArr;
@property (nonatomic,strong)UIButton *rightButton;
@end

@implementation WalletMailListAddViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.transferType == WalletMailListAddViewControllerTypeEdit){
        nameTextField.text = self.transferSingleModel.friend_name;
        addressTextField.text = self.transferSingleModel.friend_address;
        beizhuTextField.text = self.transferSingleModel.remark;
        nameTextField.userInteractionEnabled = NO;
        addressTextField.userInteractionEnabled = NO;
        beizhuTextField.userInteractionEnabled = NO;
    } else {
        [self actionAdjustManager];
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.view.backgroundColor = [UIColor whiteColor];
    self.barMainTitle = @"新增联系人";
    if (self.transferType == WalletMailListAddViewControllerTypeEdit){
        __weak typeof(self)weakSelf = self;
        self.rightButton =  [self rightBarButtonWithTitle:@"编辑" barNorImage:nil barHltImage:nil action:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (strongSelf->hasEdit){
                [strongSelf sendRequestToAddWalletInterfaceManagerWithEdit:YES];
            }
          strongSelf->hasEdit = !strongSelf->hasEdit;
          [strongSelf editTypeManager:strongSelf->hasEdit];
        }];
        [self.rightButton setTitleColor:[UIColor hexChangeFloat:@"377AFF"] forState:UIControlStateNormal];
    }
}

-(void)editTypeManager:(BOOL)hasEdit{
    if (hasEdit){
        nameTextField.userInteractionEnabled = YES;
        addressTextField.userInteractionEnabled = YES;
        beizhuTextField.userInteractionEnabled = YES;
        [self.rightButton setTitle:@"完成" forState:UIControlStateNormal];
    } else {
        nameTextField.userInteractionEnabled = NO;
        addressTextField.userInteractionEnabled = NO;
        beizhuTextField.userInteractionEnabled = NO;
        [self.rightButton setTitle:@"编辑" forState:UIControlStateNormal];
    }
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    if (self.transferType == WalletMailListAddViewControllerTypeEdit){
        self.mailListAddArr = @[@[@"姓名"],@[@"备注"],@[@"钱包地址",@"提示"],@[@"删除"]];
    } else if (self.transferType == WalletMailListAddViewControllerTypeAdd){
        self.mailListAddArr = @[@[@"姓名"],@[@"备注"],@[@"钱包地址",@"提示"],@[@"保存"]];
    }
    
}

#pragma mark - UITableView
-(void)createTableView{
    if(!self.mailListAddTableView){
        self.mailListAddTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.mailListAddTableView.dataSource = self;
        self.mailListAddTableView.delegate = self;
        [self.view addSubview:self.mailListAddTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.mailListAddArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.mailListAddArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"保存" sourceArr:self.mailListAddArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWButtonTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if(!cellWithRowOne){
            cellWithRowOne = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferTitle = @"保存";
        buttonCell = cellWithRowOne;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToAddWalletInterfaceManagerWithEdit:NO];
        }];
        
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"删除" sourceArr:self.mailListAddArr]){
        static NSString *cellIdentifyWithRowOne1 = @"cellIdentifyWithRowOne1";
        GWButtonTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne1];
        if(!cellWithRowOne){
            cellWithRowOne = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne1];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;

        [cellWithRowOne.button setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
        [cellWithRowOne.button setBackgroundImage:nil forState:UIControlStateNormal];
        cellWithRowOne.transferTitle = @"删除地址";
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [[UIAlertView alertViewWithTitle:@"确认删除钱包地址？" message:nil buttonTitles:@[@"取消",@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex == 1){
                    [strongSelf sendRequestToDeleteInterfaceManger];
                }
            }]show];
        }];
        
        return cellWithRowOne;
    } else {
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"提示" sourceArr:self.mailListAddArr]){
            static NSString*cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            GWNormalTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            }
            cellWithRowThr.transferCellHeight = LCFloat(30);
            cellWithRowThr.transferTitle = errorInfo;
            cellWithRowThr.titleLabel.textColor = [UIColor colorWithCustomerName:@"红"];
            return cellWithRowThr;
        }
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        WalletTransferInputTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[WalletTransferInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        if(indexPath.section == [self cellIndexPathSectionWithcellData:@"姓名" sourceArr:self.mailListAddArr]){
            cellWithRowTwo.transferType = WalletTransferInputTableViewCellTypeMailListName;
            nameTextField = cellWithRowTwo.inputTextField;
            __weak typeof(self)weakSelf = self;
            [cellWithRowTwo textFieldTextEndBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (addressTextField.text.length){
                    [strongSelf sendRequestToAdjustAddressManager];
                }
            }];
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"备注" sourceArr:self.mailListAddArr]){
            cellWithRowTwo.transferType = WalletTransferInputTableViewCellTypeMailListBeizhu;
            beizhuTextField = cellWithRowTwo.inputTextField;
            __weak typeof(self)weakSelf = self;
            [cellWithRowTwo textFieldTextEndBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (addressTextField.text.length){
                    [strongSelf sendRequestToAdjustAddressManager];
                }
            }];
            
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"钱包地址" sourceArr:self.mailListAddArr]){
            cellWithRowTwo.transferType = WalletTransferInputTableViewCellTypeMailListWalletAddress;
            cellWithRowTwo.notifyButton.hidden = YES;
            addressTextField = cellWithRowTwo.inputTextField;
            __weak typeof(self)weakSelf = self;
            [cellWithRowTwo tongxunBookActionClickBlock:^{
                if(!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [Tool copyWithString:strongSelf->addressTextField.text callback:NULL];
                [StatusBarManager statusBarHidenWithText:@"地址复制成功"];
            }];
            
            [cellWithRowTwo textFieldTextEndBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf sendRequestToAdjustAddressManager];
            }];
        }
        
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"保存" sourceArr:self.mailListAddArr]){      //
        return [GWButtonTableViewCell calculationCellHeight];
    } else {
        return [WalletTransferInputTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == [self cellIndexPathSectionWithcellData:@"保存" sourceArr:self.mailListAddArr]){
        return LCFloat(100);
    }
    return 10;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


#pragma mark - Interface
-(void)sendRequestToAddWalletInterfaceManagerWithEdit:(BOOL)hasEdit{
    __weak typeof(self)weakSelf = self;
    NSString *error = @"";
    if (!nameTextField.text.length){
        error = @"请输入名称";
    } else if (!addressTextField.text.length){
        error = @"请输入地址";
    }
    NSString *blockId = @"";
    if (hasEdit){
        blockId = self.transferSingleModel.address_book_id;
    } else {
        blockId = @"";
    }
    
    [[NetworkAdapter sharedAdapter] walletMailListWithAddName:nameTextField.text blockId:blockId address:addressTextField.text remark:beizhuTextField.text block:^(BOOL hasSuccessed, WalletCheckResultModel * _Nonnull resultModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        if (resultModel.resultMsg.length){
            NSString *title = strongSelf->hasEdit?@"通讯信息修改失败":@"通讯信息添加失败";
            [[UIAlertView alertViewWithTitle:title message:resultModel.resultMsg buttonTitles:@[@"确定"] callBlock:NULL]show];
        } else {
            NSString *title = strongSelf->hasEdit?@"通讯信息修改成功":@"通讯信息添加成功";
            [[UIAlertView alertViewWithTitle:title message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                void(^block)(WalletMailListSingleModel *deleteModel) = objc_getAssociatedObject(strongSelf, &addAddressBlockKey);
                if (block){
                    block(nil);
                }
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }] show];
        }
    }];
}

-(void)sendRequestToDeleteInterfaceManger{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] walletDelWithAddress:addressTextField.text block:^(NSString * _Nonnull status) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(WalletMailListSingleModel *deleteModel) = objc_getAssociatedObject(strongSelf, &deleteAddressBlockKey);
        if (block){
            block(strongSelf.transferSingleModel);
        }
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
}

// 判断地址是否成功
-(void)sendRequestToAdjustAddressManager{
    __weak typeof(self)weakSelf = self;
    [PDHUD showHUDProgress:@"正在验证地址合法性……" diary:1];
    if (addressTextField.text.length){
        [[NetworkAdapter sharedAdapter] adjustWithAddressHasSuccssed:addressTextField.text block:^(NSString * _Nonnull error) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [PDHUD dismissManager];
            if (error.length){
                strongSelf->errorInfo = error;
            } else {
                strongSelf->errorInfo = @"";
            }
            
            NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"提示" sourceArr:strongSelf.mailListAddArr];
            NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"提示" sourceArr:strongSelf.mailListAddArr];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            [strongSelf.mailListAddTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            
            [strongSelf actionAdjustManager];
        }];
    }
}



#pragma mark - block
-(void)deleteAddressBlock:(void(^)(WalletMailListSingleModel *deleteModel))block{
    objc_setAssociatedObject(self, &deleteAddressBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)addAddressBlock:(void(^)(WalletMailListSingleModel *deleteModel))block{
    objc_setAssociatedObject(self, &addAddressBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionAdjustManager{
    if (nameTextField.text.length && addressTextField.text.length && !errorInfo.length){
        [buttonCell setButtonStatus:YES];
    } else {
        [buttonCell setButtonStatus:NO];
    }
}

@end
