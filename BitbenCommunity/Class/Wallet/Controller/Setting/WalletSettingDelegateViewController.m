//
//  WalletSettingDelegateViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletSettingDelegateViewController.h"

@interface WalletSettingDelegateViewController ()

@end

@implementation WalletSettingDelegateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self smart];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"用户协议";
}

#pragma mark - smart
-(void)smart{
    [self.view showPrompt:@"当前没有用户协议" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
}



@end
