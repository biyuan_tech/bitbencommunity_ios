//
//  WalletTongxunluViewController.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "AbstractViewController.h"
#import "NetworkAdapter+Wallet.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletTongxunluViewController : AbstractViewController

-(void)actionClickChooseManagerWithBlock:(void(^)(WalletMailListSingleModel *singleModel))block;

@end

NS_ASSUME_NONNULL_END
