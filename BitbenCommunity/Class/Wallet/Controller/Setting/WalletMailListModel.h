//
//  WalletMailListModel.h
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/26.
//  Copyright © 2019 币本. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol WalletMailListSingleModel <NSObject>

@end

@interface WalletMailListSingleModel : FetchModel

@property (nonatomic,copy)NSString *address_book_id;
@property (nonatomic,copy)NSString *available;
@property (nonatomic,copy)NSString *create_time;
@property (nonatomic,copy)NSString *friend_address;
@property (nonatomic,copy)NSString *friend_name;
@property (nonatomic,copy)NSString *remark;

@end


@interface WalletMailListModel : FetchModel

@property (nonatomic,strong)NSArray<WalletMailListSingleModel>  *content;


@end

NS_ASSUME_NONNULL_END
