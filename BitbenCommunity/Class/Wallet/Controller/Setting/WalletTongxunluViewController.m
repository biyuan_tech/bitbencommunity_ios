//
//  WalletTongxunluViewController.m
//  BibenCommunity
//
//  Created by 裴烨烽 on 2019/2/14.
//  Copyright © 2019 币本. All rights reserved.
//

#import "WalletTongxunluViewController.h"
#import "WalletMailListAddViewController.h"

static char actionClickChooseManagerWithBlockKey;
@interface WalletTongxunluViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *mailListTableView;
@property (nonatomic,strong)NSMutableArray *mailListMutableArr;
@end

@implementation WalletTongxunluViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSettng];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetTongxunluManager];
}

#pragma mark - pageSetting
-(void)pageSettng{
    self.barMainTitle = @"通讯录";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_wallet_add"] barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        WalletMailListAddViewController *listVC = [[WalletMailListAddViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        [listVC addAddressBlock:^(WalletMailListSingleModel * _Nonnull deleteModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetTongxunluManager];
        }];
        
        [strongSelf.navigationController pushViewController:listVC animated:YES];
        
        
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.mailListMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.mailListTableView){
        self.mailListTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.mailListTableView.backgroundColor = [UIColor whiteColor];
        self.mailListTableView.dataSource = self;
        self.mailListTableView.delegate = self;
        [self.view addSubview:self.mailListTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mailListMutableArr.count;;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        
        UILabel *titleLabel = [GWViewTool createLabelFont:@"15" textColor:@"3C3C3C"];
        titleLabel.frame = CGRectMake(LCFloat(20), LCFloat(14), kScreenBounds.size.width - 2 * LCFloat(20), [NSString contentofHeightWithFont:titleLabel.font]);
        titleLabel.stringTag = @"titleLabel";
        [cellWithRowOne addSubview:titleLabel];
        
        UILabel *addressLabel = [GWViewTool createLabelFont:@"13" textColor:@"8F8F8F"];
        addressLabel.stringTag = @"addressLabel";
        addressLabel.frame = CGRectMake(LCFloat(20), CGRectGetMaxY(titleLabel.frame) + LCFloat(10), kScreenBounds.size.width - 2 * LCFloat(20), [NSString contentofHeightWithFont:addressLabel.font]);
        [cellWithRowOne addSubview:addressLabel];
    }
    cellWithRowOne.backgroundColor = [UIColor whiteColor];
    WalletMailListSingleModel *singleModel = [self.mailListMutableArr objectAtIndex:indexPath.row];
    
    
    // title
    UILabel *titleLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"titleLabel"];
    titleLabel.text = singleModel.friend_name;
    
    // desc
    UILabel *descLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"addressLabel"];
    descLabel.text = singleModel.friend_address;
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WalletMailListSingleModel *singleModel = [self.mailListMutableArr objectAtIndex:indexPath.row];
    
    void(^block)(WalletMailListSingleModel *singleModel) = objc_getAssociatedObject(self, &actionClickChooseManagerWithBlockKey);
    if (block){
        block(singleModel);
        [self.navigationController popViewControllerAnimated:YES];
        return;
    } else {
        WalletMailListAddViewController *detailVC = [[WalletMailListAddViewController alloc]init];
        detailVC.transferSingleModel = singleModel;
        detailVC.transferType = WalletMailListAddViewControllerTypeEdit;
        __weak typeof(self)weakSelf = self;
        [detailVC deleteAddressBlock:^(WalletMailListSingleModel * _Nonnull deleteModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetTongxunluManager];
        }];
        
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = 0;
    cellHeight = LCFloat(14) + [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"15"]] + [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"13"]] + LCFloat(10) + LCFloat(14);
    return cellHeight;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.mailListTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.mailListMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.mailListMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
    }
}


#pragma mark - 通讯录
-(void)sendRequestToGetTongxunluManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] walletMailListManagerWithPage:self.mailListTableView.currentPage block:^(WalletMailListModel * _Nonnull list) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.mailListMutableArr removeAllObjects];
        [strongSelf.mailListMutableArr addObjectsFromArray:list.content];
        [strongSelf.mailListTableView reloadData];
        if (strongSelf.mailListMutableArr.count){
            [strongSelf.mailListTableView dismissPrompt];
        } else {
            [strongSelf.mailListTableView showPrompt:@"当前没有通讯录信息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
        }
    }];
}

-(void)actionClickChooseManagerWithBlock:(void(^)(WalletMailListSingleModel *singleModel))block{
    objc_setAssociatedObject(self, &actionClickChooseManagerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
